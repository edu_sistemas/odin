<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_cursos_evaluacion() {
    include("conex.php");
    $sql = " CALL sp_moodle_cursos_evaluacion_pendientes_select()";
    $result = $conexion->query($sql);
    if ($result->num_rows > 0) {
        while ($shortnames = $result->fetch_array()) {
            $courseid = getCursoMoodleByShortname($shortnames['shortname_moodle']);
            if ($courseid > 0) {
                $exito = cursos_evaluacion($courseid, $shortnames['id_ficha']);
                if ($exito == 1) {
                    echo $shortnames['shortname_moodle'] . " nota OK";
                } else {
                    echo $shortnames['shortname_moodle'] . " nota ERROR";
                }
            } else {
                echo $shortnames['shortname_moodle'] . "no existe en moodle";
            }
        }
    } else {
        echo 'No existen cursos para insertar a la fecha.';
    }
}

function getCursoMoodleByShortname($shortName) {
    $token = 'c88af547fd2d5306e59932e60cb04669';
    $domainname = 'http://pruebamoodle.edutecno.cl';

    $functionname = 'core_course_get_courses_by_field';
    $restformat = 'json';

    $params = array('field' => 'shortname', 'value' => $shortName);

    $serverurl = $domainname . '/webservice/rest/server.php' . '?wstoken=' . $token . '&wsfunction=' . $functionname;
    require_once dirname(__FILE__) . '/curl.php';
    $curl = new curl;
    $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
    $resp = $curl->get($serverurl . $restformat, $params);
    $json = json_encode($resp);

    $obj = json_decode($resp);

    if (strpos($resp, 'exception') !== false) {
        return 0;
    }

    if (empty($obj->courses) || is_null($obj->courses)) {
        return 0;
    }
    return $obj->courses[0]->id;
}

function cursos_evaluacion($courseid, $if_ficha) {
    try {
        include("conex.php");
        mysqli_set_charset($conexion,"utf8");
        $token = 'c88af547fd2d5306e59932e60cb04669';
        $domainname = 'http://pruebamoodle.edutecno.cl';
        $functionname = 'gradereport_user_get_grade_items';
        $restformat = 'json';

        $params = array('courseid' => $courseid);

        $serverurl = $domainname . '/webservice/rest/server.php' . '?wstoken=' . $token . '&wsfunction=' . $functionname;
        require_once dirname(__FILE__) . '/curl.php';
        $curl = new curl;
        $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
        $resp = $curl->post($serverurl . $restformat, $params);

        $obj = json_decode($resp);
        for ($i = 0; $i < count($obj->usergrades); $i++) {
            for ($j = 0; $j < count($obj->usergrades[$i]->gradeitems); $j++) {
                /* $sql = "INSERT INTO `tbl_moodle_curso_evaluacion`
                  (`curso`,`userid`,`evaluacion`,`itemname`,`itemtype`,`itemmodule`,
                  `iteminstance`,`itemnumber`,`categoryid`,`outcomeid`,`scaleid`,
                  `cmid`,`weightraw`,`weightformatted`,`graderaw`,`gradedatesubmitted`,
                  `gradedategraded`,`gradehiddenbydate`,`gradeneedsupdate`,
                  `gradeishidden`,`gradeformatted`,`grademin`,`grademax`,`rangeformatted`,
                  `percentageformatted`,`feedback`,`feedbackformat`, fecha_registro)
                  VALUES
                  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"; */

                $stmt = $conexion->prepare("CALL sp_moodle_curso_evaluacion_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
                $stmt->bind_param("iiiisssiiiiiiisiiiiiisiisssi", $if_ficha, $courseid, $obj->usergrades[$i]->userid, $obj->usergrades[$i]->gradeitems[$j]->id, $obj->usergrades[$i]->gradeitems[$j]->itemname, $obj->usergrades[$i]->gradeitems[$j]->itemtype, $obj->usergrades[$i]->gradeitems[$j]->itemmodule, $obj->usergrades[$i]->gradeitems[$j]->iteminstance, $obj->usergrades[$i]->gradeitems[$j]->itemnumber, $obj->usergrades[$i]->gradeitems[$j]->categoryid, $obj->usergrades[$i]->gradeitems[$j]->outcomeid, $obj->usergrades[$i]->gradeitems[$j]->scaleid, property_exists($obj->usergrades[$i]->gradeitems[$j], 'cmid') ? $obj->usergrades[$i]->gradeitems[$j]->cmid : null, property_exists($obj->usergrades[$i]->gradeitems[$j], 'weightraw') ? $obj->usergrades[$i]->gradeitems[$j]->weightraw : null, property_exists($obj->usergrades[$i]->gradeitems[$j], 'weightformatted') ? $obj->usergrades[$i]->gradeitems[$j]->weightformatted : null, $obj->usergrades[$i]->gradeitems[$j]->graderaw, $obj->usergrades[$i]->gradeitems[$j]->gradedatesubmitted, $obj->usergrades[$i]->gradeitems[$j]->gradedategraded, $obj->usergrades[$i]->gradeitems[$j]->gradehiddenbydate, $obj->usergrades[$i]->gradeitems[$j]->gradeneedsupdate, $obj->usergrades[$i]->gradeitems[$j]->gradeishidden, $obj->usergrades[$i]->gradeitems[$j]->gradeformatted, $obj->usergrades[$i]->gradeitems[$j]->grademin, $obj->usergrades[$i]->gradeitems[$j]->grademax, $obj->usergrades[$i]->gradeitems[$j]->rangeformatted, $obj->usergrades[$i]->gradeitems[$j]->percentageformatted, $obj->usergrades[$i]->gradeitems[$j]->feedback, $obj->usergrades[$i]->gradeitems[$j]->feedbackformat);
                $stmt->execute();
                $stmt->close();
            }
        }
        $conexion->close();
    } catch (Exception $e) {
        $conexion->close();
        echo 'Excepción capturada: ' . $e->getMessage() . "\n";
        return false;
    }
    return true;
}

get_cursos_evaluacion();
