<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function CallAPISendMail($data = false) {
    $method = 'POST';
    $url = 'http://api.edutecno.com/Api/SendEmail/';
    $curl = curl_init();

    switch ($method) {
        case 'POST':
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, array('data' => serialize($data)));
            break;
        case 'PUT':
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, "username:password");
    curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    return curl_exec($curl);
}

function envio_carta_bienvenida() {
    echo "Iniciando proceso de envio de cartas";
    include("conex.php");

    $html = implode('', file(dirname(__FILE__) . "/CartaEnvio.html"));

    $sql = " CALL sp_moodle_get_alumnos_envio_carta()";
    $result = $conexion->query($sql);
    if ($result->num_rows > 0) {
        while ($datos = $result->fetch_array()) {
            if (comprobar_email($datos['correo_contacto'])) {
                $htmlEnvio = "";
                $htmlEnvio = str_replace("&nombre", $datos['nombre_alumno'], $html);
                $htmlEnvio = str_replace("&fechaCierre", $datos['fecha_cierre'], $htmlEnvio); //
                $htmlEnvio = str_replace("&url", "[ESCRIBIR URL]", $htmlEnvio);
                $htmlEnvio = str_replace("&usuario", $datos['rut_alumno'], $htmlEnvio);
                $data_email = array(
                    "destinatarios" => array('email' => $datos['correo_contacto'], 'nombre' => $datos['nombre_alumno']),
                    "cc_a" => array(),
                    "asunto" => "Test no considerar, favor eliminar",
                    "body" => $htmlEnvio,
                    "AltBody" => "Test no considerar, favor eliminar"
                );
                $res = CallAPISendMail($data_email);

                set_alumno_envio_carta(array("id_ficha" => $datos['id_ficha'], "id_alumno" => $datos['id_alumno']));
            }else{
                echo "Correo invalido: ".$datos['correo_contacto'];
            }
        }
    }else{
        echo "No existen alumnos para el envio de cartas.";
    }
}

function set_alumno_envio_carta($data) {
    /*
      lista los docentes
      Descripcion :
      Fecha Actualiza:
     */
    include("conex.php");
    $id_ficha = $data['id_ficha'];
    $id_alumno = $data['id_alumno'];

    $stmt = $conexion->prepare(" CALL sp_moodle_ficha_alumno_insert(?,?)");
    $stmt->bind_param("ii", $id_ficha, $id_alumno);
    $stmt->execute();
    $stmt->close();
    return true;
}

function comprobar_email($email) {
    $mail_correcto = 0;
    if ((strlen($email) >= 6) && (substr_count($email, "@") == 1) && (substr($email, 0, 1) != "@") && (substr($email, strlen($email) - 1, 1) != "@")) {
        if ((!strstr($email, "'")) && (!strstr($email, "\"")) && (!strstr($email, "\\")) && (!strstr($email, "\$")) && (!strstr($email, " "))) {
            if (substr_count($email, ".") >= 1) {
                $term_dom = substr(strrchr($email, '.'), 1);
                if (strlen($term_dom) > 1 && strlen($term_dom) < 5 && (!strstr($term_dom, "@"))) {
                    $antes_dom = substr($email, 0, strlen($email) - strlen($term_dom) - 1);
                    $caracter_ult = substr($antes_dom, strlen($antes_dom) - 1, 1);
                    if ($caracter_ult != "@" && $caracter_ult != ".") {
                        $mail_correcto = 1;
                    }
                }
            }
        }
    }
    if ($mail_correcto)
        return 1;
    else
        return 0;
}

envio_carta_bienvenida();
