<?php 
if(!isset($_REQUEST['rut']))
{
  $rut_alumno=0;
}
else
{
   $rut_alumno=$_REQUEST['rut'];
}
;?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="img/favicon.ico">
	<title>Solicitud_alumno</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

  <link href="font/font-awesome/css/font-awesome.css" rel="stylesheet">
  <link href="css/sweetalert/sweetalert.css" rel="stylesheet">
  <link href="css/clockpicker/clockpicker.css" rel="stylesheet">

</head>
<body class="back">

 <div class="wrapper wrapper-content animated fadeInRight" id="formu" style="display: none;">
     <div class="row  col-md-offset-6">
         <div class="col-lg-11" style="margin-top: 60px">
             <div class="ibox float-e-margins contenedor">
                 <div class="ibox-title border-tp">
                     <h5>Solicitud Alumno</h5>
                 </div>
                 <div class="ibox-content border-btm">
                     <form class="form-horizontal" role="form" id="formSolicitud" name="formSolicitud">
                         <p>Detalle el día y hora del llamado.</p>

                         <div class="form-group"><label class="col-lg-3 control-label">Fecha</label>
                             <div class="col-lg-9" style="padding-right: 31px">
                                 <div class="form-group" id="data1" style="cursor: pointer; margin-bottom: 0px;">
                                     <div class="input-group date">
                                         <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                         <input type="text" id="fechaSolicitud" name="fechaSolicitud" placeholder="Seleccione fecha" class="form-control" value="" requerido="true" mensaje="Seleccione fecha" readonly="true" />
                                     </div>
                                 </div>
                             </div>
                         </div>
                         
                         <div class="form-group"><label class="col-lg-3 control-label">Hora</label>                           
                             <div class="col-lg-9" style="padding-left: 0px">
                                 <div class="input-group clockpicker" data-autoclose="true">
                                     <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                     <input type="text" class="form-control" placeholder="Seleccione hora" id="horaSolicitud" name="horaSolicitud" requerido="true" mensaje="Seleccione fecha" readonly="true">
                                 </div>
                             </div>
                         </div>

                         <!--
                         <div class="form-group"><label class="col-lg-3 control-label">rut alumno</label>
                             <div class="col-lg-9" style="display: none">
                                 <input type="text" class="form-control" placeholder="Seleccione hora" id="rutAlumno" name="rutAlumno" value="">
                             </div>
                         </div> 
                          -->
                         <input type="hidden" value="<?php echo $rut_alumno;?>" id="rutAlumno" name="rutAlumno">
                         <div class="form-group"><label class="col-lg-3 control-label">Comentario</label>
                             <div class="col-lg-9" style="padding-left: 0px">
                                 <textarea class="form-control" name="comentarioSol" id="comentarioSol" style="width: 100%; resize: none;" rows="5" placeholder="Describa solicitud" requerido="true" mensaje="Rellene campo" maxlength="300"></textarea>
                                 <label>Caracteres restantes: <span></span></label>
                             </div>
                         </div>

                         <div class="form-group">
                             <div class="col-lg-offset-2 col-lg-10">
                                 <button class="btn btn-primary pull-right" type="submit">Enviar</button>
                             </div>
                         </div>
                     </form>
                 </div>
             </div>
         </div>
         <div class="col-md-1">            
         </div>
     </div>
 </div>


  <footer>

  </footer>


  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js"></script>

  <script src="js/datapicker/bootstrap-datepicker.js"></script>
  <script src="js/sweetalert/sweetalert.min.js"></script>
  <script src="js/clockpicker/clockpicker.js"></script>  
  <script src="js/Solicitud_alumno.js"></script>

</body>
</html>