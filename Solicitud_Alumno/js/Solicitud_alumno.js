$(document).ready(function () {

	if($("#rutAlumno").val()=="0" || $("#rutAlumno").val()=="")
	{
		swal({
			title: "Solicitud Alumno",
			text: "No ha especificado el Rut del Alumno, \n no es posible realizar la solicitud.",
			type: "error",
			showCancelButton: false,
			showConfirmButton: false,
			closeOnConfirm: false,
			closeOnCancel: false
		});
	}
	else
	{
		$("#formu").show();
	}

	$('.clockpicker').clockpicker({
		format: "HH:MM:ss"
	});

	$('#data1 .input-group.date').datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: false,
		autoclose: true,
		format: "yyyy-mm-dd"
	});


	$("#formSolicitud").submit(function(e){
		e.preventDefault();
		//--
		var str =$('#comentarioSol').val();
		var res = str.trim();
		console.log(res); 
		var qespacio = res.length;
		console.log(qespacio);
		//--
		if($("#fechaSolicitud").val()=="" || $("#horaSolicitud").val()=="" || $("#comentarioSol").val()=="" || qespacio == 0)
		{
			swal({
				title: "Solicitud Alumno",
				text: "Debe completar todos los Campos.",
				type: "warning",
				showCancelButton: false
			});
		}
		else
		{
			generarSolicitud();
		}
	});
});

//-- ELIMINAR ESPACIOS EN FECHA Y HORA
$(function() {
    $('#fechaSolicitud').on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });
    $('#horaSolicitud').on('keypress', function(e) {
        if (e.which == 32)
            return false;
    });
});
//-- FIN ELIMINAR ESPACIOS EN FEHCA Y HORA

//-- CONTADOR DE CARACTERES TEXTAREA
var inputs = "textarea[maxlength]";
$(document).on('keyup', "[maxlength]", function (e) {
	var este = $(this),
	maxlength = este.attr('maxlength'),
	maxlengthint = parseInt(maxlength),
	textoActual = este.val(),
	currentCharacters = este.val().length;
	remainingCharacters = maxlengthint - currentCharacters,
	espan = este.next('label').find('span');			
			// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
			if (document.addEventListener && !window.requestAnimationFrame) {
				if (remainingCharacters <= -1) {
					remainingCharacters = 0;            
				}
			}
			espan.html(remainingCharacters);
			if (!!maxlength) {
				var texto = este.val();	
				if (texto.length >= maxlength) {
					este.val(text.substring(0, maxlength));
					e.preventDefault();
				}
				else if (texto.length < maxlength) {
				}	
			}	
		});

//-- FIN CONTADOR DE CARACTERES TEXTAREA



function generarSolicitud(){

	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#formSolicitud').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(jqXHR, textStatus, errorThrown);
			swal({
				title: "Solicitud",
				text: "Solicitud no ingresada",
				type: "warning",
				showCancelButton: false,
				confirmButtonColor: "#a5dc86",
				confirmButtonText: "Ok",
				closeOnConfirm: false
			});
		},
		success: function (result) {
			if(result.reset==1)
			{
				$("#formSolicitud")[0].reset();
				$("#formu").hide();
			}
			swal({
				title: "Solicitud Alumno",
				text: result.mensaje,
				type: result.tipo_alerta,
				showCancelButton: false,
				showConfirmButton: false,
				closeOnConfirm: false
			});
		},
		url: "Registro.php"
	});  

	//console.log($('#textComentario').val());
}
/*
function exito(){
	$('#formSolicitud').trigger("reset");

	swal({
		title: "Solicitud",
		text: "Solicitud ingresada con éxito.",
		type: "success",
		showCancelButton: false,
		confirmButtonColor: "#a5dc86",
		confirmButtonText: "Ok",
		closeOnConfirm: false
	});
}*/

