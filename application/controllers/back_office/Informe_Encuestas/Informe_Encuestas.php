<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2017-10-19 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Informe_Encuestas extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Informe Encuestas';
    private $nombre_item_plural = 'Informe Encuestas';
    private $package = 'back_office/Informe_Encuestas';
    private $model = 'Informe_Encuestas_model';
    private $view = 'Informe_Encuestas_v';
    private $controller = 'Informe_Encuestas';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        // $this->load->model($this->package2 . '/' . $this->model2, 'contacto');
        // $this->load->model($this->holding, 'modelholding');


        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('Functions');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Informe_Encuestas',
                'label' => 'Informe Encuestas',
                'icono' => ''
            )
        );




        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            //
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.plugin.autotable.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Informe_Encuestas/Informe_Encuestas.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Informe Encuestas";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_empresa($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

      function comprobarFicha() {
        $arr_sesion = array(
            'num_ficha' => $this->input->post('num_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->comprobarFicha($arr_sesion);
        echo json_encode($datos);
    }

    function get_resultados_encuesta_satisfaccion_por_ficha() {
        $arr_sesion = array(
            'num_ficha' => $this->input->post('num_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_resultados_encuesta_satisfaccion_por_ficha($arr_sesion);
        echo json_encode($datos);
    }
    
    function get_resultados_encuesta_satisfaccion_por_fecha_de_cierre() {
        $funciones = new Functions();
        
                $start  = $funciones->formatDateBD(trim($this->input->post('start')));
                $end    = $funciones->formatDateBD(trim($this->input->post('end')));

        $arr_sesion = array(
            'start' => $start,
            'end' => $end,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_resultados_encuesta_satisfaccion_por_fecha_de_cierre($arr_sesion);
        echo json_encode($datos);
    }

    function get_relatores_cbx(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_relatores_cbx($arr_sesion);
        echo json_encode($datos);
    }
    
    function get_edutecno_cbx(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_edutecno_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function get_empresa_cbx(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function get_ejecutivo_cbx(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_ejecutivo_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function get_sede_cbx(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sede_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function get_resultados_encuesta_satisfaccion_por_relator() {
        $calendario_mes = $this->input->post('calendario_mes_x');
        $array_fecha = (explode("-", $calendario_mes));
        
                $mes = $array_fecha[0];
                $year = $array_fecha[1];
                
        $arr_sesion = array(
            'id_relator' => $this->input->post('select_relator'),
            'mes' => $mes,
            'year' => $year,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_resultados_encuesta_satisfaccion_por_relator($arr_sesion);
        echo json_encode($datos);
    }
    
    function get_resultados_anual_encuesta_satisfaccion() {
        $arr_sesion = array(
            'year' => $this->input->post('select_year'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_resultados_anual_encuesta_satisfaccion($arr_sesion);
        echo json_encode($datos);
    }
    
    function get_resultados_mensual_encuesta_satisfaccion() {
        $calendario_mes = $this->input->post('calendario_mes');
        $array_fecha = (explode("-", $calendario_mes));
        
                $mes = $array_fecha[0];
                $year = $array_fecha[1];
        $arr_sesion = array(
            'mes' => $mes,
            'year' => $year,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_resultados_mensual_encuesta_satisfaccion($arr_sesion);
        echo json_encode($datos);
    }

    function get_resultados_encuesta_satisfaccion_por_edutecno() {
        $calendario_mes = $this->input->post('calendario_mes_e');
        $array_fecha = (explode("-", $calendario_mes));
        
                $mes = $array_fecha[0];
                $year = $array_fecha[1];
                
        $arr_sesion = array(
            'id_edutecno' => $this->input->post('select_edutecno'),
            'mes' => $mes,
            'year' => $year,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_resultados_encuesta_satisfaccion_por_edutecno($arr_sesion);
        echo json_encode($datos);
    }
    

    function get_resultados_encuesta_satisfaccion_personalizada() {
        $arr_sesion = array(
            'id_relator' => $this->input->post('select_relator_x'),
            'id_empresa' => $this->input->post('select_empresa'),
            'codigo_curso' => $this->input->post('codigo_curso'),
            'id_ejecutivo' => $this->input->post('select_ejecutivo'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_resultados_encuesta_satisfaccion_personalizada($arr_sesion);
        echo json_encode($datos);
    }

    function getCodeSenceCBX() {
        
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getCodeSenceCBX($arr_sesion);
        echo json_encode($datos);
    }

    function getSalaCBX() {
        $arr_data = array(
            'id_sede' => $this->input->post('id_sede'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getSalaCBX($arr_data);
        echo json_encode($datos);
    }

    function get_resultados_cumplimiento_tecnico(){
        $calendario_mes = $this->input->post('calendario_mes_f');
        $array_fecha = (explode("-", $calendario_mes));
                $mes = count($array_fecha) == 2 ? $array_fecha[0] : '';
                $year = count($array_fecha) == 2 ? $array_fecha[1]: '';
                
        $arr_data = array(
            'year' => $year == null ? '' : $year,
            'mes' => $mes == null ? '' : $mes,
            'id_empresa' => $this->input->post('select_empresa_y') == null ? '': $this->input->post('select_empresa_y'),
            'id_edutecno' => $this->input->post('select_edutecno') == null ? '': $this->input->post('select_edutecno'),
            'codigo_curso' => $this->input->post('codigo_curso_y') == null ? '': $this->input->post('codigo_curso_y'),
            'id_sede' => $this->input->post('select_sede') == null ? '': $this->input->post('select_sede'),
            'id_sala' => $this->input->post('select_sala') == null ? '': $this->input->post('select_sala'),
            'num_ficha' => $this->input->post('num_ficha_y') == null ? '': $this->input->post('num_ficha_y'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_resultados_cumplimiento_tecnico($arr_data);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
