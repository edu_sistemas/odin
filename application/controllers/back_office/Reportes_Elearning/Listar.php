<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Reportes_Elearning';
    private $model = 'Reportes_Elearning_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

                ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Reportes_Elearning',
                'label' => 'Reportes Elearning',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            //
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.plugin.autotable.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reportes_Elearning/listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Códigos Tablet";
        // if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
        //     redirect('back_office/Home', 'refresh');
        // }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_empresa($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getFichas() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->getFichas($arr_sesion);
        echo json_encode($datos);
    }
    function getX() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->getX($arr_sesion);
        echo json_encode($datos);
    }

    function getReporte() {

        var_dump($this->input->post());
        die();

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->getReporte($arr_sesion);
        echo json_encode($datos);
    }

    function generarReporteExcel($id_ficha) {

       $arr_datos = array(
           'id_ficha' => $id_ficha,
           'user_id' => $this->session->userdata('id_user'),
           'user_perfil' => $this->session->userdata('id_perfil')
       );

       $data_excel = $this->modelo->generarReporteExcel($arr_datos);
       
       $this->load->library('excel');

       //activate worksheet number 1
       $this->excel->setActiveSheetIndex(0);
       //name the worksheet
       $this->excel->getActiveSheet()->setTitle('Reporte Curso Elearning');
       //set cell A1 content with some text

       $this->excel->getActiveSheet()->SetCellValue('A1', 'Ficha');        
       $this->excel->getActiveSheet()->SetCellValue('B1', 'Orden de Compra');
       $this->excel->getActiveSheet()->SetCellValue('C1', 'Fecha de Inicio');
       $this->excel->getActiveSheet()->SetCellValue('D1', 'Fecha Fin');
       $this->excel->getActiveSheet()->SetCellValue('E1', 'Ejecutivo');
       $this->excel->getActiveSheet()->SetCellValue('F1', 'Empresa');
       $this->excel->getActiveSheet()->SetCellValue('G1', 'OTIC');
       $this->excel->getActiveSheet()->SetCellValue('H1', 'Curso');
       $this->excel->getActiveSheet()->SetCellValue('I1', 'RUT');
       $this->excel->getActiveSheet()->SetCellValue('J1', 'Nombre');
       $this->excel->getActiveSheet()->SetCellValue('K1', 'Apellido');
       $this->excel->getActiveSheet()->SetCellValue('L1', 'Fecha Ultima Conexión');
       $this->excel->getActiveSheet()->SetCellValue('M1', 'Unidad');
       $this->excel->getActiveSheet()->SetCellValue('N1', 'Avance Unidad');
       $this->excel->getActiveSheet()->SetCellValue('O1', 'Nota Unidad');
       $this->excel->getActiveSheet()->SetCellValue('P1', 'Módulo');
       $this->excel->getActiveSheet()->SetCellValue('Q1', 'Avance Módulo');
       $this->excel->getActiveSheet()->SetCellValue('R1', 'Nota Módulo');
       $this->excel->getActiveSheet()->SetCellValue('S1', 'Tiene DJ');

       $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');
       $this->excel->getActiveSheet()->getStyle('A1:S1')->applyFromArray(
               array(
                   'fill' => array(
                       'type' => PHPExcel_Style_Fill::FILL_SOLID,
                       'color' => array('rgb' => '#D9E1F2')
                   )
               )
       );
       
        $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth('8');
        $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth('8');
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth('8');
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth('8');
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth('15');
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth('8');
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth('25');
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('15');
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('40');
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('40');
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('40');
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('10');
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('10');
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('10');
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('10');
       
       $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
       //make the font become bold
       $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
       //set aligment to center for that merged cell (A1 to D1)
       $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
       $filename = 'Reporte-Elearning-'.$data_excel[0]['ficha'].'-' . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
       ob_clean();
       header('Content-Type: application/vnd.ms-excel'); //mime type
       header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
       header('Cache-Control: private'); //no cache
       //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
       //if you want to save it as .XLSX Excel 2007 format
       $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
       //force user to download the Excel file without writing it to server's HD
       $objWriter->save('php://output');
   }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
