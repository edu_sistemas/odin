<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-02 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-02 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class DetalleFicha extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'DetalleFicha';
    private $nombre_item_plural = 'DetalleFichas';
    private $package = 'back_office/Factura';
    private $model = 'Factura_model';
    private $view = 'DetalleFicha_v';
    private $controller = 'DetalleFicha';
    private $ind = '';
    private $edutecno = 'back_office/Edutecno/Edutecno_model';
    private $ficha = 'back_office/Ficha/Ficha_model';
    private $ficha_control = 'back_office/Ficha_Control/Ficha_Control_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->edutecno, 'modeledutecno');
        $this->load->model($this->ficha, 'modelficha');
        $this->load->model($this->ficha_control, 'modelofichacontrol');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_orden_compra = false, $opcion_ficha = false) {
        
       

        //  si entra a la mala lo redirige al listar
        if ($id_orden_compra == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Facturas',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/DetalleFicha',
                'label' => 'Detalle',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/DetalleFicha.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Documentos Tributarios";

        // if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
        //     redirect('back_office/Home', 'refresh');
        // }

        $arr_input_data = array(
            'id_orden_compra' => $id_orden_compra,
            'opcion_ficha' => $opcion_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id_orden_compra_resumen($arr_input_data);


        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_id_orden_compra($arr_input_data);
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function cargaDatosOC() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelficha->get_arr_listar_datos_orden_compra($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumen() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelficha->get_arr_listar_datos_documentos_resumen_orden_compra($arr_data);
        echo json_encode($datos);
    }

    function getidEdutecnoCBX() {

        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeledutecno->get_arr_edutecno_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function cargaLastDocumentoResumen() {

        $id_ficha = $this->input->post('ficha');


        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelofichacontrol->get_arr_listar_last_documentos_actualesFC($arr_data);
        echo json_encode($datos);
    }

    /* Rectificaciones */

    function getRectificacionesAlumnosByOC() {
        $id_orden_compra = $this->input->post('id_orden_compra');
        //  variables de sesion
        $arr_data = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_by_id_orden_compra($arr_data);
        echo json_encode($datos);
    }

    function getRectificacionesAlumnosDocbyOrdenCompra() {

        $id_orden_compra = $this->input->post('id_orden_compra');
        //  variables de sesion

        $arr_data = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_rectificaciones_doc_by_id_orden_compra($arr_data);
        echo json_encode($datos);
    }

    /* END Rectificaciones */

    /* Extensiones */

    function getRectificacionesExtensionByOC() {

        $id_orden_compra = $this->input->post('id_orden_compra');

        //  variables de sesion
        $arr_data = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_rectificaciones_extension_by_id_orden_compra($arr_data);
        echo json_encode($datos);
    }

    function getRectificacionesExtensionDocByOC() {

        $id_orden_compra = $this->input->post('id_orden_compra');
        //  variables de sesion        
        $arr_data = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_rectificaciones_extension_doc_by_id_orden_compra($arr_data);
        echo json_encode($datos);
    }

    /* End Extensiones */



    /* Factura */

    function getFacturaByOC() {

        $id_orden_compra = $this->input->post('id_orden_compra');
        //  variables de sesion        
        $arr_data = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_getFacturaByOC($arr_data);
        echo json_encode($datos);
    }

    function setEstadoFactura() {


        $id_factura = $this->input->post('id_factura');
        $estado_factura = $this->input->post('estado_factura');
        //  variables de sesion        

        $arr_data = array(
            'id_factura' => $id_factura,
            'estado_factura' => $estado_factura,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_arr_data_estado_factura($arr_data);
        echo json_encode($datos);
    }

    /* end Factura */
}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
