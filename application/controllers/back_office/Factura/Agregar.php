<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Factura';
    private $perfil = 'back_office/Perfil/Perfil_model';
    private $model = 'Factura_model';
    private $view = 'Agregar_v';
    private $controller = 'Agregar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->perfil, 'modelperfil');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index($id_orden_compra = false) {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Facturas',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/agregar.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Pendientes";

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $arr_input_data = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_factura'] = $this->modelo->get_arr_factura_by_oc($arr_input_data);
//         var_dump($arr_input_data);
//         die();


        $this->load->view('amelia_1/template', $data);
    }

    function set_fecha($fecha) {
        $fecha_explotada = explode("-", $fecha);
        $fecha_emision = $fecha_explotada[2] . "-" . $fecha_explotada[1] . "-" . $fecha_explotada[0];

        return $fecha_emision;
    }

    function AgregarFactura() {

        $monto_factura = $this->input->post('monto_factura');



        $arr_input_data = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'tipo_factura' => $this->input->post('tipo_factura'),
            'n_factura' => $this->input->post('n_factura'),
            'fecha_emision' => $this->set_fecha($this->input->post('fecha_emision')),
            'fecha_vencimiento' => $this->set_fecha($this->input->post('fecha_vencimiento')),
            'monto_factura' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $monto_factura))))
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_agregar_factura($arr_input_data);



        echo json_encode($respuesta);
    }

    /*  function AgregarFactura() {

      $name = $_FILES['file']['name'];
      $tname = $_FILES['file']['tmp_name'];


      $nombre_archivo = "";
      $estado = "";
      $mensaje = "";
      $respuesta = "";
      // verifica si el archivo se puede subir
      if (is_uploaded_file($tname)) {

      $splitted = explode(".", $name);
      $reversed = array_reverse($splitted);
      $extension = $reversed[0];

      //  borra posicion de un array
      unset($reversed[0]);

      $dereched = array_reverse($reversed);
      // une string separado
      $nombre_archivo = implode("", $dereched);
      $nombre_con_fecha = $nombre_archivo . '_' . date('dmYHis') . '.' . $extension;

      $dir_subida = 'facturadoc/';
      $fichero_subido = $dir_subida . basename($nombre_con_fecha);
      //copy =  copia el archivo de la ruta temporal a la ruta del servidor
      if (copy($tname, $fichero_subido)) {

      $monto_factura = $this->input->post('monto_factura');

      $arr_input_data = array(
      'id_orden_compra' => $this->input->post('id_orden_compra'),
      'tipo_factura' => $this->input->post('tipo_factura'),
      'n_factura' => $this->input->post('n_factura'),
      'estado_factura' => $this->input->post('estado_factura'),
      'fecha_emision' => $this->set_fecha($this->input->post('fecha_emision')),
      'fecha_vencimiento' => $this->set_fecha($this->input->post('fecha_vencimiento')),
      'monto_factura' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $monto_factura)))),
      'nombre_archivo' => $nombre_con_fecha,
      'tipo_entrega' => $this->input->post('tipo_entrega'),
      'ruta_archivo' => $dir_subida
      );

      $arr_input_data['user_id'] = $this->session->userdata('id_user');
      $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

      $respuesta = $this->modelo->set_arr_agregar_factura($arr_input_data);

      $nombre_archivo = $nombre_con_fecha;
      $estado = "ok";
      $mensaje = "Archivo Subido";
      } else {
      $nombre_archivo = $nombre_con_fecha;
      $estado = "Error";
      $mensaje = "Error al Subir Archivo" . $nombre_archivo;
      }
      } else {
      $nombre_archivo = $name;
      $estado = "Error";
      $mensaje = "Error el archivo que intenta subir no es compatible con el sistema" . $nombre_archivo;
      }
      //        $arr_datos = array('archivo' => $nombre_archivo, 'status' => $estado, 'mensaje' => $mensaje );
      echo json_encode($respuesta);
      } */

    function getTipoFacturaCBX() {


        $arr_input_data = array(
            'id_orden_compra' => trim($this->input->post('id_orden_compra')),
        );

        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_tipo_facturacion_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function getEstadoFacturaCBX() {

        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_estado_facturacion_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function getPerfiles() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelperfil->get_arr_listar_perfil_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function tipo_entrega_cbx() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_entregado_cbx($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
