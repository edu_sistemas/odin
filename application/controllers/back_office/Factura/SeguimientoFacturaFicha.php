<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-07 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-07 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class SeguimientoFacturaFicha extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Factura';
    private $nombre_item_plural = 'Facturas';
    private $package = 'back_office/Factura';
    private $model = 'Factura_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $view = 'SeguimientoFacturaFicha_v';
    private $controller = 'SeguimientoFactura';
    private $ind = '';

    function __construct() {
        parent::__construct();

        // Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->holding, 'modelholding');
        // Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Seguimiento Factura informe Ficha',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/SeguimientoFacturaFicha.js')
        );

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Seguimiento Factura informe Ficha";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data ['menus'] = $this->session->userdata('menu_usuario');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listar() {
        $empresa = $this->input->post('empresa');
        $holding = $this->input->post('holding');

        $ejecutivo = $this->input->post('ejecutivo');
        $num_ficha = $this->input->post('num_ficha');

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'empresa' => $empresa,
            'holding' => $holding,
            'ejecutivo' => $ejecutivo,
            'num_ficha' => $num_ficha,
            'user_id' => $datos_menu ['user_id'],
            'user_perfil' => $datos_menu ['user_perfil']
        );

        $datos = $this->modelo->get_arr_listar_fichas_facturas_informe_by_ficha($arr_sesion);
        echo json_encode($datos);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaCBX() {
        // carga el combo box Holding

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getUsuarioTipoEcutivoComercial() {
        // carga el combo box CodigoSence
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        echo json_encode($datos);
    }

}
