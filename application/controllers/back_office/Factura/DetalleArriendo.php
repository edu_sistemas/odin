<?php

/** EditarFactura_v
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2017-07-03 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class DetalleArriendo extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Factura';
    private $model = 'Factura_model';
    private $view = 'DetalleArriendo_v';
    private $controller = 'Editar';
    private $ind = '';
    private $modalidad = 'back_office/Modalidad/Modalidad_model';
    private $curso = 'back_office/Curso/Curso_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $otic = 'back_office/Otic/Otic_model';
    private $sence = 'back_office/Code_sence/Code_sence_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $sede = 'back_office/Sede/Sede_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $sala = 'back_office/Sala/Sala_model';
    private $ficha = 'back_office/Ficha/Ficha_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->ficha, 'modelficha');
        $this->load->model($this->modalidad, 'modelmodalidad');
        $this->load->model($this->curso, 'modelcurso');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->otic, 'modelotic');
        $this->load->model($this->sence, 'modelosence');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->sede, 'modelosede');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->sala, 'modelsala');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del resumen arriendo 
    public function index($id_orden_compra = false, $opcion_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_orden_compra == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Resumen Arriendo',
                'label' => 'Resumen Arriendo',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/DetalleArriendo.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Pendientes";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }


        $arr_input_data = array(
            'id_orden_compra' => $id_orden_compra,
            'opcion_ficha' => $opcion_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id_orden_compra_resumen($arr_input_data);

        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_id_orden_compra($arr_input_data);


        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function cargaDatosOC() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelficha->get_arr_listar_datos_orden_compra($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumen() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelficha->get_arr_listar_datos_documentos_resumen_orden_compra($arr_data);
        echo json_encode($datos);
    }

    /* Factura */

    function getFacturaByOC() {

        $id_orden_compra = $this->input->post('id_orden_compra');
        //  variables de sesion        
        $arr_data = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_getFacturaByOC($arr_data);
        echo json_encode($datos);
    }

    function setEstadoFactura() {


        $id_factura = $this->input->post('id_factura');
        $estado_factura = $this->input->post('estado_factura');
        //  variables de sesion        

        $arr_data = array(
            'id_factura' => $id_factura,
            'estado_factura' => $estado_factura,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_arr_data_estado_factura($arr_data);
        echo json_encode($datos);
    }

    /* end Factura */
}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
