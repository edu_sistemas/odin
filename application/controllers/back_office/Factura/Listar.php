<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-07 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-07 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Factura';
    private $nombre_item_plural = 'Facturas';
    private $package = 'back_office/Factura';
    private $model = 'Factura_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';

    function __construct() {
        parent::__construct();

        // Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->holding, 'modelholding');
        // Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/Listar.js')
        );

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Pendientes";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data ['menus'] = $this->session->userdata('menu_usuario');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listar() {
        $empresa = $this->input->post('empresa');
        $holding = $this->input->post('holding');

        $ejecutivo = $this->input->post('ejecutivo');
        $num_ficha = $this->input->post('num_ficha');

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'empresa' => $empresa,
            'holding' => $holding,
            'ejecutivo' => $ejecutivo,
            'num_ficha' => $num_ficha,
            'user_id' => $datos_menu ['user_id'],
            'user_perfil' => $datos_menu ['user_perfil']
        );

        $datos = $this->modelo->get_arr_listar_fichas($arr_sesion);
        echo json_encode($datos);
    }

    function CambiarEstadoFactura() {
        $id_usuario = $this->input->post('id');
        $estado = $this->input->post('estado');

        $arr_datos = array(
            'id_usuario' => $id_usuario,
            'estado' => $estado,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_sp_usuario_change_status($arr_datos);

        echo json_encode($data);
    }

    function getUsuarioTipoEcutivoComercial() {
        // carga el combo box CodigoSence
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        echo json_encode($datos);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaCBX() {
        // carga el combo box Holding

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getNotasbyOrdenCompra() {
        $arr_data = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_orden_compra_select_notas($arr_data);
        echo json_encode($datos);
    }

    function getNotasDebitobyOrdenCompra() {
        $arr_data = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_orden_compra_select_notas_debito($arr_data);
        echo json_encode($datos);
    }

    function setNotaCredito() {



        $fecha_explotada = explode("-", $this->input->post('fecha_emision'));
        $fecha_emision = $fecha_explotada[2] . "-" . $fecha_explotada[1] . "-" . $fecha_explotada[0];

        $arr_input_data = array(
            'n_factura_cbx' => $this->input->post('n_factura_cbx'),
            'num_nota_credito' => $this->input->post('num_nota_credito'),
            'fecha_emision' => $fecha_emision,
            'monto_credito' => $this->input->post('monto_credito'),
            'tipo_entrega' => $this->input->post('tipo_entrega_nc')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_sp_nota_credito_insert($arr_input_data);


        //        $arr_datos = array('archivo' => $nombre_archivo, 'status' => $estado, 'mensaje' => $mensaje );
        echo json_encode($respuesta);
    }

    function setNotaDebito() {



        $fecha_explotada = explode("-", $this->input->post('fecha_emision_debito'));
        $fecha_emision = $fecha_explotada[2] . "-" . $fecha_explotada[1] . "-" . $fecha_explotada[0];

        $arr_input_data = array(
            'n_factura_cbx' => $this->input->post('n_factura_cbx_debito'),
            'num_nota_debito' => $this->input->post('num_nota_debito'),
            'fecha_emision' => $fecha_emision,
            'monto_debito' => $this->input->post('monto_debito'),
            'tipo_entrega' => $this->input->post('tipo_entrega_nd')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_sp_nota_debito_insert($arr_input_data);



        echo json_encode($respuesta);
    }

    function tipo_entrega() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_entregado_cbx($arr_data);
        echo json_encode($datos);
    }

    function updateTipoEntrega() {
        $id_factura = $this->input->post('id_fact');
        $id_tipo_entrega = $this->input->post('id_tipo_entrega');
        $arr_data = array(
            'id_factura' => $id_factura,
            'id_tipo' => $id_tipo_entrega,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_tipo_entrega_fact($arr_data);
        echo json_encode($datos);
    }

    function updateTipoEntrega_nc() {
        $id_nc = $this->input->post('id_nc');
        $id_tipo_entrega = $this->input->post('id_tipo_entrega');
        $arr_data = array(
            'id_nc' => $id_nc,
            'id_tipo' => $id_tipo_entrega,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_tipo_entrega_nc($arr_data);
        echo json_encode($datos);
    }

    function updateTipoEntrega_nd() {
        $id_nd = $this->input->post('id_nd');
        $id_tipo_entrega = $this->input->post('id_tipo_entrega');
        $arr_data = array(
            'id_nd' => $id_nd,
            'id_tipo' => $id_tipo_entrega,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_tipo_entrega_nd($arr_data);
        echo json_encode($datos);
    }

    function getDocumentosFicha() {
        $arr_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_ficha_select_docs($arr_data);
        echo json_encode($datos);
    }

    function getDocumentosRectificacion() {
        $arr_data = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_rectificacion_select_docs_by_oc($arr_data);
        echo json_encode($datos);
    }

    function getFacturasByOC() {
        $id_orden_compra = $this->input->post('id_orden_compra');

        $arr_datos = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_factura_by_oc($arr_datos);

        echo json_encode($data);
    }

    function getFacturasbyOcCBX() {
        // carga el combo box facturas por id orden de compra
        $arr_sesion = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_sp_factura_select_by_oc_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEstadoFacturaCBX() {

        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_estado_facturacion_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function updateEstadoFactura() { // este es para cambiar estados como pagado, Pendiente de pago, Anulado
        $id_factura = $this->input->post('id_factura');
        $id_estado_nuevo = $this->input->post('id_estado_nuevo');

        $arr_datos = array(
            'id_factura' => $id_factura,
            'id_estado_factura' => $id_estado_nuevo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_sp_factura_update_estado($arr_datos);

        echo json_encode($data);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
