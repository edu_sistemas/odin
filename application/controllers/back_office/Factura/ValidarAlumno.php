<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-07-20 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-07-20 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class ValidarAlumno extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Factura';
    private $nombre_item_plural = 'Facturas';
    private $package = 'back_office/Factura';
    private $model = 'Factura_model';
    private $view = 'ValidarAlumno_v';
    private $controller = 'ValidarAlumno';
    private $ind = '';

    function __construct() {
        parent::__construct();

        // Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        // Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index($id_ficha_seleccionada = false) {

        $ficha_seleccionada = 0;

        if ($id_ficha_seleccionada) {
            $ficha_seleccionada = $id_ficha_seleccionada;
        } else {
            $ficha_seleccionada = $this->input->post('id_ficha');
        }

        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Validar Alumno',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/ValidarAlumno.js')
        );

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Validar Alumnos";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }



        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['fichas_validar'] = $this->modelo->Validar_get_fichas_validar($arr_data);

        $data['ficha_seleccionada'] = $ficha_seleccionada;


        $data ['menus'] = $this->session->userdata('menu_usuario');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getFichasValidar() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->Validar_get_fichas_validar($arr_data);
        echo json_encode($datos);
    }

    function getPreviewFicha() {

        $arr_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->Validar_get_sp_ficha_select_preview_by_id($arr_data);
        echo json_encode($datos);
    }

    function getDataValidarFicha() {

        $arr_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_validar_alumnos($arr_data);
        echo json_encode($datos);
    }

    function getDatosAlumnosyDatosFicha() {



        $arr_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'ids_participantes' => $this->input->post('ids_participantes'),
            'observaciones' => $this->input->post('observaciones'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        // $data_result = array();

        $data_ficha = $this->modelo->get_arr_listar_ficha_correo($arr_data);

        // $datos_participantes = $this->modelo->get_arr_listar_participantes_ficha_correo($arr_data);



        echo json_encode($data_ficha);
    }

    function enviaEmail() {

        $datos = $this->input->post('data_ficha');
        $data_alumnos = $this->input->post('data_alumnos');
        $comentarios = $this->input->post('comentarios');

        $destinatarios = array();

        $asunto = "";
        $body = "";

        $num_ficha = $datos[0]['num_ficha'];

        $ejecutivo_nombre = $datos[0]['nombre_ejecutivo'];
        $email_ejecutivo = $datos[0]['email_ejecutivo'];
        $usuario_ingreso_nombre = $datos[0]['nombre_usuario_ingreso'];
        $usuario_ingreso_correo = $datos[0]['email_usuario_ingreso'];

        $asunto = "Ficha N°: " . $num_ficha; // asunto del correo         

        if ($email_ejecutivo == $usuario_ingreso_correo) {
            $destinatarios = array(
                array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                array('email' => 'mmoreno@edutecno.com', 'nombre' => 'Miguel Moreno')
            );
        } else {

            $destinatarios = array(
                array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                array('email' => 'mmoreno@edutecno.com', 'nombre' => 'Miguel Moreno')
            );
        }


        $body = $this->generaHtml($datos[0], $data_alumnos, $comentarios); // mensaje completo HTML o text


        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa'),
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );


        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);


        echo $body;
    }

    function generaHtml($data, $alumnos, $comentarios) {

        $html = '';
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>    Se necesita : </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)   ';
        $html .= '<br>';
        $html .= 'Se necesita :  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr >';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['curso'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['nombre_modalidad'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>  ';


        for ($i = 0; $i < count($alumnos); $i++) {

            $html .= '<tr  >';

            $html .= '<tr class="total" > ';
            $html .= '<td style="width: 100%;" >RUT</td> ';
            $html .= '<td class="alignright">' . $alumnos[$i]['rut'] . '</td>';
            $html .= '</tr>    ';

            $html .= '<tr>    ';
            $html .= '<td style="width: 100%;" >NOMBRE</td> ';
            $html .= '<td class="alignright">' . $alumnos[$i]['nombre'] . '</td>';
            $html .= '</tr> ';

            $html .= '<tr>    ';
            $html .= '<td style="width: 100%;" >AST. SENCE</td> ';
            $html .= '<td class="alignright">' . $alumnos[$i]['sence'] . '</td>';
            $html .= '</tr> ';

            $html .= '<tr>    ';
            $html .= '<td style="width: 100%;" >AST. INTERNA</td> ';
            $html .= '<td class="alignright">' . $alumnos[$i]['interna'] . '</td>';
            $html .= '</tr> ';
            $html .= '<tr>    ';
            $html .= '<td style="width: 100%;" >DJ</td> ';
            $html .= '<td class="alignright">' . $alumnos[$i]['dj'] . '</td>';
            $html .= '</tr> ';


            $html .= '</tr> ';
        }



        $html .= '<tr  class="total">';
        $html .= '<td>Comentarios </td>';
        $html .= '<td class="alignright">' . $comentarios . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno - Huérfanos 863, piso 2 - Galería España ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function todoOk() {

        $comentarios = $this->input->post('comentarios');
        $id_ficha = $this->input->post('id_ficha');


        $arr_data = array(
            'id_ficha' => $id_ficha,
            'observaciones' => $comentarios,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        $data_ficha = $this->modelo->get_arr_validacion_todo_ok($arr_data);



        echo json_encode($data_ficha);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
