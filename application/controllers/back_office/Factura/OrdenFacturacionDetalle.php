<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-07 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-07 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class OrdenFacturacionDetalle extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Factura';
    private $nombre_item_plural = 'Facturas';
    private $package = 'back_office/Factura';
    private $model = 'OrdenFactura_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $view = 'OrdenFacturacionDetalle_v';
    private $controller = 'OrdenFacturacionDetalle';
    private $ind = '';
    private $modelficha = 'back_office/Ficha_Control/Ficha_Control_model';
    private $edutecno = 'back_office/Edutecno/Edutecno_model';
    private $ficha = 'back_office/Ficha/Ficha_model';
    private $factura = 'back_office/Factura/Factura_model';

    function __construct() {
        parent::__construct();

        // Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->modelficha, 'modeloficha');
        $this->load->model($this->edutecno, 'modeledutecno');
        $this->load->model($this->ficha, 'modelfichaficha');
        $this->load->model($this->factura, 'modelfactura');

        // Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index($id_orden_compra = false) {


        if ($id_orden_compra == false) {
            redirect($this->package . '/OrdenFacturacion', 'refresh');
        }
        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/OrdenFacturacion',
                'label' => 'Orden de Facturación',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/OrdenFacturacion/index/' . $id_orden_compra,
                'label' => 'Detalle',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/OrdenFacturacionDetalle.js')
        );

        /* $arr_input_data = array(
          'id_ficha' => $id_ficha,
          'user_id' => $this->session->userdata('id_user'),
          'user_perfil' => $this->session->userdata('id_perfil')
          );
          $data['data_ficha'] = $this->modeloficha->get_arr_ficha_by_id_resumenFC($arr_input_data);
          $data['data_orden_compra'] = $this->modeloficha->get_arr_orden_compra_by_ficha_idFC($arr_input_data);
         */
        $arr_input_data = array(
            'id_orden_compra' => $id_orden_compra,
            'opcion_ficha' => 0,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );



        $data['data_ficha'] = $this->modelfactura->get_arr_ficha_by_id_orden_compra_resumen($arr_input_data);


        $data['data_orden_compra'] = $this->modelfactura->get_arr_orden_compra_by_id_orden_compra($arr_input_data);


        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Orden Factura";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data ['menus'] = $this->session->userdata('menu_usuario');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getidEdutecnoCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeledutecno->get_arr_edutecno_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getDatosFacturacionOC() {
        // carga el combo box Holding
        $orden_compra = $this->input->post('cbx_orden_compra');
        $arr_sesion = array(
            'id_orden_compra' => $orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_datos_facturaOCaaaaa($arr_sesion);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumen() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelfichaficha->get_arr_listar_datos_documentos_resumen_orden_compra($arr_data);
        echo json_encode($datos);
    }

    function setTodoOKFicha() {

        $id_ficha = $this->input->post('ficha');
        $id_oc = $this->input->post('oc');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'id_oc' => $id_oc,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_arr_data_todo_ok_ficha_factura($arr_data);
        echo json_encode($datos);
    }

    function setTodoProblemasFicha() {

        $id_ficha = $this->input->post('ficha');
        $id_oc = $this->input->post('oc');

        $observacion = $this->input->post('observacion');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'id_oc' => $id_oc,
            'observacion' => $observacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_arr_data_problemas_ficha_factura($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosObservacionesFacturar() {

        $id_ficha = $this->input->post('ficha');
        $id_oc = $this->input->post('oc');


        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'id_oc' => trim($id_oc),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_data_facturar_ficha_observaciones($arr_data);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
