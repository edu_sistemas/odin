<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-07 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-07 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Realizadas extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Factura';
    private $nombre_item_plural = 'Facturas';
    private $package = 'back_office/Factura';
    private $model = 'Factura_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $view = 'Realizadas_v';
    private $controller = 'Realizadas';
    private $ind = '';

    function __construct() {
        parent::__construct();

        // Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->holding, 'modelholding');
        // Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/Realizadas.js')
        );

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Historial";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data ['menus'] = $this->session->userdata('menu_usuario');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listar() {
        $empresa = $this->input->post('empresa');
        $holding = $this->input->post('holding');

        $ejecutivo = $this->input->post('ejecutivo');
        $num_ficha = $this->input->post('num_ficha');

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'empresa' => $empresa,
            'holding' => $holding,
            'ejecutivo' => $ejecutivo,
            'num_ficha' => $num_ficha,
            'user_id' => $datos_menu ['user_id'],
            'user_perfil' => $datos_menu ['user_perfil']
        );

        $datos = $this->modelo->get_arr_listar_fichas_realizadas($arr_sesion);
        echo json_encode($datos);
    }

    function CambiarEstadoFactura() {
        $id_usuario = $this->input->post('id');
        $estado = $this->input->post('estado');

        $arr_datos = array(
            'id_usuario' => $id_usuario,
            'estado' => $estado,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_sp_usuario_change_status($arr_datos);

        echo json_encode($data);
    }

    function getUsuarioTipoEcutivoComercial() {
        // carga el combo box CodigoSence
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        echo json_encode($datos);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaCBX() {
        // carga el combo box Holding

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getNotasbyOrdenCompra() {
        $arr_data = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_orden_compra_select_notas($arr_data);
        echo json_encode($datos);
    }

    function getNotasDebitobyOrdenCompra() {
        $arr_data = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_orden_compra_select_notas_debito($arr_data);
        echo json_encode($datos);
    }

    function setNotaCredito() {



        $fecha_explotada = explode("-", $this->input->post('fecha_emision'));
        $fecha_emision = $fecha_explotada[2] . "-" . $fecha_explotada[1] . "-" . $fecha_explotada[0];

        $arr_input_data = array(
            'n_factura_cbx' => $this->input->post('n_factura_cbx'),
            'num_nota_credito' => $this->input->post('num_nota_credito'),
            'fecha_emision' => $fecha_emision,
            'monto_credito' => $this->input->post('monto_credito'),
            'tipo_entrega' => $this->input->post('tipo_entrega_nc')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_sp_nota_credito_insert($arr_input_data);


        //        $arr_datos = array('archivo' => $nombre_archivo, 'status' => $estado, 'mensaje' => $mensaje );
        echo json_encode($respuesta);
    }

    function setNotaDebito() {



        $fecha_explotada = explode("-", $this->input->post('fecha_emision_debito'));
        $fecha_emision = $fecha_explotada[2] . "-" . $fecha_explotada[1] . "-" . $fecha_explotada[0];

        $arr_input_data = array(
            'n_factura_cbx' => $this->input->post('n_factura_cbx_debito'),
            'num_nota_debito' => $this->input->post('num_nota_debito'),
            'fecha_emision' => $fecha_emision,
            'monto_debito' => $this->input->post('monto_debito'),
            'tipo_entrega' => $this->input->post('tipo_entrega_nd')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_sp_nota_debito_insert($arr_input_data);



        echo json_encode($respuesta);
    }

    function tipo_entrega() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_entregado_cbx($arr_data);
        echo json_encode($datos);
    }

    function updateTipoEntrega() {
        $id_factura = $this->input->post('id_fact');
        $id_tipo_entrega = $this->input->post('id_tipo_entrega');
        $arr_data = array(
            'id_factura' => $id_factura,
            'id_tipo' => $id_tipo_entrega,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_tipo_entrega_fact($arr_data);
        echo json_encode($datos);
    }

    function updateTipoEntrega_nc() {
        $id_nc = $this->input->post('id_nc');
        $id_tipo_entrega = $this->input->post('id_tipo_entrega');
        $arr_data = array(
            'id_nc' => $id_nc,
            'id_tipo' => $id_tipo_entrega,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_tipo_entrega_nc($arr_data);
        echo json_encode($datos);
    }

    function updateTipoEntrega_nd() {
        $id_nd = $this->input->post('id_nd');
        $id_tipo_entrega = $this->input->post('id_tipo_entrega');
        $arr_data = array(
            'id_nd' => $id_nd,
            'id_tipo' => $id_tipo_entrega,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_tipo_entrega_nd($arr_data);
        echo json_encode($datos);
    }

    function getDocumentosFicha() {
        $arr_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_ficha_select_docs($arr_data);
        echo json_encode($datos);
    }

    function getDocumentosRectificacion() {
        $arr_data = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_rectificacion_select_docs_by_oc($arr_data);
        echo json_encode($datos);
    }

    function getFacturasByOC() {
        $id_orden_compra = $this->input->post('id_orden_compra');

        $arr_datos = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_factura_by_oc($arr_datos);

        echo json_encode($data);
    }

    function getFacturasbyOcCBX() {
        // carga el combo box facturas por id orden de compra
        $arr_sesion = array(
            'id_orden_compra' => $this->input->post('id_orden_compra'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_sp_factura_select_by_oc_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEstadoFacturaCBX() {

        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_estado_facturacion_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function updateEstadoFactura() { // este es para cambiar estados como pagado, Pendiente de pago, Anulado
        $id_factura = $this->input->post('id_factura');
        $id_estado_nuevo = $this->input->post('id_estado_nuevo');

        $arr_datos = array(
            'id_factura' => $id_factura,
            'id_estado_factura' => $id_estado_nuevo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_sp_factura_update_estado($arr_datos);

        echo json_encode($data);
    }

    function enviarFacturaAFacturacion() {
        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->enviarFacturaAFacturacion($arr_datos);

        echo json_encode($data);
    }

    function enviarFacturaADespacho() {
        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->enviarFacturaADespacho($arr_datos);

        echo json_encode($data);
    }

    function enviarSolicitudDeCorrecion() {


        $arr_datos = array(
            'id_ficha' => $this->input->post('sdc_id_ficha'),
            'num_ficha' => $this->input->post('sdc_num_ficha'),
            'comentario' => $this->input->post('sdc_comentario'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        $data = $this->modelo->enviarSolicitudDeCorrecionF($arr_datos);

        echo json_encode($data);
    }

    function EnviarMailCorreccion() {

        $datos = $this->input->post('datos');


        $destinatarios = array();

        $asunto = "";
        $body = "";

        $num_ficha = $datos[0]['num_ficha'];

        $ejecutivo_nombre = $datos[0]['ejecutivo'];
        $email_ejecutivo = $datos[0]['email_ejecutivo'];
        $usuario_ingreso_nombre = $datos[0]['usuario_ingreso_nombre'];
        $usuario_ingreso_correo = $datos[0]['usuario_ingreso_correo'];

        if ($email_ejecutivo == $usuario_ingreso_correo) {
            $destinatarios = array(
                array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre)
            );
        } else {

            $destinatarios = array(
                array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre)
            );
        }



        $asunto = "Solicitud de Corrección- Ficha N°: " . $num_ficha; // asunto del correo


        $body = $this->generaHtml($datos);

        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );


        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);


        echo json_encode($body);
    }

    function generaHtml($datos) {

        $html = "";

        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_rojo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)    ';
        $html .= '<br>';
        $html .= 'Se ha Solicitado corrección para la Ficha N°  ' . $datos[0]['num_ficha'];
        $html .= '<br/> ';
        $html .= 'Observaciones: ' . $datos[0]['comentarios'];
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-danger">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfanos 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
