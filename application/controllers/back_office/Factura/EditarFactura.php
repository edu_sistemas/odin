<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class EditarFactura extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Factura';
    private $perfil = 'back_office/Perfil/Perfil_model';
    private $model = 'Factura_model';
    private $view = 'EditarFactura_v';
    private $controller = 'Agregar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->perfil, 'modelperfil');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index($id_factura = false) {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/DocumentosTributarios',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Factura/EditarFactura.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Documentos Tributarios";

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $arr_input_data = array(
            'id_factura' => $id_factura,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_factura'] = $this->modelo->get_arr_factura_by_id_factura($arr_input_data);
//         var_dump($arr_input_data);
//         die();


        $this->load->view('amelia_1/template', $data);
    }

    function set_fecha($fecha) {
        $fecha_explotada = explode("-", $fecha);
        $fecha_emision = $fecha_explotada[2] . "-" . $fecha_explotada[1] . "-" . $fecha_explotada[0];

        return $fecha_emision;
    }

    function UPDATEFactura() {

        $monto_factura = $this->input->post('monto_factura');
 
        $arr_input_data = array(
            'id_factura' => $this->input->post('id_factura'),
            'tipo_factura' => $this->input->post('tipo_factura'),
            'n_factura' => $this->input->post('n_factura'),
            'fecha_emision' => $this->set_fecha($this->input->post('fecha_emision')),
            'fecha_vencimiento' => $this->set_fecha($this->input->post('fecha_vencimiento')),
            'monto_factura' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $monto_factura))))
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_ubdate_factura($arr_input_data);



        echo json_encode($respuesta);
    }
 
    function getTipoFacturaCBX() {


        $arr_input_data = array(
            'id_orden_compra' => trim($this->input->post('id_orden_compra')),
        );

        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_tipo_facturacion_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function getEstadoFacturaCBX() {

        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_estado_facturacion_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function getPerfiles() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelperfil->get_arr_listar_perfil_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function tipo_entrega_cbx() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_entregado_cbx($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
