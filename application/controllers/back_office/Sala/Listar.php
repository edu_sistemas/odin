<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-11 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-12-11 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Sala';
    private $model = 'Sala_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';
    private $package2 = 'back_office/Usuario';
    private $model2 = 'Usuario_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'modelousuario');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );


        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Sala/Listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Salas";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['menus'] = $this->session->userdata('menu_usuario');

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listar() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );
        $datos = $this->modelo->get_arr_listar_Sala($arr_sesion);
        echo json_encode($datos);
    }

    function CambiarEstadoSala() {

        $id_sala = $this->input->post('id');
        $estado = $this->input->post('estado');

        $arr_datos = array(
            'id_sala' => $id_sala,
            'estado' => $estado,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_sp_sala_change_status($arr_datos);

        echo json_encode($data);
    }

    function desactivarRegistroPrograma() {

        $id_programa_sala = $this->input->post('id_programa_sala');
        $arr_datos = array(
            'id_programa_sala' => $id_programa_sala,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_sp_programa_sala_desactivar($arr_datos);

        echo json_encode($data);
    }

    function getProgramasbySala() {

        $id_sala = $this->input->post('id_sala');

        $arr_datos = array(
            'id_sala' => $id_sala,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_sp_sala_select_programas_by_id($arr_datos);

        echo json_encode($data);
    }
    
    function AgregarPrograma() {
    	
    	$nombre_programa= $this->input->post('txt_nombre_programa');
    	$arr_datos = array(
    			'nombre_programa' => $nombre_programa,
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	
    	$data = $this->modelo->set_sp_programa_agregar($arr_datos);
    	
    	echo json_encode($data);
    }
    
    function AgregarProgramaSala() {

        $buscar=array(chr(13).chr(10), "\r\n", "\n", "\r");
        $reemplazar=array(" ", " ", " ", " ");
        $observacion=str_ireplace($buscar,$reemplazar,trim($this->input->post('nuevo_comentario')));
    	
    	$id_programa = $this->input->post('programas_cbx');
    	$id_sala = $this->input->post('hidden_id_sala');
    	$arr_datos = array(
    			'id_programa' => $id_programa,
    			'id_sala' => $id_sala,
    			'observacion' => $observacion,
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	
    	$data = $this->modelo->set_sp_programa_sala_agregar($arr_datos);
    	
    	echo json_encode($data);
    }
    
    function UpdateProgramaSala() {
    	
    	$id_programa = $this->input->post('programas_cbx_editar');
    	$id_programa_sala = $this->input->post('hidden_id_programa_sala_editar');
    	$observacion = $this->input->post('editar_comentario');
    	$arr_datos = array(
    			'id_programa' => $id_programa,
    			'id_programa_sala' => $id_programa_sala,
    			'observacion' => $observacion,
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	
    	$data = $this->modelo->set_sp_programa_sala_update($arr_datos);
    	
    	echo json_encode($data);
    }
    
    
    function getProgramasCBX() {
    	
    	$arr_datos = array(
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	
    	$data = $this->modelo->get_sp_programa_select_cbx($arr_datos);
    	
    	echo json_encode($data);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
