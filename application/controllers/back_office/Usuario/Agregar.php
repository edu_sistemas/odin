<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Usuario';
    private $perfil = 'back_office/Perfil/Perfil_model';
    private $model = 'Usuario_model';
    private $view = 'Agregar_v';
    private $controller = 'Agregar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->perfil, 'modelperfil');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
       
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
               array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Usuario/agregar.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Usuarios";
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;



        $this->load->view('amelia_1/template', $data);
    }

    function Registra_usuario() {

        $arr_input_data = array(
            'usuario' => $this->input->post('usuario'),
            'correo' => $this->input->post('correo'),
            'anexo_edutecno' => $this->input->post('anexo_edutecno'),
            'nombre_usuario' => $this->input->post('nombre_usuario'),
            'apellidos_usuario' => $this->input->post('apellidos_usuario'),
            'rut_usuario' => $this->input->post('rut_usuario'),
            'dv_usuario' => $this->input->post('dv_usuario'),
            'perfil' => $this->input->post('perfil'),
            'departamento' => $this->input->post('departamento'),
            'imagen' => $this->input->post('imagen')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_agregar_usuario($arr_input_data);
        //enviar mail

        $asunto = "Datos de Acceso Odín"; // asunto del correo 

        $data_correo = array(
            'nombre' => $this->input->post('nombre_usuario'),
            'nombre_usuario' => $this->input->post('rut_usuario') . '-' . $this->input->post('dv_usuario'),
            'contraseña' => $this->input->post('rut_usuario'),
            'mail' => $this->input->post('correo')
        );

        $body = $this->generaHtmlBienvenida($data_correo); // mensaje completo HTML o text 


        $AltBody = $asunto; // hover del cursor sobre el correo
        // destinos  
        $destinatarios = array(
            array('email' => $this->input->post('correo'), 'nombre' => $this->input->post('nombre_usuario'))
        );

        // con copia a 
        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
        );
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);


        echo json_encode($respuesta);
    }

    function getPerfiles() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelperfil->get_arr_listar_perfil_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getDepartamentoCBX(){
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->get_arr_listar_departamento_cbx($arr_input_data);

        echo json_encode($respuesta);
    }

    
    function generaHtmlBienvenida($data) {

        $html = "";

        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Bienvenido a Odin  </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap">';
        $html .= '<table  cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<h3>¡Bienvenido a Odín!</h3>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado(a) ' . ucwords($data['nombre']) . ', Muchas gracias por unirte a nuestra comunidad. Los datos de tu cuenta son los siguientes:';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<b>NOMBRE DE USUARIO:</b>  ' . $data['nombre_usuario'] . '';
        $html .= '<br />';
        $html .= '<b>CONTRASEÑA:</b>    ' . $data['contraseña'] . '';
        $html .= '<br />';
        $html .= 'Email:  ' . $data['mail'] . '';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Recuerde no compartir esta información. ';
        $html .= 'Si tiene algún problema al iniciar sesión,';
        $html .= 'puede comunicarse con el departamento de sistemas de Edutecno.';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block aligncenter">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= '';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '';
        $html .= '</html>';

        return $html;
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
