<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-11-08 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Otorgar_Rol extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Otorgar_Rol';
    private $nombre_item_plural = 'Otorgar_Rol';
    private $package = 'back_office/Usuario';
    private $package2 = 'back_office/Perfil';
    private $model = 'Usuario_model';
    private $model2 = 'Perfil_model';
    private $view = 'Otorgar_Rol_v';
    private $controller = 'Otorgar_Rol';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'modeloperfil');

        //  Libreria de sesion
        $this->load->library('session');
        //  $this->load->library('form_validation');
        //  $this->load->library('AddForms_lib', 'addforms_lib');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id = false) {

        //  si entra a la mala lo redirige al listar
        if ($id == false) {
            redirect($this->$package . '/UsuarioLista', 'refresh');
        }


        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/UsuarioLista',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Otorgar_Rol',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Usuario/otorgar_permiso.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Usuarios";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $arr_input_data = array(
            'id_usuario' => $id,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        $data['contenido'] = $this->modeloperfil->get_arr_listar_perfil_cbx($arr_input_data);



        $data['data_usuario'] = $this->modelo->get_arr_usuario_by_id($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function OtorgarRol() {

        /*
          Ultima fecha Modificacion: 2016-11-09
          Descripcion:  Metodo para dar permisos a los perfiles
          Usuario Actualizador : Marcelo Romero
         */


        $arr_input_data = array(
            'id' => $this->input->post('id'),
            'fk_id_perfil' => $this->input->post('perfil')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');


        $respuesta = $this->modeloperfil->set_arr_perfil_rol($arr_input_data);


        echo json_encode($respuesta);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
