<?php

/**
 * Editar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Muro extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Muro';
    private $nombre_item_plural = 'Muros';
    private $package = 'back_office/Usuario';
    private $model = 'Usuario_model';
    private $view = 'Muro_v';
    private $controller = 'Muro';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {


        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind ."back_office" . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Muro',
                'label' => 'Muro',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Usuario/Muro.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;
        $data['activo'] = "";

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');



        $arr_input_data = array(
            'id_usuario' => $this->session->userdata('id_user'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_usuario'] = $this->modelo->get_arr_usuario_muro_by_id($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function EditarUsuario() {

        $arr_input_data = array(
            'id' => $this->input->post('id'),
            'usuario' => trim($this->input->post('usuario')),
            'nombre_usuario' => trim($this->input->post('nombre_usuario')),
            'apellidos_usuario' => trim($this->input->post('apellidos_usuario')),
            'rut_usuario' => trim($this->input->post('rut_usuario')),
            'dv_usuario' => trim($this->input->post('dv_usuario')),
            'perfil' => $this->input->post('perfil'),
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_editar_usuario($arr_input_data);

        echo json_encode($respuesta);
    }

    function getPerfiles() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelperfil->get_arr_listar_perfil_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getImgParameters() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_imagenes_usr($arr_sesion);
        echo json_encode($datos);
    }

    function setImgParameters() {


        $data_image = array();

        $data_image = $this->input->post('imagen');



        foreach ($data_image as $clave => $valor) {
            $datosss = array(
                'id_img' => $valor['id_img'],
                'imagen' => $valor['imagen'],
                'seleccionada' => $valor['seleccionada'],
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $datos = $this->modelo->set_arr_imagenes_usr($datosss);

            if ($valor['seleccionada'] == "1") {
                $this->session->set_userdata('img_user', $valor['imagen']);
            }
        }

        echo json_encode($datos);
    }

    function cambiarPassword() {

        $arr_input_data = array(
            'pass_actual' => $this->input->post('pass_actual'),
            'pass_nueva' => $this->input->post('pass_nueva')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_cambiar_password($arr_input_data);

        echo json_encode($respuesta);
    }

}

/* End of file Editar.php */
/* Location: ./application/controllers/Editar.php */
?>
