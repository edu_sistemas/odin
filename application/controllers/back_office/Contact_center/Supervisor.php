<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supervisor extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = '';
    private $package = 'back_office/Contact_center';
    private $model = 'Supervisor_model';
    private $model3 = 'Comun_model';
    // informacion adicional para notificaciones  
    private $model2 = '../Crm/Notificaciones_model';
    private $controller = 'Supervisor';
    private $ind = '';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        // informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');

        $this->load->model($this->package . '/' . $this->model3, 'modelo3');
        $this->load->library('session');
    }

    public function Index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Principal',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/modules/sunburst.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Dashboard_supervisor.js'),
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Dashboard_supervisor_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    // informacion adicional para notificaciones    

    public function getNotificaciones() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

    // CARGA GRAFICOS DASHBOARD ----------------


    public function getDatosTutoresDashboard() {
//        $res = $this->modelo->get_tutores_dashboard();
//        
//        echo json_encode($res);
    }

    public function Detalle_indicadores() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Principal',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Detalle_indicadores.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Detalle_indicadores_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Asignacion_tutor() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Asignación Tutor',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Asignar_tutor.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Asignación Tutor";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Asignar_tutor_V.php';
        $data['modalidades'] = $this->modelo->get_data_cbx(array("filtro" => 5, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['costos'] = $this->modelo->get_data_cbx(array("filtro" => 6, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['fichas'] = $this->modelo->get_data_cbx(array("filtro" => 7, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['oc'] = $this->modelo->get_data_cbx(array("filtro" => 4, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->session->set_userdata("ASG_Filtro", NULL);
        $this->load->view('amelia_1/template', $data);
    }

    public function Reasignar_alumnos($num_ficha = 0) {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Asignación Tutor',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/select.dataTables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/buttons.dataTables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Reasignar_alumnos.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/dataTables.select.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/dataTables.buttons.min.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Asignación Tutor";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Reasignar_alumnos_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        if($num_ficha == 0){
            $data["alumnos"] = $this->modelo->get_alumnos_reasignar();
        }else{
            $data["alumnos"] = $this->modelo->get_alumnos_reasignar_by_filtros(
                    array(
                            "rut" => null,
                            "ficha" => $num_ficha,
                            "oc" => '',
                            "empresa" => 0
                        )
                    );
        }
        
        $data["empresas"] = $this->modelo->get_empresas();
        $data["base_url"] = base_url()."/back_office/Contact_center/Supervisor";
        $data['fichas'] = $this->modelo->get_data_cbx(array("filtro" => 7, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['oc'] = $this->modelo->get_data_cbx(array("filtro" => 4, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));

        $this->load->view('amelia_1/template', $data);
    }

    public function Tareas() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Tareas',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/select.dataTables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/dataTables.select.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Tareas_asignadas_supervisor.js'),
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Tareas";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Tareas_asignadas_supervisor_V.php';
        $data['tareas'] = $this->modelo->get_tareas(1);
        $data['tareas_nuevas'] = $this->modelo->get_tareas(2);
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Buscador() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Buscador Alumnos',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Buscador_alumno.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Buscador Alumnos";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['id_user'] = $this->session->userdata('id_user');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Buscador_alumno_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $datos = array(
            'parametro' => 6,
            'tutor' => $this->session->userdata('id_user'),
            'perfil' => $this->session->userdata('id_perfil')
        );

        $data['Resumen'] = $this->modelo3->Buscar_alumnos_vista($datos);

        $this->load->view('amelia_1/template', $data);
    }

    public function Calculo_bonos() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Calculo Bonos',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/select.dataTables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/dataTables.select.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Calculo_bonos_supervisor.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Calculo Bonos";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Calculo_bonos_supervisor_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $datos = array('p_parametro' => 3, 'p_id_tutor' => 0, 'p_periodo' => '');
        $data['CalculoBono'] = $this->modelo->CalculoBono($datos);

        $this->load->view('amelia_1/template', $data);
    }

    public function Parametros_variables() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Parametros Variables',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Formulario.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Parametros Variables";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Formulario_V.php';

        $data['otics'] = $this->modelo->get_parametros_generales_otics();
        $data['grado_bonos'] = $this->modelo->get_parametros_generales_bono();
        $data['categorias'] = $this->modelo->get_parametros_categoria();
        $data['kpis'] = $this->modelo->get_parametros_generales_kpi();
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Resultados() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Supervisor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Resultados',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Resultado_supervisor.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Resultados";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Resultado_supervisor_V.php';
        $data['empresas_cbx'] = $this->modelo->get_data_cbx(array("filtro" => 2, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['fichas'] = $this->modelo->get_data_cbx(array("filtro" => 7, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['oc'] = $this->modelo->get_data_cbx(array("filtro" => 4, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $fecha = date("Y-m-d");
        $mes = 1;

        $data['resultados'] = $this->modelo->get_resultados_supervisor(
                array(
                    "id_empresa" => 0,
                    "num_ficha" => 0,
                    "num_oc" => 0,
                    "fecha_cierre_desde" => date("Y-m-d", strtotime("$fecha -$mes month")),
                    "fecha_cierre_hasta" => date("Y-m-d"),
                    "numero_tabla" => 0,
                    "id_user" => $this->session->userdata('id_user'),
                    "id_perfil" => $this->session->userdata('id_perfil')
                )
        );
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    // <editor-fold defaultstate="collapsed" desc="Solicitudes Asignacion de tutor">

    public function Carga_inicial_data_supervisor($cargaDetalleFicha, $limpiaFiltro) {
        if ($limpiaFiltro == 'true') {
            $this->session->set_userdata("ASG_Filtro", NULL);
        }
        $detalleFichas = array();
        $alumnosTutor = $this->modelo->alumnos_asignados_tutor();
        $alumnosEmpresa = $this->modelo->alumnos_asignados_empresa();
        if ($cargaDetalleFicha == 'true') {
            $filtros = $this->session->userdata("ASG_Filtro");
            if (is_array($filtros)) {
                $detalleFichas = $this->modelo->filtrar_fichas($filtros);
            }else{
                $detalleFichas = $this->modelo->carga_inicial_detalle_ficha_select();
            }
        }

        echo json_encode(array('detalleFicha' => $detalleFichas, 'alumnosTutor' => $alumnosTutor, 'alumnosEmpresa' => $alumnosEmpresa, 'params' => $cargaDetalleFicha.' '.$limpiaFiltro));
    }

    function Carga_alumnos_by_ficha($id_ficha) {
        $alumnos = $this->modelo->carga_alumnos_by_ficha($id_ficha);

        echo json_encode($alumnos);
    }

    function Carga_tutores() {
        $tutores = $this->modelo->carga_tutores();

        echo json_encode($tutores);
    }

    function Asignar_tutor() {
        $data_alumnos = $this->input->post('data_alumnos');
        $data = array(
            "id_tutor" => $this->input->post('id_tutor'),
            "id_ficha" => $this->input->post('id_ficha'),
            "id_usuario_supervisor" => $this->session->userdata('id_user')
        );

        // LOGICA ANTIGUA
//        for ($i = 0; $i < count($data_alumnos); $i++) {
//            $this->modelo->asignar_tutor($data, $data_alumnos[$i]["rut"], $data_alumnos[$i]["oc"], 0);
//        }
        $p_data_rut_oc = "";
        for ($i = 0; $i < count($data_alumnos); $i++) {
            $p_data_rut_oc .= $data_alumnos[$i]["rut"] . "_". $data_alumnos[$i]["oc"] . (($i == (count($data_alumnos)-1)) ? "" : "|");
        }
        
        $this->modelo->asignar_tutor_aux(0, $data["id_tutor"], $data["id_ficha"], $data["id_usuario_supervisor"], $p_data_rut_oc, 0);
        echo json_encode(array('Cod' => 1));
    }

    function Set_clasif_ficha($id_ficha) {
        $data = array(
            "id_ficha" => $id_ficha,
            "clas" => $this->input->post('clas')
        );
        $result = $this->modelo->set_clasif_ficha($data);
        echo $result;
    }

    function Asignar_alumnos_tutor_by_ficha() {
        $data = array(
            "id_tutor" => $this->input->post("id_tutor"),
            "id_ficha" => $this->input->post("id_ficha"),
            "id_usuario_supervisor" => $this->session->userdata("id_user")
        );

        $data_alumnos = $this->modelo->carga_alumnos_by_ficha($data['id_ficha']);

        //LOGICA ANTIGUA
//        for ($i = 0; $i < count($data_alumnos); $i++) {
//            $this->modelo->asignar_tutor($data, $data_alumnos[$i]["rut_alumno"], $data_alumnos[$i]["id_orden_compra"], 0);
//        }
        
        $p_data_rut_oc = "";
        for ($i = 0; $i < count($data_alumnos); $i++) {
            $p_data_rut_oc .= $data_alumnos[$i]["rut_alumno"] . "_". $data_alumnos[$i]["id_orden_compra"] . (($i == (count($data_alumnos)-1)) ? "" : "|");
        }
        
        $res = $this->modelo->asignar_tutor_aux(0, $data["id_tutor"], $data["id_ficha"], $data["id_usuario_supervisor"], $p_data_rut_oc, 0);
        echo json_encode(array('Cod' => $res));
    }

    function Filtrar_fichas() {
        $data = array(
            "id_modalidad" => $this->input->post("id_modalidad"),
            "fecha_fin" => $this->input->post("fecha_fin"),
            "id_cc" => $this->input->post("id_cc"),
            "ficha" => $this->input->post("ficha"),
            "oc" => $this->input->post("oc")
        );

        $res = $this->modelo->filtrar_fichas($data);
        $this->session->set_userdata("ASG_Filtro", $data);
        echo json_encode(array('detalleFicha' => $res));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Solicitudes Reasignacion de tutor">

    public function Filtrar_alumnos_reasignar() {
        $data = array(
            "rut" => $this->input->post("rut"),
            "ficha" => $this->input->post("ficha") == '' ? 0 : $this->input->post("ficha"),
            "oc" => $this->input->post("oc"),
            "empresa" => $this->input->post("empresa") == '' ? 0 : $this->input->post("empresa")
        );

        $alumnos = $this->modelo->get_alumnos_reasignar_by_filtros($data);
        //echo var_dump($alumnos);
        echo json_encode($alumnos);
    }

    public function Reasignar_alumnos_tutor() {
        $id_tutor = $this->input->post("id_tutor");
        $id_usuario_supervisor = $this->session->userdata("id_user");
        $data_reasignacion = $this->input->post("data_reasignaciones");
        
        $data = array(
            "id_tutor" => $id_tutor,
            "id_ficha" => 0,
            "id_usuario_supervisor" => $id_usuario_supervisor
        );

        //LOGICA ANTIGUA
//        for ($i = 0; $i < count($data_reasignacion); $i++) {
//            $data = array(
//                "id_tutor" => $id_tutor,
//                "id_ficha" => 0,
//                "id_usuario_supervisor" => $id_usuario_supervisor
//            );
//            //$res[] = $this->modelo->asignar_tutor($data, 0, 0, $data_reasignacion[$i]);
//            
//            
//        }
        
        $p_id_asignacion = "";
        for ($i = 0; $i < count($data_reasignacion); $i++) {
            $p_id_asignacion .= $data_reasignacion[$i] . (($i == (count($data_reasignacion)-1)) ? "" : "|");
        }
        
        $this->modelo->asignar_tutor_aux(1, $data["id_tutor"], $data["id_ficha"], $data["id_usuario_supervisor"], 0, $p_id_asignacion);
        echo json_encode(array('Cod' => 1));
        //echo json_encode(array("res" => $res));
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Solicitudes Parametros Variables">

    public function Editar_parametros_variables_otic($id_otic) {
        $data = array(
            "id_otic" => $id_otic,
            "dj" => $this->input->post("dj"),
            "conex" => $this->input->post("conex")
        );
        $result = $this->modelo->upd_parametros_variables_otic($data);

        if ($result[0]["filas_afectadas"] > 0) {
            echo json_encode(array("Cod" => 1, "Msg" => "Registro modificado correctamente!."));
        } else {
            echo json_encode(array("Cod" => 0, "Msg" => "Ocurrió un error al intentar modificar el registro."));
        }
    }

    public function Agregar_nuevo_grado_bono() {
        $data = array(
            "grado" => $this->input->post("grado"),
            "noBeca" => $this->input->post("noBeca"),
            "beca" => $this->input->post("beca")
        );

        $result = $this->modelo->insertar_grado_bono($data);
        if ($result[0]["filas_afectadas"] > 0) {
            echo json_encode(array("Cod" => 1, "Msg" => "Registro creado correctamente!.", "LastId" => $result[0]["last_id"]));
        } else {
            if ($result[0]["existe_grado"] > 0)
                echo json_encode(array("Cod" => 0, "Msg" => "Ya existe el grado."));
            else
                echo json_encode(array("Cod" => 0, "Msg" => "Ocurrió un error al intentar crear el registro."));
        }
    }

    public function Editar_grado_bono($id_grado) {
        $data = array(
            "idgrado" => $id_grado,
            "grado" => $this->input->post("grado"),
            "noBeca" => $this->input->post("noBeca"),
            "beca" => $this->input->post("beca")
        );

        $result = $this->modelo->upd_grado_bono($data);

        if ($result[0]["filas_afectadas"] > 0) {
            echo json_encode(array("Cod" => 1, "Msg" => "Registro creado correctamente!."));
        } else {
            echo json_encode(array("Cod" => 0, "Msg" => "Ocurrió un error al intentar crear el registro."));
        }
    }

    public function Editar_parametros_variables_categoria($id_parametro_categoria) {
        $data = array(
            "id_parametro_categoria" => $id_parametro_categoria,
            "porque" => $this->input->post("porque"),
            "script" => $this->input->post("script"),
            "pauta" => $this->input->post("pauta")
        );

        $result = $this->modelo->upd_parametro_categoria($data);
        if ($result[0]["filas_afectadas"] > 0) {
            echo json_encode(array("Cod" => 1, "Msg" => "Registro editado correctamente!."));
        } else {
            echo json_encode(array("Cod" => 0, "Msg" => "Ocurrió un error al intentar editar el registro."));
        }
    }

    public function Editar_parametros_variables_kpi($id_kpi) {
        $data = array(
            "id_parametro_kpi" => $id_kpi,
            "objetivo" => $this->input->post("objetivo")
        );

        $result = $this->modelo->upd_parametro_kpi($data);
        if ($result[0]["filas_afectadas"] > 0) {
            echo json_encode(array("Cod" => 1, "Msg" => "Registro editado correctamente!."));
        } else {
            echo json_encode(array("Cod" => 0, "Msg" => "Ocurrió un error al intentar editar el registro."));
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Solicitudes Resultados supervisor">

    public function Cargar_resultados_supervisor_filtro() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 0,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );

        $result = $this->modelo->get_resultados_supervisor($data);

        echo json_encode($result);
    }

    public function Cargar_resultados_supervisor_declaracion_jurada() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 1,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );
        $result = $this->modelo->get_resultados_supervisor($data);
        echo json_encode($result);
    }

    public function Cargar_resultados_supervisor_conectividad() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 2,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );
        $result = $this->modelo->get_resultados_supervisor($data);
        echo json_encode($result);
    }

    public function Cargar_resultados_supervisor_finalizados() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 3,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );
        $result = $this->modelo->get_resultados_supervisor($data);
        echo json_encode($result);
    }

    public function Cargar_resultados_supervisor_extensiones() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 4,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );
        $result = $this->modelo->get_resultados_supervisor($data);
        echo json_encode($result);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Solicitudes Tareas Supervisor">

    public function Tarea_supervisor_cambio() {
        $data = array(
            "tselect" => $this->input->post("tselect"),
            "oselect" => $this->input->post("oselect"),
            "treemplazo" => $this->input->post("treemplazo"),
            "oreemplazo" => $this->input->post("oreemplazo")
        );

        $res = $this->modelo->upd_tareas_cambio($data);
        if ($res[0]["filas_afectadas"] > 0)
            echo json_encode(array("Cod" => 1, "Msg" => "Registro modificado"));
        else
            echo json_encode(array("Cod" => 0, "Msg" => "Ningun registro modificado"));
    }

    // </editor-fold>


    public function CalculoBonosAjax() {
        $data = array(
            "p_parametro" => $this->input->post("p_parametro"),
            "p_id_tutor" => $this->input->post("p_id_tutor"),
            "p_periodo" => $this->input->post("p_periodo")
        );

        $result = $this->modelo->CalculoBono($data);

        echo json_encode($result);
    }

}

?>