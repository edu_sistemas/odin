<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comercial extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = '';
    private $package = 'back_office/Contact_center';
    private $model = 'Comercial_model';
    private $model3 = 'Comun_model';
    // informacion adicional para notificaciones     
    private $model2 = '../Crm/Notificaciones_model';
    private $controller = 'Comercial';
    private $ind = '';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        // informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');
        $this->load->model($this->package . '/' . $this->model3, 'modelo3');
        $this->load->library('session');
    }

    public function Solicitudes_comercial() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Comercial/Avance_cursos',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Solicitudes Contact Center',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Solicitudes_comercial.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Solicitudes Contact Center";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Solicitudes_comercial_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->session->set_userdata("datos_tabla_SC", array());
        $this->session->set_userdata("SC_Ruts", array());
        $this->load->view('amelia_1/template', $data);
    }

    // informacion adicional para notificaciones    

    public function getNotificaciones() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

    //CARGA CBX FILTROS
    public function getComboFiltrosCbx() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'perfil_id' => $this->session->userdata('id_perfil'),
            'filtro' => $this->input->post('filtro')
        );
        $datosFiltros = $this->modelo->getComboFiltrosCbx($arr_sesion);
        echo json_encode($datosFiltros);
    }

    //FIN CARGA CBX FILTROS
    //CARGA CBX FILTROS
    public function getTablaBusqueda() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'empresa' => $this->input->post('empresa'),
            'ficha' => $this->input->post('ficha'),
            'oc' => $this->input->post('oc'),
            'modalidad' => $this->input->post('modalidad')
        );
        $this->session->set_tempdata('filtros_pagging', $arr_sesion);
        $datosTabla = $this->modelo->getTablaBusqueda($arr_sesion);
        echo json_encode($datosTabla);
    }

    public function getTablaBusquedaPagging() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'empresa' => $this->input->post('empresa'),
            'ficha' => $this->input->post('ficha'),
            'oc' => $this->input->post('oc'),
            'modalidad' => $this->input->post('modalidad'),
            'start' => $this->input->post('start'),
            'length' => $this->input->post('length'),
            'column' => $this->input->post('column'),
            'dir' => $this->input->post('dir')
        );
        
        $data_filtros = $this->session->userdata("SC_Filtros");
        $nuevo_filtro = false;
        if (is_array($data_filtros)) {
            $nuevo_filtro = ($data_filtros['column'] != $arr_sesion["column"] || $data_filtros['dir'] != $arr_sesion["dir"] );
        }

        $isBusqueda = filter_var($this->input->post('isBusqueda'), FILTER_VALIDATE_BOOLEAN);
        if ($isBusqueda || $nuevo_filtro) {
            $this->session->set_userdata("SC_Filtros", $arr_sesion);
            $this->session->set_userdata("SC_Ruts", array());
            $datosTabla = $this->modelo->getTablaBusqueda($arr_sesion);
            $this->session->set_userdata("datos_tabla_SC", $datosTabla);
            $datosTablaTruncados = array_slice($datosTabla, $arr_sesion['start'], $arr_sesion['length']);
            $listaRuts = $this->session->userdata("SC_Ruts");
            for ($i = 0; $i < count($datosTablaTruncados); $i++) {
                $datosTablaTruncados[$i]["isChecked"] = false;
                $datosTablaTruncados[$i]["idICheck"] = str_replace("=", "",base64_encode($datosTablaTruncados[$i]["rut"].'_'.$datosTablaTruncados[$i]["ficha"].'_'.$datosTablaTruncados[$i]["num_orden_compra"]));
                $rutFmt = explode('-', $datosTablaTruncados[$i]["rut"])[0];
                if (count($listaRuts) > 0) {
                    $idx = -1;
                    for ($j = 0; $j < count($listaRuts); $j++) {
                        if ($listaRuts[$j]["rut"] == $datosTablaTruncados[$i]["rut"] && $listaRuts[$j]["ficha"] == $datosTablaTruncados[$i]["ficha"] && $listaRuts[$j]["num_orden_compra"] == $datosTablaTruncados[$i]["num_orden_compra"]) {
                            $idx = $i;
                            break;
                        }
                    }
                    $datosTablaTruncados[$i]["isChecked"] = ($idx >= 0);
                }
            }

            echo json_encode(array("filtro" => $arr_sesion, "data" => $datosTablaTruncados, "recordsTotal" => count($datosTabla), "recordsFiltered" => count($datosTabla)));
        } else {
            $datosTabla = $this->session->userdata("datos_tabla_SC");
            $datosTablaTruncados = array_slice($datosTabla, $arr_sesion['start'], $arr_sesion['length']);
            $listaRuts = $this->session->userdata("SC_Ruts");
            for ($i = 0; $i < count($datosTablaTruncados); $i++) {
                $datosTablaTruncados[$i]["isChecked"] = false;
                $datosTablaTruncados[$i]["idICheck"] = str_replace("=", "",base64_encode($datosTablaTruncados[$i]["rut"].'_'.$datosTablaTruncados[$i]["ficha"].'_'.$datosTablaTruncados[$i]["num_orden_compra"]));
                $rutFmt = explode('-', $datosTablaTruncados[$i]["rut"])[0];
                if (count($listaRuts) > 0) {
                    $idx = -1;
                    for ($j = 0; $j < count($listaRuts); $j++) {
                        if ($listaRuts[$j]["rut"] == $datosTablaTruncados[$i]["rut"] && $listaRuts[$j]["ficha"] == $datosTablaTruncados[$i]["ficha"] && $listaRuts[$j]["num_orden_compra"] == $datosTablaTruncados[$i]["num_orden_compra"]) {
                            $idx = $i;
                            break;
                        }
                    }
                    $datosTablaTruncados[$i]["isChecked"] = ($idx >= 0);
                }
            }

            echo json_encode(array("filtro" => $arr_sesion, "data" => $datosTablaTruncados, "recordsTotal" => count($datosTabla), "recordsFiltered" => count($datosTabla)));
        }
    }

    function validar_generar_not() {
        $listaRuts = $this->session->userdata("SC_Ruts");
        $data = array();
        if (count($listaRuts) > 0) {
            $data = array("validado" => true);
        } else {
            $data = array("msg" => "Debe seleccionar al menos un alumno", "validado" => false);
        }
        echo json_encode($data);
    }

    //FIN CARGA CBX FILTROS
    // INSERT GENEREAR NOTIFIECAIONES
    public function generarNot() {
        $arr_input_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'inputAlumno' => $this->input->post('inputAlumno'),
            'selectAccion' => $this->input->post('selectAccion'),
            'inputFicha' => $this->input->post('inputFicha'),
            'inputFecha' => $this->input->post('inputFecha'),
            'textComentario' => $this->input->post('textComentario')
        );
        $listaRuts = $this->session->userdata("SC_Ruts");
        $listaRutsFmt = "";
        for ($i = 0; $i < count($listaRuts); $i++) {
            if ($i == (count($listaRuts) - 1)) {
                $listaRutsFmt .= explode('-', $listaRuts[$i]["rut"])[0];
            } else {
                $listaRutsFmt .= explode('-', $listaRuts[$i]["rut"])[0] . '|';
            }
        }
        $arr_input_data["inputAlumno"] = $listaRutsFmt;
        $datosNot = $this->modelo->set_arr_generar_notificacion($arr_input_data);

        echo json_encode($arr_input_data);
    }

    function SC_SelectAll() {
        $array_ruts = array();
        $selectAll = filter_var($this->input->post('selectAll'), FILTER_VALIDATE_BOOLEAN);
        $msg = "";
        $this->session->set_userdata("SC_Ruts", array());
        $datosTabla = $this->session->userdata("datos_tabla_SC");
        if ($selectAll) {

            for ($i = 0; $i < count($datosTabla); $i++) {
                array_push($array_ruts, array("rut" => $datosTabla[$i]["rut"], "ficha" => $datosTabla[$i]["ficha"], "num_orden_compra" => $datosTabla[$i]["num_orden_compra"]));
            }
            $this->session->set_userdata("SC_Ruts", $array_ruts);
            $msg = count($this->session->userdata("SC_Ruts")) . " alumnos seleccionados ";
        } else {
            $msg = count($this->session->userdata("SC_Ruts")) . " alumnos deseleccionados ";
        }
        echo json_encode(array("Msg" => $msg));
    }

    function SC_Checked() {
        $select = filter_var($this->input->post('select'), FILTER_VALIDATE_BOOLEAN);
        $data = explode('_', $this->input->post('datos'));
        $listaRuts = $this->session->userdata("SC_Ruts");

        $paso = false;
        if ($select) {
            array_push($listaRuts, array("rut" => $data[0], "ficha" => $data[1], "num_orden_compra" => $data[2]));
            $paso = true;
        } else {
            for ($i = 0; $i < count($listaRuts); $i++) {
                if ($listaRuts[$i]["rut"] == $data[0] && $listaRuts[$i]["ficha"] == $data[1] && $listaRuts[$i]["num_orden_compra"] == $data[2]) {
                    $idx = $i;
                    break;
                }
            }
            array_splice($listaRuts, $idx, 1);
            $paso = true;
        }
        $this->session->set_userdata("SC_Ruts", $listaRuts);

        echo json_encode(array("Upd" => $paso, "Msg" => count($listaRuts) . " alumnos seleccionados ", "s" => $listaRuts));
    }

    // FIN INSER GENERAR NOTIFICACIONES


    public function Avance_cursos() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Comercial/Avance_cursos',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Avance Cursos',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileDownload/fileDownload.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Avance_cursos.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Avance Cursos";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        $data['cursos_cbx'] = $this->modelo->get_cursos_avances_curso(array("id_user" => $datos_menu['user_id'], "id_perfil" => $datos_menu['user_perfil']));
        // $data['empresas_cbx'] = $this->modelo->get_empresas_avances_curso(array("id_user" => $datos_menu['user_id'], "id_perfil" => $datos_menu['user_perfil']));
        $data['empresas_cbx'] = $this->modelo->getComboFiltrosCbx(array("filtro" => 2, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data["fichas"] = $this->modelo->getComboFiltrosCbx(array("filtro" => 7, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));

        $data['ocs'] = $this->modelo->getComboFiltrosCbx(array("filtro" => 4, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));

        $data['ejecutivos_cbx'] = $this->modelo->get_ejecutivos_avances_curso();
        $data['avances_inicio'] = $this->modelo->get_avances_curso(
                array(
                    "id_empresa" => "",
                    "id_curso" => "",
                    "fecha_desde" => date("Y-m") . '-01',
                    "fecha_hasta" => date("Y-m-d"),
                    "num_ficha" => "",
                    "num_oc" => "",
                    "holding" => "",
                    "ano_proceso" => "",
                    "id_ejec_comercial" => ""
                )
        );
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Avance_cursos_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Reporte_basico() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Comercial/Avance_cursos',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Reporte Básico',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Reporte_basico.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Avance Cursos";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Reporte_basico_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Seguimientos_programados() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Comercial/Avance_cursos',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Seguimientos Programados',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/select.dataTables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Seguimientos_programados.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/dataTables.select.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Seguimientos Programados";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['empresas_cbx'] = $this->modelo->getComboFiltrosCbx(array("filtro" => 2, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['ejecutivos_cbx'] = $this->modelo->get_ejecutivos_avances_curso();
        $data['main_content'] = $this->package . '/' . $this->controller . '/Seguimientos_programados_V.php';
        $data['tareas'] = $this->modelo->get_tareas(1);
        $data["tareas_nuevas"] = $this->modelo->get_tareas(2);
        $data["fichas"] = $this->modelo->getComboFiltrosCbx(array("filtro" => 7, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Buscador() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Comercial/Avance_cursos',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Buscador',
                'icono' => 'fa fa-search'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Buscador_alumno.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Buscador Alumnos";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['id_user'] = $this->session->userdata('id_user');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Buscador_alumno_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $datos = array(
            'parametro' => 6,
            'tutor' => $this->session->userdata('id_user'),
            'perfil' => $this->session->userdata('id_perfil')
        );

        $data['Resumen'] = $this->modelo3->Buscar_alumnos_vista($datos);

        $this->load->view('amelia_1/template', $data);
    }

    // <editor-fold defaultstate="collapsed" desc="Solicitudes Avances Curso">

    public function Cargar_reporte_basico_avance_curso() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_desde" => $this->input->post("fecha_desde"),
            "fecha_hasta" => $this->input->post("fecha_hasta"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "holding" => $this->input->post("holding"),
            "ano_proceso" => $this->input->post("ano_proceso"),
            "id_ejec_comercial" => $this->input->post("id_ejec_comercial")
        );

        $result = $this->modelo->get_avances_curso($data);

        echo json_encode($result);
    }

    public function Cargar_avance_curso_filtro() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_desde" => $this->input->post("fecha_desde"),
            "fecha_hasta" => $this->input->post("fecha_hasta"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "holding" => $this->input->post("holding"),
            "ano_proceso" => $this->input->post("ano_proceso"),
            "id_ejec_comercial" => $this->input->post("id_ejec_comercial")
        );

        $result = $this->modelo->get_avances_curso($data);

        echo json_encode($result);
    }

    public function Reporte_general_avance_curso() {

        $data = array(
            'p_id_empresa' => $this->input->post("id_empresa"),
            'p_id_curso' => $this->input->post("id_curso"),
            'p_fecha_desde' => $this->input->post("fecha_desde"),
            'p_fecha_hasta' => $this->input->post("fecha_hasta"),
            'p_num_ficha' => $this->input->post("num_ficha"),
            'p_num_oc' => $this->input->post("num_oc"),
            'p_holding' => $this->input->post("holding"),
            'p_ano_proceso' => $this->input->post("ano_proceso"),
            'p_id_ejec_comercial' => $this->input->post("id_ejec_comercial"),
            'p_cts' => $this->input->post("chkCts"),
            'p_correo' => $this->input->post("chkCorreo"),
            'p_costo' => $this->input->post("chkCosto"),
            'p_curso' => $this->input->post("chkCurso"),
            'p_cod_sence' => $this->input->post("chkCodSence"),
            'p_dj' => $this->input->post("chkDJ"),
            'p_empresa' => $this->input->post("chkEmpresa"),
            'p_evaluacion' => $this->input->post("chkEvaluacion"),
            'p_ficha' => $this->input->post("chkFicha"),
            'p_fecha_inicio' => $this->input->post("chkFechaInicio"),
            'p_fecha_termino' => $this->input->post("chkFechaFin"),
            'p_fecha_cierre' => $this->input->post("chkFechaCierre"),
            'p_fus' => $this->input->post("chkFUS"),
            'p_fono' => $this->input->post("chkFono"),
            'p_chk_holding' => $this->input->post("chkHolding"),
            'p_hrs_curso' => $this->input->post("chkHrsCurso"),
            'p_id_sence' => $this->input->post("chkIdSence"),
            'p_indicador' => $this->input->post("chkIndicador"),
            'p_nombre' => $this->input->post("chkNombre"),
            'p_oc' => $this->input->post("chkOC"),
            'p_primer_acceso' => $this->input->post("chkPrimerAcceso"),
            'p_rut' => $this->input->post("chkRut"),
            'p_registro_sence' => $this->input->post("chkRegistroSence"),
            'p_sucursal' => $this->input->post("chkSucursal"),
            'p_situacion' => $this->input->post("chkSituacion"),
            'p_seguimiento' => $this->input->post("chkSeguimientos"),
            'p_tiempo_conexion' => $this->input->post("chkTiempoConexion"),
            'p_tutor' => $this->input->post("chkTutor"),
            'p_ultimo_acceso' => $this->input->post("chkUltimoAcceso"),
            'p_version' => $this->input->post("chkVersion"),
            'p_prctje_avance_video' => $this->input->post("chkPrctjeAvanceVideo"),
            'p_prctje_eval_realizada' => $this->input->post("chkPrctjeEvalRealizada"),
            'p_prctje_franquicia' => $this->input->post("chkPrctjeFranquicia"),
            'p_prctje_registro_sence' => $this->input->post("chkPrctjeRegistroSence"),
            'p_prctje_tiempo_conexion' => $this->input->post("chkPrcjeTiempoConexion")
        );

        $excelData = $this->modelo->get_reporte_completo_avance_curso($data);
        if (count($excelData["result"]) > 0) {
            $this->session->set_tempdata("excelData", $excelData);
            echo json_encode(array('Cod' => 1, "Link" => base_url() . "back_office/Contact_center/Comercial/Download_reporte_general_avance_curso"));
        } else {
            echo json_encode(array('Cod' => 0, "Msg" => "No existen registros según los parámetros especificados."));
        }
    }

    public function Download_reporte_general_avance_curso() {
        $excelData = $this->session->tempdata("excelData");

        $result = $excelData["result"];
        $fields = $excelData["fields"];

        $matriz = [];
        $matriz[] = $fields;

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Reporte Completo');

        for ($i = 0; $i < count($result); $i++) {
            $matriz[] = $result[$i];
        }

        for ($i = -1; $i < count($result); $i++) {
            if ($i == -1) {
                $col = 0;
                for ($j = 0; $j < count($fields); $j++) {
                    if ($matriz[1][$fields[$j]] != "-") {
                        $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $fields[$j]);
                        $this->excel->getActiveSheet()->getCellByColumnAndRow($col, 1)->getStyle()->getFont()->setBold(true);
                        $col++;
                    }
                }
            } else {
                $col = 0;
                for ($j = 0; $j < count($fields); $j++) {
                    if ($result[$i][$fields[$j]] != "-") {
                        if ($fields[$j] == "indicador") {
                            $colorCell = "";
                            $valueCell = "Sin Información";
                            $strSplit = explode('_', $result[$i][$fields[$j]]);
                            if (count($strSplit) > 1) {
                                $colorCell = $strSplit[0];
                                $valueCell = $strSplit[1];
                            }else{
                                $colorCell = $result[$i][$fields[$j]];
                            }
                            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, ($i + 2), $valueCell);
                            $this->excel->getActiveSheet()->getCellByColumnAndRow($col, ($i + 2))->getStyle()->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB($colorCell);
                        } else {
                            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, ($i + 2), $result[$i][$fields[$j]]);
                        }
                        $col++;
                    }
                }
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Reporte_Completo_Avance_Curso.xls"');
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Solicitudes Seguimientos Programados">

    public function Cargar_tareas_by_filtro() {
        $data = array(
            "id_empresa" => $this->input->post("empresa"),
            "id_ejecutivo" => $this->input->post("ejecutivo"),
            "num_ficha" => $this->input->post("ficha"),
            "tipo" => 1
        );

        $res = $this->modelo->get_tareas_comercial_by_filtro($data);

        echo json_encode($res);
    }

    public function Cargar_tareas_nuevas_by_filtro() {
        $data = array(
            "id_empresa" => $this->input->post("empresa"),
            "id_ejecutivo" => $this->input->post("ejecutivo"),
            "num_ficha" => $this->input->post("ficha"),
            "tipo" => 2
        );

        $res = $this->modelo->get_tareas_comercial_by_filtro($data);

        echo json_encode($res);
    }

    // </editor-fold>
}

?>