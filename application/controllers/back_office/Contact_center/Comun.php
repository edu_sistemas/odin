<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Comun extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = '';
    private $package = 'back_office/Contact_center';
    private $model = 'Comun_model';
    // informacion adicional para notificaciones     
    private $model2 = '../Crm/Notificaciones_model';
    private $controller = 'Comun';
    private $ind = '';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        // informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');
        $this->load->library('session');
    }

    public function Buscar_alumnos() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Buscar Alumnos',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Buscador_alumno.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js')
        );



        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Buscar Alumnos";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_user'] = $this->session->userdata('id_user');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Buscador_alumno_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();


        $this->load->view('amelia_1/template', $data);
    }

    public function Detalle_alumno($alumno = 0) {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Buscar Alumnos',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Detalle_alumno.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        switch ($data['id_perfil']) {
            case 13:
                $data['activo'] = "Buscador Alumnos";
                break;
            default:
                $data['activo'] = "Principal";
                break;
        }

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['alumno'] = $alumno;
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Detalle_alumno_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $data['DatosAlumno'] = $this->modelo->Gestion_alumnos(array('p_parametro' => 1, 'p_valor1' => $alumno, 'p_valor2' => 0, 'p_valor3' => 0));

        $data['DatosCursos'] = $this->modelo->Gestion_alumnos(array('p_parametro' => 6, 'p_valor1' => $alumno, 'p_valor2' => 0, 'p_valor3' => 0));
        $this->load->view('amelia_1/template', $data);
    }

    public function Gestion_alumnos($alumno_curso = "0_0") {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Gestion Alumnos',
                'icono' => 'fa fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Gestion_alumnos.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        switch ($data['id_perfil']) {
            case 13:
                $data['activo'] = "Buscador Alumnos";
                break;
            default:
                $data['activo'] = "Principal";
                break;
        }

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Gestion_alumnos_V';

        $ac = explode("_", $alumno_curso);

        if (count($ac) == 0) {
            $perfil = "";
            switch ($this->session->userdata('id_perfil')) {
                case 13:
                    $perfil = "Comercial";
                    break;

                case 27:
                    $perfil = "Supervisor";
                    break;

                case 39:
                    $perfil = "Tutor";
                    break;

                default:
                    $perfil = "NA";
                    break;
            }

            redirect(base_url() . 'back_office/Contact_center/' . $perfil);
        } else if (count($ac) == 1) {
            $alumno = $ac[0];
            $curso = 0;
            $data['Alumno'] = $alumno;
            $data['Curso'] = $curso;
        } else {
            $alumno = $ac[0];
            $curso = $ac[1];
            $data['Alumno'] = $alumno;
            $data['Curso'] = $curso;
        }



        $data['DatosAlumno'] = $this->modelo->Gestion_alumnos(array('p_parametro' => 1, 'p_valor1' => $alumno, 'p_valor2' => $this->session->userdata('id_user'), 'p_valor3' => 0));

        $RutAlumno = $data['DatosAlumno'][0]['cp_rut'];

        $data['Telefonos'] = $this->modelo->Gestion_alumnos(array('p_parametro' => 5, 'p_valor1' => $alumno, 'p_valor2' => $RutAlumno, 'p_valor3' => 0));
        $data['Agendamientos'] = $this->modelo->Gestion_alumnos(array('p_parametro' => 7, 'p_valor1' => $alumno, 'p_valor2' => 0, 'p_valor3' => 0));

        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        /*
          $data['DatosCurso'] = $this->modelo->Gestion_alumnos(array('p_parametro' => 3,'p_valor1' => $alumno,'p_valor2' => 0));
          $data['Evaluaciones'] = $this->modelo->Gestion_alumnos(array('p_parametro' => 4,'p_valor1' => $alumno,'p_valor2' => 0));
         */
        $data['Tareas'] = ($this->session->userdata('id_perfil') == 13 ? "0" : "1");
        $data['id_user'] = $this->session->userdata('id_user');


        $this->load->view('amelia_1/template', $data);
    }

    public function GuardarSeguimiento() {
        $datos = array(
            'id_usuario_tutor' => $this->session->userdata('id_user'),
            'id_tarea' => $this->input->post('id_tarea'),
            'tarea_nuevo' => $this->input->post('tarea_nuevo'),
            'id_categoria_origen' => $this->input->post('categoria1'),
            'id_categoria_motivo' => $this->input->post('categoria2'),
            'id_categoria_respuesta' => $this->input->post('categoria3'),
            'comentario' => $this->input->post('comentario'),
            'telefono' => $this->input->post('telefono'),
            'rut_alumno' => $this->input->post('rut_alumno'),
            'id_seguimiento' => $this->input->post('id_seguimiento'),
            'accion' => $this->input->post('accion')
        );


        $resultado = $this->modelo->GuardarSeguimiento($datos);

        if ($resultado[0]['ID'] == "0") {
            $result = array('resultado' => 'ERROR', 'mensaje' => 'Error al Insertar el Seguimiento Intente Nuevamente.');
        } else {
            $result = array('resultado' => 'OK', 'mensaje' => 'Seguimiento Generado Exitosamente.');
        }

        echo json_encode($result);
    }


    public function GuardarEmailAlumno() {
        $datos = array(
            'rut_alumno' => $this->input->post('rut_alumno'),
            'email' => $this->input->post('email')
        );

        $resultado = $this->modelo->GuardarEmailAlumno($datos);

        if ($resultado->resultado == "0") {
            $result = array('resultado' => 'ERROR', 'mensaje' => 'Error al registrar el Email, Intente Nuevamente.');
        } else {
            $result = array('resultado' => 'OK', 'mensaje' => 'Email registrado exitosamente.');
        }

        echo json_encode($result);
    }

    function getDatosGraficos() {
        $id_user = $this->session->userdata('id_user');
        $this->load->library('MySQLMultiResult', 'mysqlmultiresult');
        $sql = "CALL sp_cc_carga_graficos_dashboard(" . $id_user . ");";
        $result = $this->mysqlmultiresult->GetMultiResults($sql);
        echo json_encode($result);
    }

    function cargaCombocategoriaCbx() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'categoria' => $this->input->post('categoria'),
            'valor' => $this->input->post('valor'),
            'valor2' => $this->input->post('valor2')
        );
        $datoscat = $this->modelo->cargaCombocategoriaCbx($arr_sesion);
        echo json_encode($datoscat);
    }

    // <editor-fold defaultstate="collapsed" desc="Modulo de envio de e-mail">

    public function Envio_Mail() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }


        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Envio de Mail',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/summernote/summernote.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css'),
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/summernote/summernote.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Envio_mail.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Envio Mails";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';

        //$data['fichas'] = $this->modelo->enviomail_get_fichas($datos_menu['user_id'], $data['id_perfil']);
        $data['fichas'] = $this->modelo->getComboFiltrosCbx(array("filtro" => 9, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['plantillas'] = $this->modelo->enviomail_get_plantillas();
        $data['main_content'] = $this->package . '/Envio_mail_V.php';

        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $datoscat1 = $this->modelo->cargaCombocategoriaCbx(array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'categoria' => 1,
            'valor' => 0,
            'valor2' => 0
        ));


        $data['categoria1'] = $datoscat1;
        $this->load->view('amelia_1/template', $data);
    }

    function enviomail_get_alumnos($idFicha) {
        $params = array(
            'idFicha' => $idFicha
        );
        $obj = $this->modelo->enviomail_get_alumnos($params);
        echo json_encode($obj);
    }

    function enviomail_get_plantilla() {
        $obj = $this->modelo->enviomail_get_plantilla($this->input->post('plantilla'));
        echo $obj;
    }

    function enviomail_enviar_plantilla() {
        $this->load->library('Libreriaemail');
        $res = array();
        $ids = join(",", $this->input->post('ids'));

        $datacc = $this->input->post('cc');
        $dataasunto = $this->input->post('asunto');
        if (strpos($datacc, ',') !== true)
            $datacc .= ',';
        $emailCC = explode(",", $datacc);
        $cc = array();
        for ($i = 0; $i < count($emailCC); $i++) {
            if (strlen(trim($emailCC[$i])) > 0)
                $cc[] = array('email' => trim($emailCC[$i]), 'nombre' => '-');
        }

        $html = $this->input->post('plantilla');
        $datos = $this->modelo->enviomail_get_contacto_alumnos($ids);
        $res = array();
        for ($i = 0; $i < count($datos); $i++) {
            $htmlEnvio = "";
            $htmlEnvio = str_replace("@Nombre_Alumno", $datos[$i]['nombre_alumno'], $html);
            $htmlEnvio = str_replace("@Nombre_Curso", $datos[$i]['nombre_curso'], $htmlEnvio);
            $htmlEnvio = str_replace("@Fecha_Termino_Curso", $datos[$i]['fecha_fin'], $htmlEnvio);
            //$htmlEnvio = str_replace("@Cantidad_Evaluaciones", $datos[$i]['cantidad_evaluaciones'], $htmlEnvio);
            $data_email = array(
                "destinatarios" => array(array('email' => $datos[$i]['correo_contacto'], 'nombre' => $datos[$i]['nombre_alumno'])),
                "cc_a" => $cc,
                "asunto" => $dataasunto,
                "body" => $htmlEnvio,
                "AltBody" => $dataasunto
            );
            // $res[] = $this->libreriaemail->Mailink($data_email["asunto"], $data_email["body"], $data_email["AltBody"], $data_email["destinatarios"], $data_email["cc_a"], array());
            array_push($res, $this->libreriaemail->CallAPISendMailContact($data_email));
        }
        // echo json_encode(array("Res" => $res));
        echo json_encode($res);
    }

    function Agregar_gestion_envio_mail() {
        $cat1 = $this->input->post("categoria1");
        $cat2 = $this->input->post("categoria2");
        $cat3 = $this->input->post("categoria3");
        $coment = $this->input->post("comentario");
        $noContact = $this->input->post("noContactar");
        $alumnos = $this->input->post("alumnos");
        $id_user = $this->session->userdata('id_user');

        $cantidad = 0;


        for ($i = 0; $i < count($alumnos); $i++) {

            $data = array(
                "categoria1" => $cat1,
                "categoria2" => $cat2,
                "categoria3" => $cat3,
                "comentario" => $coment,
                "noContactar" => ($noContact == "true" ? 1 : 0),
                "idAlumno" => $alumnos[$i],
                "idUser" => $id_user
            );

            $result = $this->modelo->enviomail_add_dejar_gestion($data);

            $cantidad += $result[0]["filas_afectadas"];
        }

        echo json_encode(array("Cod" => 1, "Msg" => "Gestión creada correctamente para $cantidad alumnos!"));
    }

    //Combobox Busqueda//

    public function Buscar_alumnos_vista() {
//        $datos = array(
//            'parametro' => $this->input->post('parametro'),
//            'tutor' => $this->input->post('tutor'),
//            'perfil' => $this->input->post('perfil')
//        );
//
//        $resultado = $this->modelo->Buscar_alumnos_vista($datos);
//        echo json_encode($resultado);

        $datos = array(
            'filtro' => $this->input->post('parametro'),
            'user_id' => $this->input->post('tutor'),
            'perfil_id' => $this->input->post('perfil')
        );

        $resultado = $this->modelo->getComboFiltrosCbx($datos);
        echo json_encode($resultado);
    }

    //Fin Combobox Busqueda//
    //Buscar Alumno//

    public function BuscarAlumnos() {
        $datos = array(
            'parametro' => $this->input->post('parametro'),
            'rut' => $this->input->post('rut'),
            'nombre' => $this->input->post('nombre'),
            'email' => $this->input->post('email'),
            'oc' => $this->input->post('oc'),
            'ficha' => $this->input->post('ficha'),
            'empresa' => $this->input->post('empresa')
        );

        $resultado = $this->modelo->BuscarAlumnos($datos);
        echo json_encode($resultado);
    }

    public function DatosCursoAlumnos() {
        $datos = array(
            'parametro' => $this->input->post('parametro'),
            'id' => $this->input->post('id')
        );

        $resultado = $this->modelo->DatosCursoAlumnos($datos);
        echo json_encode($resultado);
    }

    public function Gestion_alumnos_ajax() {
        $datos = array(
            'p_parametro' => $this->input->post('parametro'),
            'p_valor1' => $this->input->post('valor1'),
            'p_valor2' => $this->input->post('valor2'),
            'p_valor3' => $this->input->post('valor3')
        );

        $resultado = $this->modelo->Gestion_alumnos($datos);
        echo json_encode($resultado);
    }

    public function DatosDetalleAlumno() {
        $datos = array(
            'p_parametro' => $this->input->post('parametro'),
            'p_valor1' => $this->input->post('valor1'),
            'p_valor2' => $this->input->post('valor2'),
            'p_valor3' => ($this->input->post('valor3') == 0 ? $this->session->userdata('id_user') : $this->input->post('valor3'))
        );

        $resultado = $this->modelo->DatosDetalleAlumno($datos);
        echo json_encode($resultado);
    }

    //Fin Buscar Alumno//
    // informacion adicional para notificaciones    

    public function getNotificaciones() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

//FIN CARGA GRAFICO
//CARGA CBX FILTROS
    function getComboFiltrosCbx() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'perfil_id' => $this->session->userdata('id_perfil'),
            'filtro' => $this->input->post('filtro')
        );
        if ($arr_sesion['perfil_id'] == 27 && $arr_sesion['filtro'] == 3) {
           $arr_sesion['filtro'] = 7;
        }else if ($arr_sesion['perfil_id'] == 27 && $arr_sesion['filtro'] == 4) {
            $arr_sesion['filtro'] = 8;
        }
        $datosFiltros = $this->modelo->getComboFiltrosCbx($arr_sesion);
        echo json_encode($datosFiltros);
    }

//FIN CARGA CBX FILTROS
// ---- FIN DETALLE GRAFICO DECLARACIONES JURADAS
    // ---- FIN DETALLE GRAFICO RESPUESTAS
    // ---- FORMULARIO SOLICITUD ALUMNO

    public function Solicitud_alumno($rut = 0) {

        if ($rut != 0) {
            ///  array ubicaciones
            $arr_page_breadcrumb = array
                    (
            );

            $arr_theme_css_files = array
                (
                array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
                array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
                array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
                array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            );

            //  js necesarios para la pagina
            $arr_theme_js_files = array
                (
                array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
                array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Solicitud_alumno.js')
            );

            // informacion adicional para la pagina
            $data['page_title'] = '';
            $data['page_title_small'] = '';
            $data['panel_title'] = $this->nombre_item_singular;
            $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
            $data['script_adicional'] = $arr_theme_js_files;
            $data['style_adicional'] = $arr_theme_css_files;
            $data['rut_alumno'] = $rut;
            $data['main_content'] = $this->package . '/Solicitud_alumno_V.php';


            $this->load->view('amelia_1/template2', $data);
        }
    }

    // INSERT GENEREAR NOTIFIECAIONES
    public function generarSolicitudCon() {
        $arr_input_data = array(
            //'user_id'        => $this->session->userdata('id_user') ,
            //'user_perfil'    => $this->session->userdata('id_perfil'),
            'rutAlumno' => $this->input->post('rutAlumno'),
            'fechaSolicitud' => $this->input->post('fechaSolicitud'),
            'horaSolicitud' => $this->input->post('horaSolicitud'),
            'comentarioSol' => $this->input->post('comentarioSol')
        );

        $datosSol = $this->modelo->set_arr_generar_solicitud($arr_input_data);
        echo json_encode($datosSol);
    }

    // FIN INSER GENERAR NOTIFICACIONES
    // ---- FIN FORMULARIO DETALLE ALUMNO
    // </editor-fold>
    //----- RESPAWN NOTIFICACIONES 

    public function selectSolicitudAlumno() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datosSolAlum = $this->modelo->selectSolicitudAlumno($arr_sesion);
        echo json_encode($datosSolAlum);
        //echo var_dump($datosSolAlum);
    }

    // <editor-fold defaultstate="collapsed" desc="Vistas y Solicitudes de Graficos">

    public function Declaraciones_juradas() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Detalle Declaraciones juradas',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Declaraciones_juradas.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        $data['parametro'] = 1;

        //echo $data['parametro'];
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Declaraciones_juradas_V.php';




        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $this->load->view('amelia_1/template', $data);
    }

    public function Evaluaciones() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Detalle Evaluaciones',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/evaluaciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Evaluaciones_V.php';

        $data['parametro'] = 2;


        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Conexion() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Detalle Conexión',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/conexion.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Conexion_V.php';

        $data['parametro'] = 3;


        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Seguimiento() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Detalle Seguimiento',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/seguimiento.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Seguimiento_V.php';

        $data['parametro'] = 4;


        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Llamadas_objetivo() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Detalle Llamadas objetivo',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/llamadas_objetivo.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Llamadas_objetivo_V.php';

        $data['parametro'] = 5;


        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Llamadas_reemplazo() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Detalle Llamadas reemplazo',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/llamadas_reemplazo.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Llamadas_reemplazo_V.php';

        $data['parametro'] = 6;


        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Respuestas() {

        $perfilb = "";

        switch ($this->session->userdata('id_perfil')) {
            case 39:
                $perfilb = "Tutor";
                break;
            case 27:
                $perfilb = "Supervisor";
                break;

            default:
                $perfilb = "Comercial/Avance_cursos";
                break;
        }

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/' . $perfilb,
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Detalle Respuestas',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/respuestas.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Respuestas_V.php';

        $data['parametro'] = 7;

        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Carga_detalle_declaraciones_juradas() {
        $data = array(
            "id_grafico" => 1,
            "id_user" => $this->session->userdata('id_user'),
            "id_holding" => $this->input->post("id_holding"),
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_cierre" => $this->input->post("fecha_cierre")
        );
        $result = $this->modelo->get_detalle_graficos($data);
        //echo $result;
        echo json_encode($result);
    }

    public function Carga_detalle_conexiones() {
        $data = array(
            "id_grafico" => 2,
            "id_user" => $this->session->userdata('id_user'),
            "id_holding" => $this->input->post("id_holding"),
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_cierre" => $this->input->post("fecha_cierre")
        );
        $result = $this->modelo->get_detalle_graficos($data);

        echo json_encode($result);
    }

    public function Carga_detalle_evaluaciones() {
        $data = array(
            "id_grafico" => 3,
            "id_user" => $this->session->userdata('id_user'),
            "id_holding" => $this->input->post("id_holding"),
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_cierre" => $this->input->post("fecha_cierre")
        );
        $result = $this->modelo->get_detalle_graficos($data);

        echo json_encode($result);
    }

    public function Carga_detalle_seguimientos() {
        $data = array(
            "id_grafico" => 4,
            "id_user" => $this->session->userdata('id_user'),
            "id_holding" => $this->input->post("id_holding"),
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_cierre" => $this->input->post("fecha_cierre")
        );
        $result = $this->modelo->get_detalle_graficos($data);

        echo json_encode($result);
    }

    public function Carga_detalle_llamadas() {
        $data = array(
            "id_grafico" => 5,
            "id_user" => $this->session->userdata('id_user'),
            "id_holding" => $this->input->post("id_holding"),
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_cierre" => $this->input->post("fecha_cierre")
        );
        $result = $this->modelo->get_detalle_graficos($data);

        echo json_encode($result);
    }

    public function Carga_detalle_respuestas() {
        $data = array(
            "id_grafico" => 6,
            "id_user" => $this->session->userdata('id_user'),
            "id_holding" => $this->input->post("id_holding"),
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_cierre" => $this->input->post("fecha_cierre")
        );
        $result = $this->modelo->get_detalle_graficos($data);

        echo json_encode($result);
    }

    public function Carga_detalle_llamadas_mayores() {
        $data = array(
            "id_grafico" => 7,
            "id_user" => $this->session->userdata('id_user'),
            "id_holding" => $this->input->post("id_holding"),
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "id_curso" => $this->input->post("id_curso"),
            "fecha_cierre" => $this->input->post("fecha_cierre")
        );
        $result = $this->modelo->get_detalle_graficos($data);

        echo json_encode($result);
    }

    // </editor-fold>

    public function Llamado($data) {
        //$this->load->library('curl');
        $datos = explode('_', $data);

        $fono='9'.$datos[2];

        $url = "https://" . $datos[0] . "/wse/call.php?exten=" . $datos[1] . "&number=" . $fono;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $response = curl_exec($ch);
        echo $response;
    }

}

?>