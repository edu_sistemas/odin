<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2016-25-10 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Relator';
    private $nombre_item_plural = 'Relatores';
    private $package = 'back_office/Relator';
    private $model = 'Relator_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';
    private $package2 = 'back_office/Relator_contacto';
    private $model2 = 'Relator_contacto_model';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'contacto');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array(
            array
                (
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ), array
                (
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );

        // array con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Relator/listar.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Relatores";
        if (in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');

        ///  fin carga menu

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function buscarPorFiltro() {


        $rut_p = $this->input->post('rut');
        $nombre_p = $this->input->post('nombre');
        $estado_p = $this->input->post('estado');

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_datos = array(
            'rut' => $rut_p,
            'nombre' => $nombre_p,
            'estado' => $estado_p,
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );

        $data = $this->modelo->get_arr_relator_listar($arr_datos);

        echo json_encode($data);
    }

    function CambiarEstadoRelator() {

        $id_relator = $this->input->post('id');
        $estado = $this->input->post('estado');

        $arr_datos = array(
            'id_relator' => $id_relator,
            'estado' => $estado,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_sp_relator_change_status($arr_datos);

        echo json_encode($data);
    }

    function verCursosRelator() {


        $id_relator = $this->input->post('id_relator');

        $arr_datos = array(
            'id_relator' => $id_relator,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_relator_cursos($arr_datos);

        echo json_encode($data);
    }

    function guardarDatosContacto() {


        $id_relator = $this->input->post('txt_id_relator');
        $correo_contacto = $this->input->post('txt_correo_contacto');
        $telefono_contacto = $this->input->post('txt_telefono');


        $arr_datos = array(
            'id_relator' => $id_relator,
            'correo_contacto' => $correo_contacto,
            'telefono_contacto' => $telefono_contacto,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_arr_relator_contact($arr_datos);

        echo json_encode($data);
    }

    function desactivarDatosContacto() {


        $id_contacto_relator = $this->input->post('id_contacto');
        $estado = $this->input->post('estado');


        $arr_datos = array(
            'id_contacto_relator' => $id_contacto_relator,
            'estado' => $estado,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_status_relator_contact($arr_datos);

        echo json_encode($data);
    }

    function actualzarDatosContacto() {

        $id_contacto_relator = $this->input->post('txt_id_contacto_relator');
        $correo_contacto = $this->input->post('txt_correo_contacto');
        $telefono_contacto = $this->input->post('txt_telefono');

        $arr_datos = array(
            'id_contacto_relator' => $id_contacto_relator,
            'correo_contacto' => $correo_contacto,
            'telefono_contacto' => $telefono_contacto,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_update_relator_contact($arr_datos);

        echo json_encode($data);
    }

    function eliminarDatosContacto() {


        $id_contacto_relator = $this->input->post('id_contacto_relator');
        $id_relator = $this->input->post('id_relator');
        $correo_contacto = $this->input->post('correo_contacto');
        $telefono_contacto = $this->input->post('telefono_contacto');



        $arr_datos = array(
            'id_relator' => $id_relator,
            'id_contacto_relator' => $id_contacto_relator,
            'correo_contacto' => $correo_contacto,
            'telefono_contacto' => $telefono_contacto,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_delete_relator_contact($arr_datos);

        echo json_encode($data);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
