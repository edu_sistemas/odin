<?php
 
/**
 * Editar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-06 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-12-06 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar'; 
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Alumno';
    private $model = 'Alumno_model';
    private $view = 'Editar_v';
    private $controller = 'Editar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');


        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del Editar Perfil
    public function index($id_perfil = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_perfil == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
             array(
                'href' => base_url() . $this->ind .'/back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Editar',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Alumno/editar.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Alumnos";



        $arr_input_data = array(
            'id' => $id_perfil,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_Alumno'] = $this->modelo->sp_alumno_select_by_id($arr_input_data);
        if($data['data_Alumno'] == false){
            redirect($this->package . '/Listar', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //   Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function EditarAlumno() {
        /*
          Ultima fecha Modificacion: 2016-11-25
          Descripcion:  función para modificar los módulos que ya existen
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_alumno'              => $this->input->post('id_alumno'),
             'rut_alumno'              => $this->input->post('rut_alumno_v'),
            'dv_alumno'                => $this->input->post('dv_alumno_v'),
            'usuario_alumno'           => $this->input->post('usuario_alumno_v'),
            'clave_alumno'             => $this->input->post('clave_alumno_v'),
            'nombre_alumno'            => $this->input->post('nombre_alumno_v'),
            'seg_nombre_alumno'        => $this->input->post('seg_nombre_alumno_v'),
            'apellido_paterno_alumno'  => $this->input->post('apellido_paterno_alumno_v'),
            'apellido_materno_alumno'  => $this->input->post('apellido_materno_alumno_v')
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_editar_alumno($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

}

/* End of file Editar.php */
/* Location: ./application/controllers/Editar.php */
?>
