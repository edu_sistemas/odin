<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-03-09 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-03-13 [Jessica Roa] <jroa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Control_Solicitudes extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Solicitudes Tablet';
    private $package = 'back_office/Tablet';
    private $model = 'Tablet_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
// informacion adicional para notificaciones       
    private $view = 'Control_Solicitudes_v';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');  
        $this->load->model($this->usuario, 'modelousuario');     

//  Libreria de sesion
        $this->load->library('session');
    }

    public function index() {

///  array ubicacigetDatosSelectGruposEjecutivosones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
			array(
                'href' => base_url() . $this->ind . $this->package . '/Control_Solicitudest',
                'label' => 'Tablet',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css')
        );

//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/Tablet/Control_Solicitudes.js')
			
        );

          
// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Control Solicitudes Tablet";

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;      
        $this->load->view('amelia_1/template', $data);
    }

    function getInventario() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_inventario_tablet($arr_data);
        echo json_encode($datos);
    } 

    function getSolicitudes() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_solicitudes_tablet_ingresadas($arr_data);
        echo json_encode($datos);
    }   

    function getModelos() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_modelos_tablet($arr_data);
        echo json_encode($datos);
    } 

    function getGamas() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_gamas_tablet($arr_data);
        echo json_encode($datos);
    } 
    function setGamas() {
        $arr_data = array(
            'nombre'  => $this->input->post('gama_nombre'),
            'descuento' => $this->input->post('gama_descuento'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->setGamas($arr_data);
        echo json_encode($datos);
    } 
    
    function desactivarGama() {
        $arr_data = array(
            'id_gama_tablet'  => $this->input->post('id_gama_tablet'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->desactivarGama($arr_data);
        echo json_encode($datos);
    } 
    
    function getCursos() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_cursos_tablet($arr_data);
        echo json_encode($datos);
    } 

    function getUsuarioTipoEcutivoComercial() {
        // carga el combo box de los ejecutivos
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        if($this->session->userdata('id_perfil') == 13 || $this->session->userdata('id_perfil') == 14) {
		for ($i=0; $i < count($datos); $i++) {
                if($this->session->userdata('id_user') == $datos[$i]["id"]){
                    $datos = array($datos[$i]);
                    break;
                }
            }
        }
        echo json_encode($datos);
    }

    function AgregarSolicitud() {
        $arr_data = array(
            'modelo' => $this->input->post('cbx_modelo'),
            'aplicacion' => $this->input->post('cbx_aplicacion'),
            'ejecutivo' => $this->input->post('cbx_ejecutivo'),
            'cantidad' => $this->input->post('cantidad_tablet'),
            'direccion_entrega' => $this->input->post('direccion_entrega'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_arr_solicitud_tablet($arr_data);
        echo json_encode($datos);
    } 

    function EditarDireccion() {
        $arr_data = array(
            'id_solicitud' => $this->input->post('id_solicitud'),
            'direccion_editar' => $this->input->post('direccion_editar'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_direccion_solicitud($arr_data);
        echo json_encode($datos);
    } 

    function anulaSolicitud() {
        $arr_data = array(
            'id_solicitud' => $this->input->post('id_solicitud'),
            'observacion' => $this->input->post('observacion'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->del_arr_solicitud_tablet($arr_data);
        echo json_encode($datos);
    }

    function aprobarSolicitud() {
        $arr_data = array(
            'id_solicitud' => $this->input->post('id_solicitud'),
            'cbx_estado' => '2',
            'observacion' => '',
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->upd_arr_solicitudes_tablet($arr_data);
        echo json_encode($datos);
    } 

    function getSolicitudesHistorico() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_solicitudes_tablet_historico_control($arr_data);
        echo json_encode($datos);
    }   

    
    function aceptarCambio() {
        $arr_data = array(
            'id_cambio_solicitud' => $this->input->post('id_cambio_solicitud'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_cambio_solicitudes_tablet($arr_data);
        echo json_encode($datos);
    } 

    function enviaEmail() {
        $datos = $this->input->post('datos');
        $tipo = $this->input->post('tipo');
        $destinatarios = array();

        switch ($tipo) {
            case 0:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Perez')
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "Ingreso solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
                $body = $this->generaHtmlCambioDireccion($datos[0]); // mensaje completo HTML o text
                break;
            case 1:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'clinares@edutecno.com', 'nombre' => 'Carlos Linares'),
                    array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Perez'),
                    array('email' => 'naranda@edutecno.com', 'nombre' => 'Natalia Aranda Correa'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "Ingreso solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
                $body = $this->generaHtmlCambioEstadoIngreso($datos[0]); // mensaje completo HTML o text
                break;
            case 2:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez')
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "La solicitud de Tablet N°: " . $datos[0]['id_solicitud']." ha sido aceptada"; // asunto del correo
                $body = $this->generaHtmlCambioEstadoAceptado($datos[0]); // mensaje completo HTML o text
                break;
            case 3:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Perez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia')
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "Anulación solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
                $body = $this->generaHtmlEstadoDespachado($datos[0]); // mensaje completo HTML o text
                break;
            case 4:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez')
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "Anulación solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
                $body = $this->generaHtmlCambioAnula($datos[0]); // mensaje completo HTML o text
                break;
        }
      /*  $asunto = "Cambio de estado en solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
        //
        // presenciales
        $body = $this->generaHtmlCambioEstado($datos[0]); // mensaje completo HTML o text
*/
        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);
        echo json_encode($datos);
    }

    function encabezadocorreo(){
        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        return $html;
    }
    
    function piecorreo(){
        $html = "";
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    function generaHtmlCambioEstadoIngreso($data) {
        $html = "";
        $html .= $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'Se ha registrado una solicitud de Tablet.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

    
    function generaHtmlCambioDireccion($data) {
        $html = "";
        $html = $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'Se ha registrado un cambio en la dirección de la solicitud de Tablet.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_solicitante'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

    
    function generaHtmlCambioAnula($data) {
        $html = "";
        $html = $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'Se ha anulado una solicitud de Tablet.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_solicitante'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

    function generaHtmlCambioEstadoAceptado($data) {
        $html = "";
        $html .= $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'La solicitud de tablet ha sido aceptada y el departamento técnico preparará los equipos. Cuando las tablet esten listas la solicitud pasará a estado "Despachado" y se le informará a través de correo electrónico.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }
    function generaHtmlEstadoDespachado($data) {
        $html = "";
        $html .= $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'La solicitud de tablet ha sido despachada exitosamente: ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

}
/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */

