<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-03-09 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-03-09 [Jessica Roa] <jroa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Aplicaciones extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Solicitudes Tablet';
    private $package = 'back_office/Tablet';
    private $model = 'Tablet_model';
// informacion adicional para notificaciones       
    private $view = 'Aplicaciones_v';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');       

//  Libreria de sesion
        $this->load->library('session');
    }

    public function index() {

///  array ubicacigetDatosSelectGruposEjecutivosones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
			array(
                'href' => base_url() . $this->ind . $this->package . '/Aplicaciones',
                'label' => 'Tablet',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tooltipster/tooltipster.bundle.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/qTip/jquery.qtip.css')
        );

//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/qTip/jquery.qtip.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/Tablet/Aplicaciones.js')
			
        );

          
// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Despacho Tablet";

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;      
        $this->load->view('amelia_1/template', $data);
    }

    function getInventario() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_inventario_tablet($arr_data);
        echo json_encode($datos);
    } 

    function getSolicitudes() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_solicitudes_tablet($arr_data);
        echo json_encode($datos);   
    }   
    function getSolicitudByID() {
        $arr_data = array(
            'id_solicitud' => $this->input->post('id_solicitud'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_solicitudes_tablet_byID($arr_data);
        echo json_encode($datos);   
    }   

    function getSolicitudesHistorico() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_solicitudes_tablet_historico($arr_data);
        echo json_encode($datos);
    }   

    function getEstados() {
        $arr_data = array(
            'id_solicitud' => $this->input->post('id_solicitud'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_estados_tablet($arr_data);
        echo json_encode($datos);
    } 

    function descontarStockTablet() {
        $arr_data = array(
            'id_solicitud' => $this->input->post('id_solicitud'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->descontarStockTablet($arr_data);
        echo json_encode($datos);
    } 

    function cambiarEstado() {
        $arr_data = array(
            'id_solicitud' => $this->input->post('id_solicitud'),
            'cbx_estado' => $this->input->post('cbx_estado'),
            'observacion' => $this->input->post('observacion'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->upd_arr_solicitudes_tablet($arr_data);
        echo json_encode($datos);
    } 

    function aceptarCambio() {
        $arr_data = array(
            'id_cambio_solicitud' => $this->input->post('id_cambio_solicitud'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_cambio_solicitudes_tablet($arr_data);
        echo json_encode($datos);
    } 

    function enviaEmail() {
        $datos = $this->input->post('datos');
        $tipo = $this->input->post('tipo');
        $destinatarios = array();

        switch ($tipo) {
            case 0:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Perez')
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "Ingreso solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
                $body = $this->generaHtmlCambioDireccion($datos[0]); // mensaje completo HTML o text
                break;
            case 1:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'clinares@edutecno.com', 'nombre' => 'Carlos Linares'),
                    array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Perez'),
                    array('email' => 'naranda@edutecno.com', 'nombre' => 'Natalia Aranda Correa'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "Ingreso solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
                $body = $this->generaHtmlCambioEstadoIngreso($datos[0]); // mensaje completo HTML o text
                break;
            case 2:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez')
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "La solicitud de Tablet N°: " . $datos[0]['id_solicitud']." ha sido aceptada"; // asunto del correo
                $body = $this->generaHtmlCambioEstadoAceptado($datos[0]); // mensaje completo HTML o text
                break;
            case 3:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Perez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia')
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "Anulación solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
                $body = $this->generaHtmlEstadoDespachado($datos[0]); // mensaje completo HTML o text
                break;
            case 4:
                $destinatarios = array(
                    array('email' => $datos[0]['correo_usuario'], 'nombre' => $datos[0]['nombre_solicitante']),
                    array('email' => $datos[0]['correo_ejecutivo'], 'nombre' => $datos[0]['nombre_ejecutivo']),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez')
                );
                $cc = array(
                    array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
                );
                $asunto = "Anulación solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
                $body = $this->generaHtmlCambioAnula($datos[0]); // mensaje completo HTML o text
                break;
        }
      /*  $asunto = "Cambio de estado en solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
        //
        // presenciales
        $body = $this->generaHtmlCambioEstado($datos[0]); // mensaje completo HTML o text
*/
        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);
        echo json_encode($datos);
    }

    function encabezadocorreo(){
        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        return $html;
    }
    
    function piecorreo(){
        $html = "";
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    function generaHtmlCambioEstadoIngreso($data) {
        $html = "";
        $html .= $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'Se ha registrado una solicitud de Tablet.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

    
    function generaHtmlCambioDireccion($data) {
        $html = "";
        $html = $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'Se ha registrado un cambio en la dirección de la solicitud de Tablet.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_solicitante'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

    
    function generaHtmlCambioAnula($data) {
        $html = "";
        $html = $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'Se ha anulado una solicitud de Tablet.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_solicitante'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

    function generaHtmlCambioEstadoAceptado($data) {
        $html = "";
        $html .= $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'La solicitud de tablet ha sido aceptada y el departamento técnico preparará los equipos. Cuando las tablet esten listas la solicitud pasará a estado "Despachado" y se le informará a través de correo electrónico.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }
    function generaHtmlEstadoDespachado($data) {
        $html = "";
        $html .= $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'La solicitud de tablet ha sido despachada exitosamente: ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_modifico'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>N° Solicitud </td>';
        $html .= '<td class="alignright"> ' . $data['id_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modelo </td>';
        $html .= '<td class="alignright"> ' . $data['modelo_tablet'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Aplicación </td>';
        $html .= '<td class="alignright">' . $data['aplicacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Cantidad </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr class="total">';
        $html .= '<td>Ficha </td>';
        $html .= '<td class="alignright">' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Fecha </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_solicitud'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td >Estado Actual </td>';
        $html .= '<td class="alignright"> ' . $data['estado_edutecno'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Observación </td>';
        $html .= '<td class="alignright">' . $data['observacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

    function generarFormulario($id_solicitud){
        $arr_data = array(
            'id_solicitud' => $id_solicitud,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_solicitudes_tablet_byID($arr_data);

        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitacion');
        $pdf->SetTitle('Libro de Clases');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(
            0,
            0,
            0
                ), $lc = array(
            0,
            0,
            0
        ));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(0);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
		$pdf->addFont('frankie', '', 'frankie.php');
		$pdf->addFont('calibri', '', 'calibri.php');
		$pdf->SetFont('Helvetica', '', 14, '', false);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('P');

        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));
        $html = '';
        $html .= '
                <table width="100%" cellspacing="0" style="opacity:0.5; border-top: 1px solid gray;border-bottom: 1px solid gray;border-left: 1px solid gray;">
                <tr><td style="border-bottom: 1px solid gray;" width="30%">
                <img src="'.base_url().'assets/img/logoEdutecno2.png" width="120px">
                </td>
                <td width="70%">
                    <table width="100%" style="border-left: 1px solid gray;" cellspacing="0">
                        <tr style="line-height: 50px;"><td colspan="4" style="border-bottom: 1px solid gray;border-right: 1px solid gray;"><font>Formulario de Control de Tablet</font></td></tr>
                        <tr style="line-height: 14px;"><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">ÁREA / DEPTO</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">ELABORADO POR</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">REVISADO POR</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">APROBADO POR</font></td></tr>
                    </table>
                </td></tr>
                <tr><td width="30%"><font size="8">Organismo Técnico de capacitacion</font></td><td width="70%">
                <table style="border-top: 1px solid gray;" cellspacing="0">
                    <tr><td valigin="bottom" style="border-left: 1px solid gray;border-right: 1px solid gray;"><font size="9">Dpto. Técnico</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">Francisco Campos</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">José Gonzalez</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">Vitalia Linares</font></td></tr>
                </table>
                </td></tr>
                </table>
                ';
                $pdf->SetAlpha(0.5);
                
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'C', $autopadding = true);
        //$pdf->AddPage('P');
        $pdf->SetAlpha(1);
        $html='<br>';

        $html.='<h1 style="text-align:right;"> N° Solicitud: '.$datos[0]['id_solicitud'].'</h1>';
        $html.='
        <table border="1">
            <tr><td width="10%" style="background-color: #4f81bd;"><font color="white" size="11">Cliente</font></td><td width="55%"><font size="11">'.$datos[0]['razon_social_empresa'].'</font></td><td width="15%" style="background-color: #4f81bd;"><font color="white" size="11">Ficha / OC</font></td><td width="20%"><font size="11">'.$datos[0]['num_ficha'].'</font></td></tr>
        </table><br><br>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'C', $autopadding = true);
        $html='
        <table border="1" width="100%" cellpadding="1">
            <tr align="left"><td style="background-color: #4f81bd;" colspan="2"><font color="white" size="11">Detalles de la solicitud</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Nombre Solicitante</font></td><td width="80%"><font size="11">'.$datos[0]['nombre_solicitante'].'</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Departamento</font></td><td width="80%"><font size="11">'.$datos[0]['departamento'].'</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Motivo de Solicitud</font></td><td width="80%"><font size="11">'.$datos[0]['motivo'].'</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Modelo</font></td><td width="80%"><font size="11">'.$datos[0]['modelo'].'</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Aplicaciones Requeridas</font></td><td width="80%" colspan="2"><font size="11">'.$datos[0]['aplicaciones_requeridas'].'</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Fecha Solicitud</font></td><td width="50%"><font size="11">'.$datos[0]['fecha_solicitud'].'</font></td><td width="30%" rowspan="3" align="center"><font size="9"><br><br><br>Firma autorización</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Fecha Requerida</font></td><td width="50%"><font size="11">'.$datos[0]['fecha_requerida'].'</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Autorizado Por</font></td><td width="50%"><font size="11"></font></td></tr>
        </table><br><br>';

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'C', $autopadding = true);

        $html='
        <table border="1" width="100%" cellpadding="1">
            <tr align="left"><td width="100%" style="background-color: #4f81bd;" colspan="2"><font color="white" size="11">Entrega</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Fecha Entrega</font></td><td width="50%"><font size="11"></font></td><td width="30%" rowspan="3" align="center"><font size="9"><br><br><br><br><br><br><br><br>Firma recibe conforme</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd; line-height:70px;" width="20%"><font color="white" size="11" >Comentarios</font></td><td width="50%"><font size="9"></font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Recibe</font></td><td width="50%"><font size="11"></font></td></tr>
        </table><br><br>';

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'C', $autopadding = true);

        $html='
        <table border="1" width="100%">
            <tr align="left"><td width="100%" style="background-color: #4f81bd;" colspan="5"><font color="white" size="11">Devoluciones</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Fecha Recepción</font></td><td width="21%"><font size="11"></font></td><td width="9%" style="background-color: #4f81bd;" ><font color="white" size="11">Cantidad</font></td><td width="20%" ><font size="11"></font></td><td width="30%" rowspan="4" align="center"><font size="9"><br><br><br><br><br><br><br><br><br>Firma recibe conforme</font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Estado</font></td><td width="50%" cellspacing="0" cellpadding="0">
            
            <table width="100%">
            <tr style="line-height:20px;">
            <td><font size="11">Reutilizable <img src="'.base_url().'assets/img/checkbox.png" width="10px" ></font></td>
            <td><font size="11">Falla Técnica <img src="'.base_url().'assets/img/checkbox.png" width="10px" ></font></td>
            <td><font size="11">Merma <img src="'.base_url().'assets/img/checkbox.png" width="10px" ></font></td>
            </tr>
            </table>
            
            </td></tr>

            <tr align="left"><td style="background-color: #4f81bd; line-height:70px;" width="20%"><font color="white" size="11">Observaciones</font></td><td width="50%"><font size="9"></font></td></tr>
            <tr align="left"><td style="background-color: #4f81bd;" width="20%"><font color="white" size="11">Recibe</font></td><td width="50%"><font size="11"></font></td></tr>
        </table>';

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'C', $autopadding = true);
        
        $nombre_archivo = utf8_decode("Formulario.pdf");
        $pdf->Output($nombre_archivo, 'I');
		
    }

}
/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */

