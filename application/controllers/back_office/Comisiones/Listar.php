<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2018-01-04 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Comisiones';
    private $model = 'Comisiones_model';
    private $view = 'Listar_v';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            //
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Comisiones/listar.js'),
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Consultar";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_empresa($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function buscarComisionGlobal() {
        $cbx_tipo_usuario = $this->input->post('cbx_tipo_usuario');
        $cbx_ejecutivo = $this->input->post('cbx_ejecutivo');
        $calendario_mes = $this->input->post('calendario_mes');

        if ($cbx_ejecutivo == null || $cbx_ejecutivo == '') {
            $cbx_ejecutivo = '';
        }
        /*
          3 Back Office
          4 Ejecutivo Comercial
          5 Jefe Grupo Comercial
          6 Jefe Marketing
          8 Marketing
         */
        //  variables de sesion
        $array_fecha = (explode("-", $calendario_mes));

        $mes = $array_fecha[0];
        $anio = $array_fecha[1];

        $input_mes = $this->input->post('calendario_mes');
        $exploded = explode('-', $input_mes);
        /*calculo 2 meses despues*/
        if(intval($mes) < 11) { // de oct a enero
            $fecha_monto = $mes + 2;
        }
        if(intval($mes) == 11) { //selecciono nov
            $fecha_monto = 01;
        }
        if(intval($mes) == 12) { //selecciono dic
            $fecha_monto = 02;
        }
        $fecha_ant_consulta = $anio . '-' . $mes . '-01'; 

        $arr_sesion = array(
            'cbx_tipo_usuario' => $cbx_tipo_usuario,
            'id_ejecutivo' => $cbx_ejecutivo,
            'fecha_ant' => $fecha_ant_consulta,
            'fecha_m' => $fecha_monto,
            'mes' => $mes,
            'anio' => $anio,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = "";
        switch ($cbx_tipo_usuario) {
            case 3:  
                $datos = $this->modelo->get_arr_listar_comision_back_office($arr_sesion);
                break;
            case 4:
                $datos = $this->modelo->get_arr_listar_comision_ejecutivo($arr_sesion);
                break;
            case 5:
                $datos = $this->modelo->get_arr_listar_comision_jefe_ejecutivo($arr_sesion);
                break;
            case 6:
                $datos = $this->modelo->get_arr_listar_comision_marketing($arr_sesion);

                break;
            case 8:
                $datos = $this->modelo->get_arr_listar_comision_marketing($arr_sesion);
                break;
        }
        echo json_encode($datos);
    }

    function buscarComisionDetalle() {

        $cbx_tipo_usuario = $this->input->post('cbx_tipo_usuario');
        $cbx_ejecutivo = $this->input->post('cbx_ejecutivo');
        $calendario_mes = $this->input->post('calendario_mes');

        if ($cbx_ejecutivo == null || $cbx_ejecutivo == '') {
            $cbx_ejecutivo = '';
        }
        /*
          3 Back Office
          4 Ejecutivo Comercial
          5 Jefe Grupo Comercial
          6 Jefe Marketing
          8 Marketing
         */
        //  variables de sesion
        $array_fecha = (explode("-", $calendario_mes));

        $mes = $array_fecha[0];
        $anio = $array_fecha[1];

        $arr_sesion = array(
            'cbx_tipo_usuario' => $cbx_tipo_usuario,
            'id_ejecutivo' => $cbx_ejecutivo,
            'mes' => $mes,
            'anio' => $anio,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = "";
        switch ($cbx_tipo_usuario) {
            case 3:
                 $datos = $this->modelo->get_arr_listar_detalle_comision_back_office($arr_sesion);
                break;
            case 4:
                $datos = $this->modelo->get_arr_listar_detalle_comision_ejecutivo($arr_sesion);
                break;
            case 5:
                $datos = $this->modelo->get_arr_listar_detalle_comision_jefe_ejecutivo($arr_sesion);
                break;
            case 6:
                $datos = $this->modelo->get_arr_listar_detalle_comision_marketing($arr_sesion);
                break;
            case 8:
                $datos = $this->modelo->get_arr_listar_detalle_comision_marketing($arr_sesion);
                break;
        }


        echo json_encode($datos);
    }

    function getTipoUsuarioComisionCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_tipo_usuarios($arr_sesion);
        echo json_encode($datos);
    }

    function getUsuarioComisionCBX() {
        // carga el combo box Holding

        $cbx_tipo_usuario = $this->input->post('id_tipo_usuario');
        $arr_sesion = array(
            'id_tipo_usuario' => $cbx_tipo_usuario,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_usuarios_comision($arr_sesion);
        echo json_encode($datos);
    }

    public function generar() {        
        $id_tipo = $this->input->get('id_tipo');
        $id_ejecutivo = $this->input->get('id_ejecutivo');
        $mes = explode('-',$this->input->get('fecha'))[0];
        $year = explode('-',$this->input->get('fecha'))[1];
        if($id_tipo != '' && $id_ejecutivo != '') {
            /*calculo 2 meses despues*/
            if(intval($mes) < 11) { // de oct a enero
                $fecha_monto = $mes + 2;
            }
            if(intval($mes) == 11) { //selecciono nov
                $fecha_monto = 01;
            }
            if(intval($mes) == 12) { //selecciono dic
                $fecha_monto = 02;
            }
            $fecha_ant_consulta = $year . '-' . $mes . '-01'; 
            $datos['fecha_ant'] = $fecha_ant_consulta;
            $datos['fecha_m'] = $fecha_monto;

            $arr_sesion = array(
                'cbx_tipo_usuario' => $id_tipo,
                'id_ejecutivo' => $id_ejecutivo,
                'mes' => $mes,
                'anio' => $year,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            
            $arr_sesion3 = array(
                'cbx_tipo_usuario' => $id_tipo,
                'id_ejecutivo' => $id_ejecutivo,
                'mes' => $mes,
                'anio' => $year,
                'fecha_ant' => $datos['fecha_ant'],
                'fecha_m' => $datos['fecha_m'],
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $comision_detalle = "";
            $comision_usuario = "";
            $datos_res = "";
            $datos_resultado = [];
            switch ($id_tipo) {
                case 3:
                    $comision_detalle = $this->modelo->get_arr_listar_detalle_comision_back_office($arr_sesion);
                    $comision_usuario = $this->modelo->get_arr_listar_comision_back_office($arr_sesion3);
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_back_office($arr_sesion3);
                    break;
                case 4:
                    $comision_detalle = $this->modelo->get_arr_listar_detalle_comision_ejecutivo($arr_sesion);
                    $comision_usuario = $this->modelo->get_arr_listar_comision_ejecutivo($arr_sesion3);
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_ejecutivo($arr_sesion3);
                    break;
                case 5:
                    $comision_detalle = $this->modelo->get_arr_listar_detalle_comision_jefe_ejecutivo($arr_sesion);
                    $comision_usuario = $this->modelo->get_arr_listar_comision_jefe_ejecutivo($arr_sesion3);
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_jefe_ejecutivo($arr_sesion3);
                    break;
                case 6:
                    $comision_detalle = $this->modelo->get_arr_listar_detalle_comision_marketing($arr_sesion);
                    $comision_usuario = $this->modelo->get_arr_listar_comision_marketing($arr_sesion3);
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_marketing($arr_sesion3);
                    break;
                case 8:
                    $comision_detalle = $this->modelo->get_arr_listar_detalle_comision_marketing($arr_sesion);
                    $comision_usuario = $this->modelo->get_arr_listar_comision_marketing($arr_sesion3);
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_marketing($arr_sesion3);
                    break;
            }
            if(count($datos_res) > 0) {
                if($id_tipo == '6' || $id_tipo == '8' || $id_tipo == '3') {
                    for ($i = 0; $i < count($datos_res); $i++) {
                        if ($datos_res[$i]['monto_rectificacion'] != '-') {
                            $datos_res[$i]['comision_pagada'] = round((intval($datos_res[$i]['val_final']) - intval($datos_res[$i]['monto_rectificacion'])) * floatval($datos_res[$i]['%comision']));
                            $datos_res[$i]['diferencia_comision'] = intval($datos_res[$i]['comision_neta']) - intval($datos_res[$i]['comision_pagada']);
                            array_push($datos_resultado,$datos_res[$i]);
                        }
                    } 
                }
                if($id_tipo == '4' || $id_tipo == '5') {
                    for ($i = 0; $i < count($datos_res); $i++) {
                        if ($datos_res[$i]['monto_rectificacion'] != '-') {
                            $datos_res[$i]['comision_pagada'] = round((intval($datos_res[$i]['venta_neta']) - intval($datos_res[$i]['monto_rectificacion'])) * floatval($datos_res[$i]['%comision']));
                            $datos_res[$i]['diferencia_comision'] = intval($datos_res[$i]['comision_neta']) - intval($datos_res[$i]['comision_pagada']);
                            array_push($datos_resultado,$datos_res[$i]);
                        }
                    }
                }
            }
            /*       $arr_sesion2 = array(
                'id_tipo_usuario' => $id_tipo,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            */
            
            $arr_sesion2 = array(
                'id_usuario' => $id_ejecutivo,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

            $grupo_comercial = $this->modelo->get_grupo_comercial($arr_sesion2);

            $total_curso = 0;
            $total_arriendo = 0;
            $total_venta_activa = 0;
            $total_descuentos = 0;
            $total_rebajas = 0;
            $comisiones_propias = 0;
            $comisiones_por_ser_jefe = 0;
            $descuento_elearning_office = 0;
            $descuento_autoInstKit = 0;
            $descuento_autoInstTablet = 0;
            $descuento_elearning_ingles = 0;
            $descuento_istas = 0;
            $descuento_elearning_365 = 0;
            $porcentaje_curso = 0;
            $porcentaje_arriendo = 0;

            if(count($comision_detalle) > 0) {
                for ($i=0; $i < count($comision_detalle); $i++) { 
                    if($comision_detalle[$i]['tipo'] == "Curso" ){
                        $total_curso += $comision_detalle[$i]['valor_venta_activa'];
                    }
                    if($comision_detalle[$i]['tipo'] == "Arriendo" ){
                        $total_arriendo += $comision_detalle[$i]['valor_venta_activa'];
                    }
                }

                $porcentaje_curso = round((($total_curso * 100) / ($total_curso + $total_arriendo)),1);
                $porcentaje_arriendo = round((100 - $porcentaje_curso),1);

                for ($i=0; $i < count($comision_detalle); $i++) { 
                    $total_venta_activa += $comision_detalle[$i]['valor_venta_activa'];
                    $total_descuentos += $comision_detalle[$i]['total_descuentos'];
                    $total_rebajas += $comision_detalle[$i]['valor_total_eliminados'];
                    if($comision_detalle[$i]['id_usuario'] == $id_ejecutivo ){
                        $comisiones_propias += $comision_detalle[$i]['comision_neta'];
                    }
                    if($comision_detalle[$i]['id_usuario'] != $id_ejecutivo ){
                        $comisiones_por_ser_jefe += $comision_detalle[$i]['comision_neta'];
                    }
                    switch ($comision_detalle[$i]['tipo_producto']) {
                        case "E-learning Office":
                            $descuento_elearning_office += $comision_detalle[$i]['total_descuentos'];
                        break;
                        case "Autoinstruccional Kit":
                            $descuento_autoInstKit += $comision_detalle[$i]['total_descuentos'];
                        break;
                        case "Autoinstruccional Tablet":
                            $descuento_autoInstTablet += $comision_detalle[$i]['total_descuentos'];
                        break;
                        case "E-learning Ingles":
                            $descuento_elearning_ingles += $comision_detalle[$i]['total_descuentos'];
                        break;
                        case "E-learning ISTAS(Salud y autocuidado)":
                            $descuento_istas += $comision_detalle[$i]['total_descuentos'];
                        break;
                        case "E-learning 365":
                            $descuento_elearning_365 += $comision_detalle[$i]['total_descuentos'];
                        break;
                    }
                }
                /*
                if($comision_detalle[$i]['tipo_producto'] == "E-learning Office"){
                    $descuento_elearning_office += $comision_detalle[$i]['total_descuentos'];
                }
                if($comision_detalle[$i]['tipo_producto'] == "Autoinstruccional Kit"){
                    $descuento_autoInstKit += $comision_detalle[$i]['total_descuentos'];
                }
                if($comision_detalle[$i]['tipo_producto'] == "Autoinstruccional Tablet"){
                    $descuento_autoInstTablet += $comision_detalle[$i]['total_descuentos'];
                }
                if($comision_detalle[$i]['tipo_producto'] == "E-learning Ingles"){
                    $descuento_elearning_ingles += $comision_detalle[$i]['total_descuentos'];
                }
                if($comision_detalle[$i]['tipo_producto'] == "E-learning ISTAS(Salud y autocuidado)"){
                    $descuento_istas += $comision_detalle[$i]['total_descuentos'];
                }
                if($comision_detalle[$i]['tipo_producto'] == "E-learning 365"){
                    $descuento_elearning_365 += $comision_detalle[$i]['total_descuentos'];
                }*/
            }
        }
        $GLOBALS ['num_ficha'] = 0;
        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitación');
        $pdf->SetTitle('Informe Comisiones');
        $pdf->SetSubject('Informe Comisiones');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

        $PDF_HEADER_LOGO = "logoEdutecno.png"; //any image file. check correct path.
        $PDF_HEADER_LOGO_WIDTH = "15";
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING = "";
        //$pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);
        $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE . ' ', $PDF_HEADER_STRING, array(0, 0, 0), array(0, 64, 128));
		$pdf->setFooterData ( $tc = array (
				0,
				64,
				0 
		), $lc = array (
				0,
				64,
				128 
		) );
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(false);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetMargins(10, 18, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->addFont('dejavusanscondensed', '', 'dejavusanscondensed.php');
        $pdf->SetFont('dejavusanscondensed', '', 8, '', false);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L');

        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));

        // Establecemos el contenido para imprimir
        // $ficha = $this->input->post('fichas');
        // $fichas = $this->modelo->getFichas();
        // foreach($fichas as $ficha)
        // {
        // $prov = $ficha['num_ficha'];
        // }
        // echo json_encode($dataInforme;
        // preparamos y maquetamos el contenido a crear
        $id_impresion = mt_rand();
        if($id_tipo != '' && $id_ejecutivo != '') {
            if(count($comision_usuario) > 0) {
                $html = '<h1 align="center">Informe de Comisiones por Ventas</h1>';
                $html .= '<p align="right">Identificador de impresión: '.$id_impresion.'</p>'; 
                $html .= '<h3 align="left">Periodo de ventas: '. $mes .'-'. $year .'</h3>';
                $html .= '<h3 align="left">Periodo liquidado en: </h3>';
                $pdf->writeHTMLCell($w = 0, $h = 25, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                $html = '<table><tr><td width="25%">';
                $html .= '<table border="1"><tr align="center"><td><b>Curso</b></td><td><b>Arriendo</b></td></tr>';
                $html .= '<tr align="center"><td>'.'$'.number_format($total_curso).'</td><td>'.'$'.number_format($total_arriendo).'</td></tr>';
                $html .= '<tr align="center"><td>'.$porcentaje_curso.'%'.'</td><td>'.$porcentaje_arriendo.'%'.'</td></tr>';
                $html .= '<tr align="center"><td><b>TOTAL: </b></td><td>'. '$'.number_format($comision_usuario[0]['valor_venta_activa']).'</td></tr>';
                $html .='</table></td>';
                $html .= '<td width="1%"></td>';
                $html .= '<td width="40%"><table border="1"><tr><td width="60%" align="center"><b>';
                for ($i=0; $i < count($comision_usuario); $i++) { 
                    if($comision_usuario[$i]['id_usuario'] == $id_ejecutivo){
                    $html .= $comision_usuario[$i]['ejecutivo'];
                    }
                }
                $html .= '</b></td><td width="40%" align="center"><b>Totales</b></td></tr>';
                $html .= '<tr><td width="60%" align="left"><b>TOTAL VENTAS: </b></td><td width="40%" align="right">'. '$'.number_format($comision_usuario[0]['valor_venta_activa']).'</td></tr>';
                $html .= '<tr><td width="60%" align="left">Total Descuentos (kits, tablet, etc.)</td><td width="40%" align="right">'. '$'.number_format($comision_usuario[0]['total_descuentos_por_productos']).'</td></tr>';
                $html .= '<tr><td width="60%" align="left">Total Rebajas (alumnos, notas de crédito, etc.)</td><td width="40%" align="right">'. '$'.number_format($comision_usuario[0]['valor_total_eliminados']).'</td></tr>';
                $html .= '<tr><td width="60%" align="left"><b>TOTAL VENTAS NETA: </b></td><td width="40%" align="right">'. '$'.number_format($total_venta_activa -  $total_descuentos - $total_rebajas).'</td></tr>';
                $html .= '<tr><td width="60%" align="left">4% Comisiones Propias</td><td width="40%" align="right">'. '$'.number_format($comisiones_propias).'</td></tr>';
                $html .= '<tr><td width="60%" align="left">1% Comisiones Jefatura</td><td width="40%" align="right">'. '$'.number_format($comisiones_por_ser_jefe).'</td></tr>';
                $html .= '<tr><td width="60%" align="left"><b>TOTAL COMISIÓN BRUTA</b></td><td width="40%" align="right">'. '$'.number_format($comision_usuario[0]['comision_neta']).'</td></tr>';
                $html .= '</table></td>';
                $html .= '<td width="1%"></td>';
                $html .= '<td width="33%">';
                $html .= '<table border="1"><tr align="center"><td>Unitario</td><td><b>Descuentos</b></td><td></td></tr>';
                $html .= '<tr><td align="right">'.'$'.number_format(10000).'</td><td align="left"><b>E-learning Office</b></td><td align="right">'.'$'.number_format($descuento_elearning_office).'</td></tr>';
                $html .= '<tr><td align="right">'.'$'.number_format(30000).'</td><td align="left"><b>AutoInst CD</b></td><td align="right">'.'$'.number_format($descuento_autoInstKit).'</td></tr>';
                $html .= '<tr><td align="right">'.'$'.number_format(130000).'</td><td align="left"><b>AutoInst Tablet</b></td><td align="right">'.'$'.number_format($descuento_autoInstTablet).'</td></tr>';
                $html .= '<tr><td align="right">'.'$'.number_format(96000).'</td><td align="left"><b>E-learning Inglés</b></td><td align="right">'.'$'.number_format($descuento_elearning_ingles).'</td></tr>';
                $html .= '<tr><td align="right">'.'$'.number_format(20000).'</td><td align="left"><b>ISTAS</b></td><td align="right">'.'$'.number_format($descuento_istas).'</td></tr>';
                $html .= '<tr><td align="right">'.'$'.number_format(20000).'</td><td align="left"><b>365</b></td><td align="right">'.'$'.number_format($descuento_elearning_365).'</td></tr>';
                $html .= '<tr><td align="right"></td><td align="center"><b>Total General</b></td><td align="right">'.'$'.number_format($descuento_elearning_office + $descuento_autoInstKit + $descuento_autoInstTablet + $descuento_elearning_ingles + $descuento_istas + $descuento_elearning_365).'</td></tr>';
                $html .= '</table>';
                $html .= '</td></tr></table>';

                $html .= '<br><br><table align="center"><tr><td>';
                $html .= '<table align="center">';
                $html .= '<tr><td> </td></tr>';
                $html .= '<tr><td> </td></tr></table>';

                $html .= '</td><td>';
                
                $html .= '<table border="1">';
                $html .= '<tr><td align="left" ><b>Rectificaciones pendientes del mes</b></td><td align="right">'.'$'.number_format($comision_usuario[0]['tot_rectificaciones']).'</td></tr>';
                $html .= '</table>';

                $html .= '</td></tr>';
                $html .='</table>';

                $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                //Grafico de Venta-Arriendo
                $xc = 45;
                $yc = 73;
                $r = 10;

                $pdf->SetFillColor(0, 0, 255);
                $pdf->PieSector($xc, $yc, $r, 0, 360, 'FD', false, 0, 2);

                $pdf->SetFillColor(255, 0, 0);
                $pdf->PieSector($xc, $yc, $r, 0, (360/100) * $porcentaje_arriendo, 'FD', false, 0, 2);

                // write labels
                $pdf->SetTextColor(0,0,255);
                $pdf->Text(35, 85, 'Curso');
                $pdf->SetTextColor(255,0,0);
                $pdf->Text(45, 85, 'Arriendo');
                $pdf->SetTextColor(0,0,0);
                //Fin Grafico de Venta-Arriendo

                $html = '';
                $html .= '<h3>Grupo Comercial</h3>';
                $html .= '<table><tr><td>';
                $html .= '<table border="1" align="center">';
                for ($i=0; $i < count($grupo_comercial); $i++) { 
                    $html .= '<tr><td>'.$grupo_comercial[$i]['ejecutivo'].'</td><td>'.$grupo_comercial[$i]['cargo'].'</td></tr>';
                }
                $html .='</table>';
                $html .= '</td><td>';
                $html .= '</td><td>';
                $html .= '<table align="center">';
                $html .= '<tr><td> </td></tr>';
                $html .= '<tr><td> </td></tr>';
                $html .= '<tr><td> </td></tr>';
                $html .= '<tr><td style="border-bottom: 1px solid black;"></td></tr>';
                $html .= '<tr><td><b>';
                for ($i=0; $i < count($comision_usuario); $i++) { 
                    if($comision_usuario[$i]['id_usuario'] == $id_ejecutivo){
                    $html .= $comision_usuario[$i]['ejecutivo'];
                    }
                }
                $html .= '</b></td></tr>';
                $html .= '<tr><td>Declaro estar conforme con las comisiones a liquidar en este periodo, comprendo la determinación de mis comisiones, no tengo reclamo alguno al respecto y autorizo que se agregen a mi liquidación de sueldo correspondiente.</td></tr>';
                $html .='</table>';
                $html .= '</td></tr>';
                $html .='</table>';
                $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = 95, $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                $html = '<table>';
                $html .= '<tr>';
                $html .= '<td style="border-bottom: 1px solid black;"></td>';
                $html .= '<td style="border-bottom: 1px solid black;"></td>';
                $html .= '<td style="border-bottom: 1px solid black;"></td>';
                $html .= '<td style="border-bottom: 1px solid black;"></td>';
                $html .= '<td style="border-bottom: 1px solid black;"></td>';
                $html .= '</tr>';
                $html .= '<tr align="center">';
                $html .= '<td><b>Hernán Rojas</b></td>';
                $html .= '<td><b>Vitalia Linares</b></td>';
                $html .= '<td><b>Carlos Linares</b></td>';
                $html .= '<td><b>Ignacio Pérez</b></td>';
                if($id_tipo == 4 || $id_tipo == 5 || $id_tipo == 3) {
                    $html .= '<td><b>'.$grupo_comercial[0]['ejecutivo'].'</b></td>';
                }
                $html .= '</tr>';
                $html .= '<tr align="center">';
                $html .= '<td>Jefe de Adm. y Finanzas</td>';
                $html .= '<td>Gerente de Operaciones</td>';
                $html .= '<td>Director</td>';
                $html .= '<td>Coordinador de Negocios</td>';
                $html .= '<td>Enc. Rev. de Grupo</td>';
                $html .= '</tr>';
                $html .= '</table>';
                $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = 175, $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
                    
                $pagina = intval($pdf->getPage()) + 1;
                $pdf->AddPage('L');
                $pdf->setPage($pagina);
                $html = '<p align="right">Identificador de impresión: '.$id_impresion.'</p>'; 
            }
            if(count($comision_detalle) > 0){
                $html .= '<table border="1">';
                $html .= '<tr align="center">';
                $html .= '<td><b>FICHA</b></td>';
                $html .= '<td><b>ALUMNOS</b></td>';
                $html .= '<td><b>VALOR</b></td>';
                $html .= '<td><b>TOTAL</b></td>';
                $html .= '<td><b>EMPRESA</b></td>';
                $html .= '<td><b>TIPO</b></td>';
                $html .= '<td><b>TIPO PRODUCTO</b></td>';
                $html .= '<td><b>TIPO SUBFAMILIA</b></td>';
                $html .= '<td><b>EJECUTIVO DE VENTAS</b></td>';
                $html .= '<td><b>COM NETA</b></td>';
                $html .= '</tr>';
                for ($i=0; $i < count($comision_detalle); $i++) { 
                    $html .= '<tr align="center">';
                    $html .= '<td>'.$comision_detalle[$i]['num_ficha'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['total_participantes'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['valor'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['valor_venta_activa'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['empresa'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['tipo'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['tipo_producto'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['tipo_producto'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['ejecutivo'].'</td>';
                    $html .= '<td>'.$comision_detalle[$i]['comision_neta'].'</td>';
                    $html .= '</tr>';
                }
                $html .= '</table>';
            }
            if(count($datos_resultado) > 0) {
                $html .= '<h3>Rectificaciones</h3>';
                $html .= '<br><br><table border="1">';
                $html .= '<tr align="center">';
                $html .= '<td><b>EJECUTIVO DE VENTAS</b></td>';
                $html .= '<td><b>FICHA</b></td>';
                $html .= '<td><b>FECHA INICIO CURSO</b></td>';
                $html .= '<td><b>MONTO RECTIFICACIÓN</b></td>'; // FALTA RANGO
                $html .= '<td><b>VALOR ACTUAL</b></td>';
                $html .= '<td><b>DESCUENTO</b></td>';
                $html .= '<td><b>TOTAL DESCUENTOS</b></td>';
                $html .= '<td><b>VENTA NETA</b></td>';
                $html .= '<td><b>COM NETA</b></td>';
                $html .= '<td><b>COM PAGADA</b></td>';
                $html .= '<td><b>DIF COM</b></td>';
                $html .= '</tr>';
                for ($i=0; $i < count($datos_resultado); $i++) { 
                    $html .= '<tr align="center">';
                    $html .= '<td>'.$datos_resultado[$i]['nom_ejecutivo'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['num_ficha'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['fecha_inicio'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['monto_rectificacion'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['val_final'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['descuento'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['total_descuentos'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['venta_neta'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['comision_neta'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['comision_pagada'].'</td>';
                    $html .= '<td>'.$datos_resultado[$i]['diferencia_comision'].'</td>';
                    $html .= '</tr>';
                }
                $html .= '</table>';
            }
            if(count($comision_detalle) > 0){
                $html .= '<p align="right">Identificador de impresión: '.$id_impresion.'</p>'; 
            } else {
                $html = '<h1 align="center">No hay registros</h1>';
            }
        } else {
            $html = '<h1 align="center">Debe seleccionar un usuario y categoria</h1>';
        }
        $pdf->writeHTMLCell($w = 0, $h = 25, $x = '', $y = 20, $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);

        $nombre_archivo = utf8_decode("Informe_presencial_Ficha_" . $GLOBALS['num_ficha'] . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }
    
    
    function listarRectificaciones() {
        $cbx_tipo_usuario = $this->input->post('cbx_tipo_usuario');
        $cbx_ejecutivo = $this->input->post('cbx_ejecutivo');
        $calendario_mes = $this->input->post('calendario_mes');
//var_dump($cbx_tipo_usuario . 'aaa' . $cbx_ejecutivo . 'aaa' . $calendario_mes);
        if ($cbx_ejecutivo == null || $cbx_ejecutivo == '') {
            $cbx_ejecutivo = '';
        }     
        $input_mes = $this->input->post('calendario_mes');
        $exploded = explode('-', $input_mes);
        /*calculo 2 meses despues*/
        if(intval($exploded[0]) < 11) { // de oct a enero
            $fecha_monto = $exploded[0] + 2;
        }
        if(intval($exploded[0]) == 11) { //selecciono nov
            $fecha_monto = 01;
        }
        if(intval($exploded[0]) == 12) { //selecciono dic
            $fecha_monto = 02;
        }
        $fecha_ant_consulta = $exploded[1] . '-' . $exploded[0] . '-01'; 

      //  $datos['id_usuario'] = $fecha_ant_consulta;
        $datos['fecha_ant'] = $fecha_ant_consulta;
        $datos['fecha_m'] = $fecha_monto;
        $datos['user_id'] = $this->session->userdata('id_user');
        $datos['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'cbx_tipo_usuario' => $cbx_tipo_usuario,
            'id_ejecutivo' => $cbx_ejecutivo,
            'fecha_ant' => $datos['fecha_ant'],
            'fecha_m' => $datos['fecha_m'],
            'user_id' => $datos['user_id'],
            'user_perfil' => $datos['user_perfil']
        );
        $datos_res = "";
       // var_dump($cbx_ejecutivo != '');
        
       if($cbx_ejecutivo != ''){
           $datos_resultado = [];
            switch ($cbx_tipo_usuario) {
                case 3:  
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_back_office($arr_sesion);
                break;
                case 4:
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_ejecutivo($arr_sesion);
                break;
                case 5:
                        $datos_res = $this->modelo->get_arr_listar_rectificaciones_jefe_ejecutivo($arr_sesion);
                break;
                case 6:
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_marketing($arr_sesion);
                break;
                case 8:
                    $datos_res = $this->modelo->get_arr_listar_rectificaciones_marketing($arr_sesion);
                break;
            }
        }
        if($cbx_tipo_usuario == '6' || $cbx_tipo_usuario == '8' || $cbx_tipo_usuario == '3') {
            for ($i = 0; $i < count($datos_res); $i++) {
                if ($datos_res[$i]['monto_rectificacion'] != '-') {
                    $datos_res[$i]['comision_pagada'] = round((intval($datos_res[$i]['val_final']) - intval($datos_res[$i]['monto_rectificacion'])) * floatval($datos_res[$i]['%comision']));
                    $datos_res[$i]['diferencia_comision'] = intval($datos_res[$i]['comision_neta']) - intval($datos_res[$i]['comision_pagada']);
                    array_push($datos_resultado,$datos_res[$i]);
                }
            } 
        }
        if($cbx_tipo_usuario == '4' || $cbx_tipo_usuario == '5') {
            for ($i = 0; $i < count($datos_res); $i++) {
                if ($datos_res[$i]['monto_rectificacion'] != '-') {
                    $datos_res[$i]['comision_pagada'] = round((intval($datos_res[$i]['venta_neta']) - intval($datos_res[$i]['monto_rectificacion'])) * floatval($datos_res[$i]['%comision']));
                    $datos_res[$i]['diferencia_comision'] = intval($datos_res[$i]['comision_neta']) - intval($datos_res[$i]['comision_pagada']);
                    array_push($datos_resultado,$datos_res[$i]);
                }
            }
          //  var_dump($datos_res,$datos_resultado, $cbx_tipo_usuario);
           // die();
        }
        echo json_encode($datos_resultado);
    }

    function setHistorico() {
        $id_tipo = $this->input->post('cbx_tipo_usuario');
        $id_ejecutivo = $this->input->post('cbx_ejecutivo');
        $mes = explode('-',$this->input->post('calendario_mes'))[0];
        $year = explode('-',$this->input->post('calendario_mes'))[1];
       // var_dump($this->input->post());
       // die();
        /*calculo 2 meses despues*/
        if(intval($mes) < 11) { // de oct a enero
            $fecha_monto = $mes + 2;
        }
        if(intval($mes) == 11) { //selecciono nov
            $fecha_monto = 01;
        }
        if(intval($mes) == 12) { //selecciono dic
            $fecha_monto = 02;
        }
        $fecha_ant_consulta = $year . '-' . $mes . '-01'; 
        $datos['fecha_ant'] = $fecha_ant_consulta;
        $datos['fecha_m'] = $fecha_monto;
        $arr_sesion = array(
            'id_ejecutivo' => $id_ejecutivo,
            'id_tipo' => $id_tipo,
            'fecha_ant' => $datos['fecha_ant'],
            'fecha_m' => $datos['fecha_m'],
            'mes' => $mes,
            'anio' => $year,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
       if($id_ejecutivo != ''){
           $datos_res = "";
            switch ($id_tipo) {
                case 3:  
                    $datos_res = $this->modelo->set_historico_back_office($arr_sesion);
                break;
                case 4:
                    $datos_res = $this->modelo->set_historico_ejecutivo($arr_sesion);
                break;
                case 5:
                    $datos_res = $this->modelo->set_historico_jefe_ejecutivo($arr_sesion);
                break;
                case 6:
                    $datos_res = $this->modelo->set_historico_marketing($arr_sesion);
                break;
                case 8:
                    $datos_res = $this->modelo->set_historico_marketing($arr_sesion);
                break;
            }
        }
        echo json_encode($datos_res);
    }

    function getFechaUltimoHistoricoComision() {
        $id_tipo = $this->input->post('cbx_tipo_usuario');
        $id_ejecutivo = $this->input->post('cbx_ejecutivo');
        $mes = explode('-',$this->input->post('calendario_mes'))[0];
        $year = explode('-',$this->input->post('calendario_mes'))[1];
        $arr_sesion = array(
            'id_ejecutivo' => $id_ejecutivo,
            'id_tipo' => $id_tipo,
            'mes' => $mes,
            'anio' => $year,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos_res = "";
       if($id_ejecutivo != ''){
            $datos_res = $this->modelo->get_consulta_ultimo_historico_comisiones($arr_sesion);
        }
        echo json_encode($datos_res);
    }

    function getFechaUltimoHistoricoRectificaiones() {
        $id_tipo = $this->input->post('cbx_tipo_usuario');
        $id_ejecutivo = $this->input->post('cbx_ejecutivo');
        $mes = explode('-',$this->input->post('calendario_mes'))[0];
        $year = explode('-',$this->input->post('calendario_mes'))[1];
        $arr_sesion = array(
            'id_ejecutivo' => $id_ejecutivo,
            'id_tipo' => $id_tipo,
            'mes' => $mes,
            'anio' => $year,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos_res = "";
       if($id_ejecutivo != ''){
            $datos_res = $this->modelo->get_consulta_ultimo_historico_rectificaciones($arr_sesion);
        }
        echo json_encode($datos_res);
    }
}

