<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AgregarComision extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Nueva Comisión';
    private $nombre_item_plural = 'Nuevas Comisiones';
    private $package = 'back_office/Comisiones';
    private $model = 'Comisiones_model';
    private $modelo_usuario = 'back_office/Usuario/Usuario_model';
    private $modelo_perfil = 'back_office/Perfil/Perfil_model';
    private $view = 'AgregarComision_v';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->modelo_usuario, 'modelousuario');
        $this->load->model($this->modelo_perfil, 'modeloperfil');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/AgregarComision',
                'label' => 'Comisiones',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            //
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Comisiones/AgregarComision.js'),
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Ver Comisiones";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_empresa($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function insert_comision() {

        $arr_sesion = array(
            'c_curso' =>  $this->input->post('porcent_comision_curso'),
            'c_arriendo' => $this->input->post('porcent_comision_arriendo'),
            'c_jefatura' =>  $this->input->post('porcent_comision_jefatura'),
            'c_otro' =>  $this->input->post('porcent_comision_especial'),
            'f_inicio' =>  $this->input->post('comision_valida_apartir_de'),
            'id_user' => $this->input->post('usuario'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        //var_dump()

        $datos = $this->modelo->insert_new_comision($arr_sesion);
        echo json_encode($datos);
    }

    function buscarComisionUsuarios() {

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_comision_usuarios($arr_sesion);
        echo json_encode($datos);
    }

    function ListarUsuarioSistema() {

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_listar_usuario_comision_cbx($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
