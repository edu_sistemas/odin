<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  
 * Fecha creacion:  2017-03-07 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar_comercial extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar_comercial';
    private $nombre_item_plural = 'Listar_comercial';
    private $package = 'back_office/Code_sence';
    private $model = 'Code_sence_model';
    private $view = 'Listar_comercial_v';
    private $controller = 'Listar_comercial';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');



        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );



        // array con los css necesarios para la pagina
        //   <link href="css/plugins/select2/select2.min.css" rel="stylesheet">
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Code_sence/listar_comercial.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Sence E. Comercial";
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_empresa($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function senceListar() {

        $txt_nombre = $this->input->post('txt_nombre');
        $txt_codigo = $this->input->post('txt_codigo');
        $cbx_modalidad = $this->input->post('cbx_modalidad');
        $cbx_version = $this->input->post('cbx_version');
        $cbx_nivel = $this->input->post('cbx_nivel');

        //  variables de sesion

        $arr_sesion = array(
            'codigo' => $txt_codigo,
            'nombre' => $txt_nombre,
            'modalidad' => $cbx_modalidad,
            'version' => $cbx_version,
            'nivel' => $cbx_nivel,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_code_buscar_comercial($arr_sesion);
        echo json_encode($datos);
    }

    function senceListarAlerta() {

        //  variables de sesion

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_sence_alerta($arr_sesion);
        echo json_encode($datos);
    }
    
    function getSenceCbx() {

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_sence_cbx($arr_sesion);
        echo json_encode($datos);
        //MODALIDAD
    }

    function getNivelCbx() {

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_nivel_cbx($arr_sesion);
        echo json_encode($datos);
    }
 
    function getVerCbx() {

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_version_cbx($arr_sesion);
        echo json_encode($datos);
    }

}
