<?php

/**
 * Editar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-12 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-12-12 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar';
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Code_sence';
    private $curso = 'back_office/Curso/Curso_model';
    private $modalidad = 'back_office/Modalidad/Modalidad_model';
    private $model = 'Code_sence_model';
    private $view = 'Editar_v';
    private $controller = 'Editar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->curso, 'modelcurso');
        $this->load->model($this->modalidad, 'modalidadmodel');

        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('Functions');
    }

    // 2016-10-11
    // Controlador del Editar Perfil
    public function index($id_perfil = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_perfil == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Editar',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Code_sence/editar.js')
        );



        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Sence";



        $arr_input_data = array(
            'id' => $id_perfil,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_code_sence'] = $this->modelo->get_arr_code_sence_by_id($arr_input_data);
        if ($data['data_code_sence'] == false) {
            redirect($this->package . '/Listar', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //   Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function EditarCodeSence() {
        /*
          Ultima fecha Modificacion: 2016-12-12
          Descripcion:  función para modificar los code_sence
          Usuario Actualizador : Marcelo Romero
         */
        $funciones = new Functions();
        $arr_input_data = array(
            'id_code_sence' => $this->input->post('id_code_sence'),
            'nombre_curso_code_sence' => trim($this->input->post('nombre_curso_code_sence')),
            'cantidad_hora_sence' => $this->input->post('cantidad_hora_sence'),
            'valor_hora_sence' => $this->input->post('valor_hora_sence'),
            'fecha_solicitud' => $funciones->formatDateBD($this->input->post('fecha_solicitud')),
            'fecha_acreditacion' => $funciones->formatDateBD($this->input->post('fecha_acreditacion')),
            'fecha_vigencia' => $funciones->formatDateBD($this->input->post('fecha_vigencia')),
            'id_otec' => $this->input->post('otec'),
            'id_modalidad_sence' => $this->input->post('id_modalidad_sence'),
            'resolucion_excenta' => $this->input->post('resolucion_excenta'),
            'valor_efectivo_participante' => $this->input->post('valor_efectivo_participante'),
            'valor_total_curso' => $this->input->post('valor_total_curso')
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_editar_code_sence($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function getCurso() {
        // carga el cbx de modalidad



        $arr_input_data = array(
            'id_modalidad' => $this->input->post('id_modalidad_sence'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


//        var_dump($arr_input_data);
//        die();
//        
        $datos = $this->modelcurso->get_arr_select_curso_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function getModalidad() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modalidadmodel->get_arr_modalidad_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getOtec() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modalidadmodel->get_arr_otec_cbx($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file Editar.php */
/* Location: ./application/controllers/Editar.php */
?>
