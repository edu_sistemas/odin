<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-07 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-12-07 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Code_sence';
    private $modelcurso = 'back_office/Curso/Curso_model';
    private $modelmodalidad = 'back_office/Modalidad/Modalidad_model';
    private $model = 'Code_sence_model';
    private $view = 'Agregar_v';
    private $controller = 'Agregar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->modelcurso, 'modelCurso');
        $this->load->model($this->modelmodalidad, 'modelModalidad');

        //  Libreria de sesion
        $this->load->library('session');

        $this->load->library('Functions');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Code_sence/agregar.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Sence";
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;



        $this->load->view('amelia_1/template', $data);
    }

    function AgregarSenceCode() {

        $funciones = new Functions();

        $name = $_FILES['file']['name'];
        $tname = $_FILES['file']['tmp_name'];


        $nombre_archivo = "";
        $estado = "";
        $mensaje = "";
        $respuesta = "";
        // verifica si el archivo se puede subir
        if (is_uploaded_file($tname)) {

            $splitted = explode(".", $name);
            $reversed = array_reverse($splitted);
            $extension = $reversed[0];

            //  borra posicion de un array
            unset($reversed[0]);

            $dereched = array_reverse($reversed);
            // une string separado
            $nombre_archivo = implode("", $dereched);
            $nombre_con_fecha = $nombre_archivo .'_'. date('dmYHis') . '.' . $extension;

            $dir_subida = 'sencedoc/';
            $fichero_subido = $dir_subida . basename($nombre_con_fecha);
            //copy =  copia el archivo de la ruta temporal a la ruta del servidor
            if (copy($tname, $fichero_subido)) {

                $arr_input_data = array(
                    'codigo_sence' => $this->input->post('codigo_sence'),
                    'fecha_solicitud' => $funciones->formatDateBD($this->input->post('fecha_solicitud')),
                    'fecha_vigencia' => $funciones->formatDateBD($this->input->post('fecha_vigencia')),
                    'estado_code_sence' => $this->input->post('estado_code_sence'),
                    'nombre_curso_code_sence' => $this->input->post('nombre_curso_code_sence'),
                    'cantidad_hora_sence' => $this->input->post('cantidad_hora_sence'),
                    'valor_hora_sence' => $this->input->post('valor_hora_sence'),
                    'id_modalidad_sence' => $this->input->post('id_modalidad_sence'),
                    'fecha_acreditacion' => $funciones->formatDateBD($this->input->post('fecha_acreditacion')),
                    'resolucion_excenta' => $this->input->post('resolucion_excenta'),
                    'valor_efectivo_participante' => $this->input->post('valor_efectivo_participante'),
                    'valor_total_curso' => $this->input->post('valor_total_curso'),
                    'nombre_archivo' => $nombre_con_fecha,
                    'ruta_archivo' => $dir_subida,
                    'otec' => $this->input->post('otec')
                );

                $arr_input_data['user_id'] = $this->session->userdata('id_user');
                $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

                $respuesta = $this->modelo->set_arr_agregar_code_sence($arr_input_data);

                $nombre_archivo = $nombre_con_fecha;
                $estado = "ok";
                $mensaje = "Archivo Subido";
            } else {
                $nombre_archivo = $nombre_con_fecha;
                $estado = "Error";
                $mensaje = "Error al Subir Archivo" . $nombre_archivo;
            }
        } else {
            $nombre_archivo = $name;
            $estado = "Error";
            $mensaje = "Error el archivo que intenta subir no es compatible con el sistema" . $nombre_archivo;
        }
//        $arr_datos = array('archivo' => $nombre_archivo, 'status' => $estado, 'mensaje' => $mensaje );
        echo json_encode($respuesta);
    }

    function getModalidadCBX() {
        // carga el combo box id modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelModalidad->get_arr_modalidad_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getOtecCBX() {
        // carga el combo box id modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelModalidad->get_arr_otec_cbx($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
