<?php

/**
 * Gestion_Permiso
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion_Permiso extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Gestion_permiso';
    private $nombre_item_plural = 'Gestion_permiso';
    private $package = 'back_office/Permiso';
    private $package2 = 'back_office/Perfil';
    private $model = 'Permiso_model';
    private $model2 = 'Perfil_model';
    private $view = 'Gestion_Permiso_v';
    private $controller = 'Gestion_Permiso';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'modeloperfil');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id = false) {

        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package2 . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package2 . '/',
                'label' => 'Permisos',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/toastr/toastr.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dualListbox/bootstrap-duallistbox.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/toastr/toastr.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Permiso/permiso_gestion.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Perfil";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }


        $arr_input_data = array(
            'id' => $id,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        // Procedimiento de alm,acenado listar !!!
        $data['contenido'] = $this->modeloperfil->get_arr_perfil_by_id($arr_input_data);
        
         if ($data['contenido'] == false) {
            redirect($this->package2 . '/Listar', 'refresh');
        }

        //  $data['perfil_items']        =  $this->modelo->get_arr_listar_permiso_by_id($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listarItemsNoOtorgados() {

        /*
          Ultima fecha Modificacion: 2016-11-11
          Descripcion:  Metodo para Cargar Los Items que no tiene asociado el perfil
          Usuario Actualizador : Luis Jarpa
         */

        $arr_input_data = array(
            'id' => $this->input->post('id'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        // llamado al modelo
        $respuesta = $this->modelo->get_arr_listar_permiso_lbx($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function listarItemsOtorgados() {

        /*
          Ultima fecha Modificacion: 2016-11-11
          Descripcion:  Metodo para Cargar Los Items que no tiene asociado el perfil
          Usuario Actualizador : Luis Jarpa
         */

        $arr_input_data = array(
            'id' => $this->input->post('id'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        // llamado al modelo
        $respuesta = $this->modelo->get_arr_listar_permiso_perfil($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function OtorgarPermisosPerfil() {

        /*
          Ultima fecha Modificacion: 2016-11-09
          Descripcion:  Metodo para dar permisos a los perfiles
          Usuario Actualizador : Marcelo Romero
         */
        $respuesta = "";
        $id_perfil = $this->input->post('id_perfil');
        $arr_ = $this->input->post('cbx_permisos_otorgados_visible');

        for ($i = 0; $i < count($arr_); $i++) {
            $arr_input_data = array(
                'fk_id_perfil' => $id_perfil,
                'fk_id_item' => $arr_[$i]
            );
            $arr_input_data['user_id'] = $this->session->userdata('id_user');
            $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
            $respuesta = $this->modelo->set_arr_otorgar_permiso($arr_input_data);
        }
        echo json_encode($respuesta);
    }

    function QuitarPermisosPerfil() {

        /*
          Ultima fecha Modificacion: 2016-11-09
          Descripcion:  Metodo para dar permisos a los perfiles
          Usuario Actualizador : Marcelo Romero
         */

        $arr_ = $this->input->post('cbx_permisos_no_otorgados_visible');
        $id_perfil = $this->input->post('id_perfil');

        for ($i = 0; $i < count($arr_); $i++) {
            $arr_input_data = array(
                'fk_id_perfil' => $id_perfil,
                'fk_id_item' => $arr_[$i]
            );

            $arr_input_data['user_id'] = $this->session->userdata('id_user');
            $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
            $respuesta = $this->modelo->set_arr_quitar_permiso($arr_input_data);
        }

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_quitar_permiso($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

}

/* End of file Gestion_Permiso.php */
/* Location: ./application/controllers/Gestion_Permiso.php */
?>
