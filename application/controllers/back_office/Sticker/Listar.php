<?php
/**
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  
 * Fecha creacion:  2017-03-16 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Listar extends MY_Controller {

	// Variables paramétricas

	private $nombre_item_singular = 'Sticker';
	private $nombre_item_plural = 'Sticker';
	private $package = 'back_office/Sticker';
	private $model = 'Sticker_model';
	private $view = 'Listar_v';
	private $controller = 'Listar';
	private $ind = '';

	function __construct() {
		parent::__construct();
		$this->load->model($this->package . '/' . $this->model, 'modelo');
	}

	public function index() {

		// / array ubicaciones

		$arr_page_breadcrumb = array(
			array(
				'href' => base_url() . $this->ind . 'back_office' . '/Home',
				'label' => 'Home',
				'icono' => ''
			),
			array(
				'href' => base_url() . $this->ind . $this->package . '/InformeDistancia',
				'label' => 'Informe Curso  E-learning',
				'icono' => ''
			)
		);
		// array con los css necesarios para la pagina

		$arr_theme_css_files = array(
			array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
			array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
		);
		// js necesarios para la pagina

		$arr_theme_js_files = array(
			array('src' => base_url() . 'assets/amelia/js/plugins/JsPdf/jspdf.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/JsPdf/dist/jspdf.debug.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/Sticker/sticker.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js')
		);
		// informacion adicional para la pagina

		$data['page_title'] = '';
		$data['page_title_small'] = '';
		$data['panel_title'] = $this->nombre_item_singular;
		$data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
		$data['script_adicional'] = $arr_theme_js_files;
		$data['style_adicional'] = $arr_theme_css_files;

		// //**** Obligatorio ****** //////

		$datos_menu['user_id'] = $this->session->userdata('id_user');
		$datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
		$data['id_perfil'] = $this->session->userdata('id_perfil');
		$data['activo'] = "Informe E-learning";
		$data['menus'] = $this->session->userdata('menu_usuario');

		// / fin carga menu
		// $data ['fichas'] = $this->modelo->getFichas ();

		$data['id_perfil'] = $this->session->userdata('id_perfil');
		$data['page_menu'] = 'dashboard';
		$data['main_content'] = $this->package . '/' . $this->view;
		$this->load->view('amelia_1/template', $data);
	}

	function fichas() {
		$arr_data = array(
			'num_ficha' => $this->input->post('num_ficha'),
			'empresa' => '0',
			'user_id' => $this->session->userdata('id_user'),
			'user_perfil' => $this->session->userdata('id_perfil')
		);
		$datos = $this->modelo->getFichas($arr_data);
		echo json_encode($datos);
	}

	function getEmpresasCBX() {
		$arr_data = array(
			'user_id' => $this->session->userdata('id_user'),
			'user_perfil' => $this->session->userdata('id_perfil')
		);
		$datos = $this->modelo->get_sp_empresa_select_cbx_sticker($arr_data);
		echo json_encode($datos);
	}

	public function generarStickerJS(){
		$id_ficha = $this->input->post('idFicha');

		$arr_datos = array(
			'id_ficha' => $id_ficha,
			'user_id' => $this->session->userdata('id_user'),
			'user_perfil' => $this->session->userdata('id_perfil')
		);

		// $dataInforme = $this->modelo->getDatosInformeElearning($arr_datos);
		$dataAlumnos = $this->modelo->getAlumnosStick($arr_datos);

		echo json_encode($dataAlumnos);
	}

	public function generarSticker() {
		$id_ficha = $this->uri->segment(5);

		$arr_datos = array(
			'id_ficha' => $id_ficha,
			'user_id' => $this->session->userdata('id_user'),
			'user_perfil' => $this->session->userdata('id_perfil')
		);

		// $dataInforme = $this->modelo->getDatosInformeElearning($arr_datos);
		$dataAlumnos = $this->modelo->getAlumnosStick($arr_datos);

		$this->load->library('Pdf');
		$pdf = new pdf('P', 'mm', 'A4', true, 'UTF-8', false, false);
		// remove default header/footer
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		// set margins
		$pdf->SetMargins(10, 10, 22, true);

		// set auto page breaks false
		$pdf->SetAutoPageBreak(false, 15.5);

		//bloque para todos los alumnos
		$pdf->SetFont('Helvetica', '', 14, '', true);
		$pdf->AddPage('P');

		$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

		$html = '';
		$pdf->setCellPaddings(11, 11, 10, 10);
		$pdf->SetLineStyle(array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(255, 255, 255)));
		$Add = 14;

		for ($i = 0; $i < count($dataAlumnos); $i++) {

			if ($i == $Add) {
				$sum1 = $Add;
				$sum2 = 14;
				
				$Add = $sum1 + $sum2;
				$pdf->AddPage('P', 'A4');
			}
			if ($i % 2 == 0) {
				$pdf->MultiCell(97, 30, 'Linares Y Compañia' . "\n" . $dataAlumnos[$i]["nombre"] . "\n" . 'Santiago fono: 24398800 ', 1, 'L', $i, 0, '', '', true);
			} else {
				$pdf->MultiCell(97, 30, 'Linares Y Compañia' . "\n" . $dataAlumnos[$i]["nombre"] . "\n" . 'Santiago fono: 24398800', 1, 'L', $i, 1, '', '', true);
			}
		}
		$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
		//$pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);
		// ---------------------------------------------------------
		// Cerrar el documento PDF y preparamos la salida
		// Este método tiene varias opciones, consulte la documentación para más información.

		$nombre_archivo = utf8_decode("Localidades de " . $id_ficha . ".pdf");
		$pdf->Output($nombre_archivo, 'I');
	}

}
