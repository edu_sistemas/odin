<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-29 [Luis jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Detalle extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Detalle';
    private $nombre_item_plural = 'Detalles';
    private $package = 'back_office/Empresa';
    private $holding = 'back_office/Holding/Holding_Model';
    private $comuna = 'back_office/Comuna/Comuna_model';
    private $model = 'Empresa_model';
    private $view = 'Detalle_v';
    private $controller = 'Detalle';
    private $ind = '';
    private $package2 = 'back_office/Empresa_contacto';
    private $model2 = 'Empresa_contacto_model';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'contacto');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->comuna, 'modelcomuna');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_empresa = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_empresa == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Detalle',
                'label' => 'Detalle',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/cropper/cropper.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/Chart.js/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Empresa/detalle.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Empresas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $arr_input_data = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_empresa'] = $this->modelo->get_arr_empresa_by_id($arr_input_data);

        if ($data['data_empresa'] == false) {
            redirect($this->package . '/Listar', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function verDatosContacto() {


        $id_empresa = $this->input->post('id_empresa');

        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->get_arr_empresa_contact($arr_datos);

        echo json_encode($data);
    }

    function verDatosDireccion() {


        $id_empresa = $this->input->post('id_empresa');

        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->get_arr_empresa_direccion($arr_datos);

        echo json_encode($data);
    }

    function verVentasEmpresa() {


        $id_empresa = $this->input->post('id_empresa');

        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_empresa_venta($arr_datos);

        echo json_encode($data);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
