<?php

/**
 * Carga
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-02 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-10-17 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Carga extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Carga';
    private $nombre_item_plural = 'Carga';
    private $package = 'back_office/Empresa';
    private $model = 'Empresa_model';
    private $view = 'Carga_v';
    private $controller = 'Carga';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Carga',
                'label' => 'Carga',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/ladda/ladda-themeless.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css')
        );



        //  js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            //   <!-- Ladda -->
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/spin.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.jquery.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Empresa/carga.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Empresas";
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        //*********-------------Cargando el contenido de la tabla temporal .........................

        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $this->load->view('amelia_1/template', $data);
    }

    function cargar_archivo_datos_empresa() {

        /* aumento el tamañao de espera del servidor   */

        $arr_datos = array();
        $user_id = $this->session->userdata('id_user');
        $user_perfil = $this->session->userdata('id_perfil');


        $name = $_FILES['file_carga_empresa']['name'];
        $tname = $_FILES['file_carga_empresa']['tmp_name'];

 
        if ($name != "" ) {

            $obj_excel = PHPExcel_IOFactory::load($tname);
            $sheetData = $obj_excel->getActiveSheet()->toArray(null, true, true, true);

            $arr_datos = array();
            foreach ($sheetData as $index => $value) {
                if ($index != 1) {


                    if (trim($value['A']) != "") {

                        $arr_datos = array(
                            'rut_empresa' => $value['A'],
                            'razon_social' => $value['B'],
                            'giro' => $value['C'],
                            'direccion' => $value['D'],
                            'descripcion' => $value['E'],
                            'user_id' => $user_id,
                            'user_perfil' => $user_perfil
                        );

                        $this->modelo->get_arr_subir_emrpesa($arr_datos);
                    } else {

                        break;
                    }
                }
            }

            $result['valido'] = true;
            $result['mensaje'] = 'Empresas importados correctamente';
        } else {

            $result['valido'] = false;
            $result['mensaje'] = 'Error al procesar el archivo de carga';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    function listar_carga_empresa() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $resultado = $this->modelo->cargar_empresas_tbl_temporal($datos_menu);

        echo json_encode($resultado);
    }

    function limpiar_carga_anterior() {

        $datos['user_id'] = $this->session->userdata('id_user');
        $datos['user_perfil'] = $this->session->userdata('id_perfil');
        $resultado = $this->modelo->get_eliminar_empresas_temporal($datos);
        echo json_encode($resultado);
    }

    function aceptar_carga_empresa() {

        // Ultima fecha Modificacion: 2016-10-13
        // Descripcion:  Metodo para registrar La empresa despues de haberla extraido de la tabla temporal de empresa
        // Usuario Actualizador : Marcelo Romero
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_agregar_final_emrpesa($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function cancelar_carga_empresa() {

        // Ultima fecha Modificacion: 2016-10-13
        // Descripcion:  Metodo para eliminar la tabla temporal 
        // Usuario Actualizador : Luis Jarpa
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->get_eliminar_empresas_temporal($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

}

/* End of file CargaMasiva.php */
/* Location: ./application/controllers/CargaMasiva.php */
?>
