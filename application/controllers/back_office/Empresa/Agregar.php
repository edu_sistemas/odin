<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Empresa';
    private $holding = 'back_office/Holding/Holding_Model';
    private $comuna = 'back_office/Comuna/Comuna_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $model = 'Empresa_model';
    private $view = 'Agregar_v';
    private $controller = 'Agregar';
    private $ind = '';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->comuna, 'modelcomuna');
        $this->load->model($this->usuario, 'modelusuario');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Empresa/agregar.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Empresas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function Registra_empresa() {

        $requiere_orden_compra = $this->input->post('requiere_orden_compra');
        $requiere_hess = $this->input->post('requiere_hess');
        $requiere_numero_contrato = $this->input->post('requiere_numero_contrato');
        $requiere_ota = $this->input->post('requiere_ota');

        $asigna_holding = $this->input->post('holding');

        if ($requiere_orden_compra == NULL) {
            $requiere_orden_compra = '0';
        }
        if ($requiere_hess == NULL) {
            $requiere_hess = '0';
        }
        if ($requiere_numero_contrato == NULL) {
            $requiere_numero_contrato = '0';
        }
        if ($requiere_ota == NULL) {
            $requiere_ota = '0';
        }
        
        if ($asigna_holding == NULL) {
            $asigna_holding = 1;
        }

        $arr_input_data = array(
            'rut' => ($this->input->post('rut') . '-' . $this->input->post('dv')),
            'nombre_fantasia' => $this->input->post('nombre_fantasia'),
            'razon' => $this->input->post('razon'),
            'holding' => $asigna_holding,
            'descripcion' => $this->input->post('descripcion'),
            'giro' => $this->input->post('giro'),
            'direccion' => $this->input->post('direccion'),
            'comuna' => $this->input->post('comuna'),
            'telefono_temporal' => $this->input->post('telefono_temporal'),
            'direccion_factura' => $this->input->post('direccion_factura'),
            'comuna_factura' => $this->input->post('comuna_factura'),
            'direccion_despacho' => $this->input->post('direccion_despacho'),
            'mail_sii' => $this->input->post('mail_sii'),
            'nombre_contacto_cobranza' => $this->input->post('nombre_contacto_cobranza'),
            'mail_contacto_cobranza' => $this->input->post('mail_contacto_cobranza'),
            'telefono_contacto_cobranza' => $this->input->post('telefono_contacto_cobranza'),
            'ejecutivo' => $this->input->post('ejecutivo'),
            'observaciones_glosa_facturacion' => $this->input->post('observaciones_glosa_facturacion'),
            'requiere_orden_compra' => $requiere_orden_compra,
            'requiere_hess' => $requiere_hess,
            'requiere_numero_contrato' => $requiere_numero_contrato,
            'requiere_ota' => $requiere_ota,
            'logo' => $this->input->post('logo')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_agregar_emrpesa($arr_input_data);

        echo json_encode($respuesta);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getComunasCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelcomuna->get_arr_listar_comuna_cbx($arr_sesion);
        echo json_encode($datos);
    }

     function getEjecutivosCBX() {
       
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $datos = $this->modelusuario->get_arr_ejecutivo_comercial($arr_sesion);
            
            echo json_encode($datos);
            //var_dump('user_perfil');
            //die();
        
    }
   

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
