<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Empresa';
    private $model = 'Empresa_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';
    private $package2 = 'back_office/Empresa_contacto';
    private $model2 = 'Empresa_contacto_model';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'contacto');
        $this->load->model($this->holding, 'modelholding');


        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );




        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            //
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Empresa/listar.js'),
            array('src' => 'https://maps.googleapis.com/maps/api/js?key=AIzaSyD6MwS9aPQtsx3SIDr7e9JyT0GdBv1vtOY')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Empresas";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_empresa($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
       // var_dump($data);
        $this->load->view('amelia_1/template', $data);
    }

    function buscarEmpresa() {

        $rut_empresa = $this->input->post('txt_rut_empresa');
        $nombre_empresa = $this->input->post('txt_nombre_empresa');
        $id_holding = $this->input->post('cbx_holding');

        //  variables de sesion

        $arr_sesion = array(
            'rut_empresa' => $rut_empresa,
            'nombre_empresa' => $nombre_empresa,
            'id_holding' => $id_holding,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_empresa($arr_sesion);
        echo json_encode($datos);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getTipoContactoCBX() {
        // carga el combo box Tipo Contacto
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->contacto->get_arr_listar_TipoContacto_cbx($arr_sesion);
        echo json_encode($datos);
    }

      function getNivelContactoCBX() {
        // carga el combo box Nivel Contacto
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->contacto->get_arr_listar_nivel_contacto_cbx($arr_sesion);
        echo json_encode($datos);
    }

    

    // procesos de los contactos empresa



    function getContactosEmpresa() {


        $id_empresa = $this->input->post('id_empresa');

        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->get_arr_empresa_contact($arr_datos);

        echo json_encode($data);
    }

    function getContactosEmpresax() {


        $id_empresa = $this->input->post('id_empresa');

        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->get_arr_empresa_direccion($arr_datos);

        echo json_encode($data);
    }

    function guardarDatosContacto() {

        $arr_datos = array(
            'id_empresa' => $this->input->post('id_empresa'),
            'nombre_contacto_empresa' => $this->input->post('nombre_contacto_empresa'),
            'tipo_contacto_empresa' => $this->input->post('tipo_contacto_empresa'),
            'cargo_contacto_empresa' => $this->input->post('cargo_contacto_empresa'),
            'departamento_contacto_empresa' => $this->input->post('departamento_contacto_empresa'),
            'nivel_contacto_empresa' => $this->input->post('nivel_contacto_empresa'),
            'correo_contacto_empresa' => $this->input->post('correo_contacto_empresa'),
            'telefono_contacto_empresa' => $this->input->post('telefono_contacto_empresa'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_arr_empresa_contact($arr_datos);

        echo json_encode($data);
    }

    function desactivarDatosContacto() {


        $id_contacto_empresa = $this->input->post('id_contacto');
        $estado = $this->input->post('estado');


        $arr_datos = array(
            'id_contacto_empresa' => $id_contacto_empresa,
            'estado' => $estado,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_status_empresa_contact($arr_datos);

        echo json_encode($data);
    }

    function actualzarDatosContacto() {
        $id_contacto_empresa = $this->input->post('txt_id_contacto_empresa_edit');       
        $nombre_contacto = $this->input->post('txt_nombre_contacto_edit');
        $id_tipo_contacto_empresa = $this->input->post('tipo_contacto_cbx_edit');
        $cargo_contacto = $this->input->post('txt_cargo_contacto_edit');
        $departamento_contacto = $this->input->post('txt_departamento_contacto_edit');
        $id_nivel_contacto = $this->input->post('nivel_contacto_cbx_edit');
        $correo_contacto = $this->input->post('txt_correo_contacto_edit');
        $telefono_contacto = $this->input->post('txt_telefono_edit');

        $arr_datos = array(
            'id_contacto_empresa' => $id_contacto_empresa,
            'nombre_contacto' => $nombre_contacto,
            'id_tipo_contacto_empresa' => $id_tipo_contacto_empresa,
            'cargo_contacto' => $cargo_contacto,
            'departamento_contacto' => $departamento_contacto,
            'id_nivel_contacto' => $id_nivel_contacto,
            'correo_contacto' => $correo_contacto,
            'telefono_contacto' => $telefono_contacto,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_update_empresa_contact($arr_datos);

        echo json_encode($data);
    }

    function eliminarDatosContacto() {

        $id_contacto_empresa = $this->input->post('id_contacto_empresa');
        $id_empresa = $this->input->post('id_empresa');
        $nombre_contacto = $this->input->post('nombre_contacto');
        $id_tipo_contacto_empresa = $this->input->post('id_tipo_contacto_empresa');
        $cargo_contacto = $this->input->post('cargo_contacto');
        $departamento_contacto = $this->input->post('departamento_contacto');
        $id_nivel_contacto = $this->input->post('id_nivel_contacto');
        $correo_contacto = $this->input->post('correo_contacto');
        $telefono_contacto = $this->input->post('telefono_contacto');

        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'id_contacto_empresa' => $id_contacto_empresa,
            'nombre_contacto' => $nombre_contacto,
            'id_tipo_contacto_empresa' => $id_tipo_contacto_empresa,
            'cargo_contacto' => $cargo_contacto,
            'departamento_contacto' => $departamento_contacto,
            'id_nivel_contacto' => $id_nivel_contacto,
            'correo_contacto' => $correo_contacto,
            'telefono_contacto' => $telefono_contacto,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_delete_empresa_contact($arr_datos);

        echo json_encode($data);
    }

    function tipoEmpresa() {

        $arr_datos = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->get_arr_tipo_empresa($arr_datos);

        echo json_encode($data);
    }

    function agregarDireccion() {
        $direccion = $this->input->post('direccion');
        $tipo = $this->input->post('tipo');
        $fecha = $this->input->post('fecha');
        $id_empresa = $this->input->post('id_empresa');
        $arr_datos = array(
            'direccion' => $direccion,
            'tipo' => $tipo,
            'fecha' => $fecha,
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->set_direccion_empresa($arr_datos);

        echo json_encode($data);
    }

    function eliminarDireccion() {
        $id_empresa = $this->input->post('id_empresa');
        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $data = $this->contacto->delete_direccion_empresa($arr_datos);
        echo json_encode($data);
    }

    function verVentasEmpresa() {


        $id_empresa = $this->input->post('id_empresa');

        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_empresa_venta($arr_datos);

        echo json_encode($data);
    }
    function verDatosDireccion() {
        $id_empresa = $this->input->post('id_empresa');
        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $data = $this->contacto->get_arr_empresa_direccion($arr_datos);
        echo json_encode($data);
    }

    function getContactoEmpresa() {


        $id_contacto_empresa = $this->input->post('id_contacto_empresa');

        $arr_datos = array(
            'id_contacto_empresa' => $id_contacto_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->get_arr_contacto_empresa($arr_datos);

        echo json_encode($data);
    }

    function actualizarContacto(){
        $arr_datos = array(
            'id_contacto' => $this->input->post('id_contacto'),
            'nombre_contacto_empresa' => $this->input->post('nombre_contacto_empresa_editar'),
            'tipo_contacto_empresa' => $this->input->post('tipo_contacto_empresa_editar'),
            'cargo_contacto_empresa' => $this->input->post('cargo_contacto_empresa_editar'),
            'departamento_contacto_empresa' => $this->input->post('departamento_contacto_empresa_editar'),
            'nivel_contacto_empresa' => $this->input->post('nivel_contacto_empresa_editar'),
            'correo_contacto_empresa' => $this->input->post('correo_contacto_empresa_editar'),
            'telefono_contacto_empresa' => $this->input->post('telefono_contacto_empresa_editar'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->contacto->update_empresa_contact($arr_datos);

        echo json_encode($data);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
