<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-29 [Luis jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Empresa';
    private $holding = 'back_office/Holding/Holding_Model';
    private $comuna = 'back_office/Comuna/Comuna_model';
    private $model = 'Empresa_model';
    private $view = 'Editar_v';
    private $controller = 'Editar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->comuna, 'modelcomuna');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_empresa = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_empresa == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Editar',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/cropper/cropper.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Empresa/editar.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Empresas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $arr_input_data = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_empresa'] = $this->modelo->get_arr_empresa_by_id($arr_input_data);

        if ($data['data_empresa'] == false) {
            redirect($this->package . '/Listar', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function EditarEmpresa() {
    	
        $requiere_orden_compra = $this->input->post('requiere_orden_compra');
        $requiere_hess = $this->input->post('requiere_hess');
        $requiere_numero_contrato = $this->input->post('requiere_numero_contrato');
        $requiere_ota = $this->input->post('requiere_ota');
         
        if( $requiere_orden_compra == NULL){
        	$requiere_orden_compra = '0';
        }
        if( $requiere_hess == NULL){
        	$requiere_hess = '0';
        }
        if( $requiere_numero_contrato == NULL){
        	$requiere_numero_contrato = '0';
        }
        if( $requiere_ota == NULL){
        	$requiere_ota = '0';
        }
        
        $arr_input_data = array(
        		'id_empresa' => trim($this->input->post('id_empresa')),
        		'rut' => ($this->input->post('rut').'-'.$this->input->post('dv')),
        		'nombre_fantasia' => $this->input->post('nombre_fantasia'),
        		'razon' => $this->input->post('razon'),
        		'holding' => $this->input->post('holding'),
        		'descripcion' => $this->input->post('descripcion'),
        		'giro' => $this->input->post('giro'),
        		'direccion' => $this->input->post('direccion'),
        		'comuna' => $this->input->post('comuna'),
        		'telefono_temporal' => $this->input->post('telefono_temporal'),
        		'direccion_factura' => $this->input->post('direccion_factura'),
        		'comuna_factura' => $this->input->post('comuna_factura'),
        		'direccion_despacho' => $this->input->post('direccion_despacho'),
        		'mail_sii' => $this->input->post('mail_sii'),
        		'nombre_contacto_cobranza' => $this->input->post('nombre_contacto_cobranza'),
        		'mail_contacto_cobranza' => $this->input->post('mail_contacto_cobranza'),
        		'telefono_contacto_cobranza' => $this->input->post('telefono_contacto_cobranza'),
        		'observaciones_glosa_facturacion' => $this->input->post('observaciones_glosa_facturacion'),
        
        		'requiere_orden_compra' => $requiere_orden_compra,
        		'requiere_hess' => $requiere_hess,
        		'requiere_numero_contrato' => $requiere_numero_contrato,
        		'requiere_ota' => $requiere_ota,
        		'logo' => $this->input->post('logo')
        );
        
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
        
        $respuesta = $this->modelo->set_arr_editar_empresa($arr_input_data);
        
        echo json_encode($respuesta);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }
    
    function getComunasCBX() {
    	// carga el combo box Holding
    	$arr_sesion = array(
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelcomuna->get_arr_listar_comuna_cbx($arr_sesion);
    	echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
