<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class PersoLibro extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'PersoLibro';
    private $nombre_item_plural = 'PersoLibro';
    private $package = 'back_office/Asistencia';
    private $model = 'PersoLibro_model';
    private $view = 'PersoLibro_v';
    private $controller = 'PersoLibro';
    private $ind = '';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
    }

    public function index() {
// / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/LibroClases',
                'label' => 'Libro de Clases Personalizado',
                'icono' => ''
            )
        );

// array con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

// js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Asistencia/persoLibro.js')
        );

// informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

// //**** Obligatorio ****** //////

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['activo'] = "Personalizar Libro";
        $data ['menus'] = $this->session->userdata('menu_usuario');
// / fin carga menu

        $data ['fichas'] = $this->modelo->getFichas();
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function buscarLibro() {

// variables de sesion
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getLibros($arr_data);
        echo json_encode($datos);
    }

    function ListarFichasPresenciales() {

// variables de sesion
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getFichasPresencial($arr_data);
        echo json_encode($datos);
    }

    function DatosFicha() {

// variables de sesion
        $arr_data = array(
            'id' => $this->input->post('id'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getDatosFicha($arr_data);
        echo json_encode($datos);
    }

    function insertPerso() {


        $arr_data = array(
            'name' => $this->input->post('nombre'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getIdPerso($arr_data);
        $fichas = $this->input->post('fichas');
        for ($i = 0; $i < count($fichas); $i++) {
            $arr_data2 = array(
                'idPerso' => $datos[0]['id'],
                'ficha' => $fichas[$i]['id_ficha'],
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $datos2 = $this->modelo->insertLibroFicha($arr_data2);
        }

        echo json_encode($datos2);
    }

    function getLibrosPersonalizadosByID() {

// variables de sesion
        $arr_data = array(
            'id_libro_personalizado' => $this->input->post('id_libro_personalizado'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_sp_select_libro_personalizado_by_id($arr_data);
        echo json_encode($datos);
    }

    function insertNewFicha() {
        $arr_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'id_libro' => $this->input->post('id_libro'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_ficha_by_id_libro($arr_data);
        echo json_encode($datos);
    }

    function deleteFichaLibro() {
        $arr_data = array(
            'id_ficha_libro' => $this->input->post('id_ficha_libro'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_sp_delete_ficha_libro($arr_data);
        echo json_encode($datos);
    }

    function deleteLibro() {
        $arr_data = array(
            'id_libro_personalizado' => $this->input->post('id_libro_personalizado'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_sp_delete_libro_personalizado($arr_data);
        echo json_encode($datos);
    }

    public function generar() {

//hay que arreglarsele
        $id_ficha = $this->uri->segment(5);

        $id_ficha = explode("-", $id_ficha);
        $GLOBALS['num_ficha'] = '';

        for ($i = 0; $i < count($id_ficha); $i++) {
            $arr_datos = array(
                'id_ficha' => $id_ficha[$i],
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

            $dataLibro[$i] = $this->modelo->getDatosLibro($arr_datos);
            $dataAlumnos[$i] = $this->modelo->getAlumnosLibro($arr_datos);
        }

        for ($i = 0; $i < count($dataLibro); $i++) {
            if ($i != 0) {
                $GLOBALS['num_ficha'] .= "-";
            }
            $GLOBALS['num_ficha'] .= $dataLibro [$i][0] ["num_ficha"];
        }

//$GLOBALS['num_ficha'] = $dataLibro [0] [0] ["num_ficha"]."-".$dataLibro [1] [0] ["num_ficha"];

        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Felipe Bulboa');
        $pdf->SetTitle('Libro de Clases');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(
            0,
            64,
            0
                ), $lc = array(
            0,
            64,
            128
        ));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// se pueden modificar en el archivo tcpdf_config.php de libraries/config
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// relación utilizada para ajustar la conversión de los píxeles
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
// ---------------------------------------------------------
// establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

// Establecer el tipo de letra
// Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
// Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('Helvetica', '', 14, '', true);

// Añadir una página
// Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L');

// fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));

// Establecemos el contenido para imprimir
// $ficha = $this->input->post('fichas');
// $fichas = $this->modelo->getFichas();
// foreach($fichas as $ficha)
// {
// $prov = $ficha['num_ficha'];
// }
// echo json_encode($dataLibro);
// preparamos y maquetamos el contenido a crear
        $html = '';
        $html .= '		
				<table width="100%">
					<tr>
						<th>
							<table border="1" cellpadding="4" cellspacing="0">
        						<tr>
									<td>RELATOR: ' . $dataLibro [0][0] ["nombre_relator"] . '</td>
        						</tr>
							</table>
						</th>
						<th></th>
						<th>
							<table border="1" cellpadding="4" cellspacing="0">
        						<tr>
									<td>FICHA: ' . $GLOBALS['num_ficha'] . '</td>
        						</tr>
							</table></th>
					</tr>					
				  </table>';
// $html = '';
// $html .= "<style type=text/css>";
// $html .= "th{color: #fff; font-weight: bold; background-color: #222}";
// $html .= "td{background-color: #AAC7E3; color: #fff}";
// $html .= "</style>";
// $html .= "<h2>Localidades de ".$prov."</h2><h4>Actualmente: ".count($ficha)." localidades</h4>";
// $html .= "<table width='100%'>";
// $html .= "<tr><th>Id localidad</th><th>Localidades</th></tr>";
// provincias es la respuesta de la función getProvinciasSeleccionadas($provincia) del modelo
// $html .= "</table>";
// Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Cell(0, 0, 'LIBRO DE CLASES', 1, 1, 'C', 0, '', 4);
        $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);
        $html2 = '</br>';
        $html2 = '<style>
			td{font-size:12px;}
			strong{font-size:13px;}
		</style>';
        $html2 .= '<table width="1160px" border="0">
					<tr>
						<td>
							<table width="100%" >
								<tr>
									<td><u><strong>RECOMENDACIONES PARA EL USO DEL LIBRO DE CLASES:</strong></u></td>									
								</tr>
								<tr>
									<td>1. En la primera clase el alumno debe llenar la hoja de datos.</td>
								</tr>
								<tr>
									<td>2. En la primera clase se debe notificar la inasistencia de un alumno.</td>
								</tr>
								<tr>
									<td>3. El profesor debe guardar el "Libro de Clases" en la carpeta correspondiente al horario del curso.</td>
								</tr>
								<tr>
									<td>4. El profesor no esta autorizado para suspender clases.</td>
								</tr>
								<tr>
									<td>5. El profesor no esta autorizado para cambiar horario, ni dias de ejecución de un curso.</td>
								</tr>
								<tr>
									<td>6. El profesor debe llevar al dia la asistencia de los alumnos y el contenido de las clases.</td>
								</tr>
								<tr>
									<td>7. Si el libro de clases no contiene el % de asistencia y las notas de los alumnos, éste no podrá ser pagado.</td>
								</tr>
								<tr>
									<td>8. El libro de clases no puede ser enmendado sin la correspondiente autorización.</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><u><strong>RECOMENDACIONES PARA LAS CLASES:</strong></u></td>
								</tr>
								<tr>
									<td>Solicito a usted que tenga MUY PRESENTES los siguientes puntos:</td>
								</tr>
								<tr>
									<td>9. En la primera clase se deben <u>entregar los manuales</u> del alumno</td>
								</tr>
								<tr>
									<td>10. El profesor debe llegar 15 minutos antes de que comience la clase para revisar los equipos.</td>
								</tr>
								<tr>
									<td>11. El profesor debe apagar el aire acondicionado y ademas solicitar a los alumnos apagar debidamente los equipos.</td>
								</tr>
								<tr>
									<td>12. Las clases no deben comenzar con un atraso superior a los 5 minutos.</td>
								</tr>
							</table>
						</td>
						<td width="10px"></td>
						<td>
							<table width="100%">
								<tr>
									<td>13. El relator debe <u>aclarar en la primera clase los objetivos del curso.</u> Para esto usted debet tener en sus manos el programa o contenido del curso antes de iniciar la clase.</td>
								</tr>
								<tr>
									<td>14. A los alumnos se les debe aclarar cuales van a ser los <u>criterios de evaluacion del curso.</u>(Nota, asistencia, apreciación, etc.)</td>
								</tr>
								<tr>
									<td>15. Los alumnos deben ingresar su e-mail en el libro de clases, esto es obligatorio, y en el caso de que no cuente con dirección propia, solicitar que ingrese la del supervisor.</td>
								</tr>
								<tr>
									<td>16. Por ningún motivo se debe cambiar el relator de un curso, esto sólo puede ser posible bajo los siguietes casos:</td>
								</tr>
								<tr>
									<td>a) Enfermedad (sólo reemplazo).</td>
								</tr>
								<tr>
									<td>b) Que los alumnos asistentes al curso soliciten el cambio de relator.</td>
								</tr>
								<tr>
									<td>17. Por ningún motivo, un relator nuevo puede comenzar a realizar clases sin haber tenido una previa inducción.</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><hr></td>
								</tr>
								<tr>
									<td style="text-align:center">Firma Relator</td>
								</tr>
								<tr>
									<td style="text-align:center"><img src="assets/img/edutecno.jpg" border="0" width="240"></td>
								</tr>
							</table>
						</td>
					</tr>
				  </table>
				';

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html2, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP2 = '';
        $htmlP2 .= '<table border="1" height="500px">
						<tr>
							<td>
								<table border="0">
									<tr>
										<td colspan="2" style="text-align:right">ID Sence: ';
        $last_item = end($dataLibro);
        foreach ($dataLibro as $key => $value) {
            if ($value == $last_item) {
                $htmlP2 .= $value[0]["id_sence"];
            } else {
                $htmlP2 .= $value[0]["id_sence"] . ', ';
            }
        }


        $htmlP2 .= '</td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2" style="text-align:center;"><u><strong>LIBRO DE CONTROL DE CLASES</strong></u></td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200px" style="text-align:right">NOMBRE OTEC: &nbsp;&nbsp;</td><td>&nbsp;&nbsp;<u>' . $dataLibro [0] [0] ["nombre_fantasia_otec"] . '</u></td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200px" style="text-align:right">NOMBRE ACTIVIDAD DE CAPACITACIÓN: &nbsp;&nbsp;</td><td>&nbsp;&nbsp;<u>' . $dataLibro [0] [0] ["nombre_curso"] . '</u></td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200px" style="text-align:right">CÓDIGO AUTORIZACIÓN POR SENCE: &nbsp;&nbsp;</td><td>&nbsp;&nbsp;<u>' . $dataLibro [0] [0] ["codigo_sence"] . '</u></td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200px" style="text-align:right">FECHA DE EJECUCIÓN: &nbsp;&nbsp;</td><td>Fecha Inicio:<u>' . $dataLibro [0] [0] ["fecha_inicio"] . '</u> &nbsp; Fecha T&eacute;rmino:<u>' . $dataLibro [0] [0] ["fecha_termino"] . '</u></td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200px" style="text-align:right">LUGAR DE EJECUCIÓN: &nbsp;&nbsp;</td><td>&nbsp;&nbsp;<u>' . $dataLibro [0] [0] ["nombre_sede"] . '</u></td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200px" style="text-align:right">HORARIO: &nbsp;&nbsp;</td><td>&nbsp;&nbsp;<u>' . $dataLibro [0] [0] ["hora_inicio"] . '</u> a <u>' . $dataLibro [0] [0] ["hora_termino"] . '</u></td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td width="200px" style="text-align:right">NOMBRE RELATOR(A): &nbsp;&nbsp;</td><td>&nbsp;&nbsp;<u>' . $dataLibro [0] [0] ["nombre_relator"] . '</u></td>
									</tr>
									<tr>
										<td>&nbsp;</td><td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="2">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP2, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP3 = '';
        $htmlP3 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $GLOBALS['num_ficha'] . '</td>	
									</tr>
									<tr>
										
										<td><strong>Instructor:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_relator"] . '</td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table border="0">
									<tr>
										<td style="font-size:10px;">Nombre del Curso:</td>
										<td colspan="3" class="contenido">' . $dataLibro [0] [0] ["nombre_curso"] . '</td>
										<td></td>
										<td colspan="6"><u><strong>Importante</strong></u>: Compruebe que su Nombre este bien escrito.</td>
									</tr>
									<tr>
										<td style="font-size:10px;">Códigos:</td>
										<td colspan="2" class="contenido">' . $dataLibro [0] [0] ["codigo_sence"] . '</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:10px;">N° de Horas:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["duracion_curso"] . '</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:10px;">Fecha de inicio:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["fecha_inicio"] . '</td>
										<td style="font-size:10px;">Hora Inicio:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["hora_inicio"] . '</td>
										<td style="font-size:10px;">Sala:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["sala"] . '</td>
										<td style="font-size:10px;">Manuales</td>
										<td><table border="1"><tr><td style="font-size:10px;width:25px;text-align:center">SI</td></tr></table></td><td><table border="1"><tr><td style="font-size:10px;width:25px;text-align:center">NO</td></tr></table></td><td><table border="1"><tr><td style="font-size:10px;width:50px;text-align:center">__/__/__</td></tr></table></td>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:10px;">Fecha de Término:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["fecha_termino"] . '</td>
										<td style="font-size:10px;">Hora Término:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["hora_termino"] . '</td>
										<td></td>
										<td></td>
										<td style="font-size:10px;">Diplomas</td>
										<td><table border="1"><tr><td style="font-size:10px;width:25px;text-align:center">SI</td></tr></table></td><td><table border="1"><tr><td style="font-size:10px;width:25px;text-align:center">NO</td></tr></table></td><td><table border="1"><tr><td style="font-size:10px;width:50px;text-align:center">__/__/__</td></tr></table></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td style="font-size:10px;">Días:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["dias"] . '</td>
										<td style="font-size:10px;">N° de alumnos:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["cant_alumno"] . '</td>
										<td style="font-size:10px;">Informe</td>
										<td><table border="1"><tr><td style="font-size:10px;width:25px;text-align:center">SI</td></tr></table></td><td><table border="1"><tr><td style="font-size:10px;width:25px;text-align:center">NO</td></tr></table></td><td><table border="1"><tr><td style="font-size:10px;width:50px;text-align:center">__/__/__</td></tr></table></td>
										<td>HOJA 1</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td><hr></td>
						</tr>
						<tr>
							<td>';
        $htmlP3 .= '
								<table border="1" style="padding-left: 5px;">
									<tr>
										<td style="font-size:12px;text-align:center">Nombre alumno</td>
										<td style="font-size:12px;text-align:center">Cédula de Identidad</td>
										<td style="font-size:12px;text-align:center">Nivel Escolaridad</td>
										<td style="font-size:12px;text-align:center">Empresa</td>
										<td style="font-size:12px;text-align:center">Cargo Desempeñado</td>
										<td style="font-size:12px;text-align:center">Firma</td>
										<td style="font-size:12px;text-align:center">Sence</td>
									</tr>';
// tr para bucle
        for ($x = 0; $x < count($dataAlumnos); $x++) {
            for ($i = 0; $i < count($dataAlumnos[$x]); $i ++) {
                $htmlP3 .= '
										<tr>
											<td style="font-size:11px;">' . $dataAlumnos [$x] [$i] ["nombre_alumno"] . '</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>';
            }
        }

// tr para bucle
        $htmlP3 .= '
								</table>
							</td>
						</tr>
					</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP3, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP4 = '';
        $htmlP4 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $GLOBALS['num_ficha'] . '</td>	
									</tr>
									<tr>
										
										<td><strong></strong></td>
										<td></td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table border="0">
									<tr>
										<td style="font-size:10px;">Nombre del Curso:</td>
										<td colspan="3" class="contenido">' . $dataLibro [0] [0] ["nombre_curso"] . '</td>
										<td></td>
										<td colspan="6"></td>
									</tr>
									<tr>
										<td style="font-size:10px;">Códigos:</td>
										<td colspan="2" class="contenido">' . $dataLibro [0] [0] ["codigo_sence"] . '</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:10px;">N° de Horas:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["duracion_curso"] . '</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:10px;">Fecha de inicio:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["fecha_inicio"] . '</td>
										<td style="font-size:10px;"></td>
										<td></td>
										<td style="font-size:10px;"></td>
										<td></td>
										<td style="font-size:10px;"></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:10px;">Fecha de Término:</td>
										<td class="contenido">' . $dataLibro [0] [0] ["fecha_termino"] . '</td>
										<td style="font-size:10px;"></td>
										<td></td>
										<td></td>
										<td></td>
										<td style="font-size:10px;"></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<td style="font-size:10px;"></td>
										<td></td>
										<td style="font-size:10px;"></td>
										<td></td>
										<td style="font-size:10px;"></td>
										<td></td>
										<td></td>
										<td></td>
										<td>HOJA 2</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td><hr></td>
						</tr>
						<tr>
							<td>';
        $htmlP4 .= '
								<table border="1" style="padding-left: 5px;">
									<tr>
										<td style="font-size:12px;text-align:center">Nombre alumno</td>
										<td style="font-size:12px;text-align:center">Teléfono trabajo</td>
										<td style="font-size:12px;text-align:center">Correo Electrónico</td>
										<td style="font-size:12px;text-align:center">Dirección Trabajo</td>
										<td style="font-size:12px;text-align:center">F. Nacimiento</td>										
									</tr>';
        for ($x = 0; $x < count($dataAlumnos); $x++) {
            for ($i = 0; $i < count($dataAlumnos[$x]); $i ++) {
                $htmlP4 .= '
										<tr>
											<td style="font-size:11px;">' . $dataAlumnos[$x] [$i] ["nombre_alumno"] . '</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>';
            }
        }
        $htmlP4 .= '
								</table>
							</td>
						</tr>
					</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP4, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        $pdf->AddPage('L');
        $htmlP5 = '';
        $htmlP5 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $GLOBALS['num_ficha'] . '</td>	
									</tr>
									<tr>
										
										<td><strong></strong></td>
										<td></td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						
						<tr>
							<td>';
        $htmlP5 .= '
								<table border="1" style="padding-left: 5px;">
									<tr>
										<td style="font-size:12px;text-align:center;width:150px;">Fecha</td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>	
										<td style="font-size:12px;text-align:center;width:70px;"></td>	
									</tr>									
									<tr>
										<td style="font-size:12px;text-align:center">Nombre alumno</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>										
									</tr>';
// tr para bucle
        for ($x = 0; $x < count($dataAlumnos); $x++) {
            for ($i = 0; $i < count($dataAlumnos[$x]); $i ++) {
                $htmlP5 .= '
									<tr>
										<td style="font-size:11px;">' . $dataAlumnos[$x] [$i] ["nombre_alumno"] . '</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>';
            }
        }
// tr para bucle
        $htmlP5 .= '
								</table>
							</td>
						</tr>
					</table>
										<br><br><br>
										<hr>
										<table style="font-size:75%;">
										<tr><td></td><td></td><td></td><td></td></tr>
										<tr><td><b>MES_______________</b></td><td><b>A&Ntilde;O_______________</b></td><td>NOTA 1:</td><td>NOTA 2:</td></tr>
										<tr><td></td><td></td><td>Presente: P <br>Ausente: A <br>Atrasado: AT</td><td>La asistencia deber&aacute; ser registrada como m&aacute;ximo a los 20 minutos del comienzo del horario formal de clases.</td></tr>
										</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP5, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        $pdf->AddPage('L');
        $htmlP6 = '';
        $htmlP6 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $GLOBALS['num_ficha'] . '</td>	
									</tr>
									<tr>
										
										<td><strong></strong></td>
										<td></td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						
						<tr>
							<td>';
        $htmlP6 .= '
								<table border="1" style="padding-left: 5px;">
									<tr>
										<td style="font-size:12px;text-align:center;width:150px;">Fecha</td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>
										<td style="font-size:12px;text-align:center;width:70px;"></td>	
										<td style="font-size:12px;text-align:center;width:70px;"></td>	
									</tr>									
									<tr>
										<td style="font-size:12px;text-align:center">Nombre alumno</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>
										<td style="font-size:12px;text-align:center">Asistencia</td>										
									</tr>';
// tr para bucle
        for ($x = 0; $x < count($dataAlumnos); $x++) {
            for ($i = 0; $i < count($dataAlumnos[$x]); $i ++) {
                $htmlP6 .= '
									<tr>
										<td style="font-size:11px;">' . $dataAlumnos [$x] [$i] ["nombre_alumno"] . '</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>';
            }
        }
// tr para bucle
        $htmlP6 .= '
								</table>
							</td>
						</tr>
					</table>
										
										<br><br><br>
										<hr>
										<table style="font-size:75%;">
										<tr><td></td><td></td><td></td><td></td></tr>
										<tr><td><b>MES_______________</b></td><td><b>A&Ntilde;O_______________</b></td><td>NOTA 1:</td><td>NOTA 2:</td></tr>
										<tr><td></td><td></td><td>Presente: P <br>Ausente: A <br>Atrasado: AT</td><td>La asistencia deber&aacute; ser registrada como m&aacute;ximo a los 20 minutos del comienzo del horario formal de clases.</td></tr>
										</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP6, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP7 = '';
        $htmlP7 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $GLOBALS['num_ficha'] . '</td>	
									</tr>
									<tr>
										
										<td><strong></strong></td>
										<td></td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						
						<tr>
							<td>';
        $htmlP7 .= '
								<table border="1">
									<tr>
										<td style="font-size:12px;text-align:center;width:100px;">Día de Clase</td>
										<td style="font-size:12px;text-align:center;width:240px;">Temas</td>
										<td style="font-size:12px;text-align:center;width:240px;">Actividades</td>
										<td style="font-size:12px;text-align:center;width:100px;">Hora Inicio</td>
										<td style="font-size:12px;text-align:center;width:100px;">Hora Término</td>
											
									</tr>									
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									';

        $htmlP7 .= '
								</table>
							</td>
						</tr>
					</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP7, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP8 = '';
        $htmlP8 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $GLOBALS['num_ficha'] . '</td>	
									</tr>
									<tr>
										
										<td><strong></strong></td>
										<td></td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						
						<tr>
							<td>';
        $htmlP8 .= '
								<table border="1">
									<tr>
										<td style="font-size:12px;text-align:center;width:100px;">Día de Clase</td>
										<td style="font-size:12px;text-align:center;width:240px;">Temas</td>
										<td style="font-size:12px;text-align:center;width:240px;">Actividades</td>
										<td style="font-size:12px;text-align:center;width:100px;">Hora Inicio</td>
										<td style="font-size:12px;text-align:center;width:100px;">Hora Término</td>
											
									</tr>									
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left">Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
										<td style="font-size:12px;text-align:center" rowspan="3"></td>
																				
									</tr>
									<tr>
										<td style="font-size:12px;text-align:left" rowspan="2">Firma:</td>
										
										
																				
									</tr>
									<tr>
										<td></td>
									</tr>
									';


        $htmlP8 .= '
								</table>
							</td>
						</tr>
					</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP8, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP9 = '';
        $htmlP9 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $GLOBALS['num_ficha'] . '</td>	
									</tr>
									<tr>
										
										<td><strong></strong></td>
										<td></td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						
						<tr>
							<td>';
        $htmlP9 .= '
								<table border="1" style="padding-left: 5px;">
									<tr>
										<td rowspan="2" style="font-size:12px;text-align:center;width:180px;">Nombre Alumno</td>
										<td rowspan="2" style="font-size:12px;text-align:center;width:50px;">Fecha:</td>
										<td rowspan="2" style="font-size:12px;text-align:center;width:50px;">Fecha:</td>
										<td rowspan="2" style="font-size:12px;text-align:center;width:50px;">Nota Final</td>
										<td rowspan="2" style="font-size:12px;text-align:center;width:50px;">Asist en %</td>										
										<td rowspan="2" style="font-size:12px;text-align:center;width:300px;">Observaciones</td>
										<td style="font-size:12px;text-align:center;width:100px;" colspan="2">Entrega Material</td>											
									</tr>									
									<tr>
										<td style="font-size:12px;text-align:center;width:35px;">Fecha</td>
										<td style="font-size:12px;text-align:center;width:65px;">Firma</td>
									</tr>
									';
// bucle para alumno
        for ($x = 0; $x < count($dataAlumnos); $x++) {
            for ($i = 0; $i < count($dataAlumnos[$x]); $i ++) {
                $htmlP9 .= '
										<tr>
											<td style="font-size:11px;">' . $dataAlumnos[$x] [$i] ["nombre_alumno"] . '</td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
										</tr>';
            }
        }
// bucle para alumno
        $htmlP9 .= '
								</table>
							</td>
						</tr>
					</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP9, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP10 = '';
        $htmlP10 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $dataLibro[0] [0] ["num_ficha"] . '</td>	
									</tr>
									<tr>
										
										<td><strong></strong></td>
										<td></td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						
						<tr>
							<td>';
        $htmlP10 .= '
								<table border="1">
									<tr>
										<td style="font-size:12px;text-align:left;width:100px;">Día de Clase</td>
										<td style="font-size:12px;text-align:center;width:680px;">Observaciones de la clase: Aquí se deben anotar los sucesos que pueden afectar la calidad de la clase.</td>																				
									</tr>
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>	
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>	
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>	
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>	
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>										
									';
        $htmlP10 .= '
								</table>
							</td>
						</tr>
					</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP10, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP11 = '';
        $htmlP11 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
				<table border="0" height="500px">
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						<tr>
							<td>
								<table>
									<tr>
										<td rowspan="2" style="font-size:20px" width="250px">LIBRO DE CLASES</td>
										<td><strong>Empresa:</strong></td>
										<td class="encabezado">' . $dataLibro [0] [0] ["nombre_empresa"] . '</td>
										<td><strong>Ficha:</strong></td>
										<td class="encabezado">' . $GLOBALS['num_ficha'] . '</td>	
									</tr>
									<tr>
										
										<td><strong></strong></td>
										<td></td>
										<td><strong></strong></td>
										<td></td>	
									</tr>
																		
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<hr>
							</td>
						</tr>
						
						<tr>
							<td>';
        $htmlP11 .= '
								<table border="1">
									<tr>
										<td style="font-size:12px;text-align:left;width:100px;">Día de Clase</td>
										<td style="font-size:12px;text-align:center;width:680px;">Observaciones de la clase: Aquí se deben anotar los sucesos que pueden afectar la calidad de la clase.</td>																				
									</tr>
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>	
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>	
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>	
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>	
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Clase:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/</td>
										<td rowspan="3"></td>
									</tr>
									<tr>
										<td rowspan="2">Firma:</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>										
									';
        $htmlP11 .= '
								</table>
							</td>
						</tr>
					</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP11, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->AddPage('L');
        $htmlP12 = '';
        $htmlP12 .= '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
				font-weight: bold;
				}
				</style>
			<table>				
				<tr>
					<td style="font-size:14px;text-align:center;">Cumplimiento Técnico - Administrativo</td>
				</tr>
				<tr>
					<td style="font-size:12px;text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table border="1">
							<tr>
								<td style="width:220px"></td>
								<td style="width:80px;font-size:12px;">Fecha</td>
								<td style="width:50px" colspan="2"></td>
								<td style="width:50px" colspan="2"></td>
								<td style="width:50px" colspan="2"></td>
								<td style="width:50px" colspan="2"></td>
								<td style="width:50px" colspan="2"></td>
								<td style="width:50px" colspan="2"></td>
								<td style="width:50px" colspan="2"></td>
								<td style="width:50px" colspan="2"></td>
								<td style="width:50px" colspan="2"></td>
								
							</tr>
							<tr>
								<td></td>
								<td style="font-size:12px;">Sala</td>
								<td colspan="2"></td>
								<td colspan="2"></td>
								<td colspan="2"></td>
								<td colspan="2"></td>
								<td colspan="2"></td>
								<td colspan="2"></td>
								<td colspan="2"></td>	
								<td colspan="2"></td>	
								<td colspan="2"></td>	
															
							</tr>
							<tr>
								<td colspan="2" style="font-size:12px;">De los Computadores</td>								
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
								<td style="text-align:center;font-size:12px;">Si</td>
								<td style="text-align:center;font-size:12px;">No</td>
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">1. Estaban todos al inicio del curso, con el software adecuado y se iniciaron en forma normal.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">2. Si durante el desarrollo del curso tuvo problemas, fue solucionado por el Departamento técnico oportunamente.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>							
						</table>
					</td>
				</tr>
				<tr>
					<td style="font-size:12px;text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table border="1">
							
							<tr>
								<td  style="width:300px;font-size:12px;" colspan="2">De la sala</td>								
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">1. Estaba limpia apta para hacer clases (mobiliario y orden)</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">2. Mantiene la misma sala.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">3. La ventilación era la adecuada.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>								
						</table>
					</td>
				</tr>
				<tr>
					<td style="font-size:12px;text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table border="1">
							
							<tr>
								<td  style="width:300px;font-size:12px" colspan="2">Del break</td>								
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
								<td style="width:25px;text-align:center;font-size:12px">Si</td>
								<td style="width:25px;text-align:center;font-size:12px">No</td>
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">1. El break estaba apto (mesa limpia y elementos en cantidad adecuada)</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">2. La coordinación del break fue adecuada, se informo con antelación.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
														
						</table>
					</td>
				</tr>
				<tr>
					<td style="font-size:12px;text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table border="1">							
							<tr>
								<td  style="width:300px;font-size:12px;" colspan="2">Del Material de apoyo</td>								
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">1.Contaba con la pizarra y borrador.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">2. si ud. tiene autorizado previamente un data para su curso, estaba en la sala al inicio.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
														
						</table>
					</td>
				</tr>
				<tr>
					<td style="font-size:12px;text-align:center;">&nbsp;</td>
				</tr>
				<tr>
					<td>
						<table border="1">							
							<tr>
								<td  style="width:300px;font-size:12px;" colspan="2">Del Material (Contestar solo cuando amerite)</td>								
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
								<td style="width:25px;text-align:center;font-size:12px;">Si</td>
								<td style="width:25px;text-align:center;font-size:12px;">No</td>
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">1. El manual fue entregado en forma oportuna y el que corresponde.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">2. Contó con el material CD, lápices, etc.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>
							<tr>
								<td colspan="2" style="font-size:11px">3. Le fue entregado el libro de clase, los temarios y test de diagnosticos.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>	
							<tr>
								<td colspan="2" style="font-size:11px">4. Le fueron entregadas las encuestas de satisfacción del cliente.</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>								
							</tr>						
						</table>
					</td>
				</tr>
			</table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $htmlP12, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
// ---------------------------------------------------------
// Cerrar el documento PDF y preparamos la salida
// Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Localidades de " . $id_ficha[0] . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

}
