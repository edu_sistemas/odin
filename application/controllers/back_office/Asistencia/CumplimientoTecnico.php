<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class CumplimientoTecnico extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'CumplimientoTecnico';
    private $nombre_item_plural = 'CumplimientoTecnico';
    private $package = 'back_office/Asistencia';
    private $model = 'CumplimientoTecnico_model';
    private $view = 'CumplimientoTecnico_v';
    private $controller = 'CumplimientoTecnico';
    private $ind = '';

    function __construct() {
        parent::__construct();

        // Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        // Libreria de sesion
        $this->load->library('session');
    }

    // 2017-01-11
    // Controlador del panel de control
    public function index() {

        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );

        // array con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array(
                'src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'
            ),
            array(
                'src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'
            ),
            array(
                'src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'
            )
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array(
                'src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'
            ),
            array(
                'src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'
            ),
            array(
                'src' => base_url() . 'assets/amelia/js/sistema/funciones.js'
            ),
            array(
                'src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'
            ),
            array(
                'src' => base_url() . 'assets/amelia/js/sistema/Asistencia/CumplimientoTecnico.js'
            )
        );

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['activo'] = "Cumplimiento Técnico";
        $data ['menus'] = $this->session->userdata('menu_usuario');
        // / fin carga menu

        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getfichaData() {
        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_listar_capsulas($arr_input_data);
        echo json_encode($data);
        // informacion adicional para la pagina
    }

    function getFichas() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_ficha_reserva_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function capsulasListar() {
        /*
         * Ultima fecha Modificacion: 2017-01-04
         * Descripcion: función rescatar los detalles de la reserva que se encuentran en diferentes tablas
         * Usuario Actualizador : Marcelo Romero
         */
        $arr_input_data = array(
            'id_ficha' => $this->uri->segment(5)
        );

        $arr_input_data ['user_id'] = $this->session->userdata('id_user');
        $arr_input_data ['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_listar_capsulas($arr_input_data);
        echo json_encode($datos);
    }

    function alumnosListar() {
        /*
         * Ultima fecha Modificacion: 2017-01-04
         * Descripcion: función rescatar los detalles de la reserva que se encuentran en diferentes tablas
         * Usuario Actualizador : Marcelo Romero
         */
        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'id_capsula' => $this->input->post('id_capsula')
        );

        $arr_input_data ['user_id'] = $this->session->userdata('id_user');
        $arr_input_data ['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_listar_alumnos($arr_input_data);
        echo json_encode($datos);
    }

    function RegistrarEncuesta() {
        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'id_capsula' => $this->input->post('capsulainput'),
            'r1' => $this->input->post('r1'),
            'r2' => $this->input->post('r2'),
            'r3' => $this->input->post('r3'),
            'r4' => $this->input->post('r4'),
            'r5' => $this->input->post('r5'),
            'r6' => $this->input->post('r6'),
            'r7' => $this->input->post('r7'),
            'r8' => $this->input->post('r8'),
            'r9' => $this->input->post('r9'),
            'r10' => $this->input->post('r10'),
            'r11' => $this->input->post('r11'),
            'r12' => $this->input->post('r12'),
            'r13' => $this->input->post('r13')
        );

        $arr_input_data ['user_id'] = $this->session->userdata('id_user');
        $arr_input_data ['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->set_arr_registrar_encuesta($arr_input_data);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
