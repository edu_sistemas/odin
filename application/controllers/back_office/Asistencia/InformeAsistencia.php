<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class InformeAsistencia extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar';
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Asistencia';
    private $model = 'InformeAsistencia_model';
    private $view = 'InformeAsistencia_v';
    private $controller = 'InformeAsistencia';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2017-01-11
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Informe Asistencia',
                'icono' => ''
            )
        );

        // array con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tinycon/tinycon.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Asistencia/informeAsistencia.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Informe Asistencia";
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getfichaData() {

        $arr_input_data = array(
            'nombre_empresa' => $this->input->post('empresa_cbx'),
            'id_ficha' => $this->input->post('num_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_listar_fichas($arr_input_data);
        echo json_encode($data);
        // informacion adicional para la pagina
    }

    function getAsistencia() {

        $id_ficha = $this->input->post('id');

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_listar_asistencia($arr_input_data);
        echo json_encode($data);
        // informacion adicional para la pagina++va
    }

    function getAsistenciaDetalle() {

        $id_ficha = $this->input->post('id_ficha');
        $id_alumno = $this->input->post('id_alumno');

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'id_alumno' => $id_alumno,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_listar_asistencia_detalle($arr_input_data);
        echo json_encode($data);
    }

    function getEmpresas() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_empresas_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function generarExcel($id) {
        $id_ficha = $id;

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Hoja 1');
        //set cell A1 content with some text
        //change the font size
        $data_excel_alumnos = $this->modelo->get_lista_detalle_excel($arr_input_data); // trae nombre, rut de los alumnos
        $data_excel_alumnos_id = $this->modelo->get_lista_detalle_excel_id_alm($arr_input_data); // trae id de alumnos
        $data_excel_datos = $this->modelo->get_lista_detalle_dias_excel($arr_input_data); // trae fechas de las clases (dias de las clases)

        $this->excel->getActiveSheet()->SetCellValue('A1', 'RUT'); //Títulos del documento
        $this->excel->getActiveSheet()->SetCellValue('B1', 'NOMBRE'); //Títulos del documento

        $contador = count($data_excel_datos);
        $contador_alumnos = count($data_excel_alumnos);

        $letra = 'C'; // Contador para ir ubicando las fechas horizontal
        $numero = 2;
        for ($i = 0; $i < $contador; $i++) {
            $this->excel->getActiveSheet()->SetCellValue($letra . '1', $data_excel_datos[$i]['horario_inicio']); //Imprime las fehcas de las clases horizontal
            $this->excel->getActiveSheet()->fromArray($data_excel_alumnos, null, 'A2'); // Imprime el rut y el nombre desde el array
            $numero ++;
            $letra ++;
        }

        $letter = 'C';
        for ($j = 0; $j < $contador_alumnos; $j++) { //ciclo for para ubicar las asistencias
            $id_detalle_alumno = $data_excel_alumnos_id[$j]['id_detalle_alumno'];
            $arr_input = array(
                'id_ficha' => $id_detalle_alumno,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $data_excel_AP = $this->modelo->get_lista_detalle_dias_dtl_excel($arr_input);
            $contador_asistencia = count($data_excel_AP);
            for ($x = 0; $x < $contador_asistencia; $x ++) {
                $this->excel->getActiveSheet()->SetCellValue($letter . ($j + 2), $data_excel_AP[$x]['asistencia']);
                $letter ++;
            }
            $letter = 'C';
        }

        $this->excel->getActiveSheet()->getStyle('A1:BB1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '#afddb2')
                    )
                )
        );

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = 'InformeDeNotas' . $data_excel_datos[0]['num_ficha'] . '.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */