<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class LibroClases extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'LibroClase';
    private $nombre_item_plural = 'LibroClases';
    private $package = 'back_office/Asistencia';
    private $model = 'LibroClases_model';
    private $view = 'libroClases_v';
    private $controller = 'LibroClases';
    private $ind = '';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
    }

    public function index() {
        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/LibroClases',
                'label' => 'Libro de Clases',
                'icono' => ''
            )
        );

        // array con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Asistencia/libroClases.js')
        );

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['activo'] = "Libro Clases";
        $data ['menus'] = $this->session->userdata('menu_usuario');
        // / fin carga menu


        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function GetRelatores() {

        // variables de sesion

        $id_ficha = $this->input->post('id_ficha');


        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->getRelatotes($arr_data);
        echo json_encode($datos);
    }

    function buscarLibro() {

        // variables de sesion
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->getLibros($arr_data);
        echo json_encode($datos);
    }

    public function generar() {

        $id_ficha = $this->uri->segment(5);
        $id_relator = $this->uri->segment(6);
        $opcion_sence = $this->uri->segment(7);
        $relator = urldecode($id_relator);

        $arr_datos = array(
			'id_ficha' => $id_ficha,
			'opcion_sence' => $opcion_sence,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

		$dataLibro = $this->modelo->getDatosLibro($arr_datos);
        $dataAlumnos = $this->modelo->getAlumnosLibro($arr_datos);
        $GLOBALS['num_ficha'] = $dataLibro[0]["num_ficha"];


        $ID_sence = "";
        switch ($opcion_sence) {
            case "0": $ID_sence = "";
                break;
            case "1": $ID_sence = $dataLibro[0]["id_consolidado"];
                break;
            case "2": $ID_sence = $dataLibro[0]["id_sence"];
                break;
		}
		$GLOBALS['codigo_sence'] = $ID_sence;

        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitacion');
        $pdf->SetTitle('Libro de Clases');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(
            0,
            0,
            0
                ), $lc = array(
            0,
            0,
            0
        ));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(0);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
		$pdf->addFont('frankie', '', 'frankie.php');
		$pdf->addFont('calibri', '', 'calibri.php');
		$pdf->SetFont('Helvetica', '', 14, '', false);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L');

        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));
        $html = '';
        $html .= '		
				<table width="100%">
					<tr><td width="50%"><img src="assets/img/librodeclases/portada_sin_logo.jpg"></td><td width="50%">
					<table><tr align="right"><td>N° FICHA: <strong>'.$GLOBALS['num_ficha'].'</strong></td></tr>
					<tr><td></td></tr> 
					<tr><td></td></tr>';
		$html .= '<tr align="left"><td><font face="Helvetica" size="8">NOMBRE ACTIVIDAD DE CAPACITACIÓN:</font></td></tr>
			<tr><td><font face="frankie" size="14">' . $dataLibro [0] ["nombre_curso"] . ' '.$dataLibro [0] ["duracion_curso"].' Hrs.</font></td></tr>
			<tr><td></td></tr>

			<tr><td><font face="Helvetica" size="8">EMPRESA:</font></td></tr>
			<tr valigin="top"><td><font face="Helvetica" size="12">' . $dataLibro [0] ["nombre_empresa"] . '</font></td></tr>
			<tr><td></td></tr>

			<tr><td><font face="Helvetica" size="8">FECHA DE EJECUCIÓN:</font></td></tr>
			<tr valigin="top"><td><font face="Helvetica" size="12">Del ' . $dataLibro [0] ["fecha_inicio"] . ' al ' . $dataLibro [0] ["fecha_termino"] . '</font></td></tr>

			<tr valigin="top"><td><font face="Helvetica" size="12">' . $dataLibro [0] ["dias"] . '</font></td></tr>
			<tr><td></td></tr>

			<tr><td><font face="Helvetica" size="8">HORARIO:</font></td></tr>
			<tr valigin="top"><td><font face="Helvetica" size="12">De ' . $dataLibro [0] ["hora_inicio"] . ' a ' . $dataLibro [0] ["hora_termino"] . '</font></td></tr>
			<tr><td></td></tr>

			<tr><td><font face="Helvetica" size="8">CANTIDAD DE ALUMNOS:</font></td></tr>
			<tr valigin="top"><td><font face="Helvetica" size="12">' . $dataLibro [0] ["cant_alumno"] . '</font></td></tr>
			<tr><td></td></tr>

			<tr><td><font face="Helvetica" size="8">LUGAR DE EJECUCIÓN:</font></td></tr>
			<tr valigin="top"><td><font face="Helvetica" size="12">' . $dataLibro [0] ["nombre_sede"] . ' - ' . $dataLibro [0] ["nombre_sala"] . '</font></td></tr>
			<tr><td></td></tr>

			<tr><td><font face="Helvetica" size="8">RELATOR:</font></td></tr>
			<tr valigin="top"><td colspan="2"><font face="Helvetica" size="12">' . $relator . '</font></td></tr>
			<tr><td></td></tr>

			<tr><td><table>
			
			<tr><td><font face="Helvetica" size="8">CODIGO AUTORIZACIÓN SENCE:</font></td><td><font face="Helvetica" size="8">ID SENCE:</font></td></tr>
			<tr valigin="top"><td><font face="Helvetica" size="12"><b>' . $dataLibro [0] ["codigo_sence"] . '</b></font></td><td><font face="Helvetica" size="12"><b>' . $ID_sence . '</b></font></td></tr>
			</table></td></tr>
				</table>
				</td></tr>			
				</table>';
		$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
		$pdf->AddPage('L');

		$pdf->Image("assets/img/librodeclases/pizarra_relator_con_logo.jpg", 25, 0, 0, 0, '', '', '', false, 300, '', false, false, 0);

		 $html = '<table>
					 <tr><td>
					 <ol>
					 <font face="Helvetica" size="8" align="left"><b>SOBRE EL USO DEL LIBRO DE CLASES</b></font><br>
					 <font face="Helvetica" size="8"><li>En la primera clase TODOS los participantes deben llenar sus datos en el libro. En el caso que no cuente con teléfono o correo electrónico propio, solicitar que escriba datos del supervisor seguido de (S).</li></font>
					 <font face="Helvetica" size="8"><li>Cada clase se debe registrar la asistencia de los participantes y el contenido visto.</li></font>
					 <font face="Helvetica" size="8"><li>El libro de clases no puede ser enmendado sin la previa autorización de la Unidad de Docencia.</li></font>
					 <font face="Helvetica" size="8"><li>Si el libro de clases no contiene el % de asistencia y las notas de los alumnos, éste no podrá ser pagado.</li></font>
					 <font face="Helvetica" size="8"><li>El profesor debe guardar el "Libro de Clases" en la carpeta correspondiente al horario del curso.</li></font>					 
					 </ol></td></tr>

					 <tr><td>
					 <ol>
					 <font face="Helvetica" size="8" align="left"><b>SOBRE EL DESARROLLO DE LA CLASE</b></font><br>
					 <font face="Helvetica" size="8"><li>El relator debe llegar 15 minutos antes de que comience la clase para revisar los equipos.</li></font>
					 <font face="Helvetica" size="8"><li>Las clases no deben comenzar con una demora superior a los 5 minutos.</li></font>
					 <font face="Helvetica" size="8"><li>En la primera clase el relator debe: indicar los objetivos del curso, los criterios de evaluación del curso (nota, asistencia, apreciación, etc.) y entregar los materiales del curso.</li></font>
					 <font face="Helvetica" size="8"><li>El relator no está autorizado para cambiar horario, días de ejecución, o suspender una o más clases.</li></font>
					 <font face="Helvetica" size="8"><li>Sólo se realizará cambio de relator bajo los siguientes casos de fuerza mayor:
					<br> a)	Enfermedad (sólo reemplazo).
					<br> b)	A solicitud de los participantes o encargado de capacitación</li></font>
					<font face="Helvetica" size="8"><li>Se solicita al relator apagar el aire acondicionado, y solicitar a los participantes apagar correctamente los equipos al término de cada clase.</li></font>					 
					 </ol></td></tr>
					 <tr>
					 <td>
					 <table>
					 <tr><td></td><td></td><td style="border-bottom: 1px solid black;">_________________</td></tr>
					 <tr align="center"><td></td><td></td><td><font face="Helvetica" size="9" >FIRMA RELATOR</font></td></tr>
					 </table>
					 </td>
					 </tr>
				 </table>';
		$pdf->writeHTMLCell($w = 150, $h = 0, $x = '94', $y = '38', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
		
		

		$paginas = ceil(count($dataAlumnos) / 15);
		$counter_strike = 0;
		$counter_strike2 = 15;

		//DATOS PERSONALES ALUMNO 1/2
		for ($i=0; $i < $paginas; $i++) { 
			$pdf->AddPage('L');
			if($i==0){
				$pdf->Image("assets/img/librodeclases/barra_titulo.png", 10, 10, 280, 0, '', '', '', false, 300, '', false, false, 0);
				$html = '<table style="margin-top:15px;"><tr color="white"><td><font face="Helvetica" size="20">DATOS PARTICIPANTES (1 de 2)</font></td></tr></table>';
				$pdf->writeHTMLCell($w = 0, $h = 0, $x = '20', $y = '16', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
			}
			 
			 
			$html = '<table border="1" cellpadding="3">
						<tr align="center" style="background-color:#d9d9d9;">
							<td width="5%"><font face="Helvetica" size="10"><b>N°</b></font></td>
							<td width="21%"><font face="Helvetica" size="10"><b>NOMBRE Y APELLIDOS</b></font></td>
							<td><font face="Helvetica" size="10"><b>RUT</b></font></td>
							<td><font face="Helvetica" size="10"><b>NIVEL ESCOLARIDAD</b></font></td>
							<td><font face="Helvetica" size="10"><b>FECHA DE NACIM.</b></font></td>
							<td  width="25%"><font face="Helvetica" size="10"><b>FECHA RECEPCIÓN MATERIALES</b></font></td>
							<td  width="8%"><font face="Helvetica" size="10"><b>SENCE</b></font></td>
						</tr>';
			
				for ($j= $counter_strike; $j < $counter_strike2; $j++) { 
					if(count($dataAlumnos) <= $j){
						break;
					}else{
						$html .= '
					<tr>
							<td><font face="Helvetica" size="8">'. ($j+1) .'</font></td>
							<td width="21%"><font face="Helvetica" size="8">' . $dataAlumnos [$j] ["nombre_alumno"] . '</font></td>
							<td><font face="Helvetica" size="8">'. $dataAlumnos[$j]['rut'] .'</font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8">'. $dataAlumnos[$j]['sence'] .'</font></td>
						</tr>';
						if( $j +1 == count($dataAlumnos) ){
							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+2) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+3) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+4) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+5) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';
							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+6) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';
						}
					}
				}
					$counter_strike = $counter_strike + 15;
					$counter_strike2 = $counter_strike2 + 15;
					
									
			$html .= '</table>';
				if($i==0){
					$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '40', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				}else{
					$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '20', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				}
				
		}

		$counter_strike = 0;
		$counter_strike2 = 15;

		//DATOS PERSONALES ALUMNO 2/2
		
		for ($i=0; $i < $paginas; $i++) { 
			$pdf->AddPage('L');
			if($i==0){
				$pdf->Image("assets/img/librodeclases/barra_titulo.png", 10, 10, 280, 0, '', '', '', false, 300, '', false, false, 0);
				$html = '<table style="margin-top:15px;"><tr color="white"><td><font face="Helvetica" size="20">DATOS PARTICIPANTES (2 de 2)</font></td></tr></table>';
				$pdf->writeHTMLCell($w = 0, $h = 0, $x = '20', $y = '16', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
			}
			 
			 
			$html = '<table border="1" cellpadding="3" width="100%">
						<tr align="center" style="background-color:#d9d9d9;">
							<td width="3%"><font face="Helvetica" size="10"><b>N°</b></font></td>
							<td width="20%"><font face="Helvetica" size="10"><b>NOMBRE Y APELLIDOS</b></font></td>
							<td width="20%"><font face="Helvetica" size="10"><b>EMPRESA</b></font></td>
							<td width="20%"><font face="Helvetica" size="10"><b>CARGO</b></font></td>
							<td width="20%"><font face="Helvetica" size="10"><b>CORREO ELECTRÓNICO</b></font></td>
							<td width="17%"><font face="Helvetica" size="10"><b>TELÉFONO</b></font></td>
						</tr>';
			
				for ($j= $counter_strike; $j < $counter_strike2; $j++) { 
					if(count($dataAlumnos) <= $j){
						break;
					}else{
						$html .= '
					<tr>
							<td><font face="Helvetica" size="8">'. ($j+1) .'</font></td>
							<td width="20%"><font face="Helvetica" size="8">' . $dataAlumnos [$j] ["nombre_alumno"] . '</font></td>
							<td><font face="Helvetica" size="8">' . $dataLibro [0] ["nombre_empresa"] . '</font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8"></font></td>
						</tr>';
						if( $j +1 == count($dataAlumnos) ){
							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+2) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+3) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+4) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+5) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';
							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+6) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';
						}
					}
				}
					$counter_strike = $counter_strike + 15;
					$counter_strike2 = $counter_strike2 + 15;
					
									
			$html .= '</table>';
				if($i==0){
					$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '40', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				}else{
					$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '20', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				}
				
		}

		//Fin Datos personales

		$paginas_asistencia = $dataLibro [0] ["capsulas"];
		if($paginas_asistencia == 0){
			$paginas_asistencia = 1;
		}

		for ($i=0; $i < $paginas_asistencia; $i++) { 
			$pdf->AddPage('L');
			$pdf->Image("assets/img/librodeclases/barra_titulo.png", 10, 10, 280, 0, '', '', '', false, 300, '', false, false, 0);
			$html = '<table style="margin-top:15px;"><tr color="white"><td><font face="Helvetica" size="20">REGISTRO ASISTENCIA Y PROGRESO DE CLASES</font></td></tr></table>';
			$pdf->writeHTMLCell($w = 0, $h = 0, $x = '20', $y = '16', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
			$html = '<table border="1" cellpadding="3" width="100%">
						<tr align="center"><td style="background-color:#d9d9d9;" width="8%"><font face="Helvetica" size="10"><b>CLASE N°</b></font></td><td width="3%"><font face="Helvetica" size="10"><b>'. ($paginas_asistencia == 1 ? "" : ($i+1)) .'</b></font></td><td style="background-color:#d9d9d9;" width="7%"><font face="Helvetica" size="10"><b>FECHA:</b></font></td><td width="30%"></td><td style="background-color:#d9d9d9;" width="9%"><font face="Helvetica" size="10"><b>RELATOR:</b></font></td><td width="44.5%"></td></tr>
					</table>';
			$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '40', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
			$counter_strike = 0;
			$counter_strike2 = 10;
			$posicion_tabla = 10;

			for ($j=0; $j < 3; $j++) { 
				$html = '<table border="1" cellpadding="3">
						<tr style="background-color:#d9d9d9;">
							<td width="2%"><font face="Helvetica" size="10"><b>N°</b></font></td>
							<td width="21%"><font face="Helvetica" size="10"><b>NOMBRE</b></font></td>
							<td width="10%"><font face="Helvetica" size="10"><b>FIRMA</b></font></td>
						</tr>';

						for ($k= $counter_strike; $k < $counter_strike2; $k++) { 
							if(count($dataAlumnos) <= $k){
								$html .= '
									<tr>
										<td><font face="Helvetica" size="8">'. ($k+1) .'</font></td>
										<td><font face="Helvetica" size="8"></font></td>
										<td><font face="Helvetica" size="8"></font></td>
									</tr>';
							}else{
								$html .= '
								<tr>
									<td><font face="Helvetica" size="8">'. ($k+1) .'</font></td>
									<td><font face="Helvetica" size="8">' . $dataAlumnos [$k] ["nombre_alumno"] . '</font></td>
									<td><font face="Helvetica" size="8"></font></td>
								</tr>';
							}
						}
				$counter_strike = $counter_strike + 10;
				$counter_strike2 = $counter_strike2 + 10;
				$html .= '</table>';

				$pdf->writeHTMLCell($w = 280, $h = 0, $x = $posicion_tabla, $y = '50', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				$posicion_tabla = $posicion_tabla +94;
			}

			$html = '<table border="1" align="center">
						<tr style="background-color:#d9d9d9;"><td width="44%"><font face="Helvetica" size="9"><b>TEMAS Y ACTIVIDADES DESARROLLADOS EN CLASE</b></font></td><td width="42%"><font face="Helvetica" size="9"><b>OBSERVACIONES</b></font></td><td width="16%"><font face="Helvetica" size="9"><b>FIRMA RELATOR</b></font></td></tr>
						<tr style="line-height: 90px;"><td></td><td></td><td></td></tr>
					</table>';

			$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '152', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
		}

		//EVALUACIONES Y PORCENTAJE DE ASISTENCIA

		$counter_strike = 0;
		$counter_strike2 = 15;

		for ($i=0; $i < $paginas; $i++) { 
			$pdf->AddPage('L');
			if($i==0){
				$pdf->Image("assets/img/librodeclases/barra_titulo.png", 10, 10, 280, 0, '', '', '', false, 300, '', false, false, 0);
				$html = '<table style="margin-top:15px;"><tr color="white"><td><font face="Helvetica" size="20">EVALUACIONES Y PORCENTAJE DE ASISTENCIA</font></td></tr></table>';
				$pdf->writeHTMLCell($w = 0, $h = 0, $x = '20', $y = '16', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
			}
			 
			 
			$html = '<table border="1" cellpadding="3">
						<tr align="center" style="background-color:#d9d9d9;">
							<td width="5%"><font face="Helvetica" size="10"><b>N°</b></font></td>
							<td width="20%"><font face="Helvetica" size="10"><b>NOMBRE Y APELLIDOS</b></font></td>
							<td width="7%"><font face="Helvetica" size="10"><b>NOTA 1</b></font></td>
							<td width="7%"><font face="Helvetica" size="10"><b>NOTA 2</b></font></td>
							<td width="10%"><font face="Helvetica" size="10"><b>NOTA FINAL</b></font></td>
							<td width="12%"><font face="Helvetica" size="10"><b>% ASISTENCIA</b></font></td>
							<td width="7%"><font face="Helvetica" size="10"><b>SENCE</b></font></td>
							<td width="33%"><font face="Helvetica" size="10"><b>OBSERVACIONES</b></font></td>
						</tr>';
			
				for ($j= $counter_strike; $j < $counter_strike2; $j++) { 
					if(count($dataAlumnos) <= $j){
						break;
					}else{
						$html .= '
					<tr>
							<td><font face="Helvetica" size="8">'. ($j+1) .'</font></td>
							<td><font face="Helvetica" size="8">' . $dataAlumnos [$j] ["nombre_alumno"] . '</font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8"></font></td>
							<td><font face="Helvetica" size="8">'. $dataAlumnos[$j]['sence'] .'</font></td>
							<td><font face="Helvetica" size="8"></font></td>
						</tr>';
						if( $j +1 == count($dataAlumnos) ){
							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+2) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+3) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+4) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';

							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+5) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';
							$html .= '
							<tr>
								<td><font face="Helvetica" size="8">'. ($j+6) .'</font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
								<td><font face="Helvetica" size="8"></font></td>
							</tr>';
						}
					}
				}
					$counter_strike = $counter_strike + 15;
					$counter_strike2 = $counter_strike2 + 15;
					
									
			$html .= '</table>';
				if($i==0){
					$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '40', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				}else{
					$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '20', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				}
				
		}

		$pdf->AddPage('L');
		$pdf->Image("assets/img/librodeclases/barra_titulo.png", 10, 10, 280, 0, '', '', '', false, 300, '', false, false, 0);
		$html = '<table style="margin-top:15px;"><tr color="white"><td><font face="Helvetica" size="20">CUMPLIMIENTO TÉCNICO - ADMINISTRATIVO</font></td></tr></table>';
		$pdf->writeHTMLCell($w = 0, $h = 0, $x = '20', $y = '16', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
		

        $htmlP12 = '
				<style>
				.encabezado{
					font-size: 70%;
				}
				.contenido{
					font-size: 70%;
					font-weight: bold;
				}
				td{
					font-size:60%;
				}
				</style>
	<table>
		<tr>
			<td>			
				<table border="1" cellpadding="1">
					<tr>
						<td style="width:305px;font-size:10px;background-color:#d9d9d9;" align="right">Fecha</td>
						<td style="width:50px" colspan="2"></td>
						<td style="width:50px" colspan="2"></td>
						<td style="width:50px" colspan="2"></td>
						<td style="width:50px" colspan="2"></td>
						<td style="width:50px" colspan="2"></td>
						<td style="width:50px" colspan="2"></td>
						<td style="width:50px" colspan="2"></td>
						<td style="width:50px" colspan="2"></td>
						<td style="width:50px" colspan="2"></td>
					</tr>
					<tr>
						<td style="font-size:10px;background-color:#d9d9d9;" align="right">Sala</td>
						<td colspan="2"></td>
						<td colspan="2"></td>
						<td colspan="2"></td>
						<td colspan="2"></td>
						<td colspan="2"></td>
						<td colspan="2"></td>
						<td colspan="2"></td>
						<td colspan="2"></td>
						<td colspan="2"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
            <td style="font-size:12px;text-align:center;">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table border="1" cellpadding="1">
                    <tr style="background-color:#d9d9d9;">
                        <td style="width:300px;font-size:10px;" colspan="2">De los Computadores</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">1. Estaban todos al inicio del curso, con el software adecuado y se iniciaron en forma normal.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">2. Si durante el desarrollo del curso tuvo problemas, fue solucionado por el Departamento técnico
                            oportunamente.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-size:12px;text-align:center;">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table border="1" cellpadding="1">

                    <tr style="background-color:#d9d9d9;">
                        <td style="width:300px;font-size:10px;" colspan="2">De la sala</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">1. Estaba limpia apta para hacer clases (mobiliario y orden)</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">2. Mantiene la misma sala.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">3. La ventilación era la adecuada.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-size:12px;text-align:center;">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table border="1" cellpadding="1">

                    <tr style="background-color:#d9d9d9;">
                        <td style="width:300px;font-size:10px;" colspan="2">Del break</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                        <td style="width:25px;text-align:center;font-size:10px">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px">No</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">1. El break estaba apto (mesa limpia y elementos en cantidad adecuada)</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">2. La coordinación del break fue adecuada, se informo con antelación.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td style="font-size:12px;text-align:center;">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table border="1" cellpadding="1">
                    <tr style="background-color:#d9d9d9;">
                        <td style="width:300px;font-size:10px;" colspan="2">Del Material de apoyo</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">1.Contaba con la pizarra y borrador.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">2. si ud. tiene autorizado previamente un data para su curso, estaba en la sala al inicio.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td style="font-size:12px;text-align:center;">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table border="1" cellpadding="1">
                    <tr style="background-color:#d9d9d9;">
                        <td style="width:300px;font-size:10px;background-color:#d9d9d9;" colspan="2">Del Material (Contestar solo cuando amerite)</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                        <td style="width:25px;text-align:center;font-size:10px;">Si</td>
                        <td style="width:25px;text-align:center;font-size:10px;">No</td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">1. El manual fue entregado en forma oportuna y el que corresponde.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">2. Contó con el material CD, lápices, etc.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">3. Le fue entregado el libro de clase, los temarios y test de diagnosticos.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="font-size:09px">4. Le fueron entregadas las encuestas de satisfacción del cliente.</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
        $pdf->writeHTMLCell($w = 0, $h = 10, $x = '', $y = '36', $htmlP12, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);


        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Libro de Clases Ficha N° " . $GLOBALS['num_ficha'] . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
	}
	
    public function fichas() {
        $res = $this->modelo->getFichas();

        return json_encode($res);
	}
	
	public function generarEncuesta() {
		
				$id_ficha = $this->uri->segment(5);
				$id_relator = $this->uri->segment(6);
				$opcion_sence = $this->uri->segment(7);
				$relator = urldecode($id_relator);
		
				$arr_datos = array(
					'id_ficha' => $id_ficha,
					'opcion_sence' => '',
					'user_id' => $this->session->userdata('id_user'),
					'user_perfil' => $this->session->userdata('id_perfil')
				);
		
				$dataLibro = $this->modelo->getDatosLibro($arr_datos);
				$dataAlumnos = $this->modelo->getAlumnosLibro($arr_datos);
				$GLOBALS['num_ficha'] = $dataLibro[0]["num_ficha"];
		
		
				$ID_sence = "";
				switch ($opcion_sence) {
					case "0": $ID_sence = "";
						break;
					case "1": $ID_sence = $dataLibro[0]["id_consolidado"];
						break;
					case "2": $ID_sence = $dataLibro[0]["id_sence"];
						break;
				}
		
				$this->load->library('Pdf');
				$pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor('Edutecno Capacitación');
				$pdf->SetTitle('Encuesta de Satisfacción');
				$pdf->SetSubject('Tutorial TCPDF');
				$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		
				// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
				// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
				$pdf->setFooterData($tc = array(
					0,
					64,
					0
						), $lc = array(
					0,
					64,
					128
				));
				$pdf->setPrintHeader(false);
				$pdf->setPrintFooter(false);
		
				// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
				// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
				$pdf->setFooterFont(Array(
					PDF_FONT_NAME_DATA,
					'',
					PDF_FONT_SIZE_DATA
				));
		
				// se pueden modificar en el archivo tcpdf_config.php de libraries/config
				$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
				// se pueden modificar en el archivo tcpdf_config.php de libraries/config
				// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				$pdf->SetMargins(10, 5, 10);
				// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
				$pdf->SetFooterMargin(0);
		
				// se pueden modificar en el archivo tcpdf_config.php de libraries/config
				// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
				// relación utilizada para ajustar la conversión de los píxeles
				// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
				// ---------------------------------------------------------
				// establecer el modo de fuente por defecto
				$pdf->setFontSubsetting(true);
		
				// Establecer el tipo de letra
				// Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
				// Helvetica para reducir el tamaño del archivo.
				$pdf->SetFont('Helvetica', '', 14, '', true);
		
				// Añadir una página
				// Este método tiene varias opciones, consulta la documentación para más información.
				$pdf->AddPage('P');
		
				// fijar efecto de sombra en el texto
				$pdf->setTextShadow(array(
					'enabled' => true,
					'depth_w' => 0.2,
					'depth_h' => 0.2,
					'color' => array(
						196,
						196,
						196
					),
					'opacity' => 1,
					'blend_mode' => 'Normal'
				));
				$html = '<style>
						.bluefondo{
							background-color:#2e74b5;
							color:#fff;
							font-weight:bold;
						}
						</style>';
						$tamano_fuente_empresa = '90%';
						if(strlen($dataLibro [0] ["nombre_empresa"]) >= 35){
							$tamano_fuente_empresa = '70%';
						}
				$html .= '		
						<table width="100%" style="font-size: 10px;border-style: solid;border-width: 1px 0 1px 1px; border-color: #bfbfbf">	
							<tr><td width="35%" style="vertical-align:middle; text-align:center;"><img style="display:block; margin: 0 auto;width: 1300%;" src="'.base_url() . 'assets/img/logoEdutecno2.png'.'"></td><td width="65%"><table width="100%" style="border-style: solid;border-width: 1px 0px 1px 1px; border-color: #bfbfbf">
																																<tr><td class="bluefondo" style="font-size: 120%;border-style: solid;border-width: 1px 0px 1px 1px; border-color: #bfbfbf" colspan="2" align="center">ENCUESTA DE SATISFACCIÓN</td></tr>
																																<tr><td align="right" style="border-style: solid;border-width: 1px 0px 1px 1px; border-color: #bfbfbf">Nombre del Relator:</td><td align="center" style="border-style: solid;border-width: 1px 0px 1px 1px; border-color: #bfbfbf">'. $relator .'</td></tr>
																																<tr><td align="rigth" style="border-style: solid;border-width: 1px 0px 1px 1px; border-color: #bfbfbf">Número de Ficha del curso:</td><td align="center" style="border-style: solid;border-width: 1px 0px 1px 1px; border-color: #bfbfbf">' . $dataLibro [0] ["num_ficha"] . '</td></tr>
																																<tr><td align="right" style="border-style: solid;border-width: 1px 0px 1px 1px; border-color: #bfbfbf">Nombre de la Empresa:</td><td align="center" style="border-style: solid;border-width: 1px 0px 1px 1px; border-color: #bfbfbf; font-size:'.$tamano_fuente_empresa.'">'. $dataLibro [0] ["nombre_empresa"].'</td></tr>
																																
																															</table></td></tr></table><br>';
				$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				$html2 = '<p style="font-size:10px;">A continuación, usted encontrará algunas afirmaciones. Marque con una <b>X</b> la nota que mejor interprete su nivel de satisfacción respecto de cada afirmación. Tenga presente que un 7 es el mayor grado de satisfacción.</p>';
				$html2 .= '<p style="font-size:10px;">Esta encuesta es de carácter anónimo y sus respuestas nos ayudarán a entregar un mejor servicio.</p></br>';
				$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html2, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				$html3 ='
				<style>
					.circulo{
						width:140%;
					}
					table, tr, td{
						padding:3px;
						border-bottom:0.7px solid #bfbfbf;
						font-size: 94.96%;
					}
				</style>
				<table style="font-size:9px;">
				<tr bgcolor="#2e74b4" style="color:#fff;"><td style="width:72%" align="left" >IMPRESIÓN GENERAL</td><td align="center" style="width: 4%;">1</td><td align="center" style="width: 4%;">2</td><td align="center"  style="width: 4%;">3</td><td align="center"  style="width: 4%;">4</td><td align="center"  style="width: 4%;">5</td><td align="center"  style="width: 4%;">6</td><td align="center" style="width: 4%;">7</td></tr>

				<tr><td style="width:72%" align="left" >Exprese su impresión general respecto del curso realizado</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr bgcolor="#2e74b4" style="color:#fff;"><td style="width:72%" align="left" >SOBRE EL CURSO</td><td align="center" style="width: 4%;">1</td><td align="center" style="width: 4%;">2</td><td align="center"  style="width: 4%;">3</td><td align="center"  style="width: 4%;">4</td><td align="center"  style="width: 4%;">5</td><td align="center"  style="width: 4%;">6</td><td align="center" style="width: 4%;">7</td></tr>

				<tr><td style="width:72%" align="left" >1.	El relator presentó de forma clara los objetivos del curso:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >2.	Los contenidos del curso concordaron con los objetivos planteados al inicio de este:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >3.	La composición del grupo fue adecuada para el logro de los objetivos:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >4.	La duración del curso fue la adecuada para el logro de los objetivos:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr bgcolor="#2e74b4" style="color:#fff;"><td style="width:72%" align="left" >SOBRE EL MATERIAL</td><td align="center" style="width: 4%;">1</td><td align="center" style="width: 4%;">2</td><td align="center"  style="width: 4%;">3</td><td align="center"  style="width: 4%;">4</td><td align="center"  style="width: 4%;">5</td><td align="center"  style="width: 4%;">6</td><td align="center" style="width: 4%;">7</td></tr>

				<tr><td style="width:72%" align="left" >5.	En contenido del manual concuerda con los objetivos del curso:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >6.	El manual es fácilmente comprensible:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >7.	El manual presta ayuda en forma real y efectiva al desarrollo del curso:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >8.	Los ejercicios y/o actividades en clases fueron adecuados para los temas vistos:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr bgcolor="#2e74b4" style="color:#fff;"><td style="width:72%" align="left" >SOBRE EL RELATOR</td><td align="center" style="width: 4%;">1</td><td align="center" style="width: 4%;">2</td><td align="center"  style="width: 4%;">3</td><td align="center"  style="width: 4%;">4</td><td align="center"  style="width: 4%;">5</td><td align="center"  style="width: 4%;">6</td><td align="center" style="width: 4%;">7</td></tr>

				<tr><td style="width:72%" align="left" >9.	Las explicaciones del relator fueron claras y comprensibles:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >10.	El relator demostró dominar realmente las materias tratadas en clases:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >11.	El relator fue capaz de integrar y relacionar los contenidos para mostrar:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >12.	La metodología usada por el relator fue adecuada para entregar los contenidos del curso:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >13.	El relator tuvo una buena dicción y fue capaz de motivar a los participantes durante el curso:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >14.	El relator se encontraba en la sala de clases a la hora de inicio y término del curso:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >15.	El relator dio respuesta en forma adecuada a las preguntas de los participantes:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr bgcolor="#2e74b4" style="color:#fff;"><td style="width:72%" align="left" >SOBRE EL INSTITUTO</td><td align="center" style="width: 4%;">1</td><td align="center" style="width: 4%;">2</td><td align="center"  style="width: 4%;">3</td><td align="center"  style="width: 4%;">4</td><td align="center"  style="width: 4%;">5</td><td align="center"  style="width: 4%;">6</td><td align="center" style="width: 4%;">7</td></tr>

				<tr><td style="width:72%" align="left" >16.	La sala de clases fue cómoda para el desarrollo del curso:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >17.	El coffee break fue bueno:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr><td style="width:72%" align="left" >18.	La ubicación del instituto es adecuada:</td><td align="center" style="width: 4%;text-align:center;"><img  class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center" style="width: 4%;"><p><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></p></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td><td align="center"  style="width: 4%;"><img class="circulo" src="'.base_url() . 'assets/amelia/images/circulo.png'.'"/></td></tr>

				<tr bgcolor="#2e74b4" style="color:#fff;"><td style="width:72%" align="left" >COMETARIOS - SUGERENCIAS</td><td align="center" style="width: 4%;"></td><td align="center" style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center" style="width: 4%;"></td></tr>

				<tr><td style="width:72%" align="left" ></td><td align="center" style="width: 4%;text-align:center;"></td><td align="center" style="width: 4%;"><p></p></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td></tr>
				<tr><td style="width:72%" align="left" ></td><td align="center" style="width: 4%;text-align:center;"></td><td align="center" style="width: 4%;"><p></p></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td></tr>
				<tr><td style="width:72%" align="left" ></td><td align="center" style="width: 4%;text-align:center;"></td><td align="center" style="width: 4%;"><p></p></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td></tr>
				<tr><td style="width:72%" align="left" ></td><td align="center" style="width: 4%;text-align:center;"></td><td align="center" style="width: 4%;"><p></p></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td></tr>
				<tr><td style="width:72%" align="left" ></td><td align="center" style="width: 4%;text-align:center;"></td><td align="center" style="width: 4%;"><p></p></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td></tr>
				<tr><td style="width:72%" align="left" ></td><td align="center" style="width: 4%;text-align:center;"></td><td align="center" style="width: 4%;"><p></p></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td></tr>
				<tr><td style="width:72%" align="left" ></td><td align="center" style="width: 4%;text-align:center;"></td><td align="center" style="width: 4%;"><p></p></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td></tr>
				</table>';
				
				$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html3, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				
				$html4 = '
				<style>
				table, tr, td{
					font-size: 80%;
					color: #b6b6b6;
				}
				</style>

				<table width="100%">
				<tr><td width="33%"></td><td width="34%"></td><td width="33%"></td></tr>
				<tr><td width="33%"></td><td width="34%"></td><td width="33%"></td></tr>
				<tr><td width="33%">ES-01 v.2 '.date("d-m-Y").'</td><td width="34%"></td><td width="33%"></td></tr>
				</table>
				
				';
				$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html4, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
				// $pdf->Cell(0, 0, 'LIBRO DE CLASES', 1, 1, 'C', 0, '', 4);
				// $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);
				
				// ---------------------------------------------------------
				// Cerrar el documento PDF y preparamos la salida
				// Este método tiene varias opciones, consulte la documentación para más información.
				$nombre_archivo = utf8_decode("Libro de Clases Ficha N° " . $GLOBALS['num_ficha'] . ".pdf");
				$pdf->Output($nombre_archivo, 'I');
			}

			

			public function generarFormularioAsignacion() {
				
						$id_ficha = $this->uri->segment(5);
						$id_relator = $this->uri->segment(6);
						$opcion_sence = $this->uri->segment(7);
						$relator = urldecode($id_relator);
				
						$arr_datos = array(
							'id_ficha' => $id_ficha,
							'user_id' => $this->session->userdata('id_user'),
							'user_perfil' => $this->session->userdata('id_perfil')
						);
				
						$dataLibro = $this->modelo->getDatosLibro($arr_datos);
						$dataAlumnos = $this->modelo->getAlumnosLibro($arr_datos);
						$GLOBALS['num_ficha'] = $dataLibro[0]["num_ficha"];
						$ID_sence = "";
						switch ($opcion_sence) {
							case "0": $ID_sence = "";
								break;
							case "1": $ID_sence = $dataLibro[0]["id_consolidado"];
								break;
							case "2": $ID_sence = $dataLibro[0]["id_sence"];
								break;
						}
				
						$this->load->library('Pdf');
						$pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
						$pdf->SetCreator(PDF_CREATOR);
						$pdf->SetAuthor('Edutecno Capacitación');
						$pdf->SetTitle('Encuesta de Satisfacción');
						$pdf->SetSubject('Tutorial TCPDF');
						$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
				
						// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
						// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
						$pdf->setFooterData($tc = array(
							0,
							64,
							0
								), $lc = array(
							0,
							64,
							128
						));
						$pdf->setPrintHeader(false);
						$pdf->setPrintFooter(false);
				
						// datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
						// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
						$pdf->setFooterFont(Array(
							PDF_FONT_NAME_DATA,
							'',
							PDF_FONT_SIZE_DATA
						));
				
						// se pueden modificar en el archivo tcpdf_config.php de libraries/config
						$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
				
						// se pueden modificar en el archivo tcpdf_config.php de libraries/config
						// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
						$pdf->SetMargins(10, 5, 10);
						// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
						$pdf->SetFooterMargin(0);
				
						// se pueden modificar en el archivo tcpdf_config.php de libraries/config
						// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
						// relación utilizada para ajustar la conversión de los píxeles
						// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
						// ---------------------------------------------------------
						// establecer el modo de fuente por defecto
						$pdf->setFontSubsetting(true);
				
						// Establecer el tipo de letra
						// Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
						// Helvetica para reducir el tamaño del archivo.
						$pdf->SetFont('Helvetica', '', 14, '', true);
				
						// Añadir una página
						// Este método tiene varias opciones, consulta la documentación para más información.
						$pdf->AddPage('P');
				
						// fijar efecto de sombra en el texto
						$pdf->setTextShadow(array(
							'enabled' => true,
							'depth_w' => 0.2,
							'depth_h' => 0.2,
							'color' => array(
								196,
								196,
								196
							),
							'opacity' => 1,
							'blend_mode' => 'Normal'
						));
						$html = '<style>
						h3{
							text-align:center;
						}
						
						td {
							white-space: nowrap;
							height: 30px;
							} 
						table,tr,td{
							border: 1px solid black;
						}
						</style>';
						$html .= '
						<table>
							<tr><td width="25%" align="center"><img src="assets/img/logoEdutecno2.png" border="0" width="90"></td><td width="50%" align="center" style="line-height:40px;"><br><b>LINARES Y CIA. LTDA.</b></td><td width="25%"></td></tr>
							<tr style="font-size: 12px;"><td width="25%">Organismo Técnico de Capacitación</td><td width="50%" align="center">Formulario de Otorgamiento de Curso al Relator ANEXO 39</td><td width="25%">Código FOR-01</td></tr>
						</table>';
						$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
						$html = '<style>
								h3{
									text-align:center;
								}
								
								td {
									white-space: nowrap;
									height: 30px;
									}
								</style>';
						$html .= '	<div></div>	<h3>Formulario De otorgamiento De Curso al Relator</h3></br></br>';
						$pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
						$html1 = '<style>
						table{
							font-size: 10.5px;
						}
						td {
							white-space: nowrap;
							height: 30px;
							} 
						</style>';
						$html1 .='
						<table style="width:15%;"><tr><td><b>Ficha: </b></td><td>'.$dataLibro[0]['num_ficha'].'</td></tr></table>
								<table cellpadding="5">
									<tr><td align="left" width="11%"><b>Empresa: </b></td><td align="left" colspan="3" width="43%">'.$dataLibro [0] ["nombre_empresa"].'</td><td align="left" width="11%"><b>Sede: </b></td><td align="left" width="35%">'.$dataLibro [0] ["nombre_sede"].'</td></tr>
									<tr><td align="left" width="11%"><b>Curso SENCE: </b></td><td align="left" colspan="3" width="43%">'.$dataLibro [0] ["nombre_sence"].'</td><td align="left" width="11%"><b>Sala: </b></td><td align="left">'.$dataLibro [0] ["sala"].'</td></tr>
									<tr><td align="left" width="11%"><b>Curso Real: </b></td><td align="left" colspan="3" width="43%">'.$dataLibro [0] ["nombre_curso"].'</td><td align="left" width="11%"><b>Alumnos: </b></td><td align="left">'.$dataLibro [0] ["cant_alumno"].'</td></tr>
									<tr><td align="left" width="11%"><b>Horas Curso: </b></td><td align="left"width="16%">'.$dataLibro [0] ["duracion_curso"].'</td><td align="left" width="11%"><b>Código SENCE: </b></td><td align="left" width="16%">'.$dataLibro [0] ["codigo_sence"].'</td><td align="left"><b>Profesor: </b></td><td align="left">'.$dataLibro [0] ["nombre_relator"].'</td></tr>
									<tr><td align="left" width="11%"><b>Hora Inicio: </b></td><td align="left" width="16%">'.$dataLibro [0] ["hora_inicio"].'</td><td align="left" width="11%"><b>Hora Término: </b></td><td align="left" width="16%">'.$dataLibro [0] ["hora_termino"].'</td><td align="left"><b>Dias: </b></td><td align="left">'.$dataLibro [0] ["dias"].'</td></tr>
									<tr><td align="left" width="11%"><b>Fecha Inicio: </b></td><td align="left" width="16%">'.$dataLibro [0] ["fecha_inicio"].'</td><td align="left" width="11%"><b>Fecha Término: </b></td><td align="left" width="16%">'.$dataLibro [0] ["fecha_termino"].'</td></tr>
									<tr><td></td><td></td><td></td><td></td><td></td><td align="center" style="border-top:1px solid black;">Revisado</td></tr>
								</table>
								<table>
									<tr><td style="height: 15px;"><b>Observaciones: </b></td></tr>
									<tr><td style="border: 1px solid black;">'.$dataLibro [0] ["observaciones"].'</td></tr>
								</table>';
						$pdf->writeHTMLCell($w = 195, $h = 0, $x = '', $y = '', $html1, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
						$html2 = '<style>
						table,tr,td{
							border: 0.5px solid black;
						}
						td {
							white-space: nowrap;
							height: 30px;
							} 
						</style>';
						$html2 .='
						<div></div>	
						<h5>Listado de Alumnos</h5>
								<table style="font-size: 12px;">
									<tr><td width="4%" align="center"></td><td><b>RUT</b></td><td><b>Nombre</b></td></tr>';
									for ($i=0; $i < count($dataAlumnos); $i++) { 
										$html2 .= '<tr><td align="center">';
										$html2 .= $i + 1;
										$html2 .= '</td>';
										$html2 .= '<td>';
										$html2 .= trim($dataAlumnos[$i]['rut']);
										$html2 .= '</td>';
										$html2 .= '<td>';
										$html2 .= $dataAlumnos[$i]['nombre_alumno'];
										$html2 .= '</td>';
										$html2 .= '</tr>';
									}
						$html2 .='</table>';
						$pdf->writeHTMLCell($w = 195, $h = 0, $x = '', $y = '', $html2, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
						// $pdf->Cell(0, 0, 'LIBRO DE CLASES', 1, 1, 'C', 0, '', 4);
						// $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);
						
						// ---------------------------------------------------------
						// Cerrar el documento PDF y preparamos la salida
						// Este método tiene varias opciones, consulte la documentación para más información.
						$nombre_archivo = utf8_decode("Libro de Clases Ficha N° " . $GLOBALS['num_ficha'] . ".pdf");
						$pdf->Output($nombre_archivo, 'I');
					}

}
