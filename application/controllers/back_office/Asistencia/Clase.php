<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Clase extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Clase';
    private $nombre_item_plural = 'Clase';
    private $package = 'back_office/Asistencia';
    private $model = 'Lista_model';
    private $view = 'Clase_v';
    private $controller = 'Listar';

    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2017-01-11
    // Controlador del panel de control
    public function index($id_sala = false, $id_detalle_sala = false) {
        if ($id_sala == false && $id_detalle_sala == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );



        // array con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Asistencia/clase.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Lista diaria";
        $data['menus'] = $this->session->userdata('menu_usuario');



        ///  fin carga menu
        $data['sala'] = $id_sala;
        $data['id_detalle_sala'] = $id_detalle_sala;

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function registrarClase() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */


        $finicio = $this->input->post('fecha_inicio');
        $hinicio = $this->input->post('txt_hora_inicio');

        $htermino = $this->input->post('txt_hora_termino');
        $finiciof = date("Y-m-d", strtotime($finicio));

        $id_sala = $this->input->post('sala');
        $id_detalle_sala = $this->input->post('id_detalle_sala');

        $inicio_completa = $finiciof . ' ' . $hinicio . ':00';
        $termino_completa = $finiciof . ' ' . $htermino . ':00';


        $arr_input_data = array(
            'id_sala' => $id_sala,
            'id_detalle_sala' => $id_detalle_sala,
            'fecha_inicio' => $inicio_completa,
            'fecha_termino' => $termino_completa
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->set_arr_insert_new_class($arr_input_data);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
