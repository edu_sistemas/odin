<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar';
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Asistencia';
    private $model = 'Lista_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');



        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2017-01-11
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );



        // array con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            //array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Asistencia/listar.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            //array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js')
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.min.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Lista diaria";
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getfichaData() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        $data = $this->modelo->get_arr_listar_capsulas($arr_input_data);
        echo json_encode($data);
        // informacion adicional para la pagina
    }

    function getRelatoresCBX() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );



        $data = $this->modelo->get_arr_listar_relatores($arr_input_data);
        echo json_encode($data);


        // informacion adicional para la pagina
    }

    function getFichas() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_ficha_reserva_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function capsulasListar() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_ficha' => $this->uri->segment(5)
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_listar_capsulas($arr_input_data);
        echo json_encode($datos);
    }

    function editarSala() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');


        $datos = $this->modelo->get_arr_listar_sala_editar_horario($arr_input_data);
        echo json_encode($datos);
    }

    /*function editarSalaSave() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         

        $arr_input_data = array(
            'sala' => $this->input->post('sala_edit'),
            'detalle_sala' => $this->input->post('detalle_sala'),
            'disponibilidad_detalle' => $this->input->post('disponibilidad_detalle')
        );
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');


        $datos = $this->modelo->set_sala_editar($arr_input_data);
        echo json_encode($datos);
    }*/

    function eliminarSala() {
        $arr_input_data = array(
            'id_capsula' => $this->input->post('id_c')
        );
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');


        $datos = $this->modelo->set_sala_eliminar_bloque($arr_input_data);
        echo json_encode($datos);
    }

    function editarHorario() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_capsula' => $this->input->post('id_capsula')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');


        $datos = $this->modelo->get_arr_listar_capsulas_editar_horario($arr_input_data);
        echo json_encode($datos);
    }

    function editarHorarioSave() {

        /*
          Ultima fecha Modificacion: 2017-07-21
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */
        $id_capsula = $this->input->post('capsule_id');
        $fInicio = $this->input->post('fecha_inicio_edit');
        $hInicio = $this->input->post('txt_hora_inicio_editar');
        $hTermino = $this->input->post('txt_hora_termino_editar');

        $finiciof = date("Y-m-d", strtotime($fInicio));

        $arr_input_data = array(
            'id_capsula' => $id_capsula,
            'fInicio' => $finiciof,
            'hInicio' => $hInicio,
            'htermino' => $hTermino
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->get_arr_listar_capsulas_editar_horario_save($arr_input_data);
        echo json_encode($datos);
    }

    function alumnosListar() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */



        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'id_capsula' => $this->input->post('id_capsula')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
//
        $datos = $this->modelo->get_arr_listar_alumnos($arr_input_data);
        echo json_encode($datos);
    }

    function alumnosRegistrarLista() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $input = $_POST['numRows'];



        $arr = array();
        $arratr = array();
        $arr_dtla = array();

        for ($var = 0; $var < $input; $var++) {

            if (isset($_POST['chkAst' . $var])) {

                array_push($arr, "1");
            } else {


                array_push($arr, "0");
            }

            if (isset($_POST['chkatrasado' . $var])) {

                array_push($arratr, "1");
            } else {


                array_push($arratr, "0");
            }

            if (isset($_POST['id_alm' . $var])) {

                array_push($arr_dtla, $_POST['id_alm' . $var]);
            } else {


                array_push($arr_dtla, $_POST['id_alm' . $var]);
            }

            //$_POST['chkAst'.$var];
        }
//                 

        $arr_input_data = array(
            'chk' => $arr,
            'chk_atrs' => $arratr,
            'numRows' => $this->input->post('numRows'),
            'capsulainput' => $this->input->post('capsulainput'),
            'id_dtl_relator_ficha' => $this->input->post('select_relator'),
            'id_alm' => $arr_dtla
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $datos = $this->modelo->set_arr_registrar_asistencia($arr_input_data);
        echo json_encode($datos);
    }

    function alumnoExtraRegistrar() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */
        $id_orden_compra = $this->input->post('id_oc');
        $id_ficha = $this->input->post('id_ficha_alm');
        $id_capsula = $this->input->post('id_capsula_alm');
        $id_relator = $this->input->post('id_relator_alm');
        $rut = $this->input->post('txt_rut');
        $dv = $this->input->post('txt_dv');
        $rut_completo = $rut . '-' . $dv;
        $nombre = $this->input->post('txt_nombre');
        $apellido = $this->input->post('txt_apellido');


        $arr_input_data = array(
            'orden_compra' => $id_orden_compra,
            'id_ficha' => $id_ficha,
            'id_capsula' => $id_capsula,
            'id_relator' => $id_relator,
            'rut' => $rut,
            'dv' => $dv,
            'usuario_alumno' => $rut_completo,
            'nombre' => $nombre,
            'apellido' => $apellido
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');


        $datos = $this->modelo->get_arr_registrar_alumnos_new($arr_input_data);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
