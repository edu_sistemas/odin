<?php

/**
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  
 * Fecha creacion:  2017-13-15 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class InformeDistancia extends MY_Controller {

    // Variables paramétricas

    private $nombre_item_singular = 'InformeDistancia';
    private $nombre_item_plural = 'InformeDistancia';
    private $package = 'back_office/Asistencia';
    private $model = 'InformeDistancia_model';
    private $view = 'InformeDistancia_v';
    private $controller = 'InformeDistancia';
    private $ind = '';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
    }

    public function index() {

        // / array ubicaciones

        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/InformeDistancia',
                'label' => 'Informe Curso Distancia',
                'icono' => ''
            )
        );
        // array con los css necesarios para la pagina

        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );
        // js necesarios para la pagina

        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Asistencia/InformeDistancia.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js')
        );
        // informacion adicional para la pagina

        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Informe Distancia";
        $data['menus'] = $this->session->userdata('menu_usuario');

        // / fin carga menu
        // $data ['fichas'] = $this->modelo->getFichas ();

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function fichas() {
        $arr_data = array(
            'num_ficha' => $this->input->post('num_ficha'),
            'empresa' => $this->input->post('empresa'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getFichas($arr_data);
        echo json_encode($datos);
    }

    function getEmpresasCBX() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_sp_empresa_select_cbx_informe_distancia($arr_data);
        echo json_encode($datos);
    }

    function buscarInforme() {
        // variables de sesion
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getInformes($arr_data);
        echo json_encode($datos);
    }

    public function generar() {
        $id_ficha = $this->uri->segment(5);
        $id_orden_compra = $this->uri->segment(6);
        $id_centro_costo = $this->uri->segment(7);
        $id_centro_costo2 = $this->uri->segment(8);
        $notas_value = $this->uri->segment(9);
        $asistencia_value = $this->uri->segment(10);


        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'id_orden_compra' => $id_orden_compra,
            'id_centro_costo' => $id_centro_costo,
            'id_centro_costo2' => $id_centro_costo2,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $dataInforme = $this->modelo->getDatosInformed($arr_datos);
        $dataAlumnos = $this->modelo->getAlumnosInforme($arr_datos);

        $GLOBALS['num_ficha'] = $dataInforme[0]["num_ficha"];
        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitación');
        $pdf->SetTitle('Informe Distancia');
        $pdf->SetSubject('Informe Presencial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

        $PDF_HEADER_LOGO = "logoEdutecno.png"; //any image file. check correct path.
        $PDF_HEADER_LOGO_WIDTH = "20";
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING = "                                                                                       "
                . "                                   " . $dataInforme[0]["num_ficha"];

        // $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);

        $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE . ' ', $PDF_HEADER_STRING, array(0, 0, 0), array(0, 64, 128));
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(false);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 2, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto

        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.

        $pdf->SetFont('Helvetica', '', 14, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.

        $pdf->AddPage('P');

        // fijar efecto de sombra en el texto

        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        // Establecemos el contenido para imprimir
        // $ficha = $this->input->post('fichas');
        // $fichas = $this->modelo->getFichas();
        // foreach($fichas as $ficha)
        // {
        // $prov = $ficha['num_ficha'];
        // }
        // echo json_encode($dataInforme;
        // preparamos y maquetamos el contenido a crear

        $html = '';
        $html .= '
				<style>
				.main_title{
					text-align: center;
					font-size: 85%;
				}
				.table_border td{
					border: 1px solid black;
					font-size: 75%;
					padding: 10%;
				}
				.cuadrado{
					border: 1px solid black;
					width: 20px;
					height: 20px;
				}
				table{
					padding-left: 5px;
				}
				.celda_bold{
					font-weight: bold;
				}
				.tipo_actividad{
					font-size: 90%;
				}
				.table_participantes{
					table-layout:fixed;
				}
				.table_participantes th{
					border-top: 1px solid black;
					border-bottom: 1px solid black;
					font-weight: bold;
					font-size: 70%;
				}
				.table_participantes td{
					font-size: 70%;
				}
		 
				.pie_de_firma {
					width: 35%;
					border: 1px solid black;
					font-weight: bold;
					font-size: 70%;
			 
				}
				
				.pie_de_fecha {
					width: 31%;
					border: 0.5px solid black;
					font-weight: bold;
					font-size: 70%;
			 
				}
				
				.fecha_emision{
				    float: right;
		 			font-weight: bold;
					font-size: 60%;
					width: 30%; 
					border: 0.5px solid black;
				} 
 
				</style>
				<table>
					<tr class="main_title"><td>CERTIFICADO DE ASISTENCIA ACTIVIDAD OTEC, CFT O ENTIDAD NIVELADORA DE ESTUDIOS, IMPUTADA EN FORMA TOTAL O PARCIAL 
				A FRANQUICIA TRIBUTARIA DE CAPACITACIÓN</td></tr>	
					<tr><td></td></tr>
				  </table>
				
				<table class="tipo_actividad">
				<tr><td class="cuadrado">X</td><td>Actividad dentro del año calendario</td></tr>
				<tr><td></td><td></td></tr>
				</table>
				
				<table class="tipo_actividad">
				<tr><td class="cuadrado"></td><td>Actividad parcial</td></tr>
				<tr><td></td><td></td></tr>
				</table>
				
				<table class="tipo_actividad">
				<tr><td class="cuadrado"></td><td>Actividad complementaria</td></tr>
				<tr><td></td><td></td></tr>
				</table>
				
				<table class="table_border">
					<tr><td class="celda_bold">Razón social OTEC, CFT o entidad niveladora</td><td>' . $dataInforme[0]["razon_social_edutecno"] . '</td></tr>
					<tr><td class="celda_bold">RUT OTEC, CFT o entidad niveladora</td><td>' . $dataInforme[0]["rut_otec"] . '</td></tr>
					<tr><td class="celda_bold">Razón social empresa</td><td>' . $dataInforme[0]["nombre_empresa"] . '</td></tr>
					<tr><td class="celda_bold">RUT Empresa</td><td>' . $dataInforme[0]["rut_empresa"] . '</td></tr>
					<tr><td class="celda_bold">Razón social OTIC</td><td>' . $dataInforme[0]["nombre_otic"] . '</td></tr>
					<tr><td class="celda_bold">RUT OTIC</td><td>' . $dataInforme[0]["rut_otic"] . '</td></tr>
					<tr><td class="celda_bold">Nombre de la actividad[1]</td><td>' . $dataInforme[0]["nombre_curso"] . '</td></tr>
					<tr><td class="celda_bold">Código Sence</td><td>' . $dataInforme[0]["codigo_sence"] . '</td></tr>
					<tr><td class="celda_bold">Fecha Inicio</td><td>' . $dataInforme[0]["fecha_inicio"] . '</td></tr>
					<tr><td class="celda_bold">Fecha de término</td><td>' . $dataInforme[0]["fecha_termino"] . '</td></tr>
					<tr><td class="celda_bold">N° de horas (para actividades parciales o complementarias indicar numero efectivo de horas realizadas en el año correspondiente)</td><td>' . $dataInforme[0]["duracion_curso"] . '</td></tr>
					

                             <tr><td class="celda_bold">N° de factura</td><td>';

        if ($id_centro_costo != 3) {
            $html .= $dataInforme[0]["facturas"];
        } else {
            $html .= '';
        }

        $html .= '</td></tr>
					<tr><td class="celda_bold">N° registro de acción Sence</td><td>';
        if ($id_centro_costo != 3) {
            $html .= $dataInforme[0]["id_sence"];
        } else {
            $html .= '';
        }


        $html .= '</td></tr> 	
                                            
					<tr><td class="celda_bold">N° de O/C</td><td>' . $dataInforme[0]["num_orden_compra"] . '</td></tr>
				</table>
				<table>
				<tr><td></td><td></td></tr>
				</table>
				
				<table class="table_participantes">
					<tr><th>N°</th><th>RUT</th><th>Apellido Paterno</th><th>Apellido Materno</th><th>Nombres</th><th>Asistencia</th>';
        if ($notas_value == 1) {
            $html .= '<th>Nota final</th> ';
        }
        $html .= ' </tr>';

        for ($i = 0; $i < count($dataAlumnos); $i++) {
            $html .= '
									<tr>
										<td>' . ($i + 1) . '</td>
										<td>' . $dataAlumnos[$i]["rut"] . '</td>
										<td>' . $dataAlumnos[$i]["apellido_paterno"] . '</td>
										<td>' . $dataAlumnos[$i]["apellido_materno"] . '</td>
										<td>' . $dataAlumnos[$i]["nombre"] . '</td>';
            switch ($asistencia_value) {
                case 0:
                    $html .= '<td>' . $dataAlumnos [$i] ["asistencia"] . '</td>';
                    break;
                case 1:
                    $html .= '<td>' . $dataAlumnos [$i] ["asistencia"] . '</td>';
                    break;
                case 2:
                    $html .= '<td>' . $dataAlumnos [$i] ["asistencia"] . '</td>';
                    break;
            }
            if ($notas_value == 1) {
                $html .= '<td>' . $dataAlumnos [$i] ["promedio"] . '</td>';
            }
            $html .= '</tr>';
        }
        $html .= '
						<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
						<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
						</table>';


        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pagina = intval($pdf->getPage()) + 1;
        $pdf->AddPage('P');
        $pdf->setPage($pagina);

        $html2 = '<style>
                        .main_title{
                            text-align: center;
                            font-size: 85%;
                        }
                        .table_border td{
                            border: 1px solid black;
                            font-size: 75%;
                            padding: 10%;
                        }
                        .cuadrado{
                            border: 1px solid black;
                            width: 20px;
                            height: 20px;
                        }
                        table{
                            padding-left: 5px;
                        }
                        .celda_bold{
                            font-weight: bold;
                        }
                        .tipo_actividad{
                            font-size: 90%;
                        }
                        .table_participantes{
                            table-layout:fixed;
                        }
                        .table_participantes th{
                            border-top: 1px solid black;
                            border-bottom: 1px solid black;
                            font-weight: bold;
                            font-size: 70%;
                        }
                        .table_participantes td{
                            font-size: 70%;
                        }
                        .pie_de_firma {
                            width: 35%;
                            border: 1px solid black;
                            font-weight: bold;
                            font-size: 70%;
                     
                        }
                        
                        .pie_de_fecha {
                            width: 31%;
                            border: 0.5px solid black;
                            font-weight: bold;
                            font-size: 70%;
                     
                        }
                        .fecha_emision{
                            width: 35%;
                            font-size: 70%;
                            border: 0.5px solid black;
                            height: 50px;
                        }
                        </style>';
        if ($dataInforme[0]["firma"] == "Vitalia") {
            $html2 .= '<br><br>
					<table>
						<tr>
							<td class="pie_de_firma">Firma representante legal OTEC, CFT o entidad niveladora[1]</td>
							<td class="pie_de_firma"></td>
						</tr>
						<tr>
							<td class="pie_de_firma">Nombre representante legar OTEC, CFT o entidad niveladora</td>							
							<td class="pie_de_firma">Vitalia Linares Oyarzún</td>
						</tr>
						<tr>
							<td class="pie_de_firma">RUT representante legal OTEC, CFT o entidad niveladora</td>
							<td class="pie_de_firma">10.017.057-4</td>
							<td width="3%"></td>
							<td class="pie_de_fecha">Fecha de Emisión :' . date('d-m-Y') . '</td>
						</tr>
					</table>';
        }

        if ($dataInforme[0]["firma"] == "Carlos") {
            $html2 .= '<br><br>
					<table>						
						<tr>
							<td class="pie_de_firma">Firma representante legal OTEC, CFT o entidad niveladora[1]</td>
							<td class="pie_de_firma"></td>
						</tr>
						<tr>
							<td class="pie_de_firma">Nombre representante legar OTEC, CFT o entidad niveladora</td>							
							<td class="pie_de_firma">Carlos Linares Oyarzún</td>
						</tr>
						<tr>
							<td class="pie_de_firma">RUT representante legal OTEC, CFT o entidad niveladora</td>
							<td class="pie_de_firma">10.067.581-1</td>
							<td width="3%"></td>
							<td class="pie_de_fecha">Fecha de Emisión :' . date('d-m-Y') . '</td>
						</tr> 
					</table>';
        }
        $html2 .= ''
                . '';

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html2, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);


        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.

        $nombre_archivo = utf8_decode("Informe_distancia_ficha" . $GLOBALS['num_ficha'] . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

    function generarDJ_2() {

        $id_ficha = $this->uri->segment(5);
        $id_orden_compra = $this->uri->segment(6);
        $id_centro_costo = $this->uri->segment(7);
        $id_centro_costo2 = $this->uri->segment(8);

        setlocale(LC_ALL, 'Spanish_Chile');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'id_orden_compra' => $id_orden_compra,
            'id_centro_costo' => $id_centro_costo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $dataInforme = $this->modelo->getDatosInformed($arr_datos);
        $dataAlumnos = $this->modelo->sp_informe_distancia_get_datos_dejota($arr_datos);

        $GLOBALS['num_ficha'] = $dataInforme[0]["num_ficha"];
        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitación');
        $pdf->SetTitle('Informe Distancia');
        $pdf->SetSubject('Informe Presencial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

        $PDF_HEADER_LOGO = "logoEdutecno.png"; //any image file. check correct path.
        $PDF_HEADER_LOGO_WIDTH = "20";
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING = "                                                                                       "
                . "                                   " . $dataInforme[0]["num_ficha"];


        // $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);

        $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE . ' ', $PDF_HEADER_STRING, array(0, 0, 0), array(0, 64, 128));
        $pdf->setPrintHeader(true);
        $pdf->setPrintFooter(false);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP + 2, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto

        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.

        $pdf->SetFont('Helvetica', '', 14, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.

        $pdf->AddPage('P');

        // fijar efecto de sombra en el texto

        $pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        // Establecemos el contenido para imprimir
        // $ficha = $this->input->post('fichas');
        // $fichas = $this->modelo->getFichas();
        // foreach($fichas as $ficha)
        // {
        // $prov = $ficha['num_ficha'];
        // }
        // echo json_encode($dataInforme;
        // preparamos y maquetamos el contenido a crear

        $html = '';
        $html .= '
                <style>
                .main_title{
                    text-align: center;
                    font-size: 75%;
                }
                .texto_superior {
                    font-size: 75%;
                    padding: 3%;
                }
         
                table{
                    padding-left: 5px;
                }
                .celda_bold{
                    font-weight: bold;
                }
                .tipo_actividad{
                    font-size: 90%;
                }
                .table_participantes{
                    table-layout:fixed;
                }
                .table_participantes th{

                    border-top: 1px solid black;
                    border-bottom: 1px solid black;
                    font-weight: bold;
                    font-size: 70%;
                }
                .table_participantes td{
                    font-size: 70%;
                }
         
                .pie_de_firma {
                    width: 35%;
                    border: 1px solid black;
                    font-weight: bold;
                    font-size: 70%;
             
                }
                
                .pie_de_fecha {
                    width: 31%;
                    border: 0.5px solid black;
                    font-weight: bold;
                    font-size: 70%;
             
                }
                
                .fecha_emision{
                    float: right;
                    font-weight: bold;
                    font-size: 60%;
                    width: 30%; 
                    border: 0.5px solid black;
                } 
                .segundo_texto{
                font-size: 75%;
                }
                
                .firma{
                
                font-size: 70%;
                text-align: center;
                
                }
             
 
                </style>
                ';
        if (count($dataAlumnos) >= 1) {

            $html .= ' 
                <table>
                <tr class="main_title">
                    <td>Declaraciones Juradas</td>
                </tr>   
                  </table>
                
                <table class="texto_superior">
                <tr>';


            $html .= '<td><p> En Santiago a ' . strftime("%A %d %B %Y") . ', yo <b>Carlos Linares Oyarzún</b>, RUT <b>10.067.581-1</b>, de profesión <b>Ingeniero Textil</b>, 
                    domiciliado en <b>San Alberto 481</b>, comuna de <b>Las Condes</b> en representación del Organismo Técnico de Capacitación <b>Linares y Moreira
                    Ltda.</b> RUT <b>78.710.830-K</b>, vengo a declarar bajo juramento que don (ña):</p></td>
                </tr>
                </table>
                
                <table class="table_participantes">
                    <tr>
                        <th style="width:6%;">N°</th>
                        <th style="width:27%;">Apellido Paterno</th>
                        <th style="width:27%;">Apellido Materno</th>
                        <th style="width:20%;">Nombres</th><th>RUT</th>
                    </tr>';

            for ($i = 0; $i < count($dataAlumnos); $i++) {
                $html .= '
                                    <tr>
                                        <td>' . ($i + 1) . '</td>
                                        <td>' . $dataAlumnos[$i]["apellido_paterno"] . '</td>
                                        <td>' . $dataAlumnos[$i]["apellido_materno"] . '</td>
                                        <td>' . $dataAlumnos[$i]["nombre"] . '</td> 
                                        <td>' . $dataAlumnos[$i]["rut"] . '</td>
                                    </tr>';
            }

            $html .= '
                        <tr>
                        <td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
                        <tr><td></td><td></td><td></td><td></td><td></td><td></td>
                        <td></td>
                        </tr>
                        </table>
                <p class="segundo_texto">ha/n efectuado el curso <b>' . $dataAlumnos[0]["nombre_curso"] . ',</b> código Sence <b>' . $dataAlumnos[0]["codigo_sence"] . '</b>,'
                    . ' entre las fechas  <b>' . $dataAlumnos[0]["inicio"] . '</b> y <b>' . $dataAlumnos[0]["termino"] . '</b>, y que las actividades de capacitación se cumplieron'
                    . 'a cabalidad en los términos autorizados por el Servicio Nacional de Capacitación y Empleo, -Sence- especialmente respecto de las horas'
                    . 'cronológicas del curso y su distribución entre horas teóricas y prácticas, de acuerdo al artículo 15 del Decreto Supremo N° 98 de 1997, del Ministerio'
                    . 'del Trabajo y Prevision Social.</p>
                 <table><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
                        <tr><td></td><td></td><td></td><td></td><td></td><td></td>
                        <td></td>
                        </tr>
                        </table>
                        
                    <p class="firma">___________________________________</p>
                    <p class="firma">Representante Legal OTEC</p>
                    
                <table><tr><td></td><td></td><td></td><td></td><td></td></tr>
    
                </table>
                <p class="segundo_texto">Nota: La falsedad en la presente declaración dará lugar a que el Servicio Nacional de Capacitación y Empleo
                efectúe la correspondiente denuncia ante el Ministerio público por los delitos conteplados en los artículos 197, 198, 212 y 470 N°8 del Código Penal.<p>';
        } else {
            $html .= '<p>Ejecutivo Comercial no ha especificado los alumnos con declaraciones juradas.</p>';
        }

        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);

        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.

        $nombre_archivo = utf8_decode("Informe_dj_Ficha" . $GLOBALS['num_ficha'] . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }


    function generarDJ() {

        $id_ficha = $this->uri->segment(5);
        $id_modalidad = $this->uri->segment(6);
        $id_orden_compra = $this->uri->segment(7);
        $id_centro_costo = $this->uri->segment(8);
        $id_centro_costo2 = $this->uri->segment(9);

        setlocale(LC_ALL, 'Spanish_Chile');
        // setlocale(LC_ALL, 'es_cL');
        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'id_orden_compra' => $id_orden_compra,
            'id_centro_costo' => $id_centro_costo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $dataInforme = $this->modelo->getDatosInformed($arr_datos);
        $dataAlumnos = $this->modelo->sp_informe_distancia_get_datos_dejota($arr_datos);

        $GLOBALS['num_ficha'] = $dataInforme[0]["num_ficha"];
        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitación');
        $pdf->SetTitle('Informe Distancia');
        $pdf->SetSubject('Informe Presencial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

        $PDF_HEADER_LOGO = "logoEdutecno.png"; //any image file. check correct path.
        $PDF_HEADER_LOGO_WIDTH = "20";
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING = "                                                                                       "
                . "                                   " . $dataInforme[0]["num_ficha"];


        // $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);

        $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE . ' ', $PDF_HEADER_STRING, array(0, 0, 0), array(0, 64, 128));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config

        $pdf->SetMargins(30,30,30);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto

        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.

        $pdf->SetFont('Helvetica', '', 12, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.

        $pdf->AddPage('P');

        // fijar efecto de sombra en el texto

        $pdf->setTextShadow(array('enabled' => false, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

        // Establecemos el contenido para imprimir
        // $ficha = $this->input->post('fichas');
        // $fichas = $this->modelo->getFichas();
        // foreach($fichas as $ficha)
        // {
        // $prov = $ficha['num_ficha'];
        // }
        // echo json_encode($dataInforme;
        // preparamos y maquetamos el contenido a crear
        if($id_modalidad == 6){    
        $html = '';
        $html .= '
                <style >
               
               
				.main_title{
					text-align: center;
					font-size: 100%;
                    font-weight:bold;


				}
               
				.texto_superior {
					font-size: 95%;
                    line-height:21px;
					
				}
		 
				table{
					padding-left: 5px;
				}
				.celda_bold{
					font-weight: bold;
				}
				.tipo_actividad{
					font-size: 90%;
				}
				.table_participantes{
					table-layout:fixed;
				}
				.table_participantes th{

					border-top: 1px solid black;
					border-bottom: 1px solid black;
                    border-left :1px solid black;
                    border-right :1px solid black;
					font-weight: bold;
					font-size: 95%;
                    text-align: center;
				}
				.table_participantes td{
					font-size: 95%;
                    border-top: 1px solid black;
                    border-bottom: 1px solid black;
                    border-left :1px solid black;
                    border-right :1px solid black;
				}
		 
				.pie_de_firma {
					width: 35%;
					border: 1px solid black;
					font-weight: bold;
					font-size: 70%;
			 
				}
				
				.pie_de_fecha {
					width: 31%;
					border: 0.5px solid black;
					font-weight: bold;
					font-size: 70%;
			 
				}
				
				.fecha_emision{
				    float: right;
		 			font-weight: bold;
					font-size: 60%;
					width: 30%; 
					border: 0.5px solid black;
				} 
				.segundo_texto{
				font-size: 95%;
                line-height:21px;
				}
				.segundo_texto2{
                font-size: 80%;
                line-height:18px;
                }
				.firma{
				
				font-size: 95%;
				text-align: center;
				
                }
                .agregar{
                    font-size: 80%;
                    text-align: center;
                }
                .pequeña{
                    font-size: 60%;
                }

			 
 
				</style>
				';
        if (count($dataAlumnos) >= 1) {

            $html .= ' 
				<table>
				<tr class="main_title">
					<td>DECLARACIÓN JURADA <br>
ORGANISMO TÉCNICO DE CAPACITACIÓN (OTEC) <br><br>
Acreditación de asistencia y cumplimiento de acción de capacitación en modalidad a distancia
</td>
				</tr>	
				  </table><span><br> </span>
				
				<table class="texto_superior" border="0">
				<tr>';



            $html .= '<td><p style="text-align:justify">En Santiago a <b>' . $dataInforme[0]["fecha_informe"] . '</b>, quien suscribe <b>' . $dataInforme[0]["nombre_representante_legal"] . '</b>, cédula de identidad N° <b>' . $dataInforme[0]["rut_representante_legal"] . '</b>, Representante Legal del organismo técnico de capacitación <b>' . $dataInforme[0]["razon_social_edutecno"] . '</b> RUT N° <b>' . $dataInforme[0]["rut_otec"] . '</b>, ambos con domicilio en <b>' . $dataInforme[0]["domicilio_representante_legal"] . '</b>, comuna de <b>' . $dataInforme[0]["comuna_representante_legal"] . '</b>, ciudad de <b>' . $dataInforme[0]["ciudad_representante_legal"] . '</b>, Región <b>' . $dataInforme[0]["region_representante_legal"] . '</b>, dando cumplimiento a lo exigido en el artículo 29 del Decreto Supremo N°98 de 1997, del Ministerio y Previsión Social vengo en declarar que las personas a continuación individualizadas:</p></td>
				</tr>
				</table>
				<span><br> </span>
				<table class="table_participantes">
					<tr>
						
						<th style="width:27%;">Apellido paterno</th>
						<th style="width:27%;">Apellido materno</th>
                        <th style="width:20%;">Nombres</th>
                        <th>N° cédula identidad</th>
					</tr>';

            for ($i = 0; $i < count($dataAlumnos); $i++) {
                $html .= '
									<tr>
										
										<td>' . $dataAlumnos[$i]["apellido_paterno"] . '</td>
										<td>' . $dataAlumnos[$i]["apellido_materno"] . '</td>
										<td>' . $dataAlumnos[$i]["nombre"] . '</td> 
										<td>' . $dataAlumnos[$i]["rut"] . '</td>
									</tr>';
            }

            $html .= '
						<tr>
						<td></td><td></td><td></td><td></td></tr>
						
						</table>
                        <span><br></span>
                        
                        <table class="segundo_texto" border="0">
                        <tr><td>

                <p class="" style="text-align:justify">Participaron en el curso ' . $dataAlumnos[0]["nombre_curso"] . ', código SENCE N° ' . $dataAlumnos[0]["codigo_sence"] . ', ID acción N° '. $dataInforme[0]["id_accion_sence"] . ', desarrollado entre el ' . $dataAlumnos[0]["inicio"] . ' y ' . $dataAlumnos[0]["termino"] . ',  y que la actividad de capacitación se cumplió a cabalidad en los términos autorizados por el Servicio Nacional de Capacitación y number Empleo <sup>1</sup> .</p> 
<br>
<p class="" style="text-align:justify">Asimismo, declaro que estoy en pleno conocimiento que la falsedad en la presente declaración, faculta al Servicio Nacional de Capacitación y Empleo para realizar la denuncia ante el Ministerio Público.  
</p>



                    </td></tr></table>
				 <table><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
						<tr><td></td><td></td><td></td><td></td><td></td><td></td>
						<td></td>
						</tr>
                        </table>
                        <table>
                        <tr><td style="text-align:center;"><span class="firma">_______________________</span></td></tr>
                        <tr><td style="text-align:center;"><span class="firma" align="center">Representante Legal</span></td></tr>
                        <tr><td style="text-align:center;"><span >OTEC "' .$dataInforme[0]["razon_social_edutecno"].'"</span></td></tr>
                        
                        </table>
                        <br><br>

						
					
        <table>
        <tr><td borderTop="1">
        <p style="text-align:justify; font-size:7px;">1 El declarar que la actividad de capacitación se desarrolló en los términos aprobados por el Servicio Nacional de Capacitación y Empleo, conlleva no sólo el cumplimiento de las horas cronológicas del curso y su distribución entre las horas teóricas y prácticas, de acuerdo a lo consignado en el artículo 15 del DS N°98 de 1998, del Ministerio del Trabajo y Previsión Social, sino que de igual forma, la entrega de material didáctico cuando la actividad de capacitación lo contemple y en esos términos hubiere sido autorizada. </p></td></tr><table>';
        } else {
            $html .= '<p>Ejecutivo Comercial no ha especificado los alumnos con declaraciones juradas.</p>';
        }
    }else{

        //aqui va el otro
                $html = '';
        $html .= '
				<style>
               
				.main_title{
					text-align: center;
					font-size: 100%;
                    text-decoration: underline;
                    font-weight:bold;


				}
               
				.texto_superior {
					font-size: 95%;
                    line-height:21px;
					
				}
		 
				table{
					padding-left: 5px;
				}
				.celda_bold{
					font-weight: bold;
				}
				.tipo_actividad{
					font-size: 90%;
				}
				.table_participantes{
					table-layout:fixed;
				}
				.table_participantes th{

					border-top: 1px solid black;
					border-bottom: 1px solid black;
                    border-left :1px solid black;
                    border-right :1px solid black;
					font-weight: bold;
					font-size: 95%;
                    text-align: center;
				}
				.table_participantes td{
					font-size: 95%;
                    border-top: 1px solid black;
                    border-bottom: 1px solid black;
                    border-left :1px solid black;
                    border-right :1px solid black;
				}
		 
				.pie_de_firma {
					width: 35%;
					border: 1px solid black;
					font-weight: bold;
					font-size: 70%;
			 
				}
				
				.pie_de_fecha {
					width: 31%;
					border: 0.5px solid black;
					font-weight: bold;
					font-size: 70%;
			 
				}
				
				.fecha_emision{
				    float: right;
		 			font-weight: bold;
					font-size: 60%;
					width: 30%; 
					border: 0.5px solid black;
				} 
				.segundo_texto{
				font-size: 95%;
                line-height:21px;
				}
				.segundo_texto2{
                font-size: 80%;
                line-height:18px;
                }
				.firma{
				
				font-size: 95%;
				text-align: center;
				
				}
			 
 
				</style>
				';
        if (count($dataAlumnos) >= 1) {

            $html .= ' 
				<table>
				<tr class="main_title">
					<td>DECLARACIÓN JURADA <br>
ORGANISMO TÉCNICO DE CAPACITACIÓN (OTEC) <br><br>
Acreditación de asistencia y cumplimiento de acción de capacitación en modalidad a distancia
</td>
				</tr>	
				  </table><span><br> </span>
				
				<table class="texto_superior" border="0">
				<tr>';



            $html .= '<td><p style="text-align:justify">En Santiago a ' . $dataInforme[0]["fecha_informe"] . ', quien suscribe <b>' . $dataInforme[0]["nombre_representante_legal"] . '</b>, cédula de identidad N° <b>' . $dataInforme[0]["rut_representante_legal"] . '</b>, Representante Legal del organismo técnico de capacitación <b>' . $dataInforme[0]["razon_social_edutecno"] . '</b> RUT <b>' . $dataInforme[0]["rut_otec"] . '</b>, ambos con domicilio en  <b>' . $dataInforme[0]["domicilio_representante_legal"] . '</b>, comuna de  <b>' . $dataInforme[0]["comuna_representante_legal"] . '</b>, ciudad de  <b>' . $dataInforme[0]["ciudad_representante_legal"] . '</b>, Región  <b>' . $dataInforme[0]["region_representante_legal"] . '</b>, dando cumplimiento a lo exigido en el artículo 29 del Decreto Supremo N°98 de 1997, del Ministerio y Previsión Social vengo en declarar que las personas a continuación individualizadas:</p></td>
				</tr>
				</table>
				<span><br> </span>
				<table class="table_participantes">
					<tr>
						
						<th style="width:27%;">Apellido paterno</th>
						<th style="width:27%;">Apellido materno</th>
						<th style="width:20%;">Nombres</th><th>RUT</th>
					</tr>';

            for ($i = 0; $i < count($dataAlumnos); $i++) {
                $html .= '
									<tr>
										
										<td>' . $dataAlumnos[$i]["apellido_paterno"] . '</td>
										<td>' . $dataAlumnos[$i]["apellido_materno"] . '</td>
										<td>' . $dataAlumnos[$i]["nombre"] . '</td> 
										<td>' . $dataAlumnos[$i]["rut"] . '</td>
									</tr>';
            }

            $html .= '
						<tr>
						<td></td><td></td><td></td><td></td></tr>
						
						</table>
                        <span><br></span>
                        
                        <table class="segundo_texto" border="0">
                        <tr><td>

				<p class="" style="text-align:justify">Participaron en el curso ' . $dataAlumnos[0]["nombre_curso"] . ', código SENCE N° ' . $dataAlumnos[0]["codigo_sence"] . ', ID acción N° '. $dataInforme[0]["id_accion_sence"] . ', desarrollado entre el ' . $dataAlumnos[0]["inicio"] . ' y ' . $dataAlumnos[0]["termino"] . ',  y que la actividad de capacitación se cumplió a cabalidad en los términos autorizados por el Servicio Nacional de Capacitación y Empleo . 

Asimismo, declaro que estoy en pleno conocimiento que la falsedad en la presente declaración, faculta al Servicio Nacional de Capacitación y Empleo para realizar la denuncia ante el Ministerio Público.  
</p>



                    </td></tr></table>
				 <table><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
						<tr><td></td><td></td><td></td><td></td><td></td><td></td>
						<td></td>
						</tr>
                        </table>
                        <table>
                        <tr><td style="text-align:center;"><span class="firma">___________________________________</span></td></tr>
                        <tr><td style="text-align:center;"><span class="firma" align="center">Representante Legal OTEC</span></td></tr>
                        </table>
                        <br><br>

						
					
        <table>
        <tr><td borderTop="1">
        <p style="text-align:justify"font-size:7px;">1 El declarar que la actividad de capacitación se desarrolló en los términos aprobados por el Servicio Nacional de Capacitación y Empleo, conlleva no sólo el cumplimiento de las horas cronológicas del curso y su distribución entre las horas teóricas y prácticas, de acuerdo a lo consignado en el artículo 15 del DS N°98 de 1998, del Ministerio del Trabajo y Previsión Social, sino que de igual forma, la entrega de material didáctico cuando la actividad de capacitación lo contemple y en esos términos hubiere sido autorizada. </p></td></tr><table>';
        } else {
            $html .= '<p>Ejecutivo Comercial no ha especificado los alumnos con declaraciones juradas.</p>';
        }
    }
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);

        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.

        $nombre_archivo = utf8_decode("Informe_dj_Ficha" . $GLOBALS['num_ficha'] . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

}
