
<?php

/**
 * Login_c
 * Description...
 * @version 0.0.1
 * Ultima edicion:	2016-07-14 [Carlos Aravena] <caravena@edutecno.cl>
 * Fecha creacion:	2016-06-01 [Carlos Aravena] <caravena@edutecno.cl>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    // Variables parametricas
    private $nombre_item_singular = 'Login Usuario';
    private $nombre_item_plural = 'Login Usuarios';
    private $package = '';
    private $package_dos = '';
    private $model = 'Login_model';
    private $view = 'Login_v';
    private $controller = 'Login';
    private $ind = '';

    // Inicio Construct
    function __construct() {
        parent::__construct();

        $this->load->model('back_office/' . $this->model, 'modelo');
        //  $this->load->model('menu_model');
        $this->load->library('form_validation');
        $this->load->library('AddForms_lib', 'addforms_lib');
        $this->load->library('session');
        $this->load->helper('form');
    }

    public function index() {

        $arr_config = array(
            array(
                'field' => 'f1',
                'label' => 'Usuario',
                'rules' => 'required',
                'type' => 'text_login',
                'placeholder' => 'Rut (Sólo números y dígito verificador)',
                'value' => ''
            ),
            array(
                'field' => 'f2',
                'label' => 'Contrase&ntilde;a',
                'rules' => 'required',
                'type' => 'password_login',
                'placeholder' => 'Contrase&ntilde;a',
                'value' => ''
            ),
            array(
                'field' => 'f3',
                'label' => 'Recordar contrase&ntilde;a',
                'rules' => '',
                'type' => 'checkbox_login',
                'value' => ''
            )
        );


        $data['arr_hidden_config'] = $this->addforms_lib->create_hidden($arr_config);
        $this->form_validation->set_rules($arr_config);

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><strong>Error!</strong> ', '</div>');

        if ($this->form_validation->run() === FALSE) {

            $data['arr_enlaces'] = array(
                'login' => base_url('') . $this->ind . 'back_office/' . $this->controller . '/index',
                'label_login' => 'Acceder'
            );



            $data['page_title'] = '';
            $data['page_title_small'] = '';
            $data['panel_title'] = $this->nombre_item_singular;

            //  $data['menus']                  = $this->menu_model->with('menu_item')->get_all();
            $data['page_menu'] = 'dashboard';
            $data['main_content'] = '/back_office/' . $this->package . '/' . $this->view;
            $data['arr_form_script'] = $this->addforms_lib->render_form($arr_config);
            $data['form_script'] = $this->addforms_lib->render_form_script($arr_config);
            $data['login'] = true;
            $data['style_adicional'] = array();
            $data['script_adicional'] = array();
            $this->load->view('amelia_login/template', $data);
        } else {
            // Escribe el arreglo para el update a través de codeigniter
            $arr_input_data = array(
                'user_name' => $this->input->post('f1'),
                'user_password' => $this->input->post('f2')
            );

            $arr_user_login = $this->modelo->get_arr_user_login($arr_input_data);

            if ($arr_user_login) {

                $arr_session_user = array(
                    'id_user' => $arr_user_login['id_user'],
                    'nombre_user' => $arr_user_login['nombre_user'],
                    'id_perfil' => $arr_user_login['id_perfil'],
                    'descrip_perfil' => $arr_user_login['descrip_perfil'],
                    'img_user' => $arr_user_login['imagen'],
                    'dashboard' => $arr_user_login['dashboard'],
                    'logged_in' => TRUE
                );

                $this->session->set_userdata($arr_session_user);

                redirect('/back_office/Home', 'refresh');

                //  redirect('https://www.google.cl/?gws_rd=ssl', 'refresh');
            } else {

                $this->session->set_flashdata('item', 'Error usuario y/o contraseña incorrecta');
                redirect('/back_office/Login', 'refresh');
            }
        }
    }

    function logout() {
        $this->session->set_flashdata('item2', 'Haz cerrado sesi&oacute;n');
        $this->session->sess_destroy();
        redirect('/back_office/Login', 'refresh');
    }

}

/* End of file Login_c.php */
/* Location: ./application/controllers/Login_c.php */
?>
