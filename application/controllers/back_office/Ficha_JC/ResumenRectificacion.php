<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-02 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-02 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ResumenRectificacion extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Ficha_JC';
    private $package2 = 'back_office/Ficha_Control';
    private $model = 'Ficha_JC_model';
    private $model2 = 'Ficha_Control_model';
    private $view = 'ResumenRectificacion_v';
    private $controller = 'ResumenRectificacion';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'fichacontrol');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_rectificacion = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_rectificacion == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Resumen',
                'label' => 'Resumen',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha_JC/resumenrectificacion.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Solicitudes";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        $arr_input_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id_rectificacionJC($arr_input_data);
        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_rectificacion_ficha_idJC($arr_input_data);

        $data['data_rectificacion'] = $this->modelo->get_estado_resctificacionesJC($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function cargaDatosOC() {

        $id_rectificacion = $this->input->post('id_rectificacion');
        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_datos_orden_compra_by_rectificacionJC($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumen() {

        $id_rectificacion = $this->input->post('id_rectificacion');
        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_datos_documentos_resumen_orden_compra_by_rectificacionJC($arr_data);
        echo json_encode($datos);
    }

    function aprobarRectificacion() {

        $id_rectificacion = $this->input->post('id_rectificacion');

        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_aprobar_resctificacionesJC($arr_data);
        echo json_encode($datos);
    }

    function rechazarRectificacion() {
        $id_rectificacion = $this->input->post('id_rectificacion_rechazo');
        $motivo_rechazo = $this->input->post('motivo_rechazo');
        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'motivo_rechazo' => $motivo_rechazo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_rechazar_resctificacionesJC($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosOCRectificacion() {
        $id_rectificacion = $this->input->post('id_rectificacion');
        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_datos_rectificacion_orden_compra_by_rectificacionJC($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumenRectificaciones() {

        $id_rectificacion = $this->input->post('id_rectificacion');
        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_datos_documentos_rectificacion_resumen_orden_compra_by_rectificacionJC($arr_data);
        echo json_encode($datos);
    }

    function enviaEmail() {


        $datos = $this->input->post('datos');

        $modalidad = $datos[0]['modalidad'];

        switch ($modalidad) {
            case 'elearning':
                $destinatarios = array(
                    array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                    array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );
                break;
            case 'presencial':
                $destinatarios = array(
                    array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                    array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                    array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                    array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola Salazar'),
                    array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );
                break;
        }


        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );

        $asunto = "Nueva Rectificación - Ficha N°: " . $datos[0]['num_ficha']; // asunto del correo
        // presenciales
        $body = $this->generaHtmlSolicitudRectificacion($datos[0]); // mensaje completo HTML o text

        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo json_encode($datos[0]);
    }

    function generaHtmlSolicitudRectificacion($data) {


        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimada Tatiana Watkins, ';
        $html .= '<br>';
        $html .= 'Se ha ingresado una nueva solicitud de rectificación.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['razon_social_empresa'] . ' </td>';
        $html .= '</tr>                                                                ';
        $html .= '<tr class="total" >';
        $html .= '<td>Tipo Rectificación</td>';
        $html .= '<td class="alignright"> ' . $data['tipo_rectificacion'] . ' </td>';
        $html .= '</tr>  ';
        $html .= '<tr >';
        $html .= '<td>Orden Compra</td>';
        $html .= '<td class="alignright"> ' . $data['num_orden_compra'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>N° Participantes </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr >';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por</td>';
        $html .= '<td class="alignright"> ' . $data['usuario_ingreso'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['curso'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['nombre_modalidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Motivo</td>';
        $html .= '<td class="alignright">' . $data['motivo_rectificacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    function enviaEmailRechazoRectificacion() {


        $datos = $this->input->post('datos');



        $destinatarios = array(
            array('email' => $datos[0]['correo_comercial'], 'nombre' => $datos[0]['nombre_ejecutivo']),
            array('email' => $datos[0]['creador_ficha'], 'nombre' => $datos[0]['creador_nombre'])
        );


        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );

        $asunto = "Nueva Rectificación - Ficha N°: " . $datos[0]['num_ficha']; // asunto del correo
        // presenciales
        $body = $this->generaHtmlRechazo($datos[0]); // mensaje completo HTML o text

        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

//		Lineas de codigo para probar el correo y los destinatarios	
//		print_r($destinatarios);
        echo json_encode($datos[0]);
    }

    function generaHtmlRechazo($data) {


        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_rojo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)  ';
        $html .= '<br>';
        $html .= 'Se ha Anulado la rectificación  Ficha N°: ' . $data['num_ficha'];
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright">' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr>';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_fin'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Sala</td>';
        $html .= '<td class="alignright"> ' . $data['nombre'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Participantes</td>';
        $html .= '<td class="alignright"> ' . $data['cantidad'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr  class="total">';
        $html .= '<td>Motivo de Rechazo de rectificación C. C. </td>';
        $html .= '<td class="alignright"> ' . $data['motivo_rectificacion_rechazo'] . '</td>';


        $html .= '</tr>';

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfano  863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function pdfResumen($id_ficha, $id_rectificacion, $num_orden_compra){

        $datos_req = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_ficha = $this->modelo->get_arr_ficha_by_id_rectificacionJC($datos_req);
        $data_orden_compra = $this->modelo->get_arr_orden_compra_by_rectificacion_ficha_idJC($datos_req);
        $data_rectificacion = $this->modelo->get_estado_resctificacionesJC($datos_req);
        $alumnos_por_rectificar = $this->modelo->get_arr_listar_datos_rectificacion_orden_compra_by_rectificacionJC($datos_req);

        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitacion');
        $pdf->SetTitle('Libro de Clases');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(
            0,
            0,
            0
                ), $lc = array(
            0,
            0,
            0
        ));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(0);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
		$pdf->addFont('frankie', '', 'frankie.php');
		$pdf->addFont('calibri', '', 'calibri.php');
		$pdf->SetFont('Helvetica', '', 14, '', false);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('P');

        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));
        
        $html= "";
        $html .= '
                <table width="100%" cellspacing="0" style="opacity:0.5; border-top: 1px solid gray;border-bottom: 1px solid gray;border-left: 1px solid gray;">
                <tr><td style="border-bottom: 1px solid gray;" width="30%">
                <img src="'.base_url().'assets/img/logoEdutecno2.png" width="120px">
                </td>
                <td width="70%">
                    <table width="100%" style="border-left: 1px solid gray;" cellspacing="0">
                        <tr style="line-height: 50px;"><td colspan="4" style="border-bottom: 1px solid gray;border-right: 1px solid gray;"><font>Informe rectificación</font></td></tr>
                        <tr style="line-height: 14px;"><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">Ficha N°</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">ID Rectificación</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">Estado Ficha</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">Fecha Consulta</font></td></tr>
                    </table>
                </td></tr>
                <tr><td width="30%"><font size="8">Organismo Técnico de capacitacion</font></td><td width="70%">
                <table style="border-top: 1px solid gray;" cellspacing="0">
                    <tr><td valigin="bottom" style="border-left: 1px solid gray;border-right: 1px solid gray;"><font size="9">'.$data_ficha[0]['num_ficha'].'</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">'.$id_rectificacion.'</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">'.$data_ficha[0]['estado'].'</font></td><td valigin="bottom" style="border-right: 1px solid gray;"><font size="9">'.$data_ficha[0]['fecha_consulta'].'</font></td></tr>
                </table>
                </td></tr>
                </table>
                ';
                $pdf->SetAlpha(0.5);
                
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = 'C', $autopadding = true);
        //$pdf->AddPage('P');
        $pdf->SetAlpha(1);

        $html= '';

        $html.= '<br><h4>Datos Rectificación</h4>';

        $html.='<table>
        <tr><td><font size="10">O.C. a Consultar: </font></td><td><font size="10">'.($num_orden_compra == 0 ? $data_ficha[0]['num_orden_compra'] : $num_orden_compra).'</font></td></tr>
        <tr><td><font size="10">Tipo Rectificación: </font></td><td><font size="10">'.$alumnos_por_rectificar[0]['tipo_rectificacion'].'</font></td></tr>
        <tr><td><font size="10">Motivo Rectificación: </font></td><td><font size="10">'.$data_rectificacion[0]['motivo_rectificacion'].'</font></td></tr>
        </table>';
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        
        $html= '';

        $html.='
        <table border="1" width="100%">
                <tr>
                    <th><font size="11">O.C.</font></th>
                    <th><font size="11">Tipo Rectificación</font></th>
                    <th><font size="11">RUT</font></th>
                    <th><font size="11">Nombre</font></th>
                    <th><font size="11">Centro Costo</font></th>
                    <th><font size="11">%FQ</font></th>
                    <th><font size="11">Total</font></th>
                </tr>';
        for ($i=0; $i < count($alumnos_por_rectificar); $i++) { 
            if($num_orden_compra == 0){
                $html.='<tr><td><font size="9">';
                $html.= $alumnos_por_rectificar[$i]['num_orden_compra'];
                $html.= '</font></td><td><font size="9">';
                $html.= $alumnos_por_rectificar[$i]['tipo_rectificacion'];
                $html.= '</font></td><td><font size="9">';
                $html.= $alumnos_por_rectificar[$i]['rut'];
                $html.= '</font></td><td><font size="9">';
                $html.= $alumnos_por_rectificar[$i]['nombre'];
                $html.= '</font></td><td><font size="9">';
                $html.= $alumnos_por_rectificar[$i]['centro_costo'];
                $html.= '</font></td><td><font size="9">';
                $html.= $alumnos_por_rectificar[$i]['porcentaje_franquicia'];
                $html.= '</font></td><td><font size="9">';
                $html.= $alumnos_por_rectificar[$i]['valor_total'];
                $html.= '</font></td></tr>';
            }else{
                if($num_orden_compra == $alumnos_por_rectificar[$i]['num_orden_compra']){
                    $html.='<tr><td><font size="9">';
                    $html.= $alumnos_por_rectificar[$i]['num_orden_compra'];
                    $html.= '</font></td><td><font size="9">';
                    $html.= $alumnos_por_rectificar[$i]['tipo_rectificacion'];
                    $html.= '</font></td><td><font size="9">';
                    $html.= $alumnos_por_rectificar[$i]['rut'];
                    $html.= '</font></td><td><font size="9">';
                    $html.= $alumnos_por_rectificar[$i]['nombre'];
                    $html.= '</font></td><td><font size="9">';
                    $html.= $alumnos_por_rectificar[$i]['centro_costo'];
                    $html.= '</font></td><td><font size="9">';
                    $html.= $alumnos_por_rectificar[$i]['porcentaje_franquicia'];
                    $html.= '</font></td><td><font size="9">';
                    $html.= $alumnos_por_rectificar[$i]['valor_total'];
                    $html.= '</font></td></tr>';
                }
            }
        }
        $html.='</table>';

        $pdf->writeHTML($html, true, false, true, false, '');

        //Close and output PDF document
        $pdf->Output('Resumen.pdf', 'I');
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
