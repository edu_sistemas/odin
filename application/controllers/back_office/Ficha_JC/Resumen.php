<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Resumen extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Ficha_JC';
    private $model = 'Ficha_JC_model';
    private $view = 'Resumen_v';
    private $controller = 'Editar';
    private $ind = '';
    private $modalidad = 'back_office/Modalidad/Modalidad_model';
    private $curso = 'back_office/Curso/Curso_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $otic = 'back_office/Otic/Otic_model';
    private $sence = 'back_office/Code_sence/Code_sence_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $sede = 'back_office/Sede/Sede_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $sala = 'back_office/Sala/Sala_model';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_ficha = false) {
        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Resumen',
                'label' => 'Resumen',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );
        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha_JC/resumen.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Solicitudes";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id_resumenJC($arr_input_data);

        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_ficha_idJC($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function cargaDatosOC() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_datos_orden_compraJC($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumen() {

        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_datos_documentos_resumen_orden_compraJC($arr_data);
        echo json_encode($datos);
    }

    //   rechazo  
    function rechazarFicha() {

        $arr_datos = array(
            'id_ficha_rechazo' => $this->input->post('id_ficha_rechazo'),
            'motivo_rechazo' => $this->input->post('motivo_rechazo'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->set_rechazar_fichaJC($arr_datos);

        echo json_encode($respuesta);
    }

    function getAdicionalesFicha(){
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
    
        $datos = $this->modelo->get_adicionales_ficha_by_id($arr_sesion);
        echo json_encode($datos);
    }

    function generaHtmlRechazo($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_rojo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)  ';
        $html .= '<br>';
        $html .= 'Se ha Anulado la  Ficha N°: ' . $data['num_ficha'];
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright">' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr>';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Sede</td>';
        $html .= '<td class="alignright"> ' . $data['nombre_sede'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Sala</td>';
        $html .= '<td class="alignright"> ' . $data['sala'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Dias</td>';
        $html .= '<td class="alignright">' . $data['dias'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Participantes</td>';
        $html .= '<td class="alignright"> ' . $data['cant_participante'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr  class="total">';
        $html .= '<td>Motivo de Anulación C. C. </td>';
        $html .= '<td class="alignright"> ' . $data['motivo_rechazo'] . '</td>';
        $html .= '</tr>';

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfano  863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function enviaEmailRechazo() {

        $datos = $this->input->post('datos');
        $destinatarios = array();
        $asunto = "";
        $body = "";
        $ejecutivo_nombre = $datos[0]['ejecutivo'];
        $email_ejecutivo = $datos[0]['email_ejecutivo'];
        $usuario_ingreso_nombre = $datos[0]['usuario_ingreso_nombre'];
        $usuario_ingreso_correo = $datos[0]['usuario_ingreso_correo'];



        $num_ficha = $datos[0]['num_ficha'];
        $modalidad = $datos[0]['modalidad'];

        if ($datos[0]['id_categoria'] == "1") {
            $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo
        } else {
            $asunto = "Nueva Venta - Ficha N°: " . $num_ficha;
        }


        switch ($modalidad) {
            case 'elearning':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo    

                if ($email_ejecutivo == $usuario_ingreso_correo) {
                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                        array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                } else {

                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                        array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                }

                $body = $this->generaHtmlRechazo($datos[0]); // mensaje completo HTML o text

                break;
            case 'presencial':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo
                $body = $this->generaHtmlRechazo($datos[0]); // mensaje completo HTML o text

                if ($email_ejecutivo == $usuario_ingreso_correo) {
                    $destinatarios = array(
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                        array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                        array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                        array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola Salazar'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                } else {

                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola Salazar'),
                        array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                        array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                        array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                }
                break;
            case 'arriendo':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha;
                $body = $this->generaHtmlRechazo($datos[0]); // mensaje completo HTML o text

                if ($email_ejecutivo == $usuario_ingreso_correo) {
                    $destinatarios = array(
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                } else {

                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                }
                break;
        }

        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );

        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo json_encode($body);
    }

    // end rechazo 
    function aprobarFicha() {
        $id_ficha = $this->input->post('id_ficha');
        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $data = $this->modelo->set_aprobar_fichaJC($arr_datos);
        echo json_encode($data);
    }

    function enviaEmail() {

        $datos = $this->input->post('datos');
        $datos_alumnos_rectificacion = array();

        $destinatarios = array();
        $asunto = "";
        $body = "";

        $ejecutivo_nombre = $datos[0]['ejecutivo'];
        $email_ejecutivo = $datos[0]['email_ejecutivo'];

        $usuario_ingreso_nombre = $datos[0]['usuario_ingreso_nombre'];
        $usuario_ingreso_correo = $datos[0]['usuario_ingreso_correo'];

        $num_ficha = $datos[0]['num_ficha'];
        $modalidad = $datos[0]['modalidad'];

        if ($datos[0]['id_categoria'] == "1") {
            $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo
        } else {
            $asunto = "Nueva Venta - Ficha N°: " . $num_ficha;
        }


        switch ($modalidad) {
            case 'elearning':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo    

                if ($email_ejecutivo == $usuario_ingreso_correo) {
                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                        array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                } else {

                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                        array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                }

                $body = $this->generaHtmlSolicitudVenta($datos[0]); // mensaje completo HTML o text

                break;
            case 'presencial':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo

                $data_arr = array(
                    'id_ficha' => $datos[0]['id_ficha'],
                    'orden_compra' => $datos[0]['id_orden_compra'],
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );
                $datos_alumnos_rectificacion = $this->modelo->get_arr_listar_datos_orden_compraJC($data_arr);
                $GLOBALS['data_alumnos'] = $datos_alumnos_rectificacion;

                $body = $this->generaHtmlPresencial($datos[0]); // mensaje completo HTML o text

                if ($email_ejecutivo == $usuario_ingreso_correo) {
                    $destinatarios = array(
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                        array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                        array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                        array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola Salazar'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                } else {

                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola Salazar'),
                        array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                        array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                        array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                }
                break;
            case 'arriendo':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha;
                $body = $this->generaHtmlSolicitudArriendo($datos[0]); // mensaje completo HTML o text

                if ($email_ejecutivo == $usuario_ingreso_correo) {
                    $destinatarios = array(
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'vlinares@edutecno.com', 'nombre' => 'Vitalia Linares'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                } else {
                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo_nombre),
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                }
                break;
        }

        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );

        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo json_encode($body);
    }

    function generaHtmlPresencial($data) {

        $xyz = $GLOBALS['data_alumnos'];

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba Venta Curso Presencial</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_verde.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)';
        $html .= '<br>';
        $html .= '<br>';
        $html .= 'Se ha aprobado la solicitud de venta - Presencial';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';

        $html .= '<tr>';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright" colspan="2"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright" colspan="2"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright" colspan="2"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr >';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright" colspan="2"> ' . $data['nombre_curso'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright" colspan="2">' . $data['nombre_modalidad'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Código Sence</td>';
        $html .= '<td class="alignright" colspan="2">' . $data['codigo_sence'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright" colspan="2">' . $data['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright" colspan="2">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright" colspan="2">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Horario del curso</td>';
        $html .= '<td class="alignright" colspan="2">' . $data['hora_inicio'] . ' - ' . $data['hora_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Sala</td>';
        $html .= '<td class="alignright" colspan="2">' . $data['sala'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Dias</td>';
        $html .= '<td class="alignright" colspan="2">' . $data['dias'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>N° Participantes </td>';
        $html .= '<td class="alignright" colspan="2">' . $data['cant_participante'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr  class="total">';
        $html .= '<td>Observaciones </td>';
        $html .= '<td class="alignright" colspan="2">' . $data['observaciones'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr class="total">';
        $html .= '<td>Alumnos Afectados</td>';
        $html .= '<td colspan="2"></td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td colspan="3"></td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td><b>RUT</b></td>';
        $html .= '<td><b>Nombre</b></td>';
        $html .= '<td><b>FQ %</b></td>';
        $html .= '</tr> ';

        for ($i=0; $i < count($xyz); $i++) { 
            $html .= '<tr>';
            $html .= '<td>'.$xyz[$i]['rut'].'</td>';
            $html .= '<td>'.$xyz[$i]['nombre'].'</td>';
            $html .= '<td>'.$xyz[$i]['fq'].'</td>';
            $html .= '</tr> ';
        }

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com/back_office/Ficha_Control/ResumenSeguimiento/index/' . $data['id_ficha'] . '" target="_blank" class="btn-primary">Ver en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function generaHtmlSolicitudVenta($data) {


        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Solicitud de venta</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_verde.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)';
        $html .= '<br>';
        $html .= 'Se ha aprobado la solicitud de venta - E-learning';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';

        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';

        $html .= '<tr  >';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr class="total" >';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>N° Participantes </td>';
        $html .= '<td class="alignright">' . $data['cant_participante'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>Solicitado por</td>';
        $html .= '<td class="alignright"> ' . $data['usuario_ingreso_nombre'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['nombre_curso'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['nombre_modalidad'] . '</td>';
        $html .= '</tr>      ';
        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr> ';

        $html .= '<tr>';
        $html .= '<td>Dias</td>';
        $html .= '<td class="alignright">' . $data['dias'] . '</td>';
        $html .= '</tr> ';

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">Edutecno - Huérfanos 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function generaHtmlSolicitudArriendo($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Solicitud de venta</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_verde.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)';
        $html .= '<br>';

        $html .= 'Se ha aprobado la solicitud de arriendo';

        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';

        $html .= '<tr>';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright">  ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>   ';

        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por</td>';
        $html .= '<td class="alignright"> ' . $data['usuario_ingreso_nombre'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr> ';

        $html .= '<tr>';
        $html .= '<td>Dias</td>';
        $html .= '<td class="alignright">' . $data['dias'] . '</td>';
        $html .= '</tr> ';

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function setCodigosTablet(){
        
        $arr_sesion = array(
            'id_solicitud_tablet' => $this->input->post('id_solicitud_tablet'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $solicitud_tablet = $this->modelo->get_solicitud_tablet_by_id_solicitud($arr_sesion);

        $cantidad_tablet = $solicitud_tablet[0]['cantidad'];
        $ficha = "".$this->input->post('num_ficha');
        $app = $solicitud_tablet[0]['validador_curso'];
        $pad = "00000";
        $ficha = substr($pad,  0 + strlen($ficha)) . $ficha;


        for ($i=0; $i < $cantidad_tablet; $i++) { 
            $str = "" + ($i+1);
            $pad = "0000";
            $ans = substr($pad,  0 + strlen($str)) . $str;
            $codigoFinal =   $this->charAt($app, 0)      //  0
                            .$this->charAt($ans, 0)      //  1
                            .$this->charAt($ficha, 0)    //  2
                            .$this->charAt($ficha, 1)    //  3
                            .$this->charAt($ans, 1)      //  4
                            .$this->charAt($ficha, 2)    //  5
                            .$this->charAt($ans, 2)      //  6
                            .$this->charAt($app, 1)      //  7
                            .$this->charAt($ans, 3)      //  8
                            .$this->charAt($ficha, 3)    //  9
                            .$this->charAt($app, 2)      //  10
                            .$this->charAt($ficha, 4);    //  11
            $codigoFinal = $this->encodeWord($codigoFinal);

            $data_array = array(
                'codigoFinal' => $codigoFinal,
                'id_ficha' => $solicitud_tablet[0]['id_ficha'],
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $data = $this->modelo->setCodigoTablet($data_array);
        }
    }

    function encodeWord($x){
        //parametros para la generacion del codigo
        $base64_characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        $base64_charactersArray = str_split($base64_characters);

        $base64_charactersRandom = "uxlD6KUVWd=be3wHvCcPZTaIM+rqzOhm8A/BSXLpFi7NoJs2fG5tQnjkYgE01R49y";
        $base64_charactersRandomArray = str_split($base64_charactersRandom);
        //fin parametros para la generacion del codigo
        $x = base64_encode($x);
        $palabraArray = str_split($x);
        $salidax="";
        $salida = "";

        for ($i = 0; $i < count($palabraArray); $i++) {
            for ($j = 0; $j < count($base64_charactersArray); $j++) {
                if ($palabraArray[$i] == $base64_charactersArray[$j]) {
                    $salida .= $base64_charactersRandomArray[$j];
                }
            }
        }

        $salida = str_split($salida,4);
        for ($i = 0; $i < count($salida); $i++) {
            if($i < count($salida) -1){
                $salidax .= $salida[$i] . "-";
            }else{
                $salidax .= $salida[$i];
            }
        }
        return $salidax;
    }
    
    function charAt($str,$pos) {
        return (substr($str,$pos,1) !== false) ? substr($str,$pos,1) : -1;
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
