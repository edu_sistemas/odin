<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-29 [Luis jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Encuesta_ficha_listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Encuestas Ficha';
    private $nombre_item_plural = 'Encuestas Ficha';
    private $package = 'back_office/Encuesta_satisfaccion';
    private $model = 'Encuesta_satisfaccion_model';
    private $view = 'Encuesta_ficha_listar_v';
    private $controller = 'Encuesta_ficha_listar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha== false) {
            redirect($this->package . '/Encuesta_satisfaccion', 'refresh');
        }


        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Encuesta_satisfaccion',
                'label' => 'Todas las fichas',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Editar',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/cropper/cropper.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Encuesta_satisfaccion/Encuesta_ficha_listar.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Encuesta Satisfacción";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['id_ficha'] = $id_ficha;

        if ($id_ficha == false) {
            redirect($this->package . '/Encuesta_satisfaccion', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listarEncuestas(){
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_encuestas_by_ficha($arr_sesion);
        echo json_encode($datos);
    }

    function getEncuestaSatisfaccion(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_encuesta_satisfaccion($arr_sesion);
        echo json_encode($datos);
    }

    function setEncuestaSatisfaccion(){
        $arr_sesion = array( 
            'id_ficha' => $this->input->post('id_ficha'),
            'id_relator' => $this->input->post('id_relator'),
            'respuesta1' => ($this->input->post('respuesta1') == null ? '0' : $this->input->post('respuesta1')),
            'respuesta2' => ($this->input->post('respuesta2') == null ? '0' : $this->input->post('respuesta2')),
            'respuesta3' => ($this->input->post('respuesta3') == null ? '0' : $this->input->post('respuesta3')),
            'respuesta4' => ($this->input->post('respuesta4') == null ? '0' : $this->input->post('respuesta4')),
            'respuesta5' => ($this->input->post('respuesta5') == null ? '0' : $this->input->post('respuesta5')),
            'respuesta6' => ($this->input->post('respuesta6') == null ? '0' : $this->input->post('respuesta6')),
            'respuesta7' => ($this->input->post('respuesta7') == null ? '0' : $this->input->post('respuesta7')),
            'respuesta8' => ($this->input->post('respuesta8') == null ? '0' : $this->input->post('respuesta8')),
            'respuesta9' => ($this->input->post('respuesta9') == null ? '0' : $this->input->post('respuesta9')),
            'respuesta10' => ($this->input->post('respuesta10') == null ? '0' : $this->input->post('respuesta10')),
            'respuesta11' => ($this->input->post('respuesta11') == null ? '0' : $this->input->post('respuesta11')),
            'respuesta12' => ($this->input->post('respuesta12') == null ? '0' : $this->input->post('respuesta12')),
            'respuesta13' => ($this->input->post('respuesta13') == null ? '0' : $this->input->post('respuesta13')),
            'respuesta14' => ($this->input->post('respuesta14') == null ? '0' : $this->input->post('respuesta14')),
            'respuesta15' => ($this->input->post('respuesta15') == null ? '0' : $this->input->post('respuesta15')),
            'respuesta16' => ($this->input->post('respuesta16') == null ? '0' : $this->input->post('respuesta16')),
            'respuesta17' => ($this->input->post('respuesta17') == null ? '0' : $this->input->post('respuesta17')),
            'respuesta18' => ($this->input->post('respuesta18') == null ? '0' : $this->input->post('respuesta18')),
            'respuesta19' => ($this->input->post('respuesta19') == null ? '0' : $this->input->post('respuesta19')),
            'respuesta20' => ($this->input->post('respuesta20') == null ? '' : $this->input->post('respuesta20')),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_encuesta_satisfaccion($arr_sesion);
        echo json_encode($datos);
    }

    function getRelatoresCBX(){
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_relatores_by_ficha($arr_sesion);
        echo json_encode($datos);
    }

    function getEncuestaById(){
        $arr_sesion = array(
            'id_encuesta' => $this->input->post('id_encuesta'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_encuesta_by_id($arr_sesion);
        echo json_encode($datos);
    }
}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
