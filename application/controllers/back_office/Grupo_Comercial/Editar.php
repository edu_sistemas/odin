<?php

/**
         * Agregar
         *
         * Description...
         *
         * @version 0.0.1
         *
         * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
         * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
         */
        defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Categoria';
    private $model = 'Categoria_model';
    private $view = 'Editar_v';
    private $controller = 'Editar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
        //  $this->load->library('form_validation');
        //  $this->load->library('AddForms_lib', 'addforms_lib');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_categoria = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_categoria == false) {
            redirect($this->$package . '/Listar', 'refresh');
        }


        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Editar',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/switchery/switchery.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/cropper/cropper.min.css') 
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/switchery/switchery.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Categoria/editar.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
		$data['activo'] = "Categoria";
		if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }

        $arr_input_data = array(
            'id_categoria' => $id_categoria,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_categoria'] = $this->modelo->get_arr_categoria_by_id($arr_input_data);

        if ($data['data_categoria'] == false) {
            redirect($this->package . '/Listar', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function EditarCategoria() {

        /*
          Ultima fecha Modificacion: 2016-11-29
          Descripcion:  Metodo para Editar la categoria
          Usuario Actualizador : Marcelo Romero
         */
        //  $this->input->post('nombre') : Recibe los parametros enviados por post

        $arr_input_data = array(
            'id_categoria' => trim($this->input->post('id_categoria')),
            'nombre_categoria' => trim($this->input->post('nombre_categoria')),
            'descripcion_breve_categoria' => trim($this->input->post('descripcion_breve_categoria')),
            'descripcion_categoria' => trim($this->input->post('descripcion_categoria')),
            'estado_categoria' => trim($this->input->post('estado_categoria'))
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
   
        // llamado al modelo
        $respuesta = $this->modelo->set_arr_update_categoria($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }
}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
