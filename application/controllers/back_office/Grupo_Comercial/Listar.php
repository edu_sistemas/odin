<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	13-11-2017 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:	13-11-2017 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Grupo Comercial';
    private $nombre_item_plural = 'Grupo Comercial';
    private $package = 'back_office/Grupo_Comercial';
    private $model = 'Grupo_Comercial_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );



        // array con los css necesarios para la pagina
        //   <link href="css/plugins/select2/select2.min.css" rel="stylesheet">
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/chosen/chosen.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Grupo_Comercial/listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Grupo Comercial";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_empresa($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function get_grupos_comerciales_cbx() {
        //  variables de sesion

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_grupos_comerciales_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function get_grupo_comercial(){
        $arr_sesion = array(
            'id_grupo_comercial' => $this->input->post('id_grupo_comercial'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_grupo_comercial($arr_sesion);
        echo json_encode($datos);
    }
    
    function get_usuarios_cbx() {
        //  variables de sesion

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_usuarios_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function get_grupocomercial_resumen() {
            //  variables de sesion

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_grupocomercial_resumen($arr_sesion);
        echo json_encode($datos);
    }

    function set_grupo_comercial(){
        $usuarios_asignados = $this->input->post('usuario');
        $retorno;
        
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo->create_grupo_comercial($arr_sesion);
        if($datos[0]['respuesta'] != 0){
            for ($i=0; $i < count($usuarios_asignados); $i++) { 
                $data_array = array(
                    'id_grupo_comercial' => $datos[0]['respuesta'],
                    'id_usuario' => $usuarios_asignados[$i],
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );
                $respuesta = $this->modelo->set_usuario_grupo_comercial($data_array);
            }
            if($respuesta[0]['respuesta'] != 0){
                $retorno = $respuesta;
            }else{
                $retorno = $datos;
            }
        }
        echo json_encode($retorno);
    }

    function set_usuario_grupo_comercial(){

        $arr_sesion = array(
            'id_grupo_comercial' => $this->input->post('grupo_comercial'),
            'id_usuario' => $this->input->post('usuario'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_usuario_grupo_comercial_comun($arr_sesion);
        echo json_encode($datos);
    }

    function quitar_usuario_grupo_comercial(){
        $arr_sesion = array(
            'id_usuario' => $this->input->post('id_usuario'),
            'id_grupo_comercial' => $this->input->post('id_grupo_comercial'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->quitar_usuario_grupo_comercial($arr_sesion);
        echo json_encode($datos);
    }

    function marcar_jefe_grupo_comercial(){
        $arr_sesion = array(
            'id_usuario' => $this->input->post('id_usuario'),
            'id_grupo_comercial' => $this->input->post('id_grupo_comercial'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->marcar_jefe_grupo_comercial($arr_sesion);
        echo json_encode($datos);
    }
}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
