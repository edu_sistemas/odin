<?php

/**
 * Alumnos
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2017-01-18 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Rectificacion extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Rectificacion';
    private $nombre_item_plural = 'Alumnos';
    private $package = 'back_office/Ficha';
    private $model = 'Ficha_model';
    private $view = 'Rectificacion_v';
    private $controller = 'Rectificacion';
    private $especial = 'back_office/Especial/Especial_model';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->especial, 'modeloespecial');
        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . 'back_office/Especial/Consulta',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Rectificacion',
                'label' => 'Rectificacion',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/ladda/ladda-themeless.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css')
        );



        //  js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            //   <!-- Ladda -->
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/spin.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.jquery.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Especial/rectificacion.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');


        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id($arr_input_data);
        $data['data_ficha_preview'] = $this->modelo->get_arr_ficha_by_id_preview($arr_input_data);
        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_ficha_id($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Fichas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        //*********-------------Cargando el contenido de la tabla temporal .........................

        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $this->load->view('amelia_1/template', $data);
    }

    function cargar_archivo_datos_alumnos() {

        /* aumento el tamañao de espera del servidor   */

        $arr_datos = array();

        $user_id = $this->session->userdata('id_user');
        $user_perfil = $this->session->userdata('id_perfil');

        $id_orden_compra = $this->input->post('cbx_orden_compra');


        $id_accion = $this->input->post('accion');
        //   $n_rect = $this->input->post('n_rect');

        $tipo_rectificacion = "";

        $name = $_FILES['file_carga_alumnos']['name'];
        $tname = $_FILES['file_carga_alumnos']['tmp_name'];

        if ($name != "") {

            $obj_excel = PHPExcel_IOFactory::load($tname);
            $sheetData = $obj_excel->getActiveSheet()->toArray(null, true, true, true);

            switch ($id_accion) {
                case 1:
                    $tipo_rectificacion = "Agregar Alumnos";
                    foreach ($sheetData as $index => $value) {
                        if ($index != 1) {
                            if (trim($value['A']) != "" && trim($value['B']) != "" && trim($value['C']) != "") {

                                $a = $value['A'];
                                $b = $value['B'];
                                $c = $value['C'];
                                $d = $value['D'];
                                $e = $value['E'];
                                $f = $value['F'];
                                $g = $value['G'];
                                $h = $value['H'];
                                $i = $value['I'];
                                $j = $value['J'];

                                if ($a === null || $a === false) {
                                    $a = "";
                                }
                                if ($b === null || $b === false) {
                                    $b = "";
                                }
                                if ($c === null || $c === false) {
                                    $c = "";
                                }
                                if ($d === null || $d === false) {
                                    $d = "";
                                }
                                if ($e === null || $e == false) {
                                    $e = "";
                                }
                                if ($f === null || $f === false) {
                                    $f = "";
                                }
                                if ($g === null || $g == false) {
                                    $g = "";
                                }
                                if ($h === null || $h === false) {
                                    $h = "";
                                }
                                if ($i === null || $i === false) {
                                    $i = "";
                                }

                                if ($j === null || $j === false) {
                                    $j = "";
                                }

                                $arr_datos = array(
                                    'id_accion' => 1,
                                    'tipo_rectificacion' => $tipo_rectificacion,
                                    'id_orden_compra' => $id_orden_compra,
                                    'rut' => $a,
                                    'dv' => $b,
                                    'nombre' => $c,
                                    'apellido_paterno' => $d,
                                    'apellido_materno' => $e,
                                    'email' => $f,
                                    'telefono' => $g,
                                    'fq' => $h,
                                    'CostoOtic' => str_replace(",", "", $i),
                                    'CostoEmpresa' => str_replace(",", "", $j),
                                    'user_id' => $user_id,
                                    'user_perfil' => $user_perfil
                                );

                                $this->modelo->get_arr_subir_insert_alumnos_orden_compra_rectificacion($arr_datos);
                            } else {
                                break;
                            }
                        }
                    }

                    break;
                case 2:

                    $tipo_rectificacion = "Actualizar Alumnos";


                    foreach ($sheetData as $index => $value) {
                        if ($index != 1) {
                            if (trim($value['A']) != "" && trim($value['B']) != "" && trim($value['C']) != "") {
                                $a = $value['A'];
                                $b = $value['B'];
                                $c = $value['C'];
                                $d = $value['D'];
                                $e = $value['E'];
                                $f = $value['F'];
                                $g = $value['G'];
                                $h = $value['H'];
                                $i = $value['I'];
                                $j = $value['J'];

                                if ($a == null || $a == false) {
                                    $a = "";
                                }
                                if ($b === null || $b === false) {
                                    $b = "";
                                }
                                if ($c == null || $c == false) {
                                    $c = "";
                                }
                                if ($d == null || $d == false) {
                                    $d = "";
                                }
                                if ($e == null || $e == false) {
                                    $e = "";
                                }
                                if ($f == null || $f == false) {
                                    $f = "";
                                }
                                if ($g == null || $g == false) {
                                    $g = "";
                                }
                                if ($h == null || $h == false) {
                                    $h = "";
                                }
                                if ($i == null || $i == false) {
                                    $i = "";
                                }
                                if ($j == null || $j == false) {
                                    $j = "";
                                }
                                $arr_datos = array(
                                    'id_accion' => 2,
                                    'tipo_rectificacion' => $tipo_rectificacion,
                                    'id_orden_compra' => $id_orden_compra,
                                    'rut' => $a,
                                    'dv' => $b,
                                    'nombre' => $c,
                                    'apellido_paterno' => $d,
                                    'apellido_materno' => $e,
                                    'email' => $f,
                                    'telefono' => $g,
                                    'fq' => $h,
                                    'CostoOtic' => str_replace(",", "", $i),
                                    'CostoEmpresa' => str_replace(",", "", $j),
                                    'user_id' => $user_id,
                                    'user_perfil' => $user_perfil
                                );
                                $this->modelo->get_arr_subir_alumnos_orden_compra_rectificacion($arr_datos);
                            } else {
                                break;
                            }
                        }
                    }
                    break;
                case 3:
                    $tipo_rectificacion = "Eliminar Alumnos";
                    foreach ($sheetData as $index => $value) {
                        if ($index != 1) {
                            if (trim($value['A']) != "" && trim($value['B']) != "" && trim($value['C']) != "") {
                                $a = $value['A'];
                                $b = $value['B'];
                                $c = $value['C'];
                                $d = $value['D'];
                                $e = $value['E'];
                                $f = $value['F'];
                                $g = $value['G'];
                                $h = $value['H'];
                                $i = $value['I'];
                                $j = $value['J'];
                                $m = $value['M'];
                                if ($a == null || $a == false) {
                                    $a = "";
                                }
                                if ($b === null || $b === false) {
                                    $b = "";
                                }
                                if ($c == null || $c == false) {
                                    $c = "";
                                }
                                if ($d == null || $d == false) {
                                    $d = "";
                                }
                                if ($e == null || $e == false) {
                                    $e = "";
                                }
                                if ($f == null || $f == false) {
                                    $f = "";
                                }
                                if ($g == null || $g == false) {
                                    $g = "";
                                }
                                if ($h == null || $h == false) {
                                    $h = "";
                                }
                                if ($i == null || $i == false) {
                                    $i = "";
                                }
                                if ($j == null || $j == false) {
                                    $j = "";
                                }
                                if ($m === null || $m === false) {
                                    $m = "0";
                                }
                                $arr_datos = array(
                                    'id_accion' => 3,
                                    'tipo_rectificacion' => $tipo_rectificacion,
                                    'id_causa' => $m,
                                    'id_orden_compra' => $id_orden_compra,
                                    'rut' => $a,
                                    'dv' => $b,
                                    'nombre' => $c,
                                    'apellido_paterno' => $d,
                                    'apellido_materno' => $e,
                                    'email' => $f,
                                    'telefono' => $g,
                                    'fq' => $h,
                                    'CostoOtic' => str_replace(",", "", $i),
                                    'CostoEmpresa' => str_replace(",", "", $j),
                                    'user_id' => $user_id,
                                    'user_perfil' => $user_perfil
                                );
                                $this->modelo->get_arr_subir_alumnos_orden_compra_eliminar_rectificacion($arr_datos);
                            } else {
                                break;
                            }
                        }
                    }
                    break;
            }
            $result['valido'] = true;
            $result['mensaje'] = 'Alumnos importados correctamente';
        } else {

            $result['valido'] = false;
            $result['mensaje'] = 'Error al procesar el archivo de carga';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    function listar_carga_orden_de_compra() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $resultado = $this->modelo->cargar_alumnos_tbl_temporal($datos_menu);

        echo json_encode($resultado);
    }

    function limpiar_carga_anterior() {

        $datos['user_id'] = $this->session->userdata('id_user');
        $datos['user_perfil'] = $this->session->userdata('id_perfil');
        $resultado = $this->modelo->get_eliminar_alumnos_temporal($datos);
        echo json_encode($resultado);
    }

    function aceptar_carga_orden_compra_alumnos() {

        // Ultima fecha Modificacion: 2017-01-16
        // Descripcion:  Metodo para registrar cargar los alumnos a un curso en especifico 
        // Usuario Actualizador : Luis Jarpa
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado


        $accion = $this->input->post('accion');

        $nombre_archivo_plantilla = "";
        $ruta_archivo_plantilla = "";
        $nombre_archivo_adjunto = "";
        $ruta_archivo_adjunto = "";


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = "";

        switch ($accion) {
            case 1:
                $respuesta = $this->modeloespecial->set_arr_orden_compra_accion_insert_alumnos_especial($arr_input_data);
                break;
            case 2:
                $respuesta = $this->modeloespecial->set_arr_orden_compra_accion_actualizar_alumnos_especial($arr_input_data);
                break;
            case 3:
                $respuesta = $this->modeloespecial->set_arr_orden_compra_accion_eliminar_alumnos_especial($arr_input_data);
                break;
        }


        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function cancelar_carga_alumnos() {

        // Ultima fecha Modificacion: 2016-10-13
        // Descripcion:  Metodo para eliminar la tabla temporal 
        // Usuario Actualizador : Luis Jarpa
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->get_eliminar_empresas_temporal($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

}

/* End of file CargaMasiva.php */
/* Location: ./application/controllers/CargaMasiva.php */
?>
