<?php

/**
         * Agregar
         *
         * Description...
         *
         * @version 0.0.1
         *
         * Ultima edicion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
         * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
         */
        defined('BASEPATH') OR exit('No direct script access allowed');

class Ver_Reporte extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Ver Reporte';
    private $nombre_item_plural = 'Ver Reporte';
    private $package = 'back_office/Reportes_Distancia';
    private $model = 'Reportes_Distancia_model';
    private $view = 'Ver_Reporte_v';
    private $controller = 'Ver_Reporte';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index($rut_alumno = false, $id_ficha = false) {

        if ($rut_alumno == false || $id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Reportes Distancia',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Ver_Curso/index/'.$id_ficha,
                'label' => 'Ver Curso',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Ver_Reporte',
                'label' => 'Ver Reporte',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.plugin.autotable.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reportes_Distancia/Ver_Reporte.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
		    $data['activo'] = "Códigos Tablet";
        $data['id_perfil'] = $this->session->userdata('id_perfil');
		// if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $data['rut'] = $rut_alumno;
        $data['id_ficha'] = $id_ficha;



        $this->load->view('amelia_1/template', $data);
    }

    function getCodigosbyIDFicha() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getCodigosbyIDFicha($arr_input_data);

        echo json_encode($respuesta);
    }

    function getReporteAlumno() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'rut_alumno' => $this->input->post('rut_alumno')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getReporteAlumno($arr_input_data);

        echo json_encode($respuesta);
    }

    function getUnidadesReporteAlumno() {

        $arr_input_data = array(
            'codigo_tablet' => $this->input->post('codigo_tablet')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getUnidadesReporteAlumno($arr_input_data);

        echo json_encode($respuesta);
    }

    function getModulosReporteAlumno() {

        $arr_input_data = array(
            'codigo_tablet' => $this->input->post('codigo_tablet'),
            'id_unidad' => $this->input->post('id_unidad')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getModulosReporteAlumno($arr_input_data);

        echo json_encode($respuesta);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
