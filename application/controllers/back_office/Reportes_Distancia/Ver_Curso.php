<?php

/**
         * Agregar
         *
         * Description...
         *
         * @version 0.0.1
         *
         * Ultima edicion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
         * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
         */
        defined('BASEPATH') OR exit('No direct script access allowed');

class Ver_Curso extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Asignar Codigos';
    private $nombre_item_plural = 'Asignar Codigos';
    private $package = 'back_office/Reportes_Distancia';
    private $model = 'Reportes_Distancia_model';
    private $view = 'Ver_Curso_v';
    private $controller = 'Ver_Curso';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Reportes Distancia',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Ver_Curso',
                'label' => 'Ver Curso',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.plugin.autotable.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/xml2json/xml2json.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/json2xml/json2xml.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reportes_Distancia/Ver_Curso.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
		    $data['activo'] = "Códigos Tablet";
        $data['id_perfil'] = $this->session->userdata('id_perfil');
		// if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;



        $this->load->view('amelia_1/template', $data);
    }

    function getCodigosbyIDFicha() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getCodigosbyIDFicha($arr_input_data);

        echo json_encode($respuesta);
    }

    function getAlumnosbyIDFicha() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getAlumnosbyIDFicha($arr_input_data);

        echo json_encode($respuesta);
    }

    function getUnidadesByIDFicha() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getUnidadesByIDFicha($arr_input_data);

        echo json_encode($respuesta);
    }

    function getModulosByFichaYUnidad() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'id_unidad' => $this->input->post('id_unidad')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getModulosByFichaYUnidad($arr_input_data);

        echo json_encode($respuesta);
    }
    function getXMLbyRUT() {

        $arr_input_data = array(
            'rut' => $this->input->post('rut'),
            'rut_con_puntos' => $this->input->post('rut_con_puntos')
        );

        $respuesta = $this->modelo->getXMLbyRUT($arr_input_data);

        echo json_encode($respuesta);
    }

    function getReporteDetalladoByIDFicha()
    {
        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->getReporteDetalladoByIDFicha($arr_input_data);

        echo json_encode($respuesta);
    }

    function sendXMLtoApitablet()
    {
        
        $xml = $this->input->post('xml');
        $url = 'http://apitablet.edutecno.cl/api/tablet/temp';
        $ch = curl_init($url);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, "json=".$xml );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/x-www-form-urlencoded'));
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);
        curl_close($ch);
        if(strpos($result, "'id_ciclo_tablet' cannot be null")){
            $result = 'El registro no se guardó ya que esta fuera de la fecha de término'; 
        }else{
            $result = 'Registro guardado';
        }
        echo $result;
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
