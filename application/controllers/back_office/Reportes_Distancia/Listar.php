<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Reportes_Distancia';
    private $model = 'Reportes_Distancia_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

                ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Reportes_Distancia',
                'label' => 'Reportes Distancia',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            //
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.plugin.autotable.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reportes_Distancia/listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Códigos Tablet";
        // if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
        //     redirect('back_office/Home', 'refresh');
        // }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_empresa($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getFichas() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->getFichas($arr_sesion);
        echo json_encode($datos);
    }
    function getX() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->getX($arr_sesion);
        echo json_encode($datos);
    }

    function generarReporteExcel($id_ficha) {

       $arr_datos = array(
           'id_ficha' => $id_ficha,
           'user_id' => $this->session->userdata('id_user'),
           'user_perfil' => $this->session->userdata('id_perfil')
       );

    //    $data_alumnos = $this->modelo->getAlumnosbyIDFicha($arr_datos);
       $data_alumnos;

       $data_excel = $this->modelo->generarReporteExcel($arr_datos);

       $array_modulos = $this->modelo->getReporteDetalladoByIDFicha($arr_datos);

       $alumnos_sin_reporte = $this->modelo->getAlumnosbyIDFicha($arr_datos);

       //ESTAS SON LAS COLUMNAS DE EXCEL EL CUAL IRÁN LAS COLUMNAS DINÁMICAS. ESTO NO COMIENZA DE 'A1' YA QUE PRIMERO VAN LAS COLUMNAS FIJAS
       $columnas = array('O1','P1','Q1','R1','S1','T1','U1','V1','W1','X1','Y1','Z1','AA1','AB1','AC1','AD1','AE1','AF1','AG1','AH1','AI1','AJ1','AK1','AL1','AM1','AN1','AO1','AP1','AQ1','AR1','AS1','AT1','AU1','AV1','AW1','AX1','AY1','AZ1','BA1','BB1','BC1','BD1','BE1','BF1','BG1','BH1','BI1','BJ1','BK1','BL1','BM1','BN1','BO1','BP1','BQ1','BR1','BS1','BT1','BU1','BV1','BW1','BX1','BY1','BZ1');


       
       $this->load->library('excel');

       //activate worksheet number 1
       $this->excel->setActiveSheetIndex(0);
       //name the worksheet
       $this->excel->getActiveSheet()->setTitle('Reporte Curso Distancia');
       //set cell A1 content with some text


       //////////////////////////////INICIO COLUMNAS FIJAS///////////////////////////////
       //AQUI VAN LAS COLUMNAS FIJAS. SI SE AGREGA O QUITA UNA COLUMNA DE ESTAS, TAMBIÉN DEBE EDITAR EL ARRAY $columnas
        $this->excel->getActiveSheet()->SetCellValue('A1', 'Empresa');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'OTIC');
        $this->excel->getActiveSheet()->SetCellValue('C1', 'Curso');
        $this->excel->getActiveSheet()->SetCellValue('D1', 'RUT');
        $this->excel->getActiveSheet()->SetCellValue('E1', 'Nombre');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'Apellido');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'Email');
        $this->excel->getActiveSheet()->SetCellValue('H1', 'Telefono');
        $this->excel->getActiveSheet()->SetCellValue('I1', 'Ficha');                
        $this->excel->getActiveSheet()->SetCellValue('J1', 'Orden de compra');                
        $this->excel->getActiveSheet()->SetCellValue('K1', 'Fecha de Inicio');                
        $this->excel->getActiveSheet()->SetCellValue('L1', 'Fecha de Termino');                
        $this->excel->getActiveSheet()->SetCellValue('M1', 'Ejecutivo');                
        $this->excel->getActiveSheet()->SetCellValue('N1', 'Ultima Conexión');                
        //ANCHO DE COLUMNAS FIJAS
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('25');
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('25');
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('25');
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('15');
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('15');
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('35');
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('15');
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth('20');
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth('20');
       //////////////////////////////FIN COLUMNAS FIJAS///////////////////////////////

       //////////////////////////////INICIO COLUMNAS DINÁMICAS///////////////////////////////
       //@@@@@@@@@AQUI SE CREAN LOS NOMBRES DE LAS COLUMNAS, MÁS ABAJO ESTÁN LOS DATOS@@@@@@@@@@@@@@@@@
        //SE PROCEDE A CREAR EL ARRAY DE LOS MÓDULOS (m1,m2,m3,etc).
        $modulos = array();
        $primer_modulo = '';
        $primer_modulo_set = false;

        for ($j=0; $j < count($data_excel); $j++) { 
            if(strpos($data_excel[$j]['modulo'], 'Módulo m') !== false){
                if(strpos($data_excel[$j]['modulo'], 'Módulo m0') === false){
                    if($data_excel[$j]['modulo'] == $primer_modulo){
                        break;
                    }else{
                        array_push($modulos, '% '.$data_excel[$j]['modulo']);
                        array_push($modulos, 'Nota '.$data_excel[$j]['modulo']);
                        if(!$primer_modulo_set){
                            $primer_modulo = $data_excel[$j]['modulo'];
                            $primer_modulo_set = true;
                        }
                    }
                }
            }
        }

        //SE AGREGA LA COLUMNA DE LA NOTA DE LA EVALUACION DIAGNÓSTICA AL PLUGIN DE EXCEL 
        //se puede meter en el for de arriba creo
        $x = 0;
        for ($i=0; $i < count($data_excel); $i++) { 
            if($data_excel[$i]['unidad'] == 'Unidad Evaluación Diagnóstica'){
                $this->excel->getActiveSheet()->SetCellValue($columnas[$x], 'Nota '.$data_excel[$i]['unidad']);
                //ANCHO DE COLUMNA
                $this->excel->getActiveSheet()->getColumnDimension(substr($columnas[$x], 0,-1))->setWidth('35');
                $x++;
                break;
            }
        }

        //SE AGREGA LA COLUMNA DE LA NOTA Y AVANCE DE LOS MÓDULOS AL PLUGIN DE EXCEL        
        for ($k=0; $k < count($modulos); $k++) { 
            $this->excel->getActiveSheet()->SetCellValue($columnas[$x], $modulos[$k]);
            //ANCHO DE COLUMNA
            $this->excel->getActiveSheet()->getColumnDimension(substr($columnas[$x], 0,-1))->setWidth('20');
            $x++;
        }

        //SE AGREGA LA COLUMNA DE LA NOTA DE LA EVALUACION FINAL AL PLUGIN DE EXCEL
        for ($i=0; $i < count($data_excel); $i++) { 
            if($data_excel[$i]['unidad'] == 'Unidad Evaluación Final'){
                $this->excel->getActiveSheet()->SetCellValue($columnas[$x], 'Nota '.$data_excel[$i]['unidad']);
                //ANCHO DE COLUMNA
                $this->excel->getActiveSheet()->getColumnDimension(substr($columnas[$x], 0,-1))->setWidth('30');
                $x++;
                break;
            }
        }
/*
        $this->excel->getActiveSheet()->SetCellValue($columnas[$x], 'Ficha');
        $this->excel->getActiveSheet()->getColumnDimension(substr($columnas[$x], 0,-1))->setWidth('10');
        $x++;      
        $this->excel->getActiveSheet()->SetCellValue($columnas[$x], 'Orden de Compra');
        $this->excel->getActiveSheet()->getColumnDimension(substr($columnas[$x], 0,-1))->setWidth('15');
        $x++;
        $this->excel->getActiveSheet()->SetCellValue($columnas[$x], 'Fecha de Inicio');
        $this->excel->getActiveSheet()->getColumnDimension(substr($columnas[$x], 0,-1))->setWidth('20');
        $x++;
        $this->excel->getActiveSheet()->SetCellValue($columnas[$x], 'Fecha Fin');
        $this->excel->getActiveSheet()->getColumnDimension(substr($columnas[$x], 0,-1))->setWidth('20');
        $x++;
        $this->excel->getActiveSheet()->SetCellValue($columnas[$x], 'Ejecutivo');
        $this->excel->getActiveSheet()->getColumnDimension(substr($columnas[$x], 0,-1))->setWidth('30');*/
       //////////////////////////////FIN COLUMNAS DINÁMICAS///////////////////////////////

       //////////////////////////////INICIO LLENADO DE DATOS///////////////////////////////
       //@@@@@@@PARA TENER UNA REFERENCIA DE COMO VIENEN LOS DATOS CONSULTAR sp_reporte_distancia_select_excel_by_ficha_id(7365,8,1); CON CUALQUIER ID DE FICHA CURSO DISTANCIA@@@@@@@@@@@@@@@@@
       //SE VALIDA QUE NO SE REPITA EL MISMO ALUMNO PARA QUE SÓLO SE INSERTE UNA VEZ LOS DATOS DE ESTE
       $rut = '';
       for ($i=0; $i < count($data_excel); $i++) { 
           if($rut != $data_excel[$i]['RUT']){
                $data_alumnos[$i]['Empresa'] = $data_excel[$i]['Empresa'];
                $data_alumnos[$i]['OTIC'] = $data_excel[$i]['OTIC'];
                $data_alumnos[$i]['Curso'] = $data_excel[$i]['Curso'];
                $data_alumnos[$i]['RUT'] = $data_excel[$i]['RUT'];
                $data_alumnos[$i]['Nombre'] = $data_excel[$i]['Nombre'];
                $data_alumnos[$i]['Apellido'] = $data_excel[$i]['Apellido'];
                $data_alumnos[$i]['Email'] = $data_excel[$i]['Email'];
                $data_alumnos[$i]['Telefono'] = $data_excel[$i]['Telefono'];
                $rut = $data_excel[$i]['RUT'];

                $data_alumnos[$i]['Ficha'] = $data_excel[$i]['Ficha'];
                $data_alumnos[$i]['Orden Compra'] = $data_excel[$i]['Orden Compra'];
                $data_alumnos[$i]['Fecha Inicio'] = $data_excel[$i]['Fecha Inicio'];
                $data_alumnos[$i]['Fecha Fin'] = $data_excel[$i]['Fecha Fin'];
                $data_alumnos[$i]['Ejecutivo'] = $data_excel[$i]['Ejecutivo'];
                $data_alumnos[$i]['Ultima Conexión'] = $data_excel[$i]['Ultima Conexión'];

                //AQUÍ SE INSERTAN EL AVANCE Y LAS NOTAS DEL ALUMNO PARA QUE QUEDE CON DISTRIBUCIÓN HORIZONTAL
                for ($j=0; $j < count($data_excel); $j++) { 
                    if($rut == $data_excel[$j]['RUT']){
                        if(strpos($data_excel[$j]['unidad'], 'Unidad Evaluación Diagnóstica') !== false){
                            $data_alumnos[$i]['u'.$j.''] = $data_excel[$j]['nota_unidad'];
                        }else
                        if(strpos($data_excel[$j]['unidad'], 'Unidad Evaluación Final') !== false){
                            $data_alumnos[$i]['u'.$j.''] = $data_excel[$j]['nota_unidad'];
                        }
                        else{
                            if(strpos($data_excel[$j]['modulo'], 'Módulo m0') === false){
                                $data_alumnos[$i]['a'.$j.''] = $data_excel[$j]['avance_modulo'];
                                $data_alumnos[$i]['n'.$j.''] = $data_excel[$j]['nota_modulo'];
                            }
                        }
                    }
                }
           }
       }

       $x = 0;

       //se insertan los alumnos que no han tenido reporte
       $filas = isset($data_alumnos) ? count($data_alumnos) : 0;

       for ($i=0; $i < count($alumnos_sin_reporte); $i++) { 
        if($alumnos_sin_reporte[$i]['codigo_tablet'] == 'Sin código activo'){                
                $this->excel->getActiveSheet()->SetCellValue('A'.($filas+$x+3), $alumnos_sin_reporte[0]['razon_social']);
                $this->excel->getActiveSheet()->SetCellValue('B'.($filas+$x+3), $alumnos_sin_reporte[0]['OTIC']);
                $this->excel->getActiveSheet()->SetCellValue('C'.($filas+$x+3), $alumnos_sin_reporte[0]['nombre_curso']);
                $this->excel->getActiveSheet()->SetCellValue('D'.($filas+$x+3), $alumnos_sin_reporte[$i]['rut']);
                $this->excel->getActiveSheet()->SetCellValue('E'.($filas+$x+3), $alumnos_sin_reporte[$i]['nombre']);
                $this->excel->getActiveSheet()->SetCellValue('F'.($filas+$x+3), $alumnos_sin_reporte[$i]['apellido_paterno']);

                $this->excel->getActiveSheet()->SetCellValue('G'.($filas+$x+3), 'NO TIENE REPORTE');
                $this->excel->getActiveSheet()->SetCellValue('h'.($filas+$x+3), 'NO TIENE REPORTE');
                
                $this->excel->getActiveSheet()->SetCellValue('I'.($filas+$x+3), $alumnos_sin_reporte[$i]['num_ficha']);
                $this->excel->getActiveSheet()->SetCellValue('J'.($filas+$x+3), $alumnos_sin_reporte[$i]['num_orden_compra']);
                $this->excel->getActiveSheet()->SetCellValue('K'.($filas+$x+3), $alumnos_sin_reporte[$i]['fecha_inicio']);
                $this->excel->getActiveSheet()->SetCellValue('L'.($filas+$x+3), $alumnos_sin_reporte[$i]['fecha_fin']);
                $this->excel->getActiveSheet()->SetCellValue('M'.($filas+$x+3), $alumnos_sin_reporte[$i]['ejecutivo']);
                
                $this->excel->getActiveSheet()->SetCellValue('N'.($filas+$x+3), 'NO TIENE REPORTE');
                $x++; 
        }
    }
       
       //////////////////////////////FIN LLENADO DE DATOS///////////////////////////////
       
       //PROGRAMACIÓN DEL PLUGIN EXCEL PARA GENERACIÓN Y DESCARGA DEL ARCHIVO
       //AQUI SE PONE EL ARRAY FINAL Y DESDE QUE FILAS COMIENZA A DESPLEGAR LOS DATOS
       if(isset($data_alumnos)){
            $this->excel->getActiveSheet()->fromArray($data_alumnos, null, 'A2');
       }
       //AQUI SE CONFIGURA EL ESTILO DE LA PRIMARA FILA, SE LE DESCUENTAN LAS COLUMNAS FIJAS + 1 POR TEMA DE ARRAY 
       $columnasx = isset($data_alumnos) ? count($data_alumnos[0]) : count($alumnos_sin_reporte)+10;
       $this->excel->getActiveSheet()->getStyle('A1:'.$columnas[$columnasx])->applyFromArray(
            array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '#D9E1F2')
                )
            )
       );
       
       
       $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
       //make the font become bold
    //    $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
       //set aligment to center for that merged cell (A1 to D1)
       $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
       $filename = 'Reporte-Distancia-'.$data_excel[0]['ficha'].'-' . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
       ob_clean();
       header('Content-Type: application/vnd.ms-excel'); //mime type
       header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
       header('Cache-Control: private'); //no cache
       //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
       //if you want to save it as .XLSX Excel 2007 format
       $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
       //force user to download the Excel file without writing it to server's HD
       $objWriter->save('php://output');
   }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
