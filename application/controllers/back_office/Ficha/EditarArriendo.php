<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class EditarArriendo extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'EditarArriendo';
    private $nombre_item_plural = 'EditarArriendo';
    private $package = 'back_office/Ficha';
    private $model = 'Ficha_model';
    private $view = 'EditarArriendo_v';
    private $controller = 'EditarArriendo';
    private $ind = '';
    private $holding = 'back_office/Holding/Holding_Model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $sede = 'back_office/Sede/Sede_model';
    private $sala = 'back_office/Sala/Sala_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->sede, 'modelsede');
        $this->load->model($this->sala, 'modelsala');


        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/EditarArriendo',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/bootstrap-datepicker/bootstrap-datepicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/editarArriendo.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Ventas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }



        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_arriendo'] = $this->modelo->get_arr_ficha_arriendo_by_id($arr_input_data);

        // $data['data_ficha'][0]['id_ficha'] 



        /* if ($data['data_arriendo'][0]['id_ficha'] == NULL) {
          redirect($this->package . '/Listar', 'refresh');
          } */

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function updateArriendo() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'empresa' => $this->input->post('empresa'),
            'hora_inicio' => $this->input->post('hora_inicio'),
            'hora_termino' => $this->input->post('hora_termino'),
            'fecha_inicio' => $this->input->post('fecha_inicio'),
            'fecha_termino' => $this->input->post('fecha_termino'),
            'lunes' => $this->input->post('lunes'),
            'martes' => $this->input->post('martes'),
            'miercoles' => $this->input->post('miercoles'),
            'jueves' => $this->input->post('jueves'),
            'viernes' => $this->input->post('viernes'),
            'sabado' => $this->input->post('sabado'),
            'domingo' => $this->input->post('domingo'),
            'sala' => $this->input->post('sala'),
            'n_dias' => $this->input->post('n_dias'),
            'valor_dia' => $this->input->post('valor_dia'),
            'total_dia' => $this->input->post('total_dia'),
            'ejecutivo' => $this->input->post('ejecutivo'),
            'break' => $this->input->post('break'),
            'n_alumnos' => $this->input->post('n_alumnos'),
            'valor_break' => $this->input->post('valor_break'),
            'total_en_break' => $this->input->post('total_en_break'),
            'valorVenta' => $this->input->post('valorVenta'),
            'iva' => $this->input->post('iva'),
            'totalVenta' => $this->input->post('totalVenta'),
            'comentario' => $this->input->post('comentario')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
        $this->modelo->set_delete_dia_ficha($arr_input_data);

        if ($arr_input_data['lunes'] != null) {
            $arr_input_data['dia'] = 1;
            $this->modelo->set_dia_ficha($arr_input_data);
        }


        if ($arr_input_data['martes'] != null) {
            $arr_input_data['dia'] = 2;
            $this->modelo->set_dia_ficha($arr_input_data);
        }

        if ($arr_input_data['miercoles'] != null) {
            $arr_input_data['dia'] = 3;
            $this->modelo->set_dia_ficha($arr_input_data);
        }

        if ($arr_input_data['jueves'] != null) {
            $arr_input_data['dia'] = 4;
            $this->modelo->set_dia_ficha($arr_input_data);
        }

        if ($arr_input_data['viernes'] != null) {
            $arr_input_data['dia'] = 5;
            $this->modelo->set_dia_ficha($arr_input_data);
        }

        if ($arr_input_data['sabado'] != null) {
            $arr_input_data['dia'] = 6;
            $this->modelo->set_dia_ficha($arr_input_data);
        }

        if ($arr_input_data['domingo'] != null) {
            $arr_input_data['dia'] = 7;
            $this->modelo->set_dia_ficha($arr_input_data);
        }

        $respuesta = $this->modelo->update_arriendo_sala($arr_input_data);

        echo json_encode($respuesta);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getUsuarioTipoEcutivoComercial() {
        // carga el combo box CodigoSence
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        echo json_encode($datos);
    }

    function getSedeCBX() {

        // carga el combo box CodigoSence
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelsede->get_arr_listar_sede_cbx($arr_data);
        echo json_encode($datos);
    }

    function getSalaCBX() {

        // carga el combo box CodigoSence
        $arr_data = array(
            'id_sede' => $this->input->post('id_sede'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelsala->get_arr_listar_sala_ficha_cbx($arr_data);
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
