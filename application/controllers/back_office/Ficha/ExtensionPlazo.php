<?php

/**
 * Alumnos
 *
 * Description...
 *
 * @version 0.0.1
 *
 *
 * Ultima edicion:  2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ExtensionPlazo extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'ExtensionPlazo';
    private $nombre_item_plural = 'Alumnos';
    private $package = 'back_office/Ficha';
    private $model = 'Ficha_model';
    private $view = 'ExtensionPlazo_v';
    private $controller = 'ExtensionPlazo';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
        $this->load->library('Functions');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/ExtencionPlazo',
                'label' => 'Extención Plazo',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/ladda/ladda-themeless.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            //   <!-- Ladda -->
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/spin.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.jquery.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/ExtensionPlazo.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');


        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );



        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id($arr_input_data);
        $data['data_ficha_preview'] = $this->modelo->get_arr_ficha_by_id_preview($arr_input_data);

        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_ficha_id($arr_input_data);

        if ($data['data_ficha'][0]['id_ficha'] == NULL) {
            redirect($this->package . '/ExtencionPlazo_v', 'refresh');
        }



        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Ventas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        //*********-------------Cargando el contenido de la tabla temporal .........................

        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $this->load->view('amelia_1/template', $data);
    }

    function cargar_datos_extetencion_plazo() {

        /* aumento el tamañao de espera del servidor   */
        $funciones = new Functions();

        $arr_input_data = array(
            'id_orden_compra' => $this->input->post('cbx_orden_compra'),
            'fecha_inicio' => $funciones->formatDateBD($this->input->post('fecha_inicio_nueva')),
            'fecha_termnino' => $funciones->formatDateBD($this->input->post('fecha_termino_nueva')),
            'comentario' => $this->input->post('comentario'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $name = $_FILES['file_orden_compra']['name'];
        $tname = $_FILES['file_orden_compra']['tmp_name'];

        $resultado = "";
        if (is_uploaded_file($tname)) {
            $splitted = explode(".", $name);
            $reversed = array_reverse($splitted);
            $extension = $reversed[0];
            //  borra posicion de un array
            unset($reversed[0]);
            $dereched = array_reverse($reversed);
            // une string separado
            $nombre_archivo = implode("", $dereched);
            $nombre_con_fecha = $nombre_archivo . '-' . date('dmYHis') . '.' . $extension;
            $dir_subida = 'rectificaciondoc/';
            $fichero_subido = $dir_subida . basename($nombre_con_fecha);
            //copy =  copia el archivo de la ruta temporal a la ruta del servidor
            if (copy($tname, $fichero_subido)) {
                $arr_input_data['ruta_archivo'] = $fichero_subido;
                $arr_input_data['nombre_archivo'] = $nombre_con_fecha;
                $resultado = $this->modelo->set_arr_rectificacion_extencion_plazo($arr_input_data);
            }
        }

        echo json_encode($resultado);
    }

    function enviaEmail() {


        $datos = $this->input->post('datos');

        $modalidad = $datos[0]['modalidad'];

        switch ($modalidad) {
            case 'elearning':
                $destinatarios = array(
                    array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                    array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );
                break;
            case 'presencial':
                $destinatarios = array(
                    array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                    array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                    array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                    array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola'),
                    array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );
                break;
        }
        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );

        $asunto = "Nueva Rectificación - Ficha N°: " . $datos[0]['num_ficha']; // asunto del correo


        $body = $this->generaHtmlSolicitudRectificacion($datos[0]); // mensaje completo HTML o text

        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */


        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);
        echo json_encode($datos);
    }

    function generaHtmlSolicitudRectificacion($data) {


        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado Ignacio ';
        $html .= '<br>';
        $html .= 'Se ha ingresado una nueva solicitud de rectificación.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['razon_social_empresa'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td  >Tipo Rectificación</td>';
        $html .= '<td class="alignright"> ' . $data['tipo_rectificacion'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Orden Compra</td>';
        $html .= '<td class="alignright"> ' . $data['num_orden_compra'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por</td>';
        $html .= '<td class="alignright"> ' . $data['usuario_ingreso'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['curso'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['modalidad'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Motivo</td>';
        $html .= '<td class="alignright">' . $data['motivo_rectificacion'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Nueva fecha de término</td>';
        $html .= '<td class="alignright">' . $data['fecha_extension'] . '</td>';
        $html .= '</tr> ';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

}

/* End of file CargaMasiva.php */
/* Location: ./application/controllers/CargaMasiva.php */
?>
