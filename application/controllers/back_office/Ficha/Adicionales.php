<?php

/**
 * Alumnos
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Adicionales extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Adicionales';
    private $nombre_item_plural = 'Alumnos';
    private $package = 'back_office/Ficha';
    private $model = 'Ficha_model';
    private $view = 'Adicionales_v';
    private $controller = 'Adicionales';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $otic = 'back_office/Otic/Otic_model';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->otic, 'modelotic');
        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/OrdenCompra',
                'label' => 'Orden Compra',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/ladda/ladda-themeless.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css')
        );



        //  js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            //   <!-- Ladda -->
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/spin.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.jquery.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/adicionales.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');


        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );



        $data['data_ficha'] = $this->modelo->get_adicionales_ficha_by_id($arr_input_data);
        


        if ($data['data_ficha'][0]['id_ficha'] == NULL) {
            die();
            redirect($this->package . '/Listar', 'refresh');
        }


        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_ficha_id($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Ventas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        //*********-------------Cargando el contenido de la tabla temporal .........................

        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $this->load->view('amelia_1/template', $data);
    }

    function cargar_archivo_datos_alumnos() {

        /* aumento el tamañao de espera del servidor   */

        $arr_datos = array();


        $user_id = $this->session->userdata('id_user');
        $user_perfil = $this->session->userdata('id_perfil');


        $id_orden_compra = $this->input->post('cbx_orden_compra');

        $name = $_FILES['file_carga_alumnos']['name'];
        $tname = $_FILES['file_carga_alumnos']['tmp_name'];

        if ($name != "") {

            $obj_excel = PHPExcel_IOFactory::load($tname);
            $sheetData = $obj_excel->getActiveSheet(0)->toArray(null, true, true, true);


            foreach ($sheetData as $index => $value) {
                if ($index != 1) {
                    if (trim($value['A']) != "" && trim($value['B']) != "" && trim($value['C']) != "") {

                        $a = $value['A'];
                        $b = $value['B'];
                        $c = $value['C'];
                        $d = $value['D'];
                        $e = $value['E'];
                        $f = $value['F'];
                        $g = $value['G'];
                        $h = $value['H'];
                        $i = $value['I'];
                        $j = $value['J'];

                        if ($a == null || $a == false) {
                            $a = "";
                        } else {
                            
                        }

                        if (intval(trim($b)) == 0) {
                            
                        } else {

                            if ($b == null || $b == false && $b != 0) {
                                $b = "";
                            }
                        }

                        if ($c == null || $c == false) {
                            $c = "";
                        }
                        if ($d == null || $d == false) {
                            $d = "";
                        }
                        if ($e == null || $e == false) {
                            $e = "";
                        }
                        if ($f == null || $f == false) {
                            $f = "";
                        }
                        if ($g == null || $g == false) {
                            $g = "";
                        }
                        if ($h == null || $h == false) {
                            $h = "";
                        }
                        if ($i == null || $i == false) {
                            $i = "";
                        }
                        /*if ($j == null || $j == false) {
                            $j = "";
                        }*/


                        $arr_datos = array(
                            'id_orden_compra' => $id_orden_compra,
                            'rut' => trim(trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $a))))),
                            'dv' => trim($b),
                            'nombre' => trim($c),
                            'apellido_paterno' => trim($d),
                            'apellido_materno' => trim($e),
                            'email' => trim($f),
                            'telefono' => trim($g),
                            'fq' => trim($h),
                            'valor_oc' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $i)))),
                           //  'valor_adicional' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $j)))),
                            'user_id' => $user_id,
                            'user_perfil' => $user_perfil
                        );
                        $this->modelo->get_arr_subir_alumnos_orden_compra($arr_datos);
                    } else {
                        break;
                    }
                }
            }

            $result['valido'] = true;
            $result['mensaje'] = 'Alumnos importados correctamente';
        } else {

            $result['valido'] = false;
            $result['mensaje'] = 'Error al procesar el archivo de carga';
        }
        //  echo json_encode($result);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    function listar_carga_orden_de_compra() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $resultado = $this->modelo->cargar_alumnos_tbl_temporal($datos_menu);

        echo json_encode($resultado);
    }

    function limpiar_carga_anterior() {

        $datos['user_id'] = $this->session->userdata('id_user');
        $datos['user_perfil'] = $this->session->userdata('id_perfil');
        $resultado = $this->modelo->get_eliminar_alumnos_temporal($datos);
        echo json_encode($resultado);
    }

    function aceptar_carga_orden_compra_alumnos() {

        // Ultima fecha Modificacion: 2017-01-16
        // Descripcion:  Metodo para registrar cargar los alumnos a un curso en especifico 
        // Usuario Actualizador : Luis Jarpa
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_orden_compra_insert_alumnos($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function cancelar_carga_alumnos() {

        // Ultima fecha Modificacion: 2016-10-13
        // Descripcion:  Metodo para eliminar la tabla temporal 
        // Usuario Actualizador : Luis Jarpa
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->get_eliminar_empresas_temporal($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function getEmpresaCBXbyHolding() {
        // carga el combo box Holding

        $id_holding = $this->input->post('id_holding');

        $arr_sesion = array(
            'id_holding' => $id_holding,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelempresa->get_arr_listar_empresa_by_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaByOC() {
        // carga el combo box Holding

        $id_orden_compra = $this->input->post('cbx_orden_compra');

        $arr_sesion = array(
            'id_orden_compra' => $id_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelempresa->get_arr_listar_empresa_by_orden_compra_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function setEmpresaByOC() {
        // carga el combo box Holding

        $id_orden_compra = $this->input->post('cbx_orden_compra');
        $id_empresa = $this->input->post('id_empresa');
        $id_otic = $this->input->post('id_otic');

        $arr_sesion = array(
            'id_orden_compra' => $id_orden_compra,
            'id_empresa' => $id_empresa,
            'id_otic' => $id_otic,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelempresa->set_arr_listar_empresa_by_orden_compra_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getOticCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelotic->get_arr_listar_otic_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getAdicionalesFichaCBX() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
    
        $datos = $this->modelo->get_adicionales_ficha_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function setItemFicha(){
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'id_item' => $this->input->post('id_item'),
            'detalle' => $this->input->post('detalle'),
            'valor' => $this->input->post('valor'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
    
        $datos = $this->modelo->set_item_ficha($arr_sesion);
        echo json_encode($datos);
    }

    function getItemFicha(){
        $arr_sesion = array(
            'id_item' => $this->input->post('id_item'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
    
        $datos = $this->modelo->get_item_ficha($arr_sesion);
        echo json_encode($datos);
    }

    function editarItemFicha(){
        $arr_sesion = array(
            'id_item_ficha_editar' => $this->input->post('id_item_ficha_editar'),
            'id_item_editar' => $this->input->post('id_item_editar'),
            'detalle_editar' => $this->input->post('detalle_editar'),
            'valor_editar' => $this->input->post('valor_editar'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
    
        $datos = $this->modelo->editar_item_ficha($arr_sesion);
        echo json_encode($datos);
    }

    function eliminarItemFicha(){
        $arr_sesion = array(
            'id_item' => $this->input->post('id_item'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
    
        $datos = $this->modelo->eliminar_item_ficha($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file CargaMasiva.php */
/* Location: ./application/controllers/CargaMasiva.php */
?>
