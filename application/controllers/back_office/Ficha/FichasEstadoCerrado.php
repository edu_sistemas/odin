<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-20 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:  2016-12-20 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class FichasEstadoCerrado extends MY_Controller {

    // Variables paramÃ©tricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Ficha';
    private $model = 'Ficha_model';
    private $view = 'FichasEstadoCerrado_v';
    private $controller = 'FichasEstadoCerrado';
    private $ind = '';
    private $package2 = 'back_office/Ficha_documento';
    private $model2 = 'Ficha_documento_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $empresa = 'back_office/Empresa/Empresa_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'documento');

        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->usuario, 'modelousuario');

        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('Functions');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );




        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/fileupload/jquery.fileupload.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/vendor/jquery.ui.widget.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.iframe-transport.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.fileupload.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/FichasEstadoCerrado.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Fichas estado cerrado";


        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function buscarFicha() {

        $funciones = new Functions();

        $start = $funciones->formatDateBD($this->input->post('start'));
        $end = $funciones->formatDateBD($this->input->post('end'));
        $estado = $this->input->post('estado');
        $empresa = $this->input->post('empresa');
        $holding = $this->input->post('holding');

        $ejecutivo = $this->input->post('ejecutivo');
        $periodo = $this->input->post('periodo');
        $num_ficha = $this->input->post('num_ficha');

        //  variables de sesion
        $arr_data = array(
            'start' => trim($start),
            'end' => trim($end),
            'estado' => $estado,
            'empresa' => $empresa,
            'holding' => $holding,
            'ejecutivo' => $ejecutivo,
            'periodo' => $periodo,
            'ficha' => trim($num_ficha),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_ficha_estado_cerrado($arr_data);
        echo json_encode($datos);
    }

    function validaFicha() {

        $id_ficha = $this->input->post('id_ficha');
        $respuesta = $this->input->post('respuesta');
        $rechazo = $this->input->post('rechazo');

        if ($respuesta == null || $respuesta == '') {
            $respuesta = '';
        }

        if ($rechazo == null || $rechazo == '') {
            $rechazo = '';
        }


        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'respuesta' => '',
            'rechazo' => '',
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_valida_envio_ficha($arr_data);
        echo json_encode($datos);
    }

    function setIDAccion() {
        $id_oc = $this->input->post('combo_oc');
        $id_accion_sence = $this->input->post('id_accion');
        $arr_data = array(
            'id_oc' => trim($id_oc),
            'id_accion' => trim($id_accion_sence),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_arr_ingresa_id_accion_sence($arr_data);
        echo json_encode($datos);
    }

    function SetcambiarOrdenCompra() {


        $id_ficha_frm_change_oc = $this->input->post('id_ficha_frm_change_oc');
        $combo_decicion = $this->input->post('combo_decicion');
        $id_oc = $this->input->post('combo_oc');
        $new_orden_compra = $this->input->post('new_orden_compra');

        $arr_data = array(
            'id_oc' => trim($id_oc),
            'new_orden_compra' => trim($new_orden_compra),
            'combo_decicion' => trim($combo_decicion),
            'id_ficha_frm_change_oc' => trim($id_ficha_frm_change_oc),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_arr_change_num_orden_compra($arr_data);
        echo json_encode($datos);
    }

    function eliminarFicha() {

        $id_ficha = $this->input->post('id_ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_delete_ficha($arr_data);
        echo json_encode($datos);
    }

    function verDocumentosFicha() {


        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->documento->get_arr_ficha_docs($arr_datos);

        echo json_encode($data);
    }

    function eliminarDocumentoFicha() {

        $id_documento = $this->input->post('id_documento');
        $nombre_documento = $this->input->post('nombre_documento');
        $fecha_registro = $this->input->post('fecha_registro');

        $arr_datos = array(
            'id_documento' => $id_documento,
            'nombre_documento' => $nombre_documento,
            'fecha_registro' => $fecha_registro,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $data = $this->documento->set_delete_ficha_doc($arr_datos);
        if ($data[0]['tipo_alerta'] == 'success') {
            unlink('fichadoc/' . $nombre_documento . '');
        }
        echo json_encode($data);
    }

    function upload_file() {

        $id_ficha = $this->input->post('id_ficha');

        $name = $_FILES['fileupload']['name'];
        $tname = $_FILES['fileupload']['tmp_name'];

        $nombre_archivo = "";
        $estado = "";
        $mensaje = "";


        // verifica si el archivo se puede subir
        if (is_uploaded_file($tname[0])) {

            $splitted = explode(".", $name[0]);
            $reversed = array_reverse($splitted);
            $extension = $reversed[0];

            //  borra posicion de un array
            unset($reversed[0]);

            $dereched = array_reverse($reversed);
            // une string separado
            $nombre_archivo = implode("", $dereched);


            $nombre_con_fecha = $nombre_archivo . '_' . date('dmYHis') . '.' . $extension;
            $nombre_con_fecha = str_replace(' ', '', $nombre_con_fecha);

            $dir_subida = 'fichadoc/';
            $fichero_subido = $dir_subida . basename($nombre_con_fecha);
            //copy =  copia el archivo de la ruta temporal a la ruta del servidor
            if (copy($tname[0], $fichero_subido)) {

                $arr_input_data = array(
                    'id_ficha' => $id_ficha,
                    'nombre_documento' => $nombre_con_fecha,
                    'ruta_documento' => $fichero_subido,
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );

                $result = $this->documento->set_document_ficha($arr_input_data);

                $id_ficha = $result[0]['id_ficha'];
                $nombre_archivo = $nombre_con_fecha;
                $estado = "ok";
                $mensaje = "Archivo Subido";
            } else {
                $nombre_archivo = $nombre_con_fecha;
                $estado = "Error";
                $mensaje = "Error al Subir Archivo" . $nombre_archivo;
            }
        } else {
            $nombre_archivo = $name;
            $estado = "Error";
            $mensaje = "Error el archivo que intenta subir no es compatible con el sistema" . $nombre_archivo;
        }
        $arr_datos = array('archivo' => $nombre_archivo, 'status' => $estado, 'mensaje' => $mensaje, 'Ficha' => $id_ficha);
        echo json_encode($arr_datos);
    }

    function getFileAdjunto() {

        $arr_input_data['id_ficha'] = $this->input->post('id_ficha');
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->documento->get_document_ficha($arr_input_data);


        echo json_encode($respuesta);
    }

    function enviarFicha() {

        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_aprobar_ficha($arr_datos);
        echo json_encode($data);
    }

    function enviaEmail() {

        $datos = $this->input->post('datos');
        $destinatarios = array();

        $asunto = "";
        $body = "";

        $num_ficha = $datos[0]['num_ficha'];
        $modalidad = $datos[0]['modalidad'];
        $envia_correo = $datos[0]['envia_correo'];


        if ($datos[0]['id_categoria'] == "1") {
            $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo
        } else {
            $asunto = "Nueva Venta - Ficha N°: " . $num_ficha;
        }

        switch ($modalidad) {
            case 'elearning':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo         

                $destinatarios = array(
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                    array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                    array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );

                $body = $this->generaHtmlSolicitudVenta($datos[0]); // mensaje completo HTML o text

                break;
            case 'presencial':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha; // asunto del correo
                $body = $this->generaHtmlPresencial($datos[0]); // mensaje completo HTML o text

                $destinatarios = array(
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                    array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                    array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                    array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola Salazar'),
                    array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'JosÃ© Gonzalez'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );
                break;
            case 'arriendo':
                $asunto = "Nueva Venta - Ficha N°: " . $num_ficha;
                $body = $this->generaHtmlSolicitudArriendo($datos[0]); // mensaje completo HTML o text

                $destinatarios = array(
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'JosÃ© Gonzalez'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );

                break;
        }

        $cc = array(
           array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );


        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        if ($envia_correo == '1') {
            $this->load->library('Libreriaemail');
            $res = $this->libreriaemail->CallAPISendMail($data_email);
        }

        echo json_encode($body);
    }

    function enviaCorreoRequiereEQuipos() {

        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $data_correo = $this->modelo->get_info_llevar_equipos_ficha($arr_datos);

        $asunto = "";
        $body = "";
        $asunto = "Solicitud de Equipos - Ficha N°: " . $data_correo[0]['num_ficha']; // asunto del correo 

        $body = $this->generaHtmlSolicitudEquipos($data_correo[0]); // mensaje completo HTML o text

        $destinatarios = array(
            array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
            array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
           array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
            array('email' => 'jmedina@edutecno.com', 'nombre' => 'Jesus Medina'),
            array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
            array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
            array('email' => 'sfigueroa@edutecno.com', 'nombre' => 'Simon Figueroa')
        );

        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );


        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */


        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);


        echo json_encode($body);
    }

    function generaHtmlSolicitudEquipos($data) {

        $html = "";

        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba Venta Curso Presencial</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)';
        $html .= '<br>';
        $html .= 'Se ha solicitado llevar equipos para esta Ficha ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td>Ejecutivo</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Rut de la Empresa</td>';
        $html .= '<td class="alignright"> ' . $data['rut_empresa'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>Razón Social</td>';
        $html .= '<td class="alignright"> ' . $data['empresa'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr >';
        $html .= '<td >Giro</td>';
        $html .= '<td class="alignright"> ' . $data['giro_empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Dirección de la empresa</td>';
        $html .= '<td class="alignright">' . $data['direccion_empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>Dirección de ejecución del curso</td>';
        $html .= '<td class="alignright">' . $data['lugar_ejecucion'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Curso a dictar</td>';
        $html .= '<td class="alignright">' . $data['nombre_curso'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Versión del Curso </td>';
        $html .= '<td class="alignright">' . $data['nombre_version'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Cantidad de Alumnos</td>';
        $html .= '<td class="alignright">' . $data['cantidad_alumno'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Inicio del curso';
        $html .= '</td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Fin del Curso.';
        $html .= '</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Horario:';
        $html .= '</td>';
        $html .= '<td class="alignright">' . $data['horario'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Dias:';
        $html .= '</td>';
        $html .= '<td class="alignright">' . $data['dias'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr  class="total">';
        $html .= '<td>Nombre del contacto';
        $html .= '</td>';
        $html .= '<td class="alignright">' . $data['nombre_contacto'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="">';
        $html .= '<td>Telefono del contacto';
        $html .= '</td>';
        $html .= '<td class="alignright">' . $data['telefono_contacto'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>Observaciones';
        $html .= '</td>';
        $html .= '<td class="alignright">' . $data['observaciones'] . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 - Piso 2, Santiago, Región Metropolitana';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function generaHtmlPresencial($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba Venta Curso Presencial</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado Ignacio Pérez';
        $html .= '<br>';
        $html .= '<br>';
        $html .= 'Se ha ingresado una nueva venta de un curso presencial';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';

        $html .= '<tr>';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr >';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['nombre_curso'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['nombre_modalidad'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Código Sence</td>';
        $html .= '<td class="alignright">' . $data['codigo_sence'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Horario del curso</td>';
        $html .= '<td class="alignright">' . $data['hora_inicio'] . ' - ' . $data['hora_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Sala</td>';
        $html .= '<td class="alignright">' . $data['sala'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Dias</td>';
        $html .= '<td class="alignright">' . $data['dias'] . '</td>';
        $html .= '</tr> ';

        $html .= '<tr>';
        $html .= '<td>N° Participantes </td>';
        $html .= '<td class="alignright">' . $data['cant_participante'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr  class="total">';
        $html .= '<td>Observaciones </td>';
        $html .= '<td class="alignright">' . $data['observaciones'] . '</td>';
        $html .= '</tr>';


        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com/back_office/Ficha_Control/ResumenSeguimiento/index/' . $data['id_ficha'] . '" target="_blank" class="btn-primary">Ver en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    function generaHtmlSolicitudVenta($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Solicitud de venta</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados Ignacio Pérez';
        $html .= '<br>';
        $html .= 'Se ha ingresado una nueva solicitud de venta ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';

        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';

        $html .= '<tr  >';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr class="total" >';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>N° Participantes </td>';
        $html .= '<td class="alignright">' . $data['cant_participante'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>Solicitado por</td>';
        $html .= '<td class="alignright"> ' . $data['usuario_ingreso_nombre'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['nombre_curso'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['nombre_modalidad'] . '</td>';
        $html .= '</tr>      ';
        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr> ';


        $html .= '<tr>';
        $html .= '<td>Dias</td>';
        $html .= '<td class="alignright">' . $data['dias'] . '</td>';
        $html .= '</tr> ';

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">Edutecno - Huérfanos 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function generaHtmlSolicitudArriendo($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Solicitud de venta</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado Ignacio Pérez';
        $html .= '<br>';

        $html .= 'Se ha ingresado una nueva solicitud de arriendo';

        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';

        $html .= '<tr>';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright">  ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>   ';

        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por</td>';
        $html .= '<td class="alignright"> ' . $data['usuario_ingreso_nombre'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr> ';



        $html .= '<tr>';
        $html .= '<td>Dias</td>';
        $html .= '<td class="alignright">' . $data['dias'] . '</td>';
        $html .= '</tr> ';

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function getMotivoRechazo() {
        $id_ficha = $this->input->post('id_ficha');
        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $data = $this->modelo->get_motivo_rechazo_ficha($arr_datos);
        echo json_encode($data);
    }

    function getEstadoCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_estado_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaCBX() {
// carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaByHoldingCBX() {
        // carga el combo box Holding

        $arr_input_data['id_holding'] = $this->input->post('id_holding');
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
        $datos = $this->modelempresa->get_arr_listar_empresa_by_holding_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getOcByFichaCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_oc_by_ficha_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEjecutivo() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        echo json_encode($datos);
    }

    function getPeriodo() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_periodo_venta($arr_data);
        echo json_encode($datos);
    }

    function getEventosFicha() {

        $arr_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_ficha_select_eventos_by_id($arr_data);
        echo json_encode($datos);
    }

    function getLogFicha() {

        $arr_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_sp_ficha_select_log_by_id($arr_data);
        echo json_encode($datos);
    }

    function enviarFichaFacturacion() {

        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_aprobar_ficha_facturacion($arr_datos);
        echo json_encode($data);
    }

    function getMotivoAnulacion() {

        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_motivo_anulacion_ficha($arr_datos);
        echo json_encode($data);
    }

    function sendMailFacturacion() {

        $datos = $this->input->post('datos');
        $destinatarios = array();

        $asunto = "";
        $body = "";

        $num_ficha = $datos[0]['num_ficha'];
        $modalidad = $datos[0]['modalidad'];


        if ($datos[0]['id_categoria'] == "1") {
            $asunto = "Facturación - Ficha N°: " . $num_ficha; // asunto del correo
        } else {
            $asunto = "Facturación - Ficha N°: " . $num_ficha;
        }

        switch ($modalidad) {
            case 'elearning':
                $asunto = "Facturación - Ficha N°: " . $num_ficha; // asunto del correo      

                $destinatarios = array(
                    array('email' => 'mmoreno@edutecno.com', 'nombre' => 'Miguel Moreno(Tom)'),
                    array('email' => 'marenas@edutecno.com', 'nombre' => 'Mirena Arenas')
                );

                $body = $this->generaHtmlEnvioCorreoElearningFacturacion($datos[0]); // mensaje completo HTML o text

                break;
            case 'presencial':

                $asunto = "Facturación - Ficha N°: " . $num_ficha; // asunto del correo         
                $body = $this->generaHtmlEnvioCorreoPresencialFacturacion($datos[0]); // mensaje completo HTML o text

                $destinatarios = array(
                    array('email' => 'mmoreno@edutecno.com', 'nombre' => 'Miguel Moreno(Tom)'),
                    array('email' => 'marenas@edutecno.com', 'nombre' => 'Mirena Arenas'),
                    array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola Salazar')
                );
                break;
            case 'arriendo':
                $asunto = "Facturación - Ficha N°: " . $num_ficha; // asunto del correo    
                $body = $this->generaHtmlEnvioCorreoArriendoFacturacion($datos[0]); // mensaje completo HTML o text

                $destinatarios = array(
                    array('email' => 'mmoreno@edutecno.com', 'nombre' => 'Miguel Moreno(Tom)'),
                    array('email' => 'marenas@edutecno.com', 'nombre' => 'Mirena Arenas')
                );

                break;
        }

        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );


        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */


        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);


        echo json_encode($body);
    }

    function generaHtmlEnvioCorreoElearningFacturacion($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)';
        $html .= '<br>';
        $html .= 'La Ficha N° : ' . $data['num_ficha'] . ' ya puede ser Facturada ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>O.C.</td>';
        $html .= '<td class="alignright">' . $data['ordenes_oc'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['nombre_modalidad'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr >';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['nombre_curso'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Código Sence</td>';
        $html .= '<td class="alignright">' . $data['codigo_sence'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>ID Sence</td>';
        $html .= '<td class="alignright">' . $data['id_Sence'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr >';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>                                                          ';
        $html .= '<tr  class="total">';
        $html .= '<td>Observaciones </td>';
        $html .= '<td class="alignright">' . $data['observaciones'] . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 Piso 2 - Galería España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function generaHtmlEnvioCorreoPresencialFacturacion($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)';
        $html .= '<br>';
        $html .= 'La Ficha N° : ' . $data['num_ficha'] . ' ya puede ser Facturada ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>O.C.</td>';
        $html .= '<td class="alignright">' . $data['ordenes_oc'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['nombre_modalidad'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr >';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['nombre_curso'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Código Sence</td>';
        $html .= '<td class="alignright">' . $data['codigo_sence'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>ID Sence</td>';
        $html .= '<td class="alignright">' . $data['id_Sence'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr >';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>                                                          ';
        $html .= '<tr  class="total">';
        $html .= '<td>Observaciones </td>';
        $html .= '<td class="alignright">' . $data['observaciones'] . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 Piso 2 - Galería España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function generaHtmlEnvioCorreoArriendoFacturacion($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)';
        $html .= '<br>';
        $html .= 'La Ficha N°: ' . $data['num_ficha'] . ' ya puede ser Facturada ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr>';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>  ';
        $html .= '<tr >';
        $html .= '<td >Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>  ';
        $html .= '<tr  class="total">';
        $html .= '<td>Observaciones </td>';
        $html .= '<td class="alignright">' . $data['observaciones'] . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 Piso 2 - Galería España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */