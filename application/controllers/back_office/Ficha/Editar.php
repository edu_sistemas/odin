<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Ficha';
    private $model = 'Ficha_model';
    private $view = 'Editar_v';
    private $controller = 'Editar';
    private $ind = '';
    private $modalidad = 'back_office/Modalidad/Modalidad_model';
    private $curso = 'back_office/Curso/Curso_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $otic = 'back_office/Otic/Otic_model';
    private $sence = 'back_office/Code_sence/Code_sence_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $sede = 'back_office/Sede/Sede_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $sala = 'back_office/Sala/Sala_model';
    private $version = 'back_office/Version/Version_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->modalidad, 'modelmodalidad');
        $this->load->model($this->curso, 'modelcurso');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->otic, 'modelotic');
        $this->load->model($this->sence, 'modelosence');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->sede, 'modelosede');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->sala, 'modelsala');
        $this->load->model($this->version, 'modelversion');

        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('Functions');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Editar',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/editar.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Ventas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getFichaByID(){
        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_ficha_by_id($arr_input_data);

        echo json_encode($datos);
    }

    function Editar_ficha() {

        $funciones = new Functions();
        $id_ficha = $this->input->post('id_ficha');
        $id_relacionar_ficha_con = $this->input->post('relacionar_ficha_con');
        $id_relacionar_ficha_con_hide = $this->input->post('id_relacionar_ficha_con');


        $categoria = 1;
        $comentario = $this->input->post('comentario');
        $curso = $this->input->post('curso');
        $empresa = $this->input->post('empresa');
        $otic = $this->input->post('otic');

        $id_version = $this->input->post('version');

        $direccion_empresa = $this->input->post('direccion_empresa');
        $nombre_contacto_empresa = $this->input->post('nombre_contacto_empresa');
        $telefono_contacto_empresa = $this->input->post('telefono_contacto_empresa');

        $lugar_ejecucion = $this->input->post('lugar_ejecucion');
        $lugar_entrega_diplomas = $this->input->post('lugar_entrega_diplomas');


        $chk_sence = $this->input->post('chk_sence');
        $codi_sence = $this->input->post('cod_sence');
        $ejecutivo = $this->input->post('ejecutivo');

        $id_solicitud_tablet = $this->input->post('id_solicitud_tablet');

        // en caso que el id de solicitud sea nulo
        if ($id_solicitud_tablet == "" || $id_solicitud_tablet == false || $id_solicitud_tablet == null) {
            $id_solicitud_tablet = 0;
        }

        // en caso que el id fiicha no sea seleccionado o sea nulo 
        if ($id_relacionar_ficha_con == "" || $id_relacionar_ficha_con == false || $id_relacionar_ficha_con == null) {
            $id_relacionar_ficha_con = 0;
        }


        $txt_hora_inicio = $this->input->post('txt_hora_inicio');
        $txt_hora_termino = $this->input->post('txt_hora_termino');
        $sala = $this->input->post('sala');


        $lunes = $this->input->post('lunes');
        $martes = $this->input->post('martes');
        $miercoles = $this->input->post('miercoles');
        $jueves = $this->input->post('jueves');
        $viernes = $this->input->post('viernes');
        $sabado = $this->input->post('sabado');
        $domingo = $this->input->post('domingo');

        $chk_sin_orden_compra = $this->input->post('chk_sin_orden_compra');

        $txt_num_o_c = $this->input->post('txt_num_o_c');

        $fecha_inicio = $funciones->formatDateBD($this->input->post('fecha_inicio'));
        $fecha_termino = $funciones->formatDateBD($this->input->post('fecha_termino'));

        $fecha_cierre = $fecha_termino;

        if ($txt_num_o_c == "" || $txt_num_o_c == false) {
            $txt_num_o_c = "";
        }

        if ($chk_sin_orden_compra == "" || $chk_sin_orden_compra == false) {
            $chk_sin_orden_compra = 0;
        }

        if ($otic == "" || $otic == false) {
            $otic = "1";
        }

        $arr_input_data_ficha['id_ficha'] = $id_ficha;
        $arr_input_data_ficha['user_id'] = $this->session->userdata('id_user');
        $arr_input_data_ficha['user_perfil'] = $this->session->userdata('id_perfil');

        $this->modelo->set_delete_dia_ficha($arr_input_data_ficha);

        if ($lunes != "" && $lunes != false) {
            $arr_input_data_ficha['dia'] = 1;
            $this->modelo->set_dia_ficha($arr_input_data_ficha);
        }

        if ($martes != "" && $martes != false) {
            $arr_input_data_ficha['dia'] = 2;
            $this->modelo->set_dia_ficha($arr_input_data_ficha);
        }

        if ($miercoles != "" && $miercoles != false) {
            $arr_input_data_ficha['dia'] = 3;
            $this->modelo->set_dia_ficha($arr_input_data_ficha);
        }

        if ($jueves != "" && $jueves != false) {
            $arr_input_data_ficha['dia'] = 4;
            $this->modelo->set_dia_ficha($arr_input_data_ficha);
        }

        if ($viernes != "" && $viernes != false) {
            $arr_input_data_ficha['dia'] = 5;
            $this->modelo->set_dia_ficha($arr_input_data_ficha);
        }

        if ($sabado != "" && $sabado != false) {
            $arr_input_data_ficha['dia'] = 6;
            $this->modelo->set_dia_ficha($arr_input_data_ficha);
        }

        if ($domingo != "" && $domingo != false) {
            $arr_input_data_ficha['dia'] = 7;
            $this->modelo->set_dia_ficha($arr_input_data_ficha);
        }


        if ($txt_num_o_c != "") {
            $orden_compra = explode(",", $txt_num_o_c);
            for ($i = 0; $i < count($orden_compra); $i++) {
                $lugar_ejecucion = $this->input->post('lugar_ejecucion');
                $lugar_entrega_diplomas = $this->input->post('lugar_entrega_diplomas');
                $arr_input_data_orden_compra = array(
                    'id_ficha' => $id_ficha,
                    'id_curso' => $curso,
                    'id_version' => $id_version,
                    'id_empresa' => $empresa,
                    'id_otic' => $otic,
                    'sin_orden_compra' => $chk_sin_orden_compra,
                    'comentario' => $comentario,
                    'num_orden_compra' => $orden_compra[$i],
                    'fecha_inicio' => $fecha_inicio,
                    'fecha_termino' => $fecha_termino,
                    'fecha_cierre' => $fecha_cierre,
                    'hora_inicio' => $txt_hora_inicio,
                    'hora_termino' => $txt_hora_termino,
                    'id_categoria' => $categoria,
                    'ejecutivo' => $ejecutivo,
                    'sala' => $sala,
                    'lugar_ejecucion' => $lugar_ejecucion,
                    'lugar_entrega_diplomas' => $lugar_entrega_diplomas,
                    'direccion_empresa' => $direccion_empresa,
                    'nombre_contacto_empresa' => $nombre_contacto_empresa,
                    'telefono_contacto_empresa' => $telefono_contacto_empresa,
                    'chk_sence' => $chk_sence,
                    'code_sence' => $codi_sence,
                    'id_solicitud_tablet' => $id_solicitud_tablet,
                    'id_relacionar_ficha_con' => $id_relacionar_ficha_con,
                    'id_relacionar_ficha_con_hide' => $id_relacionar_ficha_con_hide,
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );


                $respuesta = $this->modelo->set_edit_orden_compra($arr_input_data_orden_compra);
            }
            $result = $respuesta;
        } else {

            //  si no viene ningun orden de compra
            $arr_input_data_orden_compra = array(
                'id_ficha' => $id_ficha,
                'id_curso' => $curso,
                'id_version' => $id_version,
                'id_empresa' => $empresa,
                'id_otic' => $otic,
                'sin_orden_compra' => $chk_sin_orden_compra,
                'comentario' => $comentario,
                'num_orden_compra' => "edu",
                'fecha_inicio' => $fecha_inicio,
                'fecha_termino' => $fecha_termino,
                'fecha_cierre' => $fecha_cierre,
                'hora_inicio' => $txt_hora_inicio,
                'hora_termino' => $txt_hora_termino,
                'id_categoria' => $categoria,
                'ejecutivo' => $ejecutivo,
                'sala' => $sala,
                'lugar_ejecucion' => $lugar_ejecucion,
                'lugar_entrega_diplomas' => $lugar_entrega_diplomas,
                'chk_sence' => $chk_sence,
                'code_sence' => $codi_sence,
                'id_solicitud_tablet' => $id_solicitud_tablet,
                'id_relacionar_ficha_con' => $id_relacionar_ficha_con,
                'id_relacionar_ficha_con_hide' => $id_relacionar_ficha_con_hide,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

            $respuesta = $this->modelo->set_edit_orden_compra($arr_input_data_orden_compra);
            $result = $respuesta;
        }

        echo json_encode($result);
    }

    function deleteOC() {

        $id_ficha = $this->input->post('id_ficha');
        $id_orden_compra = $this->input->post('id_orden_compra');
        $num_orden_compra = $this->input->post('num_orden_compra');

        // carga el combo box Holding
        $arr_sesion = array(
            'id_ficha' => $id_ficha,
            'id_orden_compra' => $id_orden_compra,
            'num_orden_compra' => $num_orden_compra,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_delete_orden_compra($arr_sesion);

        echo json_encode($datos);
    }

    function getModalidadCBX() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelmodalidad->get_arr_modalidad_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getModalidadByCodeSence() {
        $arr_sesion = array(
            'id_code_sence' => $this->input->post('id_code_sence'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_modalidad_by_codeSence($arr_sesion);
        echo json_encode($datos);
    }

    function getCodeSenceCBX() {

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_codeSence_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getCursoCBX() {

        $id_modalidad = $this->input->post('modalidad');
        // carga el combo box Holding
        $arr_sesion = array(
            'id_modalidad' => $id_modalidad,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelcurso->get_arr_listar_curso_cbx_ficha($arr_sesion);
        echo json_encode($datos);
    }

    function getVersionCBX() {

        $id_curso = $this->input->post('curso');
        // carga el combo box Holding
        $arr_sesion = array(
            'id_curso' => $id_curso,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelversion->get_arr_listar_version_cbx_by_curso($arr_sesion);
        echo json_encode($datos);
    }

    function getDireccionesEmpresaCBX() {

        $id_empresa = $this->input->post('empresa');

        $arr_sesion = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelempresa->get_arr_listar_direcciones_by_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getNombreContactosEmpresaCBX() {

        $id_empresa = $this->input->post('empresa');

        $arr_sesion = array(
            'id_empresa' => $id_empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelempresa->get_arr_listar_nombre_contacto_by_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getTelefonosContactoEmpresaCBX() {

        $id_contacto = $this->input->post('id_contacto');

        $arr_sesion = array(
            'id_contacto' => $id_contacto,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelempresa->get_arr_listar_telefono_contacto_by_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaCBX() {
        // carga el combo box Holding


        $id_holding = $this->input->post('id_holding');


        $arr_sesion = array(
            'id_holding' => $id_holding,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        $datos = $this->modelempresa->get_arr_listar_empresa_by_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getOticCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelotic->get_arr_listar_otic_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getCodigoSenceByCursoCBX() {
        // carga el combo box CodigoSence
        $arr_data = array(
            'id_curso' => $this->input->post('id_curso'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelosence->get_arr_codigo_sence_by_cursoFicha($arr_data);
        echo json_encode($datos);
    }

    function getUsuarioTipoEcutivoComercial() {
        // carga el combo box CodigoSence
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        echo json_encode($datos);
    }

    function getSedeCBX() {

        // carga el combo box CodigoSence
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelosede->get_arr_listar_sede_cbx($arr_data);
        echo json_encode($datos);
    }

    function getSalaCBX() {

        // carga el combo box CodigoSence
        $arr_data = array(
            'id_sede' => $this->input->post('id_sede'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelsala->get_arr_listar_sala_ficha_cbx($arr_data);
        echo json_encode($datos);
    }

    function getLugarEjecucion() {

        // carga el combo box CodigoSence
        $arr_data = array(
            'id_sede' => $this->input->post('id_sede'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelosede->get_arr_lugar_ejecucion($arr_data);
        echo json_encode($datos);
    }

    function getFichasRelacionarCBX() {

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_FichasRelacionar_cbx($arr_sesion);

        echo json_encode($datos);
    }

    function getSolicitudesTabletCBX() {
        $arr_sesion = array(
            'id_curso' => $this->input->post('id_curso'),
            'id_ejecutivo_comercial' => $this->input->post('id_ejecutivo_comercial'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->getSolicitudesTabletCBX($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
