<?php

/**
 * Alumnos
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2017-01-18 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Rectificacion extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Rectificacion';
    private $nombre_item_plural = 'Alumnos';
    private $package = 'back_office/Ficha';
    private $package_empresa = 'back_office/Empresa';
    private $package_otic = 'back_office/Otic';
    private $model = 'Ficha_model';
    private $model_empresa = 'Empresa_model';
    private $model_otic = 'Otic_model';
    private $view = 'Rectificacion_v';
    private $controller = 'Rectificacion';
    private $ind = '';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package_empresa . '/' . $this->model_empresa, 'empresa');
        $this->load->model($this->package_otic . '/' . $this->model_otic, 'otic');
        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Rectificacion',
                'label' => 'Rectificacion',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/ladda/ladda-themeless.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            //   <!-- Ladda -->
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/spin.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.jquery.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/rectificacion.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');


        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );




        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id($arr_input_data);
        $data['data_ficha_preview'] = $this->modelo->get_arr_ficha_by_id_preview($arr_input_data);
        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_ficha_id($arr_input_data);
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Rectificación";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        //*********-------------Cargando el contenido de la tabla temporal .........................

        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $this->load->view('amelia_1/template', $data);
    }

    function cargar_archivo_datos_alumnos() {

        /* aumento el tamañao de espera del servidor   */

        $arr_datos = array();

        $user_id = $this->session->userdata('id_user');
        $user_perfil = $this->session->userdata('id_perfil');

        $id_orden_compra = $this->input->post('cbx_orden_compra');


        $id_accion = $this->input->post('accion');
        //   $n_rect = $this->input->post('n_rect');

        $tipo_rectificacion = "";

        $name = $_FILES['file_carga_alumnos']['name'];
        $tname = $_FILES['file_carga_alumnos']['tmp_name'];

        if ($name != "") {

            $obj_excel = PHPExcel_IOFactory::load($tname);
            $sheetData = $obj_excel->getActiveSheet(0)->toArray(null, true, true, true);

            switch ($id_accion) {
                case 1:
                    $tipo_rectificacion = "Agregar Alumnos";
                    foreach ($sheetData as $index => $value) {
                        if ($index != 1) {
                            if (trim($value['A']) != "" && trim($value['B']) != "" && trim($value['C']) != "") {

                                $rut = $value['A'];
                                $dv = $value['B'];
                                $nombre = $value['C'];
                                $segundo_nombre = $value['D'];
                                $apellido_p = $value['E'];
                                $apellido_m = $value['F'];
                                $correo = $value['G'];
                                $telefono = $value['H'];
                                $fq = $value['I'];
                                $valor_fq = $value['J'];
                                $valor_adicional = $value['K'];

                                if ($rut == null || $rut == false) {
                                    $rut = "";
                                } else {
                                    
                                }

                                if (intval(trim($dv)) == 0) {
                                    
                                } else {

                                    if ($dv == null || $dv == false && $dv != 0) {
                                        $dv = "";
                                    }
                                }

                                if ($nombre == null || $nombre == false) {
                                    $nombre = "";
                                }
                                if ($segundo_nombre == null || $segundo_nombre == false) {
                                    $segundo_nombre = "";
                                }
                                if ($apellido_p == null || $apellido_p == false) {
                                    $$apellido_p = "";
                                }
                                if ($apellido_m == null || $apellido_m == false) {
                                    $apellido_m = "";
                                }
                                if ($correo == null || $correo == false) {
                                    $correo = "";
                                }
                                if ($telefono == null || $telefono == false) {
                                    $telefono = "";
                                }
                                if ($fq == null || $fq == false) {
                                    $fq = "";
                                }
                                if ($valor_fq == null || $valor_fq == false) {
                                    $valor_fq = "";
                                }
                                if ($valor_adicional == null || $valor_adicional == false) {
                                    $valor_adicional = "";
                                }


                                $arr_datos = array(
                                    'id_accion' => 1,
                                    'tipo_rectificacion' => $tipo_rectificacion,
                                    'id_orden_compra' => $id_orden_compra,
                                    'rut' => trim($rut),
                                    'dv' => trim($dv),
                                    'nombre' => trim($nombre),
                                    'segundo_nombre' => trim($segundo_nombre),
                                    'apellido_paterno' => trim($apellido_p),
                                    'apellido_materno' => trim($apellido_m),
                                    'email' => trim($correo),
                                    'telefono' => trim($telefono),
                                    'fq' => trim($fq),
                                    'valor_fq' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $valor_fq)))),
                                    'valor_adicional' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $valor_adicional)))),
                                    'user_id' => $user_id,
                                    'user_perfil' => $user_perfil
                                );


                                $this->modelo->get_arr_subir_insert_alumnos_orden_compra_rectificacion($arr_datos);
                            } else {
                                break;
                            }
                        }
                    }

                    break;
                case 2:

                    $tipo_rectificacion = "Actualizar Alumnos";

                    foreach ($sheetData as $index => $value) {
                        if ($index != 1) {
                            if (trim($value['A']) != "" && trim($value['B']) != "" && trim($value['C']) != "") {


                                $rut = $value['A'];
                                $dv = $value['B'];
                                $nombre = $value['C'];
                                $segundo_nombre = $value['D'];
                                $apellido_p = $value['E'];
                                $apellido_m = $value['F'];
                                $correo = $value['G'];
                                $telefono = $value['H'];
                                $fq = $value['I'];
                                $valor_fq = $value['J'];
                                $valor_adicional = $value['K'];



                                if ($rut == null || $rut == false) {
                                    $rut = "";
                                } else {
                                    
                                }

                                if (intval(trim($dv)) == 0) {
                                    
                                } else {

                                    if ($dv == null || $dv == false && $dv != 0) {
                                        $dv = "";
                                    }
                                }

                                if ($nombre == null || $nombre == false) {
                                    $nombre = "";
                                }
                                if ($segundo_nombre == null || $segundo_nombre == false) {
                                    $segundo_nombre = "";
                                }
                                if ($apellido_p == null || $apellido_p == false) {
                                    $$apellido_p = "";
                                }
                                if ($apellido_m == null || $apellido_m == false) {
                                    $apellido_m = "";
                                }
                                if ($correo == null || $correo == false) {
                                    $correo = "";
                                }
                                if ($telefono == null || $telefono == false) {
                                    $telefono = "";
                                }
                                if ($fq == null || $fq == false) {
                                    $fq = "";
                                }
                                if ($valor_fq == null || $valor_fq == false) {
                                    $valor_fq = "";
                                }
                                if ($valor_adicional == null || $valor_adicional == false) {
                                    $valor_adicional = "";
                                }

                                $arr_datos = array(
                                    'id_accion' => 2,
                                    'tipo_rectificacion' => $tipo_rectificacion,
                                    'id_orden_compra' => $id_orden_compra,
                                    'rut' => trim($rut),
                                    'dv' => trim($dv),
                                    'nombre' => trim($nombre),
                                    'segundo_nombre' => trim($segundo_nombre),
                                    'apellido_paterno' => trim($apellido_p),
                                    'apellido_materno' => trim($apellido_m),
                                    'email' => trim($correo),
                                    'telefono' => trim($telefono),
                                    'fq' => trim($fq),
                                    'valor_fq' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $valor_fq)))),
                                    'valor_adicional' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $valor_adicional)))),
                                    'user_id' => $user_id,
                                    'user_perfil' => $user_perfil
                                );


                                $this->modelo->get_arr_subir_alumnos_orden_compra_rectificacion($arr_datos);
                            } else {
                                break;
                            }
                        }
                    }
                    break;
                case 3:
                    $tipo_rectificacion = "Eliminar Alumnos";
                    foreach ($sheetData as $index => $value) {
                        if ($index != 1) {
                            if (trim($value['A']) != "" && trim($value['B']) != "" && trim($value['C']) != "") {

                                $a = $value['A'];
                                $b = $value['B'];
                                $c = $value['C'];
                                $d = $value['D'];
                                $e = $value['E'];
                                $f = $value['F'];
                                $g = $value['G'];


                                if ($a == null || $a == false) {
                                    $a = "";
                                }

                                if (intval($b) == 0) {
                                    
                                } else {

                                    if ($b == null || $b == false && $b != 0) {
                                        $b = "";
                                    }
                                }

                                if ($c == null || $c == false) {
                                    $c = "";
                                }
                                if ($d == null || $d == false) {
                                    $d = "";
                                }
                                if ($e == null || $e == false) {
                                    $e = "";
                                }
                                if ($f == null || $f == false) {
                                    $f = "";
                                }
                                if ($g == null || $g == false) {
                                    $g = "";
                                }



                                $arr_datos = array(
                                    'id_accion' => 3,
                                    'tipo_rectificacion' => $tipo_rectificacion,
                                    'id_causa' => $g,
                                    'id_orden_compra' => $id_orden_compra,
                                    'rut' => $a,
                                    'dv' => $b,
                                    'nombre' => $c,
                                    'apellido_paterno' => $d,
                                    'apellido_materno' => $e,
                                    'user_id' => $user_id,
                                    'user_perfil' => $user_perfil
                                );

                                $this->modelo->get_arr_subir_alumnos_orden_compra_eliminar_rectificacion($arr_datos);
                            } else {
                                break;
                            }
                        }
                    }
                    break;
            }
            $result['valido'] = true;
            $result['mensaje'] = 'Alumnos importados correctamente';
        } else {

            $result['valido'] = false;
            $result['mensaje'] = 'Error al procesar el archivo de carga';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    function listar_carga_orden_de_compra() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $resultado = $this->modelo->cargar_alumnos_tbl_temporal($datos_menu);

        echo json_encode($resultado);
    }

    function limpiar_carga_anterior() {

        $datos['user_id'] = $this->session->userdata('id_user');
        $datos['user_perfil'] = $this->session->userdata('id_perfil');
        $resultado = $this->modelo->get_eliminar_alumnos_temporal($datos);
        echo json_encode($resultado);
    }

    function aceptar_carga_orden_compra_alumnos() {

        // Ultima fecha Modificacion: 2017-01-16
        // Descripcion:  Metodo para registrar cargar los alumnos a un curso en especifico 
        // Usuario Actualizador : Luis Jarpa
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado

        $name = $_FILES['file_carga_alumnos']['name'];
        $tname = $_FILES['file_carga_alumnos']['tmp_name'];
        $name2 = $_FILES['file_orden_compra']['name'];
        $tname2 = $_FILES['file_orden_compra']['tmp_name'];
        $accion = $this->input->post('accion');
        $comentario = $this->input->post('comentario');


        $nombre_archivo_plantilla = "";
        $ruta_archivo_plantilla = "";
        $nombre_archivo_adjunto = "";
        $ruta_archivo_adjunto = "";

        // verifica si el archivo se puede subir
        if (is_uploaded_file($tname)) {
            $splitted = explode(".", $name);
            $reversed = array_reverse($splitted);
            $extension = $reversed[0];
            //  borra posicion de un array
            unset($reversed[0]);
            $dereched = array_reverse($reversed);
            // une string separado
            $nombre_archivo = implode("", $dereched);
            $nombre_con_fecha = $nombre_archivo . '_' . date('dmYHis') . '.' . $extension;

            $dir_subida = 'rectificaciondoc/';
            $fichero_subido = $dir_subida . basename($nombre_con_fecha);
            //copy =  copia el archivo de la ruta temporal a la ruta del servidor
            if (copy($tname, $fichero_subido)) {
                $nombre_archivo_plantilla = $nombre_con_fecha;
                $ruta_archivo_plantilla = $fichero_subido;
            }
        }

        // verifica si el archivo se puede subir
        if (is_uploaded_file($tname2)) {
            $splitted = explode(".", $name2);
            $reversed = array_reverse($splitted);
            $extension = $reversed[0];
            //  borra posicion de un array
            unset($reversed[0]);
            $dereched = array_reverse($reversed);
            // une string separado
            $nombre_archivo = implode("", $dereched);
            $nombre_con_fecha = $nombre_archivo . '_' . date('dmYHis') . '.' . $extension;
            $dir_subida = 'rectificaciondoc/';
            $fichero_subido = $dir_subida . basename($nombre_con_fecha);
            //copy =  copia el archivo de la ruta temporal a la ruta del servidor
            if (copy($tname2, $fichero_subido)) {
                $nombre_archivo_adjunto = $nombre_con_fecha;
                $ruta_archivo_adjunto = $fichero_subido;
            }
        }
        $arr_input_data['nombre_archivo_plantilla'] = $nombre_archivo_plantilla;
        $arr_input_data['ruta_archivo_plantilla'] = $ruta_archivo_plantilla;

        $arr_input_data['nombre_archivo_adjunto'] = $nombre_archivo_adjunto;
        $arr_input_data['ruta_archivo_adjunto'] = $ruta_archivo_adjunto;
        $arr_input_data['comentario'] = $comentario;



        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
        

        // llamado al modelo
        $respuesta = "";

        switch ($accion) {
            case 1:
                $respuesta = $this->modelo->set_arr_orden_compra_accion_insert_alumnos($arr_input_data);
                break;
            case 2:
                $respuesta = $this->modelo->set_arr_orden_compra_accion_actualizar_alumnos($arr_input_data);
                break;
            case 3:
                $respuesta = $this->modelo->set_arr_orden_compra_accion_eliminar_alumnos($arr_input_data);
                break;
        }


        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function cancelar_carga_alumnos() {

        // Ultima fecha Modificacion: 2016-10-13
        // Descripcion:  Metodo para eliminar la tabla temporal 
        // Usuario Actualizador : Luis Jarpa
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->get_eliminar_empresas_temporal($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function getRazonSocialCBX() {

        // Ultima fecha Modificacion: 2017-09-11
        // Descripcion:  Metodo para obtener las empresas de un holding 
        // Usuario Actualizador : David De Filippi
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado

        $arr_input_data['id_holding'] = $this->input->post('id_holding');

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->empresa->get_arr_listar_empresa_by_holding_cbx($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function setNuevaOC() {
        // Ultima fecha Modificacion: 2017-09-11
        // Descripcion:  Metodo para agregar nuevas ordenes de compra en la pantalla rectificacion 
        // Usuario Actualizador : David De Filippi
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado

        $arr_input_data['x_id_ficha'] = $this->input->post('x_id_ficha');
        $arr_input_data['razon_social'] = $this->input->post('razon_social');
        $arr_input_data['otic'] = $this->input->post('otic');
        $arr_input_data['numero_oc'] = $this->input->post('numero_oc');
        $arr_input_data['comite_bipartito'] = $this->input->post('comite_bipartito');
        $arr_input_data['observaciones'] = $this->input->post('observaciones');

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_agregar_oc_rectificacion($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function getOticsCBX() {
        // Ultima fecha Modificacion: 2017-09-11
        // Descripcion:  Metodo para obtener las empresas de un holding 
        // Usuario Actualizador : David De Filippi
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->otic->get_arr_listar_otic_cbx($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function enviaEmail() {

        $datos = $this->input->post('datos');
        $destinatarios = array();
        $modalidad = $datos[0]['modalidad'];

        switch ($modalidad) {
            case 'elearning':
                $destinatarios = array(
                    array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                    array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );
                break;
            case 'presencial':
                $destinatarios = array(
                    array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                    array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                    array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                    array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola'),
                    array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Ulecia'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );
                break;
        }
        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );

        $asunto = "Nueva Rectificación - Ficha N°: " . $datos[0]['num_ficha']; // asunto del correo
        //
        // presenciales
        $body = $this->generaHtmlSolicitudRectificacion($datos[0]); // mensaje completo HTML o text

        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);
        echo json_encode($datos);
    }

    function generaHtmlSolicitudRectificacion($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado Ignacio, ';
        $html .= '<br>';
        $html .= 'Se ha ingresado una nueva solicitud de rectificación.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['razon_social_empresa'] . ' </td>';
        $html .= '</tr>                                                                ';
        $html .= '<tr class="total">';
        $html .= '<td >Tipo Rectificación</td>';
        $html .= '<td class="alignright"> ' . $data['tipo_rectificacion'] . ' </td>';
        $html .= '</tr>  ';
        $html .= '<tr >';
        $html .= '<td>Orden Compra</td>';
        $html .= '<td class="alignright"> ' . $data['num_orden_compra'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>N° Participantes </td>';
        $html .= '<td class="alignright">' . $data['cantidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '';
        $html .= '<tr >';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por</td>';
        $html .= '<td class="alignright"> ' . $data['usuario_ingreso'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['curso'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['nombre_modalidad'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Motivo</td>';
        $html .= '<td class="alignright">' . $data['motivo_rectificacion'] . ' </td>';
        $html .= '</tr> ';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';



        return $html;
    }

}

/* End of file CargaMasiva.php */
/* Location: ./application/controllers/CargaMasiva.php */
?>
