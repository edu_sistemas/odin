<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-20 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:  2016-12-20 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Rectificaciones extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Ficha';
    private $model = 'Ficha_model';
    private $view = 'Rectificaciones_v';
    private $controller = 'Listar';
    private $ind = '';
    private $package2 = 'back_office/Ficha_documento';
    private $model2 = 'Ficha_documento_model';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'documento');


        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Rectificaciones',
                'label' => 'Rectificaciones',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/fileupload/jquery.fileupload.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/vendor/jquery.ui.widget.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.iframe-transport.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.fileupload.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/rectificaciones.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Rectificación";


        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function buscarRectificacion() {

        $id_ficha = $this->input->post('txt_id_ficha');
        $orden_compra = $this->input->post('txt_oc_ficha');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_resctificaciones($arr_data);
        echo json_encode($datos);
    }

    function getMotivoRechazo() {

        $id_rectificacion = $this->input->post('id_rectificacion');

        $arr_datos = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_motivo_rechazo_rectificacion($arr_datos);

        echo json_encode($data);
    }

    function verDocumentosRectificacion() {

        $id_rectificacion = $this->input->post('id_rectificacion');

        $arr_datos = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_decomentos_rectificacion($arr_datos);

        echo json_encode($data);
    }

    function uploadFile() {

        $id_rectificacion = $this->input->post('id_rectificacion');

        $name = $_FILES['fileupload']['name'];
        $tname = $_FILES['fileupload']['tmp_name'];

        $nombre_archivo = "";
        $estado = "";
        $mensaje = "";


        // verifica si el archivo se puede subir
        if (is_uploaded_file($tname[0])) {

            $splitted = explode(".", $name[0]);
            $reversed = array_reverse($splitted);
            $extension = $reversed[0];

            //  borra posicion de un array
            unset($reversed[0]);

            $dereched = array_reverse($reversed);
            // une string separado
            $nombre_archivo = implode("", $dereched);
            $nombre_con_fecha = $nombre_archivo . date('dmYHis') . '.' . $extension;

            $dir_subida = 'rectificaciondoc/';
            $fichero_subido = $dir_subida . basename($nombre_con_fecha);
            //copy =  copia el archivo de la ruta temporal a la ruta del servidor
            if (copy($tname[0], $fichero_subido)) {

                $arr_input_data = array(
                    'id_rectificacion' => $id_rectificacion,
                    'nombre_documento' => $nombre_con_fecha,
                    'ruta_documento' => $fichero_subido,
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );

                $result = $this->modelo->set_document_rectificacion($arr_input_data);

                $id_rectificacion = $result[0]['id_rectificacion'];
                $nombre_archivo = $nombre_con_fecha;
                $estado = "ok";
                $mensaje = "Archivo Subido";
            } else {
                $nombre_archivo = $nombre_con_fecha;
                $estado = "Error";
                $mensaje = "Error al Subir Archivo" . $nombre_archivo;
            }
        } else {
            $nombre_archivo = $name;
            $estado = "Error";
            $mensaje = "Error el archivo que intenta subir no es compatible con el sistema" . $nombre_archivo;
        }
        $arr_datos = array('archivo' => $nombre_archivo, 'status' => $estado, 'mensaje' => $mensaje, 'Rectificacion' => $id_rectificacion);
        echo json_encode($arr_datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
