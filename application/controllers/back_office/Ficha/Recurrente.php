<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Recurrente extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Ficha';
    private $model = 'Recurrente_model';
    private $view = 'Recurrente_v';
    private $controller = 'Editar';
    private $ind = '';
    private $modalidad = 'back_office/Modalidad/Modalidad_model';
    private $curso = 'back_office/Curso/Curso_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $otic = 'back_office/Otic/Otic_model';
    private $sence = 'back_office/Code_sence/Code_sence_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $sede = 'back_office/Sede/Sede_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $sala = 'back_office/Sala/Sala_model';
    private $ficha = 'back_office/Ficha/Ficha_model';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->modalidad, 'modelmodalidad');
        $this->load->model($this->curso, 'modelcurso');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->otic, 'modelotic');
        $this->load->model($this->sence, 'modelosence');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->sede, 'modelosede');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->sala, 'modelsala');
        $this->load->model($this->otic, 'modelotic');
        $this->load->model($this->ficha, 'modelficha');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Resumen',
                'label' => 'Resumen',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/recurrente.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Ventas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id_resumen_recurrentes($arr_input_data);


        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function editarDatosRecurrentes() {

        $id_ficha = $this->input->post('id_ficha');

        $id_relacionar_ficha_con = $this->input->post('relacionar_ficha_con');
        $id_relacionar_ficha_con_hide = $this->input->post('id_relacionar_ficha_con');

        $hora_inicio = $this->input->post('txt_hora_inicio');
        $hora_termino = $this->input->post('txt_hora_termino');
        $sala = $this->input->post('sala');
        $sede = $this->input->post('sede');

        $lugar = $this->input->post('lugar_ejecucion');
        $lunes = $this->input->post('lunes');
        $martes = $this->input->post('martes');
        $miercoles = $this->input->post('miercoles');
        $jueves = $this->input->post('jueves');
        $viernes = $this->input->post('viernes');
        $sabado = $this->input->post('sabado');
        $domingo = $this->input->post('domingo');
        $observacion = $this->input->post('comentario');



        // en caso que el id fiicha no sea seleccionado o sea nulo 
        if ($id_relacionar_ficha_con == "" || $id_relacionar_ficha_con == false || $id_relacionar_ficha_con == null) {
            $id_relacionar_ficha_con = 0;
            $id_relacionar_ficha_con_hide = 0;
        }
        //post para la correccion del rechazo

        $rechazo_correcion = $this->input->post('rechazo_correcion');


        $arr_input_data_dia = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_ficha_f = $this->modelo->get_arr_ficha_by_id_resumen_recurrentes($arr_input_data_dia);

        $dia_lunes = "";
        $dia_martes = "";
        $dia_miercoles = "";
        $dia_jueves = "";
        $dia_viernes = "";
        $dia_sabado = "";
        $dia_domingo = "";

        $this->modelo->set_delete_dia_ficha($arr_input_data_dia);

        if ($lunes != "" && $lunes != false) {
            $arr_input_data_dia['diax'] = 1;
            $dia_lunes = "1 ";
            $this->modelo->set_arr_modificar_recurrentes($arr_input_data_dia);
        }

        if ($martes != "" && $martes != false) {
            $arr_input_data_dia['diax'] = "2";
            $dia_martes = "- 2 ";
            $this->modelo->set_arr_modificar_recurrentes($arr_input_data_dia);
        }

        if ($miercoles != "" && $miercoles != false) {
            $arr_input_data_dia['diax'] = 3;
            $dia_miercoles = "- 3 ";
            $this->modelo->set_arr_modificar_recurrentes($arr_input_data_dia);
        }

        if ($jueves != "" && $jueves != false) {
            $arr_input_data_dia['diax'] = 4;
            $dia_jueves = "- 4 ";
            $this->modelo->set_arr_modificar_recurrentes($arr_input_data_dia);
        }

        if ($viernes != "" && $viernes != false) {
            $arr_input_data_dia['diax'] = 5;
            $dia_viernes = "- 5 ";
            $this->modelo->set_arr_modificar_recurrentes($arr_input_data_dia);
        }

        if ($sabado != "" && $sabado != false) {
            $arr_input_data_dia['diax'] = 6;
            $dia_sabado = "- 6 ";
            $this->modelo->set_arr_modificar_recurrentes($arr_input_data_dia);
        }

        if ($domingo != "" && $domingo != false) {
            $arr_input_data_dia['diax'] = 7;
            $dia_domingo = "- 7 ";
            $this->modelo->set_arr_modificar_recurrentes($arr_input_data_dia);
        }

        $dia_ficha = $dia_lunes . $dia_martes . $dia_miercoles . $dia_jueves . $dia_viernes . $dia_sabado . $dia_domingo;


//
        if ($sala != $data_ficha_f[0]['id_sala']) {
            // Borra la reserva
            $id_reserva = $data_ficha_f[0]['id_reserva'];

            $arr_input_data_dia = array(
                'id_reserva' => $id_reserva,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $this->modelo->set_delete_reserva_sala_ficha($arr_input_data_dia);
            //Fin de borrar la reserva

            if ($lunes == null || $lunes == '') {
                $lunes = 0;
            } else {
                $lunes = 1;
            }
            if ($martes == null || $martes == '') {
                $martes = 0;
            } else {
                $martes = 2;
            }
            if ($miercoles == null || $miercoles == '') {
                $miercoles = 0;
            } else {
                $miercoles = 3;
            }
            if ($jueves == null || $jueves == '') {
                $jueves = 0;
            } else {
                $jueves = 4;
            }
            if ($viernes == null || $viernes == '') {
                $viernes = 0;
            } else {
                $viernes = 5;
            }
            if ($sabado == null || $sabado == '') {
                $sabado = 0;
            } else {
                $sabado = 6;
            }
            if ($domingo == null || $domingo == '') {
                $domingo = 0;
            } else {
                $domingo = 7;
            }

            $fecha_inicio = $this->input->post('fecha_inicio');
            $hora_inicio = $this->input->post('txt_hora_inicio');
            $fecha_format_inicio = date("Y-m-d", strtotime($fecha_inicio));

            $fechaHoraInicio = $fecha_format_inicio . ' ' . $hora_inicio;

            $fecha_termino = $this->input->post('fecha_termino');
            $fecha_format_termino = date("Y-m-d", strtotime($fecha_termino));
            $hora_termino = $this->input->post('txt_hora_termino');

            $fechaHoraTermino = $fecha_format_termino . ' ' . $hora_termino;



            $arr_input_data_reserva = array(
                'id_ficha' => $data_ficha_f[0]['id_ficha'],
                'fecha_inicio' => $fechaHoraInicio,
                'fecha_termino' => $fechaHoraTermino,
                'lunes' => $lunes,
                'martes' => $martes,
                'miercoles' => $miercoles,
                'jueves' => $jueves,
                'viernes' => $viernes,
                'sabado' => $sabado,
                'domingo' => $domingo,
                'estado' => 1,
                'id_sala' => $sala,
                'nombre_reserva' => 'Ficha N°: ' . $data_ficha_f[0]['num_ficha'] . ' ' . $data_ficha_f[0]['empresa'],
                'detalle_reserva' => $data_ficha_f[0]['comentario_orden_compra'],
                'break' => $data_ficha_f[0]['comentario_orden_compra'],
                'computadores' => $data_ficha_f[0]['comentario_orden_compra'],
                'ejecutivo' => $data_ficha_f[0]['id_ejecutivo'],
                'holding' => $data_ficha_f[0]['id_holding'],
                'empresa' => $data_ficha_f[0]['id_empresa'],
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

            $this->modelo->set_insert_reserva_recurrente($arr_input_data_reserva);
        }


        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'hora_inicio' => $hora_inicio,
            'hora_termino' => $hora_termino,
            'sala' => $sala,
            'sede' => $sede,
            'lugar' => $lugar,
            'observacion' => $observacion,
            'dia_ficha' => $dia_ficha,
            'id_relacionar_ficha_con' => $id_relacionar_ficha_con,
            'id_relacionar_ficha_con_hide' => $id_relacionar_ficha_con_hide,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $respuesta = $this->modelo->set_edit_orden_compra($arr_input_data);
        echo json_encode($respuesta);
    }

    function getSedeCBX() {

        // carga el combo box CodigoSence
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelosede->get_arr_listar_sede_cbx($arr_data);
        echo json_encode($datos);
    }

    function getSalaCBX() {

        // carga el combo box CodigoSence
        $arr_data = array(
            'id_sede' => $this->input->post('id_sede'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelsala->get_arr_listar_sala_ficha_cbx($arr_data);
        echo json_encode($datos);
    }

    function getLugarEjecucion() {
        // carga el combo box CodigoSence
        $arr_data = array(
            'id_sede' => $this->input->post('id_sede'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelosede->get_arr_lugar_ejecucion($arr_data);
        echo json_encode($datos);
    }

    function enviaEmail() {

        //$datos = $this->input->post('datos');
        $id_ficha = $this->input->post('id_ficha');

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_correo = $this->modelo->get_arr_ficha_by_id_resumen_recurrentes_correo($arr_input_data);

        $destinatarios = array();
        $asunto = "";
        $body = "";


        $modalidad = $data_correo[0]['modalidad'];
        $email_ejecutivo = $data_correo[0]['correo_comercial'];
        $ejecutivo = $data_correo[0]['nombre_ejecutivo'];


        switch ($modalidad) {
            case 'elearning':
                $destinatarios = array(
                    array('email' => $email_ejecutivo, 'nombre' => $ejecutivo),
                    array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                    array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                    array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );

                break;

            case 'presencial':

                $destinatarios = array(
                    array('email' => $email_ejecutivo, 'nombre' => $ejecutivo),
                    array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                    array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                    array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                    array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                    array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola'),
                    array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                    array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                    array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                    array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                );

                // presenciales 
                // con copia a 
                break;
        }
        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );


        $asunto = "Edición - Ficha N°: " . $data_correo[0]['num_ficha'];  // asunto del correo

        $body = $this->generaHtmlPresencial($data_correo);

        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */
        

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);


        echo json_encode($body);
    }

    function generaHtmlPresencial($data_correo) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba Venta Curso Presencial</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<br>';
        $html .= '<br>';
        $html .= 'Se Modificaron datos de la ficha';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';

        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data_correo[0]['num_ficha'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr >';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data_correo[0]['descripcion_producto'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data_correo[0]['empresa'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Horario del curso</td>';
        $html .= '<td class="alignright">' . $data_correo[0]['hora_inicio'] . ' - ' . $data_correo[0]['hora_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Sala</td>';
        $html .= '<td class="alignright">' . $data_correo[0]['sala'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Dias</td>';
        $html .= '<td class="alignright">' . $data_correo[0]['dias_corto'] . '</td>';
        $html .= '</tr> ';

        $html .= '<tr  class="total">';
        $html .= '<td>Observaciones </td>';
        $html .= '<td class="alignright">' . $data_correo[0]['comentario_orden_compra'] . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com/back_office/Ficha_Control/ResumenSeguimiento/index/' . $data_correo[0]['id_ficha'] . '" target="_blank" class="btn-primary">Ver en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

    function getFichasRelacionarCBX() {

        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelficha->get_arr_FichasRelacionar_cbx($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
