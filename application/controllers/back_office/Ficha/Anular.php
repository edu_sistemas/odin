<?php

/**
 * Alumnos
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Anular extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Anular';
    private $nombre_item_plural = 'Anular';
    private $package = 'back_office/Ficha';
    private $model = 'Anular_model';
    private $view = 'Anular_v';
    private $controller = 'Anular';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/OrdenCompra',
                'label' => 'Orden Compra',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/ladda/ladda-themeless.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/fileupload/jquery.fileupload.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css')
        );



        //  js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'), //   <!-- Ladda -->
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/spin.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.jquery.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/vendor/jquery.ui.widget.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.iframe-transport.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.fileupload.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha/Anular.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');


        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );



        $data['data_ficha'] = $this->modelo->get_arr_anulacion_by_id_ficha($arr_input_data); // ok
        $data['data_ficha_preview'] = $this->modelo->get_anulacion_by_id_ficha_view($arr_input_data); //ok


        if ($data['data_ficha'][0]['id_ficha'] == NULL) {
            redirect($this->package . '/Listar', 'refresh');
        }


        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_ficha_id($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Ventas";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        //*********-------------Cargando el contenido de la tabla temporal .........................

        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $this->load->view('amelia_1/template', $data);
    }

    function cargar_archivo() {



        $name = $_FILES['fileupload']['name'];
        $tname = $_FILES['fileupload']['tmp_name'];

        $nombre_archivo = "";
        $estado = "";
        $mensaje = "";


        // verifica si el archivo se puede subir
        if (is_uploaded_file($tname[0])) {

            $splitted = explode(".", $name[0]);
            $reversed = array_reverse($splitted);
            $extension = $reversed[0];

            //  borra posicion de un array
            unset($reversed[0]);
            $id_ficha = $this->input->post('id_ficha');
            $dereched = array_reverse($reversed);
            // une string separado
            $nombre_archivo = implode("", $dereched);
            $nombre_con_fecha = $nombre_archivo . '_' . date('dmYHis') . '.' . $extension;

            $dir_subida = 'anulaciondoc/';
            $fichero_subido = $dir_subida . basename($nombre_con_fecha);
            //copy =  copia el archivo de la ruta temporal a la ruta del servidor
            if (copy($tname[0], $fichero_subido)) {
                $arr_input_data = array(
                    'id_ficha' => $id_ficha,
                    'nombre_documento' => $nombre_con_fecha,
                    'ruta_documento' => $fichero_subido,
                    'motivo' => 'Documento de Anulacion',
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );

                $result = $this->modelo->set_document_ficha($arr_input_data);

                $id_ficha = $result[0]['id_ficha'];
                $nombre_archivo = $nombre_con_fecha;
                $estado = "ok";
                $mensaje = "Archivo Subido";
            } else {
                $nombre_archivo = $nombre_con_fecha;
                $estado = "Error";
                $mensaje = "Error al Subir Archivo" . $nombre_archivo;
            }
        } else {
            $nombre_archivo = $name;
            $estado = "Error";
            $mensaje = "Error el archivo que intenta subir no es compatible con el sistema" . $nombre_archivo;
        }
        $arr_datos = array('archivo' => $nombre_archivo, 'status' => $estado, 'mensaje' => $mensaje, 'Ficha' => $id_ficha);
        echo json_encode($arr_datos);
    }

    function verDocumentosFicha() {


        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_anulacion_docs($arr_datos);
        echo json_encode($data);
    }

    function enviar_Ficha() {

        $id_ficha = $this->input->post('id_ficha_f');
        $motivo = $this->input->post('motivo_anulacion');

        $array_datos = array(
            'id_ficha' => $id_ficha,
            'motivo' => $motivo,
            'estado_edu' => 62,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_motivo_anulacion($array_datos);
        echo json_encode($data);
    }

}

/* End of file CargaMasiva.php */
/* Location: ./application/controllers/CargaMasiva.php */
?>
