<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2017-01-19 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Colaborador';
    private $model = 'Colaborador_model';
    private $view = 'Agregar_v';
    private $controller = 'Agregar';
    private $ind = '';
    private $nacionalidad = 'back_office/Nacionalidad/Nacionalidad_Model';
    private $genero = 'back_office/Genero/Genero_Model';
    private $estadoCivil = 'back_office/EstadoCivil/EstadoCivil_Model';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->nacionalidad, 'modelnacionalidad');
        $this->load->model($this->genero, 'modelgenero');
        $this->load->model($this->estadoCivil, 'modelEstadoCivil');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . '/back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/bootstrap-datepicker/bootstrap-datepicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Colaborador/agregar.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Colaboradores";
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;



        $this->load->view('amelia_1/template', $data);
    }

    function AgregarColaborador() {

        $arr_input_data = array(
            'rut' => $this->input->post('rut'),
            'dv' => $this->input->post('dv'),
            'nombre' => $this->input->post('nombre'),
            //'seg_nombre_colaborador' => $this->input->post('seg_nombre_colaborador'),
            'apellido_pat' => $this->input->post('apellido_pat'),
            'apellido_mat' => $this->input->post('apellido_mat'),
            'nacionalidad' => $this->input->post('nacionalidad'),
            'genero' => $this->input->post('genero'),
            'fechaNac' => $this->input->post('fechaNac'),
            'estado_civil' => $this->input->post('estado_civil')
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_agregar_colaborador($arr_input_data);

        echo json_encode($respuesta);
    }

    public function getNacionalidadCBX()
    {
       $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelnacionalidad->get_arr_listar_nacionalidad_cbx($arr_sesion);
        echo json_encode($datos);
    }

     public function getGeneroCBX()
    {
       $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelgenero->get_arr_listar_genero_cbx($arr_sesion);
        echo json_encode($datos);
    }

      public function getEstadoCivilCBX()
    {
       $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelEstadoCivil->get_arr_listar_estadoCivil_cbx($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
