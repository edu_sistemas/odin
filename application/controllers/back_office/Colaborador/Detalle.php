<?php
 
/**
 * Detalle
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2017-01-20 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Detalle extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar'; 
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Colaborador';
    private $model = 'Colaborador_model';
    private $view = 'Detalle_v';
    private $controller = 'Detalle';
    private $ind = '';
    private $comuna = 'back_office/Comuna/Comuna_model';
    private $nacionalidad = 'back_office/Nacionalidad/Nacionalidad_Model';
    private $genero = 'back_office/Genero/Genero_Model';
    private $estadoCivil = 'back_office/EstadoCivil/EstadoCivil_Model';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->comuna, 'modelcomuna');
        $this->load->model($this->nacionalidad, 'modelnacionalidad');
        $this->load->model($this->genero, 'modelgenero');
        $this->load->model($this->estadoCivil, 'modelEstadoCivil');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del Detalle Colaborador
    public function index($id_perfil = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_perfil == false) {
            redirect($this->package . '/Listar', 'refresh');
        }


        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
             array(
                'href' => base_url() . $this->ind .'/back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Detalle',
                'label' => 'Detalle',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
            // array('src' => base_url() . 'assets/amelia/css/gmaps.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Colaborador/detalle.js'),
            		array('src' => 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCXFAmo7nctNNqsCNWb1bUBx6F2HrLBUMQ&libraries=places&callback=initAutocomplete
            		')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Colaboradores";



        $arr_input_data = array(
            'id_colaborador' => $id_perfil,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_colaborador'] = $this->modelo->sp_colaborador_select_by_id($arr_input_data);
        if($data['data_colaborador'] == false){
            redirect($this->package . '/Listar', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //   Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        

        // google maps
        // $this->load->library('googlemaps');

        // $config['center'] = '37.4419, -122.1419';
        // $config['zoom'] = 'auto';
        // $config['places'] = TRUE;
        // $config['placesAutocompleteInputID'] = 'myPlaceTextBox';
        // $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
        // $config['placesAutocompleteOnChange'] = 'alert(\'You selected a place\');';
        // $this->googlemaps->initialize($config);
        // $data['map'] = $this->googlemaps->create_map();

        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);


    }

    function updateBasicosColaborador() {
        /*
          Ultima fecha Modificacion: 2016-11-25
          Descripcion:  función para modificar los módulos que ya existen
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_colaborador'         	    => $this->input->post('id_colaborador'),
            'rut'     			   	   	    => $this->input->post('rut'),
            'dv'       				        => $this->input->post('dv'),
            'nombre'          			    => $this->input->post('nombre'),
            'apellido_paterno'              => $this->input->post('apellido_paterno'),
            'apellido_materno'            	=> $this->input->post('apellido_materno'),
            'fecha_nacimiento'       	    => $this->input->post('fecha_nacimiento'),
            'genero'  						=> $this->input->post('genero'),
            'estado_civil'  				=> $this->input->post('estado_civil'),
        	'nacionalidad'  				=> $this->input->post('nacionalidad')
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_editar_colaborador($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function getComunaCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelcomuna->get_arr_listar_comuna_cbx($arr_sesion);
        echo json_encode($datos);
    }
    
    
    
    function getParentescoCBX() {
    	// carga el combo box Holding
    	$arr_sesion = array(
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelo->get_arr_listar_parentesco_cbx($arr_sesion);
    	echo json_encode($datos);
    }
    
    
    function getDatosBasicosColaborador() {
    	// carga el combo box Holding
    	$arr_sesion = array(
    			'id_colaborador' => $this->input->post('id_colaborador'),
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelo->sp_colaborador_select_by_id($arr_sesion);
    	echo json_encode($datos);
    }
    
    function getTipoDatoCBX() {
    	// carga el combo box Holding
    	$arr_sesion = array(
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelo->get_sp_tipo_dato_colaborador_cbx($arr_sesion);
    	echo json_encode($datos);
    }

    function getDireccionesByIdCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'id_colaborador' => $this->input->post('id_colaborador'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_direcciones_by_id_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getDireccionById() {
        // carga el combo box Holding
        $arr_sesion = array(
            'id_direccion' => $this->input->post('id_direccion'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_direccion_by_id($arr_sesion);
        echo json_encode($datos);
    }
    
    
   
    function getTelefonosById() {
    	// carga el combo box Holding
    	$arr_sesion = array(
    			'id_colaborador' => $this->input->post('id_colaborador'),
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelo->get_sp_colaborador_select_telefonos($arr_sesion);
    	echo json_encode($datos);
    }
    
    function getMailsById() {
    	// carga el combo box Holding
    	$arr_sesion = array(
    			'id_colaborador' => $this->input->post('id_colaborador'),
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelo->get_sp_colaborador_select_mails($arr_sesion);
    	echo json_encode($datos);
    }

    function setDireccionColaborador()
    {
       $arr_input_data = array(
            'id_c'                          => $this->input->post('id_c'),
            'd_calle'                       => $this->input->post('d_calle'),
            'd_numero'                      => $this->input->post('d_numero'),
            'd_departamento'                => $this->input->post('d_departamento'),
            'd_comuna'                      => $this->input->post('d_comuna'),
            'latitud'                       => $this->input->post('latitud'),
            'longitud'                      => $this->input->post('longitud')
        );
            //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
            $arr_input_data['user_id'] = $this->session->userdata('id_user');
            $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

            // llamado al modelo
            $respuesta = $this->modelo->set_arr_direccion_colaborador($arr_input_data);

            //  respuesta procesada por la peticion AJAX
            echo json_encode($respuesta);
    }
    
    function setTelefonoColaborador()
    {
    	$arr_input_data = array(
    			'id_colaborador'                => $this->input->post('id_c_t'),
    			'tipo'                      	=> $this->input->post('tipoTelefonoCBX'),
    			'numero'                      	=> $this->input->post('telefono')
    	);
    	//  datos de la sesion , neecesarios para todo procedimeitno de almacenado
    	$arr_input_data['user_id'] = $this->session->userdata('id_user');
    	$arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
    	
    	// llamado al modelo
    	$respuesta = $this->modelo->set_sp_colaborador_insert_telefono($arr_input_data);
    	
    	//  respuesta procesada por la peticion AJAX
    	echo json_encode($respuesta);
    }
    function setMailColaborador()
    {
    	$arr_input_data = array(
    			'id_colaborador'                => $this->input->post('id_c_m'),
    			'tipo'                      	=> $this->input->post('tipoMailCBX'),
    			'email'                      	=> $this->input->post('email')
    	);
    	//  datos de la sesion , neecesarios para todo procedimeitno de almacenado
    	$arr_input_data['user_id'] = $this->session->userdata('id_user');
    	$arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
    	
    	// llamado al modelo
    	$respuesta = $this->modelo->set_sp_colaborador_insert_mail($arr_input_data);
    	
    	//  respuesta procesada por la peticion AJAX
    	echo json_encode($respuesta);
    }
    
    
    function eliminarTelefonoColaborador()
    {
    	$arr_input_data = array(
    			'id_telefono'                => $this->input->post('id_telefono')
    	);
    	//  datos de la sesion , neecesarios para todo procedimeitno de almacenado
    	$arr_input_data['user_id'] = $this->session->userdata('id_user');
    	$arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
    	
    	// llamado al modelo
    	$respuesta = $this->modelo->set_sp_colaborador_delete_telefono($arr_input_data);
    	
    	//  respuesta procesada por la peticion AJAX
    	echo json_encode($respuesta);
    }
    
    function eliminarMailColaborador()
    {
    	$arr_input_data = array(
    			'id_email'                => $this->input->post('id_email')
    	);
    	//  datos de la sesion , neecesarios para todo procedimeitno de almacenado
    	$arr_input_data['user_id'] = $this->session->userdata('id_user');
    	$arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
    	
    	// llamado al modelo
    	$respuesta = $this->modelo->set_sp_colaborador_delete_mail($arr_input_data);
    	
    	//  respuesta procesada por la peticion AJAX
    	echo json_encode($respuesta);
    }
    
    
    function updateDireccionColaborador()
    {
    	$arr_input_data = array(
    			'id_direccion'                => $this->input->post('d_direccion_e'),
    			'calle'                		  => $this->input->post('d_calle_e'),
    			'numero'                => $this->input->post('d_numero_e'),
    			'departamento'                => $this->input->post('d_departamento_e'),
    			'id_comuna'                => $this->input->post('d_comuna_e'),
    			'latitud'                => $this->input->post('latitud_e'),
    			'longitud'                => $this->input->post('longitud_e')
    	);
    	//  datos de la sesion , neecesarios para todo procedimeitno de almacenado
    	$arr_input_data['user_id'] = $this->session->userdata('id_user');
    	$arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
    	
    	// llamado al modelo
    	$respuesta = $this->modelo->set_sp_colaborador_update_direccion($arr_input_data);
    	
    	//  respuesta procesada por la peticion AJAX
    	echo json_encode($respuesta);
    }
    
    
    
    function eliminarDireccionColaborador()
    {
    	$arr_input_data = array(
    			'id_direccion'                => $this->input->post('id_direccion')
    	);
    	//  datos de la sesion , neecesarios para todo procedimeitno de almacenado
    	$arr_input_data['user_id'] = $this->session->userdata('id_user');
    	$arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
    	
    	// llamado al modelo
    	$respuesta = $this->modelo->set_sp_colaborador_delete_direccion($arr_input_data);
    	
    	//  respuesta procesada por la peticion AJAX
    	echo json_encode($respuesta);
    }
    
    public function getNacionalidadCBX()
    {
    	$arr_sesion = array(
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelnacionalidad->get_arr_listar_nacionalidad_cbx($arr_sesion);
    	echo json_encode($datos);
    }
    
    public function getGeneroCBX()
    {
    	$arr_sesion = array(
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelgenero->get_arr_listar_genero_cbx($arr_sesion);
    	echo json_encode($datos);
    }
    
    public function getEstadoCivilCBX()
    {
    	$arr_sesion = array(
    			'user_id' => $this->session->userdata('id_user'),
    			'user_perfil' => $this->session->userdata('id_perfil')
    	);
    	$datos = $this->modelEstadoCivil->get_arr_listar_estadoCivil_cbx($arr_sesion);
    	echo json_encode($datos);
    }
}

/* End of file Editar.php */
/* Location: ./application/controllers/Editar.php */
?>
