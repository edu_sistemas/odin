<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2017-02-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Informe extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Informe';
    private $nombre_item_plural = 'Informe';
    private $package = 'back_office/Nota';
    private $model = 'Informe_model';
    private $view = 'Informe_v';
    private $controller = 'Informe';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        //  Libreria de sesion
        $this->load->library('session');
        //  $this->load->library('form_validation');
        //  $this->load->library('AddForms_lib', 'addforms_lib');
    }

    // 2016-10-11
    // Controlador del Agregar
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Nota/informe.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');



        $data['activo'] = "Informe de Notas";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
        //  cargo el menu en la session
    }

    //metodos



    function CargaTaba() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_notas($arr_input_data);
        echo json_encode($data);
    }

    function GenerarExcel() {

        $id_ficha = $this->uri->segment(5);

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_excel = $this->modelo->get_arr_alumnos_notas($arr_input_data); // Nombres y rut de los alumno
        $data_excel_id = $this->modelo->get_arr_alumnos_id_alumno($arr_input_data); // id de los alumnos

        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Hoja 1');
        //set cell A1 content with some text
        //change the font size
        $this->excel->getActiveSheet()->SetCellValue('A1', 'RUT');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'NOMBRE');

        $contador = count($data_excel);
        $letra = "C";

        $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2'); //

        for ($x = 0; $x < $contador; $x++) { // Este ciclo for cuenta los alumnos
            $id_detalle_alumno = $data_excel_id[$x]['id_detalle_alumno'];
            $arr_input_data_n = array(
                'id_alumno' => $id_detalle_alumno,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

            $data_excel_notas = $this->modelo->get_notas_by_id($arr_input_data_n);
            $contador_notas = count($data_excel_notas);

            for ($v = 0; $v < $contador_notas; $v++) { //Este ciclo for inserta las notas, y le pone titulo a la columna donde van las nota Nota x
                $this->excel->getActiveSheet()->SetCellValue($letra . '1', 'Nota ' . ($v + 1));
                $this->excel->getActiveSheet()->SetCellValue($letra . ($x + 2), $data_excel_notas[$v]['nota']);
                $letra ++;
            }
            $letra = 'C';
        }

        $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');
        //   $this->excel->getActiveSheet()->fromArray($data_excel);

        $this->excel->getActiveSheet()->getStyle('A1:Z1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '#afddb2')
                    )
                )
        );

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = 'Informe_Notas.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function calcularPomedio() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_arr_calcular_promedio($arr_input_data);
        echo json_encode($data);
    }

}
