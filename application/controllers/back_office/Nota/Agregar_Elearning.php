<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2017-02-10 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar_Elearning extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar_Elearning';
    private $nombre_item_plural = 'Agregar_Elearning';
    private $package = 'back_office/Nota';
    private $model = 'Nota_elearning_model';
    private $view = 'Agregar_elearning_v';
    private $controller = 'Agregar_Elearning';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        //  Libreria de sesion
        $this->load->library('session');
        //  $this->load->library('form_validation');
        //  $this->load->library('AddForms_lib', 'addforms_lib');
    }

    // 2016-10-11
    // Controlador del Agregar
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar_elearning_v',
                'label' => 'Calificaciones e-Learning',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Nota/agregar_elearning.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;




        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');



        $data['activo'] = "Registrar Nota e-Learning";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
        //  cargo el menu en la session
    }

    //metodos

    function cargaTabla() {

        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );

        $data['contenido'] = $this->modelo->get_arr_listar_alumnos($arr_sesion);
    }

    function getFichas() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_listar_ficha_reserva_cbx($arr_input_data);
        echo json_encode($data);
    }

    function getfichaData() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->get_arr_listar_alumnos($arr_input_data);
        echo json_encode($data);
    }

    function setRegistrarNotas() {

        $total = $this->input->post('total');       //Cantidad de notas 
        $columnas = $this->input->post('columnas'); //Cantidad de Columnas notas 
        $largo = $this->input->post('largo');       // cantidad de notas desde la base datos osea que están previamente insertadas.

        $arr = array();         //Notas nuevas
        $arr_dtla = array();    //id de los alumnos 
        $arr_notas = array();   //Notas que ya están ingresadas previamente


        if ($largo > 0) {

            for ($var = 0; $var < $total * $columnas; $var++) {

                array_push($arr_notas, $_POST['id_' . $var]);
                array_push($arr, $_POST[$var]);
            }
        } else {

            for ($var = 0; $var < $total * $columnas; $var++) {

                array_push($arr_notas, $_POST['id_' . $var]);
                array_push($arr, $_POST[$var]);
            }
        }

        for ($var = 0; $var < $total; $var++) {

            array_push($arr_dtla, $_POST['dtl' . $var]);
        }

        $arr_input_data = array(
            'Nota' => $arr,
            'dtl' => $arr_dtla,
            'notas_id' => $arr_notas,
            'total' => $total,
            'columnas' => $columnas,
            'largo' => $largo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_arr_registrar_notas($arr_input_data);
        echo json_encode($data);
    }

    function cerrarCurso() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_arr_cerrar_curso($arr_input_data);
        echo json_encode($data);
    }

    function calcularPomedio() {

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_arr_calcular_promedio($arr_input_data);
        echo json_encode($data);
    }

}
