<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-11 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-12-11 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Horas Relator';
    private $nombre_item_plural = 'Horas Relator';
    private $package = 'back_office/HorasRelator';
    private $model = 'HorasRelator_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );


        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/HorasRelator/Listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Calculo horas Relator";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['menus'] = $this->session->userdata('menu_usuario');

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listar() {
        $datos_buscar['Finicio'] = $this->input->post('finicio');
        $datos_buscar['Ftermino'] = $this->input->post('ftermino');
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil'],
            'finicio' => $datos_buscar['Finicio'],
            'Ftermino' => $datos_buscar['Ftermino']
        );
        $datos = $this->modelo->get_arr_listar_docente_by_fechas($arr_sesion);
        echo json_encode($datos);
    }

    function detalleRelator() {
        $datos_buscar['id_relator'] = $this->input->post('id_relator');
        $datos_buscar['Finicio'] = $this->input->post('finicio');
        $datos_buscar['Ftermino'] = $this->input->post('ftermino');
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil'],
            'id_relator' => $datos_buscar['id_relator'],
            'finicio' => $datos_buscar['Finicio'],
            'Ftermino' => $datos_buscar['Ftermino']
        );
        $datos = $this->modelo->get_arr_listar_docente_by_fechas_id($arr_sesion);
        echo json_encode($datos);
    }

    function detalleRelatorTotal() {
        $datos_buscar['id_relator'] = $this->input->post('id_relator');
        $datos_buscar['Finicio'] = $this->input->post('finicio');
        $datos_buscar['Ftermino'] = $this->input->post('ftermino');
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil'],
            'id_relator' => $datos_buscar['id_relator'],
            'finicio' => $datos_buscar['Finicio'],
            'Ftermino' => $datos_buscar['Ftermino']
        );
        $datos = $this->modelo->get_arr_listar_docente_by_fechas_id_total($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
