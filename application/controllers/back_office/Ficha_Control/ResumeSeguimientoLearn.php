<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ResumeSeguimientoLearn extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Ficha_Control';
    private $model = 'Ficha_Control_Learn_model';
    private $view = 'ResumenElearn_Seguimiento_v';
    private $controller = 'ResumeSeguimientoLearn';
    private $ind = '';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Consulta_learn',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Resumen',
                'label' => 'Resumen',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha_Control/ResumenSeguimientoLearn.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;
        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Seguimiento C.E.";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id_resumenFC($arr_input_data);
        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_ficha_idFC($arr_input_data);
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_ficha'] = $id_ficha;
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function cargaDatosOC() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_datos_orden_compraFC($arr_data);
        echo json_encode($datos);
    }

    function DescartarAlms() {


        $idficha = $this->input->post('id_fichaff');
        $total = $this->input->post('cantidad');
        $check = array();
        $id_alm = array();
        $motivo = array();

        for ($x = 0; $x < $total; $x++) {

            if (isset($_POST['chkAst' . $x])) {

                array_push($check, "5");
            } else {

                array_push($check, "1");
            }
            //	array_push($check, $this->input->post('chkAst' . $x));
            array_push($id_alm, $this->input->post('a' . $x));
            array_push($motivo, $this->input->post('area' . $x));
        }
        //  variables de sesion
        $arr_data = array(
            'ftc' => $idficha,
            'cantidad' => $total,
            'estado' => $check,
            'id_alumno' => $id_alm,
            'motivo' => $motivo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_arr_descartar_alm($arr_data);
        echo json_encode($datos);
    }

    function enviaEmail() {

        $id_ficha = $this->input->post('id_fichaff');

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_correo = $this->modelo->get_correo_descarte($arr_input_data);

        if (count($data_correo) == 0) { //Para preguntar si el array viene vacio, pasa al else y genera otra consulta, sin alumnos descartados
            $data_correo = $this->modelo->get_correo__sin_descarte($arr_input_data);
        }

        $asunto = "";
        $num_ficha = $data_correo[0]['num_ficha'];

        $alumnos = ($data_correo[0]['cantidad_descartados']);

        $cantidad_descartados_no = ($data_correo[0]['cantidad_descartados_no']);


        if ($alumnos > 0) {
            $asunto = "Confirmación - Ficha N°: " . $num_ficha; // asunto del correo
        }

        if ($alumnos == 0) {
            $asunto = "Confirmación - Ficha N°: " . $num_ficha; // asunto del correo
        }

        if ($cantidad_descartados_no == 0) {
            $asunto = "Confirmación - Ficha N°: " . $num_ficha; // asunto del correo
        }
 

        $destinatarios = array(
            array('email' => $data_correo[0]['correo_comercial'], 'nombre' => $data_correo[0]['nombre_ejecutivo']),
            array('email' => $data_correo[0]['creador_ficha'], 'nombre' => $data_correo[0]['creador_nombre']),
            array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
            array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
            array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
            array('email' => 'fulecia@edutecno.com', 'nombre' => 'Felipe Andres Ulecia Quiroz'),
            array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
        );

        $body = $this->generaHtmlCorreo($data_correo);

        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );
        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo json_encode($data_correo);
    }

    function generaHtmlCorreo($data_correo) {

        $alumnos = ($data_correo[0]['cantidad_descartados']);
        $total = $this->input->post('cantidad');

        $comenatrios = $this->input->post('comentarios_email');

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba Venta Curso Presencial</title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';

        if ($alumnos != 0) {
            $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_rojo.jpg"/>';
        } else {
            $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        }

        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado(a): ';
        $html .= $data_correo[0]['nombre_ejecutivo'];
        $html .= '<br>';
        $html .= '<br>';

        if ($alumnos != 0) {
            $html .= 'Se han Descartado los siguientes participantes.';
        } else {
            $html .= 'Curso ha sido cargado a ofimatica';
        }

        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr class="total">';
        $html .= '<td>N° de Ficha</td>';
        $html .= '<td class="alignright"> ' . $data_correo[0]['num_ficha'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="">';
        $html .= '<td>N° de Empresa</td>';
        $html .= '<td class=""> ' . $data_correo[0]['razon_social_empresa'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="">';
        $html .= '<td>Cantidad de alumnos</td>';
        $html .= '<td class="alignright"> ' . $data_correo[0]['cantidad_descartados_no'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="">';
        $html .= '<td>Curso</td>';
        $html .= '<td class="alignright"> ' . $data_correo[0]['nombre_curso'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright"> ' . $data_correo[0]['nombre_modalidad'] . ' </td>';
        $html .= '</tr>';
        if ($alumnos != 0) {
            for ($i = 0; $i < $alumnos; $i++) {
                $html .= '<tr>
					<tr>	
						<td style="width: 100%;" >Rut: ' . $data_correo[$i]['rut'] . '</td> 
					</tr>	 
					<tr>	
						<td style="width: 100%;" >Nombre: ' . $data_correo[$i]['nombre'] . '</td> 
					</tr>
					<tr>	
						<td style="width: 100%; border-bottom: 2px solid #333;" >Motivo: ' . $data_correo[$i]['comentario'] . '</td> 
					</tr>
				 </tr>';
            }
        } else {
            
        }
        $html .= '<tr  class="">';
        $html .= '<td>Total de descartados </td>';
        $html .= '<td class="alignright">' . $data_correo[0]['cantidad_descartados'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>Fecha inicio </td>';
        $html .= '<td class="alignright">' . $data_correo[0]['fecha_inicio'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<tr  class="">';
        $html .= '<td>Fecha fin</td>';
        $html .= '<td class="alignright">' . $data_correo[0]['fecha_fin'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';

        $html .= '<td>Comentarios / Observaciones: </td>';
        $html .= '</tr>';
        $html .= '<tr  class="">';
        $html .= '<td  class="total">' . $comenatrios . '</td>';
        $html .= '<tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com/back_office/Ficha_Control/ResumenSeguimiento/index/' . $data_correo[0]['id_ficha'] . '" target="_blank" class="btn-primary">Ver en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    function generarExcel($id) {
        $id_ficha = $id;

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Hoja 1');
        //set cell A1 content with some text
        //change the font size
        $data_excel = $this->modelo->get_arr_excel_generate($arr_input_data);

        $this->excel->getActiveSheet()->SetCellValue('A1', 'MODALIDAD');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'ID APLICACION');
        $this->excel->getActiveSheet()->SetCellValue('C1', 'RUT');
        $this->excel->getActiveSheet()->SetCellValue('D1', 'ID EMPRESA');
        $this->excel->getActiveSheet()->SetCellValue('E1', 'NOMBRE USUARIO MOODLE');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'NOMBRE CORTO MOODLE');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'N° DE FICHA');
        $this->excel->getActiveSheet()->SetCellValue('H1', 'AÑO PROCESO');
        $this->excel->getActiveSheet()->SetCellValue('I1', 'FECHA INICIO');
        $this->excel->getActiveSheet()->SetCellValue('J1', 'FECHA FIN');
        $this->excel->getActiveSheet()->SetCellValue('K1', 'FECHA CIERRE');
        $this->excel->getActiveSheet()->SetCellValue('L1', 'CENTRO COSTO');
        $this->excel->getActiveSheet()->SetCellValue('M1', 'VALOR');
        $this->excel->getActiveSheet()->SetCellValue('N1', 'ORDEN COMPRA');

        $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');
        //   $this->excel->getActiveSheet()->fromArray($data_excel);

        $this->excel->getActiveSheet()->getStyle('A1:N1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '#afddb2')
                    )
                )
        );

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = 'planilla_cursos.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    function generarExcelAlumnos($id) {
        $id_ficha = $id;

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Hoja 1');
        //set cell A1 content with some text
//        $this->excel->getActiveSheet()->setCellValue('A1', ' . $data_excel[0]["idModalidad"].');
        //change the font size
        $data_excel = $this->modelo->get_arr_excel_generate_alumnos($arr_input_data);

        $this->excel->getActiveSheet()->SetCellValue('A1', 'Rut');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'Dv');
        $this->excel->getActiveSheet()->SetCellValue('C1', 'Nombre');
        $this->excel->getActiveSheet()->SetCellValue('D1', 'Apellido');
        $this->excel->getActiveSheet()->SetCellValue('E1', 'Fono 1');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'Fono 2');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'Correo 1');
        $this->excel->getActiveSheet()->SetCellValue('H1', 'Correo 2');
        $this->excel->getActiveSheet()->SetCellValue('I1', 'Usuario');
        $this->excel->getActiveSheet()->SetCellValue('J1', 'Clave');
        $this->excel->getActiveSheet()->SetCellValue('K1', 'Tipo Registro');
        $this->excel->getActiveSheet()->SetCellValue('L1', 'Id Empresa');


        $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');
        //   $this->excel->getActiveSheet()->fromArray($data_excel);

        $this->excel->getActiveSheet()->getStyle('A1:N1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '#afddb2')
                    )
                )
        );

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = 'planilla_alumnos.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

    /* RECTIFICACIONES */

    function getRectificacionesAlumnosbyFicha() {

        $id_ficha = $this->input->post('ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_by_id_fichaFC($arr_data);
        echo json_encode($datos);
    }

    function getRectificacionesAlumnosDocbyFicha() {

        $id_ficha = $this->input->post('ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_doc_by_id_fichaFC($arr_data);
        echo json_encode($datos);
    }

    function getRectificacionesExtensionbyFicha() {

        $id_ficha = $this->input->post('ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_extension_by_id_fichaFC($arr_data);
        echo json_encode($datos);
    }

    function getRectificacionesExtensionDocbyFicha() {

        $id_ficha = $this->input->post('ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_extension_doc_by_id_fichaFC($arr_data);
        echo json_encode($datos);
    }

    function ConfirmaExtension() {

        $id_ficha = $this->input->post('ficha');
        $hito = $this->input->post('hito');
        $id_estado_edutecno = $this->input->post('id_estado');
        $observacion = $this->input->post('comentario');

        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'hito' => $hito,
            'id_estado_edutecno' => $id_estado_edutecno,
            'observacion' => $observacion,
            'responsable' => 'Cristian Espinoza'
        );
//		
//		var_dump($arr_data);
//		die();
        $datos = $this->modelo->insert_log_ficha_c_e($arr_data);
        echo json_encode($datos);
    }

    function ConfirmaAlumnos() {

        $id_ficha = $this->input->post('ficha');
        $hito = $this->input->post('hitoa');
        $id_estado_edutecno = $this->input->post('id_estadoa');
        $observacion = $this->input->post('comentarioa');

        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'hito' => $hito,
            'id_estado_edutecno' => $id_estado_edutecno,
            'observacion' => $observacion,
            'responsable' => 'Cristian Espinoza'
        );
//		
//		var_dump($arr_data);
//		die();
        $datos = $this->modelo->insert_log_ficha_alum_c_e($arr_data);
        echo json_encode($datos);
    }

}

/* End of fil Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
