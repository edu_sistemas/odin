<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-08 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-08 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ResumenRectificacionExtension extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Ficha_Control';
    private $model = 'Ficha_Control_model';
    private $view = 'ResumenRectificacionExtension_v';
    private $controller = 'Editar';
    private $ind = '';
    private $ficha = 'back_office/Ficha/Ficha_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->ficha, 'modeloficha');


        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_rectificacion = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_rectificacion == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/ResumenRectificacionExtension',
                'label' => 'Resumen  Extensión de plazo',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha_Control/ResumenExtensionPlazo.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Ficha Control";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        $arr_input_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        $data['data_ficha'] = $this->modeloficha->get_arr_ficha_by_id_rectificacion($arr_input_data);
        $data['data_orden_compra'] = $this->modeloficha->get_arr_orden_compra_by_rectificacion_ficha_id($arr_input_data);
        $data['data_rectificacion'] = $this->modelo->get_estado_resctificacionesFC($arr_input_data);


        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function cargaDatosOCRectificacion() {

        $id_rectificacion = $this->input->post('id_rectificacion');
        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeloficha->get_arr_listar_datos_rectificacion_orden_compra_by_rectificacion($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumenRectificaciones() {

        $id_rectificacion = $this->input->post('id_rectificacion');
        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modeloficha->get_arr_listar_datos_documentos_rectificacion_resumen_orden_compra_by_rectificacion($arr_data);
        echo json_encode($datos);
    }

    function aprobarRectificacion() {

        $id_rectificacion = $this->input->post('id_rectificacion');

        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_aprobar_resctificacionesFC($arr_data);

        echo json_encode($datos);
    }

    function rechazarRectificacion() {
        $id_rectificacion = $this->input->post('id_rectificacion_rechazo');
        $motivo_rechazo = $this->input->post('motivo_rechazo');
        //  variables de sesion
        $arr_data = array(
            'id_rectificacion' => $id_rectificacion,
            'motivo_rechazo' => $motivo_rechazo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->set_rechazar_resctificacionesFC($arr_data);
        echo json_encode($datos);
    }

    function enviaEmail() {

        $datos = $this->input->post('datos');

        $modalidad = $datos[0]['modalidad'];
        $email_ejecutivo = $datos[0]['email_ejecutivo'];
        $ejecutivo = $datos[0]['ejecutivo'];
        $usuario_ingreso_correo = $datos[0]['email_usuario_ingreso'];
        $usuario_ingreso_nombre = $datos[0]['usuario_ingreso'];

        switch ($modalidad) {
            case 'elearning':
                if ($email_ejecutivo == $usuario_ingreso_correo) {
                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo),
                        array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                        array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                        array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                } else {
                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo),
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                        array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                        array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'lfarias@edutecno.com', 'nombre' => 'Luis Farias'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                }
                break;
            case 'presencial':
                if ($email_ejecutivo == $usuario_ingreso_correo) {
                    $destinatarios = array(
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                        array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                        array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                        array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                        array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                } else {
                    $destinatarios = array(
                        array('email' => $email_ejecutivo, 'nombre' => $ejecutivo),
                        array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre),
                        array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                        array('email' => 'psalazar@edutecno.com', 'nombre' => 'Paola'),
                        array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
                        array('email' => 'mfabres@edutecno.com', 'nombre' => 'Marianela Fabres'),
                        array('email' => 'valarcon@edutecno.com', 'nombre' => 'Viviam Alarcón'),
                        array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                        array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez'),
                        array('email' => 'iperez@edutecno.com', 'nombre' => 'Ignacio Pérez'),
                        array('email' => 'tatianaw@edutecno.com', 'nombre' => 'Tatiana Watkins')
                    );
                }
                // presenciales 
                // con copia a 
                break;
        }

        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );


        $asunto = "Nueva Rectificación - Ficha N°: " . $datos[0]['num_ficha'];  // asunto del correo
        // presenciales
        $body = $this->generaHtmlAprobarRectificacion($datos[0]); // mensaje completo HTML o text

        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo json_encode($datos[0]);
    }

    function generaHtmlAprobarRectificacion($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_verde.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as) ';
        $html .= '<br>';
        $html .= 'Se ha realizado la Rectificación solicitada para la Ficha N° ' . $data['num_ficha'];
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['razon_social_empresa'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td  >Tipo Rectificación</td>';
        $html .= '<td class="alignright"> ' . $data['tipo_rectificacion'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>Orden Compra</td>';
        $html .= '<td class="alignright"> ' . $data['num_orden_compra'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr >';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright"> ' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Solicitado por</td>';
        $html .= '<td class="alignright"> ' . $data['usuario_ingreso'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td >Curso</td>';
        $html .= '<td class="alignright"> ' . $data['curso'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Modalidad</td>';
        $html .= '<td class="alignright">' . $data['modalidad'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Motivo</td>';
        $html .= '<td class="alignright">' . $data['motivo_rectificacion'] . '</td>';
        $html .= '</tr> ';
        $html .= '<tr class="total">';
        $html .= '<td>Nueva fecha de término</td>';
        $html .= '<td class="alignright">' . $data['fecha_extension'] . '</td>';
        $html .= '</tr> ';

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfanos 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';


        return $html;
    }

    function enviaEmailRechazoRectificacion() {


        $datos = $this->input->post('datos');



        $destinatarios = array(
            array('email' => $datos[0]['correo_comercial'], 'nombre' => $datos[0]['nombre_ejecutivo']),
            array('email' => $datos[0]['creador_ficha'], 'nombre' => $datos[0]['creador_nombre'])
        );


        $cc = array(
            array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
        );

        $asunto = "Rectificación - Ficha N°: " . $datos[0]['num_ficha']; // asunto del correo
        // presenciales
        $body = $this->generaHtmlRechazo($datos[0]); // mensaje completo HTML o text

        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

//		Lineas de codigo para probar el correo y los destinatarios	
//		print_r($destinatarios);
//		var_dump($body);
//		die();
        echo json_encode($datos[0]);
    }

    function generaHtmlRechazo($data) {


        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_rojo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)  ';
        $html .= '<br>';
        $html .= 'Se ha rechazado su solicitud de rectificación para la Ficha N°: ' . $data['num_ficha'];
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright">' . $data['ejecutivo'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr>';
        $html .= '<td>ID Solicitud</td>';
        $html .= '<td class="alignright"> ' . $data['id_ficha'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr  class="total">';
        $html .= '<td>N° Ficha</td>';
        $html .= '<td class="alignright"> ' . $data['num_ficha'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data['empresa'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data['fecha_fin'] . '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Sala</td>';
        $html .= '<td class="alignright"> ' . $data['nombre'] . ' </td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>Participantes</td>';
        $html .= '<td class="alignright"> ' . $data['cantidad'] . ' </td>';
        $html .= '</tr>';


        $html .= '<tr  class="total">';
        $html .= '<td>Motivo de Rechazo de rectificación C. C. </td>';
        $html .= '<td class="alignright"> ' . $data['motivo_rectificacion_rechazo'] . '</td>';


        $html .= '</tr>';

        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huérfano  863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
