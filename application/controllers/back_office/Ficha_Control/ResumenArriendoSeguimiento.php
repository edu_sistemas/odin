<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2017-01-26 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ResumenArriendoSeguimiento extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Ficha_Control';
    private $model = 'Ficha_Control_model';
    private $view = 'ResumenArriendoSeguimiento_v';
    private $controller = 'Editar';
    private $ind = '';
    private $modalidad = 'back_office/Modalidad/Modalidad_model';
    private $curso = 'back_office/Curso/Curso_model';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $otic = 'back_office/Otic/Otic_model';
    private $sence = 'back_office/Code_sence/Code_sence_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $sede = 'back_office/Sede/Sede_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $sala = 'back_office/Sala/Sala_model';
    private $ficha = 'back_office/Ficha/Ficha_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->modalidad, 'modelmodalidad');
        $this->load->model($this->curso, 'modelcurso');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->otic, 'modelotic');
        $this->load->model($this->sence, 'modelosence');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->sede, 'modelosede');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->sala, 'modelsala');
        $this->load->model($this->ficha, 'modelficha');


        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Resumen Arriendo',
                'label' => 'Resumen Arriendo',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert2.min.css'),
            // array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            //  array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha_Control/ResumenArriendoSeguimiento.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Seguimiento";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }


        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_ficha'] = $this->modelficha->get_arr_ficha_by_id_resumen($arr_input_data);

        $data['data_orden_compra'] = $this->modelficha->get_arr_orden_compra_by_ficha_id($arr_input_data);

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
		$data['id_ficha'] = $id_ficha;
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function cargaDatosOC() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelficha->get_arr_listar_datos_orden_compra($arr_data);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumen() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelficha->get_arr_listar_datos_documentos_resumen_orden_compra($arr_data);
        echo json_encode($datos);
    }
    function getAdicionalesFicha(){
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
    
        $datos = $this->modelo->get_adicionales_ficha_by_id($arr_sesion);
        echo json_encode($datos);
    }
    

    function rechazarFicha() {
        $id_ficha = $this->input->post('id_ficha_rechazo');
        $motivo_rechazo = $this->input->post('motivo_rechazo');

        $arr_datos = array(
            'id_ficha_rechazo' => $id_ficha,
            'motivo_rechazo' => $motivo_rechazo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $respuesta = $this->modelo->set_rechazar_fichaFC($arr_datos);

        $ficha = $respuesta[0]['id_ficha'];
        $ejecutivo = $respuesta[0]['ejecutivo'];
        $email_ejecutivo = $respuesta[0]['email_ejecutivo'];
        $usuario_ingreso_nombre = $respuesta[0]['usuario_ingreso_nombre'];
        $usuario_ingreso_correo = $respuesta[0]['usuario_ingreso_correo'];

        //$modalidad = $respuesta[0]['modalidad'];


        $reset = $respuesta[0]['reset'];

        if ($reset == 1) {

            // destinos
            $destinatarios = array(
                //array('email' => $email_ejecutivo, 'nombre' => $ejecutivo),
                array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre)
            );

            $asunto = "Ficha N°: " . $respuesta[0]['num_ficha']; // asunto del correo
            // presenciales
            $body = $this->generaHtmlRechazo($respuesta); // mensaje completo HTML o text
            // con copia a
            $cc = array(
                array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
               
            );

            $AltBody = $asunto; // hover del cursor sobre el correo


            /*             * ** inicio array ** */
            $data_email = array(
                // indica a quienes esta dirigido el  email
                "destinatarios" => $destinatarios,
                "cc_a" => $cc, // asunto del correo
                "asunto" => $asunto,
                // contenido del correo , puede ser html o solo texto 
                "body" => $body,
                // AltBody (esto no sirve para para pero lo requiere la libreria XD)
                "AltBody" => $AltBody
            );
            /*             * ** Fin array ** */

            $this->load->library('Libreriaemail');
            $res = $this->libreriaemail->CallAPISendMail($data_email);
        }

        echo json_encode($respuesta);
    }

    function aprobarFicha() {
        $id_ficha = $this->input->post('id_ficha');
        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->set_aprobar_fichaFC($arr_datos);

        $ficha = $respuesta[0]['id_ficha'];
        $ejecutivo = $respuesta[0]['ejecutivo'];
        $email_ejecutivo = $respuesta[0]['email_ejecutivo'];
        $usuario_ingreso_nombre = $respuesta[0]['usuario_ingreso_nombre'];
        $usuario_ingreso_correo = $respuesta[0]['usuario_ingreso_correo'];

        //$modalidad = $respuesta[0]['modalidad'];


        $reset = $respuesta[0]['reset'];

        if ($reset == 1) {

            // destinos
            $destinatarios = array(
                array('email' => $email_ejecutivo, 'nombre' => $ejecutivo),
                array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre)
            );

            // con copia a
            $cc = array(
                array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
                array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez')
            );

            $asunto = "Ficha N°: " . $ficha; // asunto del correo            
            // presenciales
            $body = $this->generaHtml($respuesta); // mensaje completo HTML o text
            $AltBody = $asunto;  // hover del cursor sobre el correo           

            /*             * ** inicio array ** */
            $data_email = array(
                // indica a quienes esta dirigido el  email
                "destinatarios" => $destinatarios,
                "cc_a" => $cc, // asunto del correo
                "asunto" => $asunto,
                // contenido del correo , puede ser html o solo texto 
                "body" => $body,
                // AltBody (esto no sirve para para pero lo requiere la libreria XD)
                "AltBody" => $AltBody
            );
            /*             * ** Fin array ** */

            $this->load->library('Libreriaemail');
            $res = $this->libreriaemail->CallAPISendMail($data_email);
        }
        echo json_encode($respuesta);
    }

    function generaHtml($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado(a)  ' . $data[0]['usuario_ingreso_nombre'];
        $html .= '<br>';
        $html .= 'Se ha aprobado su solicitud de venta (Arriendo de Sala).  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright">' . $data[0]['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>ID Ficha</td>';
        $html .= '<td class="alignright"> ' . $data[0]['id_ficha'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data[0]['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data[0]['fecha_inicio'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data[0]['fecha_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Sede</td>';
        $html .= '<td class="alignright"> ' . $data[0]['nombre_sede'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Sala</td>';
        $html .= '<td class="alignright"> ' . $data[0]['sala'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Participantes</td>';
        $html .= '<td class="alignright"> ' . $data[0]['cant_participante'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Total Bruto</td>';
        $html .= '<td class="alignright"> ' . $data[0]['total_bruto'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>Observaciones </td>';
        $html .= '<td class="alignright"> ' . $data[0]['observaciones'] . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }

    function generaHtmlRechazo($data) {

        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Aprueba venta curso E-learning </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado(a)  ' . $data[0]['usuario_ingreso_nombre'];
        $html .= '<br>';
        $html .= 'Se ha rechazado su solicitud de venta (Arriendo de Sala).  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>E. Comercial</td>';
        $html .= '<td class="alignright">' . $data[0]['ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>ID Ficha</td>';
        $html .= '<td class="alignright"> ' . $data[0]['id_ficha'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa</td>';
        $html .= '<td class="alignright">' . $data[0]['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Fecha Inicio </td>';
        $html .= '<td class="alignright">' . $data[0]['fecha_inicio'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Fecha Termino</td>';
        $html .= '<td class="alignright">' . $data[0]['fecha_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Sede</td>';
        $html .= '<td class="alignright"> ' . $data[0]['nombre_sede'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Sala</td>';
        $html .= '<td class="alignright"> ' . $data[0]['sala'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Participantes</td>';
        $html .= '<td class="alignright"> ' . $data[0]['cant_participante'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Total Bruto</td>';
        $html .= '<td class="alignright"> ' . $data[0]['total_bruto'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  class="total">';
        $html .= '<td>Motivo de Rechazo</td>';
        $html .= '<td class="alignright"> ' . $data[0]['motivo_rechazo'] . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';

        return $html;
    }
	function pdfResumen($id) {
        $id_ficha = $id;

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_ficha = $this->modelo->get_arr_ficha_by_id_resumenFC($arr_input_data);

        $data_orden_compra = $this->modelo->get_arr_orden_compra_by_ficha_idFC($arr_input_data);

		$arr_dias = explode(',',$data_ficha[0]['id_dias']);
		//var_dump($data_ficha);
		$dias ='';
		for($y=0;$y<count($arr_dias);$y++){			
			if($y!=0){
			$dias .='-';
			}
			switch($arr_dias[$y]){
				case '1':
					$dias .= 'L';
				break;
				case '2':
					$dias .= 'Ma';
				break;
				case '3':
					$dias .= 'Mi';
				break;
				case '4':
					$dias .= 'J';
				break;
				case '5':
					$dias .= 'V';
				break;
				case '6':
					$dias .= 'S';
				break;
				case '7':
					$dias .= 'D';
				break;
			
			}
			if($dias==''){
				$dias='No Disponible';
			}
		}
		if($data_ficha[0]['break']==NULL){
			$data_ficha[0]['break'] = 'No Disponible';
		}
		
        $this->load->library('Pdf');
        $pdf = new pdf('L', 'mm', 'LETTER');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins(15, 15, 15, true);

        // set auto page breaks false
        $pdf->SetAutoPageBreak(false, 0);

        $pdf->AddPage('P', 'A4');
        $pdf->SetFont('times', '', 11);
        $html = '<style>
					td{
					padding-left: 3px;
					height:16px;
					}
				</style>';
        $html .= '<table border="0">
					<tr>
						<td style="font-size:15px;text-align:center"><u>ORDEN DE VENTA ARRIENDO</u></td>
						
					</tr>
				 </table>';
		$html .= '<table><tr><td>&nbsp;</td></tr></table>
				  <table><tr><td>Datos de la Venta</td></tr></table>
				  <table border="0">
				  	<tr>
						<td><strong>N° FICHA:</strong></td>
						<td><table border="1"><tr><td>&nbsp;'. $data_ficha[0]['num_ficha'] .'</td></tr></table></td>
						<td>&nbsp;<strong>TIPO VENTA:</strong></td>
						<td><table border="1"><tr><td>&nbsp;'. $data_ficha[0]['categoria'] .'</td></tr></table></td>
						<td>&nbsp;<strong>ESTADO:</strong></td>
						<td><table border="1"><tr><td>&nbsp;'. $data_ficha[0]['estado'] .'</td></tr></table></td>
					</tr>					
				  </table>';
        $html .= '<table><tr><td>&nbsp;</td></tr></table>
				  <table><tr><td>Datos Empresa</td></tr></table>
				  <table border="0">
				  	<tr>
						<td><strong>HOLDING</strong></td>
						<td colspan="5"><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['holding'] . '</td></tr></table></td>
					</tr>
					<tr>
						<td><strong>EMPRESA</strong></td>
						<td colspan="2"><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['empresa'] . '</td></tr></table></td>
						<td>&nbsp;<strong>RUT</strong></td>
						<td colspan="2"><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['rut_empresa'] . '</td></tr></table></td>
					</tr>
					<tr>
						<td><strong>DIRECCION</strong></td>
						<td colspan="5"><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['direccion_empresa'] . '</td></tr></table></td>
						
					</tr>					
				  </table>'; 
		$html .='<table><tr><td>&nbsp;</td></tr></table>
					<table><tr><td>Valores</td></tr></table>
					<table>
						<tr>
							<td><strong>N° DE DIAS:</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['n_dias'] . '</td></tr></table></td>
							<td><strong>BREAK</strong>:</td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['break'] . '</td></tr></table></td>
							<td><strong>VALOR VENTA:</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['total_venta'] . '</td></tr></table></td>
						</tr>
						<tr>
							<td><strong>VALOR POR DIA:</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['valor_dia'] . '</td></tr></table></td>
							<td><strong>N° DE ALUMNOS:</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['n_alumnos'] . '</td></tr></table></td>
							<td><strong>IVA:</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['total_iva'] . '</td></tr></table></td>
						</tr>
						<tr>
							<td><strong>TOTAL DIAS:</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['total_dia'] . '</td></tr></table></td>
							<td><strong>VALOR BREAK P/ ALUMNO</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['valor_break'] . '</td></tr></table></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td><strong>TOTAL EN BREAK:</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['total_en_break'] . '</td></tr></table></td>
							<td><strong>TOTAL:</strong></td>
							<td>&nbsp;<table border="1"><tr><td>&nbsp;' . $data_ficha[0]['total_bruto'] . '</td></tr></table></td>
						</tr>
					</table>';      

        
        $html .= '<table><tr><td>&nbsp;</td></tr></table>
				  <table><tr><td>Datos Lugar de Ejecucion</td></tr></table>
				  <table border="0">
				  	<tr>
						<td><strong>SEDE</strong></td>
						<td colspan="3"><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['sede'] . '</td></tr></table></td>
						<td>&nbsp;<strong>SALA</strong></td>
						<td><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['sala'] . '</td></tr></table></td>
					</tr>
					<tr>
						<td><strong>DIAS</strong></td>
						<td colspan="1"><table border="1"><tr><td>&nbsp;' . $dias . '</td></tr></table></td>
						<td>&nbsp;<strong>LUGAR EJECUCION</strong></td>
						<td colspan="3"><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['sala'] . '</td></tr></table></td>
						
					</tr>					
				  </table>';
        $html .= '<table><tr><td>&nbsp;</td></tr></table>
					<table><tr><td>Datos Ejecutivo</td></tr></table>
				  <table border="0">
				  	<tr>
						<td><strong>EJECUTIVO COMERCIAL</strong></td>
						<td colspan="5"><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['ejecutivo'] . '</td></tr></table></td>
						
					</tr>
					<tr>
						<td><strong>OBSERVACIONES</strong></td>
						<td colspan="5"><table border="1"><tr><td>&nbsp;' . $data_ficha[0]['comentario_orden_compra'] . '</td></tr></table></td>
					</tr>					
				  </table>';
        
        $pdf->writeHTML($html, true, false, true, false, '');

        //Close and output PDF document
        $pdf->Output('Resumen.pdf', 'I');
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
