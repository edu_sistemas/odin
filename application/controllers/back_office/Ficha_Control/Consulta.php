<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:  2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Consulta extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Ficha_Control';
    private $model = 'Ficha_Control_model';
    private $view = 'Consulta_v';
    private $controller = 'Consulta';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $ind = '';
    private $package2 = 'back_office/Ficha_documento';
    private $model2 = 'Ficha_documento_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $ficha = 'back_office/Ficha/Ficha_model';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'documento');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->ficha, 'modeloficha');
//  Libreria de sesion

        $this->load->library('session');
        $this->load->library('Functions');
    }

// 2016-05-25
// Controlador del panel de control
    public function index() {

///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Consulta',
                'label' => 'Seguimiento',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/bootstrap-datepicker/bootstrap-datepicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/fileupload/jquery.fileupload.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/vendor/jquery.ui.widget.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.iframe-transport.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.fileupload.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha_Control/consulta.js')
        );

// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Seguimiento";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function consultarFicha() {

        $funciones = new Functions();

        $start = $funciones->formatDateBD($this->input->post('start'));
        $end = $funciones->formatDateBD($this->input->post('end'));
        $estado = $this->input->post('estado');
        $empresa = $this->input->post('empresa');
        $holding = $this->input->post('holding');
        $ejecutivo = $this->input->post('ejecutivo');
        $periodo = $this->input->post('periodo');
        $num_ficha = $this->input->post('num_ficha');

        $arr_data = array(
            'start' => $start,
            'end' => $end,
            'estado' => $estado,
            'empresa' => $empresa,
            'holding' => $holding,
            'ejecutivo' => $ejecutivo,
            'periodo' => $periodo,
            'ficha' => trim($num_ficha),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_consultar_fichaFC($arr_data);
        echo json_encode($datos);
    }

    function verDocumentosFicha() {


        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->documento->get_arr_ficha_docs($arr_datos);

        echo json_encode($data);
    }

    function guardarDocumentosFicha() {
        
        $id_empresa = $this->input->post('txt_id_empresa');
        $nombre_contacto = $this->input->post('txt_nombre_contacto');
        $correo_contacto = $this->input->post('txt_correo_contacto');
        $telefono_contacto = $this->input->post('txt_telefono');


        $arr_datos = array(
            'id_empresa' => $id_empresa,
            'nombre_contacto' => $nombre_contacto,
            'correo_contacto' => $correo_contacto,
            'telefono_contacto' => $telefono_contacto,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->documento->set_arr_ficha_doc($arr_datos);

        echo json_encode($data);
    }

    function rechazarFicha() {


        $id_ficha = $this->input->post('id_ficha');
        $motivo_rechazo = $this->input->post('motivo_rechazo');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'motivo_rechazo' => $motivo_rechazo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_rechazar_ficha($arr_datos);

        echo json_encode($data);
    }

    function aprobarFicha() {


        $id_ficha = $this->input->post('id_ficha');

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelo->set_aprobar_ficha($arr_datos);

        echo json_encode($data);
    }

    function upload_file() {

        $id_ficha = $_POST['id_ficha'];

        $name = $_FILES['fileupload']['name'];
        $tname = $_FILES['fileupload']['tmp_name'];

        $nombre_archivo = "";
        $estado = "";
        $mensaje = "";

// verifica si el archivo se puede subir
        if (is_uploaded_file($tname[0])) {

            $splitted = explode(".", $name[0]);
            $reversed = array_reverse($splitted);
            $extension = $reversed[0];

//  borra posicion de un array
            unset($reversed[0]);

            $dereched = array_reverse($reversed);
// une string separado
            $nombre_archivo = implode("", $dereched);
            $nombre_con_fecha = $nombre_archivo . date('dmYHis') . '.' . $extension;

            $dir_subida = 'fichadoc/';
            $fichero_subido = $dir_subida . basename($nombre_con_fecha);
//copy =  copia el archivo de la ruta temporal a la ruta del servidor
            if (copy($tname[0], $fichero_subido)) {

                $arr_input_data = array(
                    'id_ficha' => $id_ficha,
                    'nombre_documento' => $nombre_con_fecha,
                    'ruta_documento' => $fichero_subido,
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );

                $result = $this->modelo->set_document_ficha($arr_input_data);

                $id_ficha = $result[0]['id_ficha'];
                $nombre_archivo = $nombre_con_fecha;
                $estado = "ok";
                $mensaje = "Archivo Subido";
            } else {
                $nombre_archivo = $nombre_con_fecha;
                $estado = "Error";
                $mensaje = "Error al Subir Archivo" . $nombre_archivo;
            }
        } else {
            $nombre_archivo = $name;
            $estado = "Error";
            $mensaje = "Error el archivo que intenta subir no es compatible con el sistema" . $nombre_archivo;
        }
        $arr_datos = array('archivo' => $nombre_archivo, 'status' => $estado, 'mensaje' => $mensaje, 'Ficha' => $id_ficha);
        echo json_encode($arr_datos);
    }

    function getFileAdjunto() {

        $arr_input_data['id_ficha'] = $this->input->post('id_ficha');
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
        $respuesta = $this->modelo->get_document_ficha($arr_input_data);

        echo json_encode($respuesta);
    }

    function getEstadoCBX() {
// carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeloficha->get_arr_listar_estado_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaCBX() {
// carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaByHoldingCBX() {
        // carga el combo box Holding

        $arr_input_data['id_holding'] = $this->input->post('id_holding');
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
        $datos = $this->modelempresa->get_arr_listar_empresa_by_holding_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEjecutivo() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        echo json_encode($datos);
    }

    function getPeriodo() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modeloficha->get_arr_listar_periodo_venta($arr_data);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
