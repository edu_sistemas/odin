<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ResumenSeguimiento extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Ficha_Control';
    private $model = 'Ficha_Control_model';
    private $view = 'ResumenSeguimiento_v';
    private $controller = 'ResumenSeguimiento';
    private $ind = '';
    private $edutecno = 'back_office/Edutecno/Edutecno_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->edutecno, 'modeledutecno');



        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index($id_ficha = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_ficha == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Resumen',
                'label' => 'Resumen',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );


        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert2.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Ficha_Control/ResumenSeguimiento.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Seguimiento";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }


        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_ficha'] = $this->modelo->get_arr_ficha_by_id_resumenFC($arr_input_data);

        $data['data_orden_compra'] = $this->modelo->get_arr_orden_compra_by_ficha_idFC($arr_input_data);


        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['id_ficha'] = $id_ficha;
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    /* Datos actuales */

    function cargaDatosOC() {
        $id_ficha = $this->input->post('ficha');
        $orden_compra = $this->input->post('oc');

        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($orden_compra),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_datos_orden_compraFC($arr_data);
        echo json_encode($datos);
    }

    function getAdicionalesFicha(){
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
    
        $datos = $this->modelo->get_adicionales_ficha_by_id($arr_sesion);
        echo json_encode($datos);
    }

    function cargaDatosOCDocumentoResumen() {

        $id_ficha = $this->input->post('ficha');


        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_datos_documentos_actualesFC($arr_data);
        echo json_encode($datos);
    }

    function cargaLastDocumentoResumen() {

        $id_ficha = $this->input->post('ficha');


        //  variables de sesion
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_last_documentos_actualesFC($arr_data);
        echo json_encode($datos);
    }

    /* END Datos actuales */


    /* Rectificaciones */

    function getRectificacionesAlumnosbyFicha() {

        $id_ficha = $this->input->post('ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_by_id_fichaFC($arr_data);
        echo json_encode($datos);
    }

    function getRectificacionesAlumnosDocbyFicha() {

        $id_ficha = $this->input->post('ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_doc_by_id_fichaFC($arr_data);
        echo json_encode($datos);
    }

    /* END Rectificaciones */

    /* Extensiones */

    function getRectificacionesExtensionbyFicha() {

        $id_ficha = $this->input->post('ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_extension_by_id_fichaFC($arr_data);
        echo json_encode($datos);
    }

    function getRectificacionesExtensionDocbyFicha() {

        $id_ficha = $this->input->post('ficha');
        //  variables de sesion
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_rectificaciones_extension_doc_by_id_fichaFC($arr_data);
        echo json_encode($datos);
    }

    /* End Extensiones */

    function getidEdutecnoCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeledutecno->get_arr_edutecno_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function listarfacturas() {

        $empresa = '';
        $holding ='';

        $ejecutivo = '';
        $num_ficha = $this->input->post('num_ficha');

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'empresa' => $empresa,
            'holding' => $holding,
            'ejecutivo' => $ejecutivo,
            'num_ficha' => $num_ficha,
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );

        $datos = $this->modelo->get_arr_listar_facturas($arr_sesion);
        echo json_encode($datos);
    }

    function listarnc() {
        
        $num_ficha = $this->input->post('num_ficha');

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(            
            'num_ficha' => $num_ficha,
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );

        $datos = $this->modelo->get_arr_listar_nc($arr_sesion);
        echo json_encode($datos);
    }

    function listarnd() {
        
        $num_ficha = $this->input->post('num_ficha');

        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(            
            'num_ficha' => $num_ficha,
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );

        $datos = $this->modelo->get_arr_listar_nd($arr_sesion);
        echo json_encode($datos);
    }

    function pdfResumen($id) {
        $id_ficha = $id;

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_ficha = $this->modelo->get_arr_ficha_by_id_resumenFC($arr_input_data);

        $data_orden_compra = $this->modelo->get_arr_orden_compra_by_ficha_idFC($arr_input_data);

        $arr_dias = explode(',', $data_ficha[0]['id_dias']);
        //var_dump($data_ficha);
        $dias = '';
        for ($y = 0; $y < count($arr_dias); $y++) {
            if ($y != 0) {
                $dias .= '-';
            }
            switch ($arr_dias[$y]) {
                case '1':
                    $dias .= 'L';
                    break;
                case '2':
                    $dias .= 'Ma';
                    break;
                case '3':
                    $dias .= 'Mi';
                    break;
                case '4':
                    $dias .= 'J';
                    break;
                case '5':
                    $dias .= 'V';
                    break;
                case '6':
                    $dias .= 'S';
                    break;
                case '7':
                    $dias .= 'D';
                    break;
            }
        }

        $this->load->library('Pdf');
        $pdf = new pdf('L', 'mm', 'LETTER');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins(15, 15, 15, true);

        // set auto page breaks false
        $pdf->SetAutoPageBreak(TRUE, 0);

        $pdf->AddPage('P', 'A4');
        $pdf->SetFont('times', '', 11);
        $html = '<style>
					td{
					padding-left: 3px;
					height:16px;
					}
				</style>';
        $html .= '
					<table border="1" width="81%">
						<tr>
							<td rowspan="2" width="200px"><img width="180px" style="margin-top: 5px;" src="' . base_url() . '/assets/img/logoEdutecno2.png"></td><td colspan="3" height="55px" style="font-size:20px;text-align:center"><strong>Solicitud de curso</strong></td>
						</tr>
						<tr style="font-size:8px">
							<td style="font-size:8px">FECHA ENVIO SOLICITUD</td><td colspan="2">PERSONA QUE SOLICITA</td>
						</tr>
						<tr>
							<td style="text-align:center">Organismo Técnico de Capacitación</td><td>&nbsp;' . $data_ficha[0]['fecha_solicitud'] . '</td><td colspan="2">&nbsp;' . $data_ficha[0]['ejecutivo'] . '</td>
						</tr>
					</table>
		';
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">HOLDING&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['holding'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">EMPRESA&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['empresa'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">RUT&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['rut_empresa'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">DIRECCIÓN&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['direccion_empresa'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">NOMBRE CONTACTO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['nombre_contacto'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">TELÉFONO CONTACTO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['telefono_contacto'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">DIRECCIÓN ENVÍO DIPLOMAS&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['direccion_diplomas'] . '</td>
						</tr>
					</table>
		';
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">OTIC&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['otic'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">N° O.C.&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['num_orden_compra'] . '</td>
						</tr>						
					</table>
		';
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">CURSO&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['descripcion_producto'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">VERSIÓN&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['version'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">MODALIDAD&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['modalidad'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">TIPO CURSO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['modalidad'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">CÓDIGO SENCE&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['codigo_sence'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">FECHA INICIO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['fecha_inicio'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">FECHA TÉRMINO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['fecha_fin'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">HORARIO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['hora_inicio'] . ' - ' . $data_ficha[0]['hora_termino'] . '</td>
						</tr>						
					</table>
		';
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">SEDE&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['sede'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">SALA&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['sala'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">DIRECCIÓN&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['sede'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">DÍAS&nbsp;&nbsp;</td>
							<td>&nbsp;' . $dias . '</td>
						</tr>
												
					</table>
		';

        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">EJECUTIVO&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['ejecutivo'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">OBSERVACIONES&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['comentario_orden_compra'] . '</td>
						</tr>
						
												
					</table>
		';


        $html .= '<table><tr><td>&nbsp;</td></tr></table>
				  <table><tr><td>Listado de Participantes</td></tr></table>
				  <table><tr><td>&nbsp;</td></tr></table>
				  <table border="1">
				  	<tr>
						<td width="10%" style="background-color:#CECECE;font-size:10px"><strong>&nbsp;&nbsp;N</strong></td>
						<td width="30%" style="background-color:#CECECE;font-size:10px"><strong>RUT</strong></td>
						<td width="40%" style="background-color:#CECECE;font-size:10px"><strong>NOMBRE ALUMNO</strong></td>															
						<td width="20%" style="background-color:#CECECE;font-size:10px"><strong>O.C.</strong></td>						
					</tr>';

        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($data_orden_compra[0]['id']),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        $z = 1;
        for ($x = 0; $x < count($data_orden_compra); $x++) {
            $arr_data = array(
                'id_ficha' => trim($id_ficha),
                'orden_compra' => trim($data_orden_compra[$x]['id']),
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $datos = $this->modelo->get_arr_listar_datos_orden_compraFC($arr_data);

            //var_dump($datos);
            for ($i = 0; $i < count($datos); $i++) {

                $html .= '  <tr>
								<td>&nbsp;&nbsp;' . $z . '</td>
								<td>' . $datos[$i]['rut'] . '</td>
								<td>' . $datos[$i]['nombre'] . '</td>								
								<td>' . $data_orden_compra[$x]['text'] . '</td>
									
							  </tr>	';
                $z++;
            }
        }
        $html .= '</table>';
        //var_dump($datos[0]);
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table><tr><td>Resumen</td></tr></table>
					<table><tr><td>&nbsp;</td></tr></table>
					<table width="50%" border="1">
						<tr>
							<td style="text-align:right">Total Alumnos&nbsp;&nbsp;</td>
							<td style="text-align:center">' . $datos[0]['total_alumnos'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right">Alumnos Sence&nbsp;&nbsp;</td>
							<td style="text-align:center">' . $datos[0]['total_sence'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right">Alumnos Empresa&nbsp;&nbsp;</td>
							<td style="text-align:center">' . $datos[0]['total_empresa'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right">Alumnos Sence/Empresa&nbsp;&nbsp;</td>
							<td style="text-align:center">' . $datos[0]['total_sence_empresa'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right">Alumnos Becados&nbsp;&nbsp;</td>
							<td style="text-align:center">' . $datos[0]['total_becados'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right">Total Valor OC&nbsp;&nbsp;</td>
							<td style="text-align:right">' . $datos[0]['total_oc'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right">Total Valor Agregado&nbsp;&nbsp;</td>
							<td style="text-align:right">' . $datos[0]['total_agregado'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right">Valor Final&nbsp;&nbsp;</td>
							<td style="text-align:right">' . $datos[0]['valor_final'] . '</td>
						</tr>
					</table>';

        $pdf->writeHTML($html, true, false, true, false, '');

        //Close and output PDF document
        $pdf->Output('Resumen.pdf', 'I');
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
