<?php

/**
 * PanelDeControl_c
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

// include('application/libraries/Sbif/Sbif.php');

//require(APPPATH.'libraries/REST_Controller.php');
class Home extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Home';
    private $nombre_item_plural = 'Home';
    private $package = 'back_office';
    private $model = 'Home_model';
    private $menu = 'Menu_model';
    private $view = 'Home_v';
    private $controller = 'Home';
    private $ind = '';
    private $dashboard = '';

    function __construct() {

        parent::__construct();

        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package . '/' . $this->menu, 'menu_model');
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {

        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            )
        );


        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js')
        );


        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/weather-icons.min.css')
        );


        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['dashboard'] = $this->session->userdata('dashboard');
        $data['activo'] = "";
        ///  cargo el menu en la session

        $arr_menu_user = array(
            'menu_usuario' => $this->menu_model->get_arr_user($datos_menu)
        );

        $this->session->set_userdata($arr_menu_user);

        $data['menus'] = $this->session->userdata('menu_usuario'); //mantener en todos los codigos

        $data['page_menu'] = 'dashboard';
        // menu de usuario 
 
        if ($data['dashboard'] == 'home') {
            $this->view = "Home_v";
        } else {
            
            redirect($data['dashboard'], 'refresh');
        }
       $this->comprobarCodigoSENCEvencimiento();


        /*    switch ($datos_menu['user_perfil']) {
          case 19: //  perfil don carlos Director
          redirect('back_office/Dashboard/Director', 'refresh');
          break;
          case 1: // perfil sistemas
          redirect('back_office/Dashboard/Director', 'refresh');
          break;
          case 21: //  perfil vitalia
          redirect('back_office/Dashboard/Director', 'refresh');
          break;
          case 13: // perfil ejecutivo
          redirect('back_office/Dashboard/Ejecutivo', 'refresh');
          break;
          case 12: // perfil Back office
          redirect('back_office/Dashboard/Backoffice', 'refresh');
          break;
          default : $this->view = "Home_v";
          break;
          } */

        //indicadores economicos
        $apiUrl = 'https://mindicador.cl/api';
        //$apiUrl = 'https://indicadoresdeldia.cl/webservice/indicadores.json';
        //Es necesario tener habilitada la directiva allow_url_fopen para usar file_get_contents
        if (ini_get('allow_url_fopen')) {
            $json = json_decode(file_get_contents($apiUrl));
        } else {
            //De otra forma utilizamos cURL
            $curl = curl_init($apiUrl);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $json = curl_exec($curl);
            curl_close($curl);
        }


        //UF
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://api.sbif.cl/api-sbifv3/recursos_api/uf?apikey=fd7ab711e75b9d1f9d12619a9fed91c7748db879&formato=json');
        $uf = curl_exec($ch);
        curl_close($ch);
        $uf = json_decode($uf);

        //Dolar
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://api.sbif.cl/api-sbifv3/recursos_api/dolar?apikey=fd7ab711e75b9d1f9d12619a9fed91c7748db879&formato=json');
        $dolar = curl_exec($ch);
        curl_close($ch);
        $dolar = json_decode($dolar);

        //UTM
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://api.sbif.cl/api-sbifv3/recursos_api/utm?apikey=fd7ab711e75b9d1f9d12619a9fed91c7748db879&formato=json');
        $utm = curl_exec($ch);
        curl_close($ch);
        $utm = json_decode($utm);

        //IPC
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://api.sbif.cl/api-sbifv3/recursos_api/ipc?apikey=fd7ab711e75b9d1f9d12619a9fed91c7748db879&formato=json');
        $ipc = curl_exec($ch);
        curl_close($ch);
        $ipc = json_decode($ipc);

        //CALIDAD DEL AIRE
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://sinca.mma.gob.cl/index.php/json/listadomapa/');
        $aire = curl_exec($ch);
        curl_close($ch);
        $aire = json_decode($aire);

        //bloque geo local
        $ip = $this->input->ip_address(); // the IP address to query
        $query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        if ($query && $query['status'] == 'success') {
            $query['city'] = strtolower($query['city']);
        } else {
            $query['city'] = 'santiago';
        }
        //bloque geilocal
        //bloque tiempo

        $BASE_URL = "http://query.yahooapis.com/v1/public/yql";
        $yql_query = 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' . $query['city'] . ', cl") AND u="c"';
        $yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&format=json";
        // Make call with cURL
        $session = curl_init($yql_query_url);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $jsonw = curl_exec($session);
        // Convert JSON to PHP object
        $phpObj = json_decode($jsonw);
        $output = $phpObj;


        //fin bloque tiempo
        //gracias totales


        $data['output'] = $output;
        $data['json'] = $json;
        $data['uf'] = $uf;
        $data['dolar'] = $dolar;
        $data['utm'] = $utm;
        $data['ipc'] = $ipc;
        $data['aire'] = $aire;
        $data['main_content'] = $this->package . '/' . $this->view;


        $this->load->view('amelia_1/template', $data);
    }

    function comprobarCodigoSENCEvencimiento(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->comprobarCodigoSENCEvencimiento($arr_sesion);
        if(count($datos) > 0){
            $this->enviaEmail($datos);
        }
    }

    function enviaEmail($datos_sence) {

        $destinatarios = array();
        $asunto = "";
        $body = "";

        $asunto = "Hay nuevos codigos por expirar!";
        $body = $this->generaHtml($datos_sence); // mensaje completo HTML o text
        $destinatarios = array(
            array('email' => 'nmunoz@edutecno.com', 'nombre' => 'Natalia Muñoz')
        );

        $cc = array(
            array('email' => 'cramos@edutecno.com', 'nombre' => 'Claudia Ramos'),
            array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
            array('email' => 'dfilippi@edutecno.com', 'nombre' => 'David De Filippi'),
        );

        $AltBody = $asunto; // hover del cursor sobre el correo
        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */

        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        $this->setCodeSenceNotificado();

        //echo json_encode($body);
    }

    function generaHtml($data) {

        $html = '';
        $html .='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .='<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .='<head>';
        $html .='<meta name="viewport" content="width=device-width" />';
        $html .='<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .='<title>Aprueba venta curso E-learning </title>';
        $html .='<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .='</head>';
        $html .='<body>';
        $html .='<table width="100%">';
        $html .='<tr>';
        $html .='<td></td>';
        $html .='<td width="100%">';
        $html .='<div width="100%">';
        $html .='<table width="100%" cellpadding="0" cellspacing="0">';
        $html .='<tr>';
        $html .='<td class="content-wrap aligncenter">';
        $html .='<table width="100%" cellpadding="0" cellspacing="0">';
        $html .='<tr>';
        $html .='<td >';
        $html .='<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        $html .='</td>';
        $html .='</tr>';
        $html .='<tr>';
        $html .='<td class="content-block" width="100%">';
        $html .='<table class="invoice">';
        $html .='<tr>';
        $html .='<td class="content-block">';
        $html .='Los siguientes códigos SENCE se encuentran próximos a vencer su vigencia: ';
        $html .='</td>';
        $html .='</tr>';
        $html .='<tr>';
        $html .='<td>';
        $html .='<table class="invoice-items" cellpadding="0" cellspacing="0" border="1">';
        $html .='<tr>';
        $html .='<th>C. SENCE</th>';
        $html .='<th>Fecha Vigencia</th>';
        $html .='<th>Estado</th>';
        $html .='<th>Nombre Curso</th>';
        $html .='</tr>';

        for ($i=0; $i < count($data); $i++) { 
            $html .='<tr>';
            $html .='<td>'.$data[$i]['codigo_sence'].'</td>';
            $html .='<td>'.$data[$i]['fecha_vigencia'].'</td>';
            $html .='<td>'.$data[$i]['vigencia_restante'].'</td>';
            $html .='<td>'.$data[$i]['nombre_curso_code_sence'].'</td>';
            $html .='</tr>';
        }

        $html .='</table>';
        $html .='</td>';
        $html .='</tr>';
        $html .='</table>';
        $html .='</td>';
        $html .='</tr>';
        $html .='<tr>';
        $html .='<td class="content-block">';
        $html .='<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .='</td>';
        $html .='</tr>';
        $html .='<tr>';
        $html .='<td class="content-block">';
        $html .='Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .='</td>';
        $html .='</tr>';
        $html .='</table>';
        $html .='</td>';
        $html .='</tr>';
        $html .='</table>';
        $html .='<div class="footer">';
        $html .='<table width="100%">';
        $html .='<tr>';
        $html .='<td id="header-text" class="aligncenter content-block"></td>';
        $html .='</tr>';
        $html .='</table>';
        $html .='</div></div>';
        $html .='</td>';
        $html .='<td></td>';
        $html .='</tr>';
        $html .='</table>';
        $html .='<script type="text/javascript">';
        $html .='var d = new Date();';
        $html .='var n = d.getFullYear();';
        $html .='document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .='</script>';
        $html .='</body>';
        $html .='</html>';
        return $html;
    }

    function setCodeSenceNotificado(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->setCodeSenceNotificado($arr_sesion);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
