<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2018-01-03 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2018-01-03 [Jessica Roa] <jroa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar';
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Descuento';
    private $model = 'Descuento_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';
    private $package2 = 'back_office/Usuario';
    private $model2 = 'Usuario_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'modelousuario');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Descuento/Listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Descuentos Fichas Pagadas";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['menus'] = $this->session->userdata('menu_usuario');

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listarRectificaciones() {
        $input_mes = $this->input->post('calendario_mes');
        // var_dump($this->input->post());
     /*   if($input_mes == '') {
            $fec_actual = getdate();
            $exploded[0] = $fec_actual["mon"] - 2;
            if($exploded[0] < 1){
                $exploded[1] = $fec_actual["year"] - 1;
                if($exploded[0] == -1) { //es enero
                    $exploded[0] = 11;
                }
                if($exploded[0] == 0) { // es feb
                    $exploded[0] = 12;
                }
            } else {
                $exploded[1] = $fec_actual["year"];
            }
        } else {
            $exploded = explode('-', $input_mes);
        }
        */
        
        $exploded = explode('-', $input_mes);
        /*calculo 2 meses despues*/
        if(intval($exploded[0]) < 11) { // de oct a enero
            $fecha_monto = $exploded[0] + 2;
            
        }
        if(intval($exploded[0]) == 11) { //selecciono nov
            $fecha_monto = 1;
        }
        if(intval($exploded[0]) == 12) { //selecciono dic
            $fecha_monto = 2;
            
        }
        $fecha_ant_consulta = $exploded[1] . '-' . $exploded[0] . '-01'; 

        $datos['fecha_ant'] = $fecha_ant_consulta;
        $datos['fecha_m'] = $fecha_monto;
        $datos['user_id'] = $this->session->userdata('id_user');
        $datos['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'fecha_ant' => $datos['fecha_ant'],
            'fecha_m' => $datos['fecha_m'],
            'user_id' => $datos['user_id'],
            'user_perfil' => $datos['user_perfil']
        );

        $datos_res = $this->modelo->get_arr_listar_rectificaciones($arr_sesion);
        echo json_encode($datos_res);
    }
}
