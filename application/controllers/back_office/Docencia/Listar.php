<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = '';
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Docencia';
    private $model = 'Docencia_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('Excel');
    }

    // 2016-10-14
    // Controlador del Editar Perfil
    public function index() {

        //  si entra a la mala lo redirige al listar
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Consulta_learn',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Resumen',
                'label' => 'Resumen',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Docencia/docencia.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;
        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Docencia";

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        //Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu 
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function ListarChange() {

        $start = $this->input->post('fecha');

        $array_datos = array(
            'star' => $start,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_consultar_ficha_data($array_datos);
        echo json_encode($datos);
    }

    function Listar() {

        $mes = date('m');
        $year = date('Y');

        $start = $year . '-' . $mes;

        $array_datos = array(
            'star' => $start,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_consultar_ficha_data($array_datos);
        echo json_encode($datos);
    }

    function generarExcel($inicio) {

        $star = $inicio;

        $arr_datos = array(
            'star' => $star,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_exel = $this->modelo->get_arr_consultar_ficha_data($arr_datos);


        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Informe Docencia');
        //set cell A1 content with some text



        $this->excel->getActiveSheet()->SetCellValue('A1', 'N° DE FICHA');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'SALA');

        $this->excel->getActiveSheet()->SetCellValue('C1', 'ESTADO');
        $this->excel->getActiveSheet()->SetCellValue('D1', 'FECHA INICIO');
        $this->excel->getActiveSheet()->SetCellValue('E1', 'FECHA FIN');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'FECHA RECTIFICACIÓN');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'CÓDIGO SENCE');
        $this->excel->getActiveSheet()->SetCellValue('H1', 'MODALIDAD');
        $this->excel->getActiveSheet()->SetCellValue('I1', 'CURSO');
        $this->excel->getActiveSheet()->SetCellValue('J1', 'DURACIÓN');
        $this->excel->getActiveSheet()->SetCellValue('K1', 'NIVEL');
        $this->excel->getActiveSheet()->SetCellValue('L1', 'N° ALUMNOS');
        $this->excel->getActiveSheet()->SetCellValue('M1', 'OTIC');
        $this->excel->getActiveSheet()->SetCellValue('N1', 'EMPRESA');
        $this->excel->getActiveSheet()->SetCellValue('O1', 'RUT EMPRESA');
        $this->excel->getActiveSheet()->SetCellValue('P1', 'RELATOR');
        $this->excel->getActiveSheet()->SetCellValue('Q1', 'RUT RELATOR');

        $this->excel->getActiveSheet()->fromArray($data_exel, null, 'A2');

        $this->excel->getActiveSheet()->getStyle('A1:Q1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '#afddb2')
                    )
                )
        );

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = 'Informe_Docencia.xls'; //save our workbook as this file name  sp_select_jd_filter
        ob_clean();
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: private'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

}

/* End of fil Agregar.php */
/* Location: ./application/controllers/Agregar.php */

