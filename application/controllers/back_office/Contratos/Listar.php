<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-11 [Elber Galarga] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-12-11 [Alan Brito] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Contratos';
    private $model = 'Contratos_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');



        //  Libreria de sesion
        $this->load->library('session');
    }

    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );


        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contratos/Listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Contratos";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['menus'] = $this->session->userdata('menu_usuario');

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listar() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );
        $datos = $this->modelo->get_arr_listar_contratos($arr_sesion);
        echo json_encode($datos);
    }

    function listarDocentes() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $ficha = $this->input->post('num_ficha');
        $arr_sesion = array(
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil'],
            'ficha' => $ficha
        );
        $datos = $this->modelo->get_arr_listar_docentes($arr_sesion);
        echo json_encode($datos);
    }

    function datosContrato() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $ficha = $this->input->post('num_ficha');
        $arr_sesion = array(
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil'],
            'ficha' => $ficha
        );
        $datos = $this->modelo->get_arr_listar_datos_contrato($arr_sesion);
        echo json_encode($datos);
    }

    function listarDatosDocentes() {


        $ficha = $this->input->post('num_ficha');
        $id_usuario = $this->input->post('id_usuario');

        $arr_sesion = array(
            'ficha' => $ficha,
            'usuario' => $id_usuario,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_datos_docentes($arr_sesion);
        echo json_encode($datos);
    }

    function listarComunas() {

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $arr_sesion = array(
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );
        $datos = $this->modelo->get_comunas($arr_sesion);
        echo json_encode($datos);
    }

    public function Contrato() {

        $date = new DateTime($_POST['fecha_termino']);
        $date2 = new DateTime($_POST['fecha_entrega']);

        $relator_ficha = $_POST['relator_ficha'];
        $fecha_emision = date("Y-m-d H:i:s");
        $nombre_relator = $_POST['nombre'];
        $rut_relator = $_POST['rut'];
        $direccion_relator = $_POST['domicilio'];
        $comuna_relator = $_POST['comuna'];
        $curso_relator = $_POST['nombre_curso'];
        $horas_curso = $_POST['horas'];
        $fecha_termino = $date->format('Y-m-d');
        $valor_hora = $_POST['valor'];
        $fecha_boleta = $date2->format('Y-m-d');
        $horas_pagadas = $_POST['horas_pagadas'];
        $alumnos = $_POST['alumnos'];
        $firma = $_POST['firma'];
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_datos = array(
            'relator_ficha' => $relator_ficha,
            'fecha_emision' => $fecha_emision,
            'nombre_relator' => $nombre_relator,
            'rut_relator' => $rut_relator,
            'direccion_relator' => $direccion_relator,
            'comuna_relator' => $comuna_relator,
            'curso_relator' => $curso_relator,
            'horas_curso' => $horas_curso,
            'fecha_termino' => $fecha_termino,
            'valor_hora' => $valor_hora,
            'fecha_boleta' => $fecha_boleta,
            'horas_pagadas' => $horas_pagadas,
            'horas_pagadas' => $horas_pagadas,
            'alumnos' => $alumnos,
            'firma' => $firma,
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );

        $respuesta = $this->modelo->set_contrato($arr_datos);


        // var_dump($respuesta[0]['fecha_boleta']);


        $fecha_boleta = $respuesta[0]['fecha_boleta'];



        $this->load->library('Pdf');
        $pdf = new pdf('P', 'mm', 'A4');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins(20, 20, 20, true);

        // set auto page breaks false
        $pdf->SetAutoPageBreak(false, 0);

        //bloque para todos los alumnos

        $pdf->AddPage('P', 'A4');
        $pdf->SetFont('times', '', 10);
        //$pdf->Image(base_url().'assets/amelia/images/backgrounds/image_contrato.jpg', 0, 0, 210, 297, 'JPG', '', '', true, 200, '', false, false, 0, false, false, true);
        $html = '<style>
		p{
			font-size: 9px;
			text-align: justify;
			
			}
			
		h3{
			text-align: center;
		}
		li {margin:0}
		</style>';

        $html .= '<table border="0"><tr><td><table border="1"><tr><td>Capac/S Privado/S Producti/Otras</td></tr></table></td><td></td><td style="font-size:15px;font-weight:bold;font-family: sans-serif;text-align:right;padding-right:40px;">' . $_POST['ficha'] . '</td></tr></table>';
        $html .= '<h3><u>Contrato de Prestación de Servicios Profesionales</u></h3>';

        if ($firma == 1) {
            $nombreEdu = 'doña Vitalia Linares Oyarzún';
            $rutEdu = '10.017.057-4';
        } else {
            $nombreEdu = 'don Carlos Linares Oyarzún';
            $rutEdu = '10.067.581-1';
        }

        $html .= '<p>En Santiago de Chile a <b>' . date("d-m-Y") . '</b>, entre '.$nombreEdu.', Rut: '.$rutEdu.' en Representación de Linares y Compañía Ltda. en adelante Edutecno, Rut 77.682.510-7, domiciliado en Huérfanos 863 Piso 2, de la Comuna de Santiago, y Don <b>' . $_POST['nombre'] . '</b>, en adelante relator, Rut <b>' . $_POST['rut'] . '</b> domiciliado <b>' . $_POST['domicilio'] . '</b> de la Comuna de <b>' . $_POST['comuna'] . '</b> se ha convenido en el siguiente contrato de prestación de servicios profesionales:</p>';
        $html .= '<p><strong>Primero: </strong> Edutecno contrata servicios profesionales de don <b>' . $_POST['nombre'] . '</b>, a fin de que éste se desempeñe como relator a honorarios, para dictar el curso de Ficha N° <b>' . $_POST['ficha'] . '</b>, Nombre <b>' . $_POST['nombre_curso'] . '</b>, de <b>' . $_POST['horas'] . '</b> horas cronológicas, con <b>' . $_POST['alumnos'] . '</b> alumnos, cuya fecha de término es <b>' . $_POST['fecha_termino'] . '</b>. El Relator desarrollará su materia bajo su plena responsabilidad.<br><strong>Segundo: </strong>El horario de clases se determinará por común acuerdo entre el relator y Edutecno; pero cada relator y Edutecno; pero cada relator podrá cambiar la distribución de sus clases cuando la Dirección de Edutecno lo requiera, siempre que este cambio no entorpezca las demás clases y siempre que haya continuidad de horarios de modo que no se produzcan vacíos o esperas que perjudiquen a los alumnos. Con todo, y tratádose de clases extraordinarias, el profesor podrá pactar, con los alumnos, y previa autorización de la Dirección de Edutecno, un horario distinto al habitual.<br>
		';
        $html .= '
		<strong>Tercero: </strong>El relator se obliga por intermedio del presente contrato a:
		<ol>
		<li>Llegar quince minutos antes de dar inicio a sus clases, al recinto donde estas se ejecuten</li>
		<li>Hacer las pruebas o exámenes en la fecha que corresponda </li>
		<li>Corregir y entregar las calificaciones en forma oportuna, no más de dos días hábiles después de la última clase. </li>
		<li>Mantener al día el libro de clases, con la asistencia y contenidos correspondientes al curso. </li>
		<li>Cumplir con las normas de conducta habitual que todo recinto educacional requiere para su correcto desempeño.</li>
		</ol>
		El incumplimiento de cualquiera de estas obligaciones, da derecho a Edutecno para:
		<ol type="a">
		  <li>Poner término de inmediato al presente contrato, sin forma de juicio, con la sola notificación al relator. </li>
		  <li>En el caso de la obligación 1, se descontara una hora de clase por día atrasado.</li>
		  <li>En el caso de la obligación 3 y 4 se sancianora con una hora de clase por día atrasado.</li>
		</ol>
		<strong>Cuarto:</strong> El relator podrá prestar servicios, además, a otras personas e instituciones. En ningún caso se le exige, al profesor, por
este contrato, dedicación exclusiva o absoluta.<br>
<strong>Quinto:</strong> Se deja constancia que este contrato no constituye vínculo de dependencia entre el relator y Edutecno, ni genera
relación laboral alguna; pues las labores del relator son discontinuas, y no son exclusivas y éste goza de amplia libertad para
impartir sus clases, debiendo , no obstante, ceñirse al programa de clases y a las materias que dicho programa contiene. Por
consiguiente, entiende que el presente contrato no es un contrato de trabajo amparado por el Código del Trabajo, sino un
contrato de prestación de servicios profesionales, de carácter civil.<br>
<strong>Sexto:</strong> Edutecno pagará al profesor, la suma de <b>$ ' . number_format($_POST['valor'], 0, ',', '.') . '</b> -por cada hora cronológica de clases según la cantidad de horas que
el curso requiera en base al programa preestablecido por Edutecno. Para el pago de estos honorarios, el relator deberá presentar
previamente la boleta de honorarios en la fecha <b>' . $fecha_boleta . '</b>, día en que se efectuara el pago correspondiente. A la
cantidad resultante se descontará el 10% de retención legal por concepto de Impuesto a la Renta. En el pago de las horas
realizadas, se entiende incluido el pago de la prestación de ellas, la corrección de las pruebas y exámenes y de los trabajos
académicos inherentes al ramo y la movilización necesaria para llegar hasta el lugar de desarrollo de las clases. Se deja
constancia que el relator no tiene derecho a ningún otro pago ni beneficio, distinto que los honorarios pactados.<br>
<strong>Séptimo:</strong> El presente contrato tendrá la duración de <b>' . $_POST['horas_pagadas'] . '</b>, que equivale a las horas cronológicas que requiere el curso que se
debe impartir, motivo del contrato. Pudiendo desahuciarse, por cualquiera de las partes, con un aviso dado a la otra con un
periodo de, a lo menos, 07 días de anticipación. Octavo: Para los efectos de este contrato, las partes fijan su domicilio en la
ciudad de Santiago, y se someten a la jurisdicción de sus Tribunales de Justicia. Noveno: El presente contrato se suscribe en
duplicado quedando una copia en poder de cada parte. 
		</p>
		<p></p>
		<p></p>
		<p>
		
		<table>
			<tr>
				<td style="text-align:center">-------------------------------------------</td>
                
                <td style="text-align:center">-------------------------------------------</td>
			</tr>
			<tr>
                                <td style="text-align:center">';
        if ($firma == 1) {
            $html .= '<b>Vitalia Linares Oyarzún</b>';
        } else {
            $html .= '<b>Carlos Linares Oyarzún</b>';
        }

        $html .= '</td>
        
                <td style="text-align:center"><b>' . $_POST['nombre'] . '</b></td>
			</tr>
			<tr>
                <td style="text-align:center">Representante Legal</td>
                <td style="text-align:center">Relator</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td></td>
			</tr>
			<tr>
				<td style="border: 2px solid #000">
					<table width="80%">
						<tr>
							<td style="text-align:right">Total Bruto:</td>
							<td style="text-align:right"><b>' . number_format($_POST['valor'] * $_POST['horas_pagadas'], 0, ',', '.') . '</b></td>
						</tr>
						<tr>
							<td style="text-align:right">Retención:</td>
							<td style="text-align:right"><b>' . number_format(($_POST['valor'] * $_POST['horas_pagadas']) * 0.1, 0, ',', '.') . '</b></td>
						</tr>
						<tr>
							<td></td>
							<td><hr></td>
						</tr>
						<tr>
							<td style="text-align:right">Liquido a Pagar:</td>
							<td style="text-align:right;font-size:12px"><b>' . number_format(($_POST['valor'] * $_POST['horas_pagadas']) - (($_POST['valor'] * $_POST['horas_pagadas']) * 0.1), 0, ',', '.') . '</b></td>
						</tr>
					</table>
				</td>
				<td>
					<table>
						<tr>
							<td style="text-align:center"></td>
						</tr>
						<tr>
							<td style="text-align:center"></td>
						</tr>
					</table>
					
				</td>
			</tr>
		</table>
		</p>
		';

        $pdf->writeHTML($html, true, false, true, false, '');


        //Close and output PDF document
        $pdf->Output('Contrato_Relator_' . $_POST['nombre'] . '.pdf', 'I');
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
