﻿
<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Ver extends MY_Controller {

    
    // Variables paramétricas	
    private $package = 'back_office/ActVigente';
    private $model = 'Ver_model';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->library('session');
        $this->load->library('Functions');
        $this->load->library('Pdf');
    }

    public function index() {



        $funciones = new Functions();
        $num_ficha = $this->input->post('num_num_ficha');
        $empresa = $this->input->post('id_empresa');

        $start = $this->input->post('fecha_start');
        $end = $this->input->post('fecha_end');

        $start = $funciones->formatDateBD($start);
        $end = $funciones->formatDateBD($end);

        //  variables de sesion

        $arr_sesion = array(
            'num_ficha' => $num_ficha,
            'empresa' => $empresa,
            'fecha_inicio' => $start,
            'fecha_termino' => $start,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos_arr = $this->modelo->get_arr_actividades($arr_sesion);

        //var_dump(count($datos_arr));
        // create new PDF document
        $GLOBALS ['num_ficha'] = 0;
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Felipe Bulboa');
        $pdf->SetTitle('Ejemplo de fichas con TCPDF');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(
            0,
            64,
            0
                ), $lc = array(
            0,
            64,
            128
        ));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('Helvetica', '', 14, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L');

        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));


        $html = '';
        $html .= '		
				<table width="100%">
					<tr>
						<th>
							<img src="' . base_url() . 'assets/amelia/images/logo_edutecno_login.png' . '" width="50px" >
						</th>
						<th>
							<table border="1" cellpadding="4" cellspacing="0" align="center">
        						<tr>
									<td>ACTIVIDADES VIGENTES</td>
        						</tr>
							</table>
						</th>
						<th>
							
						</th>
					</tr>
					<tr><td></td><td>&nbsp;</td><td></td></tr>
					<tr>
						<td colspan="3">
							<table border="1" style="font-size:9px;" align="center">
								<tr>
									<th width="30px"><strong>Ficha</strong></th>
									<th width="90px"><strong>Empresa</strong></th>
									<th><strong>F.Inicio</strong></th>
									<th><strong>F.Térm.</strong></th>
									<th width="120px"><strong>Nombre Curso</strong></th>
									<th width="25px"><strong>Hrs</strong></th>
									<th><strong>Relator</strong></th>
									<th width="70px"><strong>Dias</strong></th>
									<th width="40px"><strong>H.I</strong></th>
									<th width="40px"><strong>H.T</strong></th>
									<th><strong>Sala</strong></th>
									<th width="25px"><strong>Al.</strong></th>
									<th width="70px"><strong>Revisión</strong></th>
									<th width="45px"><strong>SenceNet</strong></th>
								</tr>';
        for ($i = 0; $i < count($datos_arr); $i++) {
            $html .= '<tr>';
            $html .= '<td>' . $datos_arr[$i]['num_ficha'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['razon_social_empresa'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['fecha_inicio'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['fecha_cierre'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['nombre_curso'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['duracion_curso'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['relator'] . '</td>';
            $html .= '<td style="font-size:8px;">' . $datos_arr[$i]['dias'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['hora_inicio'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['hora_termino'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['sala'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['alumnos'] . '</td>';
            $html .= '<td></td>';
            $html .= '<td>' . $datos_arr[$i]['id_sence'] . '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>
						</td>
					</tr>					
				  </table>';

        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Localidades de.pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

    function setGenerarpdf($fecha_reporte) {

        $GLOBALS['num_ficha'] = '0';
        $GLOBALS['codigo_sence'] = '0';
        //  variables de sesion

        $funciones = new Functions();
        $fecha_reporte = $funciones->formatDateBD($fecha_reporte);

        $arr_sesion = array(
            'fecha_reporte' => $fecha_reporte,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos_arr = $this->modelo->get_arr_act_vig($arr_sesion);

        //var_dump(count($datos_arr));
        // create new PDF document 
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitacion');
        $pdf->SetTitle('Dpto_Tecnico	');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(
            0,
            64,
            0
                ), $lc = array(
            0,
            64,
            128
        ));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('Helvetica', '', 14, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L');

        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));

        $now_consulta = $datos_arr[0]['fecha_server'];
        $now_fecha = $datos_arr[0]['fecha_corta'];

        $html = '';
        $html .= '		
				<table width="100%">
					<tr>
						<th>
							<img src="' . base_url() . 'assets/amelia/images/logo_edutecno_login.png' . '" width="50px" >
						</th>
						<th>
							<table border="1" cellpadding="4" cellspacing="0" align="center">
        						<tr>
									<td>DEPARTAMENTO TÉCNICO</td>

        						</tr>
								
        						<tr>
									<td><p>Cursos día: ' . $now_fecha . '</p></td>
									
        						</tr>
							</table>
						</th>
						<th>
							<p align="right">Consulta de datos:<br>' . $now_consulta . '</p>
						</th>
					</tr>
					<tr><td></td><td>&nbsp;</td><td></td></tr>
					<tr>
						<td colspan="3">
							<table border="1" style="font-size:9px;" align="center">
								<tr>
									<th width="35px"><strong>SALA</strong></th>
									<th width="38px"><strong>H. INICIO</strong></th>
									<th width="45px"><strong>H. TERMINO</strong></th>
									<th width="40px"><strong>FICHA</strong></th>
									<th width="130px"><strong>EMPRESA</strong></th>
									<th width="40px"><strong>DIAS</strong></th>
									<th width="50px" ><strong>F. INICIO</strong></th>
									<th width="50px"><strong>F. FIN</strong></th>
									<th width="70px"><strong>CURSO | VERSIÓN</strong></th>
									<th width="40px"><strong>DURACIÓN</strong></th>
									<th width="40px" ><strong>SENCE</strong></th>
									<th width="40px"><strong>RELATOR</strong></th>
									<th width="35px"><strong>CANT. ALUMNOS</strong></th>
									<th width="120px"><strong>OBSERVACIÓN</strong></th>
								</tr>';
        for ($i = 0; $i < count($datos_arr); $i++) {
            $html .= '<tr>';
            $html .= '<td>' . $datos_arr[$i]['nombre'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['Hora_inicio'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['Hora_termino'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['num_ficha'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['razon_social_empresa'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['dias'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['fecha_inicio'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['fecha_fin'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['version'] . '</td>';

            $html .= '<td>' . $datos_arr[$i]['horas_totales'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['sence'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['Nombre_relator'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['Alumnos'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['descripcion'] . '</td>';

            $html .= '</tr>';
        }

        $total = $i;

        $html .= '</table>
					</td>
					<p>Total cursos vigentes del día: ' . $total . '</p>
					</tr>

				  </table>';

        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);

        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Actividades Vigentes_" . $now_consulta . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }

    function setGenerarpdfDiario($fecha_reporte, $externos) {

        //  variables de sesion
        $GLOBALS['num_ficha'] = '0';
        $GLOBALS['codigo_sence'] = '0'; 
        $funciones = new Functions();
        $fecha_reporte = $funciones->formatDateBD($fecha_reporte);
        $arr_sesion = array(
            'fecha_reporte' => $fecha_reporte,
            'externos' => $externos,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos_arr = $this->modelo->get_arr_act_vig_diaria($arr_sesion);
        //var_dump(count($datos_arr));
        //die();
        if(count($datos_arr) == 0) {
            header('Location: ../../../../Error/Error?error=0');
        }


        //var_dump(count($datos_arr));
        // create new PDF document 
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Recepción');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(
            0,
            64,
            0
                ), $lc = array(
            0,
            64,
            128
        ));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('Helvetica', '', 14, '', true);
        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L');
        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));

        $now_consulta = $datos_arr[0]['fecha_server'];
        $now_fecha = $datos_arr[0]['fecha_corta'];

        $html = '';
        $html .= '		
				<table width="100%">
					<tr>
						<th>
							<img src="' . base_url() . 'assets/amelia/images/logo_edutecno_login.png' . '" width="50px" >
						</th>
						<th>
							<table border="1" cellpadding="4" cellspacing="0" align="center">
        						<tr>
									<td>DEPARTAMENTO TÉCNICO</td>

        						</tr>
								
        						<tr>
									<td><p>Cursos día: ' . $now_fecha . '</p></td>
									
        						</tr>
							</table>
						</th>
						<th>
							<p align="right">Consulta de datos:<br>' . $now_consulta . '</p>
						</th>
					</tr>
					<tr><td></td><td>&nbsp;</td><td></td></tr>
					<tr>
						<td colspan="3">
							<table border="1" style="font-size:9px; padding:0px;" align="center">
								<tr>
						 
                                    <th width="60px"><strong>H. INICIO</strong></th> 
                                    <th width="40px"><strong>SALA</strong></th>
                                    <th width="35px"><strong>CANT. ALUMNOS</strong></th>
                                    <th width="70px"><strong>CURSO | VERSIÓN</strong></th>
								    <th width="100px"><strong>EMPRESA</strong></th>
									<th width="35px"><strong>FICHA</strong></th>
									<th width="40px"><strong>DURACIÓN</strong></th>
									<th width="33px" ><strong>DIAS</strong></th>
									<th width="50px" ><strong>F. INICIO</strong></th>
									<th width="50px"><strong>F. FIN</strong></th>
									<th width="70px"><strong>RELATOR</strong></th>
                                    <th width="120px"><strong>OBSERVACIÓN</strong></th>
                                    <th width="90px"><strong>BREAK</strong></th>
								</tr>';
        for ($i = 0; $i < count($datos_arr); $i++) {
            $html .= '<tr>';
            $html .= '<td>' . $datos_arr[$i]['ini_hora'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['nombre'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['Alumnos'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['version'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['razon_social_empresa'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['num_ficha'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['horas_totales'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['dias'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['fecha_inicio'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['fecha_fin'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['Nombre_relator'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['descripcion'] . '</td>';
            $html .= '<td></td>';

            $html .= '</tr>';
        }
        $total = $i;

        $html .= '</table>
					</td>
					<p>Total cursos vigentes del día: ' . $total . '</p>
					</tr>

				  </table>';

        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Actividades Diarias_" . $now_consulta . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }


    function setGenerarpdfDiarioReserva($fecha_reporte, $externosReserva) {

        //  variables de sesion
        $GLOBALS['num_ficha'] = '0';
        $GLOBALS['codigo_sence'] = '0'; 
        $funciones = new Functions();
        $fecha_reporte = $funciones->formatDateBD($fecha_reporte);
        $arr_sesion = array(
            'fecha_reporte' => $fecha_reporte,
            'externosReserva' => $externosReserva,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos_arr = $this->modelo->get_arr_act_vig_diaria_reserva($arr_sesion);
        //var_dump(count($datos_arr));
        //die();
        if(count($datos_arr) == 0) {
            header('Location: ../../../../Error/Error?error=0');
        }


        //var_dump(count($datos_arr));
        // create new PDF document 
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetTitle('Recepción');
        $pdf->SetSubject('Tutorial TCPDF');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData($tc = array(
            0,
            64,
            0
                ), $lc = array(
            0,
            64,
            128
        ));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('Helvetica', '', 14, '', true);
        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L');
        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));

        $now_consulta = $datos_arr[0]['fecha_server'];
        $now_fecha = $datos_arr[0]['fecha_corta'];

        $html = '';
        $html .= '		
				<table width="100%">
					<tr>
						<th>
							<img src="' . base_url() . 'assets/amelia/images/logo_edutecno_login.png' . '" width="50px" >
						</th>
						<th>
							<table border="1" cellpadding="4" cellspacing="0" align="center">
        						<tr>
									<td>DEPARTAMENTO TÉCNICO</td>

        						</tr>
								
        						<tr>
									<td><p>Cursos día: ' . $now_fecha . '</p></td>
									
        						</tr>
							</table>
						</th>
						<th>
							<p align="right">Consulta de datos:<br>' . $now_consulta . '</p>
						</th>
					</tr>
					<tr><td></td><td>&nbsp;</td><td></td></tr>
					<tr>
						<td colspan="3">
							<table border="1" style="font-size:9px; padding:0px;" align="center">
								<tr>
						 
                                    <th width="60px"><strong>H. INICIO</strong></th> 
                                    <th width="40px"><strong>SALA</strong></th>
                                    <th width="35px"><strong>CANT. ALUMNOS</strong></th>
                                    <th width="70px"><strong>CURSO | VERSIÓN</strong></th>
								    <th width="100px"><strong>EMPRESA</strong></th>
									<th width="35px"><strong>FICHA</strong></th>
									<th width="40px"><strong>DURACIÓN</strong></th>
									<th width="33px" ><strong>DIAS</strong></th>
									<th width="50px" ><strong>F. INICIO</strong></th>
									<th width="50px"><strong>F. FIN</strong></th>
									<th width="70px"><strong>RELATOR</strong></th>
                                    <th width="120px"><strong>OBSERVACIÓN</strong></th>
                                    <th width="90px"><strong>BREAK</strong></th>
								</tr>';
        for ($i = 0; $i < count($datos_arr); $i++) {
            $html .= '<tr>';
            $html .= '<td>' . $datos_arr[$i]['ini_hora'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['nombre'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['Alumnos'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['version'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['razon_social_empresa'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['num_ficha'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['horas_totales'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['dias'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['fecha_inicio'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['fecha_fin'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['Nombre_relator'] . '</td>';
            $html .= '<td>' . $datos_arr[$i]['descripcion'] . '</td>';
            $html .= '<td></td>';

            $html .= '</tr>';
        }
        $total = $i;

        $html .= '</table>
					</td>
					<p>Total cursos vigentes del día: ' . $total . '</p>
					</tr>

				  </table>';

        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Actividades Diarias_" . $now_consulta . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }
}
