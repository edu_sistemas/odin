<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-12-26 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2016-12-26 [Jessica Roa] <jroa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Descuento_Producto';
    private $model = 'Descuento_Producto_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';
    private $package2 = 'back_office/Usuario';
    private $model2 = 'Usuario_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'modelousuario');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Descuento_Producto/Listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Descuento Producto";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['menus'] = $this->session->userdata('menu_usuario');

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listarDescuentoProducto() {

        $sub_linea_negocio_filtro = $this->input->post('sub_linea_negocio_filtro');

        if ($sub_linea_negocio_filtro == "" || $sub_linea_negocio_filtro == null) {

            $sub_linea_negocio_filtro = "";
        }

        $datos['sub_linea_negocio_filtro'] = $sub_linea_negocio_filtro;
        $datos['user_id'] = $this->session->userdata('id_user');
        $datos['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'sub_linea_negocio_filtro' => $datos['sub_linea_negocio_filtro'],
            'user_id' => $datos['user_id'],
            'user_perfil' => $datos['user_perfil']
        );

     
        $datos_res = $this->modelo->get_arr_listar_descuento_producto($arr_sesion);
        echo json_encode($datos_res);
    }

    function getLineaNegocio() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_linea_negocio($arr_sesion);
        echo json_encode($datos);
    }

    function getSubLineaNegocio() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_linea' => $this->input->post('id_linea')
        );
        $datos = $this->modelo->get_arr_sub_linea_negocio($arr_sesion);
        echo json_encode($datos);
    }

    function editarDescuento() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_descuentos_producto' => $this->input->post('id_descuentos_producto'),
            'sub_linea_negocio' => $this->input->post('sub_linea_negocio'),
            'valor_descuento' => $this->input->post('valor_descuento'),
            'fecha_inicio' => $this->input->post('fecha_inicio')
        );
        $datos = $this->modelo->post_arr_editar_descuento($arr_sesion);
        echo json_encode($datos);
    }

}
