<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class EncargadoRegistros extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'EncargadoRegistros';
    private $package = 'back_office/Dashboard';
    private $model = 'EncargadoRegistros_model';
    private $view = 'EncargadoRegistros_v';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

//  Libreria de sesion
        $this->load->library('session');
    }

// 2016-05-25
// Controlador del panel de control
    public function index() {

///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
        );


//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/d3/d3.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/c3/c3.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Dashboard/EncargadoRegistros.js')
        );

// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Home";

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }


    function Listar() {
        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'user_id' => $datos_menu ['user_id'],
            'user_perfil' => $datos_menu ['user_perfil']
        );

        $datos = $this->modelo->get_arr_listar_fichas_pendientes($arr_sesion);
        echo json_encode($datos);
    }












    // ESTA ES OTRA HISTORIA

    function getVentaMensual() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_consultar_venta_mensual($arr_data);
        echo json_encode($datos);
    }

    function getVentaAcumulada() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_consultar_venta_acumulada($arr_data);
        echo json_encode($datos);
    }

    function senMailtest() {

        $this->load->library('Libreriaemail');
        $asunto = "Test no considerar, favor eliminar   "; // asunto del correo 

        $body = "hola"; // mensaje completo HTML o text 
        // con copia a 
        $cc = array(
                    /* 
                        array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                        array('email' => 'chinojosa@edutecno.com', 'nombre' => 'César Hinojosa'),
                        array('email' => 'cespinoza@edutecno.com', 'nombre' => 'Cristian Espinoza'),
                        array('email' => 'luis.jarpa03@gmail.com', 'nombre' => 'Luis 2'), 
                        array('email' => 'fzapata@edutecno.com', 'nombre' => 'Fabian Zapata'),
                        array('email' => 'fbulboa@edutecno.com', 'nombre' => 'Felipe Bulboa') 
                        
                    */
                    );



        $AltBody = "Test no considerar, favor eliminar"; // hover del cursor sobre el correo
        //
        // destinos  
        $para = array(
                         array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
                     );


        // con copia oculta a
        $cco = array(
                        // array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos'),
                        // array('email' => 'ljarpa@edutecno.com', 'nombre' => 'Luis Jarpa')
                    );

        
       
        $res = $this->libreriaemail->Mailink($asunto, $body, $AltBody, $para, $cc, $cco);

        if ($res["resultado"] == 0) {
            echo $res["resultado"] . " " . $res["message"];
        }
        echo $res["resultado"] . " " . $res["message"];
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
