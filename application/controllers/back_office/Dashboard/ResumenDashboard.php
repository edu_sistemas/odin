<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-01-08 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-01-08 [Jessica Roa] <jroa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ResumenDashboard extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Dashboard - Supervisor';
    private $package = 'back_office/Dashboard';
    private $model = 'ResumenDashboard_model';
// informacion adicional para notificaciones       
    private $view = 'ResumenDashboard_v';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');       

//  Libreria de sesion
        $this->load->library('session');
    }

    public function index() {

///  array ubicacigetDatosSelectGruposEjecutivosones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
			array(
                'href' => base_url() . $this->ind . $this->package . '/ResumenDashboard',
                'label' => 'Dashboard - Supervisor',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/toastr/toastr.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dualListbox/bootstrap-duallistbox.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/daterangepicker/daterangepicker-bs3.css')
        );

//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/moment.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/toastr/toastr.min.js'), 
            array('src' => base_url() . 'assets/amelia/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/daterangepicker/daterangepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),	
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/Dashboard/ResumenDashboard.js')
			
        );

          
// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Dashboard Resumen";

        //$data['metas_globales'] = $this->getMetasGlobales();
        //$data['meta_mes']=$this->modelo->getMetasGlobales('meta_mes');
        //$data['sdaschboard_rubro'] = $this->getRubros();
        //$data['sdaschboard_lin_negocio'] = $this->getLineaNegocio();

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;      
        $this->load->view('amelia_1/template', $data);
    }


    // informacion adicional para notificaciones  

    // informacion adicional para notificaciones    
    function getNotificaciones (){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

    function getDatosSelectGruposEjecutivos() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'fecha' => $this->input->post('fecha')
        );

        $datos = $this->modelo->get_arr_datos_select_grupos_ejecutivos($arr_data);
        echo json_encode($datos);
    }    
    
    function getRubros() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'fecha' => $this->input->post('fecha')
        );
        $datos = $this->modelo->get_arr_rubros($arr_data);
        echo json_encode($datos);
    }   

    function getLineaNegocio() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'fecha' => $this->input->post('fecha')
        );
        $datos = $this->modelo->get_arr_linea_negocio($arr_data);
        echo json_encode($datos);
    }     
    
    function getEmpresas() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'fecha' => $this->input->post('fecha')
        );
        $datos = $this->modelo->get_arr_empresas($arr_data);
        echo json_encode($datos);
    }   

    function getVentaAcumulada() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_consultar_venta_acumulada($arr_data);
        echo json_encode($datos);
    }
}
/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */

