<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-03-06 [Felipe Bulboa] <fbulbo@edutecno.com>
 * Fecha creacion:  2017-03-06 [Felipe Bulboa] <fbulbo@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Backoffice extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Backoffice';
    private $package = 'back_office/Dashboard';
    private $model = 'Backoffice_model';
    private $view = 'Backoffice_v';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

//  Libreria de sesion
        $this->load->library('session');
    }

// 2016-05-25
// Controlador del panel de control
    public function index() {

///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
        );


//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/d3/d3.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/c3/c3.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Dashboard/backoffice.js')
        );

// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Home";

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu

// $apiUrl = 'http://indicadoresdeldia.cl/webservice/indicadores.json';
$apiUrl = 'https://mindicador.cl/api';
//Es necesario tener habilitada la directiva allow_url_fopen para usar file_get_contents
if (ini_get('allow_url_fopen')) {
    $json = json_decode(file_get_contents($apiUrl));
} else {
    //De otra forma utilizamos cURL
    $curl = curl_init($apiUrl);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $json = curl_exec($curl);
    curl_close($curl);
}


//UF
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://api.sbif.cl/api-sbifv3/recursos_api/uf?apikey=fd7ab711e75b9d1f9d12619a9fed91c7748db879&formato=json');
$uf = curl_exec($ch);
curl_close($ch);
$uf = json_decode($uf);

//Dolar
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://api.sbif.cl/api-sbifv3/recursos_api/dolar?apikey=fd7ab711e75b9d1f9d12619a9fed91c7748db879&formato=json');
$dolar = curl_exec($ch);
curl_close($ch);
$dolar = json_decode($dolar);

//UTM
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://api.sbif.cl/api-sbifv3/recursos_api/utm?apikey=fd7ab711e75b9d1f9d12619a9fed91c7748db879&formato=json');
$utm = curl_exec($ch);
curl_close($ch);
$utm = json_decode($utm);

//IPC
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://api.sbif.cl/api-sbifv3/recursos_api/ipc?apikey=fd7ab711e75b9d1f9d12619a9fed91c7748db879&formato=json');
$ipc = curl_exec($ch);
curl_close($ch);
$ipc = json_decode($ipc);

//CALIDAD DEL AIRE
$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://sinca.mma.gob.cl/index.php/json/listadomapa/');
$aire = curl_exec($ch);
curl_close($ch);
$aire = json_decode($aire);

//bloque geo local
$ip = $this->input->ip_address(); // the IP address to query
$query = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
if ($query && $query['status'] == 'success') {
    $query['city'] = strtolower($query['city']);
} else {
    $query['city'] = 'santiago';
}
//bloque geilocal
//bloque tiempo

$BASE_URL = "http://query.yahooapis.com/v1/public/yql";
$yql_query = 'select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="' . $query['city'] . ', cl") AND u="c"';
$yql_query_url = $BASE_URL . "?q=" . urlencode($yql_query) . "&format=json";
// Make call with cURL
$session = curl_init($yql_query_url);
curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
$jsonw = curl_exec($session);
// Convert JSON to PHP object
$phpObj = json_decode($jsonw);
$output = $phpObj;


//fin bloque tiempo
//gracias totales


$data['output'] = $output;
$data['json'] = $json;
$data['uf'] = $uf;
$data['dolar'] = $dolar;
$data['utm'] = $utm;
$data['ipc'] = $ipc;
$data['aire'] = $aire;

        $data['json'] = $json;
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }
}

 
