<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-01-08 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-01-08 [Jessica Roa] <jroa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class GeneralDashboard extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Dashboard General';
    private $package = 'back_office/Dashboard';
    private $model = 'GeneralDashboard_model';
// informacion adicional para notificaciones       
    private $view = 'GeneralDashboard_v';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');       

//  Libreria de sesion
        $this->load->library('session');
    }

    public function index() {

///  array ubicacigetDatosSelectGruposEjecutivosones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
			array(
                'href' => base_url() . $this->ind . $this->package . '/GeneralDashboard',
                'label' => 'Dashboard General',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/toastr/toastr.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dualListbox/bootstrap-duallistbox.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/daterangepicker/daterangepicker-bs3.css')
        );

//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/moment.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/toastr/toastr.min.js'), 
            array('src' => base_url() . 'assets/amelia/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/daterangepicker/daterangepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),	
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/Dashboard/GeneralDashboard.js')
			
        );

          
// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Dashboard General";

        //$data['metas_globales'] = $this->getMetasGlobales();
        //$data['meta_mes']=$this->modelo->getMetasGlobales('meta_mes');
        //$data['sdaschboard_rubro'] = $this->getRubros();
        //$data['sdaschboard_lin_negocio'] = $this->getLineaNegocio();

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;      
        $this->load->view('amelia_1/template', $data);
    }


    // informacion adicional para notificaciones  

    // informacion adicional para notificaciones    
    function getNotificaciones (){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

        function getDatosSelectEjecutivos() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'fecha' => $this->input->post('fecha')
        );

        $datos = $this->modelo->get_arr_datos_select_ejecutivos($arr_data);
        echo json_encode($datos);
    }   

       
    
    function getRubros() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'fecha' => $this->input->post('fecha')
        );
        $datos = $this->modelo->get_arr_rubros($arr_data);
        echo json_encode($datos);
    }   

    function getLineaNegocio() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'fecha' => $this->input->post('fecha')
        );
        $datos = $this->modelo->get_arr_linea_negocio($arr_data);
        echo json_encode($datos);
    }     
    
    function getEmpresas() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'fecha' => $this->input->post('fecha')
        );
        $datos = $this->modelo->get_arr_empresas($arr_data);
        echo json_encode($datos);
    }   

    function getVentaAcumulada() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_consultar_venta_acumulada($arr_data);
        echo json_encode($datos);
    }


    function generarExcelVentasEmpresas($fecha) {
        if($fecha == 0){
            $fecha = '';
        }

        $arr_datos = array(
            'fecha' => $fecha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_excel = $this->modelo->get_arr_ventas_empresas_excel($arr_datos);
        
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Ventas por Empresas');
        //set cell A1 content with some text

        $this->excel->getActiveSheet()->SetCellValue('A1', 'EMPRESA');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'EJECUTIVO');
        $this->excel->getActiveSheet()->SetCellValue('C1', '% MES '.$fecha);
        $this->excel->getActiveSheet()->SetCellValue('D1', '% ACUM.');
        $this->excel->getActiveSheet()->SetCellValue('E1', 'VENTAS MES');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'VENTAS ACUM.');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'RUBRO');
        $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');

        $this->excel->getActiveSheet()->getStyle('A1:G1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '#dbbae2')
                        
                    )
                )
        );
         $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('20');
         $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('10');
         $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('12');
         $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('8');
         $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('15');
         $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('28');
         $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('71');
        


        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = 'VentasPorEmpresa_' . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
        ob_clean();
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: private'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

     function generarExcelRankingEjecutivos( $fecha) {
            if($fecha == 0){
                $fecha = '';
            }
            
            $arr_datos = array(
                'fecha' => $fecha,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

           
           
            $this->load->library('excel');

            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Ranking Ejecutivos');

             $data_excel = $this->modelo->get_arr_datos_select_ranking_ejecutivos_excel($arr_datos);
            //set cell A1 content with some text

            $this->excel->getActiveSheet()->SetCellValue('A1', 'EJECUTIVO');        
            $this->excel->getActiveSheet()->SetCellValue('B1', '% MES');
            $this->excel->getActiveSheet()->SetCellValue('C1', '% ACUM.');
            $this->excel->getActiveSheet()->SetCellValue('D1', 'VENTA MES');
            $this->excel->getActiveSheet()->SetCellValue('E1', 'META MES');
            $this->excel->getActiveSheet()->SetCellValue('F1', 'VENTAS ACUM.');
            $this->excel->getActiveSheet()->SetCellValue('G1', 'META ACUM.');
            $this->excel->getActiveSheet()->SetCellValue('H1', 'META ANUAL');
            
            $contador = count($data_excel);
            $numero = 2;

            for ($i=0; $i <$contador ; $i++) { 
                $this->excel->getActiveSheet()->SetCellValue('A' . $numero, $data_excel[$i]['ejecutivo']); 
                $this->excel->getActiveSheet()->SetCellValue('B' . $numero, $data_excel[$i]['porcenmes']); 
                $this->excel->getActiveSheet()->SetCellValue('C' . $numero, $data_excel[$i]['porcenacum']); 
                $this->excel->getActiveSheet()->SetCellValue('D' . $numero, $data_excel[$i]['venta_mes']); 
                $this->excel->getActiveSheet()->SetCellValue('E' . $numero, $data_excel[$i]['meta_mes']); 
                $this->excel->getActiveSheet()->SetCellValue('F' . $numero, $data_excel[$i]['venta_acum']); 
                $this->excel->getActiveSheet()->SetCellValue('G' . $numero, $data_excel[$i]['meta_acum']); 
                $this->excel->getActiveSheet()->SetCellValue('H' . $numero, $data_excel[$i]['meta_anual']); 
                
                $numero ++;
            }
            $this->excel->getActiveSheet()->getStyle('A1:H1')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '#6a81a5')
                        )
                    )
                );

            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth('12');   
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth('12');   
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('14');   
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('14');
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('14');
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('12');
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('12');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('28');
        
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $filename = 'RankingEjecutivos_' . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
            ob_clean();
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: private'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }
}


/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */

