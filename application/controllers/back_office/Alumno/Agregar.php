<?php

/**
* Agregar
*
* Description...
*
* @version 0.0.1
*
* Ultima edicion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
* Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Alumno';
    private $model = 'Alumno_model';
    private $view = 'Agregar_v';
    private $controller = 'Agregar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de sesion
        $this->load->library('session');
        $this->load->library('Functions');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
             array(
                'href' => base_url() . $this->ind .'/back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/chosen/chosen.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
            
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Alumno/agregar.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
		    $data['activo'] = "Alumnos";
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;



        $this->load->view('amelia_1/template', $data);
    }

    function AgregarAlumno() {
        $funciones = new Functions();
        //$fecha_nacimiento = $this->input->post('fecha_nacimiento');
        //$fecha_nacimiento = $funciones->formatDateBD($fecha_nacimiento);
            
        $arr_input_data = array(
            'rut_alumno'               => $this->input->post('rut_alumno_v'),
            'dv_alumno'                => $this->input->post('dv_alumno_v'),
            'usuario_alumno'           => $this->input->post('usuario_alumno_v'),
            'nombre_alumno'            => $this->input->post('nombre_alumno_v'),
            'seg_nombre_alumno'        => $this->input->post('seg_nombre_alumno_v'),
            'apellido_paterno_alumno'  => $this->input->post('apellido_paterno_alumno_v'),
            'apellido_materno_alumno'  => $this->input->post('apellido_materno_alumno_v'),
            'fecha_nacimiento' => $this->input->post('fecha_nacimiento'),
            'genero'  => $this->input->post('genero'),
            'telefono_alumno_v'  => $this->input->post('telefono_alumno_v'),
            'correo_alumno'  => $this->input->post('correo_alumno'),
            'nivel_educacional'  => $this->input->post('nivel_educacional'),
            'cargo_alumno_v'  => $this->input->post('cargo_alumno_v'),
            'profesion_alumno_v'  => $this->input->post('profesion_alumno_v'),
            'direccion_alumno_v'  => $this->input->post('direccion_alumno_v')
            
        );


        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_agregar_alumno($arr_input_data);

        echo json_encode($respuesta);
    }

    function getGeneroCBX(){
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->get_arr_genero_alumno($arr_input_data);

        echo json_encode($respuesta);
    }

    function getNivelEducacionalCBX(){
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->get_arr_nivel_educacional_alumno($arr_input_data);

        echo json_encode($respuesta);
    }

    

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
