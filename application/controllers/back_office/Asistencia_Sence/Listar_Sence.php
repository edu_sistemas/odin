<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-06-27 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-06-27 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar_Sence extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar';
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Asistencia_Sence';
    private $model = 'Listar_Sence_model';
    private $view = 'Listar_Sence_v';
    private $controller = 'Listar_Sence';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2017-01-11
    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );



        // array con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Asistencia_Sence/listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Asistencia Sence";
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function getFichas() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_ficha_reserva_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function alumnosListar() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
//
        $datos = $this->modelo->get_arr_listar_alumnos($arr_input_data);
        echo json_encode($datos);
    }

    function fichasListar() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha')
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
//
        $datos = $this->modelo->get_arr_listado_ficha_sence($arr_input_data);
        echo json_encode($datos);
    }

    function alumnosRegistrarLista() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $input = $_POST['numRows'];
        $largo = $this->input->post('largo');

        $arr_asistencia = array();
        $arr_id_asis = array();
        $arr_dtla = array();

        if ($largo > 0) {
            for ($var = 0; $var < $input; $var++) {
                //update
                array_push($arr_asistencia, $_POST['por' . $var]);
                array_push($arr_id_asis, $_POST['id_asis' . $var]);
            }
        } else {
            for ($var = 0; $var < $input; $var++) {
                //nueva insert
                array_push($arr_asistencia, $_POST['por' . $var]);
                //	array_push($arr_id_asis, $_POST['id_asis' . $var]);
            }
        }

        for ($var = 0; $var < $input; $var++) {
            array_push($arr_dtla, $_POST['dtl' . $var]);
        }
//                 
        $arr_input_data = array(
            'asistencia' => $arr_asistencia,
            'id_asistencia' => $arr_id_asis,
            'numRows' => $this->input->post('numRows'),
            'id_alm' => $arr_dtla,
            'largo' => $largo
        );

        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

//	 
//		var_dump($arr_input_data);
//		die();
//
        $datos = $this->modelo->set_arr_registrar_asistencia($arr_input_data);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
?>