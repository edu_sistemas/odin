<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-31 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-01-31 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Clase extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Clase';
    private $nombre_item_plural = 'Clase';
    private $package = 'back_office/Curso';
    private $model = 'Clase_model';
    private $view = 'Clase_v';
    private $controller = 'Clase';
    private $ind = '';
    private $ficha = 'back_office/Ficha/Ficha_model';
    private $fichacontrolmodel = 'back_office/Ficha_Control/Ficha_Control_model';

    function __construct() {
        parent::__construct();
        // Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->ficha, 'modeloficha');
        $this->load->model($this->fichacontrolmodel, 'fichacontrol');

        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del Agregar
    public function index() {
        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/toastr/toastr.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/toastr/toastr.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Curso/clase.js')
        );

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////
        $datos_menu ['user_id'] = $this->session->userdata('id_user');
        $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['activo'] = "Asignar Relator";
        if (!in_array_r($data ['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        // Carga Menu dentro de la pagina
//        $data['contenido'] = $this->modelo->get_arr_reserva_by_id($arr_sesion);        
        $data ['menus'] = $this->session->userdata('menu_usuario');
        // / fin carga menu

        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);

        // cargo el menu en la session
    }

    function getFichas() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_ficha_curso_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function claseListar() {
        //
        // // Ultima fecha Modificacion: 2017-31-01
        // // Descripcion: Metodo para listar los datos de la clase
        // // Usuario Actualizador : Marcelo Romero
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_listar_clase($arr_sesion);
        echo json_encode($datos);
    }

    function listarRelatoresDisponibles() {

        /*
         * Ultima fecha Modificacion: 2017-02-01
         * Descripcion: Metodo para Cargar Los docentes disponibles paraasignar al curso
         * Usuario Actualizador : Marcelo Romero
         *
         */
        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        // llamado al modelo
        $respuesta = $this->modelo->get_arr_listar_docente_disponible($arr_input_data);

        // respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function listarRelatoresAsignados() {

        /*
         * Ultima fecha Modificacion: 2017-02-02
         * Descripcion: Metodo para Cargar Los docentes disponibles paraasignar al curso
         * Usuario Actualizador : Marcelo Romero
         *
         */
        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        // llamado al modelo
        $respuesta = $this->modelo->get_arr_listar_docente_asignado($arr_input_data);

        // respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function AsignardocenteClase() {

        /*
         * Ultima fecha Modificacion: 2016-11-09
         * Descripcion: Metodo para dar permisos a los perfiles
         * Usuario Actualizador : Marcelo Romero
         */
        $respuesta = "";

        $id_usuario = $this->input->post('relatores_asignados_visible');
        $ficha = $this->input->post('id_ficha');

        for ($i = 0; $i < count($id_usuario); $i ++) {

            $arr_input_data = array(
                'id_usuario' => $id_usuario [$i],
                'id_ficha' => $ficha
            );

            $arr_input_data ['user_id'] = $this->session->userdata('id_user');
            $arr_input_data ['user_perfil'] = $this->session->userdata('id_perfil');

            $respuesta = $this->modelo->set_arr_asignar_docenete_clase($arr_input_data);
        }

        echo json_encode($respuesta);
    }

    function QuitarAsignacionDocente() {

        /*
         * Ultima fecha Modificacion: 2017-02-03
         * Descripcion: Método para quitar o eliminar los docentes de allguna clase.
         * Usuario Actualizador : Marcelo Romero
         */
        $id_usuario = $this->input->post('relatores_asignados_visible');
        $ficha = $this->input->post('id_ficha');

        for ($i = 0; $i < count($id_usuario); $i ++) {

            $arr_input_data = array(
                'id_usuario' => $id_usuario [$i],
                'id_ficha' => $ficha
            );

            $arr_input_data ['user_id'] = $this->session->userdata('id_user');
            $arr_input_data ['user_perfil'] = $this->session->userdata('id_perfil');

            $respuesta = $this->modelo->set_arr_remover_docente($arr_input_data);
        }

        // respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function confirmarEstado() {

        /*
         * Ultima fecha Modificacion: 2017-02-03
         * Descripcion: Metodo para confirmar los docentes y no se puedan eliminar
         * Usuario Actualizador : Marcelo Romero
         *
         */
        $arr_input_data = array(
            'id_ficha' => $this->input->post('id_ficha')
        );

        $arr_input_data ['user_id'] = $this->session->userdata('id_user');
        $arr_input_data ['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_confirmar_docente_estado($arr_input_data);

        echo json_encode($respuesta);
    }

    function datosReserva() {
        /*
         * Ultima fecha Modificacion: 2017-02-03
         * Descripcion: Metodo para confirmar los docentes y no se puedan eliminar
         * Usuario Actualizador : Marcelo Romero
         *
         */

        $id_ficha = $this->input->post('id_ficha');

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $respuesta = $this->modelo->get_arr_reserva_by_id($arr_input_data);
        echo json_encode($respuesta);
    }

    function getOcByFichaCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeloficha->get_arr_listar_oc_by_ficha_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getIDSenceByFicha() {
        // carga el combo box Holding
        $arr_sesion = array(
            'id_ficha' => $this->input->post('id_ficha'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeloficha->getIDSenceByFicha($arr_sesion);
        echo json_encode($datos);
    }

    function setIDAccion() {
        $id_ficha = $this->input->post('id_ficha');
      //  $tipo_codigo = $this->input->post('tipo_codigo');
        $id_accion = $this->input->post('id_accion');

        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'id_accion' => trim($id_accion),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

            $datos = $this->modeloficha->set_arr_ingresa_id_accion_sence($arr_data);

        echo json_encode($datos);
    }

    function delIDAccion(){
        
        $id_ficha = $this->input->post('id_ficha');
      //  $tipo_codigo = $this->input->post('tipo_codigo');
        $id_accion = $this->input->post('id_ingresadas');
        
        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'id_accion' => trim($id_accion),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeloficha->del_arr_ingresa_id_accion_sence($arr_data); // cual
        echo json_encode($datos);
    }

    function setIDAccionConsolidado() {


        $id_accion_consolidado = $this->input->post('id_accion_consolidado');
        $id_ficha_consolidado = $this->input->post('id_ficha_consolidado');

        $arr_data = array(
            'id_ficha_consolidado' => $id_ficha_consolidado,
            'id_accion_consolidado' => trim($id_accion_consolidado),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modeloficha->set_id_accion_sence_consolidado($arr_data);
        echo json_encode($datos);
    }

    function pdfResumen($id) {
        $id_ficha = $id;

        $arr_input_data = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_ficha = $this->fichacontrol->get_arr_ficha_by_id_resumenFC($arr_input_data);

        $data_orden_compra = $this->fichacontrol->get_arr_orden_compra_by_ficha_idFC($arr_input_data);

        $arr_dias = explode(',', $data_ficha[0]['id_dias']);
        //var_dump($data_ficha);
        $dias = '';
        for ($y = 0; $y < count($arr_dias); $y++) {
            if ($y != 0) {
                $dias .= '-';
            }
            switch ($arr_dias[$y]) {
                case '1':
                    $dias .= 'L';
                    break;
                case '2':
                    $dias .= 'Ma';
                    break;
                case '3':
                    $dias .= 'Mi';
                    break;
                case '4':
                    $dias .= 'J';
                    break;
                case '5':
                    $dias .= 'V';
                    break;
                case '6':
                    $dias .= 'S';
                    break;
                case '7':
                    $dias .= 'D';
                    break;
            }
        }

        $this->load->library('Pdf');
        $pdf = new pdf('L', 'mm', 'LETTER');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins(15, 15, 15, true);

        // set auto page breaks false
        $pdf->SetAutoPageBreak(TRUE, 0);

        $pdf->AddPage('P', 'A4');
        $pdf->SetFont('times', '', 11);
        $html = '<style>
					td{
					padding-left: 3px;
					height:16px;
					}
				</style>';
        $html .= '
					<table border="1" width="81%">
						<tr>
							<td rowspan="2" width="200px"><img width="180px" style="margin-top: 5px;" src="' . base_url() . '/assets/img/logoEdutecno2.png"></td><td colspan="3" height="55px" style="font-size:20px;text-align:center"><strong>FICHA: ' . $data_ficha[0]['num_ficha'] . '</strong></td>
						</tr>
						<tr style="font-size:8px">
							<td style="font-size:8px">FECHA ENVIO SOLICITUD</td><td colspan="2">PERSONA QUE SOLICITA</td>
						</tr>
						<tr>
							<td style="text-align:center">Organismo Técnico de Capacitación</td><td>&nbsp;' . $data_ficha[0]['fecha_solicitud'] . '</td><td colspan="2">&nbsp;' . $data_ficha[0]['ejecutivo'] . '</td>
						</tr>
					</table>
		';
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">HOLDING&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['holding'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">EMPRESA&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['empresa'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">RUT&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['rut_empresa'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">DIRECCIÓN&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['direccion_empresa'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">NOMBRE CONTACTO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['nombre_contacto'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">TELÉFONO CONTACTO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['telefono_contacto'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">DIRECCIÓN ENVÍO DIPLOMAS&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['direccion_diplomas'] . '</td>
						</tr>
					</table>
		';
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">RELATOR&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['relator'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">TELÉFONO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['telefono_relator'] . '</td>
						</tr>						
					</table>
		';
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">CURSO&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['descripcion_producto'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">VERSIÓN&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['version'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">MODALIDAD&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['modalidad'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">TIPO CURSO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['modalidad'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">CÓDIGO SENCE&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['codigo_sence'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">FECHA INICIO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['fecha_inicio'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">FECHA TÉRMINO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['fecha_fin'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">HORARIO&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['hora_inicio'] . ' - ' . $data_ficha[0]['hora_termino'] . '</td>
						</tr>						
					</table>
		';
        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">SEDE&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['sede'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">SALA&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['sala'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">DIRECCIÓN&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['lugar_ejecucion'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">DÍAS&nbsp;&nbsp;</td>
							<td>&nbsp;' . $dias . '</td>
						</tr>
												
					</table>
		';

        $html .= '	<table><tr><td>&nbsp;</td></tr></table>
					<table border="1" width="100%">
						<tr style="font-size:10px;">
							<td width="30%" style="text-align:right;background-color:#CECECE;font-size:10px">EJECUTIVO&nbsp;&nbsp;</td>
							<td width="70%">&nbsp;' . $data_ficha[0]['ejecutivo'] . '</td>
						</tr>
						<tr>
							<td style="text-align:right;background-color:#CECECE;font-size:10px">OBSERVACIONES&nbsp;&nbsp;</td>
							<td>&nbsp;' . $data_ficha[0]['comentario_orden_compra'] . '</td>
						</tr>
						
												
					</table>
		';


        $html .= '<table><tr><td>&nbsp;</td></tr></table>
				  <table><tr><td>Listado de Participantes</td></tr></table>
				  <table><tr><td>&nbsp;</td></tr></table>
				  <table border="1">
				  	<tr>
						<td width="10%" style="background-color:#CECECE;font-size:10px"><strong>&nbsp;&nbsp;N</strong></td>
						<td width="30%" style="background-color:#CECECE;font-size:10px"><strong>RUT</strong></td>
						<td width="40%" style="background-color:#CECECE;font-size:10px"><strong>NOMBRE ALUMNO</strong></td>															
						<td width="20%" style="background-color:#CECECE;font-size:10px"><strong>O.C.</strong></td>						
					</tr>';

        $arr_data = array(
            'id_ficha' => trim($id_ficha),
            'orden_compra' => trim($data_orden_compra[0]['id']),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        $z = 1;
        
        // for ($x = 0; $x < count($data_orden_compra); $x++) {
            $arr_data = array(
                'id_ficha' => trim($id_ficha),
                'orden_compra' => "",
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $datos = $this->fichacontrol->get_arr_listar_datos_orden_compraFC($arr_data);

            for ($i = 0; $i < count($datos); $i++) {

                $html .= '  <tr>
								<td>&nbsp;&nbsp;' . $z . '</td>
								<td>' . $datos[$i]['rut'] . '</td>
								<td>' . $datos[$i]['nombre'] . '</td>								
								<td>' . $datos[$i]['num_orden_compra'] . '</td>
									
							  </tr>	';
                $z++;
            }
        //}
        $html .= '</table>';

        $pdf->writeHTML($html, true, false, true, false, '');

        //Close and output PDF document
        $pdf->Output('Resumen.pdf', 'I');
    }

    function generarExcel($id_ficha) {

        if ($id_ficha == 0) {
            $id_ficha = '';
        }

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data_exel = $this->modelo->get_arr_listar_clase_excel($arr_datos);




        $this->load->library('excel');

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Cursos Activos');
        //set cell A1 content with some text



        $this->excel->getActiveSheet()->SetCellValue('A1', 'DIAS');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'FICHA');
        $this->excel->getActiveSheet()->SetCellValue('C1', 'HRA INI');
        $this->excel->getActiveSheet()->SetCellValue('D1', 'HRA TER');
        $this->excel->getActiveSheet()->SetCellValue('E1', 'OTIC');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'EMPRESA');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'FECH INI REAL');
        $this->excel->getActiveSheet()->SetCellValue('H1', 'FECH TER REAL');
        $this->excel->getActiveSheet()->SetCellValue('I1', 'CODIGO');
        $this->excel->getActiveSheet()->SetCellValue('J1', 'CURSO');
        $this->excel->getActiveSheet()->SetCellValue('K1', 'HORAS REAL');
        $this->excel->getActiveSheet()->SetCellValue('L1', 'Instructor');
        $this->excel->getActiveSheet()->SetCellValue('M1', 'OBSERVACIONES');
        $this->excel->getActiveSheet()->SetCellValue('N1', 'NOMBRE REAL');
        $this->excel->getActiveSheet()->SetCellValue('O1', 'Sala');
        $this->excel->getActiveSheet()->SetCellValue('P1', 'N° de alumno');
        $this->excel->getActiveSheet()->SetCellValue('Q1', 'SENCENET');
        $this->excel->getActiveSheet()->SetCellValue('R1', 'CONSOLIDADO');
        $this->excel->getActiveSheet()->SetCellValue('S1', 'FICHAS RELACIONADAS');
        $this->excel->getActiveSheet()->SetCellValue('T1', 'CURSOS ACTIVOS');
        $this->excel->getActiveSheet()->SetCellValue('U1', 'TIPO CURSO');
        $this->excel->getActiveSheet()->fromArray($data_exel, null, 'A2');

        $this->excel->getActiveSheet()->getStyle('A1:U1')->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => '#afddb2')
                    )
                )
        );

        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
        //make the font become bold
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $filename = 'CursosActivos_' . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
        ob_clean();
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
        header('Cache-Control: private'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>