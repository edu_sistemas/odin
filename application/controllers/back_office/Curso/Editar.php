<?php

/**
 * Editar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-05 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-12-05 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Listar';
    private $nombre_item_plural = 'Listar';
    private $package = 'back_office/Curso';
    private $modalidad = 'back_office/Modalidad/Modalidad_model';
    private $nivel = 'back_office/Nivel/Nivel_model';
    private $version = 'back_office/Version/Version_model';
    private $familia = 'back_office/Familia/Familia_model';
    private $sence = 'back_office/Code_sence/Code_sence_model';
    private $sublinea = 'back_office/Negocio/Negocio_model';
    private $model = 'Curso_model';
    private $view = 'Editar_v';
    private $controller = 'Editar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->modalidad, 'modelmodalidad');
        $this->load->model($this->nivel, 'modelnivel');
        $this->load->model($this->version, 'modelversion');
        $this->load->model($this->familia, 'modelfamilia');
        $this->load->model($this->sence, 'modelsence');

        $this->load->model($this->sublinea, 'sublineanegocio');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del Editar Perfil
    public function index($id_perfil = false) {

        //  si entra a la mala lo redirige al listar
        if ($id_perfil == false) {
            redirect($this->package . '/Listar', 'refresh');
        }
        //  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Editar',
                'label' => 'Editar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Curso/editar.js')
        );


        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Curso";



        $arr_input_data = array(
            'id' => $id_perfil,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data['data_curso'] = $this->modelo->get_arr_curso_by_id($arr_input_data);
        if ($data['data_curso'] == false) {
            redirect($this->package . '/Listar', 'refresh');
        }

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        //   Carga Menu dentro de la pagina
        //mantener en todos los codigos
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function EditarCurso() {
        /*
          Ultima fecha Modificacion: 2016-11-25
          Descripcion:  función para modificar los módulos que ya existen
          Usuario Actualizador : Marcelo Romero
         */

        $array_version = $this->input->post('cbx_version');
        $id_curso = $this->input->post('id_curso_v');

        $arr_input_data = array(
            'id_curso' => $id_curso,
            'nombre_curso' => $this->input->post('nombre_curso_v'),
            'descripcion_curso' => $this->input->post('descripcion_curso_v'),
            'duracion_curso' => $this->input->post('duracion_curso_v'),
            'id_codigo_sence' => $this->input->post('cbx_codigo_sence'),
            'id_modalidad' => $this->input->post('cbx_modalidad'),
            'id_familia' => $this->input->post('cbx_familia'),
            'id_nivel' => $this->input->post('cbx_nivel'),
            'sub_linea_negocio' => $this->input->post('cbx_sub_linea_negocio'),
            'id_grado_bono' => $this->input->post('cbx_slt_grado_curso')
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_editar_curso($arr_input_data);


        $this->modelversion->set_arr_delete_version_curso($arr_input_data);


        for ($i = 0; $i < count($array_version); $i++) {


            $arr_input_data_version = array(
                'id_curso' => $id_curso,
                'id_version' => $array_version[$i],
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

            $this->modelversion->set_arr_insert_version_curso($arr_input_data_version);
        }

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function getModalidad() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelmodalidad->get_arr_modalidad_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getNivel() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelnivel->get_arr_listar_nivel_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getVersion() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        //  $this->load->model($this->version, 'modelversion');
        $datos = $this->modelversion->get_arr_listar_version_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getFamilia() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelfamilia->get_arr_listar_familia_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getCodigoSence() {

        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelsence->get_arr_codigo_sence_by_cursoCBX_curso($arr_sesion);
        echo json_encode($datos);
    }

    function getSubLineaNegocio() {

        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->sublineanegocio->get_arr_listar_sub_lineas_negocio($arr_sesion);
        echo json_encode($datos);
    }

    function getGradoBono(){
        $datos = $this->modelo->get_grado_bonos();
        echo json_encode($datos);
    }

}

/* End of file Editar.php */
/* Location: ./application/controllers/Editar.php */
?>
