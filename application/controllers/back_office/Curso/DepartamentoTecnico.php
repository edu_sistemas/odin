<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class DepartamentoTecnico extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'DepartamentoTecnico';
    private $nombre_item_plural = 'DepartamentoTecnico';
    private $package = 'back_office/Curso';
    private $model = 'DepartamentoTecnico_model';
    private $view = 'DepartamentoTecnico_v';
    private $controller = 'DepartamentoTecnico';
    private $ind = '';
    private $holding = 'back_office/Holding/Holding_Model';
    private $empresa = 'back_office/Empresa/Empresa_model';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->holding, 'modelholding');
        $this->load->library('session');
        $this->load->library('Functions');
    }

    public function index() {
        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/DepartamentoTecnico',
                'label' => 'Documentos Departamento Tecnico',
                'icono' => ''
            )
        );

        // array con los css necesarios para la pagina -- Informe Técnico
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Curso/DepartamentoTecnico.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js')
        );


        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');


        $data['activo'] = "Informe Técnico";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data ['menus'] = $this->session->userdata('menu_usuario');

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////
        // / fin carga menu
        //$data ['fichas'] = $this->modelo->getFichas ();
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function fichas() {

        $funciones = new Functions();
        $fecha_reporte = $this->input->post('fecha_reporte');
        $fecha_reporte = $funciones->formatDateBD($fecha_reporte);
        $arr_sesion = array(
            'fecha_reporte' => $fecha_reporte,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_curso_buscar($arr_sesion);
        echo json_encode($datos);
    }

    function Empresas() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresaByHoldingCBX() {
        // carga el combo box Holding

        $arr_input_data['id_holding'] = $this->input->post('id_holding');
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');
        $datos = $this->modelempresa->get_arr_listar_empresa_by_holding_cbx($arr_input_data);
        echo json_encode($datos);
    }

    function getHoldingCBX() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelholding->get_arr_listar_holding_cbx($arr_sesion);
        echo json_encode($datos);
    }

    public function generar() {

        $funciones = new Functions();
        $num_ficha = $this->input->post('num_num_ficha');
        $empresa = $this->input->post('id_empresa');

        $start = $this->input->post('fecha_start');
        $end = $this->input->post('fecha_end');

        $start = $funciones->formatDateBD($start);
        $end = $funciones->formatDateBD($end);

        //  variables de sesion
        $arr_datos = array(
            'num_ficha' => $num_ficha,
            'empresa' => $empresa,
            'fecha_inicio' => $start,
            'fecha_termino' => $start,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $dataInforme = $this->modelo->getDatosInforme($arr_datos);
        // $dataAlumnos = $this->modelo->getAlumnosInforme($arr_datos);
// 		var_dump($dataAlumnos);
// 		die();
        $GLOBALS ['num_ficha'] = $dataInforme[0]["num_ficha"];

        $id_ficha = $dataInforme[0]["id_ficha"];

        $this->load->library('Pdf');
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, 'mm', 'A4', true, 'UTF-8', false, false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Edutecno Capacitación');
        $pdf->SetTitle('DEPARTAMENTO TECNICO-Actividades Diarias');
        $pdf->SetSubject('Actividades Diarias');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config_alt.php de libraries/config

        $PDF_HEADER_LOGO = "logoEdutecno.png"; //any image file. check correct path.
        $PDF_HEADER_LOGO_WIDTH = "20";
        $PDF_HEADER_TITLE = "";
        $PDF_HEADER_STRING = "";
        //$pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);
        $pdf->SetHeaderData($PDF_HEADER_LOGO, $PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE . ' ', $PDF_HEADER_STRING, array(0, 0, 0), array(0, 64, 128));
// 		$pdf->setFooterData ( $tc = array (
// 				0,
// 				64,
// 				0 
// 		), $lc = array (
// 				0,
// 				64,
// 				128 
// 		) );
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // datos por defecto de cabecera, se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(
            PDF_FONT_NAME_DATA,
            '',
            PDF_FONT_SIZE_DATA
        ));

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP - 18, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // se pueden modificar en el archivo tcpdf_config.php de libraries/config
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // relación utilizada para ajustar la conversión de los píxeles
        // $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // ---------------------------------------------------------
        // establecer el modo de fuente por defecto
        $pdf->setFontSubsetting(true);

        // Establecer el tipo de letra
        // Si tienes que imprimir carácteres ASCII estándar, puede utilizar las fuentes básicas como
        // Helvetica para reducir el tamaño del archivo.
        $pdf->SetFont('Helvetica', '', 14, '', true);

        // Añadir una página
        // Este método tiene varias opciones, consulta la documentación para más información.
        $pdf->AddPage('L');

        // fijar efecto de sombra en el texto
        $pdf->setTextShadow(array(
            'enabled' => true,
            'depth_w' => 0.2,
            'depth_h' => 0.2,
            'color' => array(
                196,
                196,
                196
            ),
            'opacity' => 1,
            'blend_mode' => 'Normal'
        ));


        // Establecemos el contenido para imprimir
        // $ficha = $this->input->post('fichas');
        // $fichas = $this->modelo->getFichas();
        // foreach($fichas as $ficha)
        // {
        // $prov = $ficha['num_ficha'];
        // }
        // echo json_encode($dataInforme;
        // preparamos y maquetamos el contenido a crear
        $html = '';
        $html .= '
				<style>
				.main-title{
					font-size: 110%;
					font-weight: 600;
					text-align: center;
				}
				.logo{
					height: 30px;
				}
				.derechized{
					text-align: right;
					font-size: 60%;
				}
				.principal{
					font-size: 9px;
					padding-left: 5px;
					padding-top: 1px;
					padding-bottom: 2px;
				}
                               
				.principal th{
				border-bottom: 2px solid black;
				height: 5px;
				}
				.principal td{
				/*border: 0.1px solid black;*/
				/*width: 40px;*/
				}
				.principal2{
					font-size: 9px;
					 
					padding-left: 5px;
					padding-top: 1px;
					padding-bottom: 1px;
                                        
                                        
				width: 95%;
				}
				.principal2 tr{
					height: 100px;
				}
				
				.cuadrado{
					border: 0.1px solid black; 
					width: 12px;
					height: 3px;
				}
				.nada{
					width: 5px;
				
				}
				.item{
				width: 50%;
				}
				.texto-nada{
					width: 80px;
				font-size: 80%;
				text-align: left;
				
				}
				</style>
				<table>
					<tr><td><img class="logo" src="' . base_url() . 'assets/img/logoEdutecno2.png" /></td><td class="main-title">DEPARTAMENTO TECNICO</td><td>
						<table class="derechized">
							<tr><td><b>Hora Consulta: </b>' . $dataInforme[0]["hora_consulta"] . '</td></tr>
							<tr><td>' . strftime("%A %d de %B del %Y") . '</td></tr>
						</table>
					</td></tr>
				  </table>
									
					<table class="principal"  >
						<tr style="height: 5px; text-align: center; margin-top: 500px;">
                                                    <th style="width:30px;">Ini</th>
                                                    <th style="width:30px;">Ter</th>
                                                    <th style="width:35px;">Fich</th>
                                                    <th style="width:40px;">Dias</th>
                                                    <th style="width:50px;">Empresa</th>
                                                    <th style="width:48px;">F.In</th>
                                                    <th style="width:48px;">F.Tr</th>
                                                    <th style="width:25px;">Hrs</th>
                                                    <th style="width:60px;">Relator</th>
                                                    <th style="width:90px;">Curso</th>
                                                    <th style="width:45px;">Sala</th>
                                                    <th style="width:50px;">N° A</th> 
                                                    <th style="width:75px;">Observaciones</th>
                                                    <th style="width:50px;">Revisión</th>
                                                    <th style="width:80px;">Rev Dpto Tec</th>
						</tr>';


        for ($i = 0; $i < count($dataInforme); $i++) {
            $html .= '     
						<hr>	
						<tr style="text-align: center">
							<td style="font-size:80%;">' . $dataInforme[$i]["hora_inicio"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["hora_termino"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["num_ficha"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["dias"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["nombre_empresa"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["fecha_inicio"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["fecha_termino"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["duracion_curso"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["nombre_relator"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["nombre_curso"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["sala"] . '</td>
							<td style="font-size:80%;">' . $dataInforme[$i]["cant_alumno"] . '</td>
                                                        <td style="font-size:80%;"></td>
							<td style="font-size:80%;"></td>
							<td style="font-size:80%;"></td>
						</tr> 	
        
        
        
                              
                                        <tr>  

                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                                <td style=""></td>
                                        </tr>
                                        <tr>

                                                <td style=" ">
                                                        <table class="principal2">
                                                                <tr><td></td><td></td>
                                                                <td class="cuadrado"></td><td class="texto-nada">Aire acondic.</td>
                                                                <td class="cuadrado"></td><td class="texto-nada">Sillas</td>
                                                                <td class="cuadrado"></td><td class="texto-nada">Mesas</td>
                                                                <td class="cuadrado"></td><td class="texto-nada">PC</td>
                                                                <td class="cuadrado"></td><td class="texto-nada">Monitores</td>
                                                                <td class="cuadrado"></td><td class="texto-nada">Programas</td>
                                                                <td class="cuadrado"></td><td class="texto-nada">Archivos</td></tr>
                                                        </table>	
                                                </td>

                                        </tr>';
        }
        $html .= '	</table> 
				';



        // $html = '';
        // $html .= "<style type=text/css>";
        // $html .= "th{color: #fff; font-weight: bold; background-color: #222}";
        // $html .= "td{background-color: #AAC7E3; color: #fff}";
        // $html .= "</style>";
        // $html .= "<h2>Localidades de ".$prov."</h2><h4>Actualmente: ".count($ficha)." localidades</h4>";
        // $html .= "<table width='100%'>";
        // $html .= "<tr><th>Id localidad</th><th>Localidades</th></tr>";
        // provincias es la respuesta de la función getProvinciasSeleccionadas($provincia) del modelo
        // $html .= "</table>";
        // Imprimimos el texto con writeHTMLCell()
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        $pdf->Cell(0, 0, '', 0, 1, 'C', 0, '', 4);
        // ---------------------------------------------------------
        // Cerrar el documento PDF y preparamos la salida
        // Este método tiene varias opciones, consulte la documentación para más información.
        $nombre_archivo = utf8_decode("Localidades de " . $id_ficha . ".pdf");
        $pdf->Output($nombre_archivo, 'I');
    }
}
