<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-20-01 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-01-12 [Marcelo Romero] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Curso';
    private $modalidad = 'back_office/Modalidad/Modalidad_model';
    private $nivel = 'back_office/Nivel/Nivel_model';
    private $version = 'back_office/Version/Version_model';
    private $familia = 'back_office/Familia/Familia_model';
    private $sence = 'back_office/Code_sence/Code_sence_model';
    private $sublinea = 'back_office/Negocio/Negocio_model';
    private $model = 'Curso_model';
    private $view = 'Agregar_v';
    private $controller = 'Agregar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->modalidad, 'modelmodalidad');
        $this->load->model($this->nivel, 'modelnivel');
        $this->load->model($this->version, 'modelversion');
        $this->load->model($this->familia, 'modelfamilia');
        $this->load->model($this->sence, 'modelsence');

        $this->load->model($this->sublinea, 'sublineanegocio');

        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del Agregar
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Curso/agregar.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Curso";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
        //  cargo el menu en la session
    }

    function RegistraCurso() {

        // Ultima fecha Modificacion: 2016-12-02
        // Descripcion:  Metodo para registrar Curso
        // Usuario Actualizador : Marcelo Romero

        $array_version = $this->input->post('cbx_version');





        $arr_input_data = array(
            'nombre_curso' => $this->input->post('nombre_curso_v'),
            'descripcion_curso' => $this->input->post('descripcion_curso_v'),
            'duracion_curso' => $this->input->post('duracion_curso_v'),
            'id_codigo_sence' => $this->input->post('cbx_codigo_sence'),
            'id_modalidad' => $this->input->post('cbx_modalidad'),
            'id_familia' => $this->input->post('cbx_familia'),
            'id_nivel' => $this->input->post('cbx_nivel'),
            'sub_linea_negocio' => $this->input->post('cbx_sub_linea_negocio'),
            'id_grado_bono' => $this->input->post('cbx_slt_grado_curso'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );


        // llamado al modelo
        $respuesta = $this->modelo->set_arr_agregar_curso($arr_input_data);
        $id_curso = $respuesta[0]['id_curso'];
        if($id_curso > 0) {

            for ($i = 0; $i < count($array_version); $i++) {
                
                $arr_input_data_version = array(
                    'id_curso' => $id_curso,
                    'id_version' => $array_version[$i],
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );
                
                $this->modelversion->set_arr_insert_version_curso($arr_input_data_version);
            }
        }



        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function getModalidad() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelmodalidad->get_arr_modalidad_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getNivel() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelnivel->get_arr_listar_nivel_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getVersion() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        //  $this->load->model($this->version, 'modelversion');
        $datos = $this->modelversion->get_arr_listar_version_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getFamilia() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelfamilia->get_arr_listar_familia_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getCodigoSence() {

        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelsence->get_arr_codigo_sence_by_cursoCBX_curso($arr_sesion);
        echo json_encode($datos);
    }

    function getSubLineaNegocio() {

        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->sublineanegocio->get_arr_listar_sub_lineas_negocio($arr_sesion);
        echo json_encode($datos);
    }

    function getGradoBono(){
        $datos = $this->modelo->get_grado_bonos();
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
