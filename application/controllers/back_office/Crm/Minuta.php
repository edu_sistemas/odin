<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class minuta extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Minuta';
    private $package = 'back_office/Crm';
    private $model = 'Minuta_model';
// informacion adicional para notificaciones       
    private $model2 = 'Notificaciones_model';     
    private $view = 'Minuta_V';
    private $controller = 'Minuta';
    private $ind = ''; 




    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');        

//  Libreria de sesion
        $this->load->library('session');
    }

// 2016-05-25
// Controlador del panel de control
    public function index($variables = true) { 
      if ($variables == false) {
        redirect($this->package . '/Cuenta', 'refresh');
    }

///  array ubicaciones
    $arr_page_breadcrumb = array
    (
        array(
            'href' => base_url() . $this->ind . 'back_office' . '/Home',
            'label' => 'Home',
            'icono' => ''
            ),
        array(
            'href' => base_url() . $this->ind . 'back_office' . '/Crm/Cuenta',
            'label' => 'Cuenta',
            'icono' => ''
            ),			
        array(
            'href' => base_url() . $this->ind . $this->package . '/Minuta',                      
            'label' => 'Minuta',
            'icono' => ''
            )
        );

    $arr_theme_css_files = array
    (
       array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css')
	   
	   

       );


//  js necesarios para la pagina
    $arr_theme_js_files = array
    (			
       array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'), 
       array('src' => base_url() . 'assets/amelia/js/sistema/Crm/minuta/Minuta.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
       array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js')

       );

// informacion adicional para la pagina
    $data['page_title'] = '';
    $data['page_title_small'] = '';
    $data['panel_title'] = $this->nombre_item_singular;
    $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
    $data['script_adicional'] = $arr_theme_js_files;
    $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

    $datos_menu['user_id'] = $this->session->userdata('id_user');
    $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
    $data['id_perfil'] = $this->session->userdata('id_perfil');
    $data['activo'] = "Cuenta";


    $data['datos_minuta'] = $this->modelo->get_arr_listar_minutas_origen($this->session->userdata('id_user'));


///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
    $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
    $data['id_perfil'] = $this->session->userdata('id_perfil');
    $data['page_menu'] = 'dashboard';
    $data['main_content'] = $this->package . '/' . $this->view;
// informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();    

    //$data['datos_cliente'] = $this->getCliente($id_cliente);
    //$data['datos_compromiso_cbx'] = $this->getCompromisoCbx();
    //$data['datos_contacto_origen_cbx'] = $this->getContactoOrigenCbx();

    //$data['tipos_llamada'] = $this->getTiposLlamada();
    //$data['tipos_correo'] = $this->getTiposCorreo();
    //$data['tipos_reunion'] = $this->getTiposReunion();
    //$data['datos_gestion'] = $this->getDatosGestion($id_gestion_ori);    

    $this->load->view('amelia_1/template', $data);
}

// informacion adicional para notificaciones  
    function getNotificaciones (){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

  function ListarMinutas() {
    
  $arr_sesion = array(

    'id_usuario' => $this->input->post('id_usuario')
    
        );
    $datos = $this->modelo->get_arr_listar_minutas_filtrado($arr_sesion);
    echo json_encode($datos);

}


  function EliminarMinuta() {
    
  $arr_sesion = array(

    'user_id'=> $this->session->userdata('id_user'),
    'user_perfil'=> $this->session->userdata('id_perfil'),    
    'id' => $this->input->post('id')
    
        );
    $datos = $this->modelo->get_arr_eliminar_minuta($arr_sesion);
    echo json_encode($datos);

}


function guardarMinuta() {

$arr_input_data = array(
     'user_id'=> $this->session->userdata('id_user'),
     'user_perfil'=> $this->session->userdata('id_perfil'),
     'asunto_minuta' => $this->input->post('asunto_minuta'),
     'fecha_minuta' => $this->input->post('fecha_minuta'),
     'mensaje_minuta' => $this->input->post('mensaje_minuta')
     );

    $respuesta = $this->modelo->set_arr_guardar_minuta($arr_input_data);
    echo json_encode($respuesta);

} 


}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */

       