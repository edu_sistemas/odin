<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificaciones extends MY_Controller {

    // Variables paramétricas
        private $nombre_item_singular = 'Notificaciones';
        private $package = 'back_office/Crm';
        private $model = 'Notificaciones_model';
        private $view = 'Notificaciones_V';
        private $ind = '';

        function __construct() {
            parent::__construct();

    //  Carga el modelo que utiliza el controlador
            $this->load->model($this->package . '/' . $this->model, 'modelo');

    //  Libreria de sesion
            $this->load->library('session');
        }

    // 2016-05-25
    // Controlador del panel de control
        public function index() {

    ///  array ubicaciones
            $arr_page_breadcrumb = array
                (
                array(
                    'href' => base_url() . $this->ind . 'back_office' . '/Home',
                    'label' => 'Home',
                    'icono' => ''
                ),
                array(
                    'href' => base_url() . $this->ind . 'back_office' . '/Crm/Cuenta',
                    'label' => 'Cuenta',
                    'icono' => ''
                ),
                array(
                    'href' => base_url() . $this->ind . 'back_office' . '/Notificaciones',
                    'label' => 'Notificaciones ',
                    'icono' => ''
                )
            );

            $arr_theme_css_files = array
                (
                array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
                array('src' => base_url() . 'assets/amelia/css/Crm/crm.css')
            );


    //  js necesarios para la pagina
            $arr_theme_js_files = array
                (
                array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
                array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
                array('src' => base_url() . 'assets/amelia/js/sistema/Crm/navbar/navbar.js')
                //array('src' => base_url() . 'assets/amelia/js/sistema/Crm/notificaciones/notifiaciones.js')
            );

    // informacion adicional para la pagina
            $data['page_title'] = '';
            $data['page_title_small'] = '';
            $data['panel_title'] = $this->nombre_item_singular;
            $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
            $data['script_adicional'] = $arr_theme_js_files;
            $data['style_adicional'] = $arr_theme_css_files;

    ////**** Obligatorio ****** //////

            $datos_menu['user_id'] = $this->session->userdata('id_user');
            $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
            $data['id_perfil'] = $this->session->userdata('id_perfil');
            $data['activo'] = "Home";

    ///  cargo el menu en la session
    //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
    //   Carga Menu dentro de la pagina
            $data['notificaciones'] = $this->getNotificaciones();
            $data['notificaciones_todas'] = $this->getSetnotificacion_res();
            $data['datos_cliente'] = $this->getSetnotificacion_res();
            $data['menus'] = $this->session->userdata('menu_usuario');
    ///  fin carga menu
            $data['id_perfil'] = $this->session->userdata('id_perfil');
            $data['page_menu'] = 'dashboard';
            $data['main_content'] = $this->package . '/' . $this->view;


            $this->load->view('amelia_1/template', $data);
        }  

//---Para NOTIFICACIONES_V.---// 

        function getNotificaciones (){
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            
            $datos = $this->modelo->get_arr_listar_notificaciones($arr_sesion);
            return $datos;
        }

        function getSetnotificacion_res (){
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            
            $datos = $this->modelo->get_arr_listar_notificaciones_res($arr_sesion);
            return $datos;
        }

//---Fin para NOTIFICACIONES_V.---//         


//---Marcar notificaciones una por una.---// 

        function getSetnotificacion() {
            // carga el combo box Holding
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil'),
                'id_notificacion' => $this->input->post('id_notificacion')
            );
            $respuesta = $this->modelo->set_notificacion($arr_sesion);
            echo json_encode($respuesta);
        }

//---Fin marcar notificaciones una por una.---//


//---Marcar 10  últimas como leídas.---//

        function getSetnotificacion_Todos(){
            $arr_sesion = array(   
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil'),
                'id_notificacion' => $this->input->post('id_notificacion')
            );
            
            $respuesta = $this->modelo->set_notificacion_todas($arr_sesion);
            echo json_encode($respuesta);
        }

//---Fin marcar 10  últimas como leídas.---//        

}


/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */


