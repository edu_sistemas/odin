<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('max_execution_time', 0);

class Cuenta extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Cuenta';
    private $package = 'back_office/Crm';
    private $model = 'Cuenta_model';
// informacion adicional para notificaciones       
    private $model2 = 'Notificaciones_model';    
    private $view = 'Cuenta_V';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');

//  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
    }

// 2016-05-25
// Controlador del panel de control
    public function index($id_ejecutivo=0) {

///  array ubicaciones
        $arr_page_breadcrumb = array
        (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Cuenta',
                'label' => 'Cuenta',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
        (
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );


//  js necesarios para la pagina
        $arr_theme_js_files = array
        (			
           array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
           array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
           array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
           array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
           array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
           array('src' => base_url() . 'assets/amelia/js/sistema/Crm/cuenta/cuenta.js'),
           array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
           array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js')		
       );

// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;
        $data['ejecutivo']=$id_ejecutivo;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Cuenta";


///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

// informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $data['cliente_crm'] = $this->getCliente($id_ejecutivo);

        $data['HistorialGestion_crm'] = $this->getHistorialGestion($id_ejecutivo);

        $this->load->view('amelia_1/template', $data);
    }

// informacion adicional para notificaciones	
    function getNotificaciones (){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

    function getCliente($id_ejecutivo) {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $id_ejecutivo
        );
        $datos = $this->modelo->get_arr_listar_clientes($arr_sesion);
        return $datos;
    }

/*
        function getCliente_filtrado() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_clientes_filtro($arr_sesion);
        return $datos;
    }
*/    

    function getHistorialGestion($id_ejecutivo) {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $id_ejecutivo
        );
        $datos = $this->modelo->get_arr_listar_historial_gestion($arr_sesion);
        return $datos;
    }

    function getHistorialGestionDownload() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo')
        );
        $respuesta = $this->modelo->get_arr_listar_gestiones_download($arr_sesion);
        echo json_encode($respuesta);
    }   
    
    function getHistorialDominiosDownload() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->get_arr_listar_dominios_download($arr_sesion);
        echo json_encode($respuesta);
    }  

	// ELIMINAR CUENTA

    function getEliminarCuenta() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_empresa' => $this->input->post('id_empresa')
        );
        $respuesta = $this->modelo->eliminar_cuenta($arr_sesion);
        echo json_encode($respuesta);
    }	


	// FIN ELIMINAR

	// ELIMINAR CUENTA

    function getEliminarGestion() {
        // carga el combo box Holding
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_gestion' => $this->input->post('id_gestion')
        );
        $respuesta = $this->modelo->eliminar_gestion($arr_sesion);
        echo json_encode($respuesta);
    }	

	// FIN ELIMINAR

    function getClientesConEjecutivos(){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->get_arr_listar_clientes_ejecutivos($arr_sesion);
        echo json_encode($respuesta);
    }


    function getListaEjecutivos() {
        // carga el combo box grupo ejecutivo
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo')
        );
        $respuesta = $this->modelo->get_arr_listar_ejecutivos_cbx($arr_sesion);
        echo json_encode($respuesta);
    }

    public function CargaEmpresas()
    {
        $archivo=$_FILES['excel']['tmp_name'];
        $this->load->library('excel');

        // Cargo la hoja de cálculo
        $this->excel = PHPExcel_IOFactory::load($archivo);

        //Asigno la hoja de calculo activa
        $this->excel->setActiveSheetIndex(0);
        //Obtengo el numero de filas del archivo
        $numRows = $this->excel->setActiveSheetIndex(0)->getHighestRow();
        
        $err="";
        $crl=0;
        $crc=0;
        $cre=0;

        if($numRows<2)
        {
           $result = array('resultado' => 'ERROR','mensaje' => 'El archivo seleccionado no contiene registros.');
       }
       else
       {
        for ($i = 2; $i <= $numRows; $i++) 
        {

            $rut=addslashes(trim($this->excel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue()));
            $razon=addslashes(trim($this->excel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue()));
            $giro=addslashes(trim($this->excel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue()));
            $fantasia=addslashes(trim($this->excel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue()));
            $direccion=addslashes(trim($this->excel->getActiveSheet()->getCell('E'.$i)->getCalculatedValue()));
            $comuna=addslashes(trim($this->excel->getActiveSheet()->getCell('F'.$i)->getCalculatedValue()));
            $descripcion=addslashes(trim($this->excel->getActiveSheet()->getCell('G'.$i)->getCalculatedValue()));
            $holding=addslashes(trim($this->excel->getActiveSheet()->getCell('H'.$i)->getCalculatedValue()));
            $otic=addslashes(trim($this->excel->getActiveSheet()->getCell('I'.$i)->getCalculatedValue()));
            $rubro=addslashes(trim($this->excel->getActiveSheet()->getCell('J'.$i)->getCalculatedValue()));
            $ejecutivo=addslashes(trim($this->excel->getActiveSheet()->getCell('K'.$i)->getCalculatedValue()));

            if(strlen($rut)>8)
            {
                $res = $this->modelo->get_arr_existe_empresa($rut);
                
                $ide=($res==null?0:$res->id_empresa);

                $dCom=array('accion' => 1, 'valor' => $comuna);
                $dHol=array('accion' => 2, 'valor' => $holding);
                $dRub=array('accion' => 3, 'valor' => $rubro);
                $dEje=array('accion' => 4, 'valor' => $ejecutivo);

                $vcomuna=$this->modelo->get_arr_auxiliar_empresa($dCom);
                $vholding=$this->modelo->get_arr_auxiliar_empresa($dHol);
                $vrubro=$this->modelo->get_arr_auxiliar_empresa($dRub);
                $vejecutivo=$this->modelo->get_arr_auxiliar_empresa($dEje);

                $vcomuna=($vcomuna==null?0:$vcomuna->id);
                $vholding=($vholding==null?0: $vholding->id);
                $vrubro=($vrubro==null?0: $vrubro->id);
                $vejecutivo=($vejecutivo==null?0: $vejecutivo->id);

                if($ide!="0")
                {
                    $razon=(strlen($razon)<1?$res->razon_social_empresa:$razon);
                    $giro=(strlen($giro)<1?$res->giro_empresa:$giro);
                    $fantasia=(strlen($fantasia)<1?$res->nombre_fantasia:$fantasia);
                    $direccion=(strlen($direccion)<1?$res->direccion_empresa:$direccion);
                    $vcomuna=($vcomuna==0?$res->id_comuna:$vcomuna);
                    $descripcion=(strlen($descripcion)<1?$res->descripcion_empresa:$descripcion);
                    $vholding=($vholding==0?$res->id_holding:$vholding);
                    $otic=(strlen($otic)<1?$res->crm_otic_cuenta:$otic);
                    $vrubro=($vrubro==0?$res->crm_rubro:$vrubro);
                    $vejecutivo=($vejecutivo==0?$res->crm_id_ejecutivo:$vejecutivo);
                }
                

                $datos=array(
                    'id' => $ide,
                    'rut' => $rut,
                    'razon' => $razon,
                    'giro' => $giro,
                    'fantasia' => $fantasia,
                    'direccion' => $direccion,
                    'comuna' => $vcomuna,
                    'descripcion' => $descripcion,
                    'holding' => $vholding,
                    'otic' => $otic,
                    'rubro' => $vrubro,
                    'ejecutivo' => $vejecutivo
                );


                $eje=$this->modelo->set_arr_set_empresa($datos);

                $eje=$eje->resultado;                

                if($eje>0)
                {
                    $crc++;
                }

            }
        }

        if($crc==($numRows-1))
        {
         $result = array('resultado' => 'OK','mensaje' => 'Datos cargados correctamente.');
     }
     else
     {
       $result = array('resultado' => 'OK','mensaje' => 'Datos cargados parcialmente.');
   }
}

echo json_encode($result);
}

}






