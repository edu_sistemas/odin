<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class editar_gestion extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Editar Gestion';
    private $package = 'back_office/Crm';
    private $model = 'Editar_gestion_model';
// informacion adicional para notificaciones       
    private $model2 = 'Notificaciones_model';       
    private $view = 'Editar_gestion_V';
    private $controller = 'Editar_gestion';
    private $ind = '';




    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');        

//  Libreria de sesion
        $this->load->library('session');
    }

// 2016-05-25
// Controlador del panel de control
    public function index($variables = false) { 
      if ($variables == false) {
        redirect($this->package . '/Cuenta', 'refresh');
    }

    $varsplit = explode(".",$variables);
    $id_cliente = $varsplit[0];
    $id_gestion_ori = $varsplit[1];
    $data['id_gestion_ori'] = $id_gestion_ori;



///  array ubicaciones
    $arr_page_breadcrumb = array
    (
        array(
            'href' => base_url() . $this->ind . 'back_office' . '/Home',
            'label' => 'Home',
            'icono' => ''
        ),
        array(
            'href' => base_url() . $this->ind . 'back_office' . '/Crm/cuenta',
            'label' => 'Cuenta',
            'icono' => ''
        ),			
        array(
            'href' => base_url() . $this->ind . $this->package . '/Editar_cuenta',                      
            'label' => 'Editar Gestión',
            'icono' => ''
        )
    );

    $arr_theme_css_files = array
    (
       array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
       array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css')



   );


//  js necesarios para la pagina
    $arr_theme_js_files = array
    (			
       array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'), 
       array('src' => base_url() . 'assets/amelia/js/sistema/Crm/cliente_potencial/editar_gestion.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
       array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
       array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js')

   );

// informacion adicional para la pagina
    $data['page_title'] = '';
    $data['page_title_small'] = '';
    $data['panel_title'] = $this->nombre_item_singular;
    $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
    $data['script_adicional'] = $arr_theme_js_files;
    $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

    $datos_menu['user_id'] = $this->session->userdata('id_user');
    $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
    $data['id_perfil'] = $this->session->userdata('id_perfil');
    $data['activo'] = "Cuenta";

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
    $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
    $data['id_perfil'] = $this->session->userdata('id_perfil');
    $data['page_menu'] = 'dashboard';
    $data['main_content'] = $this->package . '/' . $this->view;

// informacion adicional para notificaciones   
    $data['notificaciones'] = $this->getNotificaciones();    

    $data['datos_cliente'] = $this->getCliente($id_cliente);
    //$data['datos_compromiso_cbx'] = $this->getCompromisoCbx();
    //$data['datos_contacto_origen_cbx'] = $this->getContactoOrigenCbx();

    //$data['tipos_llamada'] = $this->getTiposLlamada();
    //$data['tipos_correo'] = $this->getTiposCorreo();
    $data['tipos_reunion'] = $this->getTiposReunion();
    $data['datos_gestion'] = $this->getDatosGestion($id_gestion_ori);    

    $this->load->view('amelia_1/template', $data);
}


// informacion adicional para notificaciones    
function getNotificaciones (){
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );

    $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
    return $datos;
}

function Agregar_Accion_Llamada() {
    $arr_input_data = array(
     'user_id'=> $this->session->userdata('id_user'),
     'user_perfil'=> $this->session->userdata('id_perfil'),
     'id_gestion' => $this->input->post('hidden_id_gestion_ori'),     
     'empresa_id' => '0',
     'id_accion_gestion' => $this->input->post('hidden_id_accion_gestion'), 
     'tipo_accion' => $this->input->post('llamada_tipo_llamada'),
     'subtipo_accion' => $this->input->post('subtipo_correo_reunion'),
     'fecha_accion' => $this->input->post('llamada_fecha_llamada'),
     'asunto_accion' => $this->input->post('llamada_asunto_box'),
     'comentario_accion' => $this->input->post('llamada_comentario_box'),
     'compromiso_accion' => $this->input->post('llamada_compromiso_cbx'),
     'fecha_compromiso_accion' => $this->input->post('llamada_fecha_compromiso'),
     'contacto_origen' => $this->input->post('llamada_contacto_origen_cbx'),
     'id_producto' => $this->input->post('id_producto'),
     'fase' => $this->input->post('fase'),
     'modalidad' => $this->input->post('id_modalidad'),
     'comentario_otros' => (strlen($this->input->post('comentario_otros'))<3? $this->input->post('_hidden_comentario_otros'): $this->input->post('comentario_otros'))
 );

    $respuesta = $this->modelo->set_arr_agregar_accion_cliente($arr_input_data);
    echo json_encode($respuesta);

}

function getFechaCierreCompromiso(){
    $arr_input_data = array(
     'user_id'=> $this->session->userdata('id_user'),
     'user_perfil'=> $this->session->userdata('id_perfil'),
     'id_compromiso' => $this->input->get('id_compromiso')
 );
    $datos = $this->modelo->get_fecha_cierre_compromiso($arr_input_data);
    echo json_encode($datos);
}
/*
function Agregar_Accion_Pedido() {


		// $date = new DateTime($_POST['campo5']);		
		// $pedido_fecha_reunion = $date->format('Y-m-d'); 

    $arr_input_data = array(
        'id_cliente_pedido' => $this->input->post('campo0'),
        'pedido_fecha_reunion' => $this->input->post('campo5'),
        'pedido_modalidad_cbx' => $this->input->post('campo1'),
        'pedido_tipo_curso_cbx' => $this->input->post('campo2'),
        'pedido_cant_asistentes' => $this->input->post('campo3'),
        'pedido_comentario' => $this->input->post('campo4')
        );


    $arr_input_data['user_id'] = $this->session->userdata('id_user');
    $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

    $respuesta = $this->modelo->set_arr_agregar_accion_pedido($arr_input_data);
    echo json_encode($respuesta);
}		
*/

function getCliente($id_cliente) {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id_cliente' => $id_cliente
    );
    $datos = $this->modelo->get_arr_get_datos_cliente($arr_sesion);
    return $datos;
}	

function getCompromisoCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $accion=$this->input->get('fase');
    $datos = $this->modelo->get_arr_get_datos_compromiso_cbx($accion);
    echo json_encode($datos);
}		


function getContactoOrigenCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id_empresa' => $this->input->post('id_empresa')
    );
    $datos = $this->modelo->get_arr_get_datos_contacto_origen_cbx($arr_sesion);
    echo json_encode($datos);
}	

function getTiposLlamadaCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'accion' => 1,
        'idfase' => $this->input->get('fase')
    );

    $datos = $this->modelo->get_arr_get_tipos_accion_cbx($arr_sesion);
    echo json_encode($datos);
}

function getTiposCorreoCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'accion' => 2,
        'idfase' => $this->input->get('fase')
    );
    $datos = $this->modelo->get_arr_get_tipos_accion_cbx($arr_sesion);
    echo json_encode($datos);
}


function getTiposReunion() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'accion' => 3,
        'idfase' => 1
    );
    $datos = $this->modelo->get_arr_get_tipos_accion_cbx($arr_sesion);
    return $datos;
}

//--------------------------PRUEBA------------------------------

function getTiposCorreoInternoCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $datos = $this->modelo->get_arr_get_tipos_correo_interno_cbx($arr_sesion);
    echo json_encode($datos);
}

//--------------------------FIN PRUEBA--------------------------

function getFases() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $datos = $this->modelo->get_arr_listar_fases_cbx($arr_sesion);
    echo json_encode($datos);
}

function getFaseReunion() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $datos = $this->modelo->get_arr_listar_fases_cbx($arr_sesion);
    echo json_encode($datos);
}


function getTipoReunion() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'idfase' => $this->input->get('fase')
    );

    $datos = $this->modelo->get_arr_listar_tipos_reunion_cbx($arr_sesion);
    echo json_encode($datos);
}	


function getModalidad() {
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $datos = $this->modelo->get_arr_listar_modalidades_cbx($arr_sesion);
    echo json_encode($datos);
}

function getProductos() {
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id_modalidad' => $this->input->get('id_modalidad')
    );
    $datos = $this->modelo->get_arr_listar_productos_cbx($arr_sesion);
    echo json_encode($datos);
}



/*
function getTipoPedido() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
			//'idmodalidad' => $this->input->get('modalidad')
        );

    $datos = $this->modelo->get_arr_listar_modalidad_pedido_cbx($arr_sesion);
    echo json_encode($datos);
}		


function getTipoCurso() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
			//'idmodalidad' => $this->input->get('modalidad')
        );

    $datos = $this->modelo->get_arr_listar_tipo_curso_pedido_cbx($arr_sesion);
    echo json_encode($datos);
}			

*/
// NECESARIO

function getDatosGestion($id) {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id' => $id
    );

    $datos = $this->modelo->get_arr_listar_datos_gestion($arr_sesion);
    return $datos;
}    

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */

