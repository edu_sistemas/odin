<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class reasigna_cuenta extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Reasigna Cuenta';
    private $package = 'back_office/Crm';
    private $model = 'Reasigna_cuenta_model';
// informacion adicional para notificaciones       
    private $model2 = 'Notificaciones_model';       
    private $view = 'Reasigna_cuenta_V';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');        

//  Libreria de sesion
        $this->load->library('session');
    }

// 2016-05-25
// Controlador del panel de control
    public function index() {

///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
        array(
            'href' => base_url() . $this->ind . 'back_office' . '/Crm/Cuenta',
            'label' => 'Cuenta',
            'icono' => ''
            ),			
			array(
                'href' => base_url() . $this->ind . $this->package . '/Crm',
                'label' => 'Reasigna Cuenta',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
			array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
			array('src' => base_url() . 'assets/amelia/css/plugins/dualListbox/bootstrap-duallistbox.min.css'),
			array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css'),
			array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
			array('src' => base_url() . 'assets/amelia/font-awesome-crm/css/font-awesome.css'),
			array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/toastr/toastr.min.css'),
			array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
        );

//  js necesarios para la pagina
        $arr_theme_js_files = array
            (			
			array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/Crm/cuenta/reasigna_cuenta.js'),
		    array('src' => base_url() . 'assets/amelia/js/plugins/toastr/toastr.min.js'),			
			array('src' => base_url() . 'assets/amelia/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js')
        );
		


// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Home";

		
		//Cargar listado Ejecutivos
		
		 $data['datos_ejecutivos_ori'] = $this->modelo->get_arr_listar_datos_ejecutivos_origen();
         //$data['datos_gestiones'] = $this->modelo->get_arr_listar_gestiones($this->session->userdata('id_ejecutivo'));
		
		
///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
// informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();        
        $this->load->view('amelia_1/template', $data);
    }

    // informacion adicional para notificaciones  
    function getNotificaciones (){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }
    

	function ListarEmpresas() {
    
	$arr_sesion = array(

		'id_ejecutivo' => $this->input->post('id_ejecutivo')
		
        );
    $datos = $this->modelo->get_arr_listar_datos_ejecutivos_filtrado($arr_sesion);
    echo json_encode($datos);

}

    function popupGestiones() {
    
    $arr_sesion = array(

        'user_id' => $this->input->post('id_ejecutivo'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id_ejecutivo' => $this->input->post('id_ejecutivo')
        
        );
    $datos = $this->modelo->get_arr_listar_gestiones($arr_sesion);
    echo json_encode($datos);

}


function getComboEjecutivoDestino() {
       // carga el combo box grupo ejecutivo
   $arr_sesion = array(
       'user_id' => $this->session->userdata('id_user'),
       'user_perfil' => $this->session->userdata('id_perfil')
       );

   $datos = $this->modelo->get_arr_listar_ejecutivo_destino_cbx($arr_sesion);
   echo json_encode($datos);
}

function ReasignarCuentas() {
       // carga el combo box grupo ejecutivo
   $arr_sesion = array(
       'user_id' => $this->session->userdata('id_user'),
       'user_perfil' => $this->session->userdata('id_perfil'),
	   'id_empresa' => $this->input->post('id_empresa'),
	   'id_nuevo_ejecutivo' => $this->input->post('id_nuevo_ejecutivo')
       );

   $datos = $this->modelo->get_arr_reasignar_cuentas($arr_sesion);
   echo json_encode($datos);
}



//----------- EXPORTAR ----------- //

        function getReasignaCuentaBitacora() {
        // carga el combo box Holding
       
       $arr_sesion = array(
            'ano' => date('Y'),
            //'ano' => $this->session->userdata('ano'),
            //'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->get_arr_bitacora_reasignar_download($arr_sesion);
        echo json_encode($respuesta);
        
    }   

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
