<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 */
defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('max_execution_time', 0);

class edashboard extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Dashboard - Ejecutivo';
    private $package = 'back_office/Crm/';
    private $model = 'Edashboard_model';
    // informacion adicional para notificaciones       
    private $model2 = 'Notificaciones_model';    
    private $view = 'Edashboard_V';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');        

//  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
    }

// 2016-05-25
// Controlador del panel de control
    public function index($id_ejecutivo) {

///  array ubicaciones
        $arr_page_breadcrumb = array
        (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
                ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Edashboard',
                'label' => 'Dashboard - Ejecutivo',
                'icono' => ''
                )
            );

        $arr_theme_css_files = array
        (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/daterangepicker/daterangepicker-bs3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')            
            );


//  js necesarios para la pagina
        $arr_theme_js_files = array
        (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/toastr/toastr.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),

            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/moment.min.js'),
             array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'), 
            array('src' => base_url() . 'assets/amelia/js/plugins/daterangepicker/daterangepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/edashboard/edashboard.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js') 

            );


// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

// ************ F.Q ************

        //$data['metas_ejecutivos'] = $this->modelo->get_arr_listar_metas_ejecutivos($this->session->userdata('id_user'));      
        //$data['compromisos_ejecutivos'] = $this->getDatosFiltradosTablaCompromisos();
        //$data['metas_global'] = $this->modelo->get_arr_listar_metas_global();
        $data['ejecutivo']=$id_ejecutivo;


// ************ F.Q ************

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Dashboard - Ejecutivo";

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
// informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones(); 
        $this->load->view('amelia_1/template', $data);
    }




    /****************************************************/


    // informacion adicional para notificaciones    
    function getNotificaciones (){
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }



    function getListaEjecutivos() {
        // carga el combo box grupo ejecutivo
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo')
            );
        $datos = $this->modelo->get_arr_listar_ejecutivos_cbx($arr_sesion);
        echo json_encode($datos);
    }

    
/*
     function getRankingVentas() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo'),
            'desde' => $this->input->post('desde'),
            'hasta' => $this->input->post('hasta')
        );

        $datos = $this->modelo->get_arr_consultar_ranking_venta($arr_data);
        echo json_encode($datos);
    }
    */
/*
    function getComparativaVentas() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo'),
            'anio' => $this->input->post('anio')
        );

        $datos = $this->modelo->get_arr_consultar_comparativa_venta($arr_data);
        echo json_encode($datos);
    }
    */

    function getDistribucionCliente() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo')
        );

        $datos = $this->modelo->get_arr_consultar_distribucion_cliente($arr_data);
        echo json_encode($datos);
    }

    function getDatosFiltradosKpi() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo'),
            'fecha' => $this->input->post('fecha')
        );

        $datos = $this->modelo->get_arr_filtro_eventos_kpi($arr_data);
        echo json_encode($datos);
    }    

//***********************
    function getDistribucionGestion() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo')
        );

        $datos = $this->modelo->get_arr_consultar_distribucion_gestion($arr_data);
        echo json_encode($datos);
    }

//*******************


/*
     function getHistoricoGestion() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'ejecutivo' => $this->input->post('ejecutivo')
        );

        $datos = $this->modelo->get_arr_consultar_historico_gestion($arr_data);
        echo json_encode($datos);
    }
    */


     function getDatosFiltradosClientes() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'ejecutivo' => $this->input->post('ejecutivo'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'filtradoVentas' => $this->input->post('filtradoVentas')
        );

        $datos = $this->modelo->get_arr_datos_filtrados_cliente_venta($arr_data);
        echo json_encode($datos);
    }


     function getDatosFiltradosGestiones() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'ejecutivo' => $this->input->post('ejecutivo'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'filtradoGestion' => $this->input->post('filtradoGestion')
        );

        $datos = $this->modelo->get_arr_datos_filtrados_cliente_gestion($arr_data);
        echo json_encode($datos);
    }    



         function getDatosFiltradosTablaCompromisos() {
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'ejecutivo' => $this->input->post('ejecutivo'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'empresa' =>   $this->input->post('empresa')
        );

        $datos = $this->modelo->get_arr_datos_filtrados_tabla_compromisos($arr_data);
        echo json_encode($datos);
    }   

        function getDatosFiltradosTablaMetas() {
            $arr_data = array(
                'user_id' => $this->session->userdata('id_user'),
                'ejecutivo' => $this->input->post('ejecutivo'),
                'user_perfil' => $this->session->userdata('id_perfil'),
                'fecha' => $this->input->post('fecha')
            );

        $datos = $this->modelo->get_arr_datos_filtrados_tabla_metas($arr_data);
        echo json_encode($datos);
    }


        function getEMetasGlobales() {
            $arr_data = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil'),
                'fecha' => $this->input->post('fecha'),
                'ejecutivo' => $this->input->post('ejecutivo')
                //'ejecutivo' => ($this->input->post('ejecutivo')==0) ? ($this->session->userdata('id_user')):($this->input->post('ejecutivo'))
            );

            $datos = $this->modelo->get_arr_e_metas_globales($arr_data);
            echo json_encode($datos);
        }

        function generarExcelMetas($ejecutivo, $fecha) {
             if($fecha == 0){
                $fecha = '';
            }
             if($id_ejecutivo == 0){
                $id_ejecutivo = '';
            }
             
            $arr_datos = array(
                'ejecutivo' => $ejecutivo,
                'fecha' => $fecha,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

            $data_excel = $this->modelo->get_arr_datos_metas_excel($arr_datos);
           // var_dump($data_excel);
            //die();
            $this->load->library('excel');

            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Metas');
            //set cell A1 content with some text

            $this->excel->getActiveSheet()->SetCellValue('A1', 'EMPRESA(Holding)');        
            $this->excel->getActiveSheet()->SetCellValue('B1', 'VENTA MENSUAL');
            $this->excel->getActiveSheet()->SetCellValue('C1', 'VENTA ACUMULADA');
            
            $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');
            $this->excel->getActiveSheet()->getStyle('A1:C1')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '#8cd9c9e3')
                        )
                    )
                );
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('19');
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('16');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('26');
        
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $filename = 'Metas_' . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
            ob_clean();
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: private'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }

        function generarExcelCompromisos($ejecutivo) {
            if($ejecutivo == 0){
                $ejecutivo = '';
            }
             
            $arr_datos = array(
                'ejecutivo' => $ejecutivo,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

            $data_excel = $this->modelo->get_arr_datos_compromisos_excel($arr_datos);
            //var_dump($data_excel);
            //die();
            $this->load->library('excel');

            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Compromisos');
            //set cell A1 content with some text

            $this->excel->getActiveSheet()->SetCellValue('A1', 'CLIENTE');        
            $this->excel->getActiveSheet()->SetCellValue('B1', 'DESCRIPCIÓN');
            $this->excel->getActiveSheet()->SetCellValue('C1', 'FECHA COMPROMISO');
            $this->excel->getActiveSheet()->SetCellValue('D1', 'EVENTO');
            $this->excel->getActiveSheet()->SetCellValue('E1', 'FASE');
            $this->excel->getActiveSheet()->SetCellValue('F1', 'FECHA ACCION');
            
            $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');
            $this->excel->getActiveSheet()->getStyle('A1:F1')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '#8cd9c9e3')
                        )
                    )
                );
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('26');
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('41');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('26');
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('26');
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('26');
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('26');                        
        
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $filename = 'Compromisos_' . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
            ob_clean();
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: private'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }

         function generarExcelVentasEmpresas($ejecutivo, $filtradoV=0) {
          $titulo="";
        
            if($ejecutivo == 0){
                $ejecutivo = '';
            }

            $arr_datos = array(
                'ejecutivo' => $ejecutivo,
                'filtradoV' => $filtradoV,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
                
                
            );
    
            $data_excel = $this->modelo->get_arr_datos_ventas_empresas_excel($arr_datos);

            $tam = count($data_excel)-1;
            $titulo = $data_excel[$tam];
            array_pop($data_excel);
            $this->load->library('excel');
            
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle("Hoja1");

            
            //set cell A1 content with some text

            $this->excel->getActiveSheet()->SetCellValue('A1', 'EMPRESA(Holding)');        
            $this->excel->getActiveSheet()->SetCellValue('B1', 'ULTIMA VENTA');
            $this->excel->getActiveSheet()->SetCellValue('C1', 'FECHA');
            $this->excel->getActiveSheet()->SetCellValue('D1', 'MONTO');
            
            $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');
            $this->excel->getActiveSheet()->getStyle('A1:D1')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '#8cd9c9e3')
                        )
                    )
                );
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('10');
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('11');
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('57');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('26');
        
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            
                      
            $filename = $titulo . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
            ob_clean();
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: private'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }

        function generarExcelGestionesEmpresas($ejecutivo, $filtradoG) {
            $titulo="";
            if($ejecutivo == 0){
                $ejecutivo = '';
            }

            $arr_datos = array(
                'ejecutivo' => $ejecutivo,
                'filtradoG' => $filtradoG,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')

            );

            $data_excel = $this->modelo->get_arr_datos_gestiones_empresas_excel($arr_datos);
             
            $tam = count($data_excel)-1;
            $titulo = $data_excel[$tam];
            array_pop($data_excel);
            $this->load->library('excel');
            
            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
          
            $this->excel->getActiveSheet()->setTitle($titulo);
            
            
            
            //set cell A1 content with some text

            $this->excel->getActiveSheet()->SetCellValue('A1', 'EMPRESA(Holding)');        
            $this->excel->getActiveSheet()->SetCellValue('B1', 'TIPO GESTIÓN');
            $this->excel->getActiveSheet()->SetCellValue('C1', 'FECHA');
           
            
            $this->excel->getActiveSheet()->fromArray($data_excel, null, 'A2');
            $this->excel->getActiveSheet()->getStyle('A1:C1')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '#8cd9c9e3')
                        )
                    )
                );
            
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('18');
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('17');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('47');
        
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $filename = $titulo . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
            ob_clean();
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: private'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }



}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
