<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class meta extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Meta';
    private $package = 'back_office/Crm';
    private $model = 'Meta_model';
// informacion adicional para notificaciones       
    private $model2 = 'Notificaciones_model';
    private $view = 'Meta_V';
    private $controller = 'Meta';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');

//  Libreria de sesion
        $this->load->library('session');
        $this->load->library('excel');
    }

// 2016-05-25
// Controlador del panel de control
    public function index($variables = true) {
        if ($variables == false) {
            redirect($this->package . '/Cuenta', 'refresh');
        }

///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Crm/Cuenta',
                'label' => 'Cuenta',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Meta',
                'label' => 'Meta',
                'icono' => ''
            )
        );


        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/ladda/ladda-themeless.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/jasny/jasny-bootstrap.min.css')
        );


//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/spin.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/Meta/Meta.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/jasny/jasny-bootstrap.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/ladda/ladda.jquery.min.js')
        );

// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Metas";


        //$data['datos_minuta'] = $this->modelo->get_arr_listar_minutas_origen($this->session->userdata('id_user'));
///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

// informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        //$data['datos_cliente'] = $this->getCliente($id_cliente);
        //$data['datos_compromiso_cbx'] = $this->getCompromisoCbx();
        //$data['datos_contacto_origen_cbx'] = $this->getContactoOrigenCbx();
        //$data['tipos_llamada'] = $this->getTiposLlamada();
        //$data['tipos_correo'] = $this->getTiposCorreo();
        //$data['tipos_reunion'] = $this->getTiposReunion();
        //$data['datos_gestion'] = $this->getDatosGestion($id_gestion_ori);    

        $this->load->view('amelia_1/template', $data);
    }

    function getNotificaciones() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

    function cargar_archivo_metas() {

        /* aumento el tamañao de espera del servidor   */

        ini_set('memory_limit', '-1');

        $arr_datos = array();
        $user_id = $this->session->userdata('id_user');
        $user_perfil = $this->session->userdata('id_perfil');


        $name = $_FILES['file_carga_meta']['name'];
        $tname = $_FILES['file_carga_meta']['tmp_name'];

        $cr = 0;

        if ($name != "") {



            $nomarchivo = str_replace(".xlsx", "", $name);
            $nomarchivo = explode(" ", $nomarchivo);

            $this->modelo->borarano($nomarchivo[1]);

            $obj_excel = PHPExcel_IOFactory::load($tname);
            $sheetData = $obj_excel->getActiveSheet()->toArray(null, true, true, true);

            $arr_datos = array();
            foreach ($sheetData as $index => $value) {
                if ($index != 1) {


                    if (trim($value['A']) != "") {

                        $arr_datos = array(
                            'ANO' => $nomarchivo[1],
                            'Mes' => $value['A'],
                            'Monto' => trim(str_replace("$", "", str_replace(",", "", str_replace(".", "", $value['B'])))),
                            'Empresa' => $value['C'],
                            'Producto' => $value['D'],
                            'Ejecutivo' => $value['E'],
                            'Estado' => $value['F'],
                        );
 
                        $cr += $this->modelo->get_arr_subir_meta($arr_datos);
                    } else {

                        break;
                    }
                }
            }

            $result['valido'] = true;
            $result['mensaje'] = 'Metas importadas correctamente';
            $result['registros'] = $cr;
        } else {

            $result['valido'] = false;
            $result['mensaje'] = 'Error al procesar el archivo de carga';
            $result['registros'] = 0;
        }

        echo json_encode($result);


        //$this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

}
