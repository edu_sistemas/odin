<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_potencial extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Cliente Potencial';
    private $package = 'back_office/Crm';
    private $model = 'Cliente_potencial_model';
    // informacion adicional para notificaciones       
    private $model2 = 'Notificaciones_model';        
    private $view = 'Cliente_potencial_V';
    private $controller = 'Cliente_potencial';
    private $ind = '';

    function __construct() {
        parent::__construct();

        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        // informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');        

        //  Libreria de sesion
        $this->load->library('session');

        //Libreria de Cotizacion
        $this->load->library('GenCotizacion');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index($id_cliente = false) {
      if ($id_cliente == false) {
       redirect($this->$package . '/Cuenta', 'refresh');
   }

        ///  array ubicaciones
   $arr_page_breadcrumb = array
   (
    array(
        'href' => base_url() . $this->ind . 'back_office' . '/Home',
        'label' => 'Home',
        'icono' => ''
    ),
    array(
        'href' => base_url() . $this->ind . 'back_office' . '/Crm/cuenta',
        'label' => 'Cuenta',
        'icono' => ''
    ),			
    array(
        'href' => base_url() . $this->ind . $this->package . '/Cliente_potencial',
        'label' => 'Nueva Gestión',
        'icono' => ''
    )
);

   $arr_theme_css_files = array
   (
    array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
    array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
    array('src' => base_url() . 'assets/amelia/css/Crm/custom.css'),
    array('src' => base_url() . 'assets/amelia/font-awesome/css/font-awesome.css'),
    array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
    array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
    array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
    array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
    array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
    array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css'),
    array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')

);


        //  js necesarios para la pagina
   $arr_theme_js_files = array
   (			
    array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
    array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
    array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
    array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
    array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'), 
    array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
    array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
    array('src' => base_url() . 'assets/amelia/js/sistema/Crm/cliente_potencial/cliente_potencial.js'),
    array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
    array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
    array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js')
);

        // informacion adicional para la pagina
   $data['page_title'] = '';
   $data['page_title_small'] = '';
   $data['panel_title'] = $this->nombre_item_singular;
   $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
   $data['script_adicional'] = $arr_theme_js_files;
   $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

   $datos_menu['user_id'] = $this->session->userdata('id_user');
   $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
   $data['id_perfil'] = $this->session->userdata('id_perfil');
   $data['activo'] = "Cuenta";

        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
   $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
   $data['id_perfil'] = $this->session->userdata('id_perfil');
   $data['page_menu'] = 'dashboard';
   $data['main_content'] = $this->package . '/' . $this->view;
        // informacion adicional para notificaciones   
   $data['notificaciones'] = $this->getNotificaciones();    

   $data['datos_cliente'] = $this->getCliente($id_cliente);

        //$data['datos_compromiso_cbx'] = $this->getCompromisoCbx();
        //$data['datos_contacto_origen_cbx'] = $this->getContactoOrigenCbx();

        //$data['tipos_llamada'] = $this->getTiposLlamada();
        //$data['tipos_correo'] = $this->getTiposCorreo();
   $data['tipos_reunion'] = $this->getTiposReunion();

   $this->load->view('amelia_1/template', $data);
}


    // informacion adicional para notificaciones    
function getNotificaciones (){
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );

    $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
    return $datos;
}


function Agregar_Accion_Llamada() {
    $arr_input_data = array(
        'user_id'=> $this->session->userdata('id_user'),
        'user_perfil'=> $this->session->userdata('id_perfil'),
        'empresa_id' => $this->input->post('idempresallamada'),
        'id_accion' => '1',
        'tipo_accion' => $this->input->post('tipo_llamada_cbx'),
        'id_producto' => ($this->input->post('seleccione_producto_cbx_llamada')==''?NULL:$this->input->post('seleccione_producto_cbx_llamada')),
        'subtipo_accion' => '0',
        'fase' => $this->input->post('llamada_seleccione_fase_cbx'),
        'modalidad' => $this->input->post('modalidad_llamada_cbx'),
        'fecha_accion' => $this->input->post('llamada_fecha_llamada'),
        'asunto_accion' => $this->input->post('llamada_asunto_box'),
        'comentario_accion' => $this->input->post('llamada_comentario_box'),
        'compromiso_accion' => $this->input->post('llamada_compromiso_cbx'),
        'fecha_compromiso_accion' => $this->input->post('llamada_fecha_compromiso'),
        'contacto_origen' => $this->input->post('llamada_contacto_origen_cbx'),
        'asistentes_pedidos' => null,
        'comentario_otros' => $this->input->post('comentario_otros_llamada')
    );
    $respuesta = $this->modelo->set_arr_agregar_accion_cliente($arr_input_data);
    echo json_encode($respuesta);

}

function Agregar_Accion_Correo() {
    $arr_input_data = array(
        'user_id'=> $this->session->userdata('id_user'),
        'user_perfil'=> $this->session->userdata('id_perfil'),
        'empresa_id' => $this->input->post('idempresacorreo'),
        'id_accion' => '2',
        'tipo_accion' => $this->input->post('tipo_correo_cbx'),
        'id_producto' => ($this->input->post('seleccione_producto_cbx_correo')==''?NULL:$this->input->post('seleccione_producto_cbx_correo')),
        'subtipo_accion' => $this->input->post('tipo_correo_interno'),
        'fase' => $this->input->post('correo_seleccione_fase_cbx'),
        'modalidad' => $this->input->post('modalidad_correo_cbx'),
        'fecha_accion' => $this->input->post('fecha_correo'),
        'asunto_accion' => $this->input->post('correo_asunto_box'),
        'comentario_accion' => $this->input->post('correo_comentario_box'),
        'compromiso_accion' => $this->input->post('correo_compromiso_cbx'),
        'fecha_compromiso_accion' => $this->input->post('correo_fecha_compromiso'),
        'contacto_origen' => $this->input->post('correo_contacto_origen_cbx'),
        'asistentes_pedidos' => null,
        'comentario_otros' => $this->input->post('comentario_otros_correo')
    );
    $respuesta = $this->modelo->set_arr_agregar_accion_cliente($arr_input_data);
    echo json_encode($respuesta);
}


function Agregar_Accion_Reunion() {
    $arr_input_data = array(
        'user_id'=> $this->session->userdata('id_user'),
        'user_perfil'=> $this->session->userdata('id_perfil'),
        'empresa_id' => $this->input->post('idempresareunion'),
        'id_accion' => '3',
        'tipo_accion' => $this->input->post('reunion_tipo_reunion_cbx'),
        'id_producto' => ($this->input->post('seleccione_producto_cbx')==''?NULL:$this->input->post('seleccione_producto_cbx')),
        'fase' => $this->input->post('reunion_seleccione_fase_cbx'),
        'modalidad' => $this->input->post('modalidad_reunion_cbx'),
        'subtipo_accion' => '0',
        'fecha_accion' => $this->input->post('reunion_fecha_reunion'),
        'asunto_accion' => $this->input->post('reunion_asunto'),
        'comentario_accion' => $this->input->post('reunion_comentario'),
        'compromiso_accion' => $this->input->post('reunion_compromiso_cbx'),
        'fecha_compromiso_accion' => $this->input->post('reunion_fecha_compromiso'),
        'contacto_origen' => $this->input->post('reunion_contacto_origen_cbx'),
        'asistentes_pedidos' => null,
        'comentario_otros' => $this->input->post('comentario_otros_reunion')
    );
    $respuesta = $this->modelo->set_arr_agregar_accion_cliente($arr_input_data);
    echo json_encode($respuesta);
}	


function Agregar_Accion_Pedido() {
    $arr_input_data = array(
        'user_id'=> $this->session->userdata('id_user'),
        'user_perfil'=> $this->session->userdata('id_perfil'),
        'empresa_id' => $this->input->post('idempresareunion'),
        'id_accion' => '5',
        'tipo_accion' => $this->input->post('pedido_modalidad_cbx'),
        'subtipo_accion' => $this->input->post('pedido_tipo_curso_cbx'),
        'fecha_accion' => $this->input->post('pedido_fecha_reunion'),
        'asunto_accion' => null,
        'comentario_accion' => $this->input->post('pedido_comentario'),
        'compromiso_accion' => null,
        'fecha_compromiso_accion' => null,
        'contacto_origen' => null,
        'asistentes_pedidos' => $this->input->post('asistentes_pedidos')
    );
    $respuesta = $this->modelo->set_arr_agregar_accion_cliente($arr_input_data);
    echo json_encode($respuesta);
}		

function getCliente($id_cliente) {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id_cliente' => $id_cliente
    );
    $datos = $this->modelo->get_arr_get_datos_cliente($arr_sesion);
    return $datos;
}	

function getContacto($id_cliente) {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id_cliente' => $id_cliente
    );
    $datos = $this->modelo->get_arr_get_datos_contacto($arr_sesion);
    return $datos;
}

function getCompromisoCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $datos = $this->modelo->get_arr_get_datos_compromiso_cbx();
    echo json_encode($datos);
}		


function getContactoOrigenCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id_empresa' => $this->input->post('id_empresa')
    );
    $datos = $this->modelo->get_arr_get_datos_contacto_origen_cbx($arr_sesion);
    echo json_encode($datos);
}	

function getTiposLlamadaCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'accion' => 1
    );
    $datos = $this->modelo->get_arr_get_tipos_accion_cbx($arr_sesion);
    echo json_encode($datos);
}


function getTiposReunion() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'accion' => 3,
        'idfase' => 1
    );
    $datos = $this->modelo->get_arr_get_tipos_accion_cbx($arr_sesion);
    return $datos;
}

    //Prueba
function getTiposCorreoInternoCbx() {
        // trae datos del cliente para el Editor de Accion
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $datos = $this->modelo->get_arr_get_tipos_correo_interno_cbx($arr_sesion);
    echo json_encode($datos);
}

    //Fin prueba


function getFase() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $datos = $this->modelo->get_arr_listar_fases_cbx($arr_sesion);
    echo json_encode($datos);
}

function getModalidad() {
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
    );
    $datos = $this->modelo->get_arr_listar_modalidades_cbx($arr_sesion);
    echo json_encode($datos);
}

function getProductos() {
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'idmodalidad' => $this->input->get('modalidad')
    );
    $datos = $this->modelo->get_arr_listar_productos_cbx($arr_sesion);
    echo json_encode($datos);
}

function getTipoLlamada() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'idfase' => $this->input->get('fase')
    );
    $datos = $this->modelo->get_arr_listar_tipos_llamada_cbx($arr_sesion);
    echo json_encode($datos);
}	

function getTipoCorreo() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'idfase' => $this->input->get('fase')
    );
    $datos = $this->modelo->get_arr_listar_tipos_correo_cbx($arr_sesion);
    echo json_encode($datos);
}	

function getTipoReunion() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'idfase' => $this->input->get('fase')
    );
    $datos = $this->modelo->get_arr_listar_tipos_reunion_cbx($arr_sesion);
    echo json_encode($datos);
}	



function getCompromisoLlamada() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'idfase' => $this->input->get('fase')
    );
    $datos = $this->modelo->get_arr_get_datos_compromiso_cbx($arr_sesion);
    echo json_encode($datos);
}

function getCompromisoCorreo() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'idfase' => $this->input->get('fase')
    );
    $datos = $this->modelo->get_arr_get_datos_compromiso_cbx($arr_sesion);
    echo json_encode($datos);
}

function getCompromisooReunion() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'idfase' => $this->input->get('fase')
    );
    $datos = $this->modelo->get_arr_get_datos_compromiso_cbx($arr_sesion);
    echo json_encode($datos);
}	



function getTipoPedido() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
			//'idmodalidad' => $this->input->get('modalidad')
    );
    $datos = $this->modelo->get_arr_listar_modalidad_pedido_cbx($arr_sesion);
    echo json_encode($datos);
}		


function getTipoCurso() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
			//'idmodalidad' => $this->input->get('modalidad')
    );
    $datos = $this->modelo->get_arr_listar_tipo_curso_pedido_cbx($arr_sesion);
    echo json_encode($datos);
}	

    //Cotización
function getFormasPago(){
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
            //'idmodalidad' => $this->input->get('modalidad')
    );
    $datos = $this->modelo->get_arr_get_formas_pago_cbx($arr_sesion);
    echo json_encode($datos);
}

function getCursosByModalidad() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil'),
        'id_modalidad' => $this->input->post('id_modalidad')
    );
    $datos = $this->modelo->get_arr_listar_cursos_by_modalidad_cbx($arr_sesion);
    echo json_encode($datos);
} 

function addCotizacion(){
    $arr_input_data = array(
        'user_id'=> $this->session->userdata('id_user'),
        'user_perfil'=> $this->session->userdata('id_perfil'),
        'idempresacotizacion' => $this->input->post('idempresacotizacion'),
        'contactos_cotizacion_cbx' => $this->input->post('contactos_cotizacion_cbx'),
        'fecha_ingreso_cotizacion' => $this->input->post('fecha_ingreso_cotizacion'),
        'fecha_termino_cotizacion' => $this->input->post('fecha_termino_cotizacion'),
        'curso_cotizacion_cbx' => $this->input->post('curso_cotizacion_cbx'),
        'cantidad_asistentes_cotizacion' => $this->input->post('cantidad_asistentes_cotizacion'),
        'formas_pago_cotizacion_cbx' => $this->input->post('formas_pago_cotizacion_cbx'),
        'descuento_cotizacion' => $this->input->post('descuento_cotizacion'),
        'observacion_cotizacion' => $this->input->post('observacion_cotizacion'),
        'valor_total' => $this->input->post('valor_total')
    );
    $respuesta = $this->modelo->set_arr_agregar_cotizacion($arr_input_data);
    echo json_encode($respuesta);
}

function getCotizaciones(){
    $arr_input_data = array(
        'user_id'=> $this->session->userdata('id_user'),
        'user_perfil'=> $this->session->userdata('id_perfil'),
        'id_empresa' => $this->input->post('id_empresa')
    );
    $datos = $this->modelo->get_arr_listar_cotizaciones($arr_input_data);
    echo json_encode($datos);
}

function getValorTotalCurso(){
    $arr_input_data = array(
        'user_id'=> $this->session->userdata('id_user'),
        'user_perfil'=> $this->session->userdata('id_perfil'),
        'id_curso' => $this->input->get('id_curso'),
        'cantidad_asistentes' => $this->input->get('cantidad_asistentes'),
        'descuento' => $this->input->get('descuento')
    );
    $datos = $this->modelo->get_valor_total_curso_cotizacion($arr_input_data);
    echo json_encode($datos);
}

function getCotizacionById($id_cotizacion){
    $arr_input_data = array(
        'user_id'=> $this->session->userdata('id_user'),
        'user_perfil'=> $this->session->userdata('id_perfil'),
        'id_cotizacion' => $id_cotizacion
    );
    $datos = $this->modelo->get_arr_listar_cotizacion_by_id($arr_input_data);
    $pdf = new GenCotizacion();
        //$datos[0]['']
    $pdf->Generar(
        $datos[0]['Curso']
        ,$datos[0]['FechaIngreso'].' - '.$datos[0]['FechaTermino']
        ,$datos[0]['NombreCliente']
        ,$datos[0]['RutCliente']
        ,$datos[0]['NombreContacto']
        ,$datos[0]['TelefonoContacto']
        ,$datos[0]['DireccionEmpresa']
        ,$datos[0]['Comuna']
        ,$datos[0]['Email']
        ,$datos[0]['Fecha']
        ,$datos[0]['Descuento']
        ,$datos[0]['FormaPago']
        ,$datos[0]['DescripcionCurso']
        ,$datos[0]['Version']
        ,$datos[0]['NombreModalidad']
        ,$datos[0]['Nivel']
        ,$datos[0]['HorasCurso']
        ,$datos[0]['ValorParticipante']
        ,$datos[0]['NumeroAlumnos']
        ,$datos[0]['Observacion']
        ,$datos[0]['ValorTotal']);
    $pdf->mostrarPdf();
}

function getCotizacionWordById($id_cotizacion)
{
 $this->load->library('word');

 $arr_input_data = array(
    'user_id'=> $this->session->userdata('id_user'),
    'user_perfil'=> $this->session->userdata('id_perfil'),
    'id_cotizacion' => $id_cotizacion
);
 $datos = $this->modelo->get_arr_listar_cotizacion_by_id($arr_input_data);

        $PHPWord = $this->word; // New Word Document

        $document = $PHPWord->loadTemplate('application\libraries\Cotizacion\cotizacion_base.docx');

        $document->setValue('curso', utf8_decode(strval($datos[0]['Curso'])));
        $document->setValue('fecha_inicio', utf8_decode($datos[0]['FechaIngreso']));
        $document->setValue('fecha_termino', utf8_decode($datos[0]['FechaTermino']));
        $document->setValue('cliente', utf8_decode($datos[0]['NombreCliente']));
        $document->setValue('rut', utf8_decode($datos[0]['RutCliente']));
        $document->setValue('contacto', utf8_decode($datos[0]['NombreContacto']));
        $document->setValue('telefono', utf8_decode($datos[0]['TelefonoContacto']));
        $document->setValue('direccion', utf8_decode($datos[0]['DireccionEmpresa']));
        $document->setValue('ciudad', utf8_decode($datos[0]['Comuna']));
        $document->setValue('email', utf8_decode($datos[0]['Email']));
        $document->setValue('fechagen', utf8_decode(substr($datos[0]['Fecha'],0,20)));
        $document->setValue('desc', utf8_decode(strval($datos[0]["Descuento"])));
        $document->setValue('pago', utf8_decode(strval($datos[0]["FormaPago"])));

        $document->setValue('descurso', utf8_decode(substr($datos[0]["DescripcionCurso"],0,100)));
        $document->setValue('version', utf8_decode($datos[0]['Version']));
        $document->setValue('modalidad', utf8_decode($datos[0]['NombreModalidad']));
        $document->setValue('nivel', utf8_decode($datos[0]['Nivel']));
        $document->setValue('horas', utf8_decode($datos[0]['HorasCurso']));
        $document->setValue('valorpar', utf8_decode($datos[0]['ValorParticipante']));
        $document->setValue('nroalum', utf8_decode($datos[0]['NumeroAlumnos']));
        $document->setValue('observa', utf8_decode(strval($datos[0]['Observacion'])));
        $document->setValue('valorTotal', utf8_decode($datos[0]['ValorTotal']));

        //$document->save('assets\plantillas\Solarsystem.docx');
        
        $temp_file = tempnam(sys_get_temp_dir(), 'PHPWord');
        ob_clean();
        $document->save($temp_file);



        header("Content-Disposition: attachment; filename='Cotizacion_".strval($id_cotizacion).".docx'");
        readfile($temp_file); // or echo file_get_contents($temp_file);
        unlink($temp_file);  // remove temp file

        /*********************************************************************
        $section = $PHPWord->createSection(); // New portrait section
        // Add text elements
        $section->addText('Hello World!');
        $section->addTextBreak(2);
        $section->addText('Mohammad Rifqi Sucahyo.', array('name'=>'Verdana', 'color'=>'006699'));
        $section->addTextBreak(2);
        $PHPWord->addFontStyle('rStyle', array('bold'=>true, 'italic'=>true, 'size'=>16));
        $PHPWord->addParagraphStyle('pStyle', array('align'=>'center', 'spaceAfter'=>100));
        // Save File / Download (Download dialog, prompt user to save or simply open it)
        $section->addText('Ini Adalah Demo PHPWord untuk CI', 'rStyle', 'pStyle');
        
        $filename='just_some_random_name.docx'; //save our document as this file name
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
        ob_clean();
        $objWriter->save('php://output');
        ***********************************************************************/
    }

    function getFechaCierreCompromiso(){
        $arr_input_data = array(
            'user_id'=> $this->session->userdata('id_user'),
            'user_perfil'=> $this->session->userdata('id_perfil'),
            'id_compromiso' => $this->input->get('id_compromiso')
        );
        $datos = $this->modelo->get_fecha_cierre_compromiso($arr_input_data);
        echo json_encode($datos);
    }
}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
