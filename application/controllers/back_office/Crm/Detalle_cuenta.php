<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-02-16 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Detalle_cuenta extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Detalle Cuenta';
    private $package = 'back_office/Crm';
    private $model = '../Home_model';
    // informacion adicional para notificaciones       
    private $model2 = 'Notificaciones_model';    
    private $view = 'Detalle_cuenta_V';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// informacion adicional para notificaciones        
        $this->load->model($this->package . '/' . $this->model2, 'modelo2');

//  Libreria de sesion
        $this->load->library('session');
    }

// 2016-05-25
// Controlador del panel de control
    public function index() {

///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
			array(
                'href' => base_url() . $this->ind . 'back_office' . '/Crm/Cuenta',
                'label' => 'Cuenta',
                'icono' => ''
            ),
			array(
                'href' => base_url() . $this->ind . 'back_office' . '/Detalle_cuenta',
                'label' => 'Detalle Cuenta',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
			array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
			array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
			array('src' => base_url() . 'assets/amelia/font-awesome-crm/css/font-awesome.css'),
			array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css')
        );


//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
			array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/Crm/cuenta/detalle_cuenta.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
			array('src' => base_url() . 'assets/amelia/js/sistema/Crm/cuenta/jasny-bootstrap.min.js')
        );

// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Home";

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
// informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();        
        $this->load->view('amelia_1/template', $data);
    }


    // informacion adicional para notificaciones    
        function getNotificaciones (){
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            
            $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
            return $datos;
        }
    

}



/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
