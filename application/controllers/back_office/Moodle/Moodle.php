<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Moodle extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Moodle';
    private $package = 'back_office/Moodle';
    private $model = 'Moodle_model';
// informacion adicional para notificaciones   
    private $model2 = 'Notificaciones_model';
    private $package2 = 'back_office/Crm';
    private $view = 'Moodle_V';
    private $controller = 'Moodle';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// informacion adicional para notificaciones        
        $this->load->model($this->package2 . '/' . $this->model2, 'modelo2');

//  Libreria de sesion
        $this->load->library('session');
    }

// 2016-05-25
// Controlador del panel de control
    public function index() {
///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Moodle/Moodle',
                'label' => 'Moodle',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Moodle',
                'label' => 'Moodle',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/chosen/bootstrap-chosen.css')
        );


//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chosen/chosen.jquery.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Moodle/moodle.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js')
        );

// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Moodle";

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $data['fichas_aprobadas'] = $this->modelo->get_fichas_aprobadas(date('Y'));
// informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $this->load->view('amelia_1/template', $data);
    }

// informacion adicional para notificaciones    
    function getNotificaciones() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo2->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

    function ping_moodle() {
        echo "Funciona correctamente!.";
    }

    function get_fichas_aprobadas() {
        $ano = $this->input->post('txtano');
        $datos = $this->modelo->get_fichas_aprobadas($ano);
        echo json_encode(array('fichas' => $datos));
    }

    function get_ficha($id_ficha) {
        $arr_data = array(
            'id_ficha' => $id_ficha,
            'estado' => 0
        );
        $datos = $this->modelo->get_alumnos_by_id_ficha($arr_data);
        $errores = $this->modelo->get_errores_by_id_ficha($arr_data);

        echo json_encode(array('alumnos' => $datos, 'errores' => $errores));
    }

    function set_cursos_moodle() {
        $arrayReturn = array();
        $fichas = $this->input->post('fichas');
        $alumnosYaEnrolados = array();
        for ($i = 0; $i < count($fichas); $i++) {
            $cursoCreado = 0;
            $ficha = $this->modelo->get_curso_by_id_ficha(array('id_ficha' => $fichas[$i]));
            $id_curso_moodle = $ficha[0]['id_moodle'];
            //if (($cursoCreado = $this->modelo->existeCursoMoodle($ficha[0]['id_ficha'], $ficha[0]['id_curso'], $shortNameCurso)) == 0) {
            //$cursoCreado = $this->modelo->crearCurso($ficha[0]['id_ficha'], $ficha[0]['id_curso'], $shortNameCurso, $shortNameCurso, 1);
            //}
            $cursoCreado = $this->modelo->existeCursoMoodle($ficha[0]['id_ficha'], $ficha[0]['id_curso'], $id_curso_moodle);

            if ($cursoCreado > 0) {
                $alumnosEnrolados = $this->modelo->getAlumnosCurso($cursoCreado);
                $alumnos = $this->modelo->get_alumnos_by_id_ficha(array('id_ficha' => $fichas[$i]));
                $alumnoCreado = 0;
                $alumnosEnrolar = array();
                for ($j = 0; $j < count($alumnos); $j++) {
                    if (($alumnoCreado = $this->modelo->existeUsuarioMoodle($ficha[0]['id_ficha'], $ficha[0]['id_curso'], $alumnos[$j]['rut_alumno'])) == 0) {
                        $alumnoCreado = $this->modelo->crearAlumno($ficha[0]['id_ficha'], $ficha[0]['id_curso'], $alumnos[$j]['rut_alumno'], $alumnos[$j]['rut_alumno'], $alumnos[$j]['nombre_alumno'], $alumnos[$j]['apellido_paterno_alumno'], $alumnos[$j]['correo_contacto']);
                    }
                    $alumnosEnrolar[] = $alumnoCreado;
                }
                if ($this->modelo->enrolarAlumnos($ficha[0]['id_ficha'], $ficha[0]['id_curso'], $alumnosEnrolar, $cursoCreado)) {
                    for ($j = 0; $j < count($alumnosEnrolados); $j++) {
                        for ($k = 0; $k < count($alumnos); $k++) {
                            if ($alumnosEnrolados[$j]->username == $alumnos[$k]['rut_alumno']) {
                                $alumnosYaEnrolados[] = $alumnosEnrolados[$j];
                                $data = array(
                                    'user_id' => 1,
                                    'user_perfil' => 1,
                                    'id_cliente' => 1,
                                    'nombre_metodo' => 'existe_en_'.$id_curso_moodle,
                                    'msg_excepcion' => 'Usuario '.$alumnos[$k]['rut_alumno']." ya se encuentra enrolado en Moodle. (Curso ID: ".$id_curso_moodle.")",
                                    'id_ficha' => $ficha[0]['id_ficha'],
                                    'id_curso' => $ficha[0]['id_curso'],
                                    'estado' => 0
                                );
                                $this->modelo->add_error($data);
                            }
                        }
                    }
                    $data = array(
                        'user_id' => 1,
                        'user_perfil' => 1,
                        'id_cliente' => 1,
                        'nombre_metodo' => '',
                        'msg_excepcion' => '',
                        'id_ficha' => $ficha[0]['id_ficha'],
                        'id_curso' => $ficha[0]['id_curso'],
                        'estado' => 1
                    );
                    $this->modelo->add_error($data);
                }
            } else {
                $data = array(
                    'user_id' => 1,
                    'user_perfil' => 1,
                    'id_cliente' => 1,
                    'nombre_metodo' => 'existe_curso',
                    'msg_excepcion' => 'El curso moodle no existe',
                    'id_ficha' => $ficha[0]['id_ficha'],
                    'id_curso' => $ficha[0]['id_curso'],
                    'estado' => 0
                );
                $this->modelo->add_error($data);
            }
        }
        echo json_encode(array("Msg" => 'Proceso completado', "AlumnosEnrols" => $alumnosYaEnrolados));
    }

    function envio_carta_bienvenida() {
        $this->load->library('Libreriaemail');


        $datos = $this->modelo->get_alumnos_envio_carta();

        $html = implode('', file(dirname(__FILE__) . "/CartaEnvio.html"));

        for ($i = 0; $i < count($datos); $i++) {
            if ($this->comprobar_email($datos[$i]['correo_contacto'])) {
                $htmlEnvio = "";
                $htmlEnvio = str_replace("&nombre", $datos[$i]['nombre_alumno'], $html);
                $htmlEnvio = str_replace("&fechaCierre", $datos[$i]['fecha_cierre'], $htmlEnvio);
                $htmlEnvio = str_replace("&url", "[ESCRIBIR URL]", $htmlEnvio);
                $htmlEnvio = str_replace("&usuario", $datos[$i]['rut_alumno'], $htmlEnvio);
                $data_email = array(
                    "destinatarios" => array('email' => $datos[$i]['correo_contacto'], 'nombre' => $datos[$i]['nombre_alumno']),
                    "cc_a" => array(),
                    "asunto" => "Test no considerar, favor eliminar",
                    "body" => $htmlEnvio,
                    "AltBody" => "Test no considerar, favor eliminar"
                );
                $res = $this->libreriaemail->CallAPISendMail($data_email);

                $this->modelo->set_alumno_envio_carta(array("id_ficha" => $datos[$i]['id_ficha'], "id_alumno" => $datos[$i]['id_alumno']));
            }
        }
    }

    function comprobar_email($email) {
        $mail_correcto = 0;
        if ((strlen($email) >= 6) && (substr_count($email, "@") == 1) && (substr($email, 0, 1) != "@") && (substr($email, strlen($email) - 1, 1) != "@")) {
            if ((!strstr($email, "'")) && (!strstr($email, "\"")) && (!strstr($email, "\\")) && (!strstr($email, "\$")) && (!strstr($email, " "))) {
                if (substr_count($email, ".") >= 1) {
                    $term_dom = substr(strrchr($email, '.'), 1);
                    if (strlen($term_dom) > 1 && strlen($term_dom) < 5 && (!strstr($term_dom, "@"))) {
                        $antes_dom = substr($email, 0, strlen($email) - strlen($term_dom) - 1);
                        $caracter_ult = substr($antes_dom, strlen($antes_dom) - 1, 1);
                        if ($caracter_ult != "@" && $caracter_ult != ".") {
                            $mail_correcto = 1;
                        }
                    }
                }
            }
        }
        if ($mail_correcto)
            return 1;
        else
            return 0;
    }

    public function get_cursos_evaluacion() {
        $shortnames = $this->modelo->get_curso_evaluacion_pendientes();
        if (count($shortnames) > 0) {
            for ($i = 0; $i < count($shortnames); $i++) {
                $courseid = $this->modelo->getCursoMoodleByShortname($shortnames[$i]['shortname_moodle']);
                if ($courseid > 0) {
                    $exito = $this->modelo->cursos_evaluacion($courseid, $shortnames[$i]['id_ficha']);
                    if ($exito == 1) {
                        echo $shortnames[$i]['shortname_moodle'] . " nota OK";
                    } else {
                        echo $shortnames[$i]['shortname_moodle'] . " nota ERROR";
                    }
                } else {
                    echo $shortnames[$i]['shortname_moodle'] . "no existe en moodle";
                }
            }
        } else {
            echo 'No existen cursos para insertar a la fecha.';
        }
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */

       