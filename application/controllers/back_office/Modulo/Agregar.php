<?php

-/**
         * Agregar
         *
         * Description...
         *
         * @version 0.0.1
         *
         * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
         * Fecha creacion:	2016-11-21 [Marcelo Romero] <mromero@edutecno.com>
         */
        defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Agregar';
    private $nombre_item_plural = 'Agregar';
    private $package = 'back_office/Modulo';
    private $model = 'Modulo_model';
    private $view = 'Agregar_v';
    private $controller = 'Agregar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        //  Libreria de sesion
        $this->load->library('session');
        //  $this->load->library('form_validation');
        //  $this->load->library('AddForms_lib', 'addforms_lib');
    }

    // 2016-10-11
    // Controlador del Agregar
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Modulo/agregar.js')
        );

        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
		$data['activo'] = "Lista Modulos";
		if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
        //  cargo el menu en la session
    }

    function RegistraModulo() {

        // Ultima fecha Modificacion: 2016-11-21
        // Descripcion:  Metodo para registrar un nuevo módulo
        // Usuario Actualizador : Marcelo Romero
        //  $this->input->post('nombre') : Recibe los parametros enviados por post
        $arr_input_data = array(
            'nombre_m' => $this->input->post('nombre_v'),
            'titulo_m' => $this->input->post('titulo_v'),
            'descripcion_m' => $this->input->post('descripcion_v'),
            'icon_m' => $this->input->post('icon_v')
        );

        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_agregar_modulo($arr_input_data);
        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
