<?php

/**
 * Listar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-11 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-12-11 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Docente';
    private $model = 'Docente_model';
    private $view = 'Listar_v';
    private $controller = 'Listar';
    private $ind = '';
    private $package2 = 'back_office/Usuario';
    private $model2 = 'Usuario_model';

    function __construct() {

        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'modelousuario');

        //  Libreria de sesion
        $this->load->library('session');
    }

    // Controlador del panel de control
    public function index() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );


        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Docente/Listar.js')
        );


        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Docente";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data['menus'] = $this->session->userdata('menu_usuario');

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function listarDocentes() {

        $conocimiento_filtro_cbx = $this->input->post('conocimiento_filtro_cbx');
        $nivel_filtro_cbx = $this->input->post('nivel_filtro_cbx');


        if ($conocimiento_filtro_cbx == "" || $conocimiento_filtro_cbx == null) {

            $conocimiento_filtro_cbx = "";
        }
        if ($nivel_filtro_cbx == "" || $nivel_filtro_cbx == null) {

            $nivel_filtro_cbx = "";
        }


        $datos_menu['conocimiento_filtro_cbx'] = $conocimiento_filtro_cbx;
        $datos_menu['nivel_filtro_cbx'] = $nivel_filtro_cbx;
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $arr_sesion = array(
            'conocimiento_filtro_cbx' => $datos_menu['conocimiento_filtro_cbx'],
            'nivel_filtro_cbx' => $datos_menu['nivel_filtro_cbx'],
            'user_id' => $datos_menu['user_id'],
            'user_perfil' => $datos_menu['user_perfil']
        );

     
        $datos = $this->modelo->get_arr_listar_docente($arr_sesion);
        echo json_encode($datos);
    }

    function getSkillsByUsuario() {
        $id_usuario = $this->input->post('id_usuario');

        $arr_datos = array(
            'id_usuario' => $id_usuario,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelousuario->get_usuario_skills($arr_datos);

        echo json_encode($data);
        ;
    }

    function getSkillById() {
        $id_skill = $this->input->post('id_skill');

        $arr_datos = array(
            'id_skill' => $id_skill,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelousuario->get_skill_by_id($arr_datos);

        echo json_encode($data);
        ;
    }

    function updateSkill() {
        $id_skill = $this->input->post('id_skill');
        $id_nivel_conocimiento = $this->input->post('id_nivel_conocimiento');

        $arr_datos = array(
            'id_skill' => $id_skill,
            'id_nivel_conocimiento' => $id_nivel_conocimiento,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelousuario->update_usuario_skill($arr_datos);

        echo json_encode($data);
        ;
    }

    function guardarSkill() {
        $id_conocimiento = $this->input->post('id_conocimiento');
        $id_nivel_conocimiento = $this->input->post('id_nivel_conocimiento');
        $id_usuario = $this->input->post('id_usuario');

        $arr_datos = array(
            'id_conocimiento' => $id_conocimiento,
            'id_nivel_conocimiento' => $id_nivel_conocimiento,
            'id_usuario' => $id_usuario,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelousuario->insert_usuario_skill($arr_datos);

        echo json_encode($data);
        ;
    }

    function deleteSkillRelator() {
        $id_skill = $this->input->post('id_skill');

        $arr_datos = array(
            'id_skill' => $id_skill,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelousuario->delete_usuario_skill($arr_datos);

        echo json_encode($data);
        ;
    }

    function CambiarEstadoUsuario() {

        $id_docente = $this->input->post('id');
        $estado = $this->input->post('estado');

        $arr_datos = array(
            'id_docente' => $id_docente,
            'estado' => $estado,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $data = $this->modelousuario->set_sp_usuario_change_status($arr_datos);

        echo json_encode($data);
    }

    function getConocimientosCBX() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelousuario->get_arr_conocimientos_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getNivelConocimientosCBX() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelousuario->get_arr_nivel_conocimientos_cbx($arr_sesion);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
