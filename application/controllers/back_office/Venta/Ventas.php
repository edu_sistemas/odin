<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:  2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Panel de Control';
    private $nombre_item_plural = 'Panel de Control';
    private $package = 'back_office/Venta';
    private $model = 'Venta_model';
    private $view = 'Ventas_v';
    private $controller = 'Ventas';
    private $empresa = 'back_office/Empresa/Empresa_model';
    private $holding = 'back_office/Holding/Holding_Model';
    private $ind = '';
    private $package2 = 'back_office/Ficha_documento';
    private $model2 = 'Ficha_documento_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $ficha = 'back_office/Ficha/Ficha_model';
    private $fichacontrol = 'back_office/Ficha_Control/Ficha_Control_model';



    public $rm = 'back_office/ResponseModel/ResponseModel';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->package2 . '/' . $this->model2, 'documento');
        $this->load->model($this->empresa, 'modelempresa');
        $this->load->model($this->holding, 'modelholding');
        $this->load->model($this->usuario, 'modelousuario');
        $this->load->model($this->ficha, 'modeloficha');


        $this->load->model($this->rm, 'modelrm');

//  Libreria de sesion
        $this->load->library('session');
        $this->load->library('Functions');
    }

// 2016-05-25
// Controlador del panel de control
    public function index() {

///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Consulta',
                'label' => 'Ventas',
                'icono' => ''
            )
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/bootstrap-datepicker/bootstrap-datepicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/fileupload/jquery.fileupload.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),

            array('src' => base_url() . 'assets/ini/ini.css')
        );

//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/vendor/jquery.ui.widget.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.iframe-transport.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fileupload/js/jquery.fileupload.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Venta/ventas.js'),

            array('src' => base_url() . 'assets/ini/ini.js'),
            array('src' => base_url() . 'assets/ini/jqueryform.js')
        );




// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Ventas Mensuales";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

///  cargo el menu en la session
//  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu


        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function consultarVentas() {
        $rm = $this->modelrm;
        
        $ejecutivo = $this->input->post('ejecutivo');
        $periodo = $this->input->post('periodo');

        $arr_data = array(
            'ejecutivo' => $ejecutivo,
            'periodo' => $periodo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_consultar_ventas($arr_data);

        if($datos){
            $rm->function = "cargaTablaVentas()";
            $rm->setResult(true, $datos);
        }

        echo json_encode($this->modelrm);
    }

    function getEjecutivo() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial_user($arr_data);
        echo json_encode($datos);
    }

    function getPeriodo() {

        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modeloficha->get_arr_listar_periodo_venta($arr_data);
        echo json_encode($datos);
    }

    function consultarVentasPorEjecutivo() {

        $ejecutivo = $this->input->post('ejecutivo');
        $periodo = $this->input->post('periodo');

        $arr_data = array(
            'ejecutivo' => $ejecutivo,
            'periodo' => $periodo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_consultar_ventas_por_ejecutivos($arr_data);
        echo json_encode($datos);
    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
