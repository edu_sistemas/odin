<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-05-25 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-05-25 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class InformeVentaCursos extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Informe Venta Curso';
    private $nombre_item_plural = 'InformeVentaCursos';
    private $package = 'back_office/Venta';
    private $model = 'Venta_model';
    private $view = 'InformeVentaCursos_v';
    private $controller = 'InformeVentaCursos';
    private $ind = '';

    


    public $rm = 'back_office/ResponseModel/ResponseModel';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        $this->load->model($this->rm, 'modelrm');

        $this->load->library('Functions');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-05-25
    // Controlador del panel de control
    public function index() {


        ///  array ubicaciones
        $arr_page_breadcrumb = array(
            array
                (
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ), array
                (
                'href' => base_url() . $this->ind . $this->package . '/InformeVentaCursos',
                'label' => $this->nombre_item_singular,
                'icono' => ''
            )
        );

        // array con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),

            array('src' => base_url() . 'assets/ini/ini.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Venta/InformeVentaCursos.js'),

            array('src' => base_url() . 'assets/ini/ini.js'),
            array('src' => base_url() . 'assets/ini/jqueryform.js')
        );

        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Ventas Curso";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    function consultarInformeVentaCursos() {
        $rm = $this->modelrm;

        $funciones = new Functions();

        $start = $funciones->formatDateBD($this->input->post('start'));
        $end = $funciones->formatDateBD($this->input->post('end'));
        $opcion = $this->input->post('optionsRadios');


        $arr_data = array(
            'fecha_inicio' => $start,
            'fecha_termino' => $end,
            'opcion' => $opcion,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_consultar_informe_venta_cursos($arr_data);

        if($datos){
            $rm->function = "cargaTablaInformeVentaCursos()";
            $rm->setResult(true, $datos);
        }

        echo json_encode($this->modelrm);

    }

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
