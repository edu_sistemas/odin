<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-06-01 [Veronica Morales] <vmorales@edutecno.com>
 * Fecha creacion:  2018-06-01 [Veronica Morales] <vmorales@edutecno.com>
 */
    defined('BASEPATH') OR exit('No direct script access allowed');

    class CuentaSupervisor extends MY_Controller {

        // Variables paramétricas
        private $nombre_item_singular = 'CuentaSupervisor';
        private $package = 'back_office/Supervisor';
        private $model = 'CuentaSupervisor_model';
        private $empresa = 'back_office/Empresa/Empresa_model';
        private $usuario = 'back_office/Usuario/Usuario_model';
        private $view = 'CuentaSupervisor_V';
        private $ind = '';

        function __construct() {
            parent::__construct();

            //  Carga el modelo que utiliza el controlador
            $this->load->model($this->package . '/' . $this->model, 'modelo');
            $this->load->model($this->empresa, 'modelempresa');
            $this->load->model($this->usuario, 'modelousuario');
            //  Libreria de sesion
            $this->load->library('session');
            $this->load->library('excel');
        
        }

        // Controlador del panel de control
        public function index() {
            ///  array ubicaciones
            $arr_page_breadcrumb = array
            (
                array(
                    'href' => base_url() . $this->ind . 'back_office' . '/Home',
                    'label' => 'Home',
                    'icono' => ''
                ),
			    array(
                    'href' => base_url() . $this->ind . $this->package . '/CuentaSupervisor',
                    'label' => 'CuentaSupervisor',
                    'icono' => ''
                )
            );

            $arr_theme_css_files = array
            (
                array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
                array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
                array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
                array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
			    array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
			    array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
            );

            //  js necesarios para la pagina
            $arr_theme_js_files = array
            (			
                array('src' => base_url() . 'assets/amelia/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
			    array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
			    array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
                array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
			    array('src' => base_url() . 'assets/amelia/js/sistema/Supervisor/CuentaSupervisor.js'),
                array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js')
            );

            // informacion adicional para la pagina
            $data['page_title'] = '';
            $data['page_title_small'] = '';
            $data['panel_title'] = $this->nombre_item_singular;
            $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
            $data['script_adicional'] = $arr_theme_js_files;
            $data['style_adicional'] = $arr_theme_css_files;
        
            ////**** Obligatorio ****** //////
            $datos_menu['user_id'] = $this->session->userdata('id_user');
            $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
            $data['id_perfil'] = $this->session->userdata('id_perfil');
            $data['activo'] = "Cuenta Supervisor";

            ///  cargo el menu en la session
            //   Carga Menu dentro de la pagina
            $data['menus'] = $this->session->userdata('menu_usuario');
            $data['id_perfil'] = $this->session->userdata('id_perfil');
            $data['page_menu'] = 'dashboard';
            $data['main_content'] = $this->package . '/' . $this->view;

            $this->load->view('amelia_1/template', $data);
        }

        function getCliente() {
            $id_empresa = $this->input->post('id_empresa');
            $id_ejecutivo = $this->input->post('id_ejecutivo');
            $fecha = $this->input->post('mes');
            $datos_menu ['user_id'] = $this->session->userdata('id_user');
            $datos_menu ['user_perfil'] = $this->session->userdata('id_perfil');

            $arr_sesion = array(
                'fecha' => $fecha,
                'id_empresa' => $id_empresa,
                'id_ejecutivo' => $id_ejecutivo,
                'user_id' => $datos_menu['user_id'],
                'user_perfil' => $datos_menu['user_perfil']
            );
            $datos = $this->modelo->get_arr_listar_clientes($arr_sesion);
            echo json_encode($datos);
        }

        function getUsuarioTipoEcutivoComercial() {
            $arr_data = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
            echo json_encode($datos);
        }

        function getEmpresaCBX() {
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
            echo json_encode($datos);
        }

        function getHistorialGestion() {
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $datos = $this->modelo->get_arr_listar_historial_gestion($arr_sesion);
            echo json_encode($datos);
        }

        function getHistorialGestionDownload() {
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $respuesta = $this->modelo->get_arr_listar_gestiones_download($arr_sesion);
            echo json_encode($respuesta);
        }   
    
        function getHistorialDominiosDownload() {
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $respuesta = $this->modelo->get_arr_listar_dominios_download($arr_sesion);
            echo json_encode($respuesta);
        }  

 
       function getClientesConEjecutivos(){
            $arr_sesion = array(
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $respuesta = $this->modelo->get_arr_listar_clientes_ejecutivos($arr_sesion);
            echo json_encode($respuesta);
        }

        
       

        function generarExcelGestiones($id_ejecutivo, $id_empresa, $fecha) {
            if($fecha == 0){
                $fecha = '';
            }
             if($id_ejecutivo == 0){
                $id_ejecutivo = '';
            }
             if($id_empresa == 0){
                $id_empresa = '';
            }

             $arr_datos = array(
                'fecha' => $fecha,
                'id_empresa' => $id_empresa,
                'id_ejecutivo' => $id_ejecutivo,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );

           
           
            $this->load->library('excel');

            //activate worksheet number 1
            $this->excel->setActiveSheetIndex(0);
            //name the worksheet
            $this->excel->getActiveSheet()->setTitle('Gestiones Ejecutivos');

             $data_excel = $this->modelo->get_arr_datos_gestiones_ejecutivos_excel($arr_datos);
            //set cell A1 content with some text

            $this->excel->getActiveSheet()->SetCellValue('A1', 'CLIENTE');        
            $this->excel->getActiveSheet()->SetCellValue('B1', 'HOLDING');
            $this->excel->getActiveSheet()->SetCellValue('C1', 'STATUS GESTION');
            $this->excel->getActiveSheet()->SetCellValue('D1', 'ESTADO CUENTA');
            $this->excel->getActiveSheet()->SetCellValue('E1', 'RUT EMPRESA');
            $this->excel->getActiveSheet()->SetCellValue('F1', 'EJECUTIVO');
            
            $contador = count($data_excel);
            $numero = 2;

            for ($i=0; $i <$contador ; $i++) { 
                $this->excel->getActiveSheet()->SetCellValue('A' . $numero, $data_excel[$i]['nombre_cliente']); 
                $this->excel->getActiveSheet()->SetCellValue('B' . $numero, $data_excel[$i]['holding']); 
                $this->excel->getActiveSheet()->SetCellValue('C' . $numero, $data_excel[$i]['q_dias']); 
                $this->excel->getActiveSheet()->SetCellValue('D' . $numero, $data_excel[$i]['estado_empresa']); 
                $this->excel->getActiveSheet()->SetCellValue('E' . $numero, $data_excel[$i]['rut_empresa']); 
                $this->excel->getActiveSheet()->SetCellValue('F' . $numero, $data_excel[$i]['ejecutivo']); 
                $numero ++;
            }
            $this->excel->getActiveSheet()->getStyle('A1:F1')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '#6a81a5')
                        )
                    )
                );
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth('27');   
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth('15');
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth('16');
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth('29');
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth('20');
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth('75');
        
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(10);
            //make the font become bold
            $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
            //set aligment to center for that merged cell (A1 to D1)
            $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $filename = 'GestionesEjecutivos_' . date('dmYHis') . '.xls'; //save our workbook as this file name  sp_select_jd_filter
            ob_clean();
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: private'); //no cache
            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
        }
}






