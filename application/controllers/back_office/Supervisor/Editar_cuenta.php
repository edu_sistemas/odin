<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  
 * Fecha creacion:  
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class editar_cuenta extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'Editar Cuenta';
    private $package = 'back_office/Supervisor';
    private $model = 'Editar_cuenta_model';
    private $view = 'Editar_cuenta_V';
    private $ind = '';

    function __construct() {
        parent::__construct();

//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');    
//  Libreria de sesion
        $this->load->library('session');
    }

// Controlador del panel de control
    public function index($varsplit) {
        $buscarpto = strpos($varsplit, '.');
       if ($buscarpto > 0){
            $varsplit = explode(".",$varsplit);
            $id_empresa = $varsplit[0];
             $id_sugerido = $varsplit[1];
        }else{
       $id_empresa = $varsplit;
       $id_sugerido = 0;
    }
      
///  array ubicaciones
        $arr_page_breadcrumb = array
        (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
                ),
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Supervisor/CuentaSupervisor',
                'label' => 'Cuenta',
                'icono' => ''
                ),
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Editar_Cuenta',
                'label' => 'Ver Cuenta',
                'icono' => ''
                )
            );

        $arr_theme_css_files = array
        (
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/custom.css'),
            array('src' => base_url() . 'assets/amelia/font-awesome/css/font-awesome.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
			array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css')
            );


//  js necesarios para la pagina
        $arr_theme_js_files = array
        (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Supervisor/editar_cuenta.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
			array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js')
            //array('src' => base_url() . 'assets/amelia/js/sistema/Crm/cuenta/jasny-bootstrap.min.js')         
            );

// informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

//Carga datos empresa desde BBDD
        $arr_input_data = array(
            'id_empresa' => $id_empresa,
            'id_sugerido' => $id_sugerido,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
            );

        $data['datos_empresa'] = $this->modelo->get_arr_datos_by_id($arr_input_data);


        $datosSP= array('id_empresa' => $id_empresa, 'id_contacto' => "0");
        $data['datos_contacto'] = $this->modelo->get_arr_contacto_empresa($datosSP);


////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Home";

//  cargo el menu en la session
//   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
   }

    function Set_Cuenta_Editar() {
        $arr_input_data = array(
            'user_id'=> $this->session->userdata('id_user') ,
            'user_perfil'=> $this->session->userdata('id_perfil'),
            'Var_id_empresa'=> $this->input->post('Var_id_empresa'),                    
            'Var_rut_empresa'=> $this->input->post('Var_rut_empresa').'-'.$this->input->post('dv'), 
            'Var_razon_social_empresa'=> $this->input->post('Var_razon_social_empresa'),
            'Var_giro_empresa'=> $this->input->post('Var_giro_empresa'),
            'Var_nombre_fantasia'=> $this->input->post('Var_nombre_fantasia'),
            'Var_direccion_empresa'=> $this->input->post('Var_direccion_empresa'),
            'Var_id_comuna'=> $this->input->post('Var_crm_id_comuna'),        
            'Var_id_ciudad_empresa'=> '1',              
            'Var_descripcion_empresa'=> $this->input->post('Var_descripcion_empresa'),
            'Var_id_holding'=> $this->input->post('Var_id_holding'),
            'Var_telefono_temporal'=> '1',
            'Var_direccion_factura'=> $this->input->post('Var_direccion_factura'),
            'Var_id_comuna_facturacion'=> '1',
            'Var_id_ciudad_facturacion'=> '1',
            'Var_direccion_despacho'=> $this->input->post('Var_direccion_factura'),
            'Var_mail_sii'=> '1',
            'Var_nombre_contacto_cobranza'=> $this->input->post('Var_nombre_contacto_cobranza'),
            'Var_mail_contacto_cobranza'=> $this->input->post('Var_mail_contacto_cobranza'),
            'Var_telefono_contacto_cobranza'=> $this->input->post('Var_telefono_contacto_cobranza'),
            'Var_requiere_orden_compra'=> $this->input->post('Var_requiere_orden_compra'),
            'Var_requiere_hess'=> $this->input->post('Var_requiere_hess'),
            'Var_requiere_numero_contrato'=> '1',
            'Var_requiere_ota'=> $this->input->post('Var_requiere_ota'),
            'Var_observaciones_glosa_facturacion'=> $this->input->post('Var_observaciones_glosa_facturacion'),
            'Var_url_logo_empresa'=> '1',
            'Var_estado_empresa'=> '1',
            'Var_crm_clasificacion_cuenta'=> $this->input->post('Var_crm_clasificacion_cuenta'),
            'Var_crm_pais_cuenta'=> $this->input->post('Var_crm_id_pais'),
            'Var_crm_otic_cuenta'=> $this->input->post('Var_crm_otic_cuenta'),
            'Var_crm_industria'=> '1',
            'Var_crm_descripcion_cuenta'=> $this->input->post('Var_descripcion_empresa'),
            'Var_crm_oc_interna'=> $this->input->post('Var_crm_oc_interna'),
            'Var_crm_otro'=> $this->input->post('Var_observaciones_glosa_facturacion'),
            'Var_crm_sii'=> ($this->input->post('Var_crm_sii')!=null)?$this->input->post('Var_crm_sii'):0,
            'Var_crm_mano_timbrada'=> ($this->input->post('Var_crm_mano_timbrada')!=null)?$this->input->post('Var_crm_mano_timbrada'):0,
            'Var_crm_chilexpress'=> ($this->input->post('Var_crm_chilexpress')!==null)?$this->input->post('Var_crm_chilexpress'):0,
            'Var_crm_direccion_entrega_diploma'=> $this->input->post('Var_crm_direccion_entrega_diploma'),
            'Var_crm_contacto_persona_diploma'=> $this->input->post('Var_crm_contacto_persona_diploma'),
            'Var_crm_telefono_oficina_diploma'=> $this->input->post('Var_crm_telefono_oficina_diploma'),
            'Var_crm_telefono_movil_diploma'=> $this->input->post('Var_crm_telefono_movil_diploma'),
            'Var_crm_correo_diploma'=> $this->input->post('Var_crm_correo_diploma'),
			'Var_crm_id_ejecutivo'=> 0,
			'Var_crm_tamano_empresa'=> $this->input->post('Var_crm_tamano_empresa'),
			'Var_crm_rubro'=> $this->input->post('tab1_detalle_cuenta_rubro'),
			'Var_crm_subrubro'=> $this->input->post('tab1_detalle_cuenta_subrubro')
            );

        $respuesta = $this->modelo->set_arr_editar_cuenta($arr_input_data);
        echo json_encode($respuesta);
        
    }
    function cargar_contactos()
    {
    $datosSP = array('id_empresa'=> $this->input->post('id_empresa'), 'id_contacto'=> "0");
       $datos = $this->modelo->get_arr_contacto_empresa($datosSP);
       echo json_encode($datos);
    }


    function getContacto() {
       $datosSP = array('id_empresa'=> "0", 'id_contacto'=> $this->input->post('id_contacto'));
       $datos = $this->modelo->get_arr_contacto_empresa($datosSP);
       echo json_encode($datos);
   }

  
//--------------------------PRUEBA HOLDING------------------------------

function getComboHolding() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
        );
    $datos = $this->modelo->get_arr_combo_holding_cbx($arr_sesion);
    echo json_encode($datos);
}

//--------------------------PRUEBA CLASIFICACION CUENTA------------------------------

function getComboClasificacionCuenta() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
        );
    $datos = $this->modelo->get_arr_combo_clasificacion_cuenta_cbx($arr_sesion);
    echo json_encode($datos);
}

//--------------------------PRUEBA EJCUTIVO ASIGNADO------------------------------

function getComboEjecutivoAsignado() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
        );
    $datos = $this->modelo->get_arr_ejecutivo_asignado_cbx($arr_sesion);
    echo json_encode($datos);
}

//--------------------------PRUEBA RUBRO EMPRESA------------------------------

function getComboRubroEmpresa() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
        );
    $datos = $this->modelo->get_arr_rubro_empresa_cbx($arr_sesion);
    echo json_encode($datos);
}

//--------------------------PRUEBA SUBRUBRO EMPRESA------------------------------

function getComboSubRubroEmpresa() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
        );
    $datos = $this->modelo->get_arr_subrubro_empresa_cbx($arr_sesion);
    echo json_encode($datos);
}

//--------------------------PRUEBA TAMAÑO EMPRESA------------------------------

function getComboTamanoEmpresa() {
        // carga el combo box tamaño empresa
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
        );
    $datos = $this->modelo->get_arr_tamano_empresa_cbx($arr_sesion);
    echo json_encode($datos);
}

//--------------------------PRUEBA PAIS CUENTA------------------------------

function getComboPaisCuenta() {
        // carga el combo box tamaño empresa
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
        );
    $datos = $this->modelo->get_arr_pais_cuenta_cbx($arr_sesion);
    echo json_encode($datos);
}

function getComboComuna() {
        // carga el combo box grupo ejecutivo
    $arr_sesion = array(
        'user_id' => $this->session->userdata('id_user'),
        'user_perfil' => $this->session->userdata('id_perfil')
        );
    $datos = $this->modelo->get_arr_combo_comuna_cbx($arr_sesion);
    echo json_encode($datos);
}

}

/* End of file PanelDeControl_c.php */
/* Location: ./application/controllers/PanelDeControl_c.php */
