<?php

/**
 * Confirmar
 * 
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-19 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-12-19 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Confirmar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Confirmar';
    private $nombre_item_plural = 'Confirmar';
    private $package = 'back_office/Reserva';
    private $model = 'Reserva_model';
    private $view = 'Confirmar_v';
    private $controller = 'Confirmar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        // $this->load->model($this->perfil, 'modelperfil');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . '/back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/fullcalendar/fullcalendar.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datetimepicker/bootstrap-datetimepicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/tagsinput/bootstrap-tagsinput.css'),
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/moment.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/fullcalendar.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/es.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/pace/pace.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/tagsinput/bootstrap-tagsinput.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reserva/confirmar.js')
        );



        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Confirmar Reserva";
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///$arr_input_data = array(
           // 'id_reserva' => $id_reserva,
           // 'id_detalle_sala' => $id_detalle_sala,
           // 'user_id' => $this->session->userdata('id_user'),
           // 'user_perfil' => $this->session->userdata('id_perfil')
       // );

        //$data['data_reserva'] = $this->modelo->get_arr_reserva_por_confirmar($arr_input_data);

        //   Carga Menu dentro de la paginas
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;


        $this->load->view('amelia_1/template', $data);
    }

    function getFeriados() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_calendario_get_feriados($arr_sesion);
        echo json_encode($datos);
    }

    function getSalas() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_sala_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function getEmpresas() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_empresas_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function llenarCalendar() {
        $arr_sesion = array(
           'user_id' => $this->session->userdata('id_user'),
           'user_perfil' => $this->session->userdata('id_perfil'),
           'id_sala_h' => $this->uri->segment(5)
        );
        $datos = $this->modelo->get_arr_completar_calendar_by_sala($arr_sesion);
        echo json_encode($datos);
    }

    function listaReservasbyUser() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_reserva_by_id_user($arr_sesion);
        $mi_arr = array();
        for ($i = 0; $i < sizeof($datos); $i++) {
            array_push($mi_arr, array('id' => $datos[$i]['id_reserva'], 'value' => $datos[$i]['detalle_reserva']));
        }
        echo json_encode($mi_arr);
    }

    function getReservaPorConf() {
        $arr_sesion = array(
            'id_reserva' => $this->input->post('id_reserva'),
            'id_detalle_sala'=> $this->input->post('id_detalle_sala'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_reserva_por_confirmar($arr_sesion);

        $horai = explode(':',$datos[0]["hora_inicio"])[0];
        $mini = explode(':',$datos[0]["hora_inicio"])[1];
        $hora_inicio = $horai . ':' . $mini;
        
        $horaf = explode(':',$datos[0]["hora_termino"])[0];
        $minf = explode(':',$datos[0]["hora_termino"])[1];
        $hora_termino = $horaf . ':' . $minf;

        $añoi = explode('-',$datos[0]["inicio"])[0];
        $mesi = explode('-',$datos[0]["inicio"])[1];
        $diai = explode('-',$datos[0]["inicio"])[2];
        $fecha_inicio = $diai . '-' . $mesi . '-' . $añoi;
        
        $añof = explode('-',$datos[count($datos) - 1]["end"])[0];
        $mesf = explode('-',$datos[count($datos) - 1]["end"])[1];
        $diaf = explode('-',$datos[count($datos) - 1]["end"])[2];
        $fecha_termino = $diaf . '-' . $mesf . '-' . $añof;

        $lunes = $martes = $miercoles = $jueves = $viernes = $sabado = $domingo = 0;
        for ($i = 0; $i < count($datos); $i++){
           switch ($datos[$i]["dia"]){
                case '2':
                    $lunes = 1;
                break;
                case '3':
                    $martes = 1;
                break;
                case '4':
                    $miercoles = 1;
                break;
                case '5':
                    $jueves = 1;
                break;
                case '6':
                    $viernes = 1;
                break;
                case '7':
                    $sabado = 1;
                break;
                case '1':
                    $domingo = 1;
                break;
            }
        }
        $data = array(
            'nombre_reserva' => $datos[0]["nombre_reserva"],
            'estado' => $datos[0]["estado"],
            'fecha_registro' => $datos[0]["fecha_registro"],
            'detalle_reserva' => $datos[0]["detalle_reserva"],
            'direccion' => $datos[0]["direccion"],
            'break' => $datos[0]["break"],
            'computadores' => $datos[0]["computadores"],
            'fecha_inicio' => $fecha_inicio,
            'fecha_termino' => $fecha_termino,
            'hora_inicio' => $hora_inicio,
            'hora_termino' => $hora_termino,
            'sala' => $datos[0]["sala"],
            'holding' => $datos[0]["holding"],
            'empresa' => $datos[0]["empresa"],
            'ejecutivo' => $datos[0]["ejecutivo"],
            'lunes' => $lunes,
            'martes' => $martes,
            'miercoles' => $miercoles,
            'jueves' => $jueves,
            'viernes' => $viernes,
            'sabado' => $sabado,
            'domingo' => $domingo,
        );
        echo json_encode($data);
    }

    function ListarReservas() {
        $arr_sesion = array(
            'id_empresa' => $this->input->post('id_empresa'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_listar_reservas_por_conf($arr_sesion);
        echo json_encode($datos);
    }

    function getConfirmarBloque() {
        $arr_input_data = array(
            'id_reserva' => $this->input->post('id_reserva'),
            'id_detalle_sala' => $this->input->post('id_detalle_sala'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->set_arr_confirmar_bloque_reserva($arr_input_data);
        echo json_encode($respuesta);
    }

    function enviaEmailBloqueConfirmado() {
        $arr_input_data = array(
            'id_reserva' => $this->input->post('id_reserva'),
            'id_detalle_sala' => $this->input->post('id_detalle_sala'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos =  $this->modelo->set_datos_confirmaciion_bloque_email($arr_input_data);
        $asunto = "";
        $body = "";

        $id_reserva = $datos[0]['id_reserva'];
        $nombre_reserva = $datos[0]['nombre_reserva'];
        $ejecutivo = $datos[0]['ejecutivo'];
        $correo_ejecutivo = $datos[0]['correo_ejecutivo'];
        $correo_usuario = $datos[0]['correo_usuario'];
        $nombre_usuario = $datos[0]['nombre_usuario'];
        $fecha_inicio = $datos[0]['fecha_inicio'];
        $fecha_termino = $datos[0]['fecha_termino'];
        $hora_inicio = $datos[0]['hora_inicio'];
        $hora_termino = $datos[0]['hora_termino'];
        $dias = $datos[0]['dia'];
        $nombre_sala = $datos[0]['nombre_sala'];
        $direccion = $datos[0]['direccion'];
        $nombre_holding = $datos[0]['nombre_holding'];
        $razon_social_empresa = $datos[0]['razon_social_empresa'];
        $detalle_reserva = $datos[0]['detalle_reserva'];
        $break = $datos[0]['break'];
        $computadores = $datos[0]['computadores'];
        

       
        $asunto = "Bloque de la reserva con id: " . $id_reserva." confirmado"; // asunto del correo         

        $destinatarios = array(
                array('email' => $correo_ejecutivo, 'nombre' => $ejecutivo),
                array('email' => $correo_usuario, 'nombre' => $nombre_usuario),
                array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha')
            );
        $body = $this->generaHtmlBloqueConfirmado($datos[0]); // mensaje completo HTML o text
        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
         
        );
        $AltBody = $asunto; // hover del cursor sobre el correo

        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo $body;
    }

    function generaHtmlBloqueConfirmado($data) {
     
        $html = '';
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>    Se necesita : </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)   ';
        $html .= '<br>';
        $html .= 'Junto con saludar informamos la confirmación del bloque con los datos:  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>Id reserva</td>';
        $html .= '<td class="alignright"> ' . $data['id_reserva'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Nombre reserva </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_reserva'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Fecha inicio </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_inicio'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Fecha termino </td>';
        $html .= '<td class="alignright"> ' . $data['fecha_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Horario </td>';
        $html .= '<td class="alignright"> ' . $data['hora_inicio'] .$data['hora_termino'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>En los dias </td>';
        $html .= '<td class="alignright"> ' . $data['dia'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Sala </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_sala'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Direccion </td>';
        $html .= '<td class="alignright"> ' . $data['direccion'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Holding </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_holding'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Empresa </td>';
        $html .= '<td class="alignright"> ' . $data['razon_social_empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Detalle de la reserva </td>';
        $html .= '<td class="alignright"> ' . $data['detalle_reserva'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Break </td>';
        $html .= '<td class="alignright"> ' . $data['break'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Computadores</td>';
        $html .= '<td class="alignright"> ' . $data['computadores'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Se ha realizado con éxito';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno - Huérfanos 863, piso 2 - Galería España ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
       
       return $html;
    }


    function getConfirmarReserva() {
        $arr_input_data = array(
            'id_reserva' => $this->input->post('id_reserva'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->set_arr_confirmar_reserva($arr_input_data);
        echo json_encode($respuesta);
    }

    function enviaEmailReservaConfirmada() {
        $arr_input_data = array(
            'id_reserva' => $this->input->post('id_reserva'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos =  $this->modelo->set_datos_confirmaciion_reserva_email($arr_input_data);
        $asunto = "";
        $body = "";

        $id_reserva = $datos[0]['id_reserva'];
        $nombre_reserva = $datos[0]['nombre_reserva'];
        $ejecutivo = $datos[0]['ejecutivo'];
        $correo_ejecutivo = $datos[0]['correo_ejecutivo'];
        $correo_usuario = $datos[0]['correo_usuario'];
        $nombre_usuario = $datos[0]['nombre_usuario'];
        $hora_inicio = $datos[0]['hora_inicio'];
        $hora_termino = $datos[0]['hora_termino'];
        $fecha_inicio = $datos[0]['fecha_inicio'];
        $fecha_termino = $datos[0]['fecha_termino'];
        $nombre_sala = $datos[0]['nombre_sala'];
        $empresa = $datos[0]['empresa'];
        $holding = $datos[0]['holding'];

        
       
        $asunto = "Reserva Con Id: " . $id_reserva." confirmada"; // asunto del correo         

        $destinatarios = array(
                array('email' => $correo_ejecutivo, 'nombre' => $ejecutivo),
                array('email' => $correo_usuario, 'nombre' => $nombre_usuario),
                array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha')
            );
        $body = $this->generaHtmlConfirmada($datos); // mensaje completo HTML o text
        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
        );
        $AltBody = $asunto; // hover del cursor sobre el correo

        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo $body;
    }

    function generaHtmlConfirmada($data) {
     
        $html = '';
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>    Se necesita : </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)   ';
        $html .= '<br>';
        $html .= 'Junto con saludar informamos la confirmación de la reserva con los datos:  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td> Id reserva</td>';
        $html .= '<td class="alignright"> ' . $data[0]['id_reserva'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td> Nombre reserva</td>';
        $html .= '<td class="alignright"> ' . $data[0]['nombre_reserva'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td> Empresa</td>';
        $html .= '<td class="alignright"> ' . $data[0]['empresa'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td> Holding</td>';
        $html .= '<td class="alignright"> ' . $data[0]['holding'] . '</td>';
        $html .= '</tr>';
       // var_dump($data);
       //die();

       
        $numero = 1;
        
        
            foreach ($data as $value) {
                $html .= '<tr class="total" >';
                $html .= '<td> Bloque</td>';
                $html .= '<td class="alignright"> N° ' . $numero++ . '</td>';
                $html .= '</tr>';
                $html .= '<tr  >';
                $html .= '<td> Fechas</td>';
                $html .= '<td class="alignright"> ' . $value['fecha_inicio'] .' - '. $value['fecha_termino'] . '</td>';
                $html .= '</tr>';
                $html .= '<tr  >';
                $html .= '<td> Horario</td>';
                $html .= '<td class="alignright"> ' . $value['hora_inicio'] .' - '. $value['hora_termino'] . '</td>';
                $html .= '</tr>';
                $html .= '<tr  >';
                $html .= '<td> Sala</td>';
                $html .= '<td class="alignright"> ' . $value['nombre_sala'] . '</td>';
                $html .= '</tr>';
            }

        
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno - Huérfanos 863, piso 2 - Galería España ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
       
       return $html;
    }


    function RechazarReserva() {
        $arr_input_data = array(
            'id_reserva' => $this->input->post('id_reserva'),
            'motivo_rechazo' => $this->input->post('motivo_rechazo'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->set_arr_rechazar_reserva($arr_input_data);
        echo json_encode($respuesta);
    }

    function enviaEmailRechazo() {
        $arr_input_data = array(
            'id_reserva' => $this->input->post('id_reserva'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos =  $this->modelo->set_datos_rechazo_email($arr_input_data);
        $asunto = "";
        $body = "";

        $id_reserva = $datos[0]['id_reserva'];
        $nombre_reserva = $datos[0]['nombre_reserva'];
        $ejecutivo = $datos[0]['ejecutivo'];
        $correo_ejecutivo = $datos[0]['correo_ejecutivo'];
        $correo_usuario = $datos[0]['correo_usuario'];
        $nombre_usuario = $datos[0]['nombre_usuario'];
        $motivo_rechazo = $datos[0]['motivo_rechazo'];

        $asunto = "Rechazo de la Reserva Con Id: " . $id_reserva; // asunto del correo         
        $destinatarios = array(
            array('email' => $correo_ejecutivo, 'nombre' => $ejecutivo),
            array('email' => $correo_usuario, 'nombre' => $nombre_usuario),
            array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha')
        );
        $body = $this->generaHtmlRechazo($datos[0]); // mensaje completo HTML o text
        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
        );
        $AltBody = $asunto; // hover del cursor sobre el correo
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo $body;
    }

    function generaHtmlRechazo($data) {
        $html = '';
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>    Se necesita : </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)   ';
        $html .= '<br>';
        $html .= 'Junto con saludar le informamos que:  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>La reserva</td>';
        $html .= '<td class="alignright"> ' . $data['nombre_reserva'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Con id</td>';
        $html .= '<td class="alignright"> ' . $data['id_reserva'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr >';
        $html .= '<td >Fue rechazada con motivo</td>';
        $html .= '<td class="alignright"> ' . $data['motivo_rechazo'] . '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno - Huérfanos 863, piso 2 - Galería España ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
     
        return $html;
    }

    function RechazarBloqueReserva() {
        $arr_input_data = array(
            'id_reserva' =>$this->input->post('id_reserva'),
            'id_detalle_sala' => $this->input->post('id_detalle_sala'),
            'motivo_rechazo_bloque' => $this->input->post('motivo_rechazo_bloque'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $respuesta = $this->modelo->set_arr_rechazar_bloque_reserva($arr_input_data);
        echo json_encode($respuesta);
    }
}

/* End of file Confirmar.php */
/* Location: ./application/controllers/Confirmar.php */
?>