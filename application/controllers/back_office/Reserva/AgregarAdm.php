<?php

/**
 * Agregar
 * 
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  
 * Fecha creacion:  2017-03-27 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AgregarAdm extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'AgregarAdm';
    private $nombre_item_plural = 'AgregarAdm';
    private $package = 'back_office/Reserva';
    private $model = 'Reserva_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $view = 'AgregarAdm_v';
    private $controller = 'AgregarAdm';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->usuario, 'modelousuario');     
        // $this->load->model($this->perfil, 'modelperfil');
        //  Libreria de sesion
        $this->load->library('session');

        $this->load->library('Functions');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/fullcalendar/fullcalendar.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datetimepicker/bootstrap-datetimepicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/moment.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/fullcalendar.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/es.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datetimepicker/bootstrap-datetimepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datetimepicker/bootstrap-datetimepicker.es.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),         
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reserva/agregaradm.js')
        );



        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Agregar Reservas Adm";

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;


        $this->load->view('amelia_1/template', $data);
    }

    function getFeriados() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_calendario_get_feriados($arr_sesion);
        echo json_encode($datos);
    }

    function getSalas() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_sala_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function llenarCalendar() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_sala_h' => $this->uri->segment(5)
        );

        $datos = $this->modelo->get_arr_completar_calendar_by_sala($arr_sesion);
        echo json_encode($datos);
    }

    function listaReservasbyUser() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo->get_arr_reserva_by_id_user_cbx($arr_sesion);
        array_push($datos, array('id' => '-1', 'text' => 'Nueva Reserva'));
        echo json_encode(array_reverse($datos));
    }
    /*
    function listaReservasbyUser() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_reserva_by_id_user_cbx($arr_sesion);
        //var_dump($datos);
        $mi_arr = array();

        for ($i = 0; $i < sizeof($datos); $i++) {
            array_push($mi_arr, array('id' => $datos[$i]['id_reserva'], 'value' => $datos[$i]['nombre_reserva']));
        }
        echo json_encode($mi_arr);
    }*/
//

    function buscarReservaById() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_reserva' => $this->uri->segment(5)
        );
        $datos = $this->modelo->get_arr_reserva_by_id($arr_sesion);

        echo json_encode($datos);
    }

    function buscarHoldingById() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_ejecutivo' => $this->uri->segment(5)
        );
        $datos = $this->modelo->get_arr_holding_by_id($arr_sesion);

        echo json_encode($datos);
    }


    function buscarEmpresasById() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_ejecutivo' => $this->input->post('id_ejecutivo'),
            'id_holding' => $this->uri->segment(5)
        );
        $datos = $this->modelo->get_arr_empresa_by_id($arr_sesion);

        echo json_encode($datos);
    }

/*
    function getDetalleReserva() {
        
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_reserva' => $this->input->post('id_reserva'),
            'id_detalle_sala' => $this->input->post('id_detalle_sala')
        );
        $datos = $this->modelo->get_arr_reserva_by_reserva($arr_sesion);

        $horai = explode(':',$datos[0]["horainicio"])[0];
        $mini = explode(':',$datos[0]["horainicio"])[1];
        $horainicio = $horai . ':' . $mini;
        
        $horaf = explode(':',$datos[0]["horatermino"])[0];
        $minf = explode(':',$datos[0]["horatermino"])[1];
        $horatermino = $horaf . ':' . $minf;

        $añoi = explode('-',$datos[0]["inicio"])[0];
        $mesi = explode('-',$datos[0]["inicio"])[1];
        $diai = explode('-',$datos[0]["inicio"])[2];
        $fechainicio = $diai . '-' . $mesi . '-' . $añoi;
        
        $añof = explode('-',$datos[count($datos) - 1]["end"])[0];
        $mesf = explode('-',$datos[count($datos) - 1]["end"])[1];
        $diaf = explode('-',$datos[count($datos) - 1]["end"])[2];
        $fechatermino = $diaf . '-' . $mesf . '-' . $añof;

        $lunes = $martes = $miercoles = $jueves = $viernes = $sabado = $domingo = 0;
        for ($i = 0; $i < count($datos); $i++){
           switch ($datos[$i]["dia"]){
                case '2':
                    $lunes = 1;
                    break;
                case '3':
                    $martes = 1;
                    break;
                case '4':
                    $miercoles = 1;
                    break;
                case '5':
                    $jueves = 1;
                    break;
                case '6':
                    $viernes = 1;
                    break;
                case '7':
                    $sabado = 1;
                    break;
                case '1':
                    $domingo = 1;
                    break;
            }
        }

        $data = array(
            'id_reserva' => $datos[0]["id_reserva"],
            'id_ejecutivo' => $datos[0]["id_ejecutivo"],
            'id_holding' => $datos[0]["id_holding"],
            'id_empresa' => $datos[0]["id_empresa"],
            'id_ingreso' => $datos[0]["id_ingreso"],
            'id_sala' => $datos[0]["id_sala"],
            'nombre_reserva' =>$datos[0]["nombre_reserva"],
            'nombre_ejecutivo' =>$datos[0]["nombre_ejecutivo"],
            'nombre_empresa' =>$datos[0]["nombre_empresa"],
            'nombre_holding' =>$datos[0]["nombre_holding"],
            'break' => $datos[0]["break"],
            'computadores' => $datos[0]["computadores"],
            'detalle_reserva' => $datos[0]["detalle_reserva"],
            'direccion' => $datos[0]["direccion"],
            'horainicio' => $horainicio,
            'horatermino' => $horatermino,
            'fechainicio' => $fechainicio,
            'fechatermino' => $fechatermino,
            'lunes' => $lunes,
            'martes' => $martes,
            'miercoles' => $miercoles,
            'jueves' => $jueves,
            'viernes' => $viernes,
            'sabado' => $sabado,
            'domingo' => $domingo,
        );
        echo json_encode($data);
    }
*/


    function getDetalleReserva() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_reserva' => $this->input->post('id_reserva'),
            'id_detalle_sala' => $this->input->post('id_detalle_sala')
        );
        $datos = $this->modelo->get_arr_reserva_by_reserva($arr_sesion);
        echo json_encode($this->presentadatosreserva($datos));
    }

    function getDetalleReservaEmail($id_reserva, $id_detalle_sala) {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_reserva' => $id_reserva,
            'id_detalle_sala' => $id_detalle_sala
        );
        $datos = $this->modelo->get_arr_reserva_by_reserva($arr_sesion);
        $dat = array();
        $dat = $this->presentadatosreserva($datos);
        return $dat;
    }

    function presentadatosreserva($datos){
        $horai = explode(':',$datos[0]["horainicio"])[0];
        $mini = explode(':',$datos[0]["horainicio"])[1];
        $horainicio = $horai . ':' . $mini;
        
        $horaf = explode(':',$datos[0]["horatermino"])[0];
        $minf = explode(':',$datos[0]["horatermino"])[1];
        $horatermino = $horaf . ':' . $minf;

        $añoi = explode('-',$datos[0]["inicio"])[0];
        $mesi = explode('-',$datos[0]["inicio"])[1];
        $diai = explode('-',$datos[0]["inicio"])[2];
        $fechainicio = $diai . '-' . $mesi . '-' . $añoi;
        
        $añof = explode('-',$datos[count($datos) - 1]["end"])[0];
        $mesf = explode('-',$datos[count($datos) - 1]["end"])[1];
        $diaf = explode('-',$datos[count($datos) - 1]["end"])[2];
        $fechatermino = $diaf . '-' . $mesf . '-' . $añof;

        $lunes = $martes = $miercoles = $jueves = $viernes = $sabado = $domingo = 0;
        for ($i = 0; $i < count($datos); $i++){
           switch ($datos[$i]["dia"]){
                case '2':
                    $lunes = 1;
                    break;
                case '3':
                    $martes = 1;
                    break;
                case '4':
                    $miercoles = 1;
                    break;
                case '5':
                    $jueves = 1;
                    break;
                case '6':
                    $viernes = 1;
                    break;
                case '7':
                    $sabado = 1;
                    break;
                case '1':
                    $domingo = 1;
                    break;
            }
        }
        $data = array();
        $data = array(
            'id_reserva' => $datos[0]["id_reserva"],
            'id_ejecutivo' => $datos[0]["id_ejecutivo"],
            'id_holding' => $datos[0]["id_holding"],
            'id_empresa' => $datos[0]["id_empresa"],
            'id_ingreso' => $datos[0]["id_ingreso"],
            'id_sala' => $datos[0]["id_sala"],
            'nombre_reserva' =>$datos[0]["nombre_reserva"],
            'nombre_ejecutivo' =>$datos[0]["nombre_ejecutivo"],
            'correo_ejecutivo' =>$datos[0]["correo_ejecutivo"],
            'correo_usuario' => $datos[0]['correo_usuario'], 
            'nombre_usuario' => $datos[0]['nombre_usuario'],
            'nombre_empresa' =>$datos[0]["nombre_empresa"],
            'nombre_holding' =>$datos[0]["nombre_holding"],
            'nombre_sala' =>$datos[0]["nombre_sala"],
            'break' => $datos[0]["break"],
            'num_bloque' => $datos[0]["num_bloque"],
            'computadores' => $datos[0]["computadores"],
            'detalle_reserva' => $datos[0]["detalle_reserva"],
            'direccion' => $datos[0]["direccion"],
            'horainicio' => $horainicio,
            'horatermino' => $horatermino,
            'fechainicio' => $fechainicio,
            'fechatermino' => $fechatermino,
            'lunes' => $lunes,
            'martes' => $martes,
            'miercoles' => $miercoles,
            'jueves' => $jueves,
            'viernes' => $viernes,
            'sabado' => $sabado,
            'domingo' => $domingo,
        );        
        return $data;
    }

    function getReserva() {
        
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_reserva' => $this->input->post('id_reserva')
        );
        $datos = $this->modelo->get_arr_reserva_by_id($arr_sesion);


       /* $data = array(
            'id_reserva' => $datos[0]["id_reserva"],
            'id_ejecutivo' => $datos[0]["id_ejecutivo"],
            'id_holding' => $datos[0]["id_holding"],
            'id_empresa' => $datos[0]["id_empresa"],
            'id_ingreso' => $datos[0]["id_ingreso"],
            'id_sala' => $datos[0]["id_sala"],
            'nombre_reserva' =>$datos[0]["nombre_reserva"],
            'nombre_ejecutivo' =>$datos[0]["nombre_ejecutivo"],
            'nombre_empresa' =>$datos[0]["nombre_empresa"],
            'nombre_holding' =>$datos[0]["nombre_holding"],
            'break' => $datos[0]["break"],
            'computadores' => $datos[0]["computadores"],
            'detalle_reserva' => $datos[0]["detalle_reserva"],
        );*/
        echo json_encode($datos);
    }

    function insertReservaCpasula() {

        $lunes = $this->input->post('lunes');
        if ($lunes == null || $lunes == '') {

            $lunes = 0;
        }


        $martes = $this->input->post('martes');
        if ($martes == null || $martes == '') {

            $martes = 0;
        }

        $miercoles = $this->input->post('miercoles');
        if ($miercoles == null || $miercoles == '') {

            $miercoles = 0;
        }

        $jueves = $this->input->post('jueves');
        if ($jueves == null || $jueves == '') {

            $jueves = 0;
        }

        $viernes = $this->input->post('viernes');
        if ($viernes == null || $viernes == '') {

            $viernes = 0;
        }

        $sabado = $this->input->post('sabado');
        if ($sabado == null || $sabado == '') {

            $sabado = 0;
        }

        $domingo = $this->input->post('domingo');
        if ($domingo == null || $domingo == '') {

            $domingo = 0;
        }
        $funciones = new Functions();

        $arr_sesion = array(
            'fecha_inicio' => $funciones->formatDateBD($this->input->post('fecha_inicio')) . ' ' . $this->input->post('txt_hora_inicio'),
            'fecha_termino' => $funciones->formatDateBD($this->input->post('fecha_termino')) . ' ' . $this->input->post('txt_hora_termino'),
            'lunes' => $lunes,
            'martes' => $martes,
            'miercoles' => $miercoles,
            'jueves' => $jueves,
            'viernes' => $viernes,
            'sabado' => $sabado,
            'domingo' => $domingo,
            'estado' => '1',
            'id_sala' => $this->input->post('idSala'),
            'nombre_reserva' => $this->input->post('nombre_reserva'),
            'detalle_reserva' => $this->input->post('detalles_reserva'),
            'break' => $this->input->post('breakSala'),
            'computadores' => $this->input->post('computadores'),
            'id_ejecutivo' => $this->input->post('cbx_ejecutivo'),
            'id_holding' => $this->input->post('cbx_holding'),
            'id_empresa' => $this->input->post('cbx_empresa'),
            'id_reserva' => $this->input->post('cbx_reserva'),
            'direccion' => $this->input->post('direccion'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_id_insert_reservaCpasulaFull($arr_sesion);
        echo json_encode($datos);
    }

    function getUsuarioTipoEcutivoComercial() {
        // carga el combo box de los ejecutivos
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        if($this->session->userdata('id_perfil') == 13 || $this->session->userdata('id_perfil') == 14) {
		for ($i=0; $i < count($datos); $i++) {
                if($this->session->userdata('id_user') == $datos[$i]["id"]){
                    $datos = array($datos[$i]);
                    break;
                }
            }
        }
        echo json_encode($datos);
    }


    function enviaEmail() {
        $id_reserva = $this->input->post('id_reserva');
        $id_detalle_sala = $this->input->post('id_detalle_sala');
        $datos = array();
        $datos = $this->getDetalleReservaEmail($id_reserva, $id_detalle_sala);

        $destinatarios = array();
        $destinatarios = array(
            array('email' => $datos['correo_usuario'], 'nombre' => $datos['nombre_usuario']),
            array('email' => $datos['correo_ejecutivo'], 'nombre' => $datos['nombre_ejecutivo']),
            array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha')
        );
        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
        );
        $asunto = "Reserva N°: " . $datos['id_reserva']; // asunto del correo
        $body = $this->generaHtmlSolicitudReserva($datos); // mensaje completo HTML o text
      /*  $asunto = "Cambio de estado en solicitud de Tablet N°: " . $datos[0]['id_solicitud']; // asunto del correo
        //
        // presenciales
        $body = $this->generaHtmlCambioEstado($datos[0]); // mensaje completo HTML o text
*/
        $AltBody = $asunto; // hover del cursor sobre el correo       

        /*         * ** inicio array ** */
        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto 
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        /*         * ** Fin array ** */
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);
        echo json_encode($datos);
    }

    function encabezadocorreo(){
        $html = "";
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Solicitud de Reservas </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        return $html;
    }
    
    function piecorreo(){
        $html = "";
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno -  Huerfano 863 Piso 2 - Galeria España';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
        return $html;
    }
    
    function generaHtmlSolicitudReserva($data) {
        $dias = '';
        if($data['lunes'] == 1){
            $dias = 'Lunes';
        }
        if($data['martes'] == 1){
            $dias .= ' Martes';
        }
        if($data['miercoles'] == 1){
            $dias .= ' Miercoles';
        }
        if($data['jueves'] == 1){
            $dias .= ' Jueves';
        }
        if($data['viernes'] == 1){
            $dias .= ' Viernes';
        }
        if($data['sabado'] == 1){
            $dias .= ' Sabado';
        }
        if($data['domingo'] == 1){
            $dias .= ' Domingo';
        }
        $html = "";
        $html = $this->encabezadocorreo();
        $html .= 'Estimado(s): ';
        $html .= '<br>';
        $html .= 'Se ha registrado una solicitud de Reserva.  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0"> ';
        $html .= '<tr class="total">';
        $html .= '<td >Por </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_usuario'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del Ejecutivo </td>';
        $html .= '<td class="alignright">' . $data['nombre_ejecutivo'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Holding </td>';
        $html .= '<td class="alignright">' . $data['nombre_holding'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Empresa </td>';
        $html .= '<td class="alignright">' . $data['nombre_empresa'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Nombre de la reserva </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_reserva'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr class="total">';
        $html .= '<td>Bloque </td>';
        $html .= '<td class="alignright"> ' . $data['num_bloque'] . '  </td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>Sala </td>';
        $html .= '<td class="alignright"> ' . $data['nombre_sala'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Del </td>';
        $html .= '<td class="alignright"> ' . $data['fechainicio'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Al </td>';
        $html .= '<td class="alignright"> ' . $data['fechatermino'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Desde </td>';
        $html .= '<td class="alignright"> ' . $data['horainicio'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Hasta </td>';
        $html .= '<td class="alignright"> ' . $data['horatermino'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Los dias </td>';
        $html .= '<td class="alignright"> ' . $dias . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Detalles </td>';
        $html .= '<td class="alignright"> ' . $data['detalle_reserva'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Break </td>';
        $html .= '<td class="alignright">' . $data['break'] . ' </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Computadores </td>';
        $html .= '<td class="alignright"> ' . $data['computadores'] . '  </td>';
        $html .= '</tr> ';
        $html .= '<tr>';
        $html .= '<td>Dirección </td>';
        $html .= '<td class="alignright"> ' . $data['direccion'] . '  </td>';
        $html .= '</tr> ';
        $html .= $this->piecorreo();
        return $html;
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
