<?php

/**
 * Agregar
 * 
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  
 * Fecha creacion:  2017-03-27 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Editar';
    private $nombre_item_plural = 'Editar';
    private $package = 'back_office/Reserva';
    private $model = 'Reserva_model';
    private $usuario = 'back_office/Usuario/Usuario_model';
    private $view = 'Editar_v';
    private $controller = 'Editar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->usuario, 'modelousuario');     
        // $this->load->model($this->perfil, 'modelperfil');
        //  Libreria de sesion
        $this->load->library('session');

        $this->load->library('Functions');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . $this->package . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Agregar',
                'label' => 'Agregar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/fullcalendar/fullcalendar.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/clockpicker/clockpicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datetimepicker/bootstrap-datetimepicker.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/datapicker/datepicker3.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/moment.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/clockpicker/clockpicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/fullcalendar.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/es.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datetimepicker/bootstrap-datetimepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datetimepicker/bootstrap-datetimepicker.es.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),         
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reserva/editar.js')
        );



        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Editar Reservas";

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;


        $this->load->view('amelia_1/template', $data);
    }

    function getFeriados() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_calendario_get_feriados($arr_sesion);
        echo json_encode($datos);
    }

    function getSalas() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_sala_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function llenarCalendar() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_sala_h' => $this->uri->segment(5)
        );

        $datos = $this->modelo->get_arr_completar_calendar_by_sala($arr_sesion);
        echo json_encode($datos);
    }

    function listaReservasbyUser() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo->get_arr_reserva_by_id_user_cbx($arr_sesion);
        array_push($datos, array('id' => '-1', 'text' => 'Nueva Reserva'));
        echo json_encode(array_reverse($datos));
    }

    function listaReservasbyUserEditar() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        
        $datos = $this->modelo->get_arr_reserva_by_id_user_cbx($arr_sesion);
        // array_push($datos, array('id' => '-1', 'text' => 'Nueva Reserva'));
        echo json_encode(array_reverse($datos));
    }
    /*
    function listaReservasbyUser() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_reserva_by_id_user_cbx($arr_sesion);
        //var_dump($datos);
        $mi_arr = array();

        for ($i = 0; $i < sizeof($datos); $i++) {
            array_push($mi_arr, array('id' => $datos[$i]['id_reserva'], 'value' => $datos[$i]['nombre_reserva']));
        }
        echo json_encode($mi_arr);
    }*/
//

    function buscarReservaById() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_reserva' => $this->uri->segment(5)
        );
        $datos = $this->modelo->get_arr_reserva_by_id($arr_sesion);

        echo json_encode($datos);
    }

    function buscarHoldingById() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_ejecutivo' => $this->uri->segment(5)
        );
        $datos = $this->modelo->get_arr_holding_by_id($arr_sesion);

        echo json_encode($datos);
    }


    function buscarEmpresasById() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_ejecutivo' => $this->input->post('id_ejecutivo'),
            'id_holding' => $this->uri->segment(5)
        );
        $datos = $this->modelo->get_arr_empresa_by_id($arr_sesion);

        echo json_encode($datos);
    }


    function getDetalleReserva() {
        
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_reserva' => $this->input->post('id_reserva'),
            'id_detalle_sala' => $this->input->post('id_detalle_sala')
        );
        $datos = $this->modelo->get_arr_reserva_by_reserva($arr_sesion);

        $horai = explode(':',$datos[0]["horainicio"])[0];
        $mini = explode(':',$datos[0]["horainicio"])[1];
        $horainicio = $horai . ':' . $mini;
        
        $horaf = explode(':',$datos[0]["horatermino"])[0];
        $minf = explode(':',$datos[0]["horatermino"])[1];
        $horatermino = $horaf . ':' . $minf;

        $añoi = explode('-',$datos[0]["inicio"])[0];
        $mesi = explode('-',$datos[0]["inicio"])[1];
        $diai = explode('-',$datos[0]["inicio"])[2];
        $fechainicio = $diai . '-' . $mesi . '-' . $añoi;
        
        $añof = explode('-',$datos[count($datos) - 1]["end"])[0];
        $mesf = explode('-',$datos[count($datos) - 1]["end"])[1];
        $diaf = explode('-',$datos[count($datos) - 1]["end"])[2];
        $fechatermino = $diaf . '-' . $mesf . '-' . $añof;

        $lunes = $martes = $miercoles = $jueves = $viernes = $sabado = $domingo = 0;
        for ($i = 0; $i < count($datos); $i++){
           switch ($datos[$i]["dia"]){
                case '2':
                    $lunes = 1;
                    break;
                case '3':
                    $martes = 1;
                    break;
                case '4':
                    $miercoles = 1;
                    break;
                case '5':
                    $jueves = 1;
                    break;
                case '6':
                    $viernes = 1;
                    break;
                case '7':
                    $sabado = 1;
                    break;
                case '1':
                    $domingo = 1;
                    break;
            }
        }

        $data = array(
            'id_reserva' => $datos[0]["id_reserva"],
            'id_ejecutivo' => $datos[0]["id_ejecutivo"],
            'id_holding' => $datos[0]["id_holding"],
            'id_empresa' => $datos[0]["id_empresa"],
            'id_ingreso' => $datos[0]["id_ingreso"],
            'id_sala' => $datos[0]["id_sala"],
            'nombre_sala' => $datos[0]["nombre_sala"],
            'nombre_reserva' =>$datos[0]["nombre_reserva"],
            'nombre_ejecutivo' =>$datos[0]["nombre_ejecutivo"],
            'nombre_empresa' =>$datos[0]["nombre_empresa"],
            'nombre_holding' =>$datos[0]["nombre_holding"],
            'break' => $datos[0]["break"],
            'computadores' => $datos[0]["computadores"],
            'detalle_reserva' => $datos[0]["detalle_reserva"],
            'direccion' => $datos[0]["direccion"],
            'horainicio' => $horainicio,
            'horatermino' => $horatermino,
            'fechainicio' => $fechainicio,
            'fechatermino' => $fechatermino,
            'lunes' => $lunes,
            'martes' => $martes,
            'miercoles' => $miercoles,
            'jueves' => $jueves,
            'viernes' => $viernes,
            'sabado' => $sabado,
            'domingo' => $domingo,
        );
        echo json_encode($data);
    }

    function getReserva() {
        
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_reserva' => $this->input->post('id_reserva')
        );
        $datos = $this->modelo->get_arr_reserva_by_id($arr_sesion);


       /* $data = array(
            'id_reserva' => $datos[0]["id_reserva"],
            'id_ejecutivo' => $datos[0]["id_ejecutivo"],
            'id_holding' => $datos[0]["id_holding"],
            'id_empresa' => $datos[0]["id_empresa"],
            'id_ingreso' => $datos[0]["id_ingreso"],
            'id_sala' => $datos[0]["id_sala"],
            'nombre_reserva' =>$datos[0]["nombre_reserva"],
            'nombre_ejecutivo' =>$datos[0]["nombre_ejecutivo"],
            'nombre_empresa' =>$datos[0]["nombre_empresa"],
            'nombre_holding' =>$datos[0]["nombre_holding"],
            'break' => $datos[0]["break"],
            'computadores' => $datos[0]["computadores"],
            'detalle_reserva' => $datos[0]["detalle_reserva"],
        );*/
        echo json_encode($datos);
    }

    function insertReservaCpasula() {

        $lunes = $this->input->post('lunes');
        if ($lunes == null || $lunes == '') {

            $lunes = 0;
        }


        $martes = $this->input->post('martes');
        if ($martes == null || $martes == '') {

            $martes = 0;
        }

        $miercoles = $this->input->post('miercoles');
        if ($miercoles == null || $miercoles == '') {

            $miercoles = 0;
        }

        $jueves = $this->input->post('jueves');
        if ($jueves == null || $jueves == '') {

            $jueves = 0;
        }

        $viernes = $this->input->post('viernes');
        if ($viernes == null || $viernes == '') {

            $viernes = 0;
        }

        $sabado = $this->input->post('sabado');
        if ($sabado == null || $sabado == '') {

            $sabado = 0;
        }

        $domingo = $this->input->post('domingo');
        if ($domingo == null || $domingo == '') {

            $domingo = 0;
        }
        $funciones = new Functions();

        $arr_sesion = array(
            'fecha_inicio' => $funciones->formatDateBD($this->input->post('fecha_inicio')) . ' ' . $this->input->post('txt_hora_inicio'),
            'fecha_termino' => $funciones->formatDateBD($this->input->post('fecha_termino')) . ' ' . $this->input->post('txt_hora_termino'),
            'lunes' => $lunes,
            'martes' => $martes,
            'miercoles' => $miercoles,
            'jueves' => $jueves,
            'viernes' => $viernes,
            'sabado' => $sabado,
            'domingo' => $domingo,
            'estado' => '1',
            'id_sala' => $this->input->post('idSala'),
            'nombre_reserva' => $this->input->post('nombre_reserva'),
            'detalle_reserva' => $this->input->post('detalles_reserva'),
            'break' => $this->input->post('breakSala'),
            'computadores' => $this->input->post('computadores'),
            'id_ejecutivo' => $this->input->post('cbx_ejecutivo'),
            'id_holding' => $this->input->post('cbx_holding'),
            'id_empresa' => $this->input->post('cbx_empresa'),
            'id_reserva' => $this->input->post('cbx_reserva'),
            'direccion' => $this->input->post('direccion'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_id_insert_reservaCpasulaFull($arr_sesion);
        echo json_encode($datos);
    }

    function EditarDetalleReserva() {

        $arr_sesion = array(
            'id_sala' => $this->input->post('idSala'),
            'break' => $this->input->post('breakSala'),
            'computadores' => $this->input->post('computadores'),
            'direccion' => $this->input->post('direccion'),
            'id_detalle_reserva' => $this->input->post('cbx_bloque'),
            'id_reserva' => $this->input->post('cbx_reserva'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->upd_detalle_reserva($arr_sesion);
        echo json_encode($datos);
    }

    function EditarReserva() {

        $arr_sesion = array(
            'nombre_reserva' => $this->input->post('nombre_reserva'),
            'detalle_reserva' => $this->input->post('detalles_reserva'),
            'id_holding' => $this->input->post('cbx_holding'),
            'id_empresa' => $this->input->post('cbx_empresa'),
            'id_reserva' => $this->input->post('cbx_reserva'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->upd_reserva($arr_sesion);
        echo json_encode($datos);
    }

    function EditarCapsula() {
        $lunes = $this->input->post('lunes');
        if ($lunes == null || $lunes == '') {
            $lunes = 0;
        }

        $martes = $this->input->post('martes');
        if ($martes == null || $martes == '') {
            $martes = 0;
        }

        $miercoles = $this->input->post('miercoles');
        if ($miercoles == null || $miercoles == '') {
            $miercoles = 0;
        }

        $jueves = $this->input->post('jueves');
        if ($jueves == null || $jueves == '') {
            $jueves = 0;
        }

        $viernes = $this->input->post('viernes');
        if ($viernes == null || $viernes == '') {
            $viernes = 0;
        }

        $sabado = $this->input->post('sabado');
        if ($sabado == null || $sabado == '') {
            $sabado = 0;
        }

        $domingo = $this->input->post('domingo');
        if ($domingo == null || $domingo == '') {
            $domingo = 0;
        }
        $funciones = new Functions();

        $arr_sesion = array(
            'fecha_inicio' => $funciones->formatDateBD($this->input->post('fecha_inicio')) . ' ' . $this->input->post('txt_hora_inicio'),
            'fecha_termino' => $funciones->formatDateBD($this->input->post('fecha_termino')) . ' ' . $this->input->post('txt_hora_termino'),
            'lunes' => $lunes,
            'martes' => $martes,
            'miercoles' => $miercoles,
            'jueves' => $jueves,
            'viernes' => $viernes,
            'sabado' => $sabado,
            'domingo' => $domingo,
            'id_reserva' => $this->input->post('cbx_reserva'),
            'id_detalle_reserva' => $this->input->post('cbx_bloque'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->upd_horario_reserva($arr_sesion);
        echo json_encode($datos);
    }


    function getUsuarioTipoEcutivoComercial() {
        // carga el combo box de los ejecutivos
        $arr_data = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelousuario->get_arr_usuario_by_ejecutivo_comercial($arr_data);
        if($this->session->userdata('id_perfil') == 13 || $this->session->userdata('id_perfil') == 14) {
		for ($i=0; $i < count($datos); $i++) {
                if($this->session->userdata('id_user') == $datos[$i]["id"]){
                    $datos = array($datos[$i]);
                    break;
                }
            }
        }
        echo json_encode($datos);
    }

}

/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>
