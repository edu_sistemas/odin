<?php

/**
 * Confirmar
 * 
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-19 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-12-19 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Eliminar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Eliminar';
    private $nombre_item_plural = 'Eliminar';
    private $package = 'back_office/Reserva';
    private $model = 'Reserva_model';
    private $view = 'Eliminar_v';
    private $controller = 'Eliminar';
    private $ind = '';

    function __construct() {
        parent::__construct();
        //  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        // $this->load->model($this->perfil, 'modelperfil');
        //  Libreria de sesion
        $this->load->library('session');
    }

    // 2016-10-11
    // Controlador del panel de control
    public function index() {
        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . '/back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Eliminar',
                'label' => 'Eliminar',
                'icono' => ''
            )
        );

        // arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/fullcalendar/fullcalendar.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/iCheck/custom.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/moment.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/fullcalendar.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/es.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/iCheck/icheck.min.js'),
           
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
               array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reserva/eliminar.js')
        );



        //  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Eliminar Reservas";
        $data['id_perfil'] = $this->session->userdata('id_perfil');

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;


        $this->load->view('amelia_1/template', $data);
    }

    function getFeriados() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_calendario_get_feriados($arr_sesion);
        echo json_encode($datos);
    }

    function getSalas() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_sala_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function llenarCalendar() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_sala_h' => $this->uri->segment(5)
        );

        $datos = $this->modelo->get_arr_completar_calendar_by_sala($arr_sesion);
        echo json_encode($datos);
    }

    function listaReservasbyUser() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_reserva_by_id_user($arr_sesion);
        //var_dump($datos);
        $mi_arr = array();
        for ($i = 0; $i < sizeof($datos); $i++) {
            array_push($mi_arr, array('id' => $datos[$i]['id_reserva'], 'value' => $datos[$i]['detalle_reserva']));
        }
        echo json_encode($mi_arr);
    }

    function getFechas() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función para filtrar las fechas segun el id
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_capsula' => $this->uri->segment(5),
            'horario_inicio' => $this->input->post('horario_inicio'),
            'horario_termino' => $this->input->post('horario_termino')
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->get_arr_fechas($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function getDetallesReserva() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_capsula' => $this->uri->segment(5)
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->get_arr_detalles_reserva($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    /*function getFicha() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_ficha_cbx($arr_sesion);
        echo json_encode($datos);
    }*/

    function EliminarCapsula() {
        $arr_input_data = array(
            'id_capsula' => $this->input->post('id_capsula'),
            'id_detalle_sala' => $this->input->post('id_detalle_sala'),
            'id_reserva' => $this->input->post('id_reserva')
        );
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_elimina_capsula($arr_input_data);
        echo json_encode($respuesta);
    }

    function EliminarBloque() {
        $arr_input_data = array(
            'id_detalle_sala' => $this->input->post('id_detalle_sala'),
            'id_reserva' => $this->input->post('id_reserva')
        );
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->set_arr_elimina_bloque($arr_input_data);
        echo json_encode($respuesta);
    }

    function EliminarReserva() {
        /*
          Ultima fecha Modificacion: 2017-01-05
          Descripcion:  función para filtrar las fechas segun el id
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_reserva' => $this->input->post('id_reserva')
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->set_arr_elimina_reserva($arr_input_data);

        //  respuesta procesada por la peticion AJAX
        echo json_encode($respuesta);
    }

    function enviaEmailCapsulaEliminada() {
        $arr_input_data = array(
            'id_capsula' => $this->input->post('id_capsula'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos =  $this->modelo->set_datos_eliminacion_capsula_email($arr_input_data);
        
        $asunto = "";
        $body = "";

        $id_reserva = $datos[0]['id_reserva'];
        $nombre_reserva = $datos[0]['nombre_reserva'];
        $ejecutivo = $datos[0]['ejecutivo'];
        $correo_ejecutivo = $datos[0]['correo_ejecutivo'];
        $correo_usuario = $datos[0]['correo_usuario'];
        $nombre_usuario = $datos[0]['nombre_usuario'];
        $sala = $datos[0]['sala'];
        $horario = $datos[0]['horario'];
        $fechas = $datos[0]['fechas'];
               
        $asunto = "Capsula de la reserva con id: " . $id_reserva." eliminada"; // asunto del correo         

        $destinatarios = array(
                array('email' => $correo_ejecutivo, 'nombre' => $ejecutivo),
                array('email' => $correo_usuario, 'nombre' => $nombre_usuario),
                array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha')
            );
        $body = $this->generaHtmlCapsulaEliminada($datos); // mensaje completo HTML o text
        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
        );
        $AltBody = $asunto; // hover del cursor sobre el correo

        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo $body;
    }

    function generaHtmlCapsulaEliminada($data) {
     
        $html = '';
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>    Se necesita : </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)   ';
        $html .= '<br>';
        $html .= 'Junto con saludar informamos la eliminación de la clase con los datos:  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>Id reserva</td>';
        $html .= '<td class="alignright"> ' . $data[0]['id_reserva'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Nombre reserva </td>';
        $html .= '<td class="alignright"> ' . $data[0]['nombre_reserva'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Sala </td>';
        $html .= '<td class="alignright"> ' . $data[0]['sala'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Horario </td>';
        $html .= '<td class="alignright"> ' . $data[0]['horario'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Fechas desde </td>';
        $html .= '<td class="alignright"> ' . $data[0]['fechas'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Se ha realizado con éxito';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno - Huérfanos 863, piso 2 - Galería España ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
       
       return $html;
    }

    function enviaEmailBloqueEliminado() {
        $arr_input_data = array(
            'id_detalle_sala' => $this->input->post('id_detalle_sala'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos =  $this->modelo->set_datos_eliminacion_bloque_email($arr_input_data);
        
        $asunto = "";
        $body = "";

        $id_reserva = $datos[0]['id_reserva'];
        $nombre_reserva = $datos[0]['nombre_reserva'];
        $ejecutivo = $datos[0]['ejecutivo'];
        $correo_ejecutivo = $datos[0]['correo_ejecutivo'];
        $correo_usuario = $datos[0]['correo_usuario'];
        $nombre_usuario = $datos[0]['nombre_usuario'];
        $sala = $datos[0]['sala'];
        $horario = $datos[0]['horario'];
        $fechas = $datos[0]['fechas'];
               
        $asunto = "Bloque de la reserva con id: " . $id_reserva." eliminado"; // asunto del correo         

        $destinatarios = array(
                array('email' => $correo_ejecutivo, 'nombre' => $ejecutivo),
                array('email' => $correo_usuario, 'nombre' => $nombre_usuario),
                array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha')
            );
        $body = $this->generaHtmlBloqueEliminado($datos); // mensaje completo HTML o text
        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
        );
        $AltBody = $asunto; // hover del cursor sobre el correo

        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo $body;
    }

    function generaHtmlBloqueEliminado($data) {
     
        $html = '';
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>    Se necesita : </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)   ';
        $html .= '<br>';
        $html .= 'Junto con saludar informamos la eliminación del bloque con los datos:  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>Id reserva</td>';
        $html .= '<td class="alignright"> ' . $data[0]['id_reserva'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Nombre reserva </td>';
        $html .= '<td class="alignright"> ' . $data[0]['nombre_reserva'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Sala </td>';
        $html .= '<td class="alignright"> ' . $data[0]['sala'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Horario </td>';
        $html .= '<td class="alignright"> ' . $data[0]['horario'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Fecha del </td>';
        $html .= '<td class="alignright"> ' . $data[0]['fechas'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Detalle reserva </td>';
        $html .= '<td class="alignright"> ' . $data[0]['detalle_reserva'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Break </td>';
        $html .= '<td class="alignright"> ' . $data[0]['break'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Computadores </td>';
        $html .= '<td class="alignright"> ' . $data[0]['computadores'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Se ha realizado con éxito';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno - Huérfanos 863, piso 2 - Galería España ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
       
       return $html;
    }

    function enviaEmailReservaEliminada() {
        $arr_input_data = array(
            'id_reserva' => $this->input->post('id_reserva'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos =  $this->modelo-> set_datos_eliminacion_reserva_email($arr_input_data);
        
        $asunto = "";
        $body = "";

        $id_reserva = $datos[0]['id_reserva'];
        $nombre_reserva = $datos[0]['nombre_reserva'];
        $ejecutivo = $datos[0]['ejecutivo'];
        $correo_ejecutivo = $datos[0]['correo_ejecutivo'];
        $correo_usuario = $datos[0]['correo_usuario'];
        $nombre_usuario = $datos[0]['nombre_usuario'];
        $sala = $datos[0]['sala'];
        $horario = $datos[0]['horario'];
        $fechas = $datos[0]['fechas'];
               
        $asunto = "Reserva con id: " . $id_reserva." eliminada"; // asunto del correo         

        $destinatarios = array(
                array('email' => $correo_ejecutivo, 'nombre' => $ejecutivo),
                array('email' => $correo_usuario, 'nombre' => $nombre_usuario),
                array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha')
            );
        $body = $this->generaHtmlReservaEliminada($datos); // mensaje completo HTML o text
        $cc = array(
            array('email' => 'jroa@edutecno.com', 'nombre' => 'Jessica Roa')
        );
        $AltBody = $asunto; // hover del cursor sobre el correo

        $data_email = array(
            // indica a quienes esta dirigido el  email
            "destinatarios" => $destinatarios,
            "cc_a" => $cc, // asunto del correo
            "asunto" => $asunto,
            // contenido del correo , puede ser html o solo texto
            "body" => $body,
            // AltBody (esto no sirve para para pero lo requiere la libreria XD)
            "AltBody" => $AltBody
        );
        $this->load->library('Libreriaemail');
        $res = $this->libreriaemail->CallAPISendMail($data_email);

        echo $body;
    }

    function generaHtmlReservaEliminada($data) {
     
        $html = '';
        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>    Se necesita : </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap aligncenter">';
        $html .= '<table width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/odin_logo_naranjo.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<table class="invoice">';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimados(as)   ';
        $html .= '<br>';
        $html .= 'Junto con saludar informamos la eliminación de la reserva con los datos:  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td>';
        $html .= '<table class="invoice-items" cellpadding="0" cellspacing="0">';
        $html .= '<tr  class="total">';
        $html .= '<td>Id reserva</td>';
        $html .= '<td class="alignright"> ' . $data[0]['id_reserva'] . ' </td>';
        $html .= '</tr>';
        $html .= '<tr  >';
        $html .= '<td>Nombre reserva </td>';
        $html .= '<td class="alignright"> ' . $data[0]['nombre_reserva'] . '</td>';
        $html .= '</tr>';
        
        $numero = 1;
        
        
        foreach ($data as $value) {
            $html .= '<tr class="total" >';
            $html .= '<td> Bloque</td>';
            $html .= '<td class="alignright"> N° ' . $numero++ . '</td>';
            $html .= '</tr>';
            $html .= '<tr  >';
            $html .= '<td>Sala </td>';
            $html .= '<td class="alignright"> ' . $value['sala'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr  >';
            $html .= '<td>Horario </td>';
            $html .= '<td class="alignright"> ' . $value['horario'] . '</td>';
            $html .= '</tr>';
            $html .= '<tr  >';
            $html .= '<td>Fecha del </td>';
            $html .= '<td class="alignright"> ' . $value['fechas'] . '</td>';
            $html .= '</tr>';
        }
      
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Se ha realizado con éxito';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Edutecno - Huérfanos 863, piso 2 - Galería España ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';
       
       return $html;
    }



    

}

/* End of file Confirmar.php */
/* Location: ./application/controllers/Confirmar.php */
?>