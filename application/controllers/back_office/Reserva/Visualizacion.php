<?php

/**
 * Agregar
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-03-05 [Jessica Roa] <jroa@edutecno.com> 
 * Fecha creacion:  2016-12-26 [Marcelo Romero] <mromero@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Visualizacion extends MY_Controller {

// Variables paramétricas
    private $nombre_item_singular = 'visualizacion';
    private $nombre_item_plural = 'visualizacion';
    private $package = 'back_office/Reserva';
    private $model = 'Reserva_model';
    private $view = 'Visualizacion_v';
    private $controller = 'Visualizacion';
    private $ind = '';

    function __construct() {
        parent::__construct();
//  Carga el modelo que utiliza el controlador
        $this->load->model($this->package . '/' . $this->model, 'modelo');
// $this->load->model($this->perfil, 'modelperfil');
//  Libreria de sesion
        $this->load->library('session');
    }

// 2016-12-26
// Controlador del panel de control
    public function index() {
///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . '/back_office/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Listar',
                'label' => 'Listar',
                'icono' => ''
            )
        );

// arrray con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/fullcalendar-scheduler/lib/fullcalendar.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/fullcalendar-scheduler/scheduler.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/estilos.css')
        );

//  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar-scheduler/lib/moment.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar-scheduler/lib/fullcalendar.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar-scheduler/scheduler.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/fullcalendar/es.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            
            array('src' => base_url() . 'assets/amelia/js/plugins/validate/jquery.validate.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Reserva/visualizacion.js')
        );



//  informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

////**** Obligatorio ****** //////
        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');

        $data['activo'] = "Todas Las Reservas";

        $data['id_perfil'] = $this->session->userdata('id_perfil');

        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;


        $this->load->view('amelia_1/template', $data);
    }

    function getFeriados() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_calendario_get_feriados($arr_sesion);
        echo json_encode($datos);
    }

    function llenarCalendar() {
        // carga las salas
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_completar_calendar_visualizacion($arr_sesion);
        echo json_encode($datos);
    }

    
    function llenarSalas() {
        // carga las salas
        $arr_sesion = array(
            'id_sede' => $this->uri->segment(5),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_salas_calendar_visualizacion($arr_sesion);
        echo json_encode($datos);
    }

    function listaReservasbyUser() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $datos = $this->modelo->get_arr_reserva_by_id_user($arr_sesion);
        //var_dump($datos);
        $mi_arr = array();
        for ($i = 0; $i < sizeof($datos); $i++) {
            array_push($mi_arr, array('id' => $datos[$i]['id_reserva'], 'value' => $datos[$i]['detalle_reserva']));
        }
        echo json_encode($mi_arr);
    }
    
        function getDetallesReserva() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función rescatar los detalles de la reserva que se encuentran en diferentes tablas
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_capsula' => $this->uri->segment(5)
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        $respuesta = $this->modelo->get_arr_detalles_reserva($arr_input_data);
        echo json_encode($respuesta);
//        var_dump($arr_input_data);
//        die();
    }

        function getFechas() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función para filtrar las fechas segun el id
          Usuario Actualizador : Marcelo Romero
         */

        $arr_input_data = array(
            'id_capsula' => $this->uri->segment(5),
            'horario_inicio' => $this->input->post('horario_inicio'),
            'horario_termino' => $this->input->post('horario_termino')
        );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->get_arr_fechas($arr_input_data);
        echo json_encode($respuesta);
    }
        function getFechasBloque() {
            $arr_input_data = array(
                'id_detalle_sala' => $this->uri->segment(5),
                'horario_inicio' => $this->input->post('horario_inicio'),
                'horario_termino' => $this->input->post('horario_termino')
            );
        //  datos de la sesion , neecesarios para todo procedimeitno de almacenado
        $arr_input_data['user_id'] = $this->session->userdata('id_user');
        $arr_input_data['user_perfil'] = $this->session->userdata('id_perfil');

        // llamado al modelo
        $respuesta = $this->modelo->get_arr_fechas_bloques($arr_input_data);
        echo json_encode($respuesta);
    }
    
            function getalmumnos() {
        /*
          Ultima fecha Modificacion: 2017-01-04
          Descripcion:  función para filtrar las fechas segun el id
          Usuario Actualizador : Marcelo Romero
         */
        $arr_input_data = array(
            'id_ficha' => $this->uri->segment(5),
 
        );
  
        // llamado al modelo
        $respuesta = $this->modelo->get_arr_cant_alumnos($arr_input_data);
        echo json_encode($respuesta);
    }
    
    
}



/* End of file Agregar.php */
/* Location: ./application/controllers/Agregar.php */
?>