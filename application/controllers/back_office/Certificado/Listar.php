<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
defined('BASEPATH') or exit('No direct script access allowed');

class Listar extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Certificado';
    private $nombre_item_plural = 'Certificados';
    private $package = 'back_office/Certificado';
    private $model = 'Certificado_model';
    private $view = 'Listar_v';
    private $controller = 'Certificado';
    private $ind = '';
    private $empresa = 'back_office/Empresa/Empresa_model';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');
        $this->load->model($this->empresa, 'modelempresa');
    }

    public function index() {
        // / array ubicaciones
        $arr_page_breadcrumb = array(
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Home',
                'label' => 'Home',
                'icono' => ''
            ),
            array(
                'href' => base_url() . $this->ind . $this->package . '/Certificado',
                'label' => 'Certificados',
                'icono' => ''
            )
        );

        // array con los css necesarios para la pagina -- Informe Técnico
        $arr_theme_css_files = array(
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        // js necesarios para la pagina
        $arr_theme_js_files = array(
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/JsPDF/jspdf.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Certificado/listar.js')
        );


        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');


        $data['activo'] = "Certificado";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }

        $data ['menus'] = $this->session->userdata('menu_usuario');

        // informacion adicional para la pagina
        $data ['page_title'] = '';
        $data ['page_title_small'] = '';
        $data ['panel_title'] = $this->nombre_item_singular;
        $data ['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data ['script_adicional'] = $arr_theme_js_files;
        $data ['style_adicional'] = $arr_theme_css_files;

        // //**** Obligatorio ****** //////
        // / fin carga menu
        //$data ['fichas'] = $this->modelo->getFichas ();
        $data ['id_perfil'] = $this->session->userdata('id_perfil');
        $data ['page_menu'] = 'dashboard';
        $data ['main_content'] = $this->package . '/' . $this->view;
        $this->load->view('amelia_1/template', $data);
    }

    public function fichas() {

        $num_ficha = $this->input->post('num_ficha');
        $empresa = $this->input->post('empresa');


        //  variables de sesion

        $arr_sesion = array(
            'num_ficha' => $num_ficha,
            'empresa' => $empresa,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->getFichas($arr_sesion);
        echo json_encode($datos);
    }

    function Empresas() {
        // carga el cbx de modalidad
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelempresa->get_arr_listar_empresa_cbx($arr_sesion);
        echo json_encode($datos);
    }
    function OC_byIdFicha() {
        
        $idFicha = $this->input->post('id_ficha');
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil'),
            'id_ficha' =>  $idFicha
        );

        $datos = $this->modelo->get_oc_by_id_ficha($arr_sesion);
        echo json_encode($datos);
    }
    

    function tipo_entrega() {
        // carga el cbx de tipo entrega
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->get_arr_tipo_entrega_cbx($arr_sesion);
        echo json_encode($datos);
    }

    function updateTipoentrega() {
        // carga el cbx de tipo entrega
        $id_ficha = $this->input->post('id_ficha');
        $id_tipo = $this->input->post('id_tipo');
        $arr_sesion = array(
            'id_ficha' => $id_ficha,
            'id_tipo' => $id_tipo,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo->set_arr_tipo_entrega($arr_sesion);
        echo json_encode($datos);
    }

    public function generar() {
        //$id_ficha = $this->uri->segment(5);
        $id_ficha = $this->input->post('id_perso');
        $id_orden = $this->input->post('OCbyidFicha');
        $firma = $this->input->post('Perso');
        if ($firma == 1) {
            $nombreE = $this->input->post('n_encargado');
            $cargoE = $this->input->post('c_encargado');
            $empresa = $this->input->post('empresa_p');
        }
        $fondo = $this->input->post('fondo_perso');
        $participacion = $this->input->post('participacion');


        if ($participacion == null) {
            $participacion = 0;
        }
          $porcentaje = $this->input->post('porcentaje');
        

        if ($porcentaje == null) {
            $porcentaje = 0;
        }

        $opcNombre = $this->input->post('optionsRadios');
        $nombreP = '';
        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'id_orden' => $id_orden,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );
        $promedios = count($this->modelo->getPromedios($arr_datos)) == 0 ? "" : $this->modelo->getPromedios($arr_datos);
        
        if($promedios == ""){
            echo "<h1>esta ficha no tiene promedios ingresados</h1>";
            die();
        }

        $this->load->library('Pdf');
        $pdf = new pdf('L', 'mm', 'LETTER');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        if ($fondo == 1) {
            $pdf->SetMargins(0, 0, 0, true);
        } else {
            $pdf->SetMargins(0, 0, -10, true);
        }

        $num_ficha = $promedios[0]['num_ficha'];
        // set auto page breaks false
        $pdf->SetAutoPageBreak(false, 0);
        $count = 0;
        $datos_alumnos = array();


        //bloque ql para todos los alumnos
        if (count($promedios) > 0) {


            for ($i = 0; $i < count($promedios); $i++) {
                $diploma = 0;
                $arr_datos = array(
                    'id_detalle_alumno' => $promedios[$i]['id_detalle_alumno'],
                    'participacion' => $participacion,
                    'porcentaje' => $porcentaje,
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );
                $datosAl = $this->modelo->get_datos_diplomas($arr_datos);



                if ($promedios[$i]['promedio'] >= 6.7) {
                    $comentario = "Por haber Cursado y Aprobado con distinci&oacute;n M&aacute;xima de forma Destacada";
                    array_push($datosAl, $comentario);
                } else if ($promedios[$i]['promedio'] >= 4.0) {
                    $comentario = "Por haber Cursado y Aprobado de forma satisfactoria el Curso";
                    array_push($datosAl, $comentario);
                } else {
                    $comentario = "Por haber Participado del Curso";
                    array_push($datosAl, $comentario);
                    $diploma = 1;
                }
                array_push($datos_alumnos, $datosAl);
                if ($opcNombre == '1') {
                    $nombreP = $datosAl[0]['nombre_curso'];
                }
                if ($opcNombre == '2') {
                    $nombreP = $datosAl[0]['nombre_curso_code_sence'];
                }
                if ($opcNombre == '3') {
                    $nombreP = $this->input->post('saludos');
                }

                if ($promedios[$i]['promedio'] >= 6.7) {
                    $comentario = "Por haber Cursado y Aprobado con distinci&oacute;n M&aacute;xima de forma Destacada";
                } else if ($promedios[$i]['promedio'] >= 4.0) {
                    $comentario = "Por haber Cursado y Aprobado de forma satisfactoria el Curso";
                } else {
                    $comentario = "Por haber Participado del Curso";
                    $diploma = 1;
                }

                if ($participacion == 1 ) {
                    if( $porcentaje == 1){
                        $comentario = "Por haber Participado del Curso";
                        $diploma = 1;
                        if ($diploma == 1) {
                            $count++;
                            // add a page
                            $pdf->AddPage('L', 'LETTER');
                            $pdf->SetFont('times', '', 26);
                            // Display image on full page
                            if ($fondo == 1) {
                                $pdf->Image(base_url() . 'assets/amelia/images/backgrounds/image_demo.png', 0, 0, 385, 297, 'PNG', '', '', true, 200, '', false, false, 0, false, false, true);
                            }
                            $html = '<br>
                                <br><span>&nbsp;<br><br></span>
                                <br>
                                <br>
                                <p style="font-style:italic;font-size:14pt;" align="center">La Direcci&oacute;n General de nuestro Instituto hace entrega del presente Certificado a</p>		
                                <p style="font-style:italic;font-size:18pt;font-weight: bold;" align="center">' . $datosAl[0]['nombre'] . '</p>		
                                <p style="font-style:italic;font-size:14pt;" align="center">' . $comentario . '</p>		
                                <p style="font-style:italic;font-weight:bold;font-size:14pt;" align="center">' . '' . $nombreP . '' . ' Modalidad ' . $datosAl[0]['nombre_modalidad'] . '</p>		
                                <p style="font-style:italic;font-size:14pt;" align="center">Realizado en Santiago entre el ' . $datosAl[0]['fecha_inicio'] . ' y el ' . $datosAl[0]['fecha_fin'] . '</p>	
                                ';
                        	
                            

                            if ($firma == 1) {
                                $html .= '<p style="font-family:italic;font-size:18pt;">&nbsp;</p><p style="font-family:italic;font-size:9pt;" align="center">___________________________<br>';
                                $html .= '<span style="font-weight: bold;">' . htmlentities(ucwords(strtolower($nombreE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</span><br>'
                                        . '' . htmlentities(ucwords(strtolower($cargoE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '<br>'
                                        . '' . htmlentities(ucwords(strtolower($empresa)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</p>';
                            }
                            $pdf->writeHTML($html, true, false, true, false, '');
                        }
                    }else{
                        $comentario = "Por haber Participado del Curso";
                        $diploma = 1;
                        if ($diploma == 1) {
                            $count++;
                            // add a page
                            $pdf->AddPage('L', 'LETTER');
                            $pdf->SetFont('times', '', 26);
                            // Display image on full page
                            if ($fondo == 1) {
                                $pdf->Image(base_url() . 'assets/amelia/images/backgrounds/image_demo.png', 0, 0, 385, 297, 'PNG', '', '', true, 200, '', false, false, 0, false, false, true);
                            }
                            $html = '<br>
                                <br><span>&nbsp;<br><br></span>
                                <br>
                                <br>
                                <p style="font-style:italic;font-size:14pt;" align="center">La Direcci&oacute;n General de nuestro Instituto hace entrega del presente Certificado a</p>		
                                <p style="font-style:italic;font-size:18pt;font-weight: bold;" align="center">' . $datosAl[0]['nombre'] . '</p>		
                                <p style="font-style:italic;font-size:14pt;" align="center">' . $comentario . '</p>		
                                <p style="font-style:italic;font-weight:bold;font-size:14pt;" align="center">' . '' . $nombreP . '' . ' Modalidad ' . $datosAl[0]['nombre_modalidad'] . '</p>		
                                <p style="font-style:italic;font-size:14pt;" align="center">Realizado en Santiago entre el ' . $datosAl[0]['fecha_inicio'] . ' y el ' . $datosAl[0]['fecha_fin'] . '</p>	
                                <p style="font-style:italic;font-size:14pt;" align="center">con un total de ' . $datosAl[0]['duracion_curso'] . ' horas</p>';
                        	
                            

                            if ($firma == 1) {
                                $html .= '<p style="font-family:italic;font-size:18pt;">&nbsp;</p><p style="font-family:italic;font-size:9pt;" align="center">___________________________<br>';
                                $html .= '<span style="font-weight: bold;">' . htmlentities(ucwords(strtolower($nombreE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</span><br>'
                                        . '' . htmlentities(ucwords(strtolower($cargoE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '<br>'
                                        . '' . htmlentities(ucwords(strtolower($empresa)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</p>';
                            }
                            $pdf->writeHTML($html, true, false, true, false, '');
                        }
                    }
                } else {
                    if ($diploma == 0) {
                        $count++;
                        // add a page
                        $pdf->AddPage('L', 'LETTER');
                        $pdf->SetFont('times', '', 26);
                        // Display image on full page
                        if ($fondo == 1) {
                            $pdf->Image(base_url() . 'assets/amelia/images/backgrounds/image_demo.png', 0, 0, 385, 297, 'PNG', '', '', true, 200, '', false, false, 0, false, false, true);
                        }
                        $html = '<br>
                            <br><span>&nbsp;<br><br></span>
                            <br>
                            <br>
                            <p style="font-style:italic;font-size:14pt;" align="center">La Direcci&oacute;n General de nuestro Instituto hace entrega del presente Certificado a</p>		
                            <p style="font-style:italic;font-size:18pt;font-weight: bold;" align="center">' . $datosAl[0]['nombre'] . '</p>		
                            <p style="font-style:italic;font-size:14pt;" align="center">' . $comentario . '</p>		
                            <p style="font-style:italic;font-weight:bold;font-size:14pt;" align="center">' . '' . $nombreP . '' . ' Modalidad ' . $datosAl[0]['nombre_modalidad'] . '</p>		
                            <p style="font-style:italic;font-size:14pt;" align="center">Realizado en Santiago entre el ' . $datosAl[0]['fecha_inicio'] . ' y el ' . $datosAl[0]['fecha_fin'] . '</p>		
                            <p style="font-style:italic;font-size:14pt;" align="center">con un total de ' . $datosAl[0]['duracion_curso'] . ' horas</p>		
                            <p style="font-style:italic;font-size:7pt;" align="center">Código de validez ' . $promedios[$i]['id_detalle_alumno'] . '</p>';
                        if ($firma == 1) {
                            $html .= '<p style="font-family:italic;font-size:18pt;">&nbsp;</p><p style="font-family:italic;font-size:9pt;" align="center">___________________________<br>';
                            $html .= '<span style="font-weight: bold;">' . htmlentities(ucwords(strtolower($nombreE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</span><br>'
                                    . '' . htmlentities(ucwords(strtolower($cargoE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '<br>'
                                    . '' . htmlentities(ucwords(strtolower($empresa)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</p>';
                        }
                        $pdf->writeHTML($html, true, false, true, false, '');
                    }
                }
            }
        }
        if ($count > 0) {
            $arr_datos = array(
                'id_ficha' => $id_ficha,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $this->modelo->setDiplomas($arr_datos);
        } else {
            $pdf->AddPage('L', 'LETTER');
            $pdf->SetFont('times', '', 22);
            $html = '<p>No existen diplomas para generar, favor verifique las notas de los alumnos</p>';
            $pdf->writeHTML($html, true, false, true, false, '');
        }
        //Close and output PDF document
        $pdf->Output('Certificados_Ficha_' . $num_ficha . '.pdf', 'I');
        //echo json_encode(array($promedios,$datos_alumnos));
    }

    public function generarDiplomaPorAlumno($id_ficha, $id_orden, $id_dtl_alumno) {


        $participacion = 0;
        $porcentaje = 0;
        $fondo = 1;
        $opcNombre = 1;
        $nombreP = '';
        $nombreE = '';
        $firma = 0;

        $arr_datos = array(
            'id_ficha' => $id_ficha,
            'id_orden' => $id_orden,
            'id_detalle_alumno' => $id_dtl_alumno
        );

        $promedios = $this->modelo->getPromedioAlumno($arr_datos);
        if($promedios[0]['promedio'] < 4){
            $participacion = 1;
        }

        $this->load->library('Pdf');
        $pdf = new pdf('L', 'mm', 'LETTER');
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        if ($fondo == 1) {
            $pdf->SetMargins(0, 0, 0, true);
        } else {
            $pdf->SetMargins(0, 0, -10, true);
        }

        $num_ficha = $promedios[0]['num_ficha'];
        // set auto page breaks false
        $pdf->SetAutoPageBreak(false, 0);
        $count = 0;
        $datos_alumnos = array();


        //bloque ql para todos los alumnos
        if (count($promedios) > 0) {


            for ($i = 0; $i < count($promedios); $i++) {
                $diploma = 0;
                $arr_datos = array(
                    'id_detalle_alumno' => $promedios[$i]['id_detalle_alumno'],
                    'participacion' => $participacion,
                    'porcentaje' => $porcentaje,
                    'user_id' => $this->session->userdata('id_user'),
                    'user_perfil' => $this->session->userdata('id_perfil')
                );
                $datosAl = $this->modelo->get_datos_diplomas($arr_datos);



                if ($promedios[$i]['promedio'] >= 6.7) {
                    $comentario = "Por haber Cursado y Aprobado con distinci&oacute;n M&aacute;xima de forma Destacada";
                    array_push($datosAl, $comentario);
                } else if ($promedios[$i]['promedio'] >= 4.0) {
                    $comentario = "Por haber Cursado y Aprobado de forma satisfactoria el Curso";
                    array_push($datosAl, $comentario);
                } else {
                    $comentario = "Por haber Participado del Curso";
                    array_push($datosAl, $comentario);
                    $diploma = 1;
                }
                array_push($datos_alumnos, $datosAl);
                if ($opcNombre == '1') {
                    $nombreP = $datosAl[0]['nombre_curso'];
                }
                if ($opcNombre == '2') {
                    $nombreP = $datosAl[0]['nombre_curso_code_sence'];
                }
                if ($opcNombre == '3') {
                    $nombreP = $this->input->post('saludos');
                }

                if ($promedios[$i]['promedio'] >= 6.7) {
                    $comentario = "Por haber Cursado y Aprobado con distinci&oacute;n M&aacute;xima de forma Destacada";
                } else if ($promedios[$i]['promedio'] >= 4.0) {
                    $comentario = "Por haber Cursado y Aprobado de forma satisfactoria el Curso";
                } else {
                    $comentario = "Por haber Participado del Curso";
                    $diploma = 1;
                }

                if ($participacion == 1 ) {
                    if( $porcentaje == 1){
                        $comentario = "Por haber Participado del Curso";
                        $diploma = 1;
                        if ($diploma == 1) {
                            $count++;
                            // add a page
                            $pdf->AddPage('L', 'LETTER');
                            $pdf->SetFont('times', '', 26);
                            // Display image on full page
                            if ($fondo == 1) {
                                $pdf->Image(base_url() . 'assets/amelia/images/backgrounds/image_demo.png', 0, 0, 385, 297, 'PNG', '', '', true, 200, '', false, false, 0, false, false, true);
                            }
                            $html = '<br>
                                <br><span>&nbsp;<br><br></span>
                                <br>
                                <br>
                                <p style="font-style:italic;font-size:14pt;" align="center">La Direcci&oacute;n General de nuestro Instituto hace entrega del presente Certificado a</p>		
                                <p style="font-style:italic;font-size:18pt;font-weight: bold;" align="center">' . $datosAl[0]['nombre'] . '</p>		
                                <p style="font-style:italic;font-size:14pt;" align="center">' . $comentario . '</p>		
                                <p style="font-style:italic;font-weight:bold;font-size:14pt;" align="center">' . '' . $nombreP . '' . ' Modalidad ' . $datosAl[0]['nombre_modalidad'] . '</p>		
                                <p style="font-style:italic;font-size:14pt;" align="center">Realizado en Santiago entre el ' . $datosAl[0]['fecha_inicio'] . ' y el ' . $datosAl[0]['fecha_fin'] . '</p>	
                                ';
                        	
                            

                            if ($firma == 1) {
                                $html .= '<p style="font-family:italic;font-size:18pt;">&nbsp;</p><p style="font-family:italic;font-size:9pt;" align="center">___________________________<br>';
                                $html .= '<span style="font-weight: bold;">' . htmlentities(ucwords(strtolower($nombreE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</span><br>'
                                        . '' . htmlentities(ucwords(strtolower($cargoE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '<br>'
                                        . '' . htmlentities(ucwords(strtolower($empresa)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</p>';
                            }
                            $pdf->writeHTML($html, true, false, true, false, '');
                        }
                    }else{
                        $comentario = "Por haber Participado del Curso";
                        $diploma = 1;
                        if ($diploma == 1) {
                            $count++;
                            // add a page
                            $pdf->AddPage('L', 'LETTER');
                            $pdf->SetFont('times', '', 26);
                            // Display image on full page
                            if ($fondo == 1) {
                                $pdf->Image(base_url() . 'assets/amelia/images/backgrounds/image_demo.png', 0, 0, 385, 297, 'PNG', '', '', true, 200, '', false, false, 0, false, false, true);
                            }
                            $html = '<br>
                                <br><span>&nbsp;<br><br></span>
                                <br>
                                <br>
                                <p style="font-style:italic;font-size:14pt;" align="center">La Direcci&oacute;n General de nuestro Instituto hace entrega del presente Certificado a</p>		
                                <p style="font-style:italic;font-size:18pt;font-weight: bold;" align="center">' . $datosAl[0]['nombre'] . '</p>		
                                <p style="font-style:italic;font-size:14pt;" align="center">' . $comentario . '</p>		
                                <p style="font-style:italic;font-weight:bold;font-size:14pt;" align="center">' . '' . $nombreP . '' . ' Modalidad ' . $datosAl[0]['nombre_modalidad'] . '</p>		
                                <p style="font-style:italic;font-size:14pt;" align="center">Realizado en Santiago entre el ' . $datosAl[0]['fecha_inicio'] . ' y el ' . $datosAl[0]['fecha_fin'] . '</p>	
                                <p style="font-style:italic;font-size:14pt;" align="center">con un total de ' . $datosAl[0]['duracion_curso'] . ' horas</p>';
                        	
                            

                            if ($firma == 1) {
                                $html .= '<p style="font-family:italic;font-size:18pt;">&nbsp;</p><p style="font-family:italic;font-size:9pt;" align="center">___________________________<br>';
                                $html .= '<span style="font-weight: bold;">' . htmlentities(ucwords(strtolower($nombreE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</span><br>'
                                        . '' . htmlentities(ucwords(strtolower($cargoE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '<br>'
                                        . '' . htmlentities(ucwords(strtolower($empresa)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</p>';
                            }
                            $pdf->writeHTML($html, true, false, true, false, '');
                        }
                    }
                } else {
                    if ($diploma == 0) {
                        $count++;
                        // add a page
                        $pdf->AddPage('L', 'LETTER');
                        $pdf->SetFont('times', '', 26);
                        // Display image on full page
                        if ($fondo == 1) {
                            $pdf->Image(base_url() . 'assets/amelia/images/backgrounds/image_demo.png', 0, 0, 385, 297, 'PNG', '', '', true, 200, '', false, false, 0, false, false, true);
                        }
                        $html = '<br>
                            <br><span>&nbsp;<br><br></span>
                            <br>
                            <br>
                            <p style="font-style:italic;font-size:14pt;" align="center">La Direcci&oacute;n General de nuestro Instituto hace entrega del presente Certificado a</p>		
                            <p style="font-style:italic;font-size:18pt;font-weight: bold;" align="center">' . $datosAl[0]['nombre'] . '</p>		
                            <p style="font-style:italic;font-size:14pt;" align="center">' . $comentario . '</p>		
                            <p style="font-style:italic;font-weight:bold;font-size:14pt;" align="center">' . '' . $nombreP . '' . ' Modalidad ' . $datosAl[0]['nombre_modalidad'] . '</p>		
                            <p style="font-style:italic;font-size:14pt;" align="center">Realizado en Santiago entre el ' . $datosAl[0]['fecha_inicio'] . ' y el ' . $datosAl[0]['fecha_fin'] . '</p>		
                            <p style="font-style:italic;font-size:14pt;" align="center">con un total de ' . $datosAl[0]['duracion_curso'] . ' horas</p>		
                            <p style="font-style:italic;font-size:7pt;" align="center">Código de validez ' . $promedios[$i]['id_detalle_alumno'] . '</p>';
                        if ($firma == 1) {
                            $html .= '<p style="font-family:italic;font-size:18pt;">&nbsp;</p><p style="font-family:italic;font-size:9pt;" align="center">___________________________<br>';
                            $html .= '<span style="font-weight: bold;">' . htmlentities(ucwords(strtolower($nombreE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</span><br>'
                                    . '' . htmlentities(ucwords(strtolower($cargoE)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '<br>'
                                    . '' . htmlentities(ucwords(strtolower($empresa)), ENT_QUOTES | ENT_HTML401, 'UTF-8') . '</p>';
                        }
                        $pdf->writeHTML($html, true, false, true, false, '');
                    }
                }
            }
        }
        if ($count > 0) {
            $arr_datos = array(
                'id_ficha' => $id_ficha,
                'user_id' => $this->session->userdata('id_user'),
                'user_perfil' => $this->session->userdata('id_perfil')
            );
            $this->modelo->setDiplomas($arr_datos);
        } else {
            $pdf->AddPage('L', 'LETTER');
            $pdf->SetFont('times', '', 22);
            $html = '<p>No existen diplomas para generar, favor verifique las notas de los alumnos</p>';
            $pdf->writeHTML($html, true, false, true, false, '');
        }
        //Close and output PDF document
        $pdf->Output('Certificados_Ficha_' . $num_ficha . '.pdf', 'I');
        //echo json_encode(array($promedios,$datos_alumnos));
    }

}
