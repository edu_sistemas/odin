<?php

/**
 * recuperarpassword
 * Description...
 * @version 0.0.1
 * Ultima edicion:	2017-03-28 [Luis Jarpa ]<ljarpa@edutecno.cl>
 * Fecha creacion:	2017-03-28 [Luis Jarpa] <ljarpa@edutecno.cl>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class RecuperarPassword extends CI_Controller {

    // Variables paramétricas
    private $nombre_item_singular = 'Password';
    private $nombre_item_plural = 'Password';
    private $package = 'back_office';
    private $model = 'Password_model';
    private $view = 'RecuperarPassword_v';
    private $controller = 'RecuperarPassword';
    private $ind = '';

    // Inicio Construct
    function __construct() {
        parent::__construct();

        $this->load->model($this->package . '/' . $this->model, 'modelo');
        //  Libreria de email
    }

    public function index() {

        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;

        // array con los css necesarios para la pagina
        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/RecuperarPassword.js')
        );

        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->view;

        $this->load->view('amelia_login/template', $data);
    }

    function forgotPassword() {

        $arr_input_data = array(
            'rut_usuario' => $this->input->post('user_name')
        );

        $respuesta = $this->modelo->set_reset_password($arr_input_data);


        $id_user = $respuesta[0]['id_user'];
        $nombre_user = $respuesta[0]['nombre_user'];
        $correo_usuario = $respuesta[0]['correo_usuario'];
        $new_pass = $respuesta[0]['new_pass'];


        /* Parametros comuines inicio */
        $titulo = $respuesta[0]['titulo'];
        $mensaje = $respuesta[0]['mensaje'];
        $tipo_alerta = $respuesta[0]['tipo_alerta'];
        $reset = $respuesta[0]['reset'];

        /* Parametros comuines fin */

        if ($reset == 1) {
            $destinatarios = array(
                array('email' => $correo_usuario, 'nombre' => $nombre_user),
            );

            $cc = array(
                array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
                
            );

            $asunto = "Restablecer contraseña"; // asunto del correo
            // presenciales
            $body = $this->generaHtml($nombre_user, $new_pass); // mensaje completo HTML o text

            $AltBody = $asunto; // hover del cursor sobre el correo

            /*             * ** inicio array ** */
            $data_email = array(
                // indica a quienes esta dirigido el  email
                "destinatarios" => $destinatarios,
                "cc_a" => $cc, // asunto del correo
                "asunto" => $asunto,
                // contenido del correo , puede ser html o solo texto
                "body" => $body,
                // AltBody (esto no sirve para para pero lo requiere la libreria XD)
                "AltBody" => $AltBody
            );
            /*             * ** Fin array ** */

            $this->load->library('Libreriaemail');
            $res = $this->libreriaemail->CallAPISendMail($data_email);
        }




//         dfgdfulgukdfhguidu






        echo json_encode($respuesta);
    }

    function generaHtml($nombre_user, $new_pass) {



        $html = "";

        $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">';
        $html .= '<html xmlns="http://www.w3.org/1999/xhtml">';
        $html .= '<head>';
        $html .= '<meta name="viewport" content="width=device-width" />';
        $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
        $html .= '<title>Bienvenido a Odin  </title>';
        $html .= '<link href="http://odin.edutecno.com/assets/amelia/css/styles_emails.css" media="all" rel="stylesheet" type="text/css" />';
        $html .= '</head>';
        $html .= '<body>';
        $html .= '<table class="body-wrap">';
        $html .= '<tr>';
        $html .= '<td></td>';
        $html .= '<td class="container" width="600">';
        $html .= '<div class="content">';
        $html .= '<table class="main" width="100%" cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td class="content-wrap">';
        $html .= '<table  cellpadding="0" cellspacing="0">';
        $html .= '<tr>';
        $html .= '<td >';
        $html .= '<img class="img-responsive" src="http://odin.edutecno.com/assets/amelia/images/header_email.jpg"/>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">  ';
        $html .= '<h3>¡Olvidé mi contraseña !</h3>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Estimado ' . $nombre_user . ', nuestros sistemas han detectado que han solicitado restablecer su contraseña.';
        $html .= '<br>';
        $html .= 'Su nueva contraseña es:  ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">                                               ';
        $html .= '<b>CONTRASEÑA:</b>    ' . $new_pass;
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block">';
        $html .= 'Si usted NO solicitó cambio de contraseña,';
        $html .= 'por favor contáctese de inmediato con el Departamento E-Learning -';
        $html .= 'Área de Sistemas. ';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="content-block aligncenter">';
        $html .= '<a href="http://odin.edutecno.com" target="_blank" class="btn-primary">Iniciar Sesión en Odin</a>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<div class="footer">';
        $html .= '<table width="100%">';
        $html .= '<tr>';
        $html .= '<td id="header-text" class="aligncenter content-block"></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div></div>';
        $html .= '</td>';
        $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '<script type="text/javascript">';
        $html .= 'var d = new Date();';
        $html .= 'var n = d.getFullYear();';
        $html .= 'document.getElementById("header-text").innerHTML = "&copy; " + n + " Edutecno Capacitación";';
        $html .= '</script>';
        $html .= '</body>';
        $html .= '</html>';




        return $html;
    }

}

/* End of file Login_c.php */
/* Location: ./application/controllers/Login_c.php */
?>
