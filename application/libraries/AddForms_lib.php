<?php

/**
 * AddForms_lib
 * Description...
 * @version 0.0.1
 * Ultima edicion:	2016-07-07 [Carlos Aravena] <caravena@edutecno.com>
 * Fecha creacion:	2016-06-16 [Cristian Aguirre] <caguirre@edutecno.cl>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class AddForms_lib {

    //2016-06-16
    // metodo que concatena partes de un formulario, se utiliza desde la vista
    function render_form($arr_cfg) {
        $arr_form = array();
        foreach ($arr_cfg as $key => $value) {
            switch ($value['type']) {
                case 'text':
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_text($value, '');
                    break;
                case 'text_readonly':
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_text($value, 'readonly');
                    break;
                case 'textarea':
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_textarea($value);
                    break;
                case 'select':
                    if (!isset($value['combo'])) {
                        die('1.- Libreria AddForms_lib, metodo render_form: falta definir el combo para ' . $value['field'] . ', ' . $value['label']);
                    }
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_select($value, $value['combo'], $selected_item);
                    break;
                case 'date':
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_date($value, '');
                    break;
                case 'file':
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_file($value, '');
                    break;
                case 'text_login':
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_text_login($value, '');
                    break;
                case 'password_login':
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_password_login($value, '');
                    break;
                case 'checkbox_login':
                    if (!isset($value['value'])) {
                        $selected_item = '';
                    } else {
                        $selected_item = $value['value'];
                    }
                    $arr_form[$key] = $this->form_type_checkbox_login($value, '');
                    break;
            }
        }
        return $arr_form;
    }

    // 2016-07-07
    // metodo para cargar las funciones utilitarias para los formularios
    function render_form_script($arr_cfg) {
        $form_script = '';
        $form_script_final = '';
        $select_contact = false;
        foreach ($arr_cfg as $key => $value) {
            switch ($value['type']) {
                case 'select':
                    if (!$select_contact) {
                        $form_script = $this->desde_dd_a_h();
                        $select_contact = true;
                        $form_script_final = '<script>' . $form_script . '</script>';
                    }
                    break;
            }
        }
        return $form_script_final;
    }

    function form_type_text($value, $readonly) {
        if (!isset($value['value'])) {
            $input_value = '';
        } else {
            $input_value = $value['value'];
        }
        $placeholder = 'Escriba Aqu&iacute;';
        if (isset($value['placeholder'])) {
            if (strlen($value['placeholder']) > 0) {
                $placeholder = $value['placeholder'];
            }
        }
        $script = '';
        $script .= '<div class="form-group container pull-left">';
        $script .= '<?php $f_key = 1;?>';
        $script .= '<div class="row ">';
        $script .= '<div class="col-md-3">';
        $script .= '<label control-label">' . ucfirst($value['label']);
        if (strpos($value['rules'], 'required') > -1)
            $script .= ' <i class="fa fa-asterisk" style="color:#AB0606;"></i>';
        $script .= '</label>';
        $script .= '</div>';
        $script .= '<div class="col-md-9">';
        $script .= '<input ' . $readonly . ' name="' . $value['field'] . '" type="text" style="max-width:740px;" class="form-control input-sm" placeholder="' . $placeholder . '"
                                value="' . set_value($value['field'], $input_value) . '">';
        $script .= '<span class="help-block">' . form_error($value['field']) . '</span>';
        $script .= '</div>';
        $script .= '</div>';
        $script .= '</div>';
        return $script;
    }

    function form_type_textarea($value) {
        if (isset($value['value'])) {
            $value_aux = $value['value'];
        } else {
            $value_aux = '';
        }
        $placeholder = 'Escriba Aqu&iacute;';
        if (isset($value['placeholder'])) {
            if (strlen($value['placeholder']) > 0) {
                $placeholder = $value['placeholder'];
            }
        }
        $script = '';
        $script .= '<div class="form-group container pull-left">';
        $script .= '<?php $f_key = 1;?>';
        $script .= '<div class="row ">';
        $script .= '<div class="col-md-3">';
        $script .= '<label control-label">' . ucfirst($value['label']);
        if (strpos($value['rules'], 'required') > -1)
            $script .= ' <i class="fa fa-asterisk" style="color:#AB0606;"></i>';
        $script .= '</label>';
        $script .= '</div>';
        $script .= '<div class="col-md-9">';
        $script .= '<textarea name="' . $value['field'] . '" type="text" style="max-width:740px;" class="form-control input-sm" placeholder="' . $placeholder . '" >' . set_value($value['field'], $value_aux) . '</textarea>';
        $script .= '<span class="help-block">' . form_error($value['field']) . '</span>';
        $script .= '</div>';
        $script .= '</div>';
        $script .= '</div>';

        return $script;
    }

    function form_type_select($value, $arr_dropdown, $preloadoptions) {
        if (isset($value['value'])) {
            $value_aux = $value['value'];
        } else {
            $value_aux = '';
        }
        $script = '';
        $script .= '<div class="form-group container pull-left">';
        $script .= '<div class="row ">';
        $script .= '<div class="col-md-3">';
        $script .= '<label control-label">' . ucfirst($value['label']);
        if (strpos($value['rules'], 'required') > -1)
            $script .= ' <i class="fa fa-asterisk" style="color:#AB0606;"></i>';
        $script .= '</label>';
        $script .= '</div>';
        $script .= '<div class="col-md-9">';
        $js = 'id="dd_' . $value['field'] . '" onChange="desde_dd_a_h(\'dd_' . $value['field'] . '\', \'' . $value['field'] . '\');"';
        $style = 'class="form-control input-sm" style="max-width:740px;"';
        $script .= form_dropdown('select_' . $value['field'], $arr_dropdown, set_value($value['field'], $preloadoptions), $style . $js);
        $script .= '<span class="help-block">' . form_error($value['field']) . '</span>';
        $script .= '</div>';
        $script .= '</div>';
        $script .= '</div>';
        return $script;
    }

    // 2016-07-04
    function form_type_date($value) {
        if (isset($value['value'])) {
            $value_aux = $value['value'];
        } else {
            $value_aux = '';
        }
        $placeholder = 'Escriba Aqu&iacute;';
        if (isset($value['placeholder'])) {
            if (strlen($value['placeholder']) > 0) {
                $placeholder = $value['placeholder'];
            }
        }
        $script = '';
        $script .= '<div class="form-group container pull-left">';
        $script .= '<div class="row ">';
        $script .= '<div class="col-md-3">';
        $script .= '<label control-label">' . ucfirst($value['label']);
        if (strpos($value['rules'], 'required') > -1)
            $script .= ' <i class="fa fa-asterisk" style="color:#AB0606;"></i>';
        $script .= '</label>';
        $script .= '</div>';
        $script .= '<div class="col-md-9">';
        $script .= '<div class="input-group">';
        $script .= '<span class="input-group-addon"><i class="icon-calendar22"></i></span>';
        $script .= '<input name="' . $value['field'] . '" type="date" style="max-width:700px;" class="form-control input-sm pickadate-accessibility picker__input picker__input--active" placeholder="' . $placeholder . '" readonly id="txtFecha" aria-haspopup="true" aria-expanded="true" aria-readonly="false" aria-owns="txtFecha_root" value="' . set_value($value['field'], $value_aux) . '" />';
        $script .= '</div>';
        $script .= '<span class="help-block">' . form_error($value['field']) . '</span>';
        $script .= '</div>';
        $script .= '</div>';
        $script .= '</div>';
        return $script;
    }

    // 2016-07-27
    function form_type_file($value) {
        if (isset($value['value'])) {
            $value_aux = $value['value'];
        } else {
            $value_aux = '';
        }
        $placeholder = 'Escriba Aqu&iacute;';
        if (isset($value['placeholder'])) {
            if (strlen($value['placeholder']) > 0) {
                $placeholder = $value['placeholder'];
            }
        }
        $script = '';
        $script .= '<fieldset class="content-group">';
        $script .= '<div class="container">';
        $script .= '<div class="row">';
        $script .= '<div class="col-md-3">';
        $script .= '<label class="control-label">' . ucfirst($value['label']);
        if (strpos($value['rules'], 'required') > -1)
            $script .= ' <i class="fa fa-asterisk" style="color:#AB0606;"></i>';
        $script .= '</label>';
        $script .= '</div><!--/.col-md-3-->';
        $script .= '<div class="col-md-9" style="max-width: 700px;">';
        $script .= '<input name="' . $value['field'] . '" type="file" class="file-styled" placeholder="' . $placeholder . '"/>';
        $script .= '<span class="help-block">' . form_error($value['field']) . '</span>';
        $script .= '</div><!--/.col-md-9-->';
        $script .= '</div><!--/.row-->';
        $script .= '</div><!--/.container-->';
        $script .= '</fieldset><!--/.content-group-->';
        return $script;
    }

    // 2016-08-03
    // Form to users login
    function form_type_text_login($value) {
        if (isset($value['value'])) {
            $value_aux = $value['value'];
        } else {
            $value_aux = "";
        }
        $placeholder = 'Escriba Aqu&iacute;';
        if (isset($value['placeholder'])) {
            if (strlen($value['placeholder']) > 0) {
                $placeholder = $value['placeholder'];
            }
        }
        $script = '';
        $script .= '<div class="form-group has-feedback has-feedback-left">';
        $script .= '<label class="sr-only">' . ucfirst($value['label']);
        if (strpos($value['rules'], 'required') > -1)
            $script .= ' <i class="fa fa-asterisk" style="color:#AB0606;"></i>';
        $script .= '</label>';
        $script .= '<input type="text" name="' . $value['field'] . '" class="form-control" placeholder="' . $placeholder . '">';
        $script .= '<span class="help-block">' . form_error($value['field']) . '</span>';
        $script .= '<div class="form-control-feedback">';
        $script .= '<i class="icon-user text-muted"></i>';
        $script .= '</div>';
        $script .= '</div>';
        return $script;
    }

    // 2016-08-03
    // Form input password to users login
    function form_type_password_login($value) {
        if (isset($value['value'])) {
            $value_aux = $value['value'];
        } else {
            $value_aux = "";
        }
        $placeholder = 'Escriba Aqu&iacute;';
        if (isset($value['placeholder'])) {
            if (strlen($value['placeholder']) > 0) {
                $placeholder = $value['placeholder'];
            }
        }
        $script = '';
        $script .= '<div class="form-group has-feedback has-feedback-left">';
        $script .= '<label class="sr-only">' . ucfirst($value['label']);
        if (strpos($value['rules'], 'required') > -1)
            $script .= ' <i class="fa fa-asterisk" style="color:#AB0606;"></i>';
        $script .= '</label>';
        $script .= '<input type="password" name="' . $value['field'] . '" class="form-control" placeholder="' . $placeholder . '" autocomplete="off">';
        $script .= '<span class="help-block">' . form_error($value['field']) . '</span>';
        $script .= '<div class="form-control-feedback">';
        $script .= '<i class="icon-lock2 text-muted"></i>';
        $script .= '</div>';
        $script .= '</div>';
        return $script;
    }

    // 2016-08-03
    // Checkbox to remember the password
    function form_type_checkbox_login($value) {
        if (isset($value['value'])) {
            $value_aux = $value['value'];
        } else {
            $value_aux = "";
        }
        $script = '';
        $script .= '<div class="form-group login-options">';
        $script .= '<div class="row">';
        $script .= '<div class="col-sm-6">';
        $script .= '<label class="checkbox-inline">';
        $script .= '<input name="' . $value['field'] . '" type="checkbox" class="styled" checked="checked">
                                        Recordar';
        $script .= '</label>';
        $script .= '</div>';
        $script .= '<div class="col-sm-6 text-right">';
        $script .= '<a href="RecuperarPassword">Olvidaste tu contrase&ntilde;a&#63;</a>';
        $script .= '</div>';
        $script .= '</div>';
        $script .= '</div>';
        return $script;
    }

    // 2016-07-07
    function desde_dd_a_h() {
        $script = "function desde_dd_a_h(id_dropdown, name_hidden)
                  {
                        var dd = document.getElementById(id_dropdown);
                        document.getElementsByName(name_hidden)[0].value = dd.options[dd.selectedIndex].value;
                        console.log('change'+name_hidden);
                   }";
        return $script;
    }

    // 2016-07-13
    function create_hidden($arr_config) {
        $arr_hidden_config = array();
        foreach ($arr_config as $key => $value) {
            if (isset($value['value']) && $value['type'] == 'select') {
                $arr_hidden_config[$value['field']] = '';
                if (strlen($value['value']) > 0) {
                    $arr_hidden_config[$value['field']] = $value['value'];
                }
            }
        }
        return $arr_hidden_config;
    }

}

/* End of file AddForms_lib.php */
/* Location: ./application/libraries/AddForms_lib.php */
