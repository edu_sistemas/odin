
<?php
require(dirname(__FILE__).'/fpdf/fpdf.php');
class Cotizacion{

	var $img_fondo1;
	var $img_fondo2;
	var $curso;
	var $fechaInicio;
	var $cliente;
	var $rut;
	var $contacto;
	var $telefono;
	var $direccion;
	var $ciudad;
	var $email;
	var $fecha;
	var $descuento;
	var $pago;
	var $descripcionCurso;
	var $version;
	var $modalidad;
	var $nivel;
	var $horas;
	var $valorParticipante;
	var $numeroAlumnos;
	var $observaciones;
	var $valorTotal;

	function __construct(){ 
      	
   	} 

	function Generar($curso,$fechaInicio,$cliente,$rut,$contacto,$telefono,$direccion,$ciudad,$email,$fecha,$descuento,$pago,$descripcionCurso,$version,$modalidad,$nivel,$horas,$valorParticipante,$numeroAlumnos,$observaciones,$valorTotal){ 
      	$this->img_fondo1 = dirname(__FILE__) . '/img_fondo.jpg';
      	$this->img_fondo2 = dirname(__FILE__) . '/img_fondo_pag2.jpg';;
      	$this->curso = $curso;
      	$this->fechaInicio = $fechaInicio;
      	$this->cliente = $cliente;
      	$this->rut = $rut;
      	$this->contacto = $contacto;
      	$this->telefono = $telefono;
      	$this->direccion = $direccion;
      	$this->ciudad = $ciudad;
      	$this->email = $email;
      	$this->fecha = $fecha;
      	$this->descuento = $descuento;
      	$this->pago = $pago;
      	$this->descripcionCurso = $descripcionCurso;
      	$this->version = $version;
      	$this->modalidad = $modalidad;
      	$this->nivel = $nivel;
      	$this->horas = $horas;
      	$this->valorParticipante = $valorParticipante;
      	$this->numeroAlumnos = $numeroAlumnos;
      	$this->observaciones = $observaciones;
      	$this->valorTotal = $valorTotal;
   	} 

   	function mostrarPdf(){
		$pdf=new FPDF();
		$pdf->AddPage();
		$pdf->Image($this->img_fondo1,0,0,$pdf->GetPageWidth(),$pdf->GetPageHeight());
		$pdf->AddPage();
		$pdf->Image($this->img_fondo2,0,0,$pdf->GetPageWidth(),$pdf->GetPageHeight());
		//titulo
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(255,255,255);
		$pdf->SetXY(98,50);
		$pdf->Write(5,utf8_decode('PROPUESTA ECONÓMICA'));
		$pdf->Ln();
		$pdf->SetFont('Arial','B',15);
		$pdf->SetXY(98,60);
		$pdf->Cell(90,5,utf8_decode('Curso: '.((strlen($this->curso) > 30) ? substr($this->curso, 0, 30)."..." : $this->curso)),0, 0, 'C');

		$pdf->SetXY(98,75);
		$pdf->SetFont('Arial','B',11);
		$pdf->SetTextColor(0,147,180);
		$pdf->Cell(90,5,utf8_decode('FECHA INICIO Y TERMINO DEL CURSO'),0,0,'C');
		$pdf->Ln();
		$pdf->SetFont('Arial','B',10);
		$pdf->SetXY(98,80);
		$pdf->Cell(90,5,utf8_decode($this->fechaInicio),0, 0, 'C');
		//información costado izquierdo
		$pdf->SetFont('Arial','',12);
		$pdf->SetTextColor(100,100,100);
		$pdf->SetXY(5,93);
		$pdf->Write(5,'CLIENTE');
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,$this->cliente);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,'RUT');
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,$this->rut);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,'CONTACTO');
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,utf8_decode($this->contacto));
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,utf8_decode('TELÉFONO'));
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,$this->telefono);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,utf8_decode('DIRECCIÓN'));
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->MultiCell(60,5,utf8_decode($this->direccion));
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,'CIUDAD');
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,utf8_decode($this->ciudad));
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,'EMAIL');
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->MultiCell(60,5,$this->email);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,utf8_decode('FECHA GENERACIÓN'));
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,$this->fecha);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,'DESCUENTO (%)');
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,$this->descuento);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,'F. PAGO');
		$pdf->Ln();
		$pdf->SetX(5);
		$pdf->Write(5,utf8_decode($this->pago));
		//informacion costado derecho
		$pdf->SetXY(75,93);
		$pdf->Write(5,utf8_decode('DESCRIPCIÓN DEL CURSO'));
		$pdf->Ln();
		$pdf->SetFont('Arial','',11);
		/*for ($i=0; $i < count($this->descripcionCurso); $i++) { 
			$pdf->SetX(85);
			$pdf->Write(5,'- '.$this->descripcionCurso[i]);
			$pdf->Ln();
		}*/
		$pdf->SetX(75);
		$pdf->MultiCell(130,5,utf8_decode($this->descripcionCurso));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->SetFont('Arial','',12);
		$pdf->Write(5,utf8_decode('VERSIÓN'));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode($this->version));
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode('MODALIDAD'));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode($this->modalidad));
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode('NIVEL'));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode($this->nivel));
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode('N° HORAS'));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,$this->horas);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode('VALOR PARTICIPANTE'));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,"$".$this->valorParticipante);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode('NÚMERO DE ALUMNOS'));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,$this->numeroAlumnos);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,utf8_decode('OBSERVACIONES'));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->MultiCell(130,5,utf8_decode($this->observaciones));
		$pdf->Ln();
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->SetTextColor(0,147,180);
		$pdf->Write(5,utf8_decode('VALOR TOTAL'));
		$pdf->Ln();
		$pdf->SetX(75);
		$pdf->Write(5,"$".$this->valorTotal);
		$pdf->Output();
	}
}
?>