<?php

/**
 * Libreriaemail
 * Description...
 * @version 0.0.1
 * Ultima edicion:	2016-07-07 [Luis Jarpa]                <ljarpa@edutecno.com>
 * Fecha creacion:	2016-06-16 [Luis Jarpa; Felipe Bulboa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Libreriaemail {

    
        function Mailink($asunto, $body, $AltBody, $destino, $con_copia = array(), $con_copia_oculta = array()) {


            /*
            $this->load->library('Libreriaemail');

            $asunto = "Se ha rechazado su solicitud de venta ID Ficha: " . $respuesta[0]['id_ficha']; // asunto del correo
            // presenciales
            $body = $this->generaHtmlRechazo($respuesta); // mensaje completo HTML o text
            // con copia a
            $cc = array(
            array('email' => 'chinojosa@edutecno.com', 'nombre' => 'César Hinojosa'),
            array('email' => 'cmera@edutecno.com', 'nombre' => 'Carlos Mera'),
            array('email' => 'dconcha@edutecno.com', 'nombre' => 'Deiger Concha'),
            array('email' => 'jose.gonzalez@edutecno.com', 'nombre' => 'José Gonzalez')
            );

            $AltBody = "Se ha rechazado su solicitud de venta ID Ficha: " . $respuesta[0]['id_ficha']; // hover del cursor sobre el correo
            // destinos
            $para = array(
            //                array('email' => $usuario_ingreso_correo, 'nombre' => $usuario_ingreso_nombre)
            );


            // con copia oculta a
            $cco = array(

            // array('email' => 'fcampos@edutecno.com', 'nombre' => 'Francisco Campos')
           


            );

            $res = $this->libreriaemail->Mailink($asunto, $body, $AltBody, $para, $cc, $cco);

            if ($res["resultado"] == 0) {
            echo $res["resultado"] . " " . $res["message"];
            }
            */


            $mail = new PHPMailer();
            $mail->IsSMTP(); //     $mail = new PHPMailer();establecemos que utilizaremos SMTP
            $mail->SMTPAuth = true; // habilitamos la autenticación SMTP
            $mail->SMTPSecure = "ssl";  // establecemos el prefijo del protocolo seguro de comunicación con el servidor
            $mail->Host = "smtp.gmail.com"; //"smtp.gmail.com";      // establecemos GMail como nuestro servidor SMTP
            $mail->Port = 465;                   // establecemos el puerto SMTP en el servidor de GMail
            $mail->Username = "sistemas.edutecno@gmail.com";  // la cuenta de correo GMail
            $mail->Password = "sist=3=Google";            // password de la cuenta GMail
            $mail->SetFrom('sistemas.edutecno@gmail.com', 'ODÍN');  //Quien envía el correo
            $mail->AddReplyTo("sistemas.edutecno@gmail.com", "ODÍN");  //A quien debe ir dirigida la respuesta
            $mail->CharSet = "UTF-8";
            $mail->Subject = $asunto;  //Asunto del mensaje
            $mail->Body = $body;
            $mail->AltBody = $AltBody;
            //   $mail->AddAddress($destino[0]['email'], $destino[0]['nombre']);
            for ($i = 0; $i < count($destino); $i++) {
                // echo $destino[$i]['email'] . " destino " . $destino[$i]['nombre'] . "<br>";
                $mail->AddAddress($destino[$i]['email'], $destino[$i]['nombre']);
            }

            for ($i = 0; $i < count($con_copia); $i++) {
                //    echo $con_copia[$i]['email'] . " con_copia " . $con_copia[$i]['nombre'] . "<br>";
                $mail->AddCC($con_copia[$i]['email'], $con_copia[$i]['nombre']);
            }

            for ($i = 0; $i < count($con_copia_oculta); $i++) {
                //   echo $con_copia_oculta[$i]['email'] . " con_copia_ocult " . $con_copia_oculta[$i]['nombre'] . "<br>";
                $mail->AddBCC($con_copia_oculta[$i]['email'], $con_copia_oculta[$i]['nombre']);
            }



            //  $mail->AddCC("cespinoza@edutecno.com", "Cristian Espinoza");
            /* $mail->AddCC("luis.a.jarpa@outlook.cl", "Luis");
            $mail->AddCC("fbulboa@edutecno.com", "Luis");
            */

            //   $mail->AddAttachment(base_url() . "fichadoc/PlantillaInternaAlumnos2_17032017151052.xlsx");      // añadimos archivos adjuntos si es necesario
            //      echo $mail->ErrorInfo;

            $data["resultado"] = false;
            if (!$mail->Send()) {
                $data["message"] = "Error en el envío: " . $mail->ErrorInfo;
                $mail->ClearAddresses();
                $mail->ClearAttachments();
            } else {
                $data["resultado"] = true;
                $data["message"] = "¡Mensaje enviado correctamente!";
                $mail->ClearAddresses();
                $mail->ClearAttachments();
            }


            return $data;
        }

        function CallAPISendMail($data = false) {

            if($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "homologacion.odin.edutecno.com"){
               // echo "Mail falso enviado";
            }else{ //b77e2bd03e84e84c92b873d391d8ea20
                $method = 'POST';
                $url = 'http://api.edutecno.com/Api/SendEmail/';
                $curl = curl_init();

                switch ($method) {
                    case 'POST':
                        curl_setopt($curl, CURLOPT_POST, 1);
                        
                        if ($data)
                            curl_setopt($curl, CURLOPT_POSTFIELDS, array('data' => serialize($data)));
                        break;
                    case 'PUT':
                        curl_setopt($curl, CURLOPT_PUT, 1);
                        break;
                    default:
                        if ($data)
                            $url = sprintf("%s?%s", $url, http_build_query($data));
                }
                // Optional Authentication:
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($curl, CURLOPT_USERPWD, "username:password");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

                return curl_exec($curl); 
            }
        }

    function CallAPISendMailContact($data = false) {
            if($_SERVER['HTTP_HOST'] == "localhost" || $_SERVER['HTTP_HOST'] == "homologacion.odin.edutecno.com"){
               // echo "Mail falso enviado";
            }else{ //b77e2bd03e84e84c92b873d391d8ea20
                $method = 'POST';
                $url = 'http://api.edutecno.com/Api/SendEmailContact/';
                $curl = curl_init();

                switch ($method) {
                    case 'POST':
                        curl_setopt($curl, CURLOPT_POST, 1);
                        
                        if ($data)
                            curl_setopt($curl, CURLOPT_POSTFIELDS, array('data' => serialize($data)));
                        break;
                    case 'PUT':
                        curl_setopt($curl, CURLOPT_PUT, 1);
                        break;
                    default:
                        if ($data)
                            $url = sprintf("%s?%s", $url, http_build_query($data));
                }
                // Optional Authentication:
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($curl, CURLOPT_USERPWD, "username:password");
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

                return curl_exec($curl); 
            }
        }

    //        /*         * *** inicio array ** */
    //        /*   $data = array(
    //          // indica a quienes esta dirigido el  email
    //          "destinatarios" => array(
    //          // si se desea añadir mas de un destinatario se debe añadir otro array con los datos pertinetes : email y nombre
    //          array(
    //          "email" => "xxxxxx@xxxxxx.xxx",
    //          "nombre" => "xxxxxxx xxxxxx"
    //          ), array(
    //          "email" => "xxxxxx@xxxxxx.xxx",
    //          "nombre" => "xxxxxxx xxxxxx"
    //          )
    //          ),
    //          // para enviar el correo con copia a alguien es el mismo metodo que el de los destinatarios
    //          // con la diferencia que : cuando se envia el email en estos no se ven reflejados como destinatarios
    //          "cc_a" => array(
    //          array(
    //          "email" => "xxxxxx@xxxxxx.xxx",
    //          "nombre" => "xxxxx xxxxx"
    //          )
    //          ),// asunto del correo
    //          "asunto" => 'Aqui va el asunto del email',
    //          // contenido del correo , puede ser html o solo texto
    //          "body" => 'Aquí va el cuerpo del correo',
    //          // AltBody (esto no sirve para para pero lo requiere la libreria XD)
    //          "AltBody" => ""
    //          ); */
    //        /*         * *** Fin array ** */
    //
    //
    //        $result = true;
    //        //descomentar codigo en produccion  
    //
    //
    //        /* $data = json_encode($data_string);
    //
    //          $result = file_get_contents('http://api.edutecno.com/Api/SendEmail/', null, stream_context_create(array(
    //          'http' => array(
    //          'method' => 'POST',
    //          'header' => 'Content-Type: application/json' . "\r\n"
    //          . 'Content-Length: ' . strlen($data) . "\r\n",
    //          'content' => $data,
    //          ),
    //          ))); */
    //        return $result;
     //   }

        function CallAPIMAIL($data = false) {

            $method = 'POST';
            $url = 'http://api.edutecno.com/Api/SendEmail/';
            $curl = curl_init();

            switch ($method) {
                case 'POST':
                    curl_setopt($curl, CURLOPT_POST, 1);

                    if ($data)
                        curl_setopt($curl, CURLOPT_POSTFIELDS, array('data' => serialize($data)));
                    break;
                case 'PUT':
                    curl_setopt($curl, CURLOPT_PUT, 1);
                    break;
                default:
                    if ($data)
                        $url = sprintf("%s?%s", $url, http_build_query($data));
            }
            // Optional Authentication:
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, "username:password");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            return curl_exec($curl);
        }
}

/* End of file SendMail.php */
/* Location: ./application/libraries/SendMail.php */
