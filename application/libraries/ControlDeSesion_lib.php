<?php

/**
 * ControlDeSesion_lib
 * Description...
 * @version 0.0.1
 * Ultima edicion:	2016-08-04 [Carlos Aravena] <caravena@edutecno.com>
 * Fecha creacion:	2016-06-03 [Cristián Aguirre] <caravena@edutecno.cl>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ControlDeSesion_lib {

    //2016-06-03
    //Metodo de validación de la sesión is_logged_in
    function is_logged_in($is_logged_in) {
        if (!isset($is_logged_in) || $is_logged_in != TRUE) {
            //Si no esta reenviar al Login_c
            redirect('Login', 'refresh');
        }
    }

    // 2016-08-04
    // Metodo de validación de la session is_logged_in, en backoffice
    function is_logged_in_back_office($is_logged_in) {
        if (!isset($is_logged_in) || $is_logged_in != TRUE) {
            //Si no esta reenviar al Login_c
            redirect(base_url().'Login', 'refresh');
        }
    }

}

/* End of file ControlDeSesion_lib.php */
/* Location: ./application/libraries/ControlDeSesion_lib.php */
