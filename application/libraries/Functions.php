<?php

/**
 * ControlDeSesion_lib
 * Description...
 * @version 0.0.1
 * Ultima edicion:	2016-08-04 [Carlos Aravena] <caravena@edutecno.com>
 * Fecha creacion:	2016-06-03 [Cristián Aguirre] <caravena@edutecno.cl>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Functions {

    //2016-06-03
    //Metodo de validación de la sesión is_logged_in
    function formatDateBD($fecha) {
        $fecha_normalizada = $fecha;
        
        if ($fecha != "") {
            $array_fecha = (explode("-", $fecha));
            $fecha_normalizada = $array_fecha[2] . '-' . $array_fecha[1] . '-' . $array_fecha[0];
        }
        return $fecha_normalizada;
    }

}

/* End of file ControlDeSesion_lib.php */
/* Location: ./application/libraries/ControlDeSesion_lib.php */
