<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once dirname(__FILE__) . '/ezPDF/class.ezpdf.php';
 
class EzPDF extends EZPDF
{
    function __construct()
    {
        parent::__construct();
    }
}
/* application/libraries/Pdf.php */