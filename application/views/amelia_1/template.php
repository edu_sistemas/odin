<!DOCTYPE html>
<html>
    <head>
        <!-- main header -->
        <?php $this->load->view('amelia_1/head') ?>
        <?php $this->load->view('amelia_1/adic_style'); ?>
        <!-- main header -->
    </head>
    <body class="pace-done skin-3">

        <div id="wrapper">
            <!-- main sidebar -->
            <?php $this->load->view('amelia_1/sidebar'); ?>
            <!-- main sidebar -->
            <div id="page-wrapper" class="gray-bg">
                <!-- main navBar -->
                    <!--Carga el modelo que utiliza el controlador-->
                    <?php $this->load->model('../models/back_office/Crm/Notificaciones_model');?>                   

                <?php $this->load->view('amelia_1/navbar'); ?>                
                <!-- main navBar -->
                <!-- main page header -->
                <?php $this->load->view('amelia_1/page_header'); ?>
                <!-- main page header -->
                <!-- main content -->
                <?php $this->load->view($main_content); ?>
               
                    <div class="row">
                <!--
                -->
                    </div>
                
                <!-- main content -->
                <!-- main footer -->
                <?php $this->load->view('amelia_1/footer'); ?>
                <!-- main footer -->
            </div>
        </div>
        <!-- script footer -->
        <?php $this->load->view('amelia_1/footer_script'); ?>
        <?php $this->load->view('amelia_1/adic_script'); ?>
        
        <?php $this->load->view('amelia_1/analytics'); ?>
        <!-- script footer -->
        <!-- Page-Level Scripts --> 
    </body>

</html>
<?php
die();
?>
