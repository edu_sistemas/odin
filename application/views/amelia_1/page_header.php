<!-- Page header -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">

        <?php if (count($arr_page_breadcrumb) > 0) : ?>
            <h1></h1>
            <ol class="breadcrumb">
                <?php
                foreach ($arr_page_breadcrumb as $key => $val) {

                    if (count($arr_page_breadcrumb) == 1) {
                        echo '<li>' . $val['label'] . '</li>';
                    } else {
                        if (count($arr_page_breadcrumb) == $key + 1) {
                            echo '<li>' . $val['label'] . '</li>';
                        } else {
                            echo '<li><a href="' . $val['href'] . '">' . $val['label'] . '</a><i class="' . $val['icono'] . '"></i></li>';
                        }
                    }
                }
                ?>
            </ol>
            <?php endif; ?>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<!-- Page header -->
