<?php
$inicio = array();
$inicio['modulos'] = array();
$inicio['modulos_items'] = array();
$inicio['fin'] = array();

for ($i = 0; $i < count($menus); $i++) {
    for ($j = 0; $j < count($menus[$i]); $j++) {
        if ($i == 0) {
            array_push($inicio['modulos'], array(
                'id' => $menus[$i][$j]['id'],
                'nombre' => $menus[$i][$j]['nombre'],
                'titulo' => $menus[$i][$j]['titulo'],
                'descripcion' => $menus[$i][$j]['descripcion'],
                'icon' => $menus[$i][$j]['icon'],
                'active' => ''
            ));
        }
        if ($i == 1) {
            array_push($inicio['modulos_items'], array(
                'id_menu_item' => $menus[$i][$j]['id_menu_item'],
                'nombre' => $menus[$i][$j]['nombre'],
                'titulo' => $menus[$i][$j]['titulo'],
                'url' => $menus[$i][$j]['url'],
                'icon' => $menus[$i][$j]['icon'],
                'fk_id_menu_item_padre' => $menus[$i][$j]['fk_id_menu_item_padre'],
                'fk_id_menu_item_estado' => $menus[$i][$j]['fk_id_menu_item_estado'],
                'visible' => $menus[$i][$j]['visible'],
                'fk_id_modulo' => $menus[$i][$j]['fk_id_modulo'],
                'active' => ''
            ));
        }
        if ($i == 2) {
            if ($menus[$i][$j]['titulo'] == $activo) {
                $valueAct = "in";
            } else {

                $valueAct = "";
            }
            array_push($inicio['modulos_items'], array(
                'id_menu_item' => $menus[$i][$j]['id_menu_item'],
                'nombre' => $menus[$i][$j]['nombre'],
                'titulo' => $menus[$i][$j]['titulo'],
                'url' => $menus[$i][$j]['url'],
                'icon' => $menus[$i][$j]['icon'],
                'fk_id_menu_item_padre' => $menus[$i][$j]['fk_id_menu_item_padre'],
                'fk_id_menu_item_estado' => $menus[$i][$j]['fk_id_menu_item_estado'],
                'visible' => $menus[$i][$j]['visible'],
                'fk_id_modulo' => $menus[$i][$j]['fk_id_modulo'],
                'active' => $valueAct
            ));
        }
    }
}

for ($i = 0; $i < count($inicio['modulos_items']); $i++) {
    if ($inicio['modulos_items'][$i]['active'] == "in") {
        for ($j = 0; $j < count($inicio['modulos_items']); $j++) {
            if ($inicio['modulos_items'][$i]['fk_id_menu_item_padre'] == $inicio['modulos_items'][$j]['id_menu_item']) {
                $inicio['modulos_items'][$j]['active'] = "in";
                for ($k = 0; $k < count($inicio['modulos']); $k++) {
                    if ($inicio['modulos_items'][$j]['fk_id_modulo'] == $inicio['modulos'][$k]['id']) {
                        $inicio['modulos'][$k]['active'] = "in";
                    }
                }
            }
        }
    }
}
//var_dump($inicio['modulos']);
//var_dump($inicio['modulos_items']);
?>
<!-- Main sidebar -->
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                        <img id="img-sidebar" class="img-circle message-avatar" src="<?php echo $this->session->userdata('img_user'); ?>"/>
                        <span class="clear"> <span class="block m-t-xs">

                            </span> <span class="text-muted text-xs block"><?php echo $this->session->userdata('nombre_user'); ?> <b class="caret"></b></span> </span> </a>

                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="<?php echo site_url() . 'back_office/Usuario/Muro'; ?>">Mi Muro</a></li>
                        <li><a href="<?php echo site_url() . 'Login/logout'; ?>">Salir</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    <img  class="img-responsive img-center" style="margin: 0 auto; max-width: 80%" src="<?php echo base_url(); ?>assets/img/VARIANTES-02.png" >
                </div>
            </li>
            <li    <?php
            if ($activo == "Home") {
                echo "class='active'";
            }
            ?> >
                <a href="<?php echo base_url() . 'back_office/Home'; ?>">
                    <i class="fa fa-th-large"></i>

                    <span class="nav-label">Home</span> 

                </a>

            </li>

            <?php if ($id_perfil == 2 || $id_perfil == 1) { ?>

                <?php for ($i = 0; $i < count($inicio['modulos']); $i++) { ?>

                    <li <?php
                    if ($inicio['modulos'][$i]['active'] == "in") {
                        echo "class='active'";
                    }
                    ?>>
                        <a href="#" >
                            <i class="<?php echo $inicio['modulos'][$i]['icon']; ?>"></i>
                            <span class="nav-label"><?php echo $inicio['modulos'][$i]['titulo']; ?></span>
                            <span class="fa arrow"></span>

                        </a>
                        <ul class="nav nav-second-level collapse">
                            <?php for ($j = 0; $j < count($inicio['modulos_items']); $j++) { ?>
                                <?php if ($inicio['modulos_items'][$j]['fk_id_menu_item_padre'] == 0 && $inicio['modulos_items'][$j]['fk_id_modulo'] == $inicio['modulos'][$i]['id']) { ?>
                                    <li <?php
                                    if ($inicio['modulos_items'][$j]['active'] == "in") {
                                        echo "class='active'";
                                    }
                                    ?>>                                    
                                        <a href="#">
                                            <i class="<?php echo $inicio['modulos_items'][$j]['icon']; ?>"></i>
                                            <?php echo $inicio['modulos_items'][$j]['titulo']; ?>
                                            <span class="fa arrow"></span>
                                        </a>
                                        <ul class="nav nav-third-level collapse">
                                            <?php for ($k = 0; $k < count($inicio['modulos_items']); $k++) { ?>
                                                <?php if ($inicio['modulos_items'][$k]['fk_id_menu_item_padre'] == $inicio['modulos_items'][$j]['id_menu_item']) { ?>
                                                    <li <?php
                                                    if ($inicio['modulos_items'][$k]['active'] == "in") {
                                                        echo "class='active'";
                                                    }
                                                    ?>>
                                                        <a href="<?php echo base_url() . $inicio['modulos_items'][$k]['url']; ?>">
                                                            <i class="<?php echo $inicio['modulos_items'][$k]['icon']; ?>"></i>
                                                            <?php echo $inicio['modulos_items'][$k]['titulo']; ?>

                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>                                       
                                    </li>

                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
            <?php } else { ?>
                <?php for ($i = 0; $i < count($inicio['modulos']); $i++) { ?>

                    <?php for ($j = 0; $j < count($inicio['modulos_items']); $j++) { ?>
                        <?php if ($inicio['modulos_items'][$j]['fk_id_menu_item_padre'] == 0 && $inicio['modulos_items'][$j]['fk_id_modulo'] == $inicio['modulos'][$i]['id']) { ?>
                            <li <?php
                            if ($inicio['modulos_items'][$j]['active'] == "in") {
                                echo "class='active'";
                            }
                            ?>>


                                <?php
                                $mibanNuevo = false;
                                for ($k = 0; $k < count($inicio['modulos_items']); $k++) {
                                    if ($inicio['modulos_items'][$k]['fk_id_menu_item_padre'] == $inicio['modulos_items'][$j]['id_menu_item']) {
                                        $mibanNuevo = true;
                                    }
                                }
                                ?>
                                <?php if ($mibanNuevo) { ?>
                                    <a href="#">
                                        <i class="<?php echo $inicio['modulos_items'][$j]['icon']; ?>"></i>
                                        <?php echo $inicio['modulos_items'][$j]['titulo']; ?>
                                        <span class="fa arrow"></span>
                                    </a>
                                    <ul class="nav nav-second-level collapse">
                                        <?php for ($k = 0; $k < count($inicio['modulos_items']); $k++) { ?>
                                            <?php if ($inicio['modulos_items'][$k]['fk_id_menu_item_padre'] == $inicio['modulos_items'][$j]['id_menu_item']) { ?>
                                                <li <?php
                                                if ($inicio['modulos_items'][$k]['active'] == "in") {
                                                    echo "class='active'";
                                                }
                                                ?>>
                                                    <a href="<?php echo base_url() . $inicio['modulos_items'][$k]['url']; ?>">
                                                        <i class="<?php echo $inicio['modulos_items'][$k]['icon']; ?>"></i>
                                                        <?php echo $inicio['modulos_items'][$k]['titulo']; ?>

                                                    </a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    <?php } ?>

                <?php } ?>
            <?php } ?>

        </ul>
    </div>
</nav>
<!-- /main sidebar -->
