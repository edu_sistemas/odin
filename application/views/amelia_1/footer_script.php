<?php
$selectSolicitudAlumno=base_url('back_office/Contact_center/Comun/selectSolicitudAlumno');
$UrlGestion=base_url('back_office/Contact_center/Comun/Gestion_alumnos/');
?>

<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/amelia/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/amelia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>assets/amelia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/amelia/js/plugins/toastr/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/amelia/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/amelia/js/plugins/pace/pace.min.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/amelia/js/sistema/Crm/navbar/navbar.js"></script>-->

<script type="text/javascript">
	
	$(document).ready(function () {
		var url=document.URL.toUpperCase();
		var n = url.indexOf('CONTACT_CENTER');
		if(n>0)
		{
			setInterval(function(){
				selectSolicitudAlumno()
			}, 30000);
		}
	});

	function selectSolicitudAlumno(){

		toastr.options = {
			"closeButton": false,
			"debug": false,
			"newestOnTop": false,
			"progressBar": true,
			"positionClass": "toast-bottom-right",
			"preventDuplicates": false,
			"onclick": false,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "300000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}

		$.ajax({
			async: true,
			cache: false,
			dataType: "json",
			type: 'POST',
		//data: {filtro : filtro},
		error: function (jqXHR, textStatus, errorThrown) {
		        // code en caso de error de la ejecucion del ajax
		        console.log(jqXHR);
		    },
		    success: function (result) {
		    	
		    	if(result.length>0)
		    	{

		    		$.each(result, function(ndx, item){
		    			var gestion="<?php echo $UrlGestion;?>/"+item.IdAlumno;

		    			toastr.error("<a href='"+gestion+"' title='Haga click aqui para gestionar la Solicitud'> El alumno "+item.nombre+" solicita ser llamado el dia: "+item.fecha+" a las "+item.hora+".<br/> Comentario del alumno: "+item.comentario+"</a>", "Solicitud Alumno");
		    		});

		    	}
		    },
		    url: "<?php echo $selectSolicitudAlumno;?>"
		}); 
	}

// NOTIFICAIONES
	function SetNotificacion(id,indicador,link,vindex,validadorlink){
/*
	    if (vindex > 0)
	    {   
	        vurl = '../../Notificaciones/getSetnotificacion'
	        if (validadorlink == 0) { //ESTA CONDICION SIRVE PARA ENVIO MAIL/PARAMETRO
	            vurl = '../../../Crm/Notificaciones/getSetnotificacion'
	        }
	    }
	    else if(vindex == 0) //ESTA CONDICION SIRVE PARA CONTACT CENTER
	    {
	        vurl = '../../Crm/Notificaciones/getSetnotificacion'
	        if (validadorlink > 0) {
	            vurl = '../../../Crm/Notificaciones/getSetnotificacion'
	        }
	    }
	    else if(vindex < 0)
	    {
	        vurl = '../../back_office/Crm/Notificaciones/getSetnotificacion'
	    }
	    else
	    {
	        vurl = '../../back_office/Crm/Notificaciones/getSetnotificacion'
	    }
	    */

vurl = "<?php echo base_url('back_office/Crm/Notificaciones/getSetnotificacion'); ?>";

	    $.ajax({
	        async: false,
	        cache: false, 
	        //dataType: "json",
	        type: 'POST',
	        data: { id_notificacion: id },
	        error: function (jqXHR, textStatus, errorThrown) {
	            // code en caso de error de la ejecucion del ajax
	            //console.log(textStatus + errorThrown);
	            //alerta('Nueva Cuenta', jqXHR.responseText, 'error');
	        },
	        success: function (result) {
	            // if (result[0].reset == 1) {
	              //  alert(result);
	              //  alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);		  
					// $('#div_general').load(document.URL + ' #div_general');
						 // var tr = $("#tr_"+id); 
						 // tr.remove();
	                     location.href = link;

	                     
	            // }
	        },
	        url: vurl
	                     
			//http://localhost:8080/odin/application/controllers/back_office/Crm/Notificaciones/getSetnotificacion
			//C:\xampp\htdocs\odin\application\controllers\back_office\Crm\Notificaciones.php
			//C:\xampp\htdocs\odin\assets\amelia\js\sistema\Crm\navbar\navbar.js
	    });	
		
		var cont = parseInt(document.getElementById("q_notificaciones").innerText);
	//    $("#q_notificaciones").text(cont-1);

	    var count_hidden = parseInt(document.getElementById("Var_q_not").innerText);

	    if (count_hidden <= 11) {
	        if (count_hidden != 0) {
	        $("#q_notificaciones").text(count_hidden-1);
	        }
	    }
	      $("#Var_q_not").text(count_hidden-1);

		var li = $("#notdinamic_"+id); 
	    var divi = $("#dividerdinamic_"+id); 
		li.remove(); 
		divi.remove(); 
	//load
	if (indicador != 1)
	{
	    $('#div_load').load(document.URL + ' #div_load');
	}
	}		

	//---Fin marcar notificaciones una por una.---//

	//---Marcar 10  últimas como leídas.---//

	function SetNotificacionTodas2(id){
//alert(id);
	        $.ajax({
	        async: false,
	        cache: false,
	        dataType:"json",
	        type: 'POST',
	        data: { id_notificacion: id },
	        //  data: $('#frm').serialize(),
	        error: function (jqXHR, textStatus, errorThrown) {
	            // code en caso de error de la ejecucion del ajax
	            //  console.log(textStatus + errorThrown);
	        },  
	        success: function (result) {

	            // alert(result.length);
	            //for(var i=0; i<result.length; i++)
	            //{
	            //     alert(result[i].id_notificacion);
	            //     SetNotificacion(result[i].id_notificacion,1);
	            },          
	            
	        
	         url:"<?php echo base_url('back_office/Crm/Notificaciones/getSetnotificacion_Todos'); ?>"
	});
	        $('#nav_load').load(document.URL + ' #nav_load');
	        $('#div_load').load(document.URL + ' #div_load');
	        $("#q_notificaciones").text("0");

	    }

	//---Fin marcar 10  últimas como leídas.---//
</script>

