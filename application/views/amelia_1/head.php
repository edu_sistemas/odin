
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/amelia/images/favicon.ico">
<title><?php echo $page_title ?></title>

<script src="<?php echo base_url(); ?>assets/amelia/js/jquery-2.1.1.js"></script>
<link href="<?php echo base_url(); ?>assets/amelia/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/amelia/font-awesome/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/amelia/css/animate.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/amelia/css/style.css" rel="stylesheet">

<link href="<?php echo base_url(); ?>assets/amelia/css/plugins/toastr/toastr.min.css" rel="stylesheet"> 

<link href="<?php echo base_url(); ?>assets/amelia/css/jquery-ui.min.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/amelia/js/jquery-ui.min.js"></script>