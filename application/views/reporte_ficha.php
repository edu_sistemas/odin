<?php

function obtenerConexion() {
    $mysql_host = "200.111.64.171";
    $mysql_database = "bd_gestor_capacitacion";
    $mysql_user = "conexion";
    $mysql_password = "edu1234";
    $link = mysqli_connect($mysql_host, $mysql_user, $mysql_password);
    mysqli_select_db($link, $mysql_database);
    return $link;
}

ini_set('max_execution_time', 0);


$id_ficha = $_GET['ficha'];


$Consulta = "CALL sp_get_full_report_by_ficha('" . $id_ficha . "');";


$link = obtenerConexion();

$resultado = mysqli_query($link, $Consulta) or die(mysqli_error($link));
mysqli_close($link);

require_once 'Excel/reader.php';

$data = new Spreadsheet_Excel_Reader();
$data->setOutputEncoding('CP1251');

//Establecemos las cabeceras para un archivo xls
header('Content-type: application/vnd.ms-excel');
header("Content-Disposition: attachment; filename=reporte_cursos_finzalizados_sence.xls");
header("Pragma: no-cache");
header("Expires: 0");
echo("<table>");
echo("<tr>");
echo("<td>Ficha</td>");
echo("<td>Fecha Inicio</td>");
echo("<td>Fecha Fin</td>");
echo("<td>Aplicación</td>");
echo("<td>Nivel</td>");
echo("<td>Rut</td>");
echo("<td>Nombre</td>");
echo("<td>Apellido</td>");
echo("<td>Telefono 1</td>");
echo("<td>Correo 1</td>");
echo("<td>Status</td>");
echo("<td>Empresa</td>");
echo("<td>Participacion</td>");
echo("<td>Tiempo de conexión (horas)</td>");
echo("<td>% de Conectividad</td>");
echo("<td>DJ Aceptado</td>");
echo("<td>Fecha Ultima Actualizacion DJ</td>");

echo("</tr>");

while ($row = mysqli_fetch_array($resultado)) {
    echo("<tr>");
    echo("<td>" . $row['ficha'] . "</td>");
    echo("<td>" . $row['fecha_inicio'] . "</td>");
    echo("<td>" . $row['fecha_fin'] . "</td>");
    echo("<td>" . $row['aplicacion'] . "</td>");
    echo("<td>" . $row['nivel'] . "</td>");
    echo("<td>" . $row['rut'] . "</td>");
    echo("<td>" . $row['nombre'] . "</td>");
    echo("<td>" . $row['apellido'] . "</td>");
    echo("<td>" . $row['telefono_1'] . "</td>");
    echo("<td>" . $row['correo_1'] . "</td>");
    echo("<td>" . $row['status'] . "</td>");
    echo("<td>" . $row['cliente'] . "</td>");
    echo("<td>" . $row['cp_participacion'] . "</td>");
    echo("<td>" . $row['tiempo_de_conexion_horas'] . "</td>");
    echo("<td>" . $row['porcentaje_conectividad'] . "</td>");
    echo("<td>" . $row['dj_aceptado'] . "</td>");
    echo("<td>" . $row['fecha_ultima_actualizacion_dj'] . "</td>");
    echo("</tr>");
}

echo ("</table>");
?>
