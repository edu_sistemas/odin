<!-- Mainly scripts -->
<script src="<?php echo base_url(); ?>assets/amelia/js/jquery-2.1.1.js"></script>
<script src="<?php echo base_url(); ?>assets/amelia/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/amelia/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>assets/amelia/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url(); ?>assets/amelia/js/inspinia.js"></script>
<script src="<?php echo base_url(); ?>assets/amelia/js/plugins/pace/pace.min.js"></script>

