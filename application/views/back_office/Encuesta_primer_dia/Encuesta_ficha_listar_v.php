<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 */
?>

    <head>
        <style>
            /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */

            #map {
                height: 100%;
            }
            /* Optional: Makes the sample page fill the window. */

            html,
            body {
                height: 100%;
                margin: 0;
                padding: 0;
            }

            input:disabled {
                opacity: 1;
                background-color: #fff;
            }

            .ibox-title {
                color: #fff;
                background-color: #2e74b5;
                text-align: center !important;
                font-weight: bold;
                font-size: 120%;
            }

            .table_title {
                color: #fff;
                background-color: #2e74b5;
            }
            .table_sub_title{
                background-color: #e7e6e6;
                color:#000;
                font-weight: bold;
            }
            table, tr, td{
                border: 1px solid #797979;
            }
            table{
                border-collapse: collapse;
            }
            td{
                width:0.1%;
                white-space: nowrap;
                color: #000;
            }
            textarea{
                width: 100%;
                height: 100px;
                resize: none;
                font-size: 90%;
            }
            .like_button{
                width: 40px;
                height: auto;
                cursor: pointer;
            }
            .input_hidden {
                position: absolute;
                left: -9999px;
            }
            .selected {
                background-color: #ffa54a;
                border-radius: 12px;
            }
        </style>
    </head>
    <input type="hidden" id="id_ficha" value="<?php echo $id_ficha ?>" />
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <input type="hidden" id="num_ficha_hidden"> ENCUESTA PRIMER DÍA
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <!-- INICIO TABLA DATOS DEL CURSO -->
                        <table id="tbl_datos_ficha" class="table table-striped">
                            <thead>
                                <tr>
                                    <th colspan="6" class="table_title">DATOS DEL CURSO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td class="table_sub_title">Nombre del Curso (horas):</td><td colspan="5" id="nombre_curso"></td></tr>
                                <tr><td class="table_sub_title">N° Ficha</td><td id="num_ficha"></td><td class="table_sub_title">Empresa:</td><td id="razon_social"></td><td class="table_sub_title">Nombre del Relator:</td><td id="relator"></td></tr>
                                <tr><td class="table_sub_title">Fecha de ejecución:</td><td id="fecha_inicio"></td><td class="table_sub_title">Días de clases:</td><td id="dias_de_clases"></td><td class="table_sub_title">Horario:</td><td id="horario"></td></tr>
                            </tbody>
                        </table>

                        <table id="tbl_datos_alumnos" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th colspan="9" class="table_title">LISTADO DE PARTICIPANTES</th>
                                    </tr>
                                    <tr><td class="table_sub_title" style="width: 0.01%;">N°</td>
                                    <td class="table_sub_title">Nombre</td>
                                    <td class="table_sub_title">Teléfono</td>
                                    <td class="table_sub_title">Encuestador</td>
                                    <td class="table_sub_title">Relator</td>
                                    <td class="table_sub_title">Equipos</td>
                                    <td class="table_sub_title">Curso</td>
                                    <td class="table_sub_title" style="width: 1%;">Comentarios</td>
                                    <td class="table_sub_title" ></td></tr>
                                </thead>
                                <tbody></tbody>
                            </table>

                        <!-- FIN TABLA DATOS DEL CURSO -->
                    </div>
                </div>