<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
    <style>
        /*select2*/

        .select2-container--open {
            z-index: 2060;
        }

        .swal2-container {
            z-index: 2061;
        }
    </style>
    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Grupos Comerciales</h5>
                        <div class="ibox-tools">

                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Administrar</button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a data-toggle="modal" data-target="#modal_agregar_usuario" data-backdrop="static" data-keyboard="false">Agregar Usuario</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target="#modal_agregar_grupo" data-backdrop="static" data-keyboard="false">Agregar Grupo</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="ibox-content">
                        <!-- contenido aca -->
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="control-label">Seleccione un Grupo Comercial</label>
                                <br>
                                <select id="select_grupo_comercial" name="select_grupo_comercial" class="select2_demo_3 form-control" style="width: 100%"
                                    required>
                                    <option></option>
                                </select>
                            </div>
                            <div class="row">
                                <div id="cargando" class="col-lg-12 text-center">

                                </div>
                            </div>
                            <hr>
                            <br>
                            <div class="row">
                                <div class="col-lg-12" id="render-html">
                                </div>
                            </div>
                        </div>
                        <div class="row" style="border-top: 5px solid #676a6c;">
                            <div class="col-lg-4" >
                                <h5 id="nombregrupo1"></h5>
                                <ul id="grupo1">

                                </ul>
                            </div>
                            <div class="col-lg-4" >
                                <h5 id="nombregrupo2"></h5>
                                <ul id="grupo2">

                                </ul>
                            </div>
                            <div class="col-lg-4" >
                                <h5 id="nombregrupo3"></h5>
                                <ul id="grupo3">

                                </ul>
                            </div>
                        </div>
                        <div class="row" style="border-top: 5px solid #676a6c;">
                            <div class="col-lg-4" >
                                <h5 id="nombregrupo4"></h5>
                                <ul id="grupo4">

                                </ul>
                            </div>
                            <div class="col-lg-4" >
                                <h5 id="nombregrupo5"></h5>
                                <ul id="grupo5">

                                </ul>
                            </div>
                            <div class="col-lg-4" >
                                <h5 id="nombregrupo6"></h5>
                                <ul id="grupo6">

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- SECCION MODALES -->

    <!-- Modal Agregar Usuario -->
    <div id="modal_agregar_usuario" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Usuario a un Grupo Comercial</h4>
                </div>
                <div class="modal-body">
                    <form id="frm_nuevo_usuario">
                        <div class="row">
                            <label class="col-lg-2">Usuario</label>
                            <div class="col-lg-10">
                                <select name="usuario" id="usuario" class="select2_demo_3 form-control" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <label class="col-lg-2">Grupo Comercial</label>
                            <div class="col-lg-10">
                                <select name="grupo_comercial" id="grupo_comercial" class="select2_demo_3 form-control" style="width: 100%;">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <br>
                                <button class="btn btn-success pull-right">Agregar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN Modal Agregar Usuario -->

    <!-- Modal Agregar Grupo -->
    <div id="modal_agregar_grupo" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Nuevo Grupo Comercial</h4>
                </div>
                <div class="modal-body">
                    <form id="frm_nuevo_grupo">
                        <div class="row">
                            <label class="col-lg-2">Usuario(s) a cargo del grupo</label>
                            <div class="col-lg-10">
                                <select id="usuario2" name="usuario[]" data-placeholder="elija uno o más usuarios..." class="chosen-select" multiple style="width: 100%; display: block;"
                                    tabindex="-1" required>
                                </select>
                                </select>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-lg-12">
                                <button class="btn btn-success pull-right">Agregar</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- FIN Modal Agregar Grupo -->