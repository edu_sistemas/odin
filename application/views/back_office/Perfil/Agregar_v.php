<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-10-11 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Perfil</h5>
                <div class="ibox-tools">

                </div>
            </div>

            <div class="ibox-content">


                <form class="form-horizontal" id="frm_perfil_registro"  >
                    <p> Agregar Perfil </p>
                    <div class="form-group"><label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-6">

                            <input type="text" placeholder="Nombre" requerido="true" mensaje="Debe ingresar nombre del perfil" name="nombre" class="form-control">

                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Descripción:</label>

                        <div class="col-lg-6">
                            <textarea class="form-control" requerido="true" mensaje="Debe ingresar una descripción sobre el perfil" id="descripcion" name="descripcion" maxlength="100" placeholder="Descripción"></textarea>

                            <p id="text-out">0/200 caracteres restantes</p>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" name="btn_reset" id="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
