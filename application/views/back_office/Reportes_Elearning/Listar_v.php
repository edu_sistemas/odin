<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2018-05-07 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<style>
    #cargando{
        width: 100%;
    }
    #cargando img{
        margin-left: auto;
        margin-right: auto;
        display: block;
    }
    .btn-group .active{
       background-color: #1ab394 !important; 
       color: #fff !important;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Fichas Distancia</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="cargando"></div>
                    <div id="contenido" style="display:none;">
                        <form class="form-horizontal" id="frm-sabana">

                            <div class="form-group">
                                <label class="col-lg-2 control-label">FICHA:</label>
                                <div class="col-lg-7">
                                    <div class="col-md-6">
                                        <select id="id_ficha" name="id_ficha" class="select2_demo_3 form-control"
                                            required>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-2"></div>
                                <div class="col-lg-7">
                                    <h3>Seleccione que información desea agregar al reporte</h3>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">EMPRESA:</label>
                                <div class="col-lg-7">

                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="empresa_holding" name="empresa_holding" value="h.nombre_holding"
                                                autocomplete="off"> Holding
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="empresa_razon_social" name="empresa_razon_social" value="e.razon_social_empresa"
                                                autocomplete="off"> Razón Social
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="empresa_rut" name="empresa_rut" value="e.rut_empresa" autocomplete="off">
                                            RUT
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="empresa_direccion" name="empresa_direccion" value="e.direccion_empresa"
                                                autocomplete="off"> Dirección
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="empresa_linea_de_negocio" name="empresa_linea_de_negocio" value="empresa_linea_de_negocio"
                                                autocomplete="off"> Linea de Negocio
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="empresa_sub_linea_de_negocio" name="empresa_sub_linea_de_negocio" value="empresa_sub_linea_de_negocio"
                                                autocomplete="off"> Sub-línea de Negocio
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="empresa_rubro" name="empresa_rubro" value="empresa_rubro" autocomplete="off">
                                            Rubro
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">CURSO:</label>
                                <div class="col-lg-7">

                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_orden_de_compra" name="curso_orden_de_compra" value="curso_orden_de_compra"
                                                autocomplete="off"> Orden de Compra
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_nombre_curso" name="curso_nombre_curso" value="curso_nombre_curso"
                                                autocomplete="off"> Nombre del Curso (EDUTECNO)
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_nombre_sence" name="curso_nombre_sence" value="curso_nombre_sence"
                                                autocomplete="off"> Nombre del Curso (SENCE)
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_version" name="curso_version"  value="curso_version" autocomplete="off">
                                            Versión
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_horas" name="curso_horas" value="curso_horas" autocomplete="off">
                                            Horas
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_codigo_sence" name="curso_codigo_sence" value="curso_codigo_sence"
                                                autocomplete="off"> Código Sence
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_id_accion" name="curso_id_accion" value="curso_id_accion"
                                                autocomplete="off"> ID Acción
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_modalidad" name="curso_modalidad" value="curso_modalidad"
                                                autocomplete="off"> Modalidad
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_otic" name="curso_otic" value="curso_otic" autocomplete="off">
                                            OTIC
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_fecha_inicio" name="curso_fecha_inicio" value="curso_fecha_inicio"
                                                autocomplete="off"> Fecha de Inicio
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_fecha_termino" name="curso_fecha_termino" value="curso_fecha_termino"
                                                autocomplete="off"> Fecha de Término
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_hora_inicio" name="curso_hora_inicio" value="curso_hora_inicio"
                                                autocomplete="off"> Hora de Inicio
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_hora_termino" name="curso_hora_termino" value="curso_hora_termino"
                                                autocomplete="off"> Hora de Término
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="curso_lugar_de_ejecucion" name="curso_lugar_de_ejecucion" value="curso_lugar_de_ejecucion"
                                                autocomplete="off"> Lugar de Ejecución
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-2 control-label">ALUMNO:</label>
                                <div class="col-lg-7">

                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_rut" name="alumno_rut" value="alumno_rut" autocomplete="off">
                                            RUT
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_nombre" name="alumno_nombre" value="alumno_nombre" autocomplete="off">
                                            Nombre
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_apellidos" name="alumno_apellidos" value="alumno_apellidos"
                                                autocomplete="off"> Apellidos
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_centro_costo" name="alumno_centro_costo" value="alumno_centro_costo"
                                                autocomplete="off"> Centro de Costo
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_porcentaje_franquicia" name="alumno_porcentaje_franquicia" value="alumno_porcentaje_franquicia"
                                                autocomplete="off"> Porcentaje de franquicia
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_primer_acceso" name="alumno_primer_acceso" value="alumno_primer_acceso"
                                                autocomplete="off"> Primer accceso al curso
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_ultimo_acceso" name="alumno_ultimo_acceso" value="alumno_ultimo_acceso"
                                                autocomplete="off"> Último accceso al curso
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_avance_contenidos" name="alumno_avance_contenidos" value="alumno_avance_contenidos"
                                                autocomplete="off"> Avance Contenidos
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_avance_evaluaciones" name="alumno_avance_evaluaciones" value="alumno_avance_evaluaciones"
                                                autocomplete="off"> Avance Evaluaciones
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_tiempo_conexion_sence" name="alumno_tiempo_conexion_sence" value="alumno_tiempo_conexion_sence"
                                                autocomplete="off"> Tiempo de Conexión SENCE
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_porcentaje_conexion_sence" name="alumno_porcentaje_conexion_sence" value="alumno_porcentaje_conexion_sence"
                                                autocomplete="off"> % Conexión SENCE
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_estado_declaracion_jurada" name="alumno_estado_declaracion_jurada" value="alumno_estado_declaracion_jurada"
                                                autocomplete="off"> Estado Declaración Jurada
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_telefono" name="alumno_telefono" value="alumno_telefono"
                                                autocomplete="off"> Telefono
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_email" name="alumno_email" value="alumno_email" autocomplete="off">
                                            Email
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="checkbox" id="alumno_observaciones" name="alumno_observaciones" value="alumno_observaciones"
                                                autocomplete="off"> Observaciones
                                        </label>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button class="btn btn-sm btn-primary">Generar Reporte</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>