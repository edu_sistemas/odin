<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 id="h_text_title_ficha">Resumen</h5>
                <div class="ibox-tools">
                    <a href="../../Resumen/pdfResumen/<?php echo $id_ficha; ?>" target="_blank"><button type="button" id="btn_pdf" class="btn btn-w-m btn-info">Descargar PDF</button></a>
                    <?php
                    if (intval($data_ficha[0]['id_estado']) >= 60) {
                        echo '<button id="btn_anular" class="btn btn-w-m btn-danger">ANULAR FICHA</button>';
                    }
                    ?>

                </div>
            </div>
            <div class="ibox-content">
                <form method="" class="form-horizontal">
                    <input type="hidden" id="id_edutecno_original" name="id_edutecno_original" value="<?php echo $data_ficha[0]['id_edutecno']; ?>">
                    <input type="hidden" id="id_ficha" name="id_ficha" value="<?php echo $data_ficha[0]['id_ficha']; ?>">
                    <input type="hidden" id="num_ficha" name="num_ficha" value="<?php echo $data_ficha[0]['num_ficha']; ?>">
                    <p> Datos de la Venta</p> 
                    <div class="row">

                        <div class="col-md-6">
                            <div class="col-md-12">
                                <label class="control-label"><strong>N° de Ficha:</strong></label>
                                <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['num_ficha']; ?>">

                            </div>
                            <div class="col-md-12">
                                <label class="  control-label">Tipo de Venta:</label>

                                <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['categoria']; ?>">

                            </div>
                            <div class="col-md-12">
                                <label class="  control-label"><strong>Estado:</strong> </label>
                                <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['estado']; ?>">

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Documentos Adjuntos</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link" hidden id="a_tab_tabla_carga"> <i
                                                class="fa fa-chevron-down"></i>
                                        </a>

                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <table id="tbl_orden_compra_documetnos"
                                           class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Tipo Documento</th>
                                                <th>Documento</th>

                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Tipo Documento</th>
                                                <th>Documento</th>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="hr-line-dashed"></div>

                    <p> Datos de la Empresa</p> 

                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Holding</label>

                            <input type="text" class="form-control" readonly="" id="holding"
                                   value="<?php echo $data_ficha[0]['holding']; ?>">

                        </div>
                        <div class="col-md-6">
                            <label class="  control-label">Empresa</label>

                            <input type="text" class="form-control" readonly=""
                                   id="id_empresa"
                                   value="<?php echo $data_ficha[0]['rut_empresa'] . " / " . $data_ficha[0]['empresa']; ?>">

                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-3">
                            <label class="control-label">Dirección Empresa</label>
                            <input type="text" class="form-control" readonly=""
                                   id="direccion_empresa"
                                   value="<?php echo $data_ficha[0]['direccion_empresa']; ?>">

                        </div>

                        <div class="col-lg-3">
                            <label class="control-label">Nombre Contacto</label>

                            <input type="text" class="form-control" readonly=""
                                   id="nombre_contacto_empresa"
                                   value="<?php echo $data_ficha[0]['nombre_contacto']; ?>">
                        </div>

                        <div class="col-lg-3">
                            <label class="control-label">Teléfono Contacto</label>

                            <input type="text" class="form-control" readonly=""
                                   id="telefono_contacto_empresa"
                                   value="<?php echo $data_ficha[0]['telefono_contacto']; ?>">
                        </div>                        

                        <div class="col-lg-3">
                            <label class="control-label">Dirección envío Diplomas</label>

                            <input type="text" class="form-control" readonly=""
                                   id="lugar_entrega_diplomas"
                                   value="<?php echo $data_ficha[0]['direccion_diplomas']; ?>">
                        </div>   
                    </div> 

                    <div class="hr-line-dashed"></div>
                    <p>Datos de la OTIC</p> 

                    <div class="row">


                        <div class="col-lg-4">
                            <label class="  control-label">Otic</label>
                            <input type="text" class="form-control" readonly="" 
                                   value="<?php echo $data_ficha[0]['otic']; ?>">
                        </div>

                        <div class="col-md-4">
                            <label class=" control-label">Código Sence</label>
                            <input type="text" class="form-control" readonly=""

                                   value="<?php echo $data_ficha[0]['codigo_sence']; ?>">
                        </div>
                        <div class="col-md-4">
                            <label class=" control-label">N° Orden Compra</label>
                            <input type="text" class="form-control" readonly=""

                                   value="<?php echo $data_ficha[0]['num_orden_compra']; ?>">
                        </div>


                    </div>
                    <div class="hr-line-dashed"> </div>
                    <p>Datos del Curso</p>  
                    <div class="row"> 
                        <div class="col-md-4">
                            <label class="col-md-1 control-label">Modalidad</label>
                            <input type="text" class="form-control" readonly=""
                                   id="modalidad"
                                   value="<?php echo $data_ficha[0]['modalidad']; ?>">
                        </div>

                        <div class="col-md-4">
                            <label class="col-md-1 control-label">Curso</label>
                            <input type="text" class="form-control" readonly=""  id="curso_descrip" value="<?php echo $data_ficha[0]['descripcion_producto']; ?>">
                        </div>

                        <div class="col-md-4"> 
                            <label class="col-md-1 control-label">Versión</label>
                            <input type="text" class="form-control" readonly=""  id="version"  value="<?php echo $data_ficha[0]['version']; ?>">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class=" control-label">Fecha Inicio y Término</label>

                            <div class="input-daterange input-group  col-lg-12 ">

                                <input type="text" readonly="" class="form-control"
                                       name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"
                                       value="<?php echo $data_ficha[0]['fecha_inicio']; ?>"
                                       requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                                <span class="input-group-addon">A</span> 
                                <input type="text"  class="form-control" name="fecha_termino" id="fecha_termino"
                                       placeholder="AAAA-MM-DD" value="<?php echo $data_ficha[0]['fecha_fin']; ?>" readonly=""
                                       requerido="true" mensaje="Fecha Término es un campo requerido" />
                            </div>
                        </div>
                        <div class="col-lg-6">

                            <div class="col-md-6 ">
                                <label class="  control-label">Hora Inicio</label>
                                <div class="input-group clockpicker">
                                    <input type="time" class="form-control" id="txt_hora_inicio"
                                           name="txt_hora_inicio"
                                           value="<?php echo $data_ficha[0]['hora_inicio']; ?>"
                                           requerido="true" mensaje="Ingresar Hora Inicio" readonly="" />
                                    <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>

                            <div class="col-md-6 ">
                                <label class=" control-label">Hora Término</label>
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input type="time" class="form-control" id="txt_hora_termino"
                                           name="txt_hora_termino"
                                           value="<?php echo $data_ficha[0]['hora_termino']; ?>"
                                           requerido="true" mensaje="Ingresar Hora Término" readonly="">
                                    <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="hr-line-dashed"> </div>
                    <p>Lugar de Ejecución</p>  

                    <div class="row">
                        <div class="col-lg-6"> 
                            <label class=" control-label">Sede</label>
                            <input type="text" class="form-control" readonly="" id="id_sede"
                                   value="<?php echo $data_ficha[0]['sede']; ?>">

                        </div>
                        <div class="col-lg-6">
                            <label class=" control-label">Sala</label>
                            <input type="text" class="form-control" readonly="" id="id_sala"
                                   value="<?php echo $data_ficha[0]['sala']; ?>">
                        </div>

                    </div>
                    <div class="row">


                        <div class="col-lg-6">
                            <label class="control-label">Dirección o Lugar Ejecución</label>
                            <input id="lugar_ejecucion" name="lugar_ejecucion" class="form-control" readonly="" 
                                   value="<?php echo $data_ficha[0]['lugar_ejecucion']; ?>" 
                                   />
                        </div>


                        <input  type="hidden" id="id_dias" value="<?php echo $data_ficha[0]['id_dias']; ?>">
                        <div class="col-lg-6">
                            <label class="control-label">Días</label>
                            <label class="checkbox-inline" for="inlineCheckbox1"> <input
                                    type="checkbox" readonly="" id="lunes" name="lunes" value="1"
                                    title="Lunes"> Lu
                            </label> <label class="checkbox-inline" for="inlineCheckbox2"> <input
                                    type="checkbox" readonly="" id="martes" name="martes" value="2"
                                    title="Martes"> Ma
                            </label> <label class="checkbox-inline" for="inlineCheckbox3"> <input
                                    type="checkbox" readonly="" id="miercoles" name="miercoles"
                                    value="3" title="Miércoles"> Mi
                            </label> <label class="checkbox-inline" for="inlineCheckbox4"> <input
                                    type="checkbox" readonly="" id="jueves" name="jueves" value="4"
                                    title="Jueves"> Ju
                            </label> <label class="checkbox-inline" for="inlineCheckbox5"> <input
                                    type="checkbox" readonly="" id="viernes" name="viernes"
                                    value="5" title="Viernes"> Vi
                            </label> <label class="checkbox-inline" for="inlineCheckbox6"> <input
                                    type="checkbox" readonly="" id="sabado" name="sabado" value="6"
                                    title="Sábado"> Sa
                            </label> <label class="checkbox-inline" for="inlineCheckbox7"> <input
                                    type="checkbox" readonly="" id="domingo" name="domingo"
                                    value="7" title="Domingo"> Do
                            </label> 
                        </div>
                    </div>


                    <div class="hr-line-dashed">   </div>
                    <p>Información Interna</p> 

                    <div class="row">


                        <div class="col-md-6">
                            <label class="control-label">Ejecutivo Comercial</label>
                            <input type="text" class="form-control" readonly=""
                                   id="id_ejecutivo"
                                   value="<?php echo $data_ficha[0]['ejecutivo']; ?>">
                        </div>


                        <div class="col-lg-6">
                            <label class="control-label">N° Solicitud Tablet</label>
                            <input id="id_solicitd_tablet" name="id_solicitd_tablet" onkeypress="return solonumero(event)" class="form-control" 
                                   requerido="false" mensaje="Debe ingresar N° Solicitud tablet" maxlength="8"
                                   disabled="disabled"
                                   value="<?php echo $data_ficha[0]['id_solicitud_tablet']; ?>"

                                   />
                        </div>


                        <?php
                        $fichas_relacionada = $data_ficha[0]['fichas_relacionada'];
                        if ($fichas_relacionada == 'No tiene.') {
                            ?>

                            <div class = "col-lg-12">
                                <label class = "  control-label" for = "textarea">Observaciones</label>
                                <textarea readonly = "" class = "form-control" id = "comentario"
                                          name = "comentario" maxlength = "1000"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                                <p id="text-out">0/1000 caracteres restantes</p>
                            </div>
                        <?php } else { ?>
                            <div class = "col-lg-6">
                                <label class = "control-label" for = "textarea">Observaciones</label>
                                <textarea readonly = "" class = "form-control" id = "comentario"
                                          name = "comentario" maxlength = "1000"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                                <p id="text-out">0/1000 caracteres restantes</p>
                            </div>
                            <div class="col-lg-6">

                                <br>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Fichas Relacionadas
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                        $fichas_relacionada = $data_ficha[0]['fichas_relacionada'];
                                        $array_fichas_relacionadas = explode("|", $fichas_relacionada);
                                        $id_arra_fichas_relacionadas = $array_fichas_relacionadas[0];
                                        $num_arra_fichas_relacionadas = $array_fichas_relacionadas[1];
                                        $ids_ficha = explode(",", $id_arra_fichas_relacionadas);
                                        $nums_ficha = explode(",", $num_arra_fichas_relacionadas);
                                        for ($i = 0; $i < count($ids_ficha); $i++) {

                                            if ($data_ficha[0]['id_ficha'] == $ids_ficha[$i]) {
                                                ?>

                                                <button type="button" class="btn btn-outline btn-success dim" type="button">  <?php echo $nums_ficha[$i] ?>
                                                    <i class="fa fa-money"></i> 
                                                </button> 

                                            <?php } else {
                                                ?>
                                                <a href="<?php echo $ids_ficha[$i] ?>"  target="_blank">   
                                                    <button type="button" class="btn btn-outline btn-primary dim" type="button">  <?php echo $nums_ficha[$i] ?>
                                                        <i class="fa fa-money"></i> 
                                                    </button>
                                                </a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div> 
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                </form>

                <div class="row">

                    <div class="col-lg-6 ">
                        <label class=" control-label" style="text-align: right;">Empresa Edutecno que Factura:</label>
                        <select id="edutecno_cbx" name="edutecno_cbx"  class="select2_demo_3 form-control"></select>
                    </div>

                    <div class="col-lg-6 pull-right">
                        <label class=" control-label" style="text-align: right;">&nbsp; </label>
                        <?php
                        if (intval($data_ficha[0]['id_estado']) == 40) {

                            $comentario = $data_ficha[0]['comentario_rechazo'];
                            $respuesta = $data_ficha[0]['respuesta_rechazo'];

                            if ($comentario != '0' && $respuesta != '0') {
                                ?>
                                <div class="row">
                                    <div class="alert alert-success" style="width: 96%; margin-left: 15px;">
                                        <strong>Hay una respuesta  </strong>  
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6 ">
                                        <label class="  control-label" for="textarea">Solicitud de Corrección</label>
                                        <textarea style="border-color: #ed5565 !important;" readonly="" class="form-control" id="comentario_rechazo"
                                                  name="comentario_rechazo" maxlength="1000"><?php echo $data_ficha[0]['comentario_rechazo']; ?></textarea>
                                    </div>

                                    <div class="col-lg-6 ">
                                        <label class="  control-label" for="textarea">Respuesta de Ejecutivo Comercial</label>
                                        <textarea style="border-color: #1ab394 !important;" readonly="" class="form-control" id="respuesta_rechazo"
                                                  name="respuesta_rechazo" maxlength="1000"><?php echo $data_ficha[0]['respuesta_rechazo']; ?></textarea>
                                    </div>
                                </div>

                                <?php
                            } else {

                                // echo "Es una ficha nueva";
                            }
                            ?>


                            <br>
                            <button class='btn btn-md btn-primary' type="button" id='btn_aprobar_ficha'>Aceptar Aprobación</button>
                            <button class='btn btn-md btn-danger' type="button"  id='btn_rechazar_ficha'>Solicitar Corrección</button>  
                            <?php
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">


                <div class="col-md-6">

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cbx_orden_compra">Orden
                            Compra</label>
                        <div class="col-md-4">
                            <select id="cbx_orden_compra" name="cbx_orden_compra"
                                    class="form-control" requerido="true"
                                    mensaje="Debe seleccionar Orden de Compra">
                                <option value="">Todas O.C.</option>
                                <?php for ($i = 0; $i < count($data_orden_compra); $i++) { ?>
                                    <option
                                        value="<?php echo $data_orden_compra[$i]['id'] ?>"><?php echo $data_orden_compra[$i]['text'] ?></option>
                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                Seleccione Orden de compra para ver infomación adicional
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12"> 
        <!-- COSTOS/SERVICIOS ADICIONALES -->
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Costos/servicios adicionales</h5>
                <div class="ibox-tools">
                    <a class="collapse-link" hidden id="a_tab_tabla_carga"> <i
                            class="fa fa-chevron-down"></i>
                    </a>

                </div>
            </div>
            <div class="ibox-content">
                <table id="tbl_adicionales" class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>

                            <th>Gasto / Servicio</th>
                            <th>Valor</th>

                        </tr>
                    </thead>
                    <tbody></tbody>
                    <tfoot>
                        <tr>

                            <th>Total</th>
                            <th id="total_adicionales"></th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div> 


        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Alumno</h5>
                <div class="ibox-tools">
                    <b>Valor Imputable Sence: </b><p><?php echo $data_ficha[0]['valor_imputable']; ?></p>
                    <a class="collapse-link" hidden id="a_tab_tabla_carga"> <i
                            class="fa fa-chevron-down"></i>
                    </a>

                </div>
            </div>
            <div class="ibox-content">
                <table id="tbl_orden_compra_carga_alumno"
                       class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <th>Rut</th>                            
                            <th>Alumno</th>     
                            <th>CC.CC</th>
                            <th>%FQ</th>
                            <th>Valor FQ</th>
                            <th>Valor Adicional</th>
                            <th>Valor Cobrado</th>
                            <th>O.C</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Rut</th>                            
                            <th>Alumno</th>     
                            <th>CC.CC</th>
                            <th>%FQ</th>
                            <th>Valor FQ</th>
                            <th>Valor Adicional</th>
                            <th>Valor Cobrado</th>
                            <th>O.C</th>
                        </tr>
                    </tfoot>
                </table>
                <table style="width: 50%;">
                    <tr>
                        <td><label for="total_sence">Total Alumnos: </label></td>
                        <td id="total_alumnos"></td>
                    </tr>
                    <tr>
                        <td><label for="total_sence">Total Alumnos Sence: </label></td>
                        <td id="total_sence"></td>
                    </tr>
                    <tr>
                        <td><label for="total_empresa">Total Alumnos Empresa: </label></td>
                        <td id="total_empresa"></td>
                    </tr>
                    <tr>
                        <td><label for="total_sence_empresa">Total Alumnos Sence/Empresa: </label></td>
                        <td id="total_sence_empresa"></td>
                    </tr>
                    <tr>
                        <td><label for="total_becados">Total Alumnos Becados: </label></td>
                        <td id="total_becados"></td>
                    </tr>
                    <tr>
                        <td><label for="total_oc">Total OC: </label></td>
                        <td id="total_oc"></td>
                    </tr>
                    <tr>
                        <td><label for="total_adicional">Total valor adicional: </label></td>
                        <td id="total_adicional"></td>
                    </tr>
                    <tr>
                        <td><label for="valor_final">Valor Final: </label></td>
                        <td id="valor_final"></td>
                    </tr>
                </table>

            </div>

        </div>



    </div>

</div>

<div class="modal inmodal" id="modal_rechazo" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight" id="modal-principal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
                </button>
                <i class="fa fa-info-circle modal-icon"></i>
                <h2>
                    Ingrese motivo de corrección para la ficha N°:
                    <h4 class="modal-title" id="modal_id_ficha_rechazo"></h4>
                </h2>
            </div>
            <div class="modal-body"> 
                <form id="frm_rechazo" class="form-horizontal">
                    <input type="text" id="id_ficha_rechazo" name="id_ficha_rechazo"
                           hidden>
                    <div class="form-group">
                        <textarea name="motivo_rechazo" id="motivo_rechazo"
                                  class="form-control" cols="30" rows="10" requerido="true"
                                  mensaje="Debe Ingresar el motivo del rechazo"></textarea>
                    </div>
                    <div class="form-group"></div>
                </form>
                <button id="btn_aceptar" type="button" class="btn btn-warning">Aceptar</button>
                <button class="btn btn-white" data-dismiss="modal">Cancelar</button>

            </div>
        </div>
    </div>
</div>
<!--MODAL ANULACION FICHA-->
<div class="modal inmodal" id="modal_anulacion" tabindex="-1"
     role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight" id="modal-principal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span>
                </button>
                <i class="fa fa-info-circle modal-icon"></i>
                <h2>
                    Ingrese motivo de anulación para la ficha N°:
                    <h4 class="modal-title" id="modal_id_ficha_anulacion"></h4>
                </h2>
            </div>
            <div class="modal-body">
                <form id="frm_anulacion" class="form-horizontal">
                    <input type="text" id="id_ficha_anulacion" name="id_ficha_anulacion"
                           hidden>
                    <div class="form-group">
                        <textarea name="motivo_anulacion" id="motivo_anulacion"
                                  class="form-control" cols="30" rows="10" requerido="true"
                                  mensaje="Debe Ingresar el motivo de la anulación"></textarea>
                    </div>
                    <div class="form-group"></div>
                </form>
                <button id="btn_aceptar_anulacion" type="button" class="btn btn-warning">Aceptar</button>
                <button class="btn btn-white" data-dismiss="modal">Cancelar</button>

            </div>
        </div>
    </div>
</div>

