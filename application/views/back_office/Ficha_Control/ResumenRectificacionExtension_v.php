<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-02-22 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-02-22 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Datos del Curso</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <p> Datos de la Venta</p> 
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label"><strong>N° de Ficha:</strong></label>

                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['num_ficha']; ?>">

                    </div>
                    <div class="col-md-4">
                        <label class="  control-label">Tipo de Venta:</label>

                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['categoria']; ?>">

                    </div>
                    <div class="col-md-4">
                        <label class="  control-label"><strong> Estado Rectificación:</strong> </label>

                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['estado']; ?>">

                    </div>
                </div>



                <div class="hr-line-dashed"></div>

                <p> Datos de la Empresa</p> 

                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Holding</label>

                        <input type="text" class="form-control" readonly="" id="holding"
                               value="<?php echo $data_ficha[0]['holding']; ?>">

                    </div>
                    <div class="col-md-6">
                        <label class="  control-label">Empresa</label>

                        <input type="text" class="form-control" readonly=""
                               id="id_empresa"
                               value="<?php echo $data_ficha[0]['rut_empresa'] . " / " . $data_ficha[0]['empresa']; ?>">

                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-3">
                        <label class="control-label">Dirección Empresa</label>
                        <input type="text" class="form-control" readonly=""
                               id="direccion_empresa"
                               value="<?php echo $data_ficha[0]['direccion_empresa']; ?>">

                    </div>

                    <div class="col-lg-3">
                        <label class="control-label">Nombre Contacto</label>

                        <input type="text" class="form-control" readonly=""
                               id="nombre_contacto_empresa"
                               value="<?php echo $data_ficha[0]['nombre_contacto']; ?>">
                    </div>

                    <div class="col-lg-3">
                        <label class="control-label">Teléfono Contacto</label>

                        <input type="text" class="form-control" readonly=""
                               id="telefono_contacto_empresa"
                               value="<?php echo $data_ficha[0]['telefono_contacto']; ?>">
                    </div>                        

                    <div class="col-lg-3">
                        <label class="control-label">Dirección envío Diplomas</label>

                        <input type="text" class="form-control" readonly=""
                               id="lugar_entrega_diplomas" value="<?php echo $data_ficha[0]['direccion_diplomas']; ?>">
                    </div>   
                </div> 

                <div class="hr-line-dashed"></div>
                <p>Datos de la OTIC</p> 

                <div class="row">


                    <div class="col-lg-4">
                        <label class="  control-label">Otic</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['otic']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class=" control-label">Código Sence</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['codigo_sence']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class=" control-label">N° Orden Compra</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['num_orden_compra']; ?>">
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Datos del Curso</p>  
                <div class="row"> 
                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Modalidad</label>
                        <input type="text" class="form-control" readonly=""
                               id="modalidad"
                               value="<?php echo $data_ficha[0]['modalidad']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Curso</label>
                        <input type="text" class="form-control" readonly=""  id="curso_descrip" value="<?php echo $data_ficha[0]['descripcion_producto']; ?>">
                    </div>

                    <div class="col-md-4"> 
                        <label class="col-md-1 control-label">Versión</label>
                        <input type="text" class="form-control" readonly=""  id="version"  value="<?php echo $data_ficha[0]['version']; ?>">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6">
                        <label class=" control-label">Fecha Inicio y Término</label>
                        <div class="input-daterange input-group  col-lg-12 ">
                            <input type="text" readonly="" class="form-control"
                                   name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"
                                   value="<?php echo $data_ficha[0]['fecha_inicio']; ?>"
                                   requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                            <span class="input-group-addon">A</span> 
                            <input type="text"  class="form-control" name="fecha_termino" id="fecha_termino"
                                   placeholder="AAAA-MM-DD" value="<?php echo $data_ficha[0]['fecha_fin']; ?>" readonly=""
                                   requerido="true" mensaje="Fecha Término es un campo requerido" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-md-6 ">
                            <label class="  control-label">Hora Inicio</label>
                            <div class="input-group clockpicker">
                                <input type="time" class="form-control" id="txt_hora_inicio"
                                       name="txt_hora_inicio"
                                       value="<?php echo $data_ficha[0]['hora_inicio']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Inicio" readonly="" />
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <label class=" control-label">Hora Término</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input type="time" class="form-control" id="txt_hora_termino"
                                       name="txt_hora_termino"
                                       value="<?php echo $data_ficha[0]['hora_termino']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Término" readonly="">
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Lugar de Ejecución</p>  
                <div class="row">
                    <div class="col-lg-6"> 
                        <label class=" control-label">Sede</label>
                        <input type="text" class="form-control" readonly="" id="id_sede"
                               value="<?php echo $data_ficha[0]['sede']; ?>">

                    </div>
                    <div class="col-lg-6">
                        <label class=" control-label">Sala</label>
                        <input type="text" class="form-control" readonly="" id="id_sala"
                               value="<?php echo $data_ficha[0]['sala']; ?>">
                    </div>

                </div>
                <div class="row">


                    <div class="col-lg-6">
                        <label class="control-label">Dirección o Lugar Ejecución</label>
                        <input id="lugar_ejecucion" name="lugar_ejecucion" class="form-control" readonly="" 
                               value="<?php echo $data_ficha[0]['lugar_ejecucion']; ?>" 
                               />
                    </div>


                    <input  type="hidden" id="id_dias" value="<?php echo $data_ficha[0]['id_dias']; ?>">
                    <div class="col-lg-6">
                        <label class="control-label">Días</label>
                        <label class="checkbox-inline" for="inlineCheckbox1"> <input
                                type="checkbox" readonly="" id="lunes" name="lunes" value="1"
                                title="Lunes"> Lu
                        </label> <label class="checkbox-inline" for="inlineCheckbox2"> <input
                                type="checkbox" readonly="" id="martes" name="martes" value="2"
                                title="Martes"> Ma
                        </label> <label class="checkbox-inline" for="inlineCheckbox3"> <input
                                type="checkbox" readonly="" id="miercoles" name="miercoles"
                                value="3" title="Miércoles"> Mi
                        </label> <label class="checkbox-inline" for="inlineCheckbox4"> <input
                                type="checkbox" readonly="" id="jueves" name="jueves" value="4"
                                title="Jueves"> Ju
                        </label> <label class="checkbox-inline" for="inlineCheckbox5"> <input
                                type="checkbox" readonly="" id="viernes" name="viernes"
                                value="5" title="Viernes"> Vi
                        </label> <label class="checkbox-inline" for="inlineCheckbox6"> <input
                                type="checkbox" readonly="" id="sabado" name="sabado" value="6"
                                title="Sábado"> Sa
                        </label> <label class="checkbox-inline" for="inlineCheckbox7"> <input
                                type="checkbox" readonly="" id="domingo" name="domingo"
                                value="7" title="Domingo"> Do
                        </label> 
                    </div>
                </div>
                <div class="hr-line-dashed">   </div>
                <p>información Interna</p> 
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Ejecutivo Comercial</label>
                        <input type="text" class="form-control" readonly=""
                               id="id_ejecutivo"
                               value="<?php echo $data_ficha[0]['ejecutivo']; ?>">
                    </div>

                    <div class="col-lg-6">
                        <label class="control-label">N° Solicitud Tablet</label>
                        <input id="id_solicitd_tablet" name="id_solicitd_tablet" onkeypress="return solonumero(event)" class="form-control" 
                               requerido="false" mensaje="Debe ingresar N° Solicitud tablet" maxlength="8"
                               disabled="disabled"
                               value="<?php echo $data_ficha[0]['id_solicitud_tablet']; ?>"

                               />
                    </div>


                    <div class="col-md-12">
                        <label class="  control-label" for="textarea">Observaciones</label>
                        <textarea readonly="" class="form-control" id="comentario"
                                  name="comentario" maxlength="1000"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                        <p id="text-out">0/1000 caracteres restantes</p>
                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-12">        
                        <label class="control-label" for="textarea">Motivo Rectificación</label>
                        <textarea class="form-control" id="comentario_motivo" name="comentario_motivo" maxlength="200" 
                                  readonly="readonly"> <?php echo $data_rectificacion[0]['motivo_rectificacion']; ?> </textarea>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="id_rectificacion" name="id_rectificacion" value="<?php echo $data_rectificacion[0]['id_rectificacion']; ?>">
<div class="row" >
    <div class="col-lg-12">
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Rectificación Extensión de Plazo</h5>
                    <div class="ibox-tools">                        
                        <a class="collapse-link" hidden  id="a_tab_tabla_carga">
                            <i class="fa fa-chevron-down"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content"  > 
                    <table id="tbl_rectificacion_extension_plazo" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>  
                                <th>Tipo</th>        
                                <th>Fecha Inicio</th>                            
                                <th>Fecha I. a Actualizar</th>                            
                                <th>Fecha Término</th>
                                <th>Fecha Extensión</th>  
                                <th>Orden de Compra</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>   
                                <th>Tipo</th>        
                                <th>Fecha Inicio</th>         
                                <th>Fecha I. a Actualizar</th>                   
                                <th>Fecha Término</th>
                                <th>Fecha Extensión</th>  
                                <th>Orden de Compra</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Documentos Adjuntos</h5>
                    <div class="ibox-tools">                        
                        <a class="collapse-link" hidden  id="a_tab_tabla_carga">
                            <i class="fa fa-chevron-down"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content"  > 
                    <table id="tbl_rectificacion_extension_plazo_documento" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>Tipo Documento</th>                            
                                <th>Documento</th>    
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>                                
                                <th>Tipo Documento</th>                            
                                <th>Documento</th>    
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">

                <?php if (intval($data_rectificacion[0]['id_estado_edutecno']) == 40) { ?>
                    <button class='btn btn-md btn-primary' type="button" id='btn_aprobar_rectificacion'>Aprobar</button>  
                    <button class='btn btn-md btn-danger'  type="button" id='btn_rechazar_rectificacion'>Rechazar</button>  

                <?php }
                ?> 
            </div>
        </div>
    </div>
</div>



<div class="modal inmodal" id="modal_rechazo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight" id="modal-principal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <i class="fa fa-info-circle modal-icon"></i>
                <h2>Ingrese motivo de rechazo para la Rectificación N°: <h4 class="modal-title" id="modal_id_rectificacion_rechazo"></h4></h2>

            </div>
            <div class="modal-body">
                <form id="frm_rechazo" class="form-horizontal">
                    <input type="text" id="id_rectificacion_rechazo" name="id_rectificacion_rechazo" hidden>
                    <div class="form-group">
                        <textarea name="motivo_rechazo" id="motivo_rechazo" class="form-control" cols="30" rows="10" requerido="true" mensaje ="Debe Ingresar el motivo del rechazo"></textarea>
                    </div>
                    <div class="form-group">
                    </div>
                </form>
                <button id="btn_aceptar" type="button" class="btn btn-warning">Aceptar</button>
                <button class="btn btn-white" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
