<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<style>
    .select2-container{
        z-index: 2060;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="txt_id_ficha">N° de Ficha</label><br>
                                <input id="txt_id_ficha" name="txt_id_ficha" type="text" placeholder="ID Ficha o N° de Documento..." class="form-control input-lg-8" style="width: 100%"><br>
                            </div>
                            <div class="col-lg-4">
                                <label for="txt_oc_ficha">Orden de Compra</label><br>
                                <input id="txt_oc_ficha" name="txt_oc_ficha" type="text" placeholder="N° de la Orden de Compra..." class="form-control input-lg-8" style="width: 100%"><br>
                            </div>
                            <div class="col-lg-4">
                                <br>
                                <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success">
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Fichas</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>N° Ficha</th>  
                                    <th>Ejecutivo</th>
                                    <th title="Tipo venta">T.V</th>
                                    <th title="Modalidad -  Familia">Mo. - Fa.</th>
                                    <th title="Fecha Inicio -  Fecha Termino">F. Inicio. - F. Termino</th>
                                    <th>Descripción</th>
                                    <th>Otic</th>                                    
                                    <th>Empresa</th>
                                    <th>Totales</th>
                                    <th>Alumnos</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>N° Ficha</th>  
                                    <th>Ejecutivo</th>
                                    <th title="Tipo venta">T.V</th>
                                    <th title="Modalidad -  Familia">Mo. - Fa.</th>
                                    <th title="Fecha Inicio -  Fecha Termino">F. Inicio. - F. Termino</th>
                                    <th>Descripción</th>
                                    <th>Otic</th>                                    
                                    <th>Empresa</th>
                                    <th>Totales</th>
                                    <th>Alumnos</th>
                                    <th>Acciones</th>

                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiddens Modals para mostrar contactos -->

    <!-- Hiddens Modals para mostrar contactos -->

    <div class="modal inmodal" id="modal_rechazo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight" id="modal-principal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h2>Ingrese motivo de rechazo para la ficha N°: <h4 class="modal-title" id="modal_id_ficha_rechazo"></h4></h2>



                </div>
                <div class="modal-body">

                    <form id="frm_rechazo" class="form-horizontal">

                        <input type="text" id="id_ficha" name="id_ficha" hidden>

                        <div class="form-group">
                            <textarea name="motivo_rechazo" id="motivo_rechazo" class="form-control" cols="30" rows="10" requerido="true" mensaje ="Debe Ingresar el motivo del rechazo"></textarea>
                        </div>

                        <div class="form-group">

                        </div>

                    </form>
                    <button id="btn_aceptar" type="button" class="btn btn-warning">Aceptar</button>
                    <button class="btn btn-white" data-dismiss="modal">Cancelar</button>

                </div>
            </div>
        </div>
    </div>


    <!-- Hiddens Modals para mostrar contactos -->

    <div class="modal inmodal" id="modal_documentos_ficha" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight" id="modal-principal" onclick="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Documentos para ficha N°: <h4 class="modal-title" id="modal_id_ficha"></h4></h4>

                </div>
                <div class="modal-body">

                    <div  id="div_frm_add" style="display: none;"> 



                    </div>

                    <!-- <div class="table-responsive" id="div_tbl_contacto_empresa">   -->
                    <table id="tbl_documentos_ficha" class="table-md-4 table-bordered" >
                        <thead>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>        

                            </tr>
                        </thead>
                        <tbody>                              

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>          

                            </tr>
                        </tfoot>
                    </table>    


                    <!-- </div> -->



                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-white" data-dismiss="modal" id="btn_cerrarmodal_doc">Cerrar</button>

                </div>
            </div>
        </div>
    </div>




    <div class="modal inmodal" id="modal_edutecno" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight" id="modal-principal" onclick="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Facturación para ficha N°: <h4 class="modal-title" id="modal_id_ficha2"></h4></h4>

                </div>
                <div class="modal-body">

                    <form id="frm_edutecno" class="form-horizontal">
                        <input type="text" value="" id="id_ficha_edutecno" name="id_ficha_edutecno" hidden>                   

                        <div class="form-group">
                            <label class="col-lg-6 control-label">Empresa Edutecno que Factura:</label>
                            <div class="col-lg-6">
                                <select id="edutecno_cbx" name="edutecno_cbx"  class="select2_demo_3 form-control">
                                </select>
                            </div>
                        </div>

                    </form>



                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-white" data-dismiss="modal" id="btn_cerrarmodal_doc">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btn_guardar_edutecno">Guardar</button>

                </div>
            </div>
        </div>
    </div>

</div>


