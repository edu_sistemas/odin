<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-08-08 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-08-08  [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Datos</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <p> Datos de la Venta</p>
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label"><strong>N° de Ficha:</strong></label>
                        <input type="text" id="num_ficha" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['num_ficha']; ?>">
                        <input id="id_ficha" type="hidden" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['id_ficha']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class="  control-label">Tipo de Venta:</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['categoria']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class="  control-label"><strong> Estado Rectificación:</strong> </label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['estado']; ?>">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <p> Datos de la Empresa</p>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Holding</label>
                        <input type="hidden" class="form-control" readonly="" id="id_holding" value="<?php echo $data_ficha_preview[0]['id_holding']; ?>">
                        <input type="text" class="form-control" readonly="" id="holding" value="<?php echo $data_ficha_preview[0]['holding']; ?>">
                    </div>
                    <div class="col-md-6">
                        <label class="  control-label">Empresa</label>
                        <input type="text" class="form-control" readonly="" id="id_empresa" value="<?php echo $data_ficha_preview[0]['rut_empresa'] . " / " . $data_ficha_preview[0]['empresa']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3">
                        <label class="control-label">Dirección Empresa</label>
                        <input type="text" class="form-control" readonly="" id="direccion_empresa" value="<?php echo $data_ficha_preview[0]['direccion_empresa']; ?>">
                    </div>
                    <div class="col-lg-3">
                        <label class="control-label">Nombre Contacto</label>
                        <input type="text" class="form-control" readonly="" id="nombre_contacto_empresa" value="<?php echo $data_ficha_preview[0]['nombre_contacto']; ?>">
                    </div>
                    <div class="col-lg-3">
                        <label class="control-label">Teléfono Contacto</label>
                        <input type="text" class="form-control" readonly="" id="telefono_contacto_empresa" value="<?php echo $data_ficha_preview[0]['telefono_contacto']; ?>">
                    </div>
                    <div class="col-lg-3">
                        <label class="control-label">Dirección envío Diplomas</label>
                        <input type="text" class="form-control" readonly="" id="lugar_entrega_diplomas" value="<?php echo $data_ficha_preview[0]['direccion_diplomas']; ?>">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <p>Datos de la OTIC</p>
                <div class="row">
                    <div class="col-lg-4">
                        <label class="  control-label">Otic</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['otic']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class=" control-label">Código Sence</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['codigo_sence']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class=" control-label">N° Orden Compra</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['num_orden_compra']; ?>">
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Datos del Curso</p>
                <div class="row">
                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Modalidad</label>
                        <input type="text" class="form-control" readonly="" id="modalidad" value="<?php echo $data_ficha_preview[0]['modalidad']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Curso</label>
                        <input type="text" class="form-control" readonly="" id="curso_descrip" value="<?php echo $data_ficha_preview[0]['descripcion_producto']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Versión</label>
                        <input type="text" class="form-control" readonly="" id="version" value="<?php echo $data_ficha_preview[0]['version']; ?>">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6">
                        <label class=" control-label">Fecha Inicio y Término</label>
                        <div class="input-daterange input-group  col-lg-12 ">
                            <input type="text" readonly="" class="form-control" name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD" value="<?php echo $data_ficha_preview[0]['fecha_inicio']; ?>" requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                            <span class="input-group-addon">A</span>
                            <input type="text" class="form-control" name="fecha_termino" id="fecha_termino" placeholder="AAAA-MM-DD" value="<?php echo $data_ficha_preview[0]['fecha_fin']; ?>" readonly="" requerido="true" mensaje="Fecha Término es un campo requerido" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-md-6 ">
                            <label class="  control-label">Hora Inicio</label>
                            <div class="input-group clockpicker">
                                <input type="time" class="form-control" id="txt_hora_inicio" name="txt_hora_inicio" value="<?php echo $data_ficha_preview[0]['hora_inicio']; ?>" requerido="true" mensaje="Ingresar Hora Inicio" readonly="" />
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <label class=" control-label">Hora Término</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input type="time" class="form-control" id="txt_hora_termino" name="txt_hora_termino" value="<?php echo $data_ficha_preview[0]['hora_termino']; ?>" requerido="true" mensaje="Ingresar Hora Término" readonly="">
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Lugar de Ejecución</p>
                <div class="row">
                    <div class="col-lg-6">
                        <label class=" control-label">Sede</label>
                        <input type="text" class="form-control" readonly="" id="id_sede" value="<?php echo $data_ficha_preview[0]['sede']; ?>">
                    </div>
                    <div class="col-lg-6">
                        <label class=" control-label">Sala</label>
                        <input type="text" class="form-control" readonly="" id="id_sala" value="<?php echo $data_ficha_preview[0]['sala']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <label class="control-label">Dirección o Lugar Ejecución</label>
                        <input id="lugar_ejecucion" name="lugar_ejecucion" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['lugar_ejecucion']; ?>" />
                    </div>
                    <input type="hidden" id="id_dias" value="<?php echo $data_ficha_preview[0]['id_dias']; ?>">
                    <div class="col-lg-6">
                        <label class="control-label">Días</label>
                        <label class="checkbox-inline" for="inlineCheckbox1"> 
                            <input
                                type="checkbox" readonly="" id="lunes" name="lunes" value="1"
                                title="Lunes"> Lu
                        </label> <label class="checkbox-inline" for="inlineCheckbox2"> <input
                                type="checkbox" readonly="" id="martes" name="martes" value="2"
                                title="Martes"> Ma
                        </label> <label class="checkbox-inline" for="inlineCheckbox3"> <input
                                type="checkbox" readonly="" id="miercoles" name="miercoles"
                                value="3" title="Miércoles"> Mi
                        </label> <label class="checkbox-inline" for="inlineCheckbox4"> <input
                                type="checkbox" readonly="" id="jueves" name="jueves" value="4"
                                title="Jueves"> Ju
                        </label> <label class="checkbox-inline" for="inlineCheckbox5"> <input
                                type="checkbox" readonly="" id="viernes" name="viernes"
                                value="5" title="Viernes"> Vi
                        </label> <label class="checkbox-inline" for="inlineCheckbox6"> <input
                                type="checkbox" readonly="" id="sabado" name="sabado" value="6"
                                title="Sábado"> Sa
                        </label> <label class="checkbox-inline" for="inlineCheckbox7"> <input
                                type="checkbox" readonly="" id="domingo" name="domingo"
                                value="7" title="Domingo"> Do
                        </label>
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Información Interna</p>
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Ejecutivo Comercial</label>
                        <input type="text" class="form-control" readonly="" id="id_ejecutivo" value="<?php echo $data_ficha_preview[0]['ejecutivo']; ?>">
                    </div>
                    <div class="col-lg-6">
                        <label class="control-label">N° Solicitud Tablet</label>
                        <input id="id_solicitd_tablet" name="id_solicitd_tablet" onkeypress="return solonumero(event)" class="form-control" requerido="false" mensaje="Debe ingresar N° Solicitud tablet" maxlength="8" disabled="disabled" value="<?php echo $data_ficha_preview[0]['id_solicitud_tablet']; ?>" />
                    </div>
                    <div class="col-md-12">
                        <label class="  control-label" for="textarea">Observaciones</label>
                        <textarea readonly="" class="form-control" id="comentario" name="comentario" maxlength="1000"><?php echo $data_ficha_preview[0]['comentario_orden_compra']; ?></textarea>
                        <p id="text-out">0/1000 caracteres restantes</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ibox-content" style="border-color: red; border-style: solid; border-bottom-color: red;"> 
    <p>Detalles de Anulación</p>
    <div class="row">
        <div class="form-group">
            <form id="anulacion" method="post" class="form-horizontal">
                <div class="col-md-12">
                    <div class="col-lg-6">
                        <label class="control-label" for="table">Documetos de respaldo para Anular</label>
                        <table id="tbl_documentos_ficha" class="table table-bordered" >
                            <thead>
                                <tr>
                                    <th>Nombre</th> 
                                    <th>Fecha de subida</th>         
                                </tr>
                            </thead>
                            <tbody>         
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Nombre</th> 
                                    <th>Fecha de subida</th>    
                                </tr>
                            </tfoot>
                        </table>  
                    </div>
                    <div class="col-lg-6">
                        <div class="col-md-12">
                            <label class="control-label" for="textarea">Motivo de Anulación</label>
                            <textarea rows="4" readonly="" cols="100" class="form-control" id="motivo_anulacion" requerido="true" mensaje="Ingresar motivo de anulación" name="motivo_anulacion" maxlength="1000"><?php echo $data_motivo[0]['motivo_anulacion'] ?></textarea>
                            <p id="text-out">0/1000 caracteres restantes</p>
                        </div>
                    </div>
                    <input id="id_ficha_f" name="id_ficha_f" type="hidden" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['id_ficha']; ?>">
                    <button type="button" id="btn_enviar" style="margin-right:25px;" class="btn btn-w-m btn-danger pull-right">Anular Ficha</button>
                </div>
            </form>
        </div>
    </div>
</div>