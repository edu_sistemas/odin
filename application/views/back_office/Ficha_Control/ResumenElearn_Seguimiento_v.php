<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5 id="h_text_title_ficha">Resumen</h5>
				<div class="ibox-tools">
					<a href="../../ResumeSeguimientoLearn/generarExcel/<?php echo $id_ficha; ?>"><button type="button" id="btn_pdf" class="btn btn-w-m btn-info btn-rounded"><i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Descargar Planilla de Curso</button></a>
					<a href="../../ResumeSeguimientoLearn/generarExcelAlumnos/<?php echo $id_ficha; ?>"><button type="button" id="btn_ex_alm" class="btn btn-w-m btn-primary btn-rounded"><i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Descargar Planilla de Alumnos</button></a>
				</div>
			</div>
			<div class="ibox-content">

				<input type="hidden" id="id_ficha" name="id_ficha" value="<?php echo $data_ficha[0]['id_ficha']; ?>">
				<input type="hidden" id="num_ficha" name="num_ficha" value="<?php echo $data_ficha[0]['num_ficha']; ?>">
				<p> Datos de la Venta</p>
				<div class="row">
					<div class="col-md-4">
						<label class="control-label"><strong>N° de Ficha:</strong></label>

						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['num_ficha']; ?>">
					</div>
					<div class="col-md-4">
						<label class="  control-label">Tipo de Venta:</label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['categoria']; ?>">
					</div>
					<div class="col-md-4">
						<label class="  control-label"><strong>Estado:</strong> </label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['estado']; ?>"><br>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<label class="control-label"><strong>Rut Empresa:</strong></label>

						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['rut_empresa']; ?>">
					</div>
					<div class="col-md-4">
						<label class="  control-label"><strong>Empresa:</strong> </label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['empresa']; ?>">
					</div>
					<div class="col-md-4">
						<label class="  control-label"><strong>Nombre Holding:</strong> </label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['holding']; ?>">
					</div>
				</div> <br>
				<div class="row">
					<div class="col-md-4">
						<label class="control-label"><strong>Código Sence:</strong></label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['codigo_sence']; ?>">
					</div>
					<div class="col-md-4">
						<label class="control-label"><strong>Nombre Curso Según Sence:</strong></label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['nombre_curso_sence']; ?>">
					</div>
					<div class="col-md-4">
						<label class="  control-label">Nombre curso Edutecno| Duración:</label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['descripcion_producto']; ?>">
					</div>
				</div> <br>
				<div class="row">
					<div class="col-md-6">
						<label class="control-label"><strong>Ejecutivo Comercial:</strong></label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['ejecutivo']; ?>">
					</div>
					<div class="col-md-6">
						<label class="control-label"><strong>Empresa edutecno que factura:</strong></label>
						<input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['edutecno_edu']; ?>">
					</div>
					 
                                    
                                    <?php
                        $fichas_relacionada = $data_ficha[0]['fichas_relacionada'];
                        if ($fichas_relacionada == 'No tiene.') {
                            ?>

                            <div class = "col-lg-12">
                                <label class = "  control-label" for = "textarea">Observaciones</label>
                                <textarea readonly = "" class = "form-control" id = "comentario"
                                          name = "comentario" maxlength = "1000"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                                <p id="text-out">0/1000 caracteres restantes</p>
                            </div>
                        <?php } else { ?>
                            <div class = "col-lg-6">
                                <label class = "control-label" for = "textarea">Observaciones</label>
                                <textarea readonly = "" class = "form-control" id = "comentario"
                                          name = "comentario" maxlength = "1000"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                                <p id="text-out">0/1000 caracteres restantes</p>
                            </div>
                            <div class="col-lg-6">

                                <br>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        Fichas Relacionadas
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                        $fichas_relacionada = $data_ficha[0]['fichas_relacionada'];
                                        $array_fichas_relacionadas = explode("|", $fichas_relacionada);
                                        $id_arra_fichas_relacionadas = $array_fichas_relacionadas[0];
                                        $num_arra_fichas_relacionadas = $array_fichas_relacionadas[1];
                                        $ids_ficha = explode(",", $id_arra_fichas_relacionadas);
                                        $nums_ficha = explode(",", $num_arra_fichas_relacionadas);
                                        for ($i = 0; $i < count($ids_ficha); $i++) {
                                            if ($data_ficha[0]['id_ficha'] == $ids_ficha[$i]) {
                                                ?>
                                                <button type="button" class="btn btn-outline btn-success dim" type="button">  <?php echo $nums_ficha[$i] ?>
                                                    <i class="fa fa-money"></i> 
                                                </button> 
                                            <?php } else {
                                                ?>
                                                <a href="<?php echo $ids_ficha[$i] ?>"  target="_blank">   
                                                    <button type="button" class="btn btn-outline btn-primary dim" type="button">  <?php echo $nums_ficha[$i] ?>
                                                        <i class="fa fa-money"></i> 
                                                    </button>
                                                </a>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div> 
                            </div>
                            <?php
                        }
                        ?>
                                    
					<br>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-content">
								<div class="col-md-6">
									<!-- Select Basic -->
									<div class="form-group">
										<label class="col-md-4 control-label" for="cbx_orden_compra">Orden
                                            Compra</label>
										<div class="col-md-4">
											<select id="cbx_orden_compra" name="cbx_orden_compra" class="form-control" requerido="true" mensaje="Debe seleccionar Orden de Compra">
												<option value="">Todas O.C.</option>
												<?php for ($i = 0; $i < count($data_orden_compra); $i++) { ?>
													<option
														value="<?php echo $data_orden_compra[$i]['id'] ?>"><?php echo $data_orden_compra[$i]['text'] ?></option>
														<?php
													}
													?>
											</select>
										</div>
									</div>
								</div>
								Seleccione Orden de compra para ver infomación adicional
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="col-md-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Alumno</h5>

									<div class="ibox-tools">
										<a class="collapse-link" hidden id="a_tab_tabla_carga"> <i
                                                class="fa fa-chevron-down"></i>
                                        </a>
									</div>
								</div>
								<div class="ibox-content">
									<form method="post" class="form-horizontal" id="frm_descarte">

										<table id="tbl_orden_compra_carga_alumno" class="table table-striped table-bordered table-hover dataTables-example">
											<thead>
												<tr>
													<th>Rut</th>
													<th>Alumno</th>
													<th>Estado</th>
													<th>Motivo de descarte</th>
													<th>CC.CC</th>
													<th>%FQ</th>
													<th>Costo Otic</th>
													<th>Costo Empresa</th>
													<th>Total</th>
													<th>Des</th>
													<th>Motivo</th>
												</tr>
											</thead>
											<tfoot>
												<tr>
													<th>Rut</th>
													<th>Alumno</th>
													<th>Estado</th>
													<th>Motivo de descarte</th>
													<th>CC.CC</th>
													<th>%FQ</th>
													<th>Costo Otic</th>
													<th>Costo Empresa</th>
													<th>Total</th>
													<th>Des</th>
													<th>Motivo</th>
												</tr>
											</tfoot>
										</table>

										<div class="row">
											<div class="col-lg-1">
												<label>comentarios: </label>
											</div>
											<div class="col-lg-9">
												<textarea placeholder="Agregar comentarios al correo..." id="comentarios_email" name="comentarios_email" rows="5" cols="50"></textarea>
											</div>
										</div>
										<div class="row">
											<hr>
											<div class="col-lg-6">
												<button style="width: 100%;" type="button" id="btn_descarte" class="btn btn-w-m btn-danger btn-rounded"><i class="fa fa-crosshairs"></i>&nbsp;&nbsp;Descartar Alumnos</button>
											</div>
											<div class="col-lg-6">
												<button style="width: 100%;" type="button" id="btn_envio_correo" class="btn btn-w-m btn-success btn-rounded"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Enviar Correo</button>
											</div>
										</div>
										<hr>
										<input id="cantidad" name="cantidad" type="hidden">
										<input id="id_fichaff" name="id_fichaff" type="hidden">
									</form>

									<table style="width: 50%;">
										<tr>
											<td><label for="total_sence">Total Alumnos: </label></td>
											<td id="total_alumnos"></td>
										</tr>
										<tr>
											<td><label for="total_sence">Total Alumnos Sence: </label></td>
											<td id="total_sence"></td>
										</tr>
										<tr>
											<td><label for="total_empresa">Total Alumnos Empresa: </label></td>
											<td id="total_empresa"></td>
										</tr>
										<tr>
											<td><label for="total_sence_empresa">Total Alumnos Sence/Empresa: </label></td>
											<td id="total_sence_empresa"></td>
										</tr>
										<tr>
											<td><label for="total_becados">Total Alumnos Becados: </label></td>
											<td id="total_becados"></td>
										</tr>
										<tr>
											<td><label for="total_oc">Total OC: </label></td>
											<td id="total_oc"></td>
										</tr>
										<tr>
											<td><label for="total_agregado">Total Valor Agregado: </label></td>
											<td id="total_agregado"></td>
										</tr>
										<tr>
											<td><label for="valor_final">Valor Final: </label></td>
											<td id="valor_final"></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Rectificaciones</h5>
							<div class="ibox-tools">
								<a class="collapse-link" hidden id="a_tab_tabla_carga"> <i
										class="fa fa-chevron-down"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div class="row">
								<div class="col-md-12">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<h5>Alumnos</h5>
											<div class="ibox-tools">
												<div style=" margin-right: 337px; color: red; font-weight: bold;" id="mensaje_text_area_alumno"></div>
												<input type="hidden" id="txt_aprobar_alumnos" name="txt_aprobar_alumnos" value="Rectificación de Alumnos registrada en Ofimática" >
												<textarea placeholder="Agrega un comentario antes de confirmar los cambios..." style="margin-right: 10px; margin-top: 10px;"
														  id="comentario_alumno" name="comentario_alumno" requerido="true" mensaje="Ingresar un comentario"  rows="3" cols="30"></textarea>

												<button type="button" id="aprobar_alumnos" class="btn btn-w-m btn-danger btn-rounded">
													<i class="fa fa-check-square-o"></i>&nbsp;&nbsp;Cambios realizados en ofimática
												</button>
											</div>
										</div>
										<div class="ibox-content">
											<table id="tbl_orden_compra_carga_alumno_rectificacion" class="table table-striped table-bordered table-hover dataTables-example">
												<thead>
													<tr>
														<th>Tipo</th>
														<th>Rut</th>
														<th>Nombre</th>
														<th>Apellido Paterno</th>
														<th>CC.CC</th>
														<th>%FQ</th>
														<th>Total</th>
														<th>Causa</th>
														<th>Fecha</th>
														<th>Estado</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Tipo</th>
														<th>Rut</th>
														<th>Nombre</th>
														<th>Apellido Paterno</th>
														<th>CC.CC</th>
														<th>%FQ</th>
														<th>Total</th>
														<th>Causa</th>
														<th>Fecha</th>
														<th>Estado</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<h5>Documentos Rectificaciones </h5>
											<div class="ibox-tools">
												<a class="collapse-link" hidden id="a_tab_tabla_carga"> <i
														class="fa fa-chevron-down"></i>
												</a>
											</div>
										</div>
										<div class="ibox-content">
											<table id="tbl_orden_compra_documentos_rectificaciones" class="table table-striped table-bordered table-hover dataTables-example">
												<thead>
													<tr>
														<th>Tipo Documento</th>
														<th>Documento</th>
														<th>Fecha Adjuntado </th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Tipo Documento</th>
														<th>Documento</th>
														<th>Fecha Adjuntado </th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> 
				<!-- EXTENSIONES -->
				<div class="row">
					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Extensiones Plazo</h5>
							<div class="ibox-tools">
								<a class="collapse-link" hidden id="a_tab_tabla_carga"> <i
										class="fa fa-chevron-down"></i>
								</a>
							</div>
						</div>
						<div class="ibox-content">
							<div class="row">
								<div class="col-md-12">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<h5>Extensiones</h5>
											<div class="ibox-tools">
												<input type="hidden" id="estado_edu" name="estado_edu" > 
												<div style=" margin-right: 337px; color: red; font-weight: bold;" id="mensaje_text_area"></div>
												<input type="hidden" id="txt_aprobar_extension" name="txt_aprobar_extension" value="Extensión de plazo registrada en Ofimática" >
												<textarea placeholder="Agrega un comentario antes de confirmar los cambios..." style="margin-right: 10px; margin-top: 10px;"
														  id="comentario_extension" name="comentario_extension" requerido="true" mensaje="Ingresar un comentario"  rows="3" cols="30"></textarea>

												<button style="margin-top: -20px" type="button" id="aprobar_extension" class="btn btn-w-m btn-danger btn-rounded">
													<i class="fa fa-check-square-o"></i>&nbsp;&nbsp;Cambios realizados en ofimática
												</button>
											</div>
										</div>
										<div class="ibox-content">
											<table id="tbl_rectificacion_extension_plazo" class="table table-striped table-bordered table-hover dataTables-example">
												<thead>
													<tr>
														<th>Tipo</th>
														<th>Fecha Inicio</th>
														<th>Fecha Término</th>
														<th>Fecha Extensión</th>
														<th>Orden de Compra</th>
														<th>Fecha</th>
														<th>Estado</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Tipo</th>
														<th>Fecha Inicio</th>
														<th>Fecha Término</th>
														<th>Fecha Extensión</th>
														<th>Orden de Compra</th>
														<th>Fecha</th>
														<th>Estado</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<h5>Documentos Extensión </h5>
											<div class="ibox-tools">
												<a class="collapse-link" hidden id="a_tab_tabla_carga"> <i
														class="fa fa-chevron-down"></i>
												</a>
											</div>
										</div>
										<div class="ibox-content">
											<table id="tbl_orden_compra_documentos_extensiones" class="table table-striped table-bordered table-hover dataTables-example">
												<thead>
													<tr>
														<th>Tipo Documento</th>
														<th>Documento</th>
														<th>Fecha Adjuntado </th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Tipo Documento</th>
														<th>Documento</th>
														<th>Fecha Adjuntado </th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<style>
					.dataTables_wrapper {
						padding-bottom: 0px;
					}

					.dataTables_length {
						display: none;
					}
				</style>