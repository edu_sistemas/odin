<?php
/**
 * Tablet
 *
 * Description...
 *
 * @version 0.0.1
 *
 
 * Fecha creacion:	2018-03-06 [Jessica Roa] <jroa@edutecno.com>
 */
?>
    <style>
        /*select2*/

        .select2-container--open {
            z-index: 2060;
        }

        .swal2-container {
            z-index: 2061;
        }

        .skin-3 .asdf > li > a {
            background-color: #e6e6e6;
            opacity: 0.8;
        }
        .skin-3 .nav.nav-tabs > li.active > a {
            background-color: #fff;
        }
        .ui-widget{
            z-index: 2060;
        }
    </style>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <button type="button" id="btn_nuevo" class="btn btn-w-m btn-success">Nuevo Modelo</button>
                            <button type="button" id="btn_ingreso" class="btn btn-w-m btn-primary">Ingreso Tablet</button>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <ul class="nav nav-tabs asdf">
                            <li class="active">
                                <a data-toggle="tab" href="#home">Inventario</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#menu1">Ingresos</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <h3>Inventario</h3>
                                <div class="table-responsive">
                                    <table id="tbl_Inventario_tablet" class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Gamma</th>
                                                <th>Descripción</th>
                                                <th>Cantidad en Stock</th>
                                                <th>Cantidad Solicitada</th>
                                                <th>Cantidad Necesaria</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Gamma</th>
                                                <th>Descripción</th>
                                                <th>Cantidad en Stock</th>
                                                <th>Cantidad Solicitada</th>
                                                <th>Cantidad Necesaria</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            <div id="menu1" class="tab-pane fade">
                                <h3>Ingresos</h3>
                                    <table id="tbl_Ingresos_tablet" class="table table-striped table-bordered table-hover dataTables-example" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Cantidad</th>
                                                <th>Fecha Ingreso</th>
                                                <th>Descripción</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Cantidad</th>
                                                <th>Fecha Ingreso</th>
                                                <th>Descripción</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <!--MODAL NUEVO MODELO -->
    <div class="modal inmodal in" id="NuevoModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-tablet modal-icon"></i>
                    <h4 class="modal-title">Nuevo Modelo de Tablet</h4>
                    <!--small>La siguiente reserva tiene como estado: Confirmada!</small-->
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal" id="frm_nuevo_modelo">
                        <h4>Datos del nuevo modelo</h4>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Marca:</label>
                            <div class="col-sm-4">
                                <input type="text" id="marca_tablet" name="marca_tablet" class="form-control input-md" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Modelo:</label>
                            <div class="col-sm-4">
                                <input type="text" id="modelo_tablet" name="modelo_tablet" class="form-control input-md" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gamma:</label>
                            <div class="col-md-6">
                                <select id="cbx_gamma" name="cbx_gamma" class="select2_demo_3 form-control" required>
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descripción:</label>
                            <div class="col-sm-4">
                                <textarea id="detalles_tablet" name="detalles_tablet" cols="80" rows="4" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descripción Técnica:</label>
                            <div class="col-sm-4">
                                <textarea id="descripcion_tecnica_tablet" name="descripcion_tecnica_tablet" cols="80" rows="4" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <button id="btn_insert_tablet" class="btn btn-primary pull-left">Guardar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <br>
                    </form>
                    <div class="modal-footer">
                        <button type="reset" id="reset" class="btn btn-outline  btn-danger" data-dismiss="modal">Volver</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL INGRESAR TABLET -->
    <div class="modal inmodal in" id="IngresoModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-calculator modal-icon"></i>
                    <h4 class="modal-title">Registrar Ingresos de Tablet</h4>
                    <!--small>La siguiente reserva tiene como estado: Confirmada!</small-->
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal" id="form_insert_cantidad">
                        <h4>Datos del ingreso</h4>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tablet:</label>
                            <div class="col-md-6">
                                <select id="cbx_modelo" name="cbx_modelo" class="select2_demo_3 form-control" required>
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Proveedor:</label>
                            <div class="col-md-4">
                                <div class="ui-widget">
                                    <input id="proveedor" name="proveedor" class="form-control input-md"
                                    required>
                                </div>
                                <input type="hidden" id="cbx_proveedor" name="cbx_proveedor">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Cantidad:</label>
                            <div class="col-sm-4">
                                <input type="text" id="cantidad_tablet" name="cantidad_tablet" onkeypress="return solonumero(event)" class="form-control input-md"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Precio:</label>
                            <div class="col-sm-4">
                                <input type="text" id="precio_tablet" name="precio_tablet" onkeypress="return solonumero(event)" class="form-control input-md"
                                    required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Factura o Guia:</label>
                            <div class="col-sm-4">
                                <input type="text" id="guia_tablet" name="guia_tablet" class="form-control input-md" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Fecha de Ingreso:</label>
                            <div class="input-group date col-sm-4" id="datepicker_ingreso_tablet">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input type="text" class="form-control" id="fecha_ingreso_tablet" name="fecha_ingreso_tablet" placeholder="dd-mm-aaaa" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descripción:</label>
                            <div class="col-sm-4">
                                <textarea id="descripcion_ingreso" name="descripcion_ingreso" cols="80" rows="4" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <button id="btn_insert_cantidad" class="btn btn-primary pull-left">Guardar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <br>
                    </form>
                    <div class="modal-footer">
                        <button type="reset" id="reset" class="btn btn-outline  btn-danger" data-dismiss="modal">Volver</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL BORRAR INGRESO TABLET -->
    <div class="modal inmodal in" id="delModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-calculator modal-icon"></i>
                    <h4 class="modal-title">Borrar Ingreso de Tablet</h4>
                    <p id="ref_mod"></p>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal">
                        <input type="hidden" id="id_ingreso_eliminar" name="id_ingreso_eliminar" />
                        <h4>Datos del ingreso</h4>
                        <p id="ref_mod_dat"></p>
                        <br>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Observaciones:</label>
                            <div class="col-sm-4">
                                <textarea id="descripcion_eliminar" cols="80" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <button type="button" id="btn_eliminar_ingreso" class="btn btn-danger">Eliminar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <br>
                    </form>
                    <div class="modal-footer">
                        <button type="reset" id="reset" class="btn btn-outline  btn-warning" data-dismiss="modal">Volver</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL DESCRIPCION TÉCNICA -->
    <div class="modal inmodal in" id="modal_descripcion_tecnica" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h3 id="titulo_descripcion_tecnica"></h3>
                </div>
                <div class="modal-body" id="texto_descripcion_tecnica">
                </div>
                <div class="modal-footer">
                    <button type="reset" id="reset" class="btn btn-outline  btn-warning" data-dismiss="modal">Volver</button>
                </div>
            </div>
        </div>
    </div>
    </div>

    </div>

    <style>
        #ref_mod,
        #ref_mod_dat {
            font-size: larger;
        }
    </style>