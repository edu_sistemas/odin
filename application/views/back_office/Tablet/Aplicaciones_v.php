<?php
/**
 * Tablet
 *
 * Description...
 *
 * @version 0.0.1
 *
 
 * Fecha creacion:	2018-03-09 [Jessica Roa] <jroa@edutecno.com>
 */
?>
    <style>
        /*select2*/

        .select2-container--open {
            z-index: 2060;
        }

        .swal2-container {
            z-index: 2061;
        }

        #ref_mod,
        #ref_mod_dat {
            font-size: larger;
        }
        .skin-3 .asdf > li > a {
            background-color: #e6e6e6;
            opacity: 0.8;
        }
        .skin-3 .nav.nav-tabs > li.active > a {
            background-color: #fff;
        }
    </style>
    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="ibox-tools">

                        </div>
                    </div>
                    <div class="ibox-content">

                        <ul class="nav nav-tabs asdf">
                            <li class="active">
                                <a data-toggle="tab" href="#home">Solicitudes</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#menu1">Histórico de Solicitudes</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#menu2">Inventario</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <h3>Solicitudes</h3>
                                <div class="table-responsive">
                                    <table id="tbl_Solicitud_tablet" class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Nombre Solicitante</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Cantidad</th>
                                                <th>Modelo Tablet</th>
                                                <th>Aplicación</th>
                                                <th>Ficha</th>
                                                <th>Fecha Requerida</th>
                                                <th>Dirección de entrega</th>
                                                <th>Estado</th>
                                                <th>Observación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>N°</th>
                                                <th>Nombre Solicitante</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Cantidad</th>
                                                <th>Modelo Tablet</th>
                                                <th>Aplicación</th>
                                                <th>Ficha</th>
                                                <th>Fecha Requerida</th>
                                                <th>Dirección de entrega</th>
                                                <th>Estado</th>
                                                <th>Observación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <h3>Histórico de Solicitudes</h3>
                                <div class="table-responsive">
                                    <table id="tbl_Solicitud_historico" class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Nombre Solicitante</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Cantidad</th>
                                                <th>Modelo Tablet</th>
                                                <th>Aplicación</th>
                                                <th>Ficha</th>
                                                <th>Fecha Requerida</th>
                                                <th>Dirección de entrega</th>
                                                <th>Estado</th>
                                                <th>Observación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>N°</th>
                                                <th>Nombre Solicitante</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Cantidad</th>
                                                <th>Modelo Tablet</th>
                                                <th>Aplicación</th>
                                                <th>Ficha</th>
                                                <th>Fecha Requerida</th>
                                                <th>Dirección de entrega</th>
                                                <th>Estado</th>
                                                <th>Observación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <h3>Inventario</h3>
                                <div class="table-responsive">
                                    <table id="tbl_Inventario_tablet" class="table table-striped table-bordered table-hover dataTables-example" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Descripción</th>
                                                <th>Cantidad en Stock</th>
                                                <th>Cantidad Solicitada</th>
                                                <th>Cantidad Necesaria</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Descripción</th>
                                                <th>Cantidad en Stock</th>
                                                <th>Cantidad Solicitada</th>
                                                <th>Cantidad Necesaria</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL DESPACHO TABLET -->
    <div class="modal inmodal in" id="cambioSolicitudModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-calculator modal-icon"></i>
                    <h4 class="modal-title">Despachar solicitud</h4>
                    <p id="ref_mod"></p>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal" id="frm_despacho_tablet">
                        <input type="hidden" id="id_solicitud" name="id_solicitud" />
                        <h4>Datos del ingreso</h4>
                        <p id="ref_mod_dat"></p>
                        <br>
                        <input type="hidden" id="cbx_estado" name="cbx_estado" value="3">

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Observación:</label>
                            <div class="col-sm-4">
                                <textarea id="observacion" name="observacion" cols="80" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <button id="btn_cambiar_estado" class="btn btn-danger">Cambiar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <br>
                    </form>
                    <div class="modal-footer">
                        <button type="reset" id="reset" class="btn btn-outline  btn-warning" data-dismiss="modal">Volver</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>