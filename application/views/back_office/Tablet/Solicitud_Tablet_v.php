<?php
/**
 * Tablet
 *
 * Description...
 *
 * @version 0.0.1
 *
 
 * Fecha creacion:	2018-03-09 [Jessica Roa] <jroa@edutecno.com>
 */
?>
    <style>
        /*select2*/

        .select2-container--open {
            z-index: 2060;
        }

        .swal2-container {
            z-index: 2061;
        }

        #ref_mod,
        #ref_mod_a,
        #ref_mod_dat,
        #ref_mod_dat_a {
            font-size: larger;
        }
        .skin-3 .asdf > li > a {
            background-color: #e6e6e6;
            opacity: 0.8;
        }
        .skin-3 .nav.nav-tabs > li.active > a {
            background-color: #fff;
        }
    </style>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <button type="button" id="btn_ingreso" class="btn btn-w-m btn-primary">Solicitud Tablet</button>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <ul class="nav nav-tabs asdf">
                            <li class="active">
                                <a data-toggle="tab" href="#home">Solicitudes</a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#menu1">Inventario</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <h3>Solicitudes</h3>
                                <div class="table-responsive">
                                    <table id="tbl_Solicitud_tablet" class="table table-striped table-bordered table-hover dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Nombre Solicitante</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Cantidad</th>
                                                <th>Modelo Tablet</th>
                                                <th>Aplicación</th>
                                                <th>Ficha</th>
                                                <th>Fecha Requerida</th>
                                                <th>Dirección de entrega</th>
                                                <th>Estado</th>
                                                <th>Observación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>N°</th>
                                                <th>Nombre Solicitante</th>
                                                <th>Fecha Solicitud</th>
                                                <th>Cantidad</th>
                                                <th>Modelo Tablet</th>
                                                <th>Aplicación</th>
                                                <th>Ficha</th>
                                                <th>Fecha Requerida</th>
                                                <th>Dirección de entrega</th>
                                                <th>Estado</th>
                                                <th>Observación</th>
                                                <th>Acción</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <h3>Inventario</h3>
                                <div class="table-responsive">
                                    <table id="tbl_Inventario_tablet" class="table table-striped table-bordered table-hover dataTables-example" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Descripción</th>
                                                <th>Cantidad en Stock</th>
                                                <th>Cantidad Solicitada</th>
                                                <th>Cantidad Necesaria</th>
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                <th>Marca</th>
                                                <th>Modelo</th>
                                                <th>Descripción</th>
                                                <th>Cantidad en Stock</th>
                                                <th>Cantidad Solicitada</th>
                                                <th>Cantidad Necesaria</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL INGRESAR TABLET -->
    <div class="modal inmodal in" id="IngresoModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-calculator modal-icon"></i>
                    <h4 class="modal-title">Registrar Solicitud de Tablet</h4>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal" id="frm_ingreso_solicitud">
                        <h4>Datos del ingreso</h4>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Motivo Solicitud:</label>
                            <div class="col-md-6">
                                <input type="radio" name="motivo_solicitud" id="venta" value="venta" required><label for="venta">Venta</label>
                                <input type="radio" name="motivo_solicitud" id="muestra" value="muesta"><label for="muesta">Muesta</label>
                                <input type="radio" name="motivo_solicitud" id="otro" value="otro"><label for="otro">Otro</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tablet:</label>
                            <div class="col-md-6">
                                <select id="cbx_modelo" name="cbx_modelo" class="select2_demo_3 form-control" required>
                                    <option></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Aplicación:</label>
                            <div class="col-md-6">
                                <select id="cbx_aplicacion" name="cbx_aplicacion" class="select2_demo_3 form-control" required>
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-md-1">
                                <button type="button" id="btn_agregar_aplicacion" class="btn btn-default dim"><i style="color: green;" class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ejecutivo:</label>
                            <div class="col-md-6">
                                <select id="cbx_ejecutivo" name="cbx_ejecutivo" class="select2_demo_3 form-control" required>
                                    <option></option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Cantidad:</label>
                            <div class="col-sm-4">
                                <input type="number" id="cantidad_tablet" name="cantidad_tablet" class="form-control input-md" step="1" min="0" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Dirección de entrega:</label>
                            <div class="col-sm-4">
                                <textarea id="direccion_entrega" name="direccion_entrega" cols="80" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <button id="btn_insert_solicitud" class="btn btn-primary pull-left">Guardar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <br>
                    </form>
                    <div class="modal-footer">
                        <button type="reset" id="reset_ingreso" class="btn btn-outline  btn-danger" data-dismiss="modal">Volver</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL editar direccion TABLET -->
    <div class="modal inmodal in" id="editardirModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-calculator modal-icon"></i>
                    <h4 class="modal-title">Editar dirección de entrega</h4>
                    <p id="ref_mod"></p>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal">
                        <input type="hidden" id="id_solicitud" name="id_solicitud" />
                        <h4>Detalles</h4>
                        <p id="ref_mod_dat"></p>
                        <br>
                        <br>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Dirección:</label>
                            <div class="col-sm-4">
                                <textarea id="direccion_editar" cols="80" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <button type="button" id="btn_editar_direccion" class="btn btn-danger">Editar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <br>
                    </form>
                    <div class="modal-footer">
                        <button type="reset" id="reset" class="btn btn-outline  btn-warning" data-dismiss="modal">Volver</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL anula solicitud -->
    <div class="modal inmodal in" id="anularModal" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-calculator modal-icon"></i>
                    <h4 class="modal-title">Anular Solicitud</h4>
                    <p id="ref_mod_a"></p>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal">
                        <input type="hidden" id="id_solicitud_a" name="id_solicitud_a" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Observación:</label>
                            <div class="col-sm-4">
                                <textarea id="observacion_solicitud" cols="80" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <button type="button" id="btn_anula_solicitud" class="btn btn-danger">Anular</button>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <br>
                    </form>
                    <div class="modal-footer">
                        <button type="reset" id="reset" class="btn btn-outline  btn-warning" data-dismiss="modal">Volver</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>