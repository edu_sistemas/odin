<?php
$perfil = "";
switch ($id_perfil) {
    case 13:
        $perfil = "Comercial";
        $show = "style='display:none'";
        $col = "col-md-12";
        $titulo = "Seguimientos Programados";
        $link = "/Seguimientos_programados";
        $link2 = "";
        break;

    case 27:
        $perfil = "Supervisor";
        $show = " style='display:'";
        $col = "col-md-6";
        $titulo = "Tareas";
        $link = "Tareas";
        $link2 = "";
        break;

    case 39:
        $perfil = "Tutor";
        $show = " style='display:'";
        $col = "col-md-6";
        $titulo = "Tareas";
        $link = "Tareas";
        $link2 = "/Graficos";
        break;

    default:
        $show = " style='display:'";
        $col = "col-md-6";
        $titulo = "Tareas";
        $link = "Tareas";
        $link2 = "";
        break;
}
?>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">


            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <div class="col-md-3">
                                <h5>Resumen Total</h5>
                            </div>							
                        </div>
                        <div class="ibox-content">						
                            <table id="tblResumenTotal" class="table toggle-arrow-tiny table-bordered">
                                <thead>
                                    <tr>
                                        <th>Holding</th>
                                        <th>Razón Social</th>
                                        <th>Autoinstruccional <br> Pendiente</th>
                                        <th>Autoinstruccional <br> OK</th>
                                        <th>Pendiente <br> Sence</th>
                                        <th>Pendiente <br> Beca</th>
                                        <th>Listos <br> Sence</th>
                                        <th>Listos <br> Beca</th>
                                        <th>D.J. <br> Pendiente</th>
                                        <th>D.J. <br> Lista</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($Resumen as $r) {
                                        echo '<tr>
										<td>' . $r['holding'] . '</td>
										<td>' . $r['empresa'] . '</td>
										<td>' . $r['auto_instrucional_pendiente'] . '</td>
										<td>' . $r['auto_instrucional_ok'] . '</td>
										<td>' . $r['pendiente_sence'] . '</td>
										<td>' . $r['pendiente_beca'] . '</td>
										<td>' . $r['listos_sence'] . '</td>
										<td>' . $r['listos_beca'] . '</td>
										<td>' . $r['dj_pendiente'] . '</td>
										<td>' . $r['dj_lista'] . '</td>
										</tr>';
                                    }
                                    ?>
                                </tbody>

                            </table>
                        </div>
                    </div>

                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <div class="col-md-3">
                                <h5>Buscador</h5>
                            </div>							
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="col-md-4 col-xs-12 mar-bot-15">
                                                <div>
                                                    <label for="">Buscar por RUT</label>
                                                    <input type="text" class="form-control input-xs" id="buscar-rut" placeholder="Ingrese Rut">
                                                    <!--<select class="form-control" id="buscar-rut"></select>-->
                                                </div>											
                                            </div>

                                            <div class="col-md-4 col-xs-12 mar-bot-15">
                                                <div>
                                                    <label for="">Buscar por Nombre</label>
                                                    <input type="text" class="form-control input-xs" id="buscar-nombre" placeholder="Ingrese Nombre">
                                                    <!--<select class="form-control" id="buscar-nombre"></select>-->
                                                </div>											
                                            </div>

                                            <div class="col-md-4 col-xs-12 mar-bot-15">
                                                <div>
                                                    <label for="">Buscar por Correo</label>
                                                    <input type="email" class="form-control" placeholder="Ingrese Correo" id="buscar-email">
                                                </div>	
                                            </div>

                                        </div>


                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-4 col-xs-12 mar-bot-15">												
                                                    <label for="">Buscar por OC</label>
                                                    <select class="form-control" id="buscar-oc">
                                                    </select>												
                                                </div>

                                                <div class="col-md-4 col-xs-12 mar-bot-15">
                                                    <label for="">Buscar por Ficha</label>
                                                    <select class="form-control" id="buscar-ficha">
                                                    </select>
                                                </div>

                                                <div class="col-md-4 col-xs-12 mar-bot-15">
                                                    <label for="">Buscar por Empresa</label>
                                                    <select class="form-control" id="buscar-empresa">
                                                    </select>
                                                </div>

                                                <div class="col-md-12">
                                                    <button class="btn btn-primary pull-right" id="btn-buscar">Buscar</button>
                                                </div>

                                            </div>
                                        </div>	
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>





                    <div class="modal inmodal" id="modalBuscador" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-xl2">
                            <div class="modal-content animated bounceInRight">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <i class="fa fa-search modal-icon"></i>
                                    <h4 class="modal-title">Resultados Busqueda</h4>
                                </div>
                                <div class="modal-body">

                                    <div class="ibox-content">
                                        <div class="table table-responsive">
                                            <table id="tblResultadoBusquedaEmpresa" class="footable table toggle-arrow-tiny table-bordered" data-page-size="10" data-filter="">
                                                <thead>
                                                    <tr>
                                                        <th>Empresa</th>
                                                        <th class="columnHide">Holding</th>
                                                        <th>Ficha</th>
                                                        <th>OC</th>
                                                        <th class="columnHide">Modalidad</th>
                                                        <th>Curso</th>
                                                        <th nowrap>Fecha Inicio</th>
                                                        <th nowrap>Fecha Término</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="8">
                                                            <ul class="pagination pull-right"></ul>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <br>
                                        <br>

                                        <br>
                                        <table id="tblResultadoBusquedaAlumno" class="footable table toggle-arrow-tiny table-bordered" data-page-size="10" data-filter="">
                                            <thead>
                                                <tr>
                                                    <th>Rut de <br>Alumno</th>
                                                    <th>Nombre</th>
                                                    <th>Costo</th>
                                                    <th class="columnHide">Conex</th>
                                                    <th class="columnHide">Eval</th>
                                                    <th>DJ</th>
                                                    <th>Situación</th>
                                                    <th class="columnHide">Status <br>Curso</th>
                                                    <th class="columnHide">CTS</th>
                                                    <th>VIP</th>
                                                    <th>Tutor</th>
                                                    <th data-sort-ignore="true"><i class="fa fa-search"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>18.021.570-0</td>
                                                    <td><a href="<?php echo base_url() . 'back_office/Contact_center/Comun/Detalle_alumno'; ?>">Esteban Valera</a></td>
                                                    <td>Empresa</td>
                                                    <td class="columnHide"></td>
                                                    <td class="columnHide"></td>
                                                    <td>Declaración Jurada</td>
                                                    <td>Completa</td>
                                                    <td class="columnHide">Completo</td>
                                                    <td class="columnHide">CTS</td>
                                                    <td>VIP</td>
                                                    <td>Gabriel Santibañez</td>
                                                    <td><i class="fa fa-search lupa" onclick="openEval()"></i></td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="12">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>




                    <!--MODAL 2-->

                    <div class="modal inmodal" id="modalEVAL" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content animated bounceInRight">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <i class="fa fa-search modal-icon"></i>
                                    <h4 class="modal-title">Detalle Eval</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="ibox-title">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label for="cboCursos">Seleccione Curso:</label>
                                                <select class="form-control input-xs" id="cboCursos" name="cboCursos"></select>
                                                <input type="hidden" id="aux_alumno">
                                                <input type="hidden" id="aux_user" value="<?php echo $id_user; ?>">

                                            </div>											
                                        </div>
                                    </div>
                                    <div class="ibox-content">

                                        <div class="row">
                                            <div class="col-md 12">
                                                <form role="form">
                                                    <h3 class="m-t-none m-b text-center">Alumno: <span id="deNombre"></span></h3>
                                                    <br>
                                                    <div class="col-md-4 b-r">

                                                        <div class="form-group"><label><small> Rut</small></label> <br> 
                                                            <label for="" id="deRut">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Curso</small></label> <br> 
                                                            <label for="" id="deCurso">
                                                                Link Moodle
                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Ficha</small></label> <br> 
                                                            <label for="" id="deFicha">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Ejecutivo</small></label> <br> 
                                                            <label for="" id="deEjecutivo">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Costo</small></label> <br> 
                                                            <label for="" id="deCosto">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Tiempo conexión</small></label> <br> 
                                                            <label for="" id="deHorasConexion">

                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4 b-r">
                                                        <div class="form-group"><label><small> Declaraciones Juradas</small></label> <br> 
                                                            <label for="" id="deDJ">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Avance Video</small></label> <br> 
                                                            <label for="" id="deAvanceVideo">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Primer Ingreso</small></label> <br> 
                                                            <label for="" id="dePrimerIngreso">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Último Ingreso</small></label> <br> 
                                                            <label for="" id="deUltimoIngreso">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Encuesta</small></label> <br> 
                                                            <label for="" id="deEncuesta">

                                                            </label>
                                                        </div>
                                                        <div class="form-group"><label><small> Evaluaciones Realizadas</small></label> <br> 
                                                            <label for="" id="deEvaluacionesRealizadas">

                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <table class="footable table toggle-arrow-tiny table-bordered" id="tblEvaluacionResultado" data-page-size="7" data-filter="">
                                                            <thead>
                                                                <tr>
                                                                    <th>Evaluación</th>
                                                                    <th>Resultado</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>											
                                                            <tfoot>
                                                                <tr>
                                                                    <td colspan="12">
                                                                        <ul class="pagination pull-right"></ul>
                                                                    </td>
                                                                </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-md-12">
                        <div class=" <?php echo $col; ?>  text-center b-r">
                            <p class="text-center">
                                <a href="<?php echo base_url() . 'back_office/Contact_center/' . $perfil . '/' . $link . ''; ?>"><i class="fa fa-file-text-o md-icon icon-buscador" ></i></a>
                            </p>    
                            <h4><?php echo $titulo; ?></h4>                             
                        </div>
                        <div class="col-md-6 text-center"  <?php echo $show; ?> >
                            <p class="text-center">
                                <a href="<?php echo base_url() . 'back_office/Contact_center/' . $perfil . $link2 . ''; ?>"><i class="fa fa-pie-chart md-icon icon-buscador" ></i></a>
                            </p>
                            <h4>Dashboard</h4>
                        </div>
                    </div>

                    <hr><hr><hr>



                </div>
            </div>
        </div>


    </div>





</div>

<input type="hidden" id="id_perfil" value="<?php echo $id_perfil; ?>">
<input type="hidden" id="id_user" value="<?php echo $id_user; ?>">