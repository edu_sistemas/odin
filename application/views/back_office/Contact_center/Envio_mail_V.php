<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="row">
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title  back-change">
                                <div class="col-md-3">
                                    <h5>Fichas</h5>
                                </div>
                                <input type="text" id="txtBuscarFicha" class="form-control" placeholder="Buscar"/>
                            </div>
                            <div class="ibox-content table-responsive no-padding" style="max-height: 500px;overflow-y: scroll;">
                                <table border="0" class="table table-striped no-margins" id="tbl_fichas">
                                    <thead>
                                        <tr>
                                            <th width="95%"></th>
                                            <th width="5%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 0; $i < count($fichas); $i++) { ?>
                                            <tr>
                                                <td><a class="btn-block btn-link" title="Tutor: <?php echo $fichas[$i]['nombre_tutor'] ?>" onclick="getAlumnosFicha(<?php echo $fichas[$i]['id'] ?>)"><?php echo $fichas[$i]['text'] ?></a></td>
                                                <td><input type="checkbox" name="chkFicha" id="chkFicha<?php echo $fichas[$i]['id'] ?>" value="<?php echo $fichas[$i]['id'] ?>" /></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title  back-change">
                                <div class="col-md-3">
                                    <h5>Alumnos</h5>
                                </div>
                                <input type="text" id="txtBuscarAlumnos" class="form-control" placeholder="Buscar"/>
                            </div>
                            <div class="ibox-content table-responsive no-padding" style="max-height: 500px;overflow-y: scroll;">
                                <table border="0" id="tblAlumnosFicha" class="table table-striped no-margins">
                                    <thead>
                                        <tr>
                                            <th width="95%"></th>
                                            <th width="5%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">

                <div class="ibox float-e-margins" id="botonera"  style="display: ">
                    <div class="ibox-title  back-change">
                        <div class="col-md-3">
                            <h5>Seleccione Acción</h5>
                        </div>
                    </div>
                    <div class="ibox-content">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6 text-center titleBotonera b-r">
                                    <p class="text-center">
                                        <i class="fa fa-envelope-o big-icon icon-correo" onclick="activaEmail()"></i>
                                    </p>    
                                    <h3>Envío de Correo</h3>                             
                                </div>
                                <div class="col-md-6 text-center titleBotonera">
                                    <p class="text-center">
                                        <i class="fa fa-newspaper-o big-icon icon-correo" onclick="activaGestion()"></i>
                                    </p>
                                    <h3>Dejar Gestión</h3>
                                </div>
                            </div>
                        </div>                        
                    </div>

                </div>


                <div class="ibox float-e-margins btnSeleccionado" id="correoInterno"  style="display: none">
                    <div class="ibox-title  back-change">
                        <div class="col-md-3">
                            <h5>E-Mail Interno</h5>
                        </div>                       
                    </div>
                    <div class="ibox-content table-responsive">
                        <div class="row">
                            <div class="col-md-6">
                                <select name="sltPlantillas" id="sltPlantillas" class="form-control col-md-3">
                                    <option selected="" disabled="">Seleccione Plantilla</option>
                                    <?php for ($i = 0; $i < count($plantillas); $i++) { ?>
                                        <option value="<?php echo $plantillas[$i + 2] ?>"><?php echo $plantillas[$i + 2] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <button class="pull-right btn btn-outline btn-primary dim btnVolver" onclick="volverBotonera()" type="button"><i class="fa fa-mail-reply"></i> Volver</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="txtCC" placeholder="Con Copia (Los correos deben ser separados por comas)" />
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="txtAsunto" placeholder="Asunto" />
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="summernote"></div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="pull-right">
                                    <button type="button" class="btn btn-primary" onclick="" id="btnEnviar">Enviar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox float-e-margins btnSeleccionado" id="correoExterno" style="display: none">
                    <div class="ibox-title  back-change">
                        <div class="col-md-3">
                            <h5>Dejar Gestión</h5>
                        </div>
                    </div>
                    <div class="ibox-content table-responsive">

                        <div class="col-md-12">
                            <button class="pull-right btn btn-outline btn-primary dim" onclick="volverBotonera()" type="button"><i class="fa fa-mail-reply"></i> Volver</button>
                        </div>

                        <div class="col-md-12">
                            <form id="form-llamado" class="form-horizontal">
                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="porque">Categoria 1:</label>
                                    <div class="col-sm-9">
                                        <select id="sltCategoria1" class="form-control">
                                            <option value="">Seleccione...</option>
                                            <?php
                                            for ($i = 0; $i < count($categoria1); $i++) {
                                                echo '<option value="'.$categoria1[$i]["id"].'">'.$categoria1[$i]["text"].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="script">Categoria 2:</label>
                                    <div class="col-sm-9">
                                        <select id="sltCategoria2" class="form-control">
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="pauta">Categoria 3:</label>
                                    <div class="col-sm-9">
                                        <select id="sltCategoria3" class="form-control">
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-3" for="comentario">Comentario:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="4" name="comentario" id="comentario"></textarea>
                                    </div>
                                </div>   

                                <div class="form-group">        
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="nocontactar" id="nocontactar">No Contactar</label>
                                        </div>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-md btn-primary pull-right" id="btnGuardarGestion">Guardar</button>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>