 <input type="hidden" id="perfil" name="perfil" value="<?php echo $id_perfil;?>"> 
 <!-- Valor temporal de Anexo (Extension) debe venir de la BBDD -->
 <input type="hidden" id="extension" name="extension" value="<?php echo $DatosAlumno[0]['anexo_edutecno'];?>"> 
 <input type="hidden" id="id_alumno" value="<?php echo $Alumno;?>">
 <input type="hidden" id="rut_alumno" value="<?php echo $DatosAlumno[0]['cp_rut'];?>">
 <input type="hidden" id="id_curso" value="<?php echo $Curso;?>">
 <input type="hidden" id="id_user" value="<?php echo $id_user;?>">
 <input type="hidden" id="Tareas" value="<?php echo $Tareas;?>">
 <input type="hidden" id="ip_elastix" value="<?php echo $this->config->item('ip_elastix');?>">

 <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-4">
            <div class="profile-image" style="text-align: center">
                <i class="fa fa-user fa-5x"></i>
                <!--<img src="img/a4.jpg" class="img-circle circle-border m-b-md" alt="profile">-->
            </div>
            <div class="profile-info">

                <div class="">
                    <div>
                        <h2 class="no-margins">
                            <?php echo $DatosAlumno[0]['nombre'];?>
                        </h2>
                        <h4><?php echo $DatosAlumno[0]['email1'];?></h4>
                    </div>
                    <div class="form-group"><label><small> Tutor:</small></label> <br> 
                        <label for="">
                            <?php echo $DatosAlumno[0]['tutor'];?>
                        </label>
                    </div> 
                    
                    <a id="btn-ant" class="btn btn-primary bt" title="Alumno Anterior"><i class="fa fa-arrow-left"></i></a>
                    <a id="btn-sig" class="btn btn-primary bt" title="Alumno Siguiente"><i class="fa fa-arrow-right"></i></a>
                    <small class="bt"><br>Navegación Tareas Asignadas</small>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <table class="table small m-b-xs">
                <tbody>
                    <tr>
                        <td>
                            <strong>Rut: </strong> <?php echo $DatosAlumno[0]['rut'];?>
                        </td>
                    </tr>
                    <tr>
                      <td>
                        <strong>Email Secundario: </strong> <?php echo $DatosAlumno[0]['email2'];?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <div class="form-group"><label> Observaciones:</label> <br> 
                        <label for="">
                            <?php echo $DatosAlumno[0]['observaciones'];?>
                        </label>
                    </div> 
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-md-3">
    <div class="text-center">
        <h3 class="no-margins">Alumno VIP</h3>
        <?php 
        if($DatosAlumno[0]['alumno_vip']==1)
        {
            echo '<i class="fa fa-star fa-5x" style="color: #e3e333;"></i>';
        }
        else
        {
            echo '<i class="fa fa-star-o fa-5x"></i>';
        }
        ;?>
        <br><br>
        <a href="<?php echo base_url('back_office/Contact_center/Comun/Detalle_alumno')."/".$Alumno;?>">Detalle Alumno <i class="fa fa-edit"></i></a>

    </div>
</div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-inline">
            <div class="form-group">
                <label for="cboCursos">Seleccione Curso:</label>
                <select class="form-control input-xs" id="cboCursos" name="cboCursos"></select>
                <input type="hidden" id="aux_alumno">
            </div>                                          
        </div>
    </div>
</div>

<br>

<div class="row">
    <div class="col-lg-4">
     <div class="ibox-title">
        <h5>Datos del Curso</h5>
        <div class="ibox-tools">
            <span id="tit_nc" title="Nuevo Curso"> <b><i class="fa fa-bell fa-2x text-success"></i></b></span>
        </div>
    </div>
    <div class="ibox">
        <div class="ibox-content">
            <div class="row">

                <div class="col-md-6">
                    <div class="form-group"><label><small> Curso</small></label> <br> 
                        <label for="" id="dcLink">

                        </label>
                    </div>
                    <div class="form-group"><label><small> Modalidad</small></label> <br> 
                        <label for="" id="dcModalidad">

                        </label>
                    </div>
                    <div class="form-group"><label><small> Ficha</small></label> <br> 
                        <label for="" id="dcFicha">

                        </label>
                    </div>
                    <div class="form-group"><label><small> Ejecutivo</small></label> <br> 
                        <label for="" id="dcEjecutivo">

                        </label>
                    </div>                   
                    <div class="form-group"><label><small> Costo Empresa</small></label> <br> 
                        <label for="" id="dcCosto">

                        </label>
                    </div>
                    <div class="form-group"><label><small> Tiempo conexión</small></label> <br> 
                        <label for="" id="dcHorasConexion">

                        </label>
                    </div>
                    <div class="form-group"><label><small> FUS</small></label> <br> 
                        <label for="" id="dcFus">

                        </label>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group"><label><small> Declaraciones Juradas</small></label> <br> 
                        <label for="" id="dcDJ">
                            Si
                        </label>
                    </div>
                    <div class="form-group"><label><small> Avance Video</small></label> <br> 
                        <label for="" id="dcAvanceVideo">

                        </label>
                    </div>
                    <div class="form-group"><label><small> Primer Ingreso</small></label> <br> 
                        <label for="" id="dcPrimerIngreso">

                        </label>
                    </div>
                    <div class="form-group"><label><small> Último Ingreso</small></label> <br> 
                        <label for="" id="dcUltimoIngreso">

                        </label>
                    </div>
                    <div class="form-group"><label><small> Encuesta</small></label> <br> 
                        <label for="" id="dcEncuesta">
                            Si
                        </label>
                    </div>
                    <div class="form-group"><label><small> Evaluaciones Realizadas</small></label> <br> 
                        <label for=""  id="dcEvaluacionesRealizadas">

                        </label>
                    </div>                
                </div>
            </div>




        </div>
    </div>
</div>

<div class="col-lg-5">
    <div class="ibox-title">
        <h5>Evaluaciones</h5>
    </div>
    <div class="ibox">
        <div class="ibox-content">
            <table class="footable table toggle-arrow-tiny table-bordered" id="tblEvaluaciones" data-page-size="8" data-filter="">
                <thead>
                    <tr>
                        <th>Evaluación</th>
                        <th>Resultado</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>                                            
                <tfoot>
                    <tr>
                        <td colspan="12">
                            <ul class="pagination pull-right"></ul>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div class="col-lg-3 m-b-lg">
 <div class="ibox-title">
    <h5>Teléfonos <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Para dejar gestión haga click sobre algún Teléfono."><i class="fa fa-info-circle"></i></a></h5>
</div>
<div class="ibox">
    <div class="ibox-content">
       <table id="tblTelefonos" class="table toggle-arrow-tiny" style="text-align: center; display: none; ">
        <tbody>
            <?php
            if(count($Telefonos)==0)
            {
                echo "<tr><a href='".base_url('back_office/Contact_center/Comun/Detalle_alumno/'.$Alumno)."'>Haga click aqui para agregar Teléfono</a></tr>";
            }
            else
            {
               foreach ($Telefonos as $t) 
               {
                if($t['id']==0)
                {
                    $icono="<i class='fa fa-star' style='color: #7AB900' title='Teléfono Ofimatica'></i>";
                }
                else
                {
                    $icono=($t['favorito']==1)?'<i class="fa fa-star" style="color: #e3e333"></i>':'<i class="fa fa-star-o"></i>';
                }
                
                echo '<tr>
                <td>'.$icono.'</td>
                <td nowrap="nowrap">'.$t['numero'].'</td>
                <td><a class="ancla" href="#divLlamar" data-ancla="divLlamar"><button id="'.$t['numero'].'" onClick="GenerarLlamado(\''.$t['numero'].'\')" class="btn btn-success btn-sm" title="Llamar"><i class="fa fa-phone"></i></button><a></td>
                </tr>';
            }
        }


        ?>

    </tbody>
</table>

<table id="tblTelefonos2" class="table toggle-arrow-tiny" style="text-align: center; display: none; ">
    <tbody>            
        <?php
        foreach ($Telefonos as $t) 
        {
            $icono=($t['favorito']==1)?'<i class="fa fa-star" style="color: #e3e333"></i>':'<i class="fa fa-star-o"></i>';
            echo '<tr>
            <td>'.$icono.'</td>
            <td nowrap="nowrap">'.$t['numero'].'</td>
            <td><button class="btn btn-sm btn-llamar disabled" title="Llamar"><i class="fa fa-phone"></i></button></td>
            </tr>';
        }
        ?>           
    </tbody>
</table>

</div>
</div>

</div>

</div>



<div class="row" id="divLlamar" style="display: none">
    <div class="col-lg-12 col-md-12">
        <div class="ibox-title">
            <h5>Llamar</h5>
        </div>
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">

                    <div class="col-sm-4 b-r">
                        <p class="text-center">
                            <i class="fa fa fa-phone big-icon icon-gestion"></i>
                        </p>
                        <p class="text-center">
                            <small class="fono-seleccionado-gestion">Teléfono seleccionado</small>
                        </p>
                        <br>
                        <?php if($DatosAlumno[0]['seguimiento']!="0")  
                        {
                            echo "<center><h2 class='text-danger'><i class='fa fa-bell-slash'></i> No Contactar</h2></center>";
                        }
                        ?>
                        <h2 id="fsel" class="text-center fono-seleccionado-gestion">+56 2 26598741</h2>
                    </div>

                    <div class="col-sm-8">
                        <h2 class="no-margins" style="text-align: right;">
                            <?php echo $DatosAlumno[0]['nombre'];?>
                        </h2>
                        <h4  style="text-align: right;">
                            <?php echo $DatosAlumno[0]['email1'];?>                                
                        </h4>

                        <form id="form-llamado" class="form-horizontal">
                            <input type="hidden" id="rut_alumno" name="rut_alumno" value="<?php echo $DatosAlumno[0]['cp_rut'];?>">
                            <input type="hidden" id="id_seguimiento" name="id_seguimiento" value="0">
                            <input type="hidden" id="accion" name="accion" value="1">
                            <input type="hidden" id="id_tarea" name="id_tarea" value="">
                            <input type="hidden" id="tarea_nuevo" name="tarea_nuevo" value="">
                            <input type="hidden" id="telefono" name="telefono" value="">
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="porque">¿Por Qué?:</label>
                                <div class="col-sm-9">
                                    <!--                                    <input type="text" disabled="disabled" class="form-control" id="dcPorque">-->
                                    <textarea disabled="disabled" class="form-control" id="dcPorque"></textarea>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="script">Script:</label>
                                <div class="col-sm-9">
                                    <!--                                    <input type="text" disabled="disabled" class="form-control" id="dcScript">-->
                                    <textarea disabled="disabled" class="form-control" id="dcScript"></textarea>
                                </div>
                            </div> 

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="pauta">Pauta Seguimiento:</label>
                                <div class="col-sm-9">
                                    <!--                                    <input type="text" disabled="disabled" class="form-control" id="dcPauta">-->
                                    <textarea disabled="disabled" class="form-control" id="dcPauta"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="">Categoría 1</label>
                                <div class="col-sm-9">
                                    <select name="categoria1" id="categoria1" class="form-control">

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="categoria2">Categoría 2</label>
                                <div class="col-sm-9">
                                    <select name="categoria2" id="categoria2" class="form-control">

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="categoria3">Categoría 3</label>
                                <div class="col-sm-9">
                                    <select name="categoria3" id="categoria3" class="form-control">

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-3" for="comentario">Comentario:</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="4" name="comentario" id="comentario"></textarea>
                                </div>
                            </div>   

        <!--
        <div class="form-group">        
        <div class="col-sm-offset-3 col-sm-9">

        <button class="btn btn-primary btn-sm" type="button">

        <?php 
        if($DatosAlumno[0]['seguimiento']=="0")
        {
            echo "&nbsp; &nbsp;";
        }
        else
        {
         echo "<i class='fa fa-check'></i> ";
     }
     ;?> 
     </button>
     No Contactar
     </div>
     </div>
 -->
 <button type="submit" class="btn btn-md btn-primary pull-right" id="btn-guardar">Guardar</button>  
 <button type="button" class="btn btn-md btn-white pull-right" onclick="ocultarModal();">Cerrar</button>
</form>


</div>

</div>        
</div>
</div>
</div>
</div>


<div class="row">
   <div class="col-lg-12 col-md-12">
       <div class="ibox-title">
           <h5>Seguimiento</h5>
       </div>
       <div class="ibox">
           <div class="ibox-content">
               <div class="table-responsive">
                   <table class="footable table toggle-arrow-tiny table-bordered table-responsive" id="tblSeguimiento" data-page-size="8" data-filter="">
                       <thead>
                           <tr>
                               <th>Origen</th>
                               <th>Script</th>
                               <th>Pauta Seguimiento</th>
                               <th>Comentario</th>
                               <th>Teléfono</th>
                               <th>Fecha</th>
                               <th>Tutor</th>
                               <?php
                               // when the user requests to remove comments 
                               // if($id_perfil!=13) { echo "<th>Acciones</th>";} 
                               ?>                               
                           </tr>
                       </thead>
                       <tbody>

                       </tbody>                                            
                       <tfoot>
                           <tr>
                               <td colspan="12">
                                   <ul class="pagination pull-right"></ul>
                               </td>
                           </tr>
                       </tfoot>
                   </table>
               </div>           
           </div>
       </div>
   </div>
</div>

</div>

<!-- Modal Telefonos -->
<div class="modal inmodal" id="mdlEditar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <i class="fa fa-edit modal-icon"></i>
                <h4 class="modal-title" id="titulo-modal">Editar Seguimiento</h4>
            </div>
            <div class="modal-body">
             <form id="form-llamado-edit" class="form-horizontal">
                <input type="hidden" id="rut_alumno" name="rut_alumno" value="">
                <input type="hidden" id="id_seguimientoEdit" name="id_seguimiento" value="">
                <input type="hidden" id="accion" name="accion" value="2">
                <input type="hidden" id="id_tarea" name="id_tarea" value="">
                <input type="hidden" id="tarea_nuevo" name="tarea_nuevo" value="">
                <input type="hidden" id="telefono" name="telefono" value="">

                <div class="form-group">
                    <label class="control-label col-sm-3" for="">Categoría 1</label>
                    <div class="col-sm-9">
                        <select name="categoria1" id="categoria1Edit" class="form-control">

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="categoria2">Categoría 2</label>
                    <div class="col-sm-9">
                        <select name="categoria2" id="categoria2Edit" class="form-control">

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="categoria3">Categoría 3</label>
                    <div class="col-sm-9">
                        <select name="categoria3" id="categoria3Edit" class="form-control">

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="comentario">Comentario:</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" rows="4" name="comentario" id="comentarioEdit"></textarea>
                    </div>
                </div>   
                <button type="submit" class="btn btn-md btn-primary pull-right" id="btn-guardar">Guardar</button>
                 <br>  
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
    </div>

</div>
</div>
