<?php 
$rut_corto=explode("-",$DatosAlumno[0]['cp_rut'])[0];
$rut_dv=$DatosAlumno[0]['cp_rut'];
;?>

<input type="hidden" id="id_usuario" value="<?php echo $alumno;?>">
<input type="hidden" id="rut_usuario" value="<?php echo $rut_dv;?>">
<div class="wrapper wrapper-content">
	<div class="row animated fadeInDown">
		<div class="col-lg-12">
			<div class="ibox-content" style="text-align: center;">
				<h2>Detalle Alumno</h2>
			</div>
			<div class="ibox-content">
				<div class="row">
					<div class="col-md-3 b-r">
						<table style="text-align: left;">
							<thead>
							</thead>
							<tbody>
								<tr>
									<h5>Nombre: </h5>
									<td style="text-align: left;">
										<h1><?php echo $DatosAlumno[0]['nombre'];?></h1>
									</td>
								</tr>							
								<tr>
									<td style="text-align: center;">
										<br><br>
										<h4>Marcar como VIP</h4>
										<br>
										<input type="hidden" id="estado_vip" value="<?php echo $DatosAlumno[0]['alumno_vip'];?>">
										<a href="#" id="btn-vip">
											<?php 
											if($DatosAlumno[0]['alumno_vip']==1)
											{
												echo '<i class="fa fa-star fa-5x" style="color: #e3e333;"></i>';
											}
											else
											{
												echo '<i class="fa fa-star-o fa-5x"></i>';
											}
											;?>
										</a>
									</td>
								</tr>
								<tr>
									<td style="text-align: center;">
										<hr>
										<h4 id="titNoContactar">
											<?php echo ($DatosAlumno[0]['seguimiento']==1?"No contactar":"Disponible para contacto");?>
										</h4>
										<input type="hidden" id="nocontactar" value="<?php echo $DatosAlumno[0]['seguimiento'];?>">
										<a href="#" id="btn-nocontactar">
											<?php 
											if($DatosAlumno[0]['seguimiento']==1)
											{
												echo '<i class="fa fa-bell-slash text-danger fa-3x"></i>';
											}
											else
											{
												echo '<i class="fa fa-phone text-primary fa-3x"></i>';
											}
											;?>
										</a>
									</td>
								</tr>																												
							</tbody>
						</table>
					</div>
					<div class="col-md-6 b-r">
						<table style="text-align: left;">
							<thead>
							</thead>
							<tbody>
								<tr>
									<td>
										<h5 class="m-t-none m-b">Rut: </h5>
										<h5><?php echo $DatosAlumno[0]['rut'];?></h5>
									</td>
								</tr>
								<tr>
									<td>
										<h5 class="m-t-none m-b">Mail: </h5>
										<h5 class="m-t-none m-b">
											<span id="txtMail"><?php echo $DatosAlumno[0]['email1'];?></span>
											<input type="hidden" id="txtMailOriginal" value="<?php echo $DatosAlumno[0]['email1'];?>">
											<button class="btn btn-xs btn-primary" id="btnECorreo"> <i class="fa fa-edit"></i></button>
										</h5>
									</td>										
								</tr>
							<!--
								<tr>
									<td>
										<h5 class="m-t-none m-b">Link Moodle: </h5>
										<h5 class="m-t-none m-b"><a>moodle.prueba.cl/alumno/25</a></h5>
									</td>
								</tr>
							-->
							<tr>
								<td>
									<h5 class="m-t-none m-b">Observación: <span id="txtObs"><?php echo $DatosAlumno[0]['observaciones'];?></span></h5> 
								</td>
							</tr>					
						</tbody>
					</table>
					<form id="form-observacion">
						<input type="hidden" name="id_seguimiento" id="id_seguimiento" value=""> 
						<input type="hidden" id="accion" name="accion" value="1">
						<input type="hidden" name="categoria1" id="categoria1" value="4">
						<input type="hidden" name="categoria2" id="categoria2" value="0">
						<input type="hidden" name="categoria3" id="categoria3" value="0">
						<input type="hidden" name="id_tarea" id="id_tarea" value="0">
						<input type="hidden" name="tarea_nuevo" id="tarea_nuevo" value="0">
						<input type="hidden" name="telefono" id="telefono" value="0">
						<input type="hidden" name="rut_alumno" id="rut_alumno" value="<?php echo $rut_corto;?>">
						<textarea id="comentario" name="comentario" placeholder="Ingrese Observación" style="width: 100%; border: 1px solid #aaaaaa; resize:none; height: 80px" ></textarea>
						<br>
						<button type="submit" class="btn btn-primary pull-right" id="btn-guardar">Guardar</button>
					</form>
				</div>
				<div class="col-md-3">
					<table id="tblTelefonos" class="table toggle-arrow-tiny" style="text-align: center;">
						<thead>
							<tr>
								<th></th>
								<th style="text-align: center;">Teléfono</th>
								<th style="text-align: center;"><a id="btn-add-fono" title="Agregar Número"><i class="fa fa-plus-circle"></i></a></th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="ibox ibox-content" style="margin-bottom: 0px; text-align: center;">
			<div class="row">
				<div class="col-md-12">
					<table class="table table-bordered table-" id="tblSeguimiento" style="width: 100%;">
						<thead>
							<tr>
								<th style="text-align: center">Empresa</th>
								<th style="text-align: center">Teletutor</th>
								<th style="text-align: center">Ficha</th>
								<th style="text-align: center">OC</th>
								<th style="text-align: center">Aplic.</th>
								<th style="text-align: center">Fecha ini</th>
								<th style="text-align: center">Fecha cierre</th>
								<th style="text-align: center">Eval. realiz.</th>
								<th style="text-align: center">Costo</th>
								<th style="text-align: center">Conex. Sence</th>
								<th style="text-align: center">Situación</th>
								<th style="text-align: center">Encuesta</th>
								<th style="text-align: center">Seguimiento</th>
								<th style="text-align: center">Status curso</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							foreach ($DatosCursos as $c) 
							{
								echo '
								<tr>
								<td>'.$c['empresa'].'</td>
								<td>'.$c['tutor'].'</td>
								<td>'.$c['ficha'].'</td>
								<td>'.$c['oc'].'</td>
								<td>'.$c['aplicacion'].'</td>
								<td NOWRAP>'.$c['fecha_inicio'].'</td>
								<td NOWRAP>'.$c['fecha_termino'].'</td>
								<td>'.$c['evaluaciones_realizadas'].'%</td>
								<td>'.$c['costo'].'</td>
								<td>'.$c['conexion_sence'].'</td>
								<td>'.$c['situacion'].'</td>
								<td>'.$c['encuesta'].'</td>
								<td style="text-align: center"><a href="'.base_url().'back_office/Contact_center/Comun/Gestion_alumnos/'.$alumno.'_'.$c['id_curso'].'"><i class="fa fa-search"></i></a></td>
								<td style="color: '.$c['color_status_curso'].'">'.$c['status_curso'].'</td>
								</tr>
								';	

							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>	

		<input type="hidden" id="perfil" name="perfil" value="<?php echo $id_perfil;?>"> 						

	</div>
</div>
</div>


<!-- Modal Telefonos -->
<div class="modal inmodal" id="mdlTelefono" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<i class="fa fa-phone modal-icon"></i>
				<h4 class="modal-title" id="titulo-modal"></h4>
			</div>
			<div class="modal-body">
				<input type="hidden" id="id_fono">
				<div class="form-inline text-center">
					<div class="form-group">
						<label for="nTelefono">Teléfono: </label>
						<input type="number" class="form-control" id="nTelefono">
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-primary" id="btn-save-fono">Guardar</button>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>

	</div>
</div>


<!-- Modal Email -->
<div class="modal inmodal" id="mdlEmail" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content animated bounceInRight">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<i class="fa fa-envelope modal-icon"></i>
				<h4 class="modal-title">Editar Email</h4>
			</div>
			<div class="modal-body">
				<div class="">
					<div class="form-group row">
						<label class="control-label col-sm-2" style="top: 10px;">Email:</label>
						<div class="col-md-8">
							<input type="email" class="form-control" id="dEmail">
						</div>
						<div class="col-lg-2">
								<button type="button" class="btn btn-primary" id="btn-save-email">Guardar</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			</div>
		</div>

	</div>
</div>