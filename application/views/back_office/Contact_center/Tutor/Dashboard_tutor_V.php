<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">


            <!--
            27 SUPER
            39 TELETUTOR
            13 EJECUTIVO COMERCIAL
            -->
            <input type="hidden" id="perfil" name="perfil" value="<?php echo $id_perfil; ?>"> 

            <div class="row">


                <div class="col-md-6">
                    <a href="<?php echo base_url() . 'back_office/Contact_center/Comun/Declaraciones_juradas'; ?>">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">

                                <h5>Declaraciones juradas</h5>                

                            </div>

                            <div class="ibox-content no-padding">

                                <div id="chartDeclaraciones" height="100">                      
                                </div>

                            </div>
                        </div>
                    </a>

                </div>

                <div class="col-md-6">
                    <a href="<?php echo base_url() . 'back_office/Contact_center/Comun/Conexion'; ?>">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">

                                <h5>Conexión</h5>                

                            </div>

                            <div class="ibox-content no-padding">

                                <div id="chartConectadas" height="400">                      
                                </div>

                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6">
                    <a href="<?php echo base_url() . 'back_office/Contact_center/Comun/Evaluaciones'; ?>">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">

                                <h5>Evaluaciones</h5>                

                            </div>

                            <div class="ibox-content no-padding">

                                <div id="chartEvaluaciones" height="100">                      
                                </div>

                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6">
                    <a href="<?php echo base_url() . 'back_office/Contact_center/Comun/Seguimiento'; ?>">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">

                                <h5>Seguimiento </h5>                

                            </div>

                            <div class="ibox-content no-padding">

                                <div id="chartSeguimiento" height="100">                      
                                </div>

                            </div>

                        </div>
                    </a>
                </div>

                <div class="col-md-6">
                    <a href="<?php echo base_url() . 'back_office/Contact_center/Comun/Llamadas_objetivo'; ?>">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">

                                <h5>Llamadas (Objetivo)</h5>                

                            </div>

                            <div class="ibox-content no-padding">

                                <div id="chartLlamados" height="100">                      
                                </div>

                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-md-6">
                    <a href="<?php echo base_url() . 'back_office/Contact_center/Comun/Respuestas'; ?>">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">

                                <h5>Respuestas / Total Gestiones</h5>                

                            </div>

                            <div class="ibox-content no-padding">

                                <div id="chartRespuestas" height="100">                      
                                </div>

                            </div>

                        </div>
                    </a>
                </div>

                <div class="col-md-12">
                    <a href="<?php echo base_url() . 'back_office/Contact_center/Comun/Llamadas_reemplazo'; ?>">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">

                                <h5>Llamadas mayores a 30 Segundos </h5>                

                            </div>

                            <div class="ibox-content no-padding">

                                <div id="chartLlamadaMayor" height="100">                      
                                </div>

                            </div>

                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

