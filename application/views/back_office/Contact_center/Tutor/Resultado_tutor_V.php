<?php
$hoy = date('Y-m-d');
$anterior = strtotime ( '-1 month' , strtotime ( $hoy ) ) ;
$anterior = date ( 'Y-m-d' , $anterior );
?>

<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <h5>Filtros</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3 mar-bot-15">
                                        <select class="form-control" id="id_empresa" name="id_empresa">
                                            <option selected value="">Seleccione Empresa</option>
                                            <?php
                                            for ($i = 0; $i < count($empresas_cbx); $i++) {
                                                if($empresas_cbx[$i]["id"] != 0)
                                                echo '<option value="' . $empresas_cbx[$i]["id"] . '">' . $empresas_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>									
                                        </select>
                                    </div>
                                    <div class="col-md-2 mar-bot-15">
                                        <select class="form-control" type="text" name="num_ficha" id="num_ficha">
                                            <option></option>
                                            <?php
                                            for ($i = 0; $i < count($ficha_cbx); $i++) {
                                                if ($ficha_cbx[$i]["id"] != 0)
                                                    echo '<option value="' . $ficha_cbx[$i]["id"] . '">' . $ficha_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>								
                                    <div class="col-md-3 mar-bot-15">
                                        <select class="form-control" type="text" name="num_oc" id="num_oc">
                                            <option></option>
                                            <?php
                                            for ($i = 0; $i < count($oc_cbx); $i++) {
                                                    echo '<option value="' . $oc_cbx[$i]["id"] . '">' . $oc_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group" id="data1" style="cursor: pointer;">
                                            <div class="input-daterange input-group date" id="">
                                                <input type="text" id="fecha_cierre_desde" name="fecha_cierre_desde" class="form-control" value="<?php echo $anterior; ?>" placeholder="Desde"/>
                                                <span class="input-group-addon">Fecha Cierre</span>
                                                <input type="text" id="fecha_cierre_hasta" name="fecha_cierre_hasta" class="form-control" value="<?php echo $hoy; ?>" placeholder="Hasta"/>
                                            </div>
                                        </div>
 <!-- 
<div class="col-md-6">
    <div class="form-group" id="data1" style="cursor: pointer;">
        <div class="input-group date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" id="fecha_cierre_desde" name="fecha_cierre_desde" class="form-control" value="<?php echo date("Y-m-d"); ?>"/>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="form-group" id="data2" style="cursor: pointer;">
        <div class="input-group date">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            <input type="text" id="fecha_cierre_hasta" name="fecha_cierre_hasta" class="form-control" value="<?php echo date("Y-m-d"); ?>"/>
        </div>
    </div>
</div>
  -->
                                    </div>
                                    <div class="col-md-12">
                                        <br>
                                        <button type="button" id="btnBuscar" class="btn btn-primary pull-right">Buscar</button>
                                    </div>
                                </div>							
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <table class=" table toggle-arrow-tiny table-bordered" id="tbl_resultados" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th rowspan="2">Tutor</th>
                                    <th colspan="2" style="text-align: center;">DJ (OK) 
                                        <a id="Ojo1" onclick="myModalDJ(1);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalle alumnos con DJ enviada."><i class="fa fa-eye"></i></a></th>
                                        <th colspan="2" style="text-align: center;">Conectividad (OK) 
                                            <a id="Ojo2" onclick="myModalConect(2);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalle alumnos con conectividad cumplida."><i class="fa fa-eye"></i></a></th>
                                            <th colspan="2" style="text-align: center;">Finalizados (OK) 
                                             <a id="Ojo3" onclick="myModalFin(3);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalle alumnos con todas sus evaluaciones finalizadas."><i class="fa fa-eye"></i></a></th>
                                             <th colspan="2" style="text-align: center;">Llamadas Mensuales</th>
                                             <th colspan="2" style="text-align: center;">Pedidos Especiales</th>
                                             <th colspan="2" style="text-align: center;">Extensiones  
                                               <a id="Ojo4" onclick="myModalExt(4);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalle extensiones por ficha."><i class="fa fa-eye"></i></a></th>
                                </tr>
                                <tr>
                                    <th style="text-align: center;">Cantidad Alumnos</th>
                                    <th style="text-align: center;">%</th>
                                    <th style="text-align: center;">Cantidad Alumnos</th>
                                    <th style="text-align: center;">%</th>
                                    <th style="text-align: center;">Cantidad Alumnos</th>
                                    <th style="text-align: center;">%</th>
                                    <th style="text-align: center;">Cantidad</th>
                                    <th style="text-align: center;">%</th>
                                    <th style="text-align: center;">Cantidad</th>
                                    <th style="text-align: center;">%</th>
                                    <th style="text-align: center;">Cantidad Fichas (OC)</th>
                                    <th style="text-align: center;">%</th>
                                </tr>						
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($resultados); $i++) {
                                    echo '<tr>'
                                    . '<td>' . $resultados[$i]["Nombre_Tutor"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Cant_Dj"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Dj"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Cant_Con"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Con"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Cant_Fin"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Fin"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Cant_LlamMen"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Pcje_LlamMen"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Cant_Ped"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Ped"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Cant_Ex"] . '</td>'
                                    . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Ex"] . '</td>'
                                    . '</tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="modal inmodal" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <h2>Declaración Jurada</h2>
                        </div>
                        <div class="modal-body">
                            <form class="form-horizontal" id="modal_dj" name="modal_dj" >		
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_djs">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">Ficha</th>
                                                        <th style="text-align: center;">OC</th>
                                                        <th style="text-align: center;">Empresa</th>
                                                        <th style="text-align: center;">Curso</th>
                                                        <th style="text-align: center;">Fecha ini.</th>
                                                        <th style="text-align: center;">Fecha cierre</th>
                                                        <th style="text-align: center;">Rut</th>
                                                        <th style="text-align: center;">Nombre</th>
                                                        <th style="text-align: center;">DJ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div> 
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <h2>Conectividad</h2>
                        </div>

                        <div class="modal-body">
                            <form class="form-horizontal" id="modal_conect" name="modal_conect" >		
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_con">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">Ficha</th>
                                                        <th style="text-align: center;">OC</th>
                                                        <th style="text-align: center;">Empresa</th>
                                                        <th style="text-align: center;">Curso</th>
                                                        <th style="text-align: center;">Fecha ini.</th>
                                                        <th style="text-align: center;">Fecha cierre</th>
                                                        <th style="text-align: center;">Rut</th>
                                                        <th style="text-align: center;">Nombre</th>
                                                        <th style="text-align: center;">Tiempo Conexión</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div> 
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content animated bounceInRight">

                        <div class="modal-header">
                            <h2>Finalizados</h2>
                        </div>

                        <div class="modal-body">
                            <form class="form-horizontal" id="reasignar_alumnos" name="reasignar_alumnos" >		
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_fin">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">Ficha</th>
                                                        <th style="text-align: center;">OC</th>
                                                        <th style="text-align: center;">Empresa</th>
                                                        <th style="text-align: center;">Curso</th>
                                                        <th style="text-align: center;">Fecha ini.</th>
                                                        <th style="text-align: center;">Fecha cierre</th>
                                                        <th style="text-align: center;">Rut</th>
                                                        <th style="text-align: center;">Nombre</th>
                                                        <th style="text-align: center;">% Eval</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: center;">13627</td>
                                                        <td style="text-align: center;">3659821</td>
                                                        <td style="text-align: center;">EDUTECNO</td>
                                                        <td style="text-align: center;">Excel Básico</td>
                                                        <td style="text-align: center;">05-05-2018</td>
                                                        <td style="text-align: center;">05-06-2018</td>
                                                        <td style="text-align: center;">12.653.985-k</td>
                                                        <td style="text-align: center;">Jimmy</td>
                                                        <td style="text-align: center;">35%</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;">16985</td>
                                                        <td style="text-align: center;">1236985</td>
                                                        <td style="text-align: center;">VMICA</td>
                                                        <td style="text-align: center;">Excel Intermedio</td>
                                                        <td style="text-align: center;">10-05-2018</td>
                                                        <td style="text-align: center;">10-07-2018</td>
                                                        <td style="text-align: center;">7.564.568-6</td>
                                                        <td style="text-align: center;">Esteban</td>
                                                        <td style="text-align: center;">43%</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;">14496</td>
                                                        <td style="text-align: center;">4499623</td>
                                                        <td style="text-align: center;">Hexagon</td>
                                                        <td style="text-align: center;">Excel Avanzado</td>
                                                        <td style="text-align: center;">17-06-2018</td>
                                                        <td style="text-align: center;">18-07-2018</td>
                                                        <td style="text-align: center;">14.257.698-1</td>
                                                        <td style="text-align: center;">Gabriel</td>
                                                        <td style="text-align: center;">57%</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div> 
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>	

            <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content animated bounceInRight">

                        <div class="modal-header">
                            <h2>Extensiones</h2>
                        </div>

                        <div class="modal-body">
                            <form class="form-horizontal" id="reasignar_alumnos" name="reasignar_alumnos" >		
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_ext">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">Ficha</th>
                                                        <th style="text-align: center;">OC</th>
                                                        <th style="text-align: center;">Empresa</th>
                                                        <th style="text-align: center;">Curso</th>
                                                        <th style="text-align: center;">Fecha ini.</th>
                                                        <th style="text-align: center;">Fecha cierre</th>
                                                        <th style="text-align: center;">Fecha Ext</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: center;">13627</td>
                                                        <td style="text-align: center;">3659821</td>
                                                        <td style="text-align: center;">EDUTECNO</td>
                                                        <td style="text-align: center;">Excel Básico</td>
                                                        <td style="text-align: center;">05-05-2018</td>
                                                        <td style="text-align: center;">05-06-2018</td>
                                                        <td style="text-align: center;">05-06-2018</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;">16985</td>
                                                        <td style="text-align: center;">1236985</td>
                                                        <td style="text-align: center;">VMICA</td>
                                                        <td style="text-align: center;">Excel Intermedio</td>
                                                        <td style="text-align: center;">10-05-2018</td>
                                                        <td style="text-align: center;">10-07-2018</td>
                                                        <td style="text-align: center;">05-06-2018</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center;">14496</td>
                                                        <td style="text-align: center;">4499623</td>
                                                        <td style="text-align: center;">Hexagon</td>
                                                        <td style="text-align: center;">Excel Avanzado</td>
                                                        <td style="text-align: center;">17-06-2018</td>
                                                        <td style="text-align: center;">18-07-2018</td>
                                                        <td style="text-align: center;">05-06-2018</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div> 
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

