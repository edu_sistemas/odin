<?php
if ($BonoActual == NULL) {
    $periodo = "-";
    $bono = 0;
    $bono_potencial = 0;
} else {
    $periodo = $BonoActual[0]["periodo"];
    $bono = $BonoActual[0]["bono"];
    $bono_potencial = $BonoActual[0]["bono_potencial"];
}
?>
<style>
#datatables_buttons_info {
    z-index: 9999;
}
</style>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title  back-change">
                    <h5>Mes Actual (<?php echo $periodo; ?>)</h5>
                </div>
                <div class="ibox-content" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">

                                <div class="row">
                                    <div class="col-md-2 col-md-offset-2"></div>
                                    <div class="col-md-4">
                                        <div class="widget style1 navy-bg">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <i class="fa fa-database fa-5x"></i>
                                                </div>
                                                <div class="col-xs-8 text-right">
                                                    <span>Actual</span>
                                                    <h2 class="font-bold">
                                                        $<?php echo number_format($bono, 0, ',', '.'); ?>
                                                    </h2>
                                                    <span>Meta</span>
                                                    <h2 class="font-bold">
                                                        $<?php echo number_format($bono_potencial, 0, ',', '.'); ?>
                                                    </h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <hr>

                            <div class="row">
                                <div id="bonoMen" style="min-width: 310px; height: 400px; margin: 0 auto">
                                    <center>

                                        <h1 style="padding-top: 10%;"><i class="fa fa-refresh fa-spin fa-3x"></i></h1>
                                        Cargando Datos...
                                    </center>
                                </div>
                            </div>

                        </div>
                        <!--<button class="pull-right btn btn-primary" onclick="detalleBono()">Ver Detalle</button>-->
                    </div>
                </div>
            </div>


            <div class="ibox float-e-margins">

                <div class="ibox-title  back-change">
                    <h5>Resultado</h5>
                </div>

                <div class="ibox-content" id="divResultado">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-md-12">


                                    <div class="table-responsive">
                                        <table id="tblResultado" class="table toggle-arrow-tiny table-bordered" style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <th>Tutor</th>
                                                    <th>Bono</th>
                                                    <th>Bono Potencial</th>
                                                    <th>Bono</th>
                                                    <th>Bono Potencial</th>
                                                    <th>Periodo</th>
                                                    <th></th>
                                                    <!--
                                                    <th data-hide="all">Ficha</th>
                                                    <th data-hide="all">OC</th>
                                                    <th data-hide="all">Modalidad</th>
                                                    <th data-hide="all">Fecha Ini</th>
                                                    <th data-hide="all">Fecha Fin</th>
                                                    <th data-hide="all">Costo</th>
                                                    <th data-hide="all">DJ</th>
                                                    <th data-hide="all">Conexión</th>
                                                    <th data-hide="all">Segmento</th>
                                                    <th data-hide="all">Ind</th>
                                                    <th data-hide="all">Seleccionar</th>
                                                    -->
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal inmodal" id="modalDetalle" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content animated bounceInRight">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                                <i class="fa fa-address-card modal-icon"></i>
                                <h4 class="modal-title">Detalle por alumno</h4>
                            </div>
                            <div class="modal-body">

                                <div class="ibox-content">
									
                                            <table id="tblAlumnosporTutor" class="table toggle-arrow-tiny table-bordered" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th>Bono</th>
                                                        <th>Bono</th>
                                                        <th>Tutor</th>
                                                        <th>Rut</th>
                                                        <th>Nombre</th>
                                                        <th>Ficha</th>
                                                        <th>Modalidad</th>
                                                        <th>Fecha Inicio</th>
                                                        <th>Fecha Fin</th>
                                                        <th>Costo</th>
                                                        <th>Declaraciones <br>Juradas</th>
                                                        <th>Conexión</th>
                                                        <th>Eval. (%)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>		



        </div>	
    </div>
</div>

<input type="hidden" id="id_tutor" value="<?php echo $id_tutor; ?>">
