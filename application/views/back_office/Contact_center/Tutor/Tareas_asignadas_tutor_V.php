<?php
if($tareas==null)
{
    $fini="-";
    $ffin="-";
}
else
{
  $fini=$tareas[0]["fecha_desde"];
  $ffin=$tareas[0]["fecha_hasta"];
}

?>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <h5>Filtros</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-8">
                                        <select class="form-control" id="empresa" name="empresa" requerido="true" mensaje="Seleccione Empresa">
                                            <option value="" selected>Seleccione Empresa</option>
                                            <?php
                                            for ($i = 0; $i < count($empresas_cbx); $i++) {
                                                if ($empresas_cbx[$i]["id"] > 0)
                                                    echo '<option value="' . $empresas_cbx[$i]["id"] . '">' . $empresas_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control" id="filt_modalidad" name="filt_modalidad" requerido="true" mensaje="Seleccione Modalidad">
                                            <option value="" selected>Seleccione Modalidad</option>
                                            <?php
                                            for ($i = 0; $i < count($modalidades_cbx); $i++) {
                                                echo '<option value="' . $modalidades_cbx[$i]["id"] . '">' . $modalidades_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <select class="form-control" id="filt_tcosto" name="filt_tcosto" requerido="true" mensaje="Seleccione tipo costo">
                                            <option value='' selected>Seleccione Costo</option>
                                            <?php
                                            for ($i = 0; $i < count($costos_cbx); $i++) {
                                                echo '<option value="' . $costos_cbx[$i]["id"] . '">' . $costos_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>	
                                    <div class="col-md-3">
                                        <!--
                                        <input class="form-control" type="number" name="inp_ficha" id="inp_ficha" requerido="true" placeholder="Ficha">
                                    -->
                                    <select class="form-control" name="inp_ficha" id="inp_ficha" requerido="true" placeholder="Ficha">
                                        <option value="" selected>Seleccione Ficha</option>
                                        <?php 
                                        foreach ($ficha_cbx as $f ) {
                                            echo "<option value='".$f['id']."'>".$f['text']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>						
                                <div class="col-md-3">
                                        <!--
                                        <input class="form-control" type="number" name="inp_oc" id="inp_oc" requerido="true" placeholder="OC">
                                    -->
                                    <select class="form-control" name="inp_oc" id="inp_oc" requerido="true" placeholder="Orden de Compra">
                                        <option value="" selected>Seleccione Orden de Compra</option>
                                        <?php 
                                        foreach ($oc_cbx as $o ) {
                                            echo "<option value='".$o['id']."'>".$o['text']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" id="curso" name="curso" class="form-control" placeholder="Nombre Curso"/>
                                </div>

                                <div class="col-md-12">
                                    <br>
                                    <button type="button" id="btnBuscar" class="btn btn-primary pull-right">Buscar</button>
                                </div>
                            </div>							
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1">Tareas</a></li>
                <li class=""><a data-toggle="tab" href="#tab-2">Tareas (Nuevos)</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <table class="table table-bordered no-padd-table" cellpadding="0" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Fecha Inicio Tareas</th>
                                            <th>Fecha Fin Tareas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $fini;?></td>
                                            <td><?php echo $ffin;?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-8">
                                <table class="table toggle-arrow-tiny table-bordered" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th  style="text-align: center;">DJ Listas</th>
                                            <th  style="text-align: center;">Conect. Listo</th>
                                            <th  style="text-align: center;">Evaluaciones</th>				
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $estadisticas_tareas[0]['porcen'] . "%"; ?></td>
                                            <td style="text-align: center;"><?php echo $estadisticas_tareas[1]['porcen'] . "%"; ?></td>
                                            <td style="text-align: center;"><?php echo $estadisticas_tareas[2]['porcen'] . "%"; ?></td>					
                                        </tr>				
                                    </tbody>
                                </table>
                            </div>		
                            <div class="col-md-12">
                                <table class="table toggle-arrow-tiny table-bordered  no-padd-table" id="tbl_tareas" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Rut</th>
                                            <th>Nombre</th>
                                            <th>Ficha</th>
                                            <th>OC</th>
                                            <th>Modal.</th>
                                            <th>Fecha Desde</th>
                                            <th>Fecha Hasta</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Fecha Cierre</th>
                                            <th>Costo</th>
                                            <th>DJ</th>
                                            <th>Conex.</th>
                                            <th>Seg.</th>
                                            <th>Ind.</th>
                                            <th>Ver</th>	
                                            <th>Mail</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        for ($i = 0; $i < count($tareas); $i++) {
                                            echo '<tr>
                                            <td>' . ($i + 1) . '</td>
                                            <td>' . $tareas[$i]['rut_dv'] . '</td>
                                            <td>' . $tareas[$i]['nombre_apellido'] . '</td>
                                            <td>' . $tareas[$i]['ficha'] . '</td>
                                            <td>' . $tareas[$i]['orden_compra'] . '</td>
                                            <td width="100">' . $tareas[$i]['cp_modalidad'] . '</td>
                                            <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_desde"] . '</td>
                                            <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_hasta"] . '</td>
                                            <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_inicio"] . '</td>
                                            <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_fin"] . '</td>
                                            <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_cierre"] . '</td>
                                            <td>' . $tareas[$i]['cp_costo'] . '</td>
                                            <td>' . $tareas[$i]['dj'] . '</td>
                                            <td>' . round($tareas[$i]['Tiempo_conexion'], 2) . '</td>
                                            <td style="text-align: center">' . $tareas[$i]['seguimiento'] . '</td> <!-- Omar lo la a enviar -->
                                            <td style="text-align: center"><i class="fa fa-circle fa-lg" style="color: #' . $tareas[$i]['indicador'] . '" title="'.$tareas[$i]['desc_tipo_tarea'].'"></i></td>
                                            <td style="text-align: center"><a href="../Comun/Detalle_alumno/' . $tareas[$i]['cp_idUsuario'] . '"><i class="fa fa-search"></i></a></td>
                                            <td style="text-align: center"><button type="button" class="btn btn-link btn-xs" onclick="setMailToSend(' . $tareas[$i]['id_tarea'] . ',\'' . $tareas[$i]['nombre_apellido'] . '\', \'' . $tareas[$i]['mail'] . '\', \'' . base64_encode($tareas[$i]['nombre_curso']) . '\', \'' . $tareas[$i]['fecha_termino_curso'] . '\')"><i class="fa fa-envelope"></i></button></td>
                                            <td><i class="fa fa-thumbs-up" title="'.($tareas[$i]['id_estado_tarea'] == 2 ? "Contactado" : "NO contactado").'" style="font-size:20px;color:'.($tareas[$i]['id_estado_tarea'] == 2 ? "green" : "gray").';" aria-hidden="true"></i></td>
                                            </tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>					
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <br>
                                <table class="table toggle-arrow-tiny table-bordered" style="width: 100%;" >
                                    <thead>
                                        <tr>
                                            <th  style="text-align: center;">DJ Listas</th>
                                            <th  style="text-align: center;">Conect. Listo</th>
                                            <th  style="text-align: center;">Evaluaciones</th>				
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align: center;"><?php echo $estadisticas_tareas_nuevas[0]['porcen'] . "%"; ?></td>
                                            <td style="text-align: center;"><?php echo $estadisticas_tareas_nuevas[1]['porcen'] . "%"; ?></td>
                                            <td style="text-align: center;"><?php echo $estadisticas_tareas_nuevas[2]['porcen'] . "%"; ?></td>					
                                        </tr>				
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12">
                                <br>
                                <table class="table toggle-arrow-tiny table-bordered" id="tbl_tareas_nuevo" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Rut</th>
                                            <th>Nombre</th>
                                            <th>Ficha</th>
                                            <th>OC</th>
                                            <th>Modal.</th>
                                            <th>Fecha Encuentro</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Fecha Cierre</th>
                                            <th>Costo</th>
                                            <th  class="none" style="background-color: #f5f5f6;">DJ</th>
                                            <th  class="none" style="background-color: #f5f5f6;">Conex.</th>
                                            <th style="background-color: #f5f5f6;">Seg.</th>
                                            <th  class="none">Ind.</th>
                                            <th class="none">Ver</th>
                                            <th class="none">Mail</th>	
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        for ($i = 0; $i < count($tareas_nuevas); $i++) {
                                            echo '<tr>
                                            <td>' . ($i + 1) . '</td>
                                            <td>' . $tareas_nuevas[$i]['rut_dv'] . '</td>
                                            <td>' . $tareas_nuevas[$i]['nombre_apellido'] . '</td>
                                            <td>' . $tareas_nuevas[$i]['ficha'] . '</td>
                                            <td>' . $tareas_nuevas[$i]['orden_compra'] . '</td>
                                            <td width="100">' . $tareas_nuevas[$i]['cp_modalidad'] . '</td>
                                            <td width="110">' . $tareas_nuevas[$i]['fecha_encuentro'] . '</td>
                                            <td width="110">' . $tareas_nuevas[$i]['fecha_inicio'] . '</td>
                                            <td width="110">' . $tareas_nuevas[$i]['fecha_fin'] . '</td>
                                            <td width="110">' . $tareas_nuevas[$i]['fecha_cierre'] . '</td>
                                            <td>' . $tareas_nuevas[$i]['cp_costo'] . '</td>
                                            <td>' . $tareas_nuevas[$i]['dj'] . '</td>
                                            <td>' . /*$tareas_nuevas[$i]['Tiempo_conexion'] .*/ '</td>
                                            <td style="text-align: center">' . $tareas_nuevas[$i]['seguimiento'] . '</td> <!-- Omar lo la a enviar -->
                                            <td style="text-align: center"><i class="fa fa-circle fa-lg" style="color: #' . $tareas_nuevas[$i]['indicador'] . '"></i></td>
                                            <td style="text-align: center"><a href="../Comun/Detalle_alumno/' . $tareas_nuevas[$i]['cp_idUsuario'] . '"><i class="fa fa-search"></i></a></td>
                                            <td style="text-align: center"><button type="button" class="btn btn-link btn-xs" onclick="setMailToSend(' . $tareas_nuevas[$i]['id_tarea_nuevo'] . ',\'' . $tareas_nuevas[$i]['nombre_apellido'] . '\', \'' . $tareas_nuevas[$i]['mail'] . '\')"><i class="fa fa-envelope"></i></button></td>	
                                            <td><i class="fa fa-thumbs-up" title="'.($tareas_nuevas[$i]['id_estado_tarea'] == 2 ? "Contactado" : "NO contactado").'" style="font-size:20px;color:'.($tareas_nuevas[$i]['id_estado_tarea'] == 2 ? "green" : "gray").';" aria-hidden="true"></i></td>
                                            </tr>'; 
                                        }
                                        ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>


<div class="modal inmodal" id="mdlMail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-envelope modal-icon"></i>
                <h4 class="modal-title">Enviar Mail a <p id="txtMail"></p></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <select name="sltPlantillas" id="sltPlantillas" class="form-control col-md-3">
                            <option selected="" value="">Seleccione Plantilla</option>
                            <?php for ($i = 0; $i < count($plantillas); $i++) { ?>
                                <option value="<?php echo $plantillas[$i + 2] ?>"><?php echo $plantillas[$i + 2] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <br>
                <input type="hidden" value="" id="hdnTarea" />
                <div class="row">
                    <div class="col-md-6">
                        <input type="email" class="form-control" id="hdnMail" value="" placeholder="Correo Electrónico"/>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="form-control" id="txtCC" placeholder="Con Copia (Los correos deben ser separados por comas)" />
                    </div>
                </div>
                <br>
                 <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="form-control" id="txtAsunto" placeholder="Asunto" />
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div id="summernote"></div>
                    </div>
                </div>
                <br>
                <input type="hidden" id="hdnNombreCurso" value="" />
                <input type="hidden" id="hdnFechaTerminoCurso" value="" />

                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <button type="button" class="btn btn-primary" onclick="" id="btnEnviar">Enviar</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
        </div> 
    </div>
    </div
</div>
