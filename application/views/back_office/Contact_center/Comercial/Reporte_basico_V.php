<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">

            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <table class="table toggle-arrow-tiny table-bordered">
                            <thead>
                                <tr>
                                    <th>Empresa</th>
                                    <th>Ficha</th>
                                    <th>Aplicación</th>
                                    <th>Fecha inicio</th>
                                    <th>Fecha cierre</th>
                                    <th>Nombre</th>
                                    <th>RUT</th>
                                    <th>Costo</th>
                                    <th>% Avance</th>
                                    <th>Tiempo de Conexión</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

