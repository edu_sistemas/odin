<style type="text/css">
.swal2-container {
  z-index: 10000;
}

#datatables_buttons_info {
    z-index: 9999;
}
</style>

<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <h5>Filtros</h5>
                        </div>
                        <div class="ibox-content form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3 mar-bot-15">
                                        <select class="form-control" id="empresa" name="empresa" requerido="true" mensaje="Seleccione Empresa" placeholder="Seleccione Empresa">
                                            <option selected value="">Seleccione Empresa</option>
                                            <?php
                                            for ($i = 0; $i < count($empresas_cbx); $i++) {
                                                echo '<option value="' . $empresas_cbx[$i]["id"] . '">' . $empresas_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3 mar-bot-15">
                                        <select class="form-control" id="curso" name="curso" requerido="true" mensaje="Seleccione Curso" placeholder="Seleccione Curso">
                                            <option selected value="">Seleccione Curso</option>
                                            <?php
                                            for ($i = 0; $i < count($cursos_cbx); $i++) {
                                                echo '<option value="' . $cursos_cbx[$i]["id"] . '">' . $cursos_cbx[$i]["text"] . ' (ID: '.$cursos_cbx[$i]["id"].')'  . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group" id="data1" style="cursor: pointer;">
                                            <div class="input-daterange input-group date" id="">
                                                <input type="text" id="fecha_desde" name="fecha_desde" class="form-control" value="<?php echo date("Y-m-01"); ?>" placeholder="Desde"/>
                                                <span class="input-group-addon">Fecha Cierre</span>
                                                <input type="text" id="fecha_hasta" name="fecha_hasta" class="form-control" value="<?php echo date("Y-m-d"); ?>" placeholder="Hasta"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2 mar-bot-15">
                                        <!--
                                        <input class="form-control" type="text" name="inp_ficha" id="inp_ficha" requerido="true" placeholder="Ficha">
                                    -->
                                    <select class="form-control" name="inp_ficha" id="inp_ficha" requerido="true" placeholder="Seleccione Ficha">
                                        <option value="" selected>Seleccione Ficha</option>
                                        <?php 
                                        foreach ($fichas as $f ) {
                                            echo "<option value='".$f['id']."'>".$f['text']."</option>";
                                        }
                                        ?>
                                    </select>
                                </div>	
                            </div>							
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3 mar-bot-15">
                                    <!--
                                    <input class="form-control" type="text" name="inp_oc" id="inp_oc" requerido="true" placeholder="OC">
                                -->
                                <select class="form-control" name="inp_oc" id="inp_oc" requerido="true" placeholder="OC">
                                    <option value="" selected>Seleccione Orden de Compra</option>
                                    <?php 
                                    foreach ($ocs as $o ) {
                                        echo "<option value='".$o['id']."'>".$o['text']."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-3 mar-bot-15">
                                <input class="form-control" type="text" name="holding" id="holding" requerido="true" placeholder="Holding">
                            </div>
                            <div class="col-md-3 mar-bot-15">
                                <input class="form-control" type="number" name="proceso" id="proceso" min="1900" requerido="true" placeholder="Proceso">
                            </div>							
                            <div class="col-md-3">
                                <select class="form-control" id="ejec_comercial" name="ejec_comercial" requerido="true" mensaje="Seleccione Ejec Comercial">
                                    <option selected value="">Seleccione Ejec. Comercial</option>
                                    <?php
                                    for ($i = 0; $i < count($ejecutivos_cbx); $i++) {
                                        echo '<option value="' . $ejecutivos_cbx[$i]["id"] . '">' . $ejecutivos_cbx[$i]["text"] . '</option>';
                                    }
                                    ?>									
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button type="button" id="btnBuscar" class="btn btn-primary pull-right">Buscar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <div class="col-md-12">
                <br>
                <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_avance_cursos">
                    <thead>
                        <tr>
                            <th>Nombre Apellido</th>
                            <th>RUT</th>
                            <th>Aplicación</th>
                            <th>Fecha inicio</th>
                            <th>Fecha cierre</th>
                            <th>Costo</th>
                            <th>% Avance</th>
                            <th>Tiempo conex.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for ($i = 0; $i < count($avances_inicio); $i++) {
                            echo '<tr>'
                            . '<td>' . $avances_inicio[$i]["nombre_apellido"] . '</td>'
                            . '<td>' . $avances_inicio[$i]["rut"] . '</td>'
                            . '<td>' . $avances_inicio[$i]["nombre_curso"] . '</td>'
                            . '<td>' . $avances_inicio[$i]["fecha_inicio"] . '</td>'
                            . '<td>' . $avances_inicio[$i]["fecha_fin"] . '</td>'
                            . '<td>' . $avances_inicio[$i]["costo"] . '</td>'
                            . '<td>' . $avances_inicio[$i]["porcentaje_avance_video"] . '</td>'
                            . '<td>' . round($avances_inicio[$i]["tiempo_conexion"], 2) . '</td>'
                            . '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="ibox-content">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6 buttonAvance1 mar-bot-15">
                    <form action="Reporte_basico"> 
                        <button type="button" id="btnReporteBasico" class="btn btn-primary">Generar reporte básico</button>
                    </form>
                </div>
                <div class="col-md-6 buttonAvance2 mar-bot-30">
                    <form action="#"> 
                        <button type="button" id="btnReporteCompleto" class="btn btn-primary">Generar reporte completo</button>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <div class="modal inmodal" id="mdlReporteCompleto" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h2>Campos a mostrar</h2>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="reasignar_alumnos" name="reasignar_alumnos" >		
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table toggle-arrow-tiny table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Seleccione campos a exportar</th>
                                                <th><input type="checkbox" id="chkSeleccionarTodo">  Seleccionar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr><td>C.T.S</td><td><input  type="checkbox" name="chkCampo" id="chkCts"></td></tr>
                                            <tr><td>Correo</td><td><input  type="checkbox" name="chkCampo" id="chkCorreo"></td></tr>
                                            <tr><td>Costo</td><td><input  type="checkbox" name="chkCampo" id="chkCosto"></td></tr>
                                            <tr><td>Curso</td><td><input  type="checkbox" name="chkCampo" id="chkCurso"></td></tr>
                                            <tr><td>Código sence</td><td><input  type="checkbox" name="chkCampo" id="chkCodSence"></td></tr>
                                            <tr><td>Declaración Jurada</td><td><input  type="checkbox" name="chkCampo" id="chkDJ"></td></tr>
                                            <tr><td>Empresa</td><td><input  type="checkbox" name="chkCampo" id="chkEmpresa"></td></tr>
                                            <tr><td>Evaluación</td><td><input  type="checkbox" name="chkCampo" id="chkEvaluacion"></td></tr>
                                            <tr><td>Ficha</td><td><input  type="checkbox" name="chkCampo" id="chkFicha"></td></tr>
                                            <tr><td>Fecha Inicio</td><td><input  type="checkbox" name="chkCampo" id="chkFechaInicio"></td></tr>
                                            <tr><td>Fecha Fin</td><td><input  type="checkbox" name="chkCampo" id="chkFechaFin"></td></tr>
                                            <tr><td>Fecha Cierre</td><td><input  type="checkbox" name="chkCampo" id="chkFechaCierre"></td></tr>
                                            <tr><td>F.U.S</td><td><input  type="checkbox" name="chkCampo" id="chkFUS"></td></tr>
                                            <tr><td>Fono</td><td><input  type="checkbox" name="chkCampo" id="chkFono"></td></tr>
                                            <tr><td>Holding</td><td><input  type="checkbox" name="chkCampo" id="chkHolding"></td></tr>
                                            <tr><td>Horas curso</td><td><input  type="checkbox" name="chkCampo" id="chkHrsCurso"></td></tr>
                                            <tr><td>ID sence</td><td><input  type="checkbox" name="chkCampo" id="chkIdSence"></td></tr>
                                            <tr><td>Indicador</td><td><input  type="checkbox" name="chkCampo" id="chkIndicador"></td></tr>
                                            <tr><td>Nombre</td><td><input  type="checkbox" name="chkCampo" id="chkNombre"></td></tr>
                                            <tr><td>Orden de Compra</td><td><input  type="checkbox" name="chkCampo" id="chkOC"></td></tr>
                                            <tr><td>Primer Acceso</td><td><input  type="checkbox" name="chkCampo" id="chkPrimerAcceso"></td></tr>
                                            <tr><td>RUT</td><td><input  type="checkbox" name="chkCampo" id="chkRut"></td></tr>
                                            <tr><td>Registro Sence</td><td><input  type="checkbox" name="chkCampo" id="chkRegistroSence"></td></tr>
                                            <tr><td>Sucursal</td><td><input  type="checkbox" name="chkCampo" id="chkSucursal"></td></tr>
                                            <tr><td>Situación</td><td><input  type="checkbox" name="chkCampo" id="chkSituacion"></td></tr>
                                            <tr><td>Seguimientos</td><td><input  type="checkbox" name="chkCampo" id="chkSeguimientos"></td></tr>
                                            <tr><td>Tiempo conexión</td><td><input  type="checkbox" name="chkCampo" id="chkTiempoConexion"></td></tr>
                                            <tr><td>Tutor asignado</td><td><input  type="checkbox" name="chkCampo" id="chkTutor"></td></tr>
                                            <tr><td>Último Acceso</td><td><input  type="checkbox" name="chkCampo" id="chkUltimoAcceso"></td></tr>
                                            <tr><td>Versión</td><td><input  type="checkbox" name="chkCampo" id="chkVersion"></td></tr>
                                            <tr><td>% Avance Video</td><td><input  type="checkbox" name="chkCampo" id="chkPrctjeAvanceVideo"></td></tr>
                                            <tr><td>% Evaluaciones Realizadas</td><td><input  type="checkbox" name="chkCampo" id="chkPrctjeEvalRealizada"></td></tr>
                                            <tr><td>% Franquicia</td><td><input  type="checkbox" name="chkCampo" id="chkPrctjeFranquicia"></td></tr>
                                            <tr><td>% Registro Sence</td><td><input  type="checkbox" name="chkCampo" id="chkPrctjeRegistroSence"></td></tr>
                                            <tr><td>% Tiempo conexión</td><td><input  type="checkbox" name="chkCampo" id="chkPrcjeTiempoConexion"></td></tr>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnGenerarReporteGeneral" class="btn btn-primary">Generar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal" id="mdlReporteBasico" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h2>Reporte Básico</h2>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="reasignar_alumnos" name="reasignar_alumnos" >		
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table toggle-arrow-tiny table-bordered" id="tbl_reporte_basico">
                                    <thead>
                                        <tr>
                                            <th>Empresa</th>
                                            <th>Ficha</th>
                                            <th>Aplicación</th>
                                            <th>Fecha inicio</th>
                                            <th>Fecha cierre</th>
                                            <th>Nombre</th>
                                            <th>RUT</th>
                                            <th>Costo</th>
                                            <th>% Avance</th>
                                            <th>Tiempo de Conexión</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" value="Submit" class="btn btn-default">Cerrar</button>
            </div>
        </div>
    </div>
</div>

</div>
</div>
</div>

