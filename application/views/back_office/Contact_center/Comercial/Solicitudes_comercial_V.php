<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">

            <input type="hidden" id="perfil" name="perfil" value="<?php echo $id_perfil; ?>"> 

            <div class="ibox float-e-margins">
                <div class="ibox-title  back-change">
                    <h5>Buscador</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" >
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-md-12">

                                    <form action="" id="filtrosSolicitud">
                                        <div class="col-md-6 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Empresa</label>
                                                <select id="filtro-empresa" class="filtro-empresa form-control">

                                                </select>
                                            </div>											
                                        </div>

                                        <div class="col-md-6 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Ficha</label>
                                                <select id="filtro-ficha" class="filtro-ficha form-control">						

                                                </select>
                                            </div>											
                                        </div>

                                        <div class="col-md-6 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Orden de Compra</label>
                                                <select id="filtro-oc" class="filtro-oc form-control">

                                                </select>
                                            </div>	
                                        </div>

                                        <div class="col-md-6 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Modalidad</label>
                                                <select id="filtro-modalidad" class="filtro-modalidad form-control">

                                                </select>
                                            </div>	
                                        </div>

                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-primary pull-right" id="buscar" onclick="resultadoBusqueda()">Buscar</button>
                                        </div>
                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>		

            <div class="ibox float-e-margins">

                <div class="ibox-title  back-change">
                    <h5>Resultado</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content" id="divResultado" style="display: none">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-md-12">


                                    <div class="table-responsive">
                                        <table id="tblResultado" class="footable table toggle-arrow-tiny" style="display: ">
                                            <thead>
                                                <tr>
                                                    <th>Tutor</th>
                                                    <th nowrap>Rut</th>
                                                    <th>Nombre</th>
                                                    <th>Ficha</th>
                                                    <th>OC</th>
                                                    <th>Modalidad</th>
                                                    <th nowrap>Fecha Ini</th>
                                                    <th nowrap>Fecha Fin</th>
                                                    <th>Costo</th>
                                                    <th>DJ</th>
                                                    <th>Conexión</th>
                                                    <th>Seguimiento</th>
                                                    <th>
                                                        <input type="checkbox" id="chkAll" />
<!--                                                        <button onclick="seleccionar()" style="display: " class="btn btn-primary btn-xs" id="selectall"><i class="fa fa-square"></i></button>
                                                        <button onclick="seleccionar()" style="display: none" class="btn btn-primary btn-xs" id="selectNone"><i class="fa fa-square-o"></i></button>-->
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>										
                                            </tbody>
                                        </table>
                                    </div>

                                    <button style="display: none" class="btn btn-primary pull-right" id="selecAccion" onclick="camposChecked()">Seleccionar Acción</button>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="modal inmodal" id="modalAcc" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-md">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-bell-o modal-icon"></i>
                            <h4 class="modal-title">Generar Notificación</h4>
                        </div>
                        <div class="modal-body">

                            <div class="ibox-content">


                                <div class="row">
                                    <div class="col-md 12">
                                        <form role="form" id="formGenNot" name="formGenNot">

                                            <br>

                                            <div class="col-md-12">
                                                <div class="form-group"><label><small> Seleccione Acción</small></label> <br> 
                                                    <select class="form-control" id="selectAccion" name="selectAccion" requerido="true" 
                                                            mensaje="Seleccione una opción">
                                                        <option value="" disabled="" selected="">Seleccione Acción</option>
                                                        <option value="1">Marcar Vip</option>
                                                        <option value="2">Seguimiento</option>
                                                        <option value="3">Mail C.C. a terceros</option>
                                                        <option value="4">Script Especial</option>
                                                        <option value="5">Carta Especial</option>
                                                        <option value="6">Fechas Envío Mail Especiales</option>
                                                        <option value="7">No Requiere Seguimiento</option>

                                                    </select>
                                                </div>
                                                <hr>
                                                <div class="form-group" id="divFicha" style="display: none"><label><small>Especifique Ficha</small></label> <br>
                                                    <input class="form-control" id="inputFicha" name="inputFicha" type="number" min="0" placeholder="Ficha">
                                                </div>

                                                <div class="form-group" id="divAlumn" style="display: none"><label><small>CAMPOS RUT(ocultar)</small></label> <br>
                                                    <input class="form-control" id="inputAlumno" name="inputAlumno" type="text" placeholder="rut">
                                                </div>

                                                <div class="form-group" id="divFecha" style="display: none; cursor: pointer"><label><small>Especifique Fecha</small></label> <br>
                                                    <div class="input-group date" onclick="fecha()">
                                                        <span class="input-group-addon"><i class="fa fa-calendar" ></i></span>
                                                        <input id="inputFecha" name="inputFecha" type="text" class="form-control" placeholder="Fecha"/>
                                                    </div>
                                                </div>

                                                <div class="form-group"><label><small>Escribir comentario</small></label> <br> 
                                                    <textarea class="form-control" cols="" rows="5" style="width: 100%; resize: none" placeholder="Comentario" id="textComentario" name="textComentario" requerido="true" 
                                                              mensaje="Escriba un comentario"></textarea>
                                                </div>												

                                        </form>
                                    </div>
                                </div>



                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary" onclick="validarNot()">Generar Notificación</button>
                        </div>
                    </div>
                </div>
            </div>
                <input type="hidden" id="hdnEmpresa" value="" />
                <input type="hidden" id="hdnFicha" value="" />
                <input type="hidden" id="hdnOc" value="" />
                <input type="hidden" id="hdnModalidad" value="" />
        </div>


    </div>
</div>
</div>

