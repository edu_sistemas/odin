<?php
if ($tareas == null) {
    $fini = "-";
    $ffin = "-";
} else {
    $fini = $tareas[0]["fecha_desde"];
    $ffin = $tareas[0]["fecha_hasta"];
}
?>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <h5>Filtros</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="display: none;">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <select class="form-control mar-bot-15" id="empresa" name="empresa">
                                            <option></option>
                                            <?php
                                            for ($i = 0; $i < count($empresas_cbx); $i++) {
                                                if ($empresas_cbx[$i]["id"] > 0)
                                                    echo '<option value="' . $empresas_cbx[$i]["id"] . '">' . $empresas_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>										
                                        </select>
                                    </div>
                                    <div class="col-md-3 mar-bot-15">
                                        <select class="form-control" id="ejecutivo" name="ejecutivo">
                                            <option></option>
                                            <?php
                                            for ($i = 0; $i < count($ejecutivos_cbx); $i++) {
                                                echo '<option value="' . $ejecutivos_cbx[$i]["id"] . '">' . $ejecutivos_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>									
                                        </select>
                                    </div>
                                    <div class="col-md-2  mar-bot-15">
                                        <select class="form-control" type="number" name="inp_ficha" id="inp_ficha" placeholder="Ficha">
                                            <option></option>
                                            <?php
                                            for ($i = 0; $i < count($fichas); $i++) {
                                                echo '<option value="' . $fichas[$i]["id"] . '">' . $fichas[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="button" id="btnBuscar" class="btn btn-primary">Buscar</button>
                                    </div>
                                </div>							
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1">Tareas</a></li>
                <li class=""><a data-toggle="tab" href="#tab-2">Tareas (nuevos)</a></li>
            </ul>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <table class="table table-bordered no-padd-table" cellpadding="0" cellspacing="0" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Fecha Inicio Tareas</th>
                                            <th>Fecha Fin Tareas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $fini; ?></td>
                                            <td><?php echo $ffin; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>						
                            <div class="col-md-12 no-padd-table">
                                <table class="table toggle-arrow-tiny table-bordered no-padd-table" id="tbl_tareas" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Tutor</th>
                                            <th>Rut</th>
                                            <th>Nombre</th>
                                            <th>Ficha</th>
                                            <th>OC</th>
                                            <th>Modalidad</th>
                                            <th>Fecha Desde</th>
                                            <th>Fecha Hasta</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Fecha Cierre</th>
                                            <th>Costo</th>
                                            <th>DJ</th>
                                            <th>Conex.</th>
                                            <th>Seg.</th>
                                            <th>Ind.</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        for ($i = 0; $i < count($tareas); $i++) {
                                            echo '<tr id="' . $tareas[$i]["id_tarea"] . '">
                                                <td>' . ($i + 1) . '</td>
                                                <td>' . $tareas[$i]["nombre_apellido_tutor"] . '</td>
                                                <td>' . $tareas[$i]["rut_dv"] . '</td>
                                                <td>' . $tareas[$i]["nombre_apellido"] . '</td>
                                                <td>' . $tareas[$i]["ficha"] . '</td>
                                                <td>' . $tareas[$i]["orden_compra"] . '</td>
                                                <td width="100">' . $tareas[$i]["cp_modalidad"] . '</td>
                                                <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_desde"] . '</td>
                                                <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_hasta"] . '</td>
                                                <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_inicio"] . '</td>
                                                <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_fin"] . '</td>
                                                <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_cierre"] . '</td>                                                
                                                <td>' . $tareas[$i]["cp_costo"] . '</td>
                                                <td>' . $tareas[$i]["dj"] . '</td>
                                                <td>' . bcdiv($tareas[$i]["Tiempo_conexion"], '1', 2) . '</td>
                                                <td style="text-align: center">' . $tareas[$i]["seguimiento"] . '</td> <!-- Omar lo la a enviar -->
                                                <td style="text-align: center"><i class="fa fa-circle fa-lg" style="color: #' . $tareas[$i]["indicador"] . '" title="' . $tareas[$i]['desc_tipo_tarea'] . '"></i></td>	
                                                <td><i class="fa fa-thumbs-up" title="' . ($tareas[$i]['id_estado_tarea'] == 2 ? "Contactado" : "NO contactado") . '" style="font-size:20px;color:' . ($tareas[$i]['id_estado_tarea'] == 2 ? "green" : "gray") . ';" aria-hidden="true"></i></td>
                                                </tr>';
                                        }
                                        ?>			
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-pane">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table toggle-arrow-tiny table-bordered" id="tbl_tareas_nuevo" style="overflow-x:auto;">
                                    <thead>
                                        <tr>
                                            <th>N°</th>
                                            <th>Tutor</th>
                                            <th>Rut</th>
                                            <th>Nombre</th>
                                            <th>Ficha</th>
                                            <th>OC</th>
                                            <th>Modalidad</th>
                                            <th>Fecha Encuentro</th>
                                            <th>Fecha Inicio</th>
                                            <th>Fecha Fin</th>
                                            <th>Fecha Cierre</th>
                                            <th>Costo</th>
                                            <th  class="none" style="background-color: #f5f5f6;">DJ</th>
                                            <th  class="none" style="background-color: #f5f5f6;">Conex.</th>
                                            <th style="background-color: #f5f5f6;">Seg.</th>
                                            <th  class="none">Ind.</th>	
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        for ($i = 0; $i < count($tareas_nuevas); $i++) {
                                            echo '<tr id="' . $tareas_nuevas[$i]["id_tarea_nuevo"] . '">
                                                <td>' . ($i + 1) . '</td>
                                                <td>' . $tareas_nuevas[$i]["nombre_apellido_tutor"] . '</td>
                                                <td>' . $tareas_nuevas[$i]["rut_dv"] . '</td>
                                                <td>' . $tareas_nuevas[$i]["nombre_apellido"] . '</td>
                                                <td>' . $tareas_nuevas[$i]["ficha"] . '</td>
                                                <td>' . $tareas_nuevas[$i]["orden_compra"] . '</td>
                                                <td width="100">' . $tareas_nuevas[$i]["cp_modalidad"] . '</td>
                                                <td width="110">' . $tareas_nuevas[$i]["fecha_encuentro"] . '</td>
                                                <td width="110">' . $tareas_nuevas[$i]['fecha_inicio'] . '</td>
                                                <td width="110">' . $tareas_nuevas[$i]['fecha_fin'] . '</td>
                                                <td width="110">' . $tareas_nuevas[$i]['fecha_cierre'] . '</td>
                                                <td>' . $tareas_nuevas[$i]["cp_costo"] . '</td>
                                                <td>' . $tareas_nuevas[$i]["dj"] . '</td>
                                                <td>' . /* round($tareas_nuevas[$i]["Tiempo_conexion"], 2) */'' . '</td>
                                                <td style="text-align: center">' . $tareas_nuevas[$i]["seguimiento"] . '</td> <!-- Omar lo la a enviar -->
                                                <td style="text-align: center"><i class="fa fa-circle fa-lg" style="color: #D1D1D1"></i></td>
                                                <td><i class="fa fa-thumbs-up" title="' . ($tareas_nuevas[$i]['id_estado_tarea'] == 2 ? "Contactado" : "NO contactado") . '" style="font-size:20px;color:' . ($tareas_nuevas[$i]['id_estado_tarea'] == 2 ? "green" : "gray") . ';" aria-hidden="true"></i></td>
                                                </tr>';
                                        }
                                        ?>				
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

