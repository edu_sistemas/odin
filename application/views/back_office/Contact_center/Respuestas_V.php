<!--
27 SUPER
36 TELETUTOR
13 EJECUTIVO COMERCIAL
-->
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">


            <input type="hidden" id="perfil" name="perfil" value="<?php echo $id_perfil; ?>"> 

            <div class="row">




                <div class="col-md-6">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">

                            <h5>Respuestas/Total Gestiones</h5>                

                        </div>

                        <div class="ibox-content no-padding">

                            <div id="chartRespuestas" height="100">                      
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">

                            <h5>Entrantes</h5>                

                        </div>

                        <div class="ibox-content no-padding">

                            <div id="chartEntrantes" height="100">
                                <center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>               
                            </div>

                        </div>
                    </div>
                </div>


               


                <div class="ibox-content">
                    <div class="row">

                        <div class="col-md-6 b-r">



                            <table id="indicadoresTutor" class="footable table toggle-arrow-tiny table-bordered" data-page-size="5" data-filter=""  style="width: 100%;">
                                <thead>
                                    <!--CLASE PARA OCULTAR COLUMNAS: columHide-->
                                    <tr>
                                        <th class="">Tutor</th>
                                        <th class=" respuetas">Respuestas</th>
                                        <th class=" respuetas">Otros Seguimientos</th>

                                    </tr>
                                </thead>
                                <tbody>         
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">



                            <table id="indicadoresEmpresa" class="footable table toggle-arrow-tiny table-bordered" data-page-size="5" data-filter=""  style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Empresas</th>
                                        <th class=" respuetas">Respuestas</th>
                                        <th class=" respuetas">Otros Seguimientos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <br>
                
                 <div class="ibox float-e-margins">
                    <div class="ibox-title  back-change">
                        <h5>Filtros</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" >
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Holding</label>
                                                <select class="filtro-holding form-control" id="filtro-holding" name="filtro-holding">

                                                </select>
                                            </div>                      
                                        </div>

                                        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Empresa</label>
                                                <select class="filtro-empresa form-control" id="filtro-empresa" name="filtro-empresa">                                         

                                                </select>
                                            </div>                      
                                        </div>

                                        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Ficha</label>
                                                <select class="filtro-ficha form-control" id="filtro-ficha" name="filtro-ficha">

                                                </select>
                                            </div>  
                                        </div>

                                        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Orden de Compra</label>
                                                <select class="filtro-oc form-control" id="filtro-oc" name="filtro-oc">

                                                </select>
                                            </div>  
                                        </div>

                                        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Curso</label>
                                                <input type="text" class="form-control" id="filtro-curso" placeholder="Escriba Nombre del Curso"/>
                                            </div>  
                                        </div>

                                        <div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
                                            <div>
                                                <label for="">Fecha Término</label>
                                                <input class="form-control" type="text" id="fecha-termino" placeholder="Escriba Fecha Término">
                                            </div>  
                                        </div>


                                        <div class="col-md-12">
                                            <button class="btn btn-primary pull-right" id="btnBuscar">Buscar</button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title  back-change">
                                <h5>Detalle Alumnos</h5>
                            </div>

                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        

                                        <table id="indicadoresAlumnos" class="table toggle-arrow-tiny table-bordered" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Rut</th>
                                                    <th>Nombre</th>
                                                    <th>Tutor Asignado</th>
                                                    <th>Empresa</th>
                                                    <th>Ficha</th>
                                                    <th>Curso</th>
                                                    <th>Fecha Cierre</th>
                                                    <th>Costo</th>
                                                    <th>Indicador</th>
                                                    <th>Seg.</th>
                                                    <th>% Registro Sence</th>
                                                    <th></th>
                                                    <th class="columnHide">Declaración Jurada</th>
                                                    <th class="columnHide">Plazo Final entrega</th>
                                                    <th class="columnHide">Módulo</th>
                                                    <th class="columnHide">Nota</th>
                                                    <th class="columnHide">Promedio</th>
                                                    <th class="columnHide">Evaluaciones Realizadas</th>
                                                    <th class="columnHide">CTS</th>
                                                    <th class="columnHide">Cantidad Seguimientos Salientes</th>
                                                    <th class="columnHide">Cantidad Llamadas Realizadas</th>
                                                    <th class="columnHide">Cantidad Seguimientos Entrantes</th>
                                                    <th class="columnHide">Cantidad Llamadas Recibidas</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>



            </div>

        </div>

    </div>


</div>

