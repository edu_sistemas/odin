<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
       

        <div class="ibox-content">
            <div class="row">
                
                <div class="col-md-12">
                    <div class="col-md-6" style="text-align: left;">
                        <h4 style="text-align: center;">Alumnos asignados por tutor <a data-toggle="tooltip" data-placement="top" title="Los totales y porcentajes solo hacen referencia a alumnos cuyo costo sea Sence, Beca y Empresa"><i class="fa fa-info-circle"></i></a></h4>
                        <!--<table class="table table-striped">-->
                        <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_asignados_tutor">

                            <thead>
                                <tr>
                                    <th>Tutor</th>
                                    <th>Tot. Sence</th>
                                    <th>Tot. Beca</th>
                                    <th>Tot. Empresa</th>
                                    <th>Tot. Alumnos</th>
                                    <th>Clasif. Ficha</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-md-6" style="text-align: left;">
                        <h4 style="text-align: center;">Alumnos asignados por empresa <a data-toggle="tooltip" data-placement="top" title="Los totales y porcentajes solo hacen referencia a alumnos cuyo costo sea Sence, Beca y Empresa"><i class="fa fa-info-circle"></i></a></h4>
                        <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_asignados_empresa">
                            <thead>
                                <tr>
                                    <th>Empresa</th>
                                    <th>Tot. Sence</th>
                                    <th>Tot. Beca</th>
                                    <th>Tot. Empresa</th>
                                    <th>Tot. Alumnos</th>
                                    									
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
         <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title back-change">
                        <h5>Filtros</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-4 mar-bot-15">
                                <select class="form-control" id="filt_modalidad" name="filt_modalidad" requerido="true" mensaje="Seleccione Modalidad">
                                    <option selected value="">Seleccione Modalidad</option>
                                    <?php 
                                    for($i=0; $i < count($modalidades);$i++){
                                        echo '<option value="'.$modalidades[$i]["id"].'">'.$modalidades[$i]["text"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4 mar-bot-15">
                                <select class="form-control" id="filt_tcosto" name="filt_tcosto" requerido="true" mensaje="Seleccione tipo costo">
                                    <option selected value="">Seleccione Costo</option>
                                    <?php 
                                    for($i=0; $i < count($costos);$i++){
                                        echo '<option value="'.$costos[$i]["id"].'">'.$costos[$i]["text"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-1" style="text-align: left;">
                                <label><h5>Fecha fin</h5></label>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="data1" style="cursor: pointer;">
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar" onclick="fecha()"></i></span>
                                        <input type="text" id="fecha_cierre" name="fecha_cierre" class="form-control" value="" placeholder="Seleccione Fecha"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <select class="form-control" id="filt_fichas" name="filt_fichas" requerido="true" mensaje="Seleccione Ficha">
                                    <option selected value="">Seleccione Ficha</option>
                                    <?php 
                                    for($i=0; $i < count($fichas);$i++){
                                        echo '<option value="'.$fichas[$i]["id"].'">'.$fichas[$i]["text"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" id="filt_oc" name="filt_oc" requerido="true" mensaje="Seleccione Orden de Compra">
                                    <option selected value="">Seleccione Orden de Compra</option>
                                    <?php 
                                    for($i=0; $i < count($oc);$i++){
                                        echo '<option value="'.$oc[$i]["id"].'">'.$oc[$i]["text"].'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-9"></div>
                                <div class="col-md-2">
                                    <button type="button" id="btnListarTodos" class="btn btn-white pull-right">Listar Todos</button>
                                </div>
                                <div class="col-md-1">
                                    <button type="button" data-dismiss="modal" id="btnBuscar" class="btn btn-primary pull-right">Buscar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ibox-content">
            <div class="row">
                <h4 style="text-align: center;">Detalle por ficha</h4>
                <div class="col-md-12">
                    <table id="tbl_asignaciones" name="tbl_asignaciones" class="footable table toggle-arrow-tiny table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Ficha</th>
                                <th>OC</th>
                                <th>Empresa</th>
                                <th>Ejecutivo</th>
                                <th>Modalidad</th>
                                <th>Curso</th>
                                <th width="10%">F. Inicio</th>
                                <th width="10%">F. Fin</th>
                                <th>% Sence</th>
                                <th>N° Alumnos</th>
                                <th>C. Ficha</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>

            </div>
        </div> 



        <div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-xl2">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h2>Asignación por Alumno</h2>
                        <h2>Ficha: <b><label id="lblNumFicha"></label></b></h2>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="modal_dj" name="modal_dj" >		
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table toggle-arrow-tiny table-bordered" id="tbl_alumnos_ficha">
                                            <thead>
                                                <tr>
                                                    <th>Rut</th>
                                                    <th>Nombre</th>
                                                    <th>Orden de Compra</th>
                                                    <th>Costo</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <input type="hidden" id="hdnIdFicha" value="" />
                                </div>
                            </div> 
                        </form>
                    </div>
                    <div class="modal-footer">
                        <td><button type="button" class="btn btn-primary" id="btnTutores">Asignar</button></td>
                    </div>
                </div>
            </div>
        </div>


        <div class="ibox-content mar-bot-30">
            <div class="row">
                <div class="col-md-7 buttonAvance1 mar-bot-15 ">
                    <form> 
                        <a href="Reasignar_alumnos">
                            <button type="button" value="Submit" class="btn btn-primary center">Reasignar Tutor</button>
                        </a>
                    </form>
                </div>
<!-- 
<div class="col-md-6 buttonAvance2">
    <form> 
        <button type="button" data-dismiss="modal" value="Submit" class="btn btn-primary" onclick="modalasignar();">Asignar</button>
    </form>
</div>
 -->
            </div>
        </div>

        <div class="modal inmodal" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">

                    <div class="modal-header">
                        <h2>Asignar Tutor</h2>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" id="form_asignar_tutor" name="form_asignar_tutor" >		
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_tutores">
                                            <thead>
                                                <tr>
                                                    <th>Tutor</th>
                                                    <th>Q Alumnos</th>
                                                    <th>Asignar tutor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div> 
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">

                    <div class="modal-header">
                        <h2>Asignar Tutor</h2>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" id="form_asignar_tutor" name="form_asignar_tutor" >		
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_tutores_todos">
                                            <thead>
                                                <tr>
                                                    <th>Tutor</th>
                                                    <th>Q Alumnos</th>
                                                    <th>Asignar tutor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div> 
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>




        <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">

                    <div class="modal-header">
                        <h2>Clasificación Ficha</h2>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" id="form_asignar_tutor" name="form_asignar_tutor" >		
                            <div class="ibox-content" style="text-align: center">
                                <div class="row">
                                    <h4>Ingrese Valor</h4>
                                    <div class="col-md-12" style="text-align: center">
                                        <select id="sltClasifFicha">
                                            <option disabled="" selected="" value="-1">Seleccione</option>
                                            <option value="1">1</option>
                                            <option value="0.75">0.75</option>
                                            <option value="0.5">0.5</option>
                                            <option value="0.1">0.1</option>
                                        </select>
                                    </div>
                                    <input type="hidden" id="hdnIdFichaClas" value="" />
                                </div>
                            </div> 
                        </form>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

