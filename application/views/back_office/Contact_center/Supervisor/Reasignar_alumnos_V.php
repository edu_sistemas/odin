<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <h5>Filtros</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content" style="display: none;">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3 mar-bot-15">
                                        <input class="form-control" type="text" name="inp_rut" id="inp_rut" requerido="true" placeholder="Rut Alumno">
                                    </div>
                                    <div class="col-md-3 mar-bot-15">
                                        <select class="form-control" name="inp_ficha" id="inp_ficha" requerido="true" mensaje="Seleccione Ficha">
                                            <option value="">Seleccione Ficha</option>
                                             <?php 
                                        foreach ($fichas as $f ) {
                                            echo "<option value='".$f['id']."'>".$f['text']."</option>";
                                        }
                                        ?>
                                        </select>
                                    </div>								
                                    <div class="col-md-3 mar-bot-15">
                                         <select class="form-control" name="inp_oc" id="inp_oc" requerido="true" mensaje="Seleccione Orden de Compra">
                                            <option value="">Seleccione Orden de Compra</option>
                                             <?php 
                                        foreach ($oc as $o ) {
                                            echo "<option value='".$o['id']."'>".$o['text']."</option>";
                                        }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control" id="empresa" name="empresa">
                                            <option value="">Seleccione Empresa</option>
                                            <?php 
                                            for($i=0;$i < count($empresas); $i++){
                                                if($empresas[$i]["id"] > 0){
                                                    echo "<option value='".$empresas[$i]["id"]."'>".$empresas[$i]["text"]."</option>";
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-12">
                                        <br>
                                        <button type="button" class="btn btn-primary pull-right" id="btnBuscar">Buscar</button>
                                    </div>
                                </div>							
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <div class="row">
                    
                    <div class="col-md-12">
                        <table class="table toggle-arrow-tiny table-bordered" id="tbl_alumnos_reasignar">
                            <thead>
                                <tr>
                                    <th>Tutor</th>
                                    <th>Empresa</th>
                                    <th>Ficha</th>
                                    <th>OC</th>
                                    <th>Rut</th>
                                    <th>Nombre</th>
                                    <th><input type="checkbox" id="chkSelTodosAlumnos" /></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    for($i=0;$i < count($alumnos); $i++){
                                    echo "<tr id='".$alumnos[$i]["id_asignacion"]."'>"
                                            . "<td>".$alumnos[$i]["nombre_tutor"]."</td>"
                                            . "<td>".$alumnos[$i]["razon_social_empresa"]."</td>"
                                            . "<td>".$alumnos[$i]["id_ficha"]."</td>"
                                            . "<td>".$alumnos[$i]["num_orden_compra"]."</td>"
                                            . "<td>".$alumnos[$i]["rut_alumno_fmt"]."</td>"
                                            . "<td>".$alumnos[$i]["nombre_alumno"]."</td>"
                                            . "<td><input type='checkbox' name='chkAlumno' value='".$alumnos[$i]["id_asignacion"]."' /></td>"
                                       . "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>

            <div class="ibox-content mar-bot-30">
                <div class="row">
                    <div class="col-md-12 pull-right" style="text-align: right;">
                        <form action="#"> 
                            <button type="button" data-dismiss="modal" value="Submit" class="btn btn-primary" onclick="modalasignar();">Reasignar</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">

                        <div class="modal-header">
                            <h2>Asignar Tutor</h2>
                        </div>

                        <div class="modal-body">
                            <form class="form-horizontal" id="reasignar_alumnos" name="reasignar_alumnos" >		
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table class="footable table toggle-arrow-tiny table-bordered" id="tbl_tutores_todos">
                                                <thead>
                                                    <tr>
                                                        <th>Tutor</th>
                                                        <th>Q Alumnos</th>
                                                        <th>Asignar tutor</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div> 
                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

<input type="hidden" id="hdnBaseUrl" value="<?php echo $base_url; ?>" />

