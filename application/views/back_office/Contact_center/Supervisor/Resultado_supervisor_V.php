<?php
$inicio = date("Y-m-d", strtotime(date("Y-m-d") . " -1 month"));
$fin = date("Y-m-d");
?>

<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title  back-change">
                            <h5>Filtros</h5>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-3">
                                        <select class="form-control" id="id_empresa" name="id_empresa">
                                            <option></option>
                                            <?php
                                            for ($i = 0; $i < count($empresas_cbx); $i++) {
                                                if ($empresas_cbx[$i]["id"] != 0)
                                                    echo '<option value="' . $empresas_cbx[$i]["id"] . '">' . $empresas_cbx[$i]["text"] . '</option>';
                                            }
                                            ?>											
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control" type="text" name="num_ficha" id="num_ficha">
                                            <option></option>
                                            <?php
                                            for ($i = 0; $i < count($fichas); $i++) {
                                                    echo '<option value="' . $fichas[$i]["id"] . '">' . $fichas[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>								
                                    <div class="col-md-3">
                                        <select class="form-control" type="text" name="num_oc" id="num_oc">
                                            <option></option>
                                            <?php
                                            for ($i = 0; $i < count($oc); $i++) {
                                                    echo '<option value="' . $oc[$i]["id"] . '">' . $oc[$i]["text"] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-daterange input-group date" id="">
                                            <input type="text" id="fecha_cierre_desde" name="fecha_cierre_desde" class="form-control" value="<?php echo $inicio; ?>" placeholder="Desde"/>
                                            <span class="input-group-addon">Fecha Cierre</span>
                                            <input type="text" id="fecha_cierre_hasta" name="fecha_cierre_hasta" class="form-control" value="<?php echo $fin; ?>" placeholder="Hasta"/>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <br>
                                        <button type="button" id="btnBuscar" class="btn btn-primary pull-right">Buscar</button>
                                    </div>
                                </div>							
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-12 especialtable">
                        <br>
                        <table class="table toggle-arrow-tiny table-bordered" id="tbl_resultados" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th rowspan="2">Tutores</th>
                                    <th colspan="2" style="text-align: center;">DJ (OK) 
                                        <a id="Ojo1" onclick="myModalDJ(1);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalle alumnos con DJ enviada."><i class="fa fa-eye"></i></a></th>
                                        <th colspan="2" style="text-align: center;">Conectividad (OK) 
                                            <a id="Ojo2" onclick="myModalConect(2);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalle alumnos con conectividad cumplida."><i class="fa fa-eye"></i></a></th>
                                            <th colspan="2" style="text-align: center;">Finalizados (OK) 
                                             <a id="Ojo3" onclick="myModalFin(3);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalle alumnos con todas sus evaluaciones finalizadas."><i class="fa fa-eye"></i></a></th>
                                             <th colspan="2" style="text-align: center;">Llamadas Mensuales</th>
                                             <th colspan="2" style="text-align: center;">Pedidos Especiales</th>
                                             <th colspan="2" style="text-align: center;">Extensiones  
                                                <a id="Ojo4" onclick="myModalExt(4);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalle extensiones por ficha."><i class="fa fa-eye"></i></a></th>
                                           </tr>
                                           <tr>
                                            <th style="text-align: center;">Cantidad Alumnos</th>
                                            <th style="text-align: center;">%</th>
                                            <th style="text-align: center;">Cantidad Alumnos</th>
                                            <th style="text-align: center;">%</th>
                                            <th style="text-align: center;">Cantidad Alumnos</th>
                                            <th style="text-align: center;">%</th>
                                            <th style="text-align: center;">Cantidad</th>
                                            <th style="text-align: center;">%</th>
                                            <th style="text-align: center;">Cantidad</th>
                                            <th style="text-align: center;">%</th>
                                            <th style="text-align: center;">Cantidad Fichas (OC)</th>
                                            <th style="text-align: center;">%</th>
                                        </tr>						
                                    </thead>
                                    <tbody>
                                        <?php
                                        for ($i = 0; $i < count($resultados); $i++) {
                                            echo '<tr>'
                                            . '<td>' . $resultados[$i]["Nombre_Tutor"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Cant_Dj"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Dj"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Cant_Con"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Con"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Cant_Fin"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Fin"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Cant_LlamMen"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Pcje_LlamMen"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Cant_Ped"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Ped"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Cant_Ex"] . '</td>'
                                            . '<td style="text-align: center;">' . $resultados[$i]["Pcje_Ex"] . '</td>'
                                            . '</tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

                    <div class="modal inmodal" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content animated bounceInRight">
                                <div class="modal-header">
                                    <h2>Declaración Jurada</h2>
                                </div>
                                <div class="modal-body">
                                    <form class="form-horizontal" id="modal_dj" name="modal_dj" >		
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class=" table toggle-arrow-tiny table-bordered" id="tbl_djs">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: center;">Ficha</th>
                                                                <th style="text-align: center;">OC</th>
                                                                <th style="text-align: center;">Empresa</th>
                                                                <th style="text-align: center;">Curso</th>
                                                                <th style="text-align: center;">Fecha ini.</th>
                                                                <th style="text-align: center;">Fecha cierre</th>
                                                                <th style="text-align: center;">Rut</th>
                                                                <th style="text-align: center;">Nombre</th>
                                                                <th style="text-align: center;">DJ</th>
                                                                <th style="text-align: center;">Tutor</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div> 
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal inmodal" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content animated bounceInRight">
                                <div class="modal-header">
                                    <h2>Conectividad</h2>
                                </div>

                                <div class="modal-body">
                                    <form class="form-horizontal" id="modal_conect" name="modal_conect" >		
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table toggle-arrow-tiny table-bordered" id="tbl_con">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: center;">Ficha</th>
                                                                <th style="text-align: center;">OC</th>
                                                                <th style="text-align: center;">Empresa</th>
                                                                <th style="text-align: center;">Curso</th>
                                                                <th style="text-align: center;">Fecha ini.</th>
                                                                <th style="text-align: center;">Fecha cierre</th>
                                                                <th style="text-align: center;">Rut</th>
                                                                <th style="text-align: center;">Nombre</th>
                                                                <th style="text-align: center;">Tiempo Conexión</th>
                                                                <th style="text-align: center;">Tutor</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div> 
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal inmodal" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content animated bounceInRight">

                                <div class="modal-header">
                                    <h2>Finalizados</h2>
                                </div>

                                <div class="modal-body">
                                    <form class="form-horizontal" id="reasignar_alumnos" name="reasignar_alumnos" >		
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table toggle-arrow-tiny table-bordered" id="tbl_fin">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: center;">Ficha</th>
                                                                <th style="text-align: center;">OC</th>
                                                                <th style="text-align: center;">Empresa</th>
                                                                <th style="text-align: center;">Curso</th>
                                                                <th style="text-align: center;">Fecha ini.</th>
                                                                <th style="text-align: center;">Fecha cierre</th>
                                                                <th style="text-align: center;">Rut</th>
                                                                <th style="text-align: center;">Nombre</th>
                                                                <th style="text-align: center;">% Eval</th>
                                                                <th style="text-align: center;">Tutor</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div> 
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>	

                    <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog modal-xl">
                            <div class="modal-content animated bounceInRight">

                                <div class="modal-header">
                                    <h2>Extensiones</h2>
                                </div>

                                <div class="modal-body">
                                    <form class="form-horizontal" id="reasignar_alumnos" name="reasignar_alumnos" >		
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table toggle-arrow-tiny table-bordered" id="tbl_ext">
                                                        <thead>
                                                            <tr>
                                                                <th style="text-align: center;">Ficha</th>
                                                                <th style="text-align: center;">OC</th>
                                                                <th style="text-align: center;">Empresa</th>
                                                                <th style="text-align: center;">Curso</th>
                                                                <th style="text-align: center;">Fecha ini.</th>
                                                                <th style="text-align: center;">Fecha cierre</th>
                                                                <th style="text-align: center;">Fecha Ext</th>
                                                                <th style="text-align: center;">Tutor</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>
                                        </div> 
                                    </form>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

