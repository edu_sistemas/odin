<?php
if($tareas==null)
{
    $fini="-";
    $ffin="-";
}
else
{
  $fini=$tareas[0]["fecha_desde"];
  $ffin=$tareas[0]["fecha_hasta"];
}

?>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#tab-1">Tareas</a></li>
                    <li class=""><a data-toggle="tab" href="#tab-2">Tareas (nuevos)</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <table class="table table-bordered no-padd-table" cellpadding="0" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Fecha Inicio Tareas</th>
                                                <th>Fecha Fin Tareas</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                               <td><?php echo $fini;?></td>
                                               <td><?php echo $ffin;?></td>
                                           </tr>
                                       </tbody>
                                   </table>
                               </div>
                               <div class="col-md-8" style="text-align: right;">
                                <span>Reordenar Tareas &ensp; </span>
                                <div class="fc-button-group">
                                 <button title="Poner en 1ª Posición" type="button" class="btn btn-primary" id="btnTop"><i class="fa fa-arrow-up"></i></button>
                                 <button title="Bajar Posición" type="button" class="btn btn-warning" id="btnDown"><i class="fa fa-chevron-down"></i></button>
                                 <button title="Subir Posición" type="button" class="btn btn-warning" id="btnUp"><i class="fa fa-chevron-up"></i></button>

                             </div>
                         </div>								
                         <div class="col-md-12 no-padd-table">
                            <table class="table toggle-arrow-tiny table-bordered no-padd-table" id="tbl_tareas" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Tutor</th>
                                        <th>Rut</th>
                                        <th>Nombre</th>
                                        <th>Ficha</th>
                                        <th>OC</th>
                                        <th>Modali.</th>
                                        <th>Fecha Desde</th>
                                        <th>Fecha Hasta</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Fin</th>
                                        <th>Fecha Cierre</th>
                                        <th>Costo</th>
                                        <th>DJ</th>
                                        <th>Conex.</th>
                                        <th>Eval</th>
                                        <th>Seguim.</th>
                                        <th class="none">Ind.</th>
                                        <th class="none">Ver</th>	
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    for ($i = 0; $i < count($tareas); $i++) {
                                        echo '
                                        <tr id="' . $tareas[$i]["id_tarea"] . '">
                                        <td class="mouse-hnd">' . ($i + 1) . '
                                        <input type="hidden" id="tarea_' . ($i + 1) . '" value="' . $tareas[$i]["id_tarea"] . '">
                                        <input type="hidden" id="orden_' . ($i + 1) . '" value="' . $tareas[$i]["orden"] . '">
                                        </td>
                                        <td class="mouse-hnd" width="100">' . $tareas[$i]["nombre_apellido_tutor"] . '</td>
                                        <td class="mouse-hnd">' . $tareas[$i]["rut_dv"] . '</td>
                                        <td class="mouse-hnd" width="100">' . $tareas[$i]["nombre_apellido"] . '</td>
                                        <td class="mouse-hnd">' . $tareas[$i]["ficha"] . '</td>
                                        <td class="mouse-hnd">' . $tareas[$i]["orden_compra"] . '</td>
                                        <td class="mouse-hnd" width="100">' . $tareas[$i]["cp_modalidad"] . '</td>
                                        <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_desde"] . '</td>
                                        <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_hasta"] . '</td>
                                        <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_inicio"] . '</td>
                                        <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_fin"] . '</td>
                                        <td class="mouse-hnd" width="110">' . $tareas[$i]["fecha_cierre"] . '</td>
                                        <td class="mouse-hnd">' . $tareas[$i]["cp_costo"] . '</td>
                                        <td class="mouse-hnd">' . $tareas[$i]["dj"] . '</td>
                                        <td class="mouse-hnd">' . round($tareas[$i]["Tiempo_conexion"], 2) . '</td>
                                        <td class="mouse-hnd">' . $tareas[$i]["Evaluaciones_Realizadas"] . '</td>
                                        <td class="mouse-hnd" style="text-align: center">' . $tareas[$i]["seguimiento"] . '</td> <!-- Omar lo la a enviar -->
                                        <td class="mouse-hnd" style="text-align: center"><i class="fa fa-circle fa-lg" style="color: #' . $tareas[$i]["indicador"] . '" title="'.$tareas[$i]["desc_tipo_tarea"].'"></i></td>
                                        <td class="mouse-hnd" style="text-align: center"><a href="../Comun/Detalle_alumno/' . $tareas[$i]['cp_idUsuario'] . '"><i class="fa fa-search"></i></a></td>		
                                        <td><i class="fa fa-thumbs-up" title="' . ($tareas[$i]['id_estado_tarea'] == 2 ? "Contactado" : "NO contactado") . '" style="font-size:20px;color:' . ($tareas[$i]['id_estado_tarea'] == 2 ? "green" : "gray") . ';" aria-hidden="true"></i></td>                                                
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-2" class="tab-pane">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12" style="text-align: right; display: none;">
                            <br>
                            <div class="fc-button-group">
                                <button type="button"><span class="fa fa-chevron-down"></span></button>
                                <button type="button"><span class="fa fa-chevron-up"></span></button>				
                            </div>
                        </div>	
                        <div class="col-md-12 no-padd-table">
                            <br>
                            <table class="table toggle-arrow-tiny table-bordered no-padd-table" id="tbl_tareas_nuevo" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>N°</th>
                                        <th>Tutor</th>
                                        <th>Rut</th>
                                        <th>Nombre</th>
                                        <th>Ficha</th>
                                        <th>OC</th>
                                        <th>Modali.</th>
                                        <th>Fecha Encuentro</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Fin</th>
                                        <th>Fecha Cierre</th>
                                        <th>Costo</th>
                                        <th style="background-color: #f5f5f6;">DJ</th>
                                        <th style="background-color: #f5f5f6;">Conex.</th>
                                        <th style="background-color: #f5f5f6;">Eval</th>
                                        <th style="background-color: #f5f5f6;">Seguim.</th>
                                        <th>Ind.</th>
                                        <th>Ver</th>	
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    for ($i = 0; $i < count($tareas_nuevas); $i++) {
                                        echo '<tr id="' . $tareas_nuevas[$i]["id_tarea_nuevo"] . '">
                                        <td>' . ($i + 1) . '</td>
                                        <td>' . $tareas_nuevas[$i]["nombre_apellido_tutor"] . '</td>
                                        <td>' . $tareas_nuevas[$i]["rut_dv"] . '</td>
                                        <td>' . $tareas_nuevas[$i]["nombre_apellido"] . '</td>
                                        <td>' . $tareas_nuevas[$i]["ficha"] . '</td>
                                        <td>' . $tareas_nuevas[$i]["orden_compra"] . '</td>
                                        <td width="100">' . $tareas_nuevas[$i]["cp_modalidad"] . '</td>
                                        <td width="110">' . $tareas_nuevas[$i]["fecha_desde"] . '</td>
                                        <td width="110">' . $tareas_nuevas[$i]["fecha_inicio"] . '</td>
                                        <td width="110">' . $tareas_nuevas[$i]["fecha_fin"] . '</td>
                                        <td width="110">' . $tareas_nuevas[$i]["fecha_cierre"] . '</td>
                                        <td>' . $tareas_nuevas[$i]["cp_costo"] . '</td>
                                        <td>' . $tareas_nuevas[$i]["dj"] . '</td>
                                        <td>' . /*round($tareas_nuevas[$i]["Tiempo_conexion"], 2) .*/ '</td>
                                        <td>' . $tareas_nuevas[$i]["Evaluaciones_Realizadas"] . '</td>
                                        <td style="text-align: center">' . $tareas_nuevas[$i]["seguimiento"] . '</td> <!-- Omar lo la a enviar -->
                                        <td style="text-align: center"><i class="fa fa-circle fa-lg" style="color: #D1D1D1"></i></td>
                                        <td style="text-align: center"><a href="../Comun/Detalle_alumno/' . $tareas_nuevas[$i]['cp_idUsuario'] . '"><i class="fa fa-search"></i></a></td>	
                                        <td><i class="fa fa-thumbs-up" title="' . ($tareas_nuevas[$i]['id_estado_tarea'] == 2 ? "Contactado" : "NO contactado") . '" style="font-size:20px;color:' . ($tareas_nuevas[$i]['id_estado_tarea'] == 2 ? "green" : "gray") . ';" aria-hidden="true"></i></td>
                                        </tr>';
                                    }
                                    ?>		
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

