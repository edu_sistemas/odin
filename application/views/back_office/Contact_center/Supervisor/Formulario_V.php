<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1"> Valores Según OTIC</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-2">Bonificación Según Grado Curso</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-3">Pauta de Categorías</a></li>
                                <li class="" style="display: none;"><a data-toggle="tab" href="#tab-4">Objetivos KPI´S</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-5">Día Bonificación</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <table id="tblSegunOtic" class="table toggle-arrow-tiny table-bordered" style="margin: 0 auto">
                                            <thead>
                                                <tr>
                                                    <th>RUT OTIC</th>
                                                    <th>Plazo entrega D.J.</th>
                                                    <th>Límite de Conexión (%). *Valor 1 > que 0 segundos</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                for ($i = 0; $i < count($otics); $i++) {
                                                    echo "<tr>"
                                                    . "<td>" . $otics[$i]["rut_otic"] . "</td>"
                                                    . "<td><input type='number' name='inputOtic' id='inputOticDJ_" . $otics[$i]["id_parametro"] . "' class='form-control inputForm' value='" . $otics[$i]["plazo_entrega_dj"] . "' disabled></td>"
                                                    . "<td><input type='number' name='inputOtic' id='inputOticConex_" . $otics[$i]["id_parametro"] . "' class='form-control inputForm' value='" . $otics[$i]["porcen_tiempo_minimo_conex_sence"] . "' disabled></td>"
                                                    . "<td><button class='btnEditarOtic btn btn-primary btn-xs' id='btnEditarOtic_" . $otics[$i]["id_parametro"] . "'>Editar</button>"
                                                    . "<button class='btnGuardarOtic btn btn-primary btn-xs' id='btnGuardarOtic_" . $otics[$i]["id_parametro"] . "' style='display: none'>Guardar</button>"
                                                    . "<button class='btnCancelarOtic btn btn-danger btn-xs' id='btnCancelarOtic_" . $otics[$i]["id_parametro"] . "' style='display: none'>Cancelar</button></td>"
                                                    . "<input type='hidden' id='hdnEditadoOtic_" . $otics[$i]["id_parametro"] . "' value='0' />"
                                                    . "</tr>";
                                                }
                                                ?>
                                            </tbody>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <table id="tblSegunGrado" class="display table table-bordered" style="margin: 0 auto">
                                            <thead>
                                                <tr>
                                                    <th>Grado Bono</th>
                                                    <th width="400px">Valor ($) No Beca</th>
                                                    <th width="400px">Valor ($) Beca</th>
                                                    <th width="120px"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                for ($i = 0; $i < count($grado_bonos); $i++) {
                                                    echo "<tr>"
                                                    . "<td><input type='text' class='form-control' id='txt_grado_bono_" . ($i + 1) . "' name='txt_grado_bono' value='" . $grado_bonos[$i]["grado_bono"] . "' maxlength='20' disabled>
                                                        <input type='hidden'  id='txt_grado_bono_or_" . ($i + 1) . "' value='" . $grado_bonos[$i]["grado_bono"] . "'>
                                                        </td>"
                                                    . "<td><input type='number' class='form-control' id='txt_grado_bono_valor_no_beca_" . ($i + 1) . "' name='txt_grado_bono_valor_no_beca' value='" . $grado_bonos[$i]["valor_no_beca"] . "' disabled></td>"
                                                    . "<td><input type='number' class='form-control' id='txt_grado_bono_valor_beca_" . ($i + 1) . "' name='txt_grado_bono_valor_beca' value='" . $grado_bonos[$i]["valor_beca"] . "' disabled></td>"
                                                    . "<td><button class='btn btn-primary btn-xs' id='btnEditarGrado_" . ($i + 1) . "' onclick='editarGrado(" . ($i + 1) . ")'>Editar</button>"
                                                    . "<button class='btnGuardarGrado btn btn-primary btn-xs' id='btnGuardarGrado_" . ($i + 1) . "' style='display: none' onclick='guardarGrado(" . ($i + 1) . ")'>Guardar</button><br>"
                                                    . "<button class='btn btn-danger btn-xs' id='btnCancelarGrado_" . ($i + 1) . "' onclick='cancelarGrado(" . ($i + 1) . ", true)' style='display: none'>Cancelar</button>"
                                                    . "<input type='hidden' value='" . $grado_bonos[$i]["id_parametros_generales_bono"] . "' id='hdnGradoId" . ($i + 1) . "'/></td>"
                                                    . "<input type='hidden' id='hdnEditandoGrado_" . ($i + 1) . "' value='0' />"
                                                    . "</tr>";
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td class="text-center"><button type="button" id="btnAgregarGradoBono" class="btn btn-xs agregar-campo"><i class=" fa fa-plus-circle fa-2x"></i></button></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div id="tab-3" class="tab-pane">
                                    <div class="panel-body">
                                        <table id="tblPauta" class="table toggle-arrow-tiny table-bordered" style="margin: 0 auto">
                                            <thead>
                                                <tr>
                                                    <th>Clasificación</th>
                                                    <th></th>
                                                    <th>Por qué</th>
                                                    <th>Script</th>
                                                    <th>Pauta Seguimiento</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                for ($i = 0; $i < count($categorias); $i++) {
                                                    echo '<tr>
                                                <td>' . $categorias[$i]["clasificacion"] . '</td>
                                                <td><i class="fa fa-circle" style="color: ' . $categorias[$i]["color"] . ';"></i></td>
                                                <td><textarea class="form-control" id="txtAreaPorque_' . $categorias[$i]["id_parametro_categoria"] . '" disabled>' . $categorias[$i]["porque"] . '</textarea></td>
                                                <td><textarea class="form-control" id="txtAreaScript_' . $categorias[$i]["id_parametro_categoria"] . '" disabled>' . $categorias[$i]["script"] . '</textarea></td>
                                                <td><textarea class="form-control" id="txtAreaPauta_' . $categorias[$i]["id_parametro_categoria"] . '" disabled>' . $categorias[$i]["pauta_seguimiento"] . '</textarea></td>
                                                <td>
                                                <button class="btnEditarCategoria btn btn-primary btn-xs" id="btnEditarCategoria_' . $categorias[$i]["id_parametro_categoria"] . '">Editar</button>
                                                <button class="btnGuardarCategoria btn btn-primary btn-xs" id="btnGuardarCategoria_' . $categorias[$i]["id_parametro_categoria"] . '" style="display: none">Guardar</button><br>
                                                <button class="btnCancelarCategoria btn btn-danger btn-xs" id="btnCancelarCategoria_' . $categorias[$i]["id_parametro_categoria"] . '" style="display: none">Cancelar</button>
                                                <input type="hidden" id="hdnEditadoCategoria_' . $categorias[$i]["id_parametro_categoria"] . '" value="0" />
                                                </td>
                                                </tr>';
                                                }
                                                ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="tab-4" class="tab-pane">
                                    <div class="panel-body">
                                        <table id="tblObjetivo" class=" table toggle-arrow-tiny table-bordered" style="margin: 0 auto">
                                            <thead>
                                                <tr>
                                                    <th>Indicador</th>
                                                    <th>Objetivo</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                for ($i = 0; $i < count($kpis) - 1; $i++) {
                                                    echo '<tr>'
                                                    . '<td>' . $kpis[$i]["desc_indicador"] . '</td>'
                                                    . '<td><input type="number" class="form-control" value="' . $kpis[$i]["valor_objetivo"] . '" id="txtObjetivo_' . $kpis[$i]["id_parametros_generales_kpi"] . '" disabled></td>'
                                                    . '<td><button class="btnEditarKpi btn btn-primary btn-xs" id="btnEditarKpi_' . $kpis[$i]["id_parametros_generales_kpi"] . '">Editar</button>'
                                                    . '<button class="btnGuardarKpi btn btn-primary btn-xs" id="btnGuardarKpi_' . $kpis[$i]["id_parametros_generales_kpi"] . '" style="display: none">Guardar</button>'
                                                    . '<button class="btnCancelarKpi btn btn-danger btn-xs" id="btnCancelarKpi_' . $kpis[$i]["id_parametros_generales_kpi"] . '" style="display: none">Cancelar</button></td>'
                                                    . '<input type="hidden" id="hdnEditadoKpi_' . $kpis[$i]["id_parametros_generales_kpi"] . '" value="0" />'
                                                    . '</tr>';
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div id="tab-5" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="control-label col-sm-2" for="dia_bono">Día Bonificación:</label>
                                                <div class="col-sm-2">
                                                    <?php $dia_bono = end($kpis); ?>
                                                    <input type="number" min="1" max="31" step="1" class="form-control diaBon" id="txtObjetivo_<?php echo $dia_bono['id_parametros_generales_kpi']; ?>" value="<?php echo $dia_bono['valor_objetivo']; ?>">
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="button" id="btnGuardarKpi_<?php echo $dia_bono['id_parametros_generales_kpi']; ?>" class="btnGuardarKpi btn btn-sm btn-primary"> Guardar</button>
                                                </div>
                                                <div class="col-md-6">
                                                    <span class="form-text m-b-none">Ingrese el día de cierre del periodo para el cálculo de Bonos. (1 a 31)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="alert alert-info" role="alert">
                                            <h3>Información</h3>
                                            Se recuerda que para el correcto cálculo de los Bonos, la fecha de cierre del curso debe coincidir con el periodo donde se asignó la Ficha / Alumno al tutor correspondiente.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

