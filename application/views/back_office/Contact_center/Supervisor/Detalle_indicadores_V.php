<div class="wrapper wrapper-content">
	<div class="row animated fadeInDown">
		<div class="col-lg-12">


			<!--
			27 SUPER
			36 TELETUTOR
			13 EJECUTIVO COMERCIAL
			-->

<input type="" id="perfil" name="perfil" value="<?php echo $id_perfil;?>"> 

			<div class="col-md-12">
				<div class="row">
					<div class="ibox float-e-margins">
						<div class="text-center ibox-title back-change">
							<h5 class="text-center">Gráfico</h5>				 		
						</div>
						<div id="chartDeclaraciones" height="100">
						</div>
					</div>
				</div>
			</div>

			<div class="ibox float-e-margins">
				<div class="ibox-title  back-change">
					<h5>Filtros</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content" >
					<div class="row">
						<div class="col-xs-12">
							<div class="row">

								<div class="col-md-12">

									<div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
										<div>
											<label for="">Holding</label>
											<select class="filtro-holding form-control">
												<option></option>
												<option value="1">Lider</option>
												<option value="2">IBM</option>
												<option value="3">Vmica</option>
												<option value="4">Edutecno</option>
												<option value="5">Samsung</option>
												<option value="6">DuocUC</option>
												<option value="7">Inacap</option>
												<option value="8">AIEP</option>

											</select>
										</div>											
									</div>

									<div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
										<div>
											<label for="">Empresa</label>
											<select class="filtro-empresa form-control">
												<option></option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>											

											</select>
										</div>											
									</div>

									<div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
										<div>
											<label for="">Ficha</label>
											<select class="filtro-ficha form-control">
												<option></option>
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>

											</select>
										</div>	
									</div>

									<div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
										<div>
											<label for="">Orden de Compra</label>
											<select class="filtro-oc form-control">
												<option></option>
												<option value="e-learnig">E-Learning</option>
												<option value="presencial">Presencial</option>
												<option value="distancia">Distancia</option>

											</select>
										</div>	
									</div>

									<div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
										<div>
											<label for="">Curso</label>
											<select class="filtro-curso form-control">
												<option></option>
												<option value="1">Excel</option>
												<option value="2">Word</option>
												<option value="3">Power Point</option>
												<option value="4">Planificación</option>
												<option value="5">Edición</option>

											</select>
										</div>	
									</div>

									<div class="col-md-4 col-xs-12" style="margin-bottom: 10px">
										<div>
											<label for="">Fecha Termino</label>
											<input class="form-control" type="text" placeholder="Escriba Fecha Termino">
										</div>	
									</div>


									<div class="col-md-12">
										<button class="btn btn-primary pull-right">Buscar</button>
									</div>

								</div>

							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="ibox-content">
				<div class="row">

					<div class="col-md-6 b-r">

						<h5 style="text-align: right;">Exportar</h5>
						<div class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="html5buttons">
								<div class="dt-buttons btn-group">
									<a class="btn btn-default buttons-copy buttons-html5" tabindex="0"><span>Copiar</span></a>
									<a class="btn btn-default buttons-excel buttons-html5" tabindex="0"><span>Excel</span></a>
									<a class="btn btn-default buttons-print" tabindex="0"><span>Imprimir</span></a>
								</div>
							</div>
						</div>

						<br>

						<table id="indicadoresTutor" class="footable table toggle-arrow-tiny table-bordered" data-page-size="5" data-filter="">
							<thead>
								<tr>
									<th class="">Tutor</th>
									<th class="decJuradas">E-E</th>
									<th class="decJuradas">E-NE</th>
									<th class="decJuradas">D-E</th>
									<th class="decJuradas">D-NE</th>
									<th class="columnHide conectadas">Cumple</th>
									<th class="columnHide conectadas">No Cumple</th>
									<th class="columnHide evaluacion">Lista</th>
									<th class="columnHide evaluacion">Pendiente</th>
									<th class="columnHide seguimiento">Tiene</th>
									<th class="columnHide seguimiento">No Tiene</th>
									<th class="columnHide llamadas">Cumple</th>
									<th class="columnHide llamadas">No Cumple</th>
									<th class="columnHide respuetas">Respuestas</th>
									<th class="columnHide respuetas">Otros Seguimientos</th>

								</tr>
							</thead>
							<tbody>
								<tr>
									<td>John Doe</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">5/20</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">18/20</td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide respuestas"></td>
									<td class="columnHide respuestas"></td>
								</tr>
								<tr>
									<td>John Doe</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">5/20</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">18/20</td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide respuestas"></td>
									<td class="columnHide respuestas"></td>
								</tr>	
								<tr>
									<td>John Doe</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">5/20</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">18/20</td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide respuestas"></td>
									<td class="columnHide respuestas"></td>
								</tr>	
								<tr>
									<td>John Doe</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">5/20</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">18/20</td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide respuestas"></td>
									<td class="columnHide respuestas"></td>
								</tr>	
								<tr>
									<td>John Doe</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">5/20</td>
									<td class="decJuradas">10/20</td>
									<td class="decJuradas">18/20</td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide conectadas"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide evaluacion"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide seguimiento"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide llamadas"></td>
									<td class="columnHide respuestas"></td>
									<td class="columnHide respuestas"></td>
								</tr>								
							</tbody>
							<tfoot>
								<tr>
									<td colspan="6">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
					<div class="col-md-6">

						<h5 style="text-align: right;">Exportar</h5>
						<div class="dataTables_wrapper form-inline dt-bootstrap">
							<div class="html5buttons">
								<div class="dt-buttons btn-group">
									<a class="btn btn-default buttons-copy buttons-html5" tabindex="0"><span>Copiar</span></a>
									<a class="btn btn-default buttons-excel buttons-html5" tabindex="0"><span>Excel</span></a>
									<a class="btn btn-default buttons-print" tabindex="0"><span>Imprimir</span></a>
								</div>
							</div>
						</div>

						<br>

						<table id="indicadoresEmpresa" class="footable table toggle-arrow-tiny table-bordered" data-page-size="5" data-filter="">
							<thead>
								<tr>
									<th>Empresas</th>
									<th class="decJuradas">E-C</th>
									<th class="decJuradas">E-NC</th>
									<th class="decJuradas">D-C</th>
									<th class="decJuradas">D-NC</th>
									<th class="columnHide conectadas">Cumple</th>
									<th class="columnHide conectadas">No Cumple</th>
									<th class="columnHide evaluacion">Lista</th>
									<th class="columnHide evaluacion">Pendiente</th>
									<th class="columnHide seguimiento">Tiene</th>
									<th class="columnHide seguimiento">No Tiene</th>
									<th class="columnHide llamadas">Cumple</th>
									<th class="columnHide llamadas">No Cumple</th>
									<th class="columnHide respuetas">Respuestas</th>
									<th class="columnHide respuetas">Otros Seguimientos</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>VMICA</td>
									<td>10/20</td>
									<td>5/20</td>
									<td>10/20</td>
									<td>18/20</td>
								</tr>
								<tr>
									<td>Edutecno</td>
									<td>10/20</td>
									<td>5/20</td>
									<td>10/20</td>
									<td>18/20</td>
								</tr>
								<tr>
									<td>BHP</td>
									<td>10/20</td>
									<td>5/20</td>
									<td>10/20</td>
									<td>18/20</td>
								</tr>
								<tr>
									<td>VMICA</td>
									<td>10/20</td>
									<td>5/20</td>
									<td>10/20</td>
									<td>18/20</td>
								</tr>
								<tr>
									<td>Edutecno</td>
									<td>10/20</td>
									<td>5/20</td>
									<td>10/20</td>
									<td>18/20</td>
								</tr>
								<tr>
									<td>BHP</td>
									<td>10/20</td>
									<td>5/20</td>
									<td>10/20</td>
									<td>18/20</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="6">
										<ul class="pagination pull-right"></ul>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>

				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-title  back-change">
							<h5>Detalle Alumnos</h5>
						</div>

						<div class="ibox-content table-responsive">

							<h5 style="text-align: right;">Exportar</h5>
							<div class="dataTables_wrapper form-inline dt-bootstrap">
								<div class="html5buttons">
									<div class="dt-buttons btn-group">
										<a class="btn btn-default buttons-copy buttons-html5" tabindex="0"><span>Copiar</span></a>
										<a class="btn btn-default buttons-excel buttons-html5" tabindex="0"><span>Excel</span></a>
										<a class="btn btn-default buttons-print" tabindex="0"><span>Imprimir</span></a>
									</div>
								</div>
							</div>

							<br>

							<table id="indicadoresAlumnos" class="footable table toggle-arrow-tiny table-bordered" data-page-size="5" data-filter="">
								<thead>
									<tr>


										<th>Rut</th>
										<th>Emprea</th>
										<th>Ficha</th>
										<th>Curso</th>
										<th>Fecha Termino</th>
										<th>Costo</th>
										<th>Indicador</th>
										<th>Seguimiento</th>
										<th>Tutor Asignado</th>
										<th>Nombre</th>
										<th>% Registro Sence</th>

										<th class="columnHide">Declaración Jurada</th>
										<th class="columnHide">Plazo Final entrega</th>
										<th class="columnHide">Tutor Asignado</th>
										<th class="columnHide">Evaluación Diagnóstica</th>
										<th class="columnHide">Evaluación 1</th>
										<th class="columnHide">Evaluación 2</th>
										<th class="columnHide">Evaluación n</th>
										<th class="columnHide">Examen Final</th>
										<th class="columnHide">Promedio</th>
										<th class="columnHide">% Evaluaciones Realizadas</th>
										<th class="columnHide">C.T.S</th>
										<th class="columnHide">Indicador</th>
										<th class="columnHide">Seguimientos</th>
										<th class="columnHide">Cant. Seguim. salientes</th>
										<th class="columnHide">Cant. llamadas realizadas PBX</th>
										<th class="columnHide">Cant. Seguim entrantes</th>
										<th class="columnHide">Cant. llamadas recibidas PNX</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>18021570-0</td>
										<td>VMICA</td>
										<td>5</td>
										<td>Excel Básico</td>
										<td>12/02/2018</td>
										<td>CENCE</td>
										<td><i class="fa fa-circle" style="color: red;"></i></td>
										<td>4</td>
										<td>Gabriel Santibañez</td>
										<td>Esteban Valera</td>
										<td>50%</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="24">
											<ul class="pagination pull-right"></ul>
										</td>
									</tr>
								</tfoot>
							</table>

						</div>
					</div>
				</div>
			</div>


		</div>


	</div>
</div>

