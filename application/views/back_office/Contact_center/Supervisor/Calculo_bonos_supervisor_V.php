
<style>
#datatables_buttons_info {
    z-index: 9999;
}
</style>

<div class="wrapper wrapper-content">
    <div class="row animated fadeInDown">
        <div class="col-lg-12">



            <div class="ibox float-e-margins">

                <div class="ibox-title  back-change">
                    <h5>Resultado</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content" id="divResultado">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="row">

                                <div class="col-md-12">

                                    <div class="table-responsive">
                                        <table id="tblResultado" class="table toggle-arrow-tiny table-bordered" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>Tutor</th>
                                                    <th>Bono</th>
                                                    <th>Bono Potencial</th>
                                                    <th>Bono</th>
                                                    <th>Bono Potencial</th>
                                                    <th>Periodo</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (count($CalculoBono) == 0) {
                                                    
                                                }else{
                                                    foreach ($CalculoBono as $b) {
                                                        echo "<tr>
                                                        <td>".$b['tutor']."</td>
                                                        <td>$".number_format($b['actual'],0,',','.')."</td>
                                                        <td>$".number_format($b['meta'],0,',','.')."</td>
                                                        <td>".$b['actual']."</td>
                                                        <td>".$b['meta']."</td>
                                                        <td>" . $b['periodo'] . "</td>
                                                        <td align='center'>
                                                        <button class='btn btn-sm btn-primary detalle' id='" . $b['id_tutor'] . "@".$b['periodo']."'> Detalle</button>
                                                        </td>
                                                        </tr>";
                                                    }
                                                }
                                                ?>										
                                            </tbody>
                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal inmodal" id="modalDetalle" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-xl">
                        <div class="modal-content animated bounceInRight">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-address-card modal-icon"></i>
                                <h4 class="modal-title">Detalle por alumno</h4>
                            </div>
                            <div class="modal-body">

                                <div class="ibox-content">


                                    <div class="row">
                                        <div class="col-md 12">

                                            <table id="tblAlumnosporTutor" class="table toggle-arrow-tiny table-bordered" style="width: 100%;">
                                                <thead>
                                                    <tr>
                                                        <th>Bono</th>
                                                        <th>Bono</th>
                                                        <th>Tutor</th>
                                                        <th>Rut</th>
                                                        <th>Nombre</th>
                                                        <th>Ficha</th>
                                                        <th>Modalidad</th>
                                                        <th>Fecha Inicio</th>
                                                        <th>Fecha Fin</th>
                                                        <th>Costo</th>
                                                        <th>Declaraciones <br>Juradas</th>
                                                        <th>Conexión</th>
                                                        <th>Eval. (%)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>



                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>		



        </div>	
    </div>
</div>

