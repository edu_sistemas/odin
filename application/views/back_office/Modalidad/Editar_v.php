<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-12-02 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar Modalidad</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <form class="form-horizontal" id="frm_modalidad_editar">
                    <p>Modifique Modalidad</p>
                    <div class="form-group"><label class="col-lg-2 control-label">Nombre de la modalidad</label>
                        <div class="col-lg-3">
                            <input type="hidden" name="id_modalidad_v" class="form-control" value="<?php echo $data_modalidad[0]['id_modalidad']; ?>">
                            <input type="text" placeholder="Nombre" requerido="true" mensaje="Debe ingresar nombre del perfil" name="nombre_modalidad_v" class="form-control" value="<?php echo $data_modalidad[0]['nombre_modalidad']; ?>">

                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Descripción de la Modalidad</label>
                        <div class="col-lg-6">
                            <textarea class="form-control" requerido="true" mensaje="Debe ingresar nombre de la Modalidad"
                                      id="descripcion_modalidad_v" name="descripcion_modalidad_v" maxlength="100" 
                                      placeholder="Descripción"><?php echo $data_modalidad[0]['descripcion_modalidad']; ?></textarea>
                          <p id="text-out">0/200 caracteres restantes</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_modalidad" id="btn_editar_modalidad">Actualizar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
