<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">

                        <!-- Text input-->
                        <div class="form-group">

                            <div class="col-md-3">
                                <input id="txt_rut_otic"
                                       name="txt_rut_otic"
                                       type="text" placeholder="78 895 485 - 5"
                                       class="form-control input-md">
                                <p class="help-block">Rut</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">

                            <div class="col-md-3">
                                <input id="txt_nombre_otic"
                                       name="txt_nombre_otic"
                                       type="text" placeholder="Santander"
                                       class="form-control input-md">
                                <p class="help-block">Nombre</p>
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="btn_buscar_otic"></label>
                            <div class="col-md-3">
                                <button type="button" id="btn_buscar_otic" name="btn_buscar_otic" class="btn btn-outline btn-success">
                                    Buscar
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Otic's</h5>
                    <div class="ibox-tools">

                        <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo</button>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_otic" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Rut</th> 
                                    <th>Nombre</th>
                                    <th>Nombre Corto</th>
                                    <th>Direccion</th>
                                    <th>Descripcion</th>
                                    <th>Estado</th>
                                    <th>Modificar</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Rut</th> 
                                    <th>Nombre</th>
                                    <th>Nombre Corto</th>
                                    <th>Direccion</th>
                                    <th>Descripcion</th>
                                    <th>Estado</th>
                                    <th>Modificar</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
