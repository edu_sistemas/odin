<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar Otic</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <form class="form-horizontal" id="frm_otic_registro"  >
                    <p>Modifique Otic</p>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">RUT:</label>
                        <div class="col-lg-4">
                            <?php
                            $rut = $data_otic[0]['rut_otic'];

                            $rut = explode("-", $rut);
                            ?>
                            <input type="hidden" id="id_otic" name="id_otic"                                   
                                   value="<?php echo $data_otic[0]['id_otic']; ?>">
                            <input type="number" class="form-control required"
                                   id="rut" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el RUT correspondiente"
                                   placeholder="Ej. 123456789"
                                   name="rut"
                                   maxlength="8"
                                   class="form-control" onkeypress="return solonumero(event);"
                                   value="<?php echo $rut[0]; ?>"/>

                        </div>
                        <p style="width: 0" class="col-lg-1">-</p>
                        <div class="col-lg-1">
                            <input type="text" class="form-control required"
                                   id="dv" 
                                   maxlength="1" 
                                   requerido="true"
                                   mensaje ="Debe Ingresar el digito verificador"
                                   placeholder="Ej. 0"
                                   name="dv"
                                   class="form-control" 
                                   value="<?php echo $rut[1] ?>"/>
                        </div>
                        <div class="col-lg-3">
                            <p id="out_print" style="color: red"></p>
                        </div>
                    </div>

                    <!--  -->

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-6">
                            <input type="text"
                                   id="nombre_otic"                                   
                                   requerido="true"
                                   mensaje ="Debe Ingresar el nombre"                                    
                                   placeholder="Ingrese el nombre"
                                   name="nombre_otic"
                                   class="form-control"
                                   value="<?php echo $data_otic[0]['nombre_otic']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre Corto:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="nombre_corto_otic" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el nombre corto""
                                   placeholder="Ingrese nombre corto"
                                   name="nombre_corto_otic"
                                   value="<?php echo $data_otic[0]['nombre_corto_otic']; ?>"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Direccion:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="direccion_otic" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el nombre corto""
                                   placeholder="Ingrese nombre corto"
                                   name="direccion_otic"
                                   value="<?php echo $data_otic[0]['direccion_otic']; ?>"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Descripción:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control required"
                                      id="descripcion_otic" requerido="true"
                                      mensaje ="Debe Ingresar una o más observaciones"
                                      name="descripcion_otic"
                                      placeholder="Ingrese observaciones o referencias de la empresa a registrar. Estas observaciones pueden servir para recordar de que empresa se trata en caso que no se recuerde bien..."
                                      maxlength="200"
                                      rows="6"                                      
                                      ><?php echo $data_otic[0]['descripcion_otic']; ?></textarea>
                            <p id="text-out">Quedan 200 caracteres restantes</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_otic" id="btn_editar_otic">Actualizar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

