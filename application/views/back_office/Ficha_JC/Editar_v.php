<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Lusi Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-20 [David De Filippi] <dfilippi@edutecno.com>
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 id="h_text_title_ficha">Editar Ficha</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">

                <form class="form-horizontal" id="frm_ficha_registro">
                    <p> Ingrese una nueva Ficha</p> 
                    <input type="hidden" id="id_ficha" name="id_ficha" value="<?php echo $data_ficha[0]['id_ficha']; ?>">
                    <div class="row" >
                        <div class="col-lg-6">
                            <label class="control-label">Modalidad</label>
                            <input type="hidden" id="id_modalidad" value="<?php echo $data_ficha[0]['id_modalidad']; ?>">
                            <select id="modalidad" name="modalidad"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Modalidad"
                                    accesskey>
                                <option></option>
                            </select>
                        </div>                   
                        <div class="col-lg-6">
                            <label class="control-label">Curso</label>
                            <input type="hidden" id="id_curso" value="<?php echo $data_ficha[0]['id_curso']; ?>">
                            <select id="curso" name="curso"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Curso"
                                    >
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label class=" control-label">Empresa</label>
                            <input type="hidden" id="id_empresa" value="<?php echo $data_ficha[0]['id_empresa']; ?>">
                            <select id="empresa" name="empresa"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Empresa" 
                                    >
                                <option></option>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label class=" control-label">Otic</label>
                            <input type="hidden" id="id_otic" value="<?php echo $data_ficha[0]['id_otic']; ?>">
                            <select id="otic" name="otic"  class="select2_demo_3 form-control"                                    
                                    requerido="true" mensaje="Debe seleccionar OTIC" 
                                    >
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="row"> 
                        <div class="col-lg-6"> 
                            <label class="control-label">Fecha Inicio y Término</label>
                            <div class="input-daterange input-group  col-lg-12 " id="datepicker">    
                                <input type="text" class="input-sm form-control" name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"  
                                       value="<?php echo $data_ficha[0]['fecha_inicio']; ?>"
                                       requerido="true" mensaje="Fecha Inicio es un campo requerido"    

                                       />
                                <span class="input-group-addon">A</span>
                                <input type="text" class="input-sm form-control" name="fecha_termino" id="fecha_termino" placeholder="AAAA-MM-DD"
                                       value="<?php echo $data_ficha[0]['fecha_fin']; ?>"
                                       requerido="true" mensaje="Fecha Término es un campo requerido"   
                                       />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label">Fecha Cierre</label>
                            <div class="input-group date col-lg-12 "> 
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control" name="fecha_cierre" id="fecha_cierre" placeholder="AAAA-MM-DD"
                                       value="<?php echo $data_ficha[0]['fecha_cierre']; ?>"
                                       requerido="true" mensaje="Fecha Cierre es un campo requerido"   />
                            </div>
                        </div>
                    </div> 

                    <div class="row">
                        <div class="col-lg-6"> 
                            <label class="control-label" for="txt_num_o_c">Sin O.C.?</label>  
                            <input type="checkbox" value="" name="chk_sin_orden_compra" id="chk_sin_orden_compra"/>  
                        </div>

                        <div class="col-lg-6">
                            <label class="control-label" for="txt_num_o_c">N° Orden Compra</label>  
                            <input type="hidden" id="id_num_o_c" value="<?php echo $data_ficha[0]['id_orden_compra']; ?>">
                            <input id="txt_num_o_c" name="txt_num_o_c" type="text" placeholder="" class="form-control input-md"                                   
                                   requerido="true" mensaje="Ingresar N° Orden de Compra" 
                                   value="<?php echo $data_ficha[0]['num_orden_compra']; ?>"
                                   data-role="tagsinput"  
                                   />
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-lg-12">        
                            <label class="control-label" for="textarea">Comentario</label>
                            <textarea class="form-control" id="comentario" name="comentario" maxlength="200"></textarea>
                            <p id="text-out">0/1000 caracteres restantes</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>                        
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


