<?php
/**
 * Consulta_v
 * Description...
 * @version 0.0.1
 * Ultima edicion:	2017-21-08 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-21-08 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
				<form role="form" class="form-inline" id="frm" name="frm" method="post">
					<div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>Seleccione mes para generar Informe</label>
                                <div>
                                    <select class="form-control" id="mes" name="mes">

                                    </select>

                                </div>
								<div>
									<button  type="button" id="generar" name="btn_limpiar_form" class="col-lg-6 btn btn-warning pull-right"style="margin-top: -30px;" >Generar</button> 

								</div>
                            </div>
                        </div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="ibox-content">
	<div class="table-responsive">
		<table id="tbl_fichas" class="table table-striped table-bordered table-hover dataTables-example" >
			<thead>
				<tr>
					<th>Ficha</th>
					<th>Estado</th>
					<th>F. Inicio</th>
					<th>F. Fin</th>
					<th>F. Rectificacion</th>
					<th>Curso</th>
					<th>Empresa</th>
				</tr>
			</thead>

			<tfoot>
				<tr>
					<th>Ficha</th>
					<th>Estado</th>
					<th>F. Inicio</th>
					<th>F. Fin</th>
					<th>F. Rectificacion</th>
					<th>Curso</th>
					<th>Empresa</th>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

<style>
	.dataTables_filter {
		display: none; 
	}

	.html5buttons{
		display: none; 
	}

	.dataTables_length{
		margin-right: 10px !important; 
	}

</style>

