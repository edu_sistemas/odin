 <?php
/**
 * Agregar_v3
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-07 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-02-07 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div  class="row">
                    <div  class="col-lg-6">
                        <h5>Módulo de Calificaciones e-Learning</h5>
                    </div>
                   <!-- <div class="col-lg-6" style="text-align: right"><a target="_blank" href="../../documentacion/nota.html">Necesito ayuda con esto</a></div> -->
                </div>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div id="mensaje"></div>
                 <div id="mensaje2"></div>
                <p>Notas de los alumnos modalidad e-learning</p>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Seleccionar ficha</label>
                
                    <div class="col-md-3" >
                        <select id="id_ficha_v" name="id_ficha_v"  class="select2_demo_1 form-control">
                            <option></option>
                        </select>
                    </div>
                          <button type="reset" id="reset_superior" class="btn btn-info col-lg-2" data-dismiss="modal">Volver</button>
                    <label class="col-lg-2" style="margin-left: 100px;">Agregar/Quitar Evaluación</label>
                    <div class="col-md-2" >
                        <button class="btn btn-primary" type="button" data-column="4" id="n2" onclick="AgregarNotaN()"> <i class="fa fa-plus-square"></i></button>
                        <button class="btn btn-primary" type="button" data-column="4" id="n3" onclick="QuitarNotaN()"> <i class="fa fa-minus-square"></i></button>
                    </div>
                </div>
                <hr>
                <br>
                <form method="post" class="form-horizontal" id="frm_notas_registro">
                    <input type="hidden" id="total" name="total">               <!--  cuenta el total de alumnos -->
                    <input type="hidden" id="columnas" name="columnas">         <!--  cuenta las cantida de columnas de notas --> 
                    <input type="hidden" id="largo" name="largo">
                    <input type="hidden" id="cierre" name="cierre">             <!--  cuenta la cantidad de notas que ya estan ingresadas -->
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="nota" class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr id="row">
                                        <th>Rut</th> 
                                        <th>Nombre</th>
                                        <th>Curso</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Volver</button>
                        <button type="button" id="guardarN" class="btn btn-primary">Guardar Notas</button>
                        <button type="button" id="cierresend" class="btn btn-warning">Cerrar curso</button>
                        <br><br>
                    </div>
                </form>
            </div>
        </div> 
    </div>
</div>