<?php
/**
 * Agregar_v3
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2017-08-29 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-08-29 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div  class="row">
                    <div  class="col-lg-6">
                        <h5>Módulo de Calificaciones</h5><br>
                    </div> 
                </div>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="fichas" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>N° Ficha</th> 
                                    <th>Curso</th> 
                                    <th>Duración</th>
                                    <th>Empresa</th> 
                                    <th>Acción</th> 
                                </tr>

                            </thead>
                            <tfoot>
                                <tr>
                                    <th>N° Ficha</th> 
                                    <th>Curso</th> 
                                    <th>Duración</th>
                                    <th>Empresa</th> 
                                    <th>Acción</th> 
                                </tr>

                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                </div> 
            </div>
        </div> 
    </div>
</div>

