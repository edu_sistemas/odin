<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2018-05-07 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<style>
    #cargando {
        width: 100%;
    }

    #cargando img {
        margin-left: auto;
        margin-right: auto;
        display: block;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <!-- content -->
                    <ul class="nav nav-pills">
                        <li class="active">
                            <a data-toggle="pill" href="#home">Agregar Usuario</a>
                        </li>
                        <li>
                            <a data-toggle="pill" href="#menu1">Agregar Ficha</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">
                            <br>
                            <h3>Agregar Usuario</h3>
                            <br>
                            <form id="frm_agregar_usuario">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="">Tipo Usuario</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="radio" name="tipo_usuario" value="alumno" id="alumno" checked>
                                        <label for="alumno"> Alumno</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="radio" name="tipo_usuario" value="edutecno" id="edutecno">
                                        <label for="edutecno"> Edutecno</label>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="">Trivia</label>
                                    </div>
                                    <div class="col-lg-10">
                                        <select id="id_trivia" name="id_trivia" class="select2_demo_3 form-control" style="width: 100%" required>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="">RUT</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <input type="text" name="rut" id="rut" style="width: 100%" maxlength="8" onkeypress="return solonumero(event)" required>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="text" name="dv" id="dv" style="width: 100%" maxlength="1" required>
                                    </div>
                                </div>
                                <br>
                                <div class="seleccion-alumno">
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label for="">Ficha</label>
                                        </div>
                                        <div class="col-lg-10">
                                            <select id="id_ficha" name="id_ficha" class="select2_demo_3 form-control" style="width: 100%" required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <label for="">Alumno</label>
                                        </div>
                                        <div class="col-lg-10">
                                            <select id="id_dtl_alumno" name="id_dtl_alumno" class="select2_demo_3 form-control" style="width: 100%" required>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4">
                                        <button class="btn btn-success">Agregar Usuario</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="menu1" class="tab-pane fade">
                            <br>
                            <h3>Agregar Ficha</h3>
                            <br>
                            <form id="frm_agregar_ficha">
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="">Trivia</label>
                                    </div>
                                    <div class="col-lg-10">
                                        <select id="id_trivia_2" name="id_trivia" class="select2_demo_3 form-control" style="width: 100%" required>
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <label for="">Ficha</label>
                                    </div>
                                    <div class="col-lg-8">
                                        <select id="id_ficha_2" name="id_ficha" class="select2_demo_3 form-control" style="width: 100%" required>
                                            <option></option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <button type="button" class="btn btn-success" id="btn_seleccionar_ficha">
                                            Seleccionar
                                        </button>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="tbl_alumnos" class="table" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        <input type="checkbox" name="select_all" value="1" id="tbl_alumnos-select-all">
                                                    </th>
                                                    <th>RUT</th>
                                                    <th>Nombre</th>
                                                    <th>Apellido Paterno</th>
                                                    <th>Apellido Materno</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th>RUT</th>
                                                    <th>Nombre</th>
                                                    <th>Apellido Paterno</th>
                                                    <th>Apellido Materno</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <table id="tbl_output" style="width: 50%">
                                        </table>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-2"></div>
                                    <div class="col-lg-4">
                                        <button class="btn btn-success">Agregar Usuarios</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="menu2" class="tab-pane fade">
                            <h3>Menu 2</h3>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                                totam rem aperiam.</p>
                        </div>
                        <div id="menu3" class="tab-pane fade">
                            <h3>Menu 3</h3>
                            <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>