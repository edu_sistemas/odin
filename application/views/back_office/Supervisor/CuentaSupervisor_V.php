<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight"> 
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="wrapper wrapper-content animated fadeInRight">
					<div class="row">
						<div class="col-lg-12">
							<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Busqueda</h5>
									<div class="ibox-tools">
										<a class="collapse-link">
											<i class="fa fa-chevron-up"></i>
										</a>
									</div>
								</div>
								<div class="ibox-content">

									<form role="form" class="form" id="frm" name="frm">
										
													<div class="row">
														<div class="col-lg-6">
															<label for="id_empresa">Empresa</label>
															<select id="id_empresa" name="id_empresa" class="select2_demo_3 form-control" style="width: 100%">
																<option></option>
															</select>
														</div>
													
													<div class="col-lg-6">
														<label for="id_ejecutivo">Ejecutivo</label>
														<select id="id_ejecutivo" name="id_ejecutivo" class="select2_demo_3 form-control" style="width: 100%">
															<option></option>
														</select>
													</div>
												</div>
												<br>
												<div class="row">
													<div class="col-lg-6">
														<label>Mes</label>
														<select class="form-control" id="mes" name="mes">	
															<option value="" selected="selected">Seleccione</option>

                                   						 </select>
													</div>
												
												<div class="col-lg-6">
													<button type="button" id="btn_buscar" name="btn_buscar" class="btn btn-outline btn-success">Buscar</button>
													<p class="help-block">&nbsp; </p>
												</div>
											</div>

									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
								<div class="ibox-title">
									<h5>Gestiones Cliente - Ejecutivo</h5>
									<button type="button" name="btn_generar_excel" id="btn_generar_excel" class="btn btn-outline btn-primary" style="margin-top:-8px; margin-left: 10px;"><i class="fa fa-download"></i>Descargar</button>
										
								</div>
							
							<div class="ibox-content">
								<div class="table-responsive" style="padding: 20px !important;">
				
				
									<table id="tbl_cuenta" class="table table-striped table-bordered table-hover dataTables-example">
										<thead>
											<tr>
												<th>Fecha</th>
												<th>Cliente</th>
												<th>Holding</th>
												<th>Status Gestion</th>
												<th>Estado Cuenta</th>
												<th>Rut Empresa</th>
												<th>Ejecutivo</th>
												<th></th>
												
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Fecha</th>
												<th>Cliente</th>
												<th>Holding</th>
												<th>Status Gestion</th>
												<th>Estado Cuenta</th>
												<th>Rut Empresa</th>
												<th>Ejecutivo</th>
												<th></th>
												
												
											</tr>
										</tfoot>
									</table>
								
							</div>
						</div>
					</div>
				</div>
						<div class="col-lg-12 col-xs-12">
							<div class="ibox-content m-b-sm border-bottom">
								<div class="row">
									<div class="col-md-4">
										<h4>Historial de Gestiones</h4>
									</div>  
									<div class="col-md-4">
										<div class="form-group">
											<input type="text" id="empresa_nombre" name="empresa_nombre" value="" placeholder="Filtrar Cliente" class="form-control">
										</div>
									</div>
								</div>
							</div>

						

									<div class="table-responsive col-lg-6 col-xs-12" style="padding:0px;">
										<div class="ibox-content">
											<h5 style="text-align: center;">Exportar Gestiones</h5>
											<table style="display: none" id="tbl_gestion">
												<thead>
													<tr>
														<th>Ejecutivo</th>
														<th>Empresa</th>
														<th>Holding</th>
														<th>Evento</th>
														<th>Asunto</th>
														<th>Fecha Evento</th>
														<th>Comentario</th>
														<th>Fecha Compromiso</th>
														<th>Compromiso</th>
														<th>Fase</th>
														<th>Tipo accion</th>
														<th>Producto</th>
														
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Ejecutivo</th>
														<th>Empresa</th>
														<th>Holding</th>
														<th>Evento</th>
														<th>Asunto</th>
														<th>Fecha Evento</th>
														<th>Comentario</th>
														<th>Fecha Compromiso</th>
														<th>Compromiso</th>
														<th>Fase</th>
														<th>Tipo accion</th>
														<th>Producto</th>
														
													</tr>
												</tfoot>
											</table>
										</div>
									</div>

									<div class="table-responsive col-lg-6 col-xs-12" style="padding:0px ">
										<div class="ibox-content">
											<h5 style="text-align: center;">Exportar Dominios sin Asignar</h5>
											<table style="display: none" id="tbl_dominio_sin_asignar">
												<thead>
													<tr>
														<th>Dominio</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>Dominio</th>
													</tr>
												</tfoot>
											</table>
										</div>
									</div>	

							<div class="row">
								<div class="col-lg-12">
									<div class="ibox float-e-margins">
							
										<div class="ibox-content">
											<div class="table-responsive" style="padding: 20px !important;">
							
							
												<table id="tbl_gestion_empresa" class="table table-striped table-bordered table-hover dataTables-example" data-filter="#empresa_nombre">
													<thead>
														<tr>
															<th>Tipo</th>
															<th>Cliente</th>
															<th>Holding</th>
															<th>Ejecutivo</th>
															<th>Evento</th>
															<th>Asunto</th>
															<th>Fecha</th>
															<th>Comentario</th>
															<th>Fecha Compromiso</th>
															<th>Compromiso</th>
															<th>Fase</th>
															<th>Tipo accion</th>
															<th>Producto</th>
														
															
														</tr>
													</thead>
													<tfoot>
														<tr>
															<th>Tipo</th>
															<th>Cliente</th>
															<th>Holding</th>
															<th>Ejecutivo</th>
															<th>Evento</th>
															<th>Asunto</th>
															<th>Fecha</th>
															<th>Comentario</th>
															<th>Fecha Compromiso</th>
															<th>Compromiso</th>
															<th>Fase</th>
															<th>Tipo accion</th>
															<th>Producto</th>
															
															
												
														</tr>
													</tfoot>
												</table>
							
											</div>
										</div>
									</div>
								</div>

					</div>
					</div>
					</div>


				</div>
<div class="modal inmodal" id="mdlListaClientes" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog  modal-lg">
  <div class="modal-content animated bounceInRight">

    <!--MODAL HEADER-->
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <i class="fa fa-file-text modal-icon"></i>
        <h4 class="modal-title">Clientes Edutecno</h4>
        <small class="font-bold">Detalle de clientes por asignaciones.</small>
    </div>
    <!--FIN MODAL HEADER-->

    <!--MODAL BODY-->
    <div class="modal-body">
    
         <div class="table-responsive">
         	<table  class="table table-striped" style="width: 100%" id="tbl_clientes_ejecutivos">
         		<thead>
         			<tr>
         				<th>Cliente</th>
         				<th>Ejecutivo</th>
         				<th>Gestion</th>
                        <th>Rut_Empresa</th>
         			</tr>
         		</thead>
         		<tbody>
         			
         		</tbody>
         	</table>
         </div>
    </div>
    <!--FIN MODAL BODY-->
    
    <!--MODAL FOOTER-->
    <div class="modal-footer">                                            
        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>  
    </div>
    <!--FIN MODAL FOOTER-->
  </div>
</div>
</div>