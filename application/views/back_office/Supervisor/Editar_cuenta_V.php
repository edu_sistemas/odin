<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="ibox-title">
        <h5>Ver Cuenta</h5>
    </div>
    <div class="row m-t-lg">    
        <div class="col-md-7">            
            <div class="tabs-container">

                <div class="tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active"  data-toggle="tooltip" data-placement="bottom" title="Detalle de la Cuenta"><a data-toggle="tab" href="#tab-1"><i class="fa fa-eye fa-2x"></i></a></li>
                        <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos para Facturación"><a data-toggle="tab" href="#tab-3"><i class="fa fa-pencil-square-o fa-2x"></i></a></li>
                        <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Entrega Factura"><a data-toggle="tab" href="#tab-4"><i class="fa fa-check-square-o fa-2x" ></i></a></li>
                        <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Finanzas"><a data-toggle="tab" href="#tab-5"><i class="fa fa-area-chart fa-2x" ></i></a></li>
                        <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Entrega de Diplomas"><a data-toggle="tab" href="#tab-6"><i class="fa fa-mortar-board fa-2x" ></i></a></li>
                    </ul>
                    <form class="" id="form-editar-cuenta" name="form-editar-cuenta"> 
                        <input type="hidden" value="<?php echo $datos_empresa[0]['id_empresa'];?>" name="Var_id_empresa" id="Var_id_empresa">					
                        <div class="tab-content ">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <strong>Detalle de la Cuenta</strong>
                                    <h5>Datos Empresa</h5>
                                    <hr>
									<!--PRIMERA COLUMNA -->
                                    <div class="col-md-6">  
									
										 <div class="form-group">
											 <label><small>Nombre de Fantasía</small></label> 
											 <input type="text" 
											 id="Var_nombre_fantasia" 
											 name="Var_nombre_fantasia" 
											 placeholder="Ingrese Nombre Fantasía" 
											 class="form-control" 
											 value="<?php echo $datos_empresa[0]['nombre_fantasia'];?>"
											 style="border: 1px solid #aaaaaa;" >
										 </div>

										 <div class="form-group">
											 <label><small>Razón Social (*)</small></label> 
											 <input type="text" 
											 id="Var_razon_social_empresa" 
											 name="Var_razon_social_empresa" 
											 placeholder="Ingrese Razon Social" 
											 class="form-control" requerido="true" 
											 mensaje="Ingrese Razón Social" 
											 value="<?php echo $datos_empresa[0]['razon_social_empresa'];?>"
											 style="border: 1px solid #aaaaaa;" >
										 </div>

										 <div class="form-group">
											<label><small>Giro de Empresa</small></label> 
											<input type="text" 
											id="Var_giro_empresa" 
											name="Var_giro_empresa" 
											placeholder="Ingrese Giro de Empresa" 
											class="form-control" 
											value="<?php echo $datos_empresa[0]['giro_empresa'];?>"
											style="border: 1px solid #aaaaaa;" >
										</div>

										<div class="form-group" style="margin-bottom: 0px;">
											<div class="row">
												 <label class="control-label" style="padding-left: 14px" ><small>Rut (*)</small></label>
												<div class="col-md-12">											
												  <div class="row">	
													<div class="col-md-8" style="padding-right: 0">	
														<?php
														$rut = $datos_empresa[0]['rut_empresa'];

														$rut = explode("-", $rut);
														?>
														
														<input type="hidden" id="id_edutecno" name="id_edutecno"                                   
															   value="<?php echo $datos_empresa[0]['rut_empresa']; ?>">	
														   
														<input type="number" class="form-control required"
															   id="Var_rut_empresa" 
															   requerido="false"
															   mensaje ="Debe Ingresar el RUT correspondiente"
															   placeholder="Ej. 123456789"
															   name="Var_rut_empresa"
															   maxlength="8"
															   class="form-control" onkeypress="return solonumero(event)"
															   value="<?php echo $rut [0] ?>"
															   style="border: 1px solid #aaaaaa;" >
													</div>
													<p style="width: 0; padding:7px" class="col-md-1">-</p>
														<div class="col-md-3" style="padding-left: 0">
															<input type="text" class="form-control required"
																   id="dv" 
																   maxlength="1" 
																   requerido="true"
																   mensaje ="Debe Ingresar el digito verificador"
																   placeholder="Ej. 0"
																   name="dv"
																   class="form-control" 
																   value="<?php echo $rut [1] ?>"
																   style="border: 1px solid #aaaaaa;" >
														</div>
													
												</div>	
											   </div>
											</div>			
										</div>
										 

										 <div class="form-group">
											 <label><small>Clasificación de la Cuenta</small></label>
											 <input id="hidden_id_clasificacion_cuenta" 
											 type="hidden" 
											 value="<?php echo $datos_empresa[0]['crm_clasificacion_cuenta'];?>" >										 										 
											 
											 <select class="form-control" 
											 id="Var_crm_clasificacion_cuenta" 
											 name="Var_crm_clasificacion_cuenta" 
											 requerido="true" >
											 </select>
										 </div>

										 <div class="form-group">
											 <label><small>Dirección Empresa (*)</small></label> 
											 <input 
											 type="text" 
											 id="Var_direccion_empresa" 
											 name="Var_direccion_empresa" 
											 placeholder="Ingrese Dirección" 
											 class="form-control" 
											 requerido="true" 
											 mensaje="Ingrese Dirección" 
											 value="<?php echo $datos_empresa[0]['direccion_empresa'];?>"
											 style="border: 1px solid #aaaaaa;" >
										 </div>
										 
										 <div class="form-group">
											<label><small>País</small></label> 
											<input type="hidden"
											id="hidden_id_pais_cuenta"  
											value="<?php echo $datos_empresa[0]['crm_pais_cuenta'];?>" >
											
											<select class="form-control" 
											id="Var_crm_id_pais" 
											name="Var_crm_id_pais">											
											</select>
										 </div>

										 <div class="form-group">
											<label><small>Comuna</small></label> 
											<input type="hidden"
											id="hidden_id_comuna"  
											value="<?php echo $datos_empresa[0]['id_comuna'];?>" >
											
											<select class="form-control" 
											id="Var_crm_id_comuna" 
											name="Var_crm_id_comuna">											
											</select>
										 </div>										 
										 
										
											   <br>
											   
										 <div class="form-group">
											<label><small>Descripción de la Cuenta</small></label> 
											<textarea name="Var_descripcion_empresa" 
											id="Var_descripcion_empresa" 
											cols="30" rows="2" 
											style="width: 100%; border: 1px solid #aaaaaa;" >
											<?php echo $datos_empresa[0]['crm_descripcion_cuenta'];?></textarea>
										</div>

											
                                    </div>
								<!--FIN PRIMERA COLUMNA -->
									
								<!--SEGUNDA COLUMNA -->
									<div class="col-md-6"> 
										
											<div class="form-group" style="margin-bottom: 0px;">
												<label><small>OTIC Relacionada</small></label> 
												<input 
												type="text" 
												id="Var_crm_otic_cuenta"  
												name="Var_crm_otic_cuenta" 
												placeholder="Ingrese OTIC" 
												class="form-control" 
												style="border: 1px solid #aaaaaa;"
												value="<?php echo $datos_empresa[0]['crm_otic_cuenta'];?>" >
											</div>

										

											<br>
											
											<div class="form-group" style="margin-bottom: 0px;">
												<label for=""><small>Holdding Perteneciente (*)</small></label>
												<input 
												id="hidden_id_holding" 
												type="hidden" 
												value="<?php echo $datos_empresa[0]['id_holding'];?>" >
												
												<select class="form-control holding" 
												id="Var_id_holding" 
												name="Var_id_holding" 
												requerido="true" 
												mensaje="Seleccione Holdding">                                                                        
												</select>
											</div>

									   <br>
											
											<div class="form-group" style="margin-bottom: 0px;">
											   <label for=""><small>Tamaño de la Empresa (*)</small></label>
											   <input id="hidden_crm_tamano_empresa" 
											   type="hidden" 
											   value="<?php echo $datos_empresa[0]['crm_tamano_empresa'];?>" >
											   
											   <select class="form-control" 
											   id="Var_crm_tamano_empresa" 
											   name="Var_crm_tamano_empresa" 
											   requerido="true" 
											   mensaje="Seleccione Tamaño Empresa">
											   </select>
										   </div>   
										   
									   <br>
									   
											<div class="form-group">
												<label><small>Rubro Empresa (*)</small></label>
												<input id="hidden_detalle_cuenta_rubro" 
												type="hidden" 
												value="<?php echo $datos_empresa[0]['crm_rubro'];?>" >
												
												<select class="form-control" 
												id="tab1_detalle_cuenta_rubro" 
												name="tab1_detalle_cuenta_rubro" 
												requerido="true" 
												mensaje="Seleccione Rubro Empresa">
												</select>
											</div>
										
											<div class="form-group">
												<label><small>Subrubro Empresa</small></label>
												<input id="hidden_detalle_cuenta_subrubro" 
												type="hidden" 
												value="<?php echo $datos_empresa[0]['crm_subrubro'];?>" >
												
												<select class="form-control" 
												id="tab1_detalle_cuenta_subrubro" 
												name="tab1_detalle_cuenta_subrubro" 
												requerido="true" 
												mensaje="Seleccione Subrubro Empresa">
												</select>
											</div>
										
									</div>
								<!--FIN SEGUNDA COLUMNA -->
						</div>  
					                     
                    </div>

                    <div id="tab-3" class="tab-pane">
                        <div class="panel-body">
                            <strong>Datos para Facturación</strong>
                            <hr>

                            <div class="col-md-6">                                             
                                <div class="form-group">
									<label><small>Dirección Despacho Factura</small></label> 
									<input type="text" 
									id="Var_direccion_factura" 
									name="Var_direccion_factura" 
									placeholder="Ingrese Dirección" 
									class="form-control" 
									value="<?php echo $datos_empresa[0]['direccion_factura'];?>"
									style="border: 1px solid #aaaaaa;">
								</div>

                                <div class="form-group"><label><small>Empresa Trabaja con OC Interna</small></label>
                                    <div>
										<label> 
											<input type="radio" 
											id="Var_crm_oc_interna" 
											name="Var_crm_oc_interna" 
											value="1" <?php if($datos_empresa[0]['crm_oc_interna']=="1") { echo ' checked="checked" ';}?>>Si</label>
									</div>
									
                                    <div>
										<label> 
											<input type="radio" 
											id="Var_crm_oc_interna" 
											name="Var_crm_oc_interna" 
											value="0" <?php if($datos_empresa[0]['crm_oc_interna']=="0") { echo ' checked="checked" ';}?>>No</label>
									</div>
									
                                </div>


                                <div class="form-group">
									<label><small>Empresa Trabaja con OTA</small></label>
									<div>	
											<label> 
											<input type="radio" 
											id="Var_requiere_ota" 
											name="Var_requiere_ota" 
											value="1" <?php if($datos_empresa[0]['requiere_ota']=="1") { echo ' checked="checked" ';}?>>Si</label>
									</div>
									
									<div>
											<label> 
											<input type="radio" 
											id="Var_requiere_ota" 
											name="Var_requiere_ota" 
											value="0" <?php if($datos_empresa[0]['requiere_ota']=="0") { echo ' checked="checked" ';}?>>No</label>
									</div>
								</div>
                            </div>

							<div class="col-md-6"> 

								<div class="form-group"><label><small>Empresa Trabaja con HES</small></label>

									<div>
										<label> 
										<input type="radio" 
										id="Var_requiere_hess" 
										name="Var_requiere_hess" 
										value="1" <?php if($datos_empresa[0]['requiere_hess']=="1") { echo ' checked="checked" ';}?>>Si</label>
									</div>
									
									<div>
										<label> 
										<input type="radio" 
										id="Var_requiere_hess" 
										name="Var_requiere_hess" 
										value="0" <?php if($datos_empresa[0]['requiere_hess']=="0") { echo ' checked="checked" ';}?>>No</label>
									</div>

								</div>

								<div class="form-group">
									<label><small>Otro (Describa)</small></label> 
									<textarea name="Var_observaciones_glosa_facturacion" 
									id="Var_observaciones_glosa_facturacion" 
									cols="30" rows="2" 
									style="width: 100%; border: 1px solid #aaaaaa;"><?php echo $datos_empresa[0]['crm_otro'];?></textarea>									
								</div>
								
							</div>


                    </div>
                </div>

                <div id="tab-4" class="tab-pane">
                    <div class="panel-body">
					
                        <strong>Datos Entrega de Factura</strong>
                        <hr>
						
                        <div class="form-group">
						
                            <h5>Seleccione una o más opciones.</h5>

                            <label class="checkbox-inline"> 
								<input type="checkbox" 
								value="1" 
								id="Var_crm_sii" 
								name="Var_crm_sii" <?php if($datos_empresa[0]['crm_sii']=="1") { echo ' checked="checked" ';}?>> Sólo SII </label> 								
								<br>
                            <label class="checkbox-inline"> 
								<input type="checkbox" 
								value="1" 
								id="Var_crm_mano_timbrada" 
								name="Var_crm_mano_timbrada" <?php if($datos_empresa[0]['crm_mano_timbrada']=="1") { echo ' checked="checked" ';}?>> Mano (Timbre) </label> 
								<br>
                            <label class="checkbox-inline"> 
								<input type="checkbox" 
								value="1" 
								id="Var_crm_chilexpress" 
								name="Var_crm_chilexpress" <?php if($datos_empresa[0]['crm_chilexpress']=="1") { echo ' checked="checked" ';}?>> Chilexpress </label>

                        </div>
                    </div>
                </div>

                <div id="tab-5" class="tab-pane">
                    <div class="panel-body">
                        <strong>Datos Cobranza</strong>                 
                        <h5>Datos Persona de Contacto para Cobranza</h5>                         
                        <hr>
                        <div class="col-md-6">                                             
                            <div class="form-group">
								<label><small>Nombre Contacto Cobranza</small></label> 
								<input type="text" 
								id="Var_nombre_contacto_cobranza" 
								name="Var_nombre_contacto_cobranza" 
								placeholder="Ingrese Contacto Cobranza" 
								class="form-control" 
								style="border: 1px solid #aaaaaa;"
								value="<?php echo $datos_empresa[0]['nombre_contacto_cobranza'];?>">
							</div>
							
                            <div class="form-group">
								<label><small>Teléfono</small></label> 
								<input type="text" 
								id="Var_telefono_contacto_cobranza" 
								name="Var_telefono_contacto_cobranza" 
								placeholder="Ingrese Teléfono" 
								class="form-control" 
								style="border: 1px solid #aaaaaa;"
								value="<?php echo $datos_empresa[0]['telefono_contacto_cobranza'];?>">
							</div>
							
                         
                        </div>

                        <div class="col-md-6"> 
						
                            <div class="form-group">
								<label><small>Correo</small></label> 
								<input type="email" 
								id="Var_mail_contacto_cobranza" 
								name="Var_mail_contacto_cobranza" 
								placeholder="Ingrese Correo" 
								class="form-control" 
								style="border: 1px solid #aaaaaa;"
								value="<?php echo $datos_empresa[0]['mail_contacto_cobranza'];?>">
							</div>

                        </div>
                    </div>
                </div>

                <div id="tab-6" class="tab-pane">
                    <div class="panel-body">
                        <strong>Datos Entrega de Diplomas</strong>
                        <hr>
                        <div class="col-md-6"> 
						
                            <div class="form-group">
								<label><small>Contacto Persona Diploma</small></label> 
                                <input type="text" 
								id="Var_crm_contacto_persona_diploma"  
								name="Var_crm_contacto_persona_diploma" 
								placeholder="Ingrese Contacto" 
								class="form-control" 
								style="border: 1px solid #aaaaaa;"
								value="<?php echo $datos_empresa[0]['crm_contacto_persona_diploma'];?>">
							</div>	
							
                            <div class="form-group">
								<label><small>Dirección para Entrega Diplomas</small></label> 
								<input type="text" 
								id="Var_crm_direccion_entrega_diploma" 
								name="Var_crm_direccion_entrega_diploma" 
								placeholder="Ingrese Dirección" 
								class="form-control" 
								style="border: 1px solid #aaaaaa;"
								value="<?php echo $datos_empresa[0]['crm_direccion_entrega_diploma'];?>">
							</div>
							
                            <div class="form-group">
								<label><small>Teléfono Oficina</small></label> 
								<input type="text" 
								id="Var_crm_telefono_oficina_diploma"  
								name="Var_crm_telefono_oficina_diploma" 
								placeholder="Ingrese Teléfono" 
								class="form-control bfh-phone" 
								data-format="+56 (d) ddd-dddd" 
								style="border: 1px solid #aaaaaa;"
								value="<?php echo $datos_empresa[0]['crm_telefono_oficina_diploma'];?>">
							</div>
							
                        </div>

                        <div class="col-md-6">                                            
                            <div class="form-group">
								<label><small>Teléfono Móvil</small></label> 
								<input type="text" 
								id="Var_crm_telefono_movil_diploma" 
								name="Var_crm_telefono_movil_diploma" 
								placeholder="Ingrese Móvil" class="form-control bfh-phone" 
								data-format="+56 (d) dddddddd" 
								style="border: 1px solid #aaaaaa;"
								value="<?php echo $datos_empresa[0]['crm_telefono_movil_diploma'];?>">
							</div>
							
                            <div class="form-group">
								<label><small>Correo</small></label> 
								<input type="email" 
								id="Var_crm_correo_diploma" 
								name="Var_crm_correo_diploma" 
								placeholder="Ingrese Correo" 
								class="form-control" 
								style="border: 1px solid #aaaaaa;"
								value="<?php echo $datos_empresa[0]['crm_correo_diploma'];?>">
							</div>

                        </div>

                    </div>                                    
                </div>
                  <br>
				  
            </form>

               


         </div>
     </div>

 </div>
</div>

            <div class="row">
                <div class="col-md-5">

                    <div class="ibox">
                        <div class="ibox-content" id="ContenedorContactos">

                            <table class="table table-stripped">
                                <thead>
                                    <tr>
                                        <th>Nivel Contacto</th>
                                        <th>Contacto</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                   ?>
                                </tbody>
                             </table>

                         </div>
                    </div>


</div>
<div class="modal inmodal" id="modalcontactos" name="modalcontactos" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <!-- MODAL HEADER -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-user modal-icon"></i>
                <h4 class="modal-title">Ver Datos de Contacto</h4>
                <small class="font-bold">Información.</small>
            </div>
            <!-- MODAL BODY -->
            <div class="modal-body col-md-12">
                <form role="form" id="form-contactos">
                    <input type="hidden" name="idcontacto" id="idcontacto">
                    <input type="hidden" name="idempresa" id="idempresa" value="<?php echo $datos_empresa[0]['id_empresa'];?>">
                    <h5>Datos Persona de Contacto</h5>
                    <div class="col-md-6">
                        <div class="form-group">
                            <!-- HIDDEN -->
                            <input id="hidden_correo_sug" 
                            type="hidden" 
                            value="<?php echo $datos_empresa[0]['correo'];?>">

                            <input id="hidden_nombre_sug" 
                            type="hidden" 
                            value="<?php echo $datos_empresa[0]['nombre'];?>">

							<!-- HIDDEN -->

                            <label>Nombre(*)</label>

                            <input type="text" placeholder="Ingrese Nombre" class="form-control" id="txtnombre" name="txtnombre" requerido="true" mensaje="Ingrese Nombre" readonly>
                        </div>
                        <div class="form-group">
                            <label>Cargo (*)</label>
                            <input type="text" placeholder="Ingrese Cargo" class="form-control" id="txtcargo" name="txtcargo" requerido="true" mensaje="Ingrese Cargo" readonly>
                        </div>
                        <div class="form-group">
                            <label>Departamento (*)</label>
                            <input type="text" placeholder="Ingrese Departamento" class="form-control" id="txtdepto" name="txtdepto" requerido="true" mensaje="Ingrese Departamento" readonly>
                        </div>
                        <div class="form-group">
                            <label>Correo Electrónico (*)</label> 
                            <input type="email" placeholder="Ingrese Email" class="form-control" id="txtmail" name="txtmail" requerido="true" mensaje="Ingrese Correo Electronico" readonly>
                        </div>
                        <div class="form-group">
                           <label>Cantidad de Hijos</label> 
                           <input type="number" placeholder="" class="form-control" id="txtcantidad" name="txtcantidad" value="0" min="0" readonly>
                       </div>
                       <div class="form-group">
                        <label>Descripción del Contacto</label>
                        <textarea name="txtdescripcion" id="txtdescripcion" cols="30" rows="2" style="width: 100%;" disabled></textarea>
                    </div>
                </div>

                <div class="col-md-6">
                    <div> <!--MENU SELECT -->
                        <label for="">Nivel</label>
                        <input type="number" placeholder="Ingrese Cantidad" class="form-control" id="txtnivel" name="txtnivel"  readonly="readonly">

                    </div> <br>
                    <div class="form-group">
                        <label>Estado de Contacto (*)</label>
                        <select class="form-control" id="txtestado" name="txtestado" requerido="true" mensaje="Ingrese Estado Contacto" readonly>
                            <option value="1">Activo</option>
                            <option value="0">Inactivo</option>
                        </select>

                    </div>
                    <div class="form-group">
                        <label>Teléfono de Oficina (*)</label>
                        <input type="text" placeholder="Ingrese Télefono" class="form-control" id="txtfonooficina" name="txtfonooficina" requerido="true" mensaje="Ingrese Teléfono de Oficina" readonly>
                    </div>
                    <div class="form-group">
                        <label>Teléfono Móvil</label>
                        <input type="text" placeholder="Ingrese Móvil" class="form-control" id="txtfonomovil" name="txtfonomovil" readonly>
                    </div>
                    <div class="form-group" id="data_1">
                        <label class="font-normal">Fecha Cumpleaños</label>
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" id="txtcumpleanos" name="txtcumpleanos" readonly>
                      </div>
                  </div>
                  <div class="form-group">
                    <label>Fax</label>
                    <input type="text" placeholder="Ingrese Fax" class="form-control" id="txtfax" name="txtfax" readonly>
                </div>
            </div>                          

        </form>   
    </div>
    <!-- MODAL FOOTER -->
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        
    </div>

</div>
</div>
</div>
</div>
