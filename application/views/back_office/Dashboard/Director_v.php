<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<div class="row">
    <div class="row">

        <!--              <a href="Director/senMailtest">test mail</a> 
                      
                      <button type="button" id="btn" >test mail</button>-->

        <div class="col-lg-10">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Grafico De Ventas:  
                        <strong id="total_venta_acumulada"> </strong>
                    </h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="lineChart_ventas" height="140"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-10">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ventas Ejecutivo

                    </h5>
                </div>
                <div class="ibox-content">
                    <div>
                        <canvas id="lineChart_ejecutivo" height="350" width="600"></canvas>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>

