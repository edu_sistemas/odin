<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<div class="row">
    <div class="row">

        <!--              <a href="Director/senMailtest">test mail</a> 
                      
                      <button type="button" id="btn" >test mail</button>-->

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h1>Resumen Fichas</h1>
                </div>
                <div class="ibox-content">
                <div class="row">

                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <!-- <span class="label label-success pull-right">Monthly</span> -->
                            <h5>Pendientes de gestión</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 id="fichas_pendientes" class="no-margins"></h1>
                            <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div> -->
                            <small>Fichas pendientes</small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <!-- <span class="label label-info pull-right">Annual</span> -->
                            <h5>Gestiones#</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 id="cantidad_gestiones" class="no-margins"></h1>
                            <!-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> -->
                            <small>Total Gestiones TOM#</small>
                        </div>
                    </div>
                </div>

                <!-- <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-primary pull-right">Today</span>
                            <h5>visitas</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">106,120</h1>
                            <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                            <small>New visits</small>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-danger pull-right">Low value</span>
                            <h5>User activity</h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins">80,600</h1>
                            <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                            <small>In first month</small>
                        </div>
                    </div>
                </div> -->
                
            </div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">

    <div class="col-lg-12">
    <div class="ibox">



        <div class="ibox-content">

            <div class="table-responsive">
                <table id="tabla_pendientes" class="table table-striped">
                    <h1>Fichas pendientes próximas a vencer</h1>
                    <thead>
                        <th>Dias Restantes</th>
                        <th>Num. Ficha</th>
                        <th>Empresa</th>
                        <th>Ejecutivo</th>
                        <th>Estado Factura</th>
                        <th>Gestiones#</th>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>

    </div>
</div>
</div>

