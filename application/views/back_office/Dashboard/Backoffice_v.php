<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-03-06 [Felipe Bulboa] <fbulbo@edutecno.com>
 * Fecha creacion:  2017-03-06 [Felipe Bulboa] <fbulbo@edutecno.com>
 */
$dailyIndicators = $json;
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div class="row">
                    <a href="../Ficha/Agregar">
                        <div class="col-lg-3">
                            <div class="widget style1 lazur-bg">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-graduation-cap fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <h3 class="font-bold" style="margin-top: 16px;">Venta curso</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="../Ficha/Arriendo">
                        <div class="col-lg-3">
                            <div class="widget style1 navy-bg">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-credit-card fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <h3 class="font-bold" style="margin-top: 16px;">Arriendo Sala</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="../Ficha/Consulta">
                        <div class="col-lg-3">
                            <div class="widget style1 yellow-bg">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-eye fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <h3 class="font-bold" style="margin-top: 16px;">Seguimiento Ficha</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="../Ficha/Rectificaciones">
                        <div class="col-lg-3">
                            <div class="widget style1 red-bg">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-cogs fa-5x"></i>
                                    </div>
                                    <div class="col-xs-8 text-right">
                                        <h3 class="font-bold" style="margin-top: 16px;">Rectificación Ficha</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
            <div class="ibox-title">
                <h4>INDICADORES </h4>
            </div>
            <div class="ibox-content" style="overflow: hidden;">
                <div class="table-responsive" style="width: 48%; float: left;">
                    <table class="table table-bordered">
                        <tr>
                            <td class="btn-primary">
                                <div class="col-xs-4">
                                    <i class="fa fa-home fa-5x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span>UF</span>
                                    <h2 class="font-bold">
                                        <?php 
                                                                    if(!isset($uf->UFs[0]->Valor)){
                                                                        echo "sin información.";
                                                                    }else{
                                                                        echo "$".$uf->UFs[0]->Valor;
                                                                    }
                                                                    ?>
                                    </h2>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="btn-primary">
                                <div class="col-xs-4">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span>Dólar</span>
                                    <h2 class="font-bold">
                                        <?php 
                                                                    if(!isset($dolar->Dolares[0]->Valor)){
                                                                        echo "sin información.";
                                                                    }
                                                                    else{
                                                                        echo "$".$dolar->Dolares[0]->Valor;
                                                                    }?>
                                    </h2>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="btn-primary">
                                <div class="col-xs-4">
                                    <i class="fa fa-eur fa-5x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span>Euro</span>
                                    <h2 class="font-bold">
                                        <?php 
                                                                    if(!isset($dailyIndicators->moneda->euro)){
                                                                        echo "sin información.";
                                                                    }
                                                                    else{
                                                                        echo "$".$dailyIndicators->moneda->euro;
                                                                    }?>
                                    </h2>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="btn-primary">
                                <div class="col-xs-4">
                                    <i class="fa fa-gavel fa-5x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span>UTM</span>
                                    <h2 class="font-bold">
                                        <?php 
                                                                    if(!isset($utm->UTMs[0]->Valor)){
                                                                        echo "sin información.";
                                                                    }
                                                                    else{
                                                                        echo "$".$utm->UTMs[0]->Valor;
                                                                    }?>
                                    </h2>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="btn-primary">
                                <div class="col-xs-4">
                                    <i class="fa fa-line-chart fa-5x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span>IPC</span>
                                    <h2 class="font-bold">
                                        <?php 
                                                                    if(!isset($ipc->IPCs[0]->Valor)){
                                                                        echo "sin información.";
                                                                    }
                                                                    else{
                                                                        echo $ipc->IPCs[0]->Valor."%";
                                                                    }?>
                                    </h2>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="btn-primary">
                                <div class="col-xs-4">
                                    <i class="fa fa-star fa-5x"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span>Santoral</span>
                                    <h3 class="font-bold">
                                        <?php
                                                                        if(!isset($dailyIndicators->santoral->hoy)){
                                                                            echo "sin información.";
                                                                        }
                                                                        else{
                                                                            echo $dailyIndicators->santoral->hoy;
                                                                        }
                                                                        ?>
                                    </h3>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>


                <!--//bloque tiempo-->
                <?php  // var_dump($output);?>
                <?php if($output->query->results != NULL){?>
                <?php $forecast = $output->query->results->channel->item->forecast;?>
                <?php $location = $output->query->results->channel->location;?>
                <?php //var_dump($output->query->results->channel->units->item->forecast);?>

                <div class="table-responsive" style="width: 48%; float: right;">
                    <table class="table table-bordered">
                        <tr>
                            <td class="widget style1 lazur-bg">
                                <?php
                            $forecast_text = "";
                            $forecast_icon = "";
                            switch ($forecast[0]->text) {
                            case 'Showers':
                            $forecast_text = "Chubascos";
                            $forecast_icon = "wi wi-showers";
                            break;
                            case 'Mostly Cloudy':
                            $forecast_text = "Mayormente Nublado";
                            $forecast_icon = "wi wi-cloudy";
                            break;
                            case 'Partly Cloudy':
                            $forecast_text = "Parcialmente Nublado";
                            $forecast_icon = "wi wi-day-cloudy";
                            break;
                            case 'Sunny':
                            $forecast_text = "Soleado";
                            $forecast_icon = "wi wi-day-sunny";
                            break;
                            case 'Rain':
                            $forecast_text = "Lluvia";
                            $forecast_icon = "wi wi-rain";
                            break;
                            case 'Mostly Sunny':
                            $forecast_text = "Mayormente Despejado";
                            $forecast_icon = "wi wi-day-sunny-overcast";
                            break;
                            case 'Rain And Snow':
                            $forecast_text = "Lluvia y Nieve";
                            $forecast_icon = "wi wi-rain-mix";
                            break;
                            default:
                            $forecast_text = $forecast[0]->text;
                            break;
                            }


                            ?>
                                <div class="col-xs-4">

                                    <i style="font-size:350%;" class="<?php echo $forecast_icon; ?>"></i>
                                </div>

                                <div class="col-xs-8 text-right">
                                    <span>
                                        <?php
                            echo $location->city.' Hoy ('.$forecast_text.')';?>
                                    </span>
                                    <h2 class="font-bold">Max.
                                        <?php echo $forecast[0]->high;?>°C - Min.
                                        <?php echo $forecast[0]->low;?>°C</h2>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="widget style1 lazur-bg">
                                <?php 
                                $forecast_text = "";
                                $forecast_icon = "";
                                switch ($forecast[1]->text) {
                                case 'Showers':
                                $forecast_text = "Chubascos";
                                $forecast_icon = "wi wi-showers";
                                break;
                                case 'Mostly Cloudy':
                                $forecast_text = "Mayormente Nublado";
                                $forecast_icon = "wi wi-cloudy";
                                break;
                                case 'Partly Cloudy':
                                $forecast_text = "Parcialmente Nublado";
                                $forecast_icon = "wi wi-day-cloudy";
                                break;
                                case 'Sunny':
                                $forecast_text = "Soleado";
                                $forecast_icon = "wi wi-day-sunny";
                                break;
                                case 'Rain':
                                $forecast_text = "Lluvia";
                                $forecast_icon = "wi wi-rain";
                                break;
                                case 'Mostly Sunny':
                                $forecast_text = "Mayormente Despejado";
                                $forecast_icon = "wi wi-day-sunny-overcast";
                                break;
                                case 'Rain And Snow':
                                $forecast_text = "Lluvia y Nieve";
                                $forecast_icon = "wi wi-rain-mix";
                                break;
                                default:
                                $forecast_text = $forecast[1]->text;
                                break;
                                }
                                ?>
                                <div class="col-xs-4">
                                    <i style="font-size:350%;" class="<?php echo $forecast_icon; ?>"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span><?php
                                        echo $location->city.' Mañana';?>
                                    </span>
                                    </br>
                                    <span><?php echo '('.$forecast_text.')';?>
                                    </span>
                                    <h3 class="font-bold">Max.
                                        <?php echo $forecast[1]->high;?>°C - Min.
                                        <?php echo $forecast[1]->low;?>°C</h3>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="widget style1 lazur-bg">
                                <?php
                                $forecast_text = "";
                                $forecast_icon = "";
                                switch ($forecast[2]->text) {
                                case 'Showers':
                                $forecast_text = "Chubascos";
                                $forecast_icon = "wi wi-showers";
                                break;
                                case 'Mostly Cloudy':
                                $forecast_text = "Mayormente Nublado";
                                $forecast_icon = "wi wi-cloudy";
                                break;
                                case 'Partly Cloudy':
                                $forecast_text = "Parcialmente Nublado";
                                $forecast_icon = "wi wi-day-cloudy";
                                break;
                                case 'Sunny':
                                $forecast_text = "Soleado";
                                $forecast_icon = "wi wi-day-sunny";
                                break;
                                case 'Rain':
                                $forecast_text = "Lluvia";
                                $forecast_icon = "wi wi-rain";
                                break;
                                case 'Mostly Sunny':
                                $forecast_text = "Mayormente Despejado";
                                $forecast_icon = "wi wi-day-sunny-overcast";
                                break;
                                case 'Rain And Snow':
                                $forecast_text = "Lluvia y Nieve";
                                $forecast_icon = "wi wi-rain-mix";
                                break;
                                default:
                                $forecast_text = $forecast[2]->text;
                                break;
                                }

                                ?>
                                <div class="col-xs-4">
                                    <i style="font-size:350%;" class="<?php echo $forecast_icon; ?>"></i>
                                </div>
                                <div class="col-xs-8 text-right">
                                    <span><?php echo $location->city.' Pasado Mañana';?>
                                    </span>
                                    </br>
                                    <span>
                                        <?php
                                    echo '('.$forecast_text.')';?>
                                    </span>
                                    <h3 class="font-bold">Max.
                                        <?php echo $forecast[2]->high;?>°C - Min.
                                        <?php echo $forecast[2]->low;?>°C</h3>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table class="table table-bordered">
                        <tr>
                            <?php
                            
                            $status_color = "";
                            $font_color = "";
                            for($i = 0 ; $i < count($aire) ; $i ++){

                            if($aire[$i]->comuna == "Santiago"){

                            

                            switch ($aire[$i]->realtime[0]->tableRow->status) {
                            case 'bueno':
                            $status_color = "green";
                            $font_color = "white";
                            break;
                            case 'regular':
                            $status_color = "yellow";
                            $font_color = "black";
                            break;
                            case 'alerta':
                            $status_color = "orange";
                            $font_color = "white";
                            break;
                            case 'preemergencia':
                            $status_color = "red";
                            $font_color = "black";
                            break;
                            case 'emergencia':
                            $status_color = "blueviolet";
                            $font_color = "black";
                            break;
                            case 'no disponible':
                            $status_color = "gray";
                            $font_color = "black";
                            break;
                            default:
                            $status_color = "green";
                            $font_color = "white";
                            break;
                            }
                            
                            
                            ?>
                                <td class="widget style1 lazur-bg" style="background-color: <?php echo $status_color; ?> ; color: <?php echo $font_color; ?>;">
                                <div class="col-xs-4">
                                </div>

                                <div class="col-xs-8 text-right">
                                    <span>
                                        <?php
                                    echo 'Calidad del Aire ('.$aire[$i]->comuna.')';?>
                                    </span>
                                    <h2 class="font-bold"><?php echo $aire[$i]->realtime[0]->tableRow->status; }}?></h2>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>