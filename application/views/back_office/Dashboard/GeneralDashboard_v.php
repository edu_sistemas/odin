<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2018-05-23 [Verónica Morales] <vmorales@edutecno.com>
 */
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="ibox-content">						
	    <div class="row">
            <div class="form-group" id="data1">												
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <label class="font-normal">Seleccione Mes</label>
                    <div class="input-group date" style="padding-bottom: 10px;">
                        <span class="input-group-addon"><i class="fa fa-calendar" onclick="fecha()";></i></span><input type="text" class="form-control" id="fecha_sdashboard" name="fecha_sdashboard" value="<?php echo date("Y-m");?>"/>
                    </div>											
                    <button type="button" value="button" class="btn btn-primary" id="btn-fecha-sdashboard">Filtrar</button>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
<div class="col-md-12">
	<div class="row">
        <div class="col-md-12" style=" padding-left: 0px; padding-right: 0px;">
            <div class="ibox-title">
                <h5>Ranking Ejecutivos<small></small></h5>
				 <button type="button" name="btn_generar_excel_ranking_ejecutivos" id="btn_generar_excel_ranking_ejecutivos" class="btn btn-outline btn-primary" style="margin-top:-8px; margin-left: 10px;">
                <i class="fa fa-download"></i> Descargar</button>
			</div>			
			<div class="ibox-content">
                <div class="ibox-content" style="height	:400px; overflow: auto; border-style: none;">
                    <table id="tblSelectEjecutivos" class="table table-hover" style="width: 100%; border-bottom: 20px solid #fff;">
                        <thead>
                            <th style="width: 20%; text-align:right;">Ejecutivo</th>
							<th style="text-align:right;">% Mes</th>
							<th style="text-align:right;">% Acum.</th>
							<th style="text-align:right;"><i class="fa fa-money"></i> Venta Mes</th>
							<th style="text-align:right;"><i class="fa fa-money"></i> Meta Mes</th>
							<th style="text-align:right;"><i class="fa fa-money"></i> Venta Anual Hasta<br> Mes Seleccionado</th>	
							<th style="text-align:right;"><i class="fa fa-money"></i> Meta Anual Hasta<br> Mes Seleccionado</th>	
							<th style="text-align:right;"><i class="fa fa-money"></i> Meta Anual</th>
						</thead>
						<tbody>
                        </tbody>
					</table>
				</div>
			</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12" style=" padding-left: 0px; padding-right: 0px;">
            <div class="ibox-title">
                <h5>Ventas por Empresas
                    <small></small>
                </h5>
				 <button type="button" name="btn_generar_excel_ventas_empresas" id="btn_generar_excel_ventas_empresas" class="btn btn-outline btn-primary" style="margin-top:-8px; margin-left: 10px;">
                <i class="fa fa-download"></i> Descargar</button>
            </div>
            <div class="ibox-content">
                <div class="ibox-content" style="height	:600px; overflow: auto; border-style: none;">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
						</div>
						<div class="col-md-3">
							<h5>Filtro por rango de:</h5>
							<input type="radio" id="filtro1" name="filtro" value="2">
							<label for="filtro1">% Mes</label>
							<input type="radio" id="filtro2" name="filtro" value="3">
							<label for="filtro2">% Acum.</label>
							<input type="radio" id="filtro3" name="filtro" value="4" checked>
							<label for="filtro3">Mes</label>
							<input type="radio" id="filtro4" name="filtro" value="5">
							<label for="filtro4">Acum.</label>
						</div>
						<div class="col-md-2">
							<label for="min">Monto mínimo:</label><input type="text" id="min" name="min" class="form-control">
						</div>
						<div class="col-md-2">
							<label for="max">Monto máximo:</label><input type="text" id="max" name="max" class="form-control">
						</div>
					</div>
					<br>
					<table id="tblListVentasEmpresas" class="table table-striped table-bordered table-hover dataTables-example" style="width: 100%; border-bottom: 20px solid #fff;">
						<thead>
							<th style="width: 15%; text-align:right;">Empresa</th>
							<th style="width: 20%; text-align:right;">Ejecutivo</th>
							<th style="text-align:right;">% Mes</th>
							<th style="text-align:right;">% Acum.</th>
							<th style="text-align:right;">
								<i class="fa fa-money"></i> Mes</th>
							<th style="text-align:right;">
								<i class="fa fa-money"></i> Acum.</th>
							<th style="text-align:right;">Rubro</th>
						</thead>
						<tbody>
						</tbody>
					</table>
                </div>
            </div>
        </div>
	</div>
</div>
<div class="col-md-12">
	<div class="row">
		<div class="col-md-6" style=" padding-left: 0px; padding-right: 0px;">
			<div class="ibox-title">
				<h5>Ventas por Rubro<small></small></h5>
			</div>				
			<div class="ibox-content">
				<div style="height: 450px; overflow: auto; border-bottom: 20px solid #fff;">
					<table id="tblRubro" class="table table-hover" style="width: 100%;">
						<thead>
							<tr>
								<th style="width: 40%">Rubro</th>
								<th style="width: 30%;text-align:right;"><i class="fa fa-money"></i> Mes</th>
								<th style="width: 30%;text-align:right;"><i class="fa fa-money"></i> Acum.</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>  
			</div>
		</div>
		<div class="col-md-6" style=" padding-left: 0px; padding-right: 0px;">
			<div class="ibox-title">
				<h5>Venta por Línea de Negocio<small></small></h5>
			</div>				
			<div class="ibox-content">
				<div style="height: 450px; overflow: auto; border-bottom: 20px solid #fff;">
					<table id="tbllinNegocio" class="table table-hover" style="width: 100%;">
						<thead>
							<tr>
								<th>Linea</th>
								<th>Sublinea</th>
								<th style="text-align:right;"><i class="fa fa-money"></i> Mes</th>
								<th style="text-align:right;"><i class="fa fa-money"></i> Acum.</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div> 
			</div>
		</div>
	</div>	
</div>

<div class="col-md-12" style="padding: 0px 0px 30px 0px;">
	<div class="row">	
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Grafico De Ventas:  
						<strong id="total_venta_acumulada"> </strong>
					</h5>
				</div>
				<div class="ibox-content">
					<div>
						<canvas id="lineChart_ventas" height="60"></canvas>
					</div>
				</div>
			</div>
		</div>	
	</div>	
</div>

</div>	
</div>




