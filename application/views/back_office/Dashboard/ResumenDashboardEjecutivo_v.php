<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-01-11 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-01-11 [Jessica Roa] <jroa@edutecno.com>
 */
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="ibox-content">						
            <div class="row">
                <div class="form-group" id="data1">												
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <label class="font-normal">Seleccione Mes</label>
                        <div class="input-group date" style="padding-bottom: 10px;">
                            <span class="input-group-addon"><i class="fa fa-calendar" onclick="fecha()";></i></span><input type="text" class="form-control" id="fecha_sdashboard" name="fecha_sdashboard" value="<?php echo date("Y-m");?>"/>
                        </div>											
                        <button type="button" value="button" class="btn btn-primary" id="btn-fecha-sdashboard">Filtrar</button>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12" style=" padding-left: 0px; padding-right: 0px;">
                    <div class="ibox-title">
                        <h5>Ranking 5 Mejores Ejecutivos del Mes
                            <small></small>
                        </h5>
                    </div>
                    <div class="ibox-content">
                        <div class="ibox-content" style="height	:288px; overflow: auto; border-style: none;">
                            <table id="tblSelectMejoresEjecutivos" class="table table-hover" style="width: 100%; border-bottom: 20px solid #fff;">
                                <thead>
                                    <th style="width: 20%; text-align:right;">Posición</th>
                                    <th style="width: 20%; text-align:right;">Ejecutivo</th>
                                    <th style="text-align:right;">% Mes</th>
                                    <th style="text-align:right;">% Acum.</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12" style=" padding-left: 0px; padding-right: 0px;">
                    <div class="ibox-title">
                        <h5>Ranking Ejecutivos<small></small></h5>
                    </div>			
                    <div class="ibox-content">
                        <div class="ibox-content" style="height	:288px; overflow: auto; border-style: none;">
                            <table id="tblSelectGruposEjecutivos" class="table table-hover" style="width: 100%; border-bottom: 20px solid #fff;">
                                <thead>
                                    <th style="width: 20%; text-align:right;">Ejecutivo</th>
                                    <th style="text-align:right;">% Mes</th>
                                    <th style="text-align:right;">% Acum.</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>	
</div>




