<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-03-06 [Felipe Bulboa] <fbulbo@edutecno.com>
 * Fecha creacion:  2017-03-06 [Felipe Bulboa] <fbulbo@edutecno.com>
 */
?>

<div class="row">

    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Mi Grafico De Ventas:  
                    <strong id="total_venta_acumulada"> </strong>
                </h5>
            </div>
            <div class="ibox-content">
                <div>
                    <canvas id="lineChart_ventas" height="140"></canvas>
                </div>
            </div>
        </div>
    </div> 

    <div class="col-lg-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Mis Ventas  

                </h5>
            </div>
            <div class="ibox-content">
                <div>
                    <canvas id="lineChart_ejecutivo" height="140"></canvas>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-10">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>
                    Ventas Grupo Comercial
                </h5>
            </div>
            <div class="ibox-content">
                <div>
                    <canvas id="lineChart_grupo_comercial" height="350" width="600"></canvas>
                </div>
            </div>
        </div>
    </div>

</div>


