<?php
/**
 * Listar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2017-05-12 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-02-07 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-lg-6">
                        <h5>Módulo de asistencia</h5>
                    </div>
					<!--   <div class="col-lg-6" style="text-align: right"><a target="_blank" href="../../documentacion/asistencia.html">Necesito ayuda con esto</a></div>-->
                    <div class="ibox-tools"></div>
                </div>
            </div>
            <div class="ibox-content"> 
				<div id="mensaje"></div>
				<div class="form-group">
					<label class="col-lg-2 control-label">Seleccionar ficha:</label>
					<div class="col-md-3" style="padding-left: 0px !important;">
						<select id="id_ficha_v" name="id_ficha_v" class="select2_demo_1 form-control">
							<option></option>
						</select>
					</div>
				</div>
				<hr><br>
				<div id="exp" class="form-group" style="margin-left: 15px;">
					<div id="chk" class="i-checks icheckbox_square-green checked"></div>
					<label style="margin-left: 10px;"> Con declaración jurada </label>
					<div id="chk" style="margin-left: 100px;" class="i-checks icheckbox_square-green hover"></div>
					<label style="margin-left: 10px;"> Sin declaración jurada </label>
				</div>
				<br>
            </div>
        </div>
    </div>
</div>
<div id="demo" class="row">
	<div class="col-lg-12">
		<form method="post" class="form-horizontal" id="frm_dj_registro">
			<div class="ibox-content">
				<input type="hidden" id="numRows" name="numRows" value=""> 
				<div class="table-responsive">
					<table id="listaAlm" class="table table-striped table-bordered table-hover dataTables-example">
						<thead>
							<tr>
								<th></th>
								<th>Rut</th>
								<th>Nombre</th>
								<th>Declaración Jurada</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th></th>
								<th>Rut</th>
								<th>Nombre</th>
								<th>Declaración Jurada</th>
							</tr>
						</tfoot>
					</table>
				</div>
			
				<div class="form-group">
					<div class="col-md-offset-9 col-md-3">
						<button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Cancelar</button>
						<button type="button" id="confirmarR" class="btn btn-primary">Guardar Estado DJ</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>

<style type="text/css">
    .dataTables_filter {
        display: none;
    }
</style>