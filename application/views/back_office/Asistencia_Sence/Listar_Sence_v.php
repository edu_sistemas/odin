<?php
/**
 * Listar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2017-05-12 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-02-07 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Módulo de asistencia Sence</h5>
                        </div>
                        <div class="col-lg-6" style="text-align: right"><a target="_blank" href="../../documentacion/asistencia.html">Necesito ayuda con esto</a></div>
                        <div class="ibox-tools"></div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="mensaje2"></div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Seleccionar ficha</label>
                        <div class="col-md-3" style="padding-left: 0px !important;">
                            <select id="id_ficha_v" name="id_ficha_v" class="select2_demo_1 form-control">
							<option></option>
						</select>
                        </div>
                        <button type="reset" id="reset_superior" class="btn btn-info col-lg-2" data-dismiss="modal">Volver</button>
                    </div>
                    <hr>
                    <br>
                    <div class="ibox-content">
                        <div class="row">
                        </div>
                    </div>
                    <div id="mensaje" class="row">
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="fichas" class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Ficha</th>
                                        <th>Empresa</th>
                                        <th>Cod. Sence</th>
                                        <th>Curso</th>
                                        <th>Fecha inicio</th>
                                        <th>Fecha fin</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Ficha</th>
                                        <th>Empresa</th>
                                        <th>Cod. Sence</th>
                                        <th>Curso</th>
                                        <th>Fecha inicio</th>
                                        <th>Fecha fin</th>
                                        <th>Acción</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style type="text/css">
        .dataTables_filter {
            display: none;
        }
    </style>

    <div class="modal inmodal in" id="asistencia" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <form method="post" class="form-horizontal" id="frmLista" name="frmLista">
                <div class="modal-content animated fadeIn">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span>
					</button>
                        <i class="fa fa-book modal-icon"></i>
                        <h4 class="modal-title">Asistencia</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label style="margin-left: 15px;"> Encargado de asistencia: </label>
                                    <label><?php echo $this->session->userdata('nombre_user'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <input type="hidden" id="numRows" name="numRows" value="">
                            <input type="hidden" id="largo" name="largo" value="">
                            <input type="hidden" id="id_detalle_alm" name="id_detalle_alm" value="">
                            <div class="table-responsive">
                                <table id="listaAlm" class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                        <tr id="row">
                                            <th>Rut</th>
                                            <th>Nombre</th>
                                            <th>Asistencia %</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="confirmarR" class="btn btn-primary">Guardar Asistencia</button>
                        <br>
                        <br>
                    </div>
                </div>
            </form>
        </div>
    </div>