<?php
/**
 * UsuarioLista_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2018-01-03 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2018-01-03 [Jessica Roa] <jroa@edutecno.com>
 */
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Descuentos Fichas Pagadas</h5>
                    <br>
                    <form class="form-horizontal" id="frm_filtrarxxxx" name="frm_filtrarxxxx">
                        <div class="row">
                            <div class="col-lg-4">
                                <label class="control-label">Mes a consultar</label>
                                <input id="calendario_mes" name="calendario_mes" type="text" class="form-control input-md" required>
                                <p class="help-block"></p>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-lg-4">
                        </div>
                        <div class="col-lg-4">
                        </div>
                        <div class="col-lg-4">
                            <button id="btn_filtrar" type="button" class="btn btn-primary">Filtrar</button>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="tbl_descuentos_productos" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>Ejecutivo</th>
                                    <th>Ficha</th>
                                    <th>Fecha Inicio Curso</th>
                                    <th>Monto Rectificación <br><span class='rango'></span></th>
                                    <th>Valor Actual</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Ejecutivo</th>
                                    <th>Ficha</th>
                                    <th>Fecha Inicio Curso</th>
                                    <th>Monto Rectificación <br><span class='rango'></span></th>
                                    <th>Valor Actual</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>