<?php
/**
 * Agregar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-06-12 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-06-12 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Menú</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_alumno_editar"  >
                    <p> Ingrese el nombre de un nuevo Menú</p>
                    <input type="hidden" id="id_alumno" name="id_alumno" value="<?php echo $data_Alumno[0]['id_alumno']; ?>">


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Rut</label>
                        <div class="col-lg-3">
                            <input type="text"  class="form-control required"  value="<?php echo $data_Alumno[0]['rut_alumno']; ?>" id="rut_alumno_v" requerido="false" onkeypress="return solonumero(event)" mensaje ="Debe Ingresar la URL del menú" name="rut_alumno_v" size="8" class="form-control"/>
                        </div>

                        <p style="width: 0" class="col-lg-1">-</p>
                        <div class="col-lg-1">
                            <input type="text" class="form-control required" id="dv_alumno_v" requerido="false" mensaje ="Debe Ingresar la URL del menú" value="<?php echo $data_Alumno[0]['dv_alumno']; ?>" name="dv_alumno_v" size="1" class="form-control"/>
                        </div>
                        <div class="col-lg-3">
                            <p id="out_print" style="color: red"></p>
                        </div>

                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Apellido Paterno</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" value="<?php echo $data_Alumno[0]['apellido_paterno_alumno']; ?>"  id="apellido_paterno_alumno_v" requerido="false" mensaje ="Debe Ingresar el Apellido paterno del alumno" name="apellido_paterno_alumno_v" placeholder="Apellido Paterno" class="form-control"/>
                        </div>

                        <label class="col-lg-2 control-label">Apellido Materno</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="apellido_materno_alumno_v" value="<?php echo $data_Alumno[0]['apellido_materno_alumno']; ?>" requerido="false" mensaje ="Debe Ingresar el Apellido materno del alumno" name="apellido_materno_alumno_v" placeholder="Apellido Materno" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" value="<?php echo $data_Alumno[0]['nombre_alumno']; ?>" id="nombre_alumno_v" requerido="false" mensaje ="Debe Ingresar la URL del menú" name="nombre_alumno_v" class="form-control" placeholder="Nombre del Alumno"/>
                        </div>

                        <label class="col-lg-2 control-label">Segundo nombre</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="seg_nombre_alumno_v"  value="<?php echo $data_Alumno[0]['seg_nombre_alumno']; ?>" requerido="false" mensaje ="Debe Ingresar la URL del menú" name="seg_nombre_alumno_v" class="form-control" placeholder="Segundo nombre del Alumno"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Ingrese el nombre de usuario</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="usuario_alumno_v" value="<?php echo $data_Alumno[0]['usuario_alumno']; ?>"  requerido="false" mensaje ="Debe Ingresar el nombre de usuario con el cual se identificara en el sistema" name="usuario_alumno_v" class="form-control"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_editar_alumno">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
