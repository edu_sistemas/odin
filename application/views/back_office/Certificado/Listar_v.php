
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Busqueda</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form role="form" class="form-inline" id="frm_busqueda" name="frm_busqueda">
                    <div class="row">
                        <div class="col-lg-3">
                            <label for="num_ficha">N° Ficha</label><br>
                            <input id="num_ficha" name="num_ficha" class="select2_demo_3 form-control" onkeypress="return solonumero(event)" style="width: 100%" />

                        </div>
                        <div class="col-lg-4">
                            <label for="empresa">Empresa</label><br>
                            <select id="empresa" name="empresa" class="select2_demo_3 form-control" style="width: 100%">
                                <option></option>
                            </select>
                            <br>
                        </div>

                        <div class="col-lg-2" style="margin-top: 23px;">
                            <button style="margin-left: 20px;" type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                            <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Fichas disponibles</h5>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">

                    <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>

                                <th>N° Ficha</th>
                                <th>Modalidad</th>
                                <th>Curso</th>
                                <th>F. Termino</th>
                                <th>Empresa</th>
                                <th>Cert. entregados?</th>
                                <th>Cert.generado?</th>
                                <th>Cert. Disponible</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>

                                <th>N° Ficha</th>
                                <th>Modalidad</th>
                                <th>Curso</th>
                                <th>F. Termino</th>
                                <th>Empresa</th>
                                <th>Cert. entregados?</th>
                                <th>Cert.generado?</th>
                                <th>Cert. Disponible</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade in" id="modalCertificados" aria-hidden="true" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Entrega de Certificado</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><span id="n_empresa"></span> <small>Favor seleccione la modalidad de entrega de los certificados entregados a la empresa</small></h5>

                    </div>
                    <div class="ibox-content">
                        <form method="get" class="form-horizontal">
                            <div class="form-group"><label class="col-sm-2 control-label">Entrega</label>
                                <div class="col-sm-10">
                                    <select id="tipo_entrega" name="tipo_entrega"  class="form-control" requerido="true">
                                        <option></option>
                                    </select>
                                    <input type="hidden" id="id_ficha">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" id="tipo_entrega_btn" class="btn btn-primary">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal fade in" id="modalCertificadoPersonalizado" aria-hidden="true" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Personalización de Certificado</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5><span id="n_empresa"></span> <small>Favor seleccione las caracteristicas que desea los certificados.</small></h5>

                    </div>

                    <div class="ibox-content">
                        <form method="post" class="form-horizontal" action="./Listar/generar" id="form_perso" target="_blank">
                            <div class="form-group"><label class="col-sm-2 control-label">Seleccione nombre del curso</label>
                                <div class="col-sm-10">
                                    <div>
                                        <label> 
                                            <input type="radio" checked="checked" value="1" id="optionsRadios1" name="optionsRadios"> Nombre curso ODIN&reg; 
                                        </label>
                                    </div>
                                    <div>
                                        <label> 
                                            <input type="radio" value="2" id="optionsRadios2" name="optionsRadios"> Nombre curso SENCE
                                        </label>
                                    </div>
                                    <div>
                                        <label> 
                                            <input type="radio" value="3" id="optionsRadios3" name="optionsRadios"> 
                                            <input type="text" name="saludos" id="saludos" disabled="">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Orden de Compra</label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="OCbyidFicha" name="OCbyidFicha">
                                        <option>Todas O.C.</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-4 control-label">Firma Personalizada</label>
                                <div class="col-sm-6">
                                    <input type="checkbox" name="Perso" value="0" class="form-control" id="Perso">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-2 control-label">Nombre Encargado</label>
                                <div class="col-sm-10">
                                    <input type="text" value="" class="form-control" id="n_encargado" name="n_encargado">
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Cargo Encargado</label>
                                <div class="col-sm-10">
                                    <input type="text" value="" class="form-control" id="c_encargado" name="c_encargado">
                                </div>
                            </div>

                            <div class="form-group"><label class="col-sm-2 control-label">Empresa</label>
                                <div class="col-sm-10">
                                    <input type="text" value="" class="form-control" id="empresa_p" name="empresa_p">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-4 control-label">Fondo?</label>
                                <div class="col-sm-6">
                                    <input type="checkbox" name="fondo_perso" value="1" class="form-control" id="fondo_perso">
                                    <input type="hidden" name="id_perso" id="id_perso">
                                </div>

                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-4 control-label">Participación</label>
                                <div class="col-sm-6">
                                    <input type="checkbox" name="participacion" value="1" class="form-control" id="participacion">
                                </div>
                            </div>

                            <div class="hr-line-dashed"></div>
                            <div class="form-group"><label class="col-sm-4 control-label">No mostrar porcentaje</label>
                                <div class="col-sm-6">
                                    <input type="checkbox" name="porcentaje" value="1" class="form-control" id="porcentaje">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                <button type="button" id="tipo_entrega_btn" class="btn btn-primary" onclick="validarPerso();">Generar Certificado</button>
            </div>
        </div>
    </div>
</div>