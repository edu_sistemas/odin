<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 *Fecha  creacion:  2016-11-10 [Luis Jarpa] <ljarpa@edutecno.com>
 * Ultima edicion:  2017-03-21  [Jessica Roa] <jroa@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Actualizar Usuario</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_usuario_editar"  >
                    <p> Actualizar usuario </p>


                    <div class="form-group">
                        <input type="text" name="id" id="id" value="<?php echo $data_usuario[0]['id']; ?>" hidden=""> 
                        <label class="col-lg-2 control-label">RUT:</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required"
                                   id="rut_usuario" requerido="false"
                                   mensaje ="Debe Ingresar el RUT del usuario"
                                   placeholder="Ej. 9876543"
                                   name="rut_usuario"
                                   class="form-control"
                                   value="<?php echo $data_usuario[0]['rut']; ?>" >
                        </div>
                        <div class="col-lg-1">
                            <input type="text" class="form-control required"
                                   id="dv_usuario" requerido="false"
                                   mensaje ="Debe Ingresar el digito verificador"
                                   placeholder="2"
                                   name="dv_usuario"
                                   class="form-control"
                                   value="<?php echo $data_usuario[0]['dv']; ?>">
                        </div>

                        <label class="col-lg-2 control-label">Nombre de Usuario:</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control required"
                                   id="usuario" requerido="true"
                                   mensaje ="Debe Ingresar un nombre de usuario válido"
                                   placeholder="Ingrese Nombre identificatorio del usuario"
                                   name="usuario"
                                   class="form-control"
                                   value="<?php echo $data_usuario[0]['usuario']; ?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="col-lg-2 control-label">Email:</label>
                        <div class="col-lg-4">
                            <input type="email" class="form-control required"
                                   id="correo" requerido="true"
                                   mensaje ="Debe Ingresar un email válido"
                                   placeholder="Ej.: jperez@edutecno.com"
                                   name="correo"
                                   class="form-control"
                                   value="<?php echo $data_usuario[0]['correo']; ?>"
                                   >
                        </div>
                        
                        <label class="col-lg-2 control-label">Anexo Edutecno:</label>
                        <div class="col-lg-2">
                            <input type="number" class="form-control required"
                                   id="anexo_edutecno" requerido="true"
                                   mensaje ="Debe Ingresar un número de anexo válido"
                                   placeholder=""
                                   name="anexo_edutecno"
                                   class="form-control"
                                   value="<?php echo $data_usuario[0]['anexo_edutecno']; ?>" 
                                   min="0"
                                   >
                                   <small><span class="help-block m-b-none">Ingresar "0" Si no posee Anexo.</span></small>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control required"
                                   id="nombre_usuario" requerido="true"
                                   mensaje ="Debe Ingresar un nombre de usuario válido"
                                   placeholder="Ingrese Nombre real del usuario"
                                   name="nombre_usuario"
                                   class="form-control"
                                   value="<?php echo $data_usuario[0]['nombre']; ?>"
                                   >
                        </div>

                        <label class="col-lg-2 control-label">Apellidos:</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control required"
                                   id="apellidos_usuario"
                                   requerido="true"
                                   mensaje ="Debe Ingresar un nombre de usuario válido"
                                   placeholder="Ingrese Apellidos reales del usuario"
                                   name="apellidos_usuario"
                                   class="form-control"
                                   value="<?php echo $data_usuario[0]['apellidos']; ?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Perfil:</label>
                        <div class="col-lg-8">

                            <div class="col-md-6" id="LabelP">

                                <input type="hidden" class="form-control required"
                                       id="id_perfil_seleccionado"
                                       name="id_perfil_seleccionado"
                                       class="form-control"
                                       value="<?php echo $data_usuario[0]['id_perfil']; ?>"
                                       >

                                <select id="perfil" name="perfil"  class="select2_demo_3 form-control" >

                                </select>

                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Departamento:</label>
                        <div class="col-lg-8">
                    
                            <div class="col-md-6" id="LabelP">
                    
                                <input type="hidden" class="form-control required" id="id_departamento_seleccionado" name="id_departamento_seleccionado" class="form-control"
                                    value="<?php echo $data_usuario[0]['id_departamento']; ?>">
                    
                                <select id="departamento" name="departamento" class="select2_demo_3 form-control">
                    
                                </select>
                    
                            </div>
                    
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_usuario" id="btn_editar_usuario">Actualizar</button>
                            <button class="btn btn-sm btn-primary" type="button" name="btn_reset_password" id="btn_reset_password">Reestablecer Contraseña</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
