<?php
/**
 * UsuarioLista_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Usuarios</h5><br>
                    <h6>Presionando el botón "Otorgar Rol", se puede entregar asignar un rol en el sistema al usuario selecciondo</h6>

                    <div class="ibox-tools">

                        <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo</button>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_usuarios" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>                                 
                                    <th>Usuario</th>
                                    <th>Nombre</th>
                                    <th>Apellidos</th>    
                                    <th>Telefono</th>    
                                    <th>Perfil</th>
                                    <th>Tipo Usuario</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Usuario</th>
                                    <th>Nombre</th>
                                    <th>Apellidos</th>    
                                    <th>Telefono</th>    
                                    <th>Perfil</th>
                                    <th>Tipo Usuario</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                               
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
