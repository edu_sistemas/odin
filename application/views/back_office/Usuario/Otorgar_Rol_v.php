<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Asignar Rol</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <form class="form-horizontal" id="frm_otorgar_rol"  >
                    <p>Datos del usuario</p>
                    <hr style="border-top: 1px solid #000000">

                    <div class="form-group">
                        <div class="col-lg-10">
                            <input type="hidden" placeholder="Nombre" name="id" class="form-control" value="<?php echo $data_usuario[0]['id']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre de usuario:</label>
                        <div class="col-lg-2">

                            <label name="usuario" class="col-lg-1 control-label"><?php echo $data_usuario[0]['usuario']; ?></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">nombre:</label>
                        <div class="col-lg-2">
                            <label name="nombre" class="col-lg-5 control-label"><?php echo $data_usuario[0]['nombre']; ?></label>
                            <label name="apellidos" class="col-lg-2 control-label"><?php echo $data_usuario[0]['apellidos']; ?></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Rut:</label>
                        <div class="col-lg-2">
                            <label name="rut" class="col-lg-2 control-label"><?php echo $data_usuario[0]['rut']; ?></label>

                            <label name="dv" class="col-lg-6 control-label"> - <?php echo $data_usuario[0]['dv']; ?></label>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Estado:</label>
                        <div class="col-lg-2">
                            <label name="estado" class="col-lg-1 control-label"><?php echo $data_usuario[0]['estado']; ?></label>
                        </div>
                    </div>

                    <hr style="border-top: 1px solid #000000">

                    <p>Asignar un perfil</p>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Perfil:</label>
                        <div class="col-lg-10">
                            <select class="" name="perfil">
                                <option selected="<?php echo $data_usuario[0]['id_perfil']; ?>"><?php echo $data_usuario[0]['descripcion']; ?></option>

                                <?php
                                for ($i = 0; $i < count($contenido); $i++) {
                                    ?>
                                    <option  value="<?php echo $contenido[$i]['id']; ?>"><?php echo $contenido[$i]['text']; ?></option>

                                    <?php
                                }
                                ?>
                            </select>
                          <!-- <input type="text"  id="true" requerido="true"  mensaje ="Debe Ingresar el nombre que se le asignara al subdominio en Moddle"  placeholder="Ingrese el nombre del subdominio de Moddle" name="fk_id_perfil" class="form-control" value="<?php echo $data_usuario[0]['id_perfil']; ?>"> -->
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_otorgar_rol" id="btn_otorgar_rol">Actualizar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>

                </form>
            </div>

        </div>
    </div>
</div>
</div>
