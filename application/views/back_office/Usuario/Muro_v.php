<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Mis Detalles</h5>
                </div>
                <div>
                    <div class="ibox-content no-padding border-left-right">
                        <img id="mainImg" alt="image" class="img-responsive" src="<?php echo $data_usuario[0]['imagen']; ?>">
                        <a href="#openModal"><button type="button" class="btn btn-w-m btn-success" style="margin-top: 5px;"><i class="fa fa-file-image-o"></i> Cambiar imagen</button></a>
                        <a style="color:#fff" href="#openModal2">
                            <button type="button" class="btn btn-w-m  btn-primary" style="margin-top: 5px;"><i class="fa fa-key"></i> Cambiar Contraseña</button>
                        </a>
                    </div>

                    <div class="ibox-content profile-content">
                        <h4><strong><?php echo $data_usuario[0]['nombre']; ?> <?php echo $data_usuario[0]['apellidos']; ?></strong></h4>
                        <p><i class="fa fa-map-marker"></i> Departamento de sistemas</p>
                        <div class="row m-t-lg">
                            <div class="col-md-4">
                                <span class="bar" style="display: none;">5,3,9,6,5,9,7,3,5,2</span><svg class="peity" height="16" width="32"><rect fill="#1ab394" x="0" y="7.111111111111111" width="2.3" height="8.88888888888889"></rect><rect fill="#d7d7d7" x="3.3" y="10.666666666666668" width="2.3" height="5.333333333333333"></rect><rect fill="#1ab394" x="6.6" y="0" width="2.3" height="16"></rect><rect fill="#d7d7d7" x="9.899999999999999" y="5.333333333333334" width="2.3" height="10.666666666666666"></rect><rect fill="#1ab394" x="13.2" y="7.111111111111111" width="2.3" height="8.88888888888889"></rect><rect fill="#d7d7d7" x="16.5" y="0" width="2.3" height="16"></rect><rect fill="#1ab394" x="19.799999999999997" y="3.555555555555557" width="2.3" height="12.444444444444443"></rect><rect fill="#d7d7d7" x="23.099999999999998" y="10.666666666666668" width="2.3" height="5.333333333333333"></rect><rect fill="#1ab394" x="26.4" y="7.111111111111111" width="2.3" height="8.88888888888889"></rect><rect fill="#d7d7d7" x="29.7" y="12.444444444444445" width="2.3" height="3.5555555555555554"></rect></svg>
                                <h5><strong>169</strong> Posts</h5>
                            </div>
                            <div class="col-md-4">
                                <span class="line" style="display: none;">5,3,9,6,5,9,7,3,5,2</span><svg class="peity" height="16" width="32"><polygon fill="#1ab394" points="0 15 0 7.166666666666666 3.5555555555555554 10.5 7.111111111111111 0.5 10.666666666666666 5.5 14.222222222222221 7.166666666666666 17.77777777777778 0.5 21.333333333333332 3.833333333333332 24.888888888888886 10.5 28.444444444444443 7.166666666666666 32 12.166666666666666 32 15"></polygon><polyline fill="transparent" points="0 7.166666666666666 3.5555555555555554 10.5 7.111111111111111 0.5 10.666666666666666 5.5 14.222222222222221 7.166666666666666 17.77777777777778 0.5 21.333333333333332 3.833333333333332 24.888888888888886 10.5 28.444444444444443 7.166666666666666 32 12.166666666666666" stroke="#169c81" stroke-width="1" stroke-linecap="square"></polyline></svg>
                                <h5><strong>28</strong> Siguiendo</h5>
                            </div>
                            <div class="col-md-4">
                                <span class="bar" style="display: none;">5,3,2,-1,-3,-2,2,3,5,2</span><svg class="peity" height="16" width="32"><rect fill="#1ab394" x="0" y="0" width="2.3" height="10"></rect><rect fill="#d7d7d7" x="3.3" y="4" width="2.3" height="6"></rect><rect fill="#1ab394" x="6.6" y="6" width="2.3" height="4"></rect><rect fill="#d7d7d7" x="9.899999999999999" y="10" width="2.3" height="2"></rect><rect fill="#1ab394" x="13.2" y="10" width="2.3" height="6"></rect><rect fill="#d7d7d7" x="16.5" y="10" width="2.3" height="4"></rect><rect fill="#1ab394" x="19.799999999999997" y="6" width="2.3" height="4"></rect><rect fill="#d7d7d7" x="23.099999999999998" y="4" width="2.3" height="6"></rect><rect fill="#1ab394" x="26.4" y="0" width="2.3" height="10"></rect><rect fill="#d7d7d7" x="29.7" y="6" width="2.3" height="4"></rect></svg>
                                <h5><strong>240</strong> Seguidores</h5>
                            </div>
                        </div>
                        <div class="user-button">
                            <div class="row">
                                <div class="col-md-7">
                                    <!--  -->
                                </div>
                                <div class="col-md-5">
                                    <button type="button" class="btn btn-default btn-sm btn-block"><i class="fa fa-coffee"></i> Invitar a un café</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Actividades</h5>
                </div>
                <div class="ibox-content">


                </div>
            </div>

        </div>
    </div>


    <div id="openModal" class="modalDialog">
        <div>
            <h2>Cambiar imagen de perfil</h2>
            <div class="row">
                <div class="col-lg-2 imgContainerMuroGallery" onmouseover="showImageOptions(1)" onmouseout="hideImageOptions(1)">
                    <div id="1" class="imgOptionsMuroGallery">

                        <label title="Subir un nueva imagen" for="inputImage0">
                            <input type="file" id="inputImage0" onclick="imageneafofaof(this.id)" requerido="false" mensaje="Debe subir una imagen" name="file" accept="image/*" class="hide">
                            <i class="fa fa-cloud-upload iconUploadMuroGallery"></i>
                        </label>

                        <i class="fa fa-trash-o iconDeleteMuroGallery" onclick="eliminarImagen(0)"></i>

                    </div>

                    <img id="i0" class="imgMuroGAllery" onclick="seleccionar(this.id)" src="<?php echo $data_usuario[0]['imagen']; ?>"/>

                </div>
                <div class="col-lg-2 imgContainerMuroGallery" onmouseover="showImageOptions(2)" onmouseout="hideImageOptions(2)">
                    <div id="2" class="imgOptionsMuroGallery">
                        <label title="Subir un nueva imagen" for="inputImage1">
                            <input type="file" id="inputImage1" onclick="imageneafofaof(this.id)" requerido="false" mensaje="Debe subir una imagen" name="file" accept="image/*" class="hide">
                            <i class="fa fa-cloud-upload iconUploadMuroGallery"></i>
                        </label>

                        <i class="fa fa-trash-o iconDeleteMuroGallery" onclick="eliminarImagen(1)"></i>
                    </div>
                    <img id="i1" class="imgMuroGAllery" onclick="seleccionar(this.id)" src="<?php echo $data_usuario[0]['imagen']; ?>"/>

                </div>
                <div class="col-lg-2 imgContainerMuroGallery" onmouseover="showImageOptions(3)" onmouseout="hideImageOptions(3)">
                    <div id="3" class="imgOptionsMuroGallery">
                        <label title="Subir un nueva imagen" for="inputImage2">
                            <input type="file" id="inputImage2" onclick="imageneafofaof(this.id)" requerido="false" mensaje="Debe subir una imagen" name="file" accept="image/*" class="hide">
                            <i class="fa fa-cloud-upload iconUploadMuroGallery"></i>
                        </label>
                        <i class="fa fa-trash-o iconDeleteMuroGallery" onclick="eliminarImagen(2)"></i>
                    </div>
                    <img id="i2" class="imgMuroGAllery" onclick="seleccionar(this.id)" src="<?php echo $data_usuario[0]['imagen']; ?>"/>

                </div>
                <div class="col-lg-2 imgContainerMuroGallery" onmouseover="showImageOptions(4)" onmouseout="hideImageOptions(4)">
                    <div id="4" class="imgOptionsMuroGallery">
                        <label title="Subir un nueva imagen" for="inputImage3">
                            <input type="file" id="inputImage3" onclick="imageneafofaof(this.id)" requerido="false" mensaje="Debe subir una imagen" name="file" accept="image/*" class="hide">
                            <i class="fa fa-cloud-upload iconUploadMuroGallery"></i>
                        </label>
                        <i class="fa fa-trash-o iconDeleteMuroGallery" onclick="eliminarImagen(3)"></i>
                    </div>
                    <img id="i3" class="imgMuroGAllery" onclick="seleccionar(this.id)"  src="<?php echo $data_usuario[0]['imagen']; ?>"/>

                </div>
                <div class="col-lg-2 imgContainerMuroGallery" onmouseover="showImageOptions(5)" onmouseout="hideImageOptions(5)">
                    <div id="5" class="imgOptionsMuroGallery">
                        <label title="Subir un nueva imagen" for="inputImage4">
                            <input type="file" id="inputImage4" onclick="imageneafofaof(this.id)" requerido="false" mensaje="Debe subir una imagen" name="file" accept="image/*" class="hide">
                            <i class="fa fa-cloud-upload iconUploadMuroGallery"></i>
                        </label>
                        <i class="fa fa-trash-o iconDeleteMuroGallery" onclick="eliminarImagen(4)"></i>
                    </div>
                    <img id="i4" class="imgMuroGAllery" onclick="seleccionar(this.id)" src="<?php echo $data_usuario[0]['imagen']; ?>"/>

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style="height:50px;">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                </div>
                <div class="col-lg-2">
                    <button class="btn btn-sm btn-primary" onclick="setImgParameters()"><a href="#close">Guardar Cambios</a></button>
                </div>
                <div class="col-lg-1">
                    <button class="btn btn-sm btn-secondary" onclick="getImgParameters()"><a href="#close" style="text-decoration:none;">Cancelar</a></button>
                </div>
            </div>
        </div>
    </div>
    <input type="text" id="idDelUsuario" value="<?php echo $data_usuario[0]['id']; ?>" hidden="">
</div>

<div id="openModal2" class="modalDialog">
    <div>
        <h2>Cambiar contraseña</h2>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-2">Contraseña actual</div>
            <div class="col-lg-4"><input id="pass_actual" type="password"></div>
            <div class="col-lg-2"></div>

        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-2">Nueva Contraseña</div>
            <div class="col-lg-4"><input id="pass_nueva" type="password"></div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row" style="height: 10px;">
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-2">Repita Nueva Contraseña</div>
            <div class="col-lg-4"><input id="pass_nueva2" type="password"></div>
            <div class="col-lg-2"></div>
        </div>
        <div class="row" style="height: 10px;">
            <div class="col-lg-4"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-6"><p id="out-print" style="color: red;"></p></div>
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-2"><a><button type="button" class="btn btn-primary btn-sm btn-block" onclick="cambiarPassword()">Guardar</button></a></div>
            <div class="col-lg-2"><a><button type="button" class="btn btn-secondary btn-sm btn-block" onclick="abortPass()">Cancelar</button></a></div>
            <div class="col-lg-4"></div>
        </div>

    </div>

</div>

