<?php
/**
 * Agregar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2017-25-01 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo registro sence</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form   class="form-horizontal" id="frm_sence_registro"  >
                    <p> Ingrese datos para el nuevo código sence</p>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Codigo sence:</label>
                        <div class="col-lg-2">
                            <input type="text"  class="form-control required" id="codigo_sence" onkeypress="return solonumero(event)" name="codigo_sence" placeholder="Ingresar código" size="8" requerido="true" mensaje ="Debe ingresar código" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Nombre del curso sence:</label>
                            <div class="col-lg-3" id="LabelP">
                                <input type="text" class="form-control required" id="nombre_curso_code_sence" requerido="true" mensaje ="Debe Ingresar Nombre" name="nombre_curso_code_sence" placeholder="Nombre del curso" class="form-control"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Cantidad de horas sence:</label>
                        <div class="col-lg-2">
                            <input type="text"  class="form-control required" id="cantidad_hora_sence" requerido="true" onkeypress="return solonumero(event)" mensaje ="Debe Ingresar total de horas" name="cantidad_hora_sence" placeholder="Ingresar horas" size="8" class="form-control"/>
                        </div>

                        <label class="col-sm-2 control-label" style="padding-left: 13px;">Valor Imputable Participante:</label>
                        <div class="form-group col-lg-3">
                            <input  style="margin-left: 4px;" type="text" id="valor_hora_sence" name="valor_hora_sence" onkeypress="return solonumero(event)" requerido="true" mensaje ="Debe Ingresar valor hora en pesos" placeholder="Ingresar valor hora en pesos" class="form-control">
                        </div>
                    </div>
                    
                    
                      <div class="form-group">
                        <label class="col-lg-2 control-label">Valor Efectivo Participante:</label>
                        <div class="col-lg-2">
                            <input type="text"  class="form-control required" id="valor_efectivo_participante" requerido="true" onkeypress="return solonumero(event)" mensaje ="Debe Ingresar el valor por participante" name="valor_efectivo_participante" placeholder="valor por participante" size="8" class="form-control"/>
                        </div>

                        <label class="col-sm-2 control-label">Valor Total Curso:</label>
                        <div class="form-group col-lg-3">
                            <input type="text" id="valor_total_curso" name="valor_total_curso" onkeypress="return solonumero(event)" requerido="true" mensaje ="Debe Ingresar el valor total del curso" placeholder="Ingresar valor total del curso" class="form-control">
                        </div>
                    </div>


                    <div class="form-group" id="data_1">
                        <label class="col-lg-2 control-label">Fecha de Solicitud:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" name="fecha_solicitud" placeholder="02-01-2017"   requerido="true" mensaje="Debe seleccionar una fecha">
                        </div>
                    </div>


                    <div class="form-group" id="data_1">
                        <label class="col-lg-2 control-label">Fecha acreditación:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" name="fecha_acreditacion" placeholder="02-01-2017"  requerido="true" mensaje="Debe seleccionar una fecha">
                        </div>
                    </div>

                    <div class="form-group" id="data_1">
                        <label class="col-lg-2 control-label">Fecha Término de vigencia:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" name="fecha_vigencia" placeholder="02-01-2017"  requerido="true" mensaje="Debe seleccionar una fecha">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Otec:</label>
                        <div class="col-md-3" id="LabelP">
                            <select id="otec" requerido="true" mensaje="Debe seleccionar una Otec" name="otec" class="select2_demo_1 form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Modalidad:</label>
                        <div class="col-md-3" id="LabelP">
                            <select id="id_modalidad_sence" requerido="true" mensaje="Debe seleccionar una Modalidad" name="id_modalidad_sence" onChange="getCursoidCBX()" class="select2_demo_1 form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">N° Resolución excenta:</label>
                        <div class="col-lg-3">
                            <input type="text"  class="form-control required" id="resolucion_excenta" requerido="true" onkeypress="return solonumero(event)" mensaje ="Ingresar el número de resolución" name="resolucion_excenta" placeholder="Ingresar el número de resolución" size="14" class="form-control" style="margin-left: -15px;"/>
                        </div>
                    </div>


                    <div class="form-group">

                        <label class="col-lg-2 control-label">Adjuntar documento:</label>
                        <div class="col-md-5" id="LabelP">
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span  class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Seleccionar archivo</span>
                                    <span class="fileinput-exists">Cambiar</span>
                                    <input type="file" name="file" id="file" requerido="true" mensaje="Debe subir un archivo" />
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Quitar</a>
                            </div> 
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar_sence">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
