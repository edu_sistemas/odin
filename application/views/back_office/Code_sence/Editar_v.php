<?php
/**
 * Agregar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-12 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-02-01 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Modificar Registro sence</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_sence_editar"  >
                    <p> Ingrese datos para modificar el código sence</p>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Código sence: 
                            <?php echo $data_code_sence[0]['codigo_sence']; ?></label>
                    </div>

                    <div class="form-group">

                        <div class="">
                            <input type="hidden"  class="form-control required" id="id_code_sence" name="id_code_sence" placeholder="Ingresar código" size="20"  class="form-control" value="<?php echo $data_code_sence[0]['id_code_sence']; ?>" />
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Nombre del curso sence:</label>
                            <div class="col-lg-3" id="LabelP">
                                <input type="text" value="<?php echo $data_code_sence[0]['nombre_curso_code_sence']; ?>" class="form-control required" id="nombre_curso_code_sence" requerido="true" mensaje ="Debe Ingresar Nombre" name="nombre_curso_code_sence" placeholder="Nombre del curso" class="form-control"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Cantidad de horas sence:</label>
                        <div class="col-lg-2">
                            <input type="text" style="margin-left: -25px;" class="form-control required" value="<?php echo $data_code_sence[0]['cantidad_hora_sence']; ?>" id="cantidad_hora_sence" requerido="true" onkeypress="return solonumero(event)" mensaje ="Debe Ingresar total de horas" name="cantidad_hora_sence" placeholder="Ingresar horas" size="8" class="form-control"/>
                        </div>

                        <label class="col-sm-2 control-label">Valor Imputable Participante:</label>
                        <div class="form-group col-lg-3">
                            <input type="text" id="valor_hora_sence" name="valor_hora_sence"  value="<?php echo $data_code_sence[0]['valor_hora_sence']; ?>" onkeypress="return solonumero(event)" requerido="true" mensaje ="Debe Ingresar valor hora en pesos" placeholder="Ingresar valor hora en pesos" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Valor Efectivo Participante:</label>
                        <div class="col-lg-2">
                            <input type="text"  class="form-control required" id="valor_efectivo_participante" requerido="true" onkeypress="return solonumero(event)" value="<?php echo $data_code_sence[0]['valor_efectivo_participante']; ?>"  mensaje ="Debe Ingresar el valor por participante" name="valor_efectivo_participante" placeholder="valor por participante" size="8" class="form-control"/>
                        </div>

                        <label class="col-sm-2 control-label">Valor Total Curso:</label>
                        <div class="form-group col-lg-3">
                            <input type="text" id="valor_total_curso" name="valor_total_curso" onkeypress="return solonumero(event)" requerido="true" value="<?php echo $data_code_sence[0]['valor_total_curso']; ?>" mensaje ="Debe Ingresar el valor total del curso" placeholder="Ingresar valor total del curso" class="form-control">
                        </div>
                    </div>


                    <div class="form-group" id="data_1">
                        <label class="col-lg-2 control-label">Fecha de Solicitud:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" name="fecha_solicitud"  value="<?php echo $data_code_sence[0]['fecha_solicitud']; ?>" requerido="true" mensaje="Debe seleccionar una fecha">
                        </div>
                    </div>

                    <div class="form-group" id="data_1">
                        <label class="col-lg-2 control-label">Fecha acreditación:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" name="fecha_acreditacion" value="<?php echo $data_code_sence[0]['fecha_acreditacion']; ?>"  requerido="true" mensaje="Debe seleccionar una fecha">
                        </div>
                    </div>

                    <div class="form-group" id="data_1">
                        <label class="col-lg-2 control-label">Fecha Término de vigencia:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" name="fecha_vigencia" value="<?php echo $data_code_sence[0]['fecha_vigencia']; ?>" requerido="true" mensaje="Debe seleccionar una fecha">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Otec</label>
                        <div class="col-md-3" style="padding-left: 0px !important;">
                            <input type="hidden" id="cbx_selected3" name="cbx_selected3" value="<?php echo $data_code_sence[0]['id_otec']; ?>">
                            <select id="otec" name="otec"  class="select2_demo_3 form-control">

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Modalidad</label>
                        <div class="col-md-3" style="padding-left: 0px !important;">
                            <input type="hidden" id="cbx_selected2" name="cbx_selected2" value="<?php echo $data_code_sence[0]['id_modalidad']; ?>">
                            <select id="id_modalidad_sence" name="id_modalidad_sence"  class="select2_demo_3 form-control">

                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">N° Resolución excenta:</label>
                        <div class="col-lg-3">
                            <input type="text" value="<?php echo $data_code_sence[0]['n_resolucion_excenta']; ?>" class="form-control required" id="resolucion_excenta" requerido="true" onkeypress="return solonumero(event)" mensaje ="Ingresar el número de resolución" name="resolucion_excenta" placeholder="Ingresar el número de resolución" size="14" class="form-control" style="margin-left: -15px;"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_editar_sence">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_back" name="btn_back">volver</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
