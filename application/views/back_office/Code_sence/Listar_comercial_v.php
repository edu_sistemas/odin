<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:
 * Fecha creacion:	2016-03-07 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Sence</h5><br>
                    <div class="ibox-content">
                        <form role="form" class="form-inline" id="frm" name="frm">           
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-3">
                                    <input id="txt_codigo" name="txt_codigo" type="text" placeholder="" class="form-control input-md">
                                    <p class="help-block">Código</p>
                                </div>
                            </div>
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-3">
                                    <input id="txt_nombre" name="txt_nombre" type="text" placeholder="" class="form-control input-md">
                                    <p class="help-block">Nombre</p>
                                </div>
                            </div>
                            <!-- Select Basic -->
                            <div class="form-group">
                                <div class="col-md-3">
                                    <select id="cbx_modalidad" name="cbx_modalidad"  class="select2_demo_3 form-control">
                                        <option></option>
                                    </select>
                                    <p class="help-block">Modalidad</p>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3">
                                    <select id="cbx_nivel" name="cbx_nivel"  class="select2_demo_3 form-control">
                                        <option></option>
                                    </select>
                                    <p class="help-block">Nivel</p>
                                </div>
                            </div>
 
                            <div class="form-group">
                                <div class="col-md-3">
                                    <select id="cbx_version" name="cbx_version"  class="select2_demo_3 form-control">
                                        <option></option>
                                    </select>
                                    <p class="help-block">Version</p>
                                </div>
                            </div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="btn_buscar_codigo"></label>
                                <div class="col-md-3">
                                    <button type="button" id="btn_buscar_codigo" name="btn_buscar_codigo" class="btn btn-success">
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">

                        <h4>Códigos por expirar</h4>
                        <table id="tbl_sence_alerta" name="tbl_sence_alerta" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Fecha solicitud</th>
                                    <th>Fecha acreditación</th> 
                                    <th>Fecha vigencia</th>
                                    <th title="N° Resolución">N° R.</th>
                                    <th title="Cantidad de horas">C. Horas</th>
                                    <th title="Valor Imputable Participante">Valor Imputable P.</th>
                                    <th>Modalidad</th>
                                    <th>Documento</th>
                                    <th>Estado</th>
                                    <th>Vigencia Restante</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>

                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Fecha solicitud</th>
                                    <th>Fecha acreditación</th> 
                                    <th>Fecha vigencia</th>
                                    <th title="N° Resolución">N° R.</th>
                                    <th title="Cantidad de horas">C. Horas</th>
                                    <th title="Valor Imputable Participante">Valor Imputable P.</th>
                                    <th>Modalidad</th>
                                    <th>Documento</th>
                                    <th>Estado</th>
                                    <th>Vigencia Restante</th>
                                    <th>Accion</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                <div class="table-responsive">
                    <h4>Listado de códigos SENCE</h4>
                    <table id="tbl_sence" name="tbl_sence" class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th title="N° Resolución">Res</th>
                                <th title="Cantidad de horas">Horas</th>
                                <th>Versión</th>
                                <th>Nivel</th>                                
                                <th>Modalidad</th>
                                <th>Descriptor</th>

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Código</th>
                                <th>Nombre</th>
                                <th title="N° Resolución">N° R.</th>
                                <th title="Cantidad de horas">C. Horas</th>
                                <th>Versión</th>
                                <th>Nivel</th>                                
                                <th>Modalidad</th>
                                <th>Descriptor</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
