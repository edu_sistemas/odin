<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">



    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Códigos SENCE</h5>
                    <div class="ibox-tools">

                        <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo registro</button>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">

                        <h4>Códigos por expirar</h4>
                        <table id="tbl_sence_alerta" name="tbl_sence_alerta" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Fecha solicitud</th>
                                    <th>Fecha acreditación</th> 
                                    <th>Fecha vigencia</th>
                                    <th title="N° Resolución">N° R.</th>
                                    <th title="Cantidad de horas">C. Horas</th>
                                    <th title="Valor Imputable Participante">Valor Imputable P.</th>
                                    <th>Modalidad</th>
                                    <th>Documento</th>
                                    <th>Estado</th>
                                    <th>Estado vigencia</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>

                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Fecha solicitud</th>
                                    <th>Fecha acreditación</th> 
                                    <th>Fecha vigencia</th>
                                    <th title="N° Resolución">N° R.</th>
                                    <th title="Cantidad de horas">C. Horas</th>
                                    <th title="Valor Imputable Participante">Valor Imputable P.</th>
                                    <th>Modalidad</th>
                                    <th>Documento</th>
                                    <th>Estado</th>
                                    <th>Estado vigencia</th>
                                    <th>Accion</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <h4>Listado de códigos SENCE</h4>
                        <table id="tbl_sence" name="tbl_sence" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Fecha solicitud</th>
                                    <th>Fecha acreditación</th> 
                                    <th>Fecha vigencia</th>
                                    <th title="N° Resolución">N° R.</th>
                                    <th title="Cantidad de horas">C. Horas</th>
                                    <th title="Valor Imputable Participante">Valor Imputable P.</th>
                                    <th>Modalidad</th>
                                    <th>Documento</th>
                                    <th>Estado</th>
                                    <th>Estado vigencia</th>
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>

                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Fecha solicitud</th>
                                    <th>Fecha acreditación</th> 
                                    <th>Fecha vigencia</th>
                                    <th title="N° Resolución">N° R.</th>
                                    <th title="Cantidad de horas">C. Horas</th>
                                    <th title="Valor Imputable Participante">Valor Imputable P.</th>
                                    <th>Modalidad</th>
                                    <th>Documento</th>
                                    <th>Estado</th>
                                    <th>Estado vigencia</th>
                                    <th>Accion</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal inmodal" id="modal_docs_sence" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight" id="modal-principal" onclick="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Documentos para Sence N°: <h4 class="modal-title" id="modal_id_sence"></h4></h4>

                </div>
                <div class="modal-body">
                    <div class="btn-group">

                        <button class="btn btn-success" type="button" id="btn_nuevo_contacto" title="Subir Archivos">Subir un nuevo archivo</button> 
                        <br><br><br>
                    </div>
                    <div  id="div_frm_add" style="display: none;"> 

                        <form class="form-horizontal" id="frm_nuevo_contacto_empresa" >


                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="control-label">Archivos Adjuntos</label>
                                    <span class="btn btn-file fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Seleccionar archivos</span>
                                        <!-- The file input field used as target for the file upload widget  -->
                                        <input id="fileupload" type="file" name="fileupload[]" multiple="" accept="application/pdf,application/vnd.ms-excel,image/gif, image/jpeg,image/png,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">

                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <div id="progress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <span id="porsncetaje_carga" class="pull-right">0%</span>
                                </div> 
                            </div>

                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_guardar_contacto"></label>
                                <div class="col-md-8">
                                    <button id="btn_cancelar_contacto" type="button" name="btn_cancelar_contacto" class="btn btn-success">Volver</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <!-- <div class="table-responsive" id="div_tbl_contacto_empresa">   -->
                    <table id="tbl_docs_sence" class="table-md-4 table-bordered" >
                        <thead>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>        
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>                              

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>          
                                <th>Opciones</th>
                            </tr>
                        </tfoot>
                    </table>    
                 </div>
                <div class="modal-footer">
                   <button type="button" class="btn btn-white" data-dismiss="modal" id="btn_cerrarmodal_doc">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
