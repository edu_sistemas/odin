<?php
/**
 * Consulta_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:	2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <form role="form" class="form-inline" id="frm" name="frm" action="Ventas/consultarVentas">

                        <!-- Date Picker -->

                        <div class="row">



                        </div>



                        <div class="row">
                            <div class="col-lg-6">
                                <label for="holding">Ejecutivo</label><br>
                                <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="periodo">Periodo</label><br>
                                <select id="periodo" name="periodo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                        </div>


                        <div class="row">

                            <div class="col-lg-6 pull-right">
                                <br>
                                <br>
                                <button type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                                <button type="button" data-ajax="true" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Ventas</h5>
            </div>
            <div class="ibox-content">
                <table id="tbl_ventas" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Ejecutivo</th> 
                            <th>Periodo</th> 
                            <th>Arriendo</th>                                
                            <th>Capacitación</th>
                            <th>Total</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table> 
                <div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                </button>
                                <h4 class="modal-title" id="nombre_ejecutivo">Modal title</h4>
                                <small class="font-bold" id="periodo_venta">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                            </div>
                            <div class="modal-body">

                                <table id="tbl_ventas_ejecutivo" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Empresa</th> 
                                            <th>OTIC</th> 
                                            <th>Arriendo</th>                                
                                            <th>Capacitación</th>
                                            <th>Total</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>


                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Hiddens Modals para mostrar contactos -->




