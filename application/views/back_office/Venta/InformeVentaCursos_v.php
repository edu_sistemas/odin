<?php
/**
 * Consulta_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:	2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Seleccione opciones para buscar reporte</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm" action="InformeVentaCursos/consultarInformeVentaCursos">    
                        <div class="row">
                            <div class="col-lg-6">

                                <label>
                                    <input type="radio" checked="" value="1" id="optionsRadios1" name="optionsRadios"> 
                                    Fecha Inscripción
                                </label>

                                <div class="hr-line-dashed"></div>
                                <label>
                                    <input type="radio" value="2" id="optionsRadios2" name="optionsRadios"> 
                                    Fecha Inicio Curso
                                </label>

                            </div>
                            <div class="col-lg-6">
                                <label for="datepicker">Ingrese un rango de fechas</label>
                                <div id="data_5">
                                    <div class="input-daterange input-group" id="datepicker" style="width: 100%;">
                                        <span class="input-group-addon">Desde</span>
                                        <input type="text" class="input-sm form-control" id="start" name="start" placeholder="Desde" value="<?php
                                        $fecha = date('Y-m-j');
                                        $nuevafecha = strtotime('-200 day', strtotime($fecha));
                                        $nuevafecha = date('d-m-Y', $nuevafecha);
                                        echo $nuevafecha;
                                        ?>" readonly=""/>
                                        <span class="input-group-addon">Hasta</span>
                                        <input type="text" class="input-sm form-control" id="end" name="end" placeholder="Hasta" value="<?php echo date("d-m-Y"); ?>" readonly="" />
                                    </div>
                                </div>
                                <br>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-lg-12">

                                <br>
                                <button type="reset" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-white pull-right">Limpiar</button>
                                <button type="button" data-ajax="true" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ventas</h5>                   
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_informe_venta_curso" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>N° Ficha</th> 
                                    <th>N° S. Tablet</th> 
                                    <th id="columna_fecha">Fecha</th>
                                    <th>Holding</th> 
                                    <th>N.Fantasía</th>
                                    <th>RUT Empresa</th>                                    
                                    <th>Razón Social</th>
                                    <th>Dirección Empresa</th>
                                    <th>RUT Otic</th>
                                    <th>Nombre Otic</th>
                                    <th>Nombre Curso</th>
                                    <th>Modalidad</th>
                                    <th>Sub Linea Negocio</th>
                                    <th>Monto Total</th>
                                    <th title="Total / Sence /Empresa / (Sence/Empresa) /Becado / Otros">Total / Sence /Empresa / (Sence/Empresa) /Becado / Otros</th>
                                    <th>Ejecutivo Comercial</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>N° Ficha</th> 
                                    <th>N° S. Tablet</th> 
                                    <th id="columna_fecha2">Fecha</th>
                                    <th>Holding</th>  
                                    <th>N.Fantasía</th>
                                    <th>RUT Empresa</th>                                    
                                    <th>Razón Social</th>
                                    <th>Dirección Empresa</th>
                                    <th>RUT Otic</th>
                                    <th>Nombre Otic</th>
                                    <th>Nombre Curso</th>
                                    <th>Modalidad</th>
                                    <th>Sub Linea Negocio</th>
                                    <th>Monto Total</th>
                                    <th title="Total / Sence /Empresa / (Sence/Empresa) /Becado / Otros">Total / Sence /Empresa / (Sence/Empresa) /Becado / Otros</th>
                                    <th>Ejecutivo Comercial</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





