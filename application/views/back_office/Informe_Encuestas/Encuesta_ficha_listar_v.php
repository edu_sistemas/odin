<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 */
?>

    <head>
        <style>
            /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */

            #map {
                height: 100%;
            }
            /* Optional: Makes the sample page fill the window. */

            html,
            body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            input:disabled{
                opacity: 1;
                background-color: #fff;
            }
        </style>
    </head>
    <input type="hidden" id="id_ficha" value="<?php echo $id_ficha ?>" />
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <input type="hidden" id="num_ficha_hidden">
                        <h5 id="main_title">Listado de Encuestas ficha </h5>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <button id="btn_nueva_encuesta" class="btn btn-success">Ingresar Nueva Encuesta de Satisfacción</button>
                        <!-- INICIO TABLA LISTAR -->
                        <div id="tabla_encuestas" class="table-responsive">
                            <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>ID Encuesta</th>
                                        <th>Fecha Ingreso</th>
                                        <th>Relator</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>ID Encuesta</th>
                                        <th>Fecha Ingreso</th>
                                        <th>Relator</th>
                                        <th>Acción</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- FIN TABLA LISTAR -->
                        <div>
                            <form id="frm_encuesta_satisfaccion" style="display:none;">
                                <input type="hidden" id="id_ficha_form" name="id_ficha" value="<?php echo $id_ficha ?>" />
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label for="id_relator">Seleccione Relator</label>
                                        <select name="id_relator" id="id_relator" required></select>
                                    </div>
                                </div>
                                <br>
                                <table style="width: 100%;">
                                </table>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button class="btn btn-success" id="btn_guardar_encuesta">Guardar</button>
                                        <button class="btn btn-warning" type="button" id="btn_descartar_encuesta">Descartar</button>
                                    </div>
                                </div>
                            </form>
                        </div>




                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Ver Encuesta por ID -->
        <div id="modal_encuesta" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Respuesta de la Encuesta ID: </h4>
                        <h4 class="modal-title2">Ficha: </h4>
                    </div>
                    <div class="modal-body">


                        <table id="tbl_respuestas" style="width: 100%;">
                            <tbody>
                                <tr style="color:#fff;background-color:#2e74b4;">
                                    <td style="width:72%; text-align:left">IMPRESIÓN GENERAL</td>
                                    <td align="center" style="width: 4%;">1</td>
                                    <td align="center" style="width: 4%;">2</td>
                                    <td align="center" style="width: 4%;">3</td>
                                    <td align="center" style="width: 4%;">4</td>
                                    <td align="center" style="width: 4%;">5</td>
                                    <td align="center" style="width: 4%;">6</td>
                                    <td align="center" style="width: 4%;">7</td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta1">
                                    <td style="width:72%;color:#000;" align="left">Exprese su impresión general respecto del curso realizado</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta1" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta1" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta1" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta1" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta1" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta1" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta1" value="7"></td>
                                </tr>
                                <tr style="color:#fff;background-color:#2e74b4;">
                                    <td style="width:72%; text-align:left">SOBRE EL CURSO</td>
                                    <td align="center" style="width: 4%;">1</td>
                                    <td align="center" style="width: 4%;">2</td>
                                    <td align="center" style="width: 4%;">3</td>
                                    <td align="center" style="width: 4%;">4</td>
                                    <td align="center" style="width: 4%;">5</td>
                                    <td align="center" style="width: 4%;">6</td>
                                    <td align="center" style="width: 4%;">7</td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta2">
                                    <td style="width:72%;color:#000;" align="left">1. El relator presentó de forma clara los objetivos del curso:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta2" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta2" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta2" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta2" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta2" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta2" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta2" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta3">
                                    <td style="width:72%;color:#000;" align="left">2. Los contenidos del curso concordaron con los objetivos planteados al inicio de este:</td>
                                    <td
                                        style="width: 4%;text-align:center;"><input type="radio" name="respuesta3" value="1"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta3" value="2"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta3" value="3"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta3" value="4"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta3" value="5"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta3" value="6"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta3" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta4">
                                    <td style="width:72%;color:#000;" align="left">3. La composición del grupo fue adecuada para el logro de los objetivos:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta4" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta4" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta4" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta4" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta4" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta4" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta4" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta5">
                                    <td style="width:72%;color:#000;" align="left">4. La duración del curso fue la adecuada para el logro de los objetivos:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta5" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta5" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta5" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta5" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta5" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta5" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta5" value="7"></td>
                                </tr>
                                <tr style="color:#fff;background-color:#2e74b4;">
                                    <td style="width:72%; text-align:left">SOBRE EL MATERIAL</td>
                                    <td align="center" style="width: 4%;">1</td>
                                    <td align="center" style="width: 4%;">2</td>
                                    <td align="center" style="width: 4%;">3</td>
                                    <td align="center" style="width: 4%;">4</td>
                                    <td align="center" style="width: 4%;">5</td>
                                    <td align="center" style="width: 4%;">6</td>
                                    <td align="center" style="width: 4%;">7</td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta6">
                                    <td style="width:72%;color:#000;" align="left">5. En contenido del manual concuerda con los objetivos del curso:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta6" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta6" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta6" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta6" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta6" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta6" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta6" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta7">
                                    <td style="width:72%;color:#000;" align="left">6. El manual es fácilmente comprensible:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta7" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta7" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta7" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta7" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta7" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta7" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta7" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta8">
                                    <td style="width:72%;color:#000;" align="left">7. El manual presta ayuda en forma real y efectiva al desarrollo del curso:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta8" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta8" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta8" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta8" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta8" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta8" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta8" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta9">
                                    <td style="width:72%;color:#000;" align="left">8. Los ejercicios y/o actividades en clases fueron adecuados para los temas vistos:</td>
                                    <td
                                        style="width: 4%;text-align:center;"><input type="radio" name="respuesta9" value="1"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta9" value="2"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta9" value="3"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta9" value="4"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta9" value="5"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta9" value="6"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta9" value="7"></td>
                                </tr>
                                <tr style="color:#fff;background-color:#2e74b4;">
                                    <td style="width:72%; text-align:left">SOBRE EL RELATOR</td>
                                    <td align="center" style="width: 4%;">1</td>
                                    <td align="center" style="width: 4%;">2</td>
                                    <td align="center" style="width: 4%;">3</td>
                                    <td align="center" style="width: 4%;">4</td>
                                    <td align="center" style="width: 4%;">5</td>
                                    <td align="center" style="width: 4%;">6</td>
                                    <td align="center" style="width: 4%;">7</td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta10">
                                    <td style="width:72%;color:#000;" align="left">9. Las explicaciones del relator fueron claras y comprensibles:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta10" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta10" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta10" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta10" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta10" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta10" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta10" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta11">
                                    <td style="width:72%;color:#000;" align="left">10. El relator demostró dominar realmente las materias tratadas en clases:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta11" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta11" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta11" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta11" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta11" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta11" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta11" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta12">
                                    <td style="width:72%;color:#000;" align="left">11. El relator fue capaz de integrar y relacionar los contenidos para mostrar:</td>
                                    <td
                                        style="width: 4%;text-align:center;"><input type="radio" name="respuesta12" value="1"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta12" value="2"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta12" value="3"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta12" value="4"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta12" value="5"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta12" value="6"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta12" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta13">
                                    <td style="width:72%;color:#000;" align="left">12. La metodología usada por el relator fue adecuada para entregar los contenidos del
                                        curso:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta13" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta13" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta13" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta13" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta13" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta13" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta13" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta14">
                                    <td style="width:72%;color:#000;" align="left">13. El relator tuvo una buena dicción y fue capaz de motivar a los participantes durante
                                        el curso:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta14" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta14" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta14" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta14" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta14" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta14" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta14" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta15">
                                    <td style="width:72%;color:#000;" align="left">14. El relator se encontraba en la sala de clases a la hora de inicio y término del curso:</td>
                                    <td
                                        style="width: 4%;text-align:center;"><input type="radio" name="respuesta15" value="1"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta15" value="2"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta15" value="3"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta15" value="4"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta15" value="5"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta15" value="6"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta15" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta16">
                                    <td style="width:72%;color:#000;" align="left">15. El relator dio respuesta en forma adecuada a las preguntas de los participantes:</td>
                                    <td
                                        style="width: 4%;text-align:center;"><input type="radio" name="respuesta16" value="1"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta16" value="2"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta16" value="3"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta16" value="4"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta16" value="5"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta16" value="6"></td>
                                        <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta16" value="7"></td>
                                </tr>
                                <tr style="color:#fff;background-color:#2e74b4;">
                                    <td style="width:72%; text-align:left">SOBRE EL INSTITUTO</td>
                                    <td align="center" style="width: 4%;">1</td>
                                    <td align="center" style="width: 4%;">2</td>
                                    <td align="center" style="width: 4%;">3</td>
                                    <td align="center" style="width: 4%;">4</td>
                                    <td align="center" style="width: 4%;">5</td>
                                    <td align="center" style="width: 4%;">6</td>
                                    <td align="center" style="width: 4%;">7</td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta17">
                                    <td style="width:72%;color:#000;" align="left">16. La sala de clases fue cómoda para el desarrollo del curso:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta17" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta17" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta17" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta17" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta17" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta17" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta17" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta18">
                                    <td style="width:72%;color:#000;" align="left">17. El coffee break fue bueno:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta18" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta18" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta18" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta18" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta18" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta18" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta18" value="7"></td>
                                </tr>
                                <tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta19">
                                    <td style="width:72%;color:#000;" align="left">18. La ubicación del instituto es adecuada:</td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta19" value="1"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta19" value="2"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta19" value="3"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta19" value="4"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta19" value="5"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta19" value="6"></td>
                                    <td style="width: 4%;text-align:center;"><input type="radio" name="respuesta19" value="7"></td>
                                </tr>
                                <tr style="color:#fff;background-color:#2e74b4;">
                                    <td style="width:72%; text-align:left">COMENTARIOS - SUGERENCIAS</td>
                                    <td align="center" style="width: 4%;"></td>
                                    <td align="center" style="width: 4%;"></td>
                                    <td align="center" style="width: 4%;"></td>
                                    <td align="center" style="width: 4%;"></td>
                                    <td align="center" style="width: 4%;"></td>
                                    <td align="center" style="width: 4%;"></td>
                                    <td align="center" style="width: 4%;"></td>
                                </tr>
                                <tr>
                                    <td><br></td>
                                </tr>
                                <tr></tr>
                                <tr id="pregunta20">
                                    <td><textarea id="respuesta20" name="respuesta20" rows="4" cols="50"></textarea></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>