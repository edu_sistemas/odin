<?php
/**
 * Listar_v
 *
 * Description...
 */
?>

    <head>
        <style>
            /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */

            #map {
                height: 100%;
            }
            /* Optional: Makes the sample page fill the window. */

            html,
            body {
                height: 100%;
                margin: 0;
                padding: 0;
            }

            .footer {
                bottom: 0px;
            }

            #cargando {
                animation-name: animacion;
                animation-duration: 4s;
                animation-iteration-count: infinite;
            }

            .ibox-content {
                min-height: 300px;
            }

            @keyframes animacion {
                0% {
                    font-size: 140%;
                }
                50% {
                    font-size: 180%;
                }
                100% {
                    font-size: 140%;
                }
            }
            #render-html{
                width: 100%;
            }
        </style>
    </head>
    <div class="wrapper wrapper-content animated fadeInRight">
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active botones-nav" id="porFicha"><a href="#">Por Ficha</a></li>
                        <li class="botones-nav" id="porFechaDeCierre"><a href="#">Por Fecha de Cierre</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Por Relator <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="botones-nav" id="porRelator"><a href="#">Resultados Relator</a></li>
                                <li class="botones-nav" id="porYear"><a href="#">Ranking Anual</a></li>
                                <li class="botones-nav" id="porMes"><a href="#">Ranking Mensual</a></li>
                            </ul>
                        </li>
                        <li class="botones-nav" id="porEdutecno"><a href="#">Por Empresa Edutecno</a></li>
                        <li class="botones-nav" id="busquedaPersonalizada"><a href="#">Búsqueda Personalizada</a></li>
                        <li class="botones-nav" id="CumplimientoTecnico"><a href="#">Cump. Téc. Administrativo</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>


        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5 id="titulo_seccion"></h5>
                        <div class="ibox-tools">

                        </div>
                    </div>
                    <div class="ibox-content text-center">

                        <div id="formularios_informe_encuestas">

                            <div id="porFicha_formulario" class="hidden">
                                <form id="frm_buscar_por_ficha">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4"><input type="text" class="form-control" style="float:left;width: 75%;" id="num_ficha"
                                                name="num_ficha" placeholder="Ingrese numero de ficha" onkeypress="return solonumero(event);"
                                                required>
                                            <button class="btn btn-primary" style="float:right;">Buscar</button>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                </form>
                            </div>

                            <div id="porFechaDeCierre_formulario" class="hidden">
                                <form id="frm_buscar_por_fecha_de_cierre">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label for="datepicker">Ingrese un rango de fechas (Fechas de Cierre)</label>
                                            <div id="data_5">
                                                <div class="input-daterange input-group" id="datepicker" style="width: 100%">
                                                    <input type="text" class="input-sm form-control" name="start" value="" placeholder="Fecha más antigua" required/>
                                                    <span class="input-group-addon" style="background-color: darkseagreen; color: beige;">hasta</span>
                                                    <input type="text" class="input-sm form-control" name="end" value="" placeholder="Fecha más actual" required/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button class="btn btn-success">Buscar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div id="porRelator_formulario" class="hidden">
                                <form id="frm_buscar_por_relator">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="control-label">Seleccione un Relator para continuar</label><br>
                                            <select id="select_relator" name="select_relator" class="select2_demo_3 form-control" style="width: 100%" required>
                                                <option></option>
                                                </select>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <br>
                                            <label class="control-label">Seleccione Año y Mes</label><br>
                                            <input id="calendario_mes_x" name="calendario_mes_x" type="text" class="form-control input-md" required>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12"><br>
                                            <button class="btn btn-success">Buscar</button>
                                        </div>

                                    </div>
                                </form>
                            </div>

                            <div id="porYear_formulario" class="hidden">
                                <form id="frm_ranking_relator_anual">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="control-label">Seleccione un Año para continuar</label><br>
                                            <select id="select_year" name="select_year" class="select2_demo_3 form-control" style="width: 100%" required>
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12"><br>
                                            <button class="btn btn-success">Buscar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div id="porMes_formulario" class="hidden">
                                <form id="frm_ranking_relator_mensual">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="control-label">Seleccione Año y Mes</label><br>
                                            <input id="calendario_mes" name="calendario_mes" type="text" class="form-control input-md" required>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12"><br>
                                            <button class="btn btn-success">Buscar</button>
                                        </div>

                                    </div>
                                </form>
                            </div>

                            <div id="porEdutecno_formulario" class="hidden">
                                <form id="frm_buscar_por_edutecno">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="control-label">Seleccione una razón social de Edutecno</label><br>
                                            <select id="select_edutecno" name="select_edutecno" class="select2_demo_3 form-control" style="width: 100%">
                                                        <option></option>
                                                    </select>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <br>
                                            <label class="control-label">Seleccione Año y Mes</label><br>
                                            <input id="calendario_mes_e" name="calendario_mes_e" type="text" class="form-control input-md" required>
                                            <p class="help-block"></p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12"><br>
                                            <button class="btn btn-success">Buscar</button>
                                        </div>

                                    </div>
                                </form>
                            </div>

                            <div id="busquedaPersonalizada_formulario" class="hidden">
                                <form id="frm_busqueda_personalizada">
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="control-label">Seleccione un relator</label><br>
                                            <select id="select_relator_x" name="select_relator_x" class="select2_demo_3 form-control" style="width: 100%">
                                                            <option></option>
                                                    </select>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="control-label">Seleccione una Empresa</label><br>
                                            <select id="select_empresa" name="select_empresa" class="select2_demo_3 form-control" style="width: 100%">
                                                            <option></option>
                                                    </select>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="control-label">Código Curso</label><br>
                                            <select id="codigo_curso" name="codigo_curso" class="select2_demo_3 form-control" style="width: 100%">
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4">
                                            <label class="control-label">Seleccione un Ejecutivo Comercial</label><br>
                                            <select id="select_ejecutivo" name="select_ejecutivo" class="select2_demo_3 form-control" style="width: 100%">
                                                            <option></option>
                                                    </select>
                                        </div>
                                        <div class="col-lg-4">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12"><br>
                                            <button class="btn btn-success">Buscar</button>
                                        </div>

                                    </div>
                                </form>
                            </div>

                            <div id="CumplimientoTecnico_formulario" class="hidden">
                                    <form id="frm_CumplimientoTecnico">
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <label class="control-label">Seleccione Año y Mes</label><br>
                                                <input id="calendario_mes_f" name="calendario_mes_f" type="text" class="form-control input-md">
                                                <p class="help-block"></p>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <label class="control-label">Seleccione una Empresa</label><br>
                                                <select id="select_empresa_y" name="select_empresa_y" class="select2_demo_3 form-control" style="width: 100%">
                                                                <option></option>
                                                        </select>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <label class="control-label">Seleccione una razón social de Edutecno</label><br>
                                                <select id="select_edutecno_2" name="select_edutecno" class="select2_demo_3 form-control" style="width: 100%">
                                                            <option></option>
                                                        </select>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <label class="control-label">Código Curso</label><br>
                                                <select id="codigo_curso_y" name="codigo_curso_y" class="select2_demo_3 form-control" style="width: 100%">
                                                    <option></option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <label class="control-label">Seleccione una Sede</label><br>
                                                <select id="select_sede" name="select_sede" class="select2_demo_3 form-control" style="width: 100%">
                                                                <option></option>
                                                        </select>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4"></div>
                                            <div class="col-lg-4">
                                                <label class="control-label">Seleccione una Sala</label><br>
                                                <select id="select_sala" name="select_sala" class="select2_demo_3 form-control" style="width: 100%">
                                                                <option></option>
                                                        </select>
                                            </div>
                                            <div class="col-lg-4">
                                            </div>
                                        </div>
                                        <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-4">
                                                <label class="control-label">Ingrese N° de Ficha</label><br>
                                                    <input type="text" class="form-control" style="width: 100%;" id="num_ficha_y"
                                                        name="num_ficha_y" placeholder="Ingrese numero de ficha" onkeypress="return solonumero(event);"
                                                        >
                                                </div>
                                                <div class="col-lg-4"></div>
                                            </div>
                                        <div class="row">
                                            <div class="col-lg-12"><br>
                                                <button class="btn btn-success">Buscar</button>
                                            </div>
    
                                        </div>
                                    </form>
                                </div>

                        </div>

                        <p id="cargando"></p>
                        <div class="row">
                            <div id="render-html" class="col-lg-12">
                                
                             </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>