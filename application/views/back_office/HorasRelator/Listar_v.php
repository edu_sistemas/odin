<div class="row">
	<div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Busqueda</h5>            
            </div>
            <div class="ibox-content col-md-12">            
                <form role="form" class="form-inline" id="frm" name="frm">
                    <!-- Text input-->                                    
                    <div class="form-group col-md-4">
                        <div class="col-md-12"> 
                            <label class="col-md-5 control-label">Fecha de Inicio Pago </label>                           
                            <div class='input-group date col-md-6' id='datetimepickerInicio'>
                                <input type='text' class="form-control" placeholder="<?php echo 'Ej. '.date('d-m-Y');?>" id="fechaI"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>                                                           
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="col-md-12"> 
                            <label class="col-md-5 control-label">Fecha de Termino Pago </label>                           
                            <div class='input-group date col-md-6' id='datetimepickerTermino'>
                                <input type='text' class="form-control" placeholder="<?php echo 'Ej. '.date('d-m-Y');?>" id="fechaT"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>                                                           
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <div class="col-md-12"> 
                            <input type="button" class="btn btn-w-m btn-success" id="btn_buscar" value="Buscar"/>
                        </div>
                    </div>  
                </form>            
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Resultados</h5><br>                   
            </div>
            <div class="ibox-content">
                <div class="table-responsive">


                    <table id="tbl_docentes" class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Rut</th>                                      
                                <th>Nombre</th>
                                <th>Apellidos</th>                                                                                 
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Rut</th>                                      
                                <th>Nombre</th>
                                <th>Apellidos</th>                                                                                 
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal inmodal fade in" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; ">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="nombreRelator">David Fleto <3</h4>
                <p class="font-bold" id="periodos"></p>
            </div>
            <div class="modal-body">
                <table class="table">
                	<thead>
                    	<tr><td></td><td><strong>Curso</strong></td><td><strong>Fecha Hora Inicio</strong></td><td><strong>Fecha Hora Termino</strong></td><td><strong>Horas Cronologicas</strong></td></tr></thead>
                    <tbody id="body">
                    
                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
