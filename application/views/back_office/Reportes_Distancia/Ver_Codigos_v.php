<?php
/**
 * Ver_Curso_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2018-05-07 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<style>
    #cargando {
        width: 100%;
    }

    #cargando img {
        margin-left: auto;
        margin-right: auto;
        display: block;
    }

    .accordion {
        background-color: #1bb394;
        color: #fff;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }

    .active,
    .accordion:hover {
        background-color: #18a689;
    }

    .accordion:after {
        content: '\002B';
        color: #fff;
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2212";
    }

    .panel {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
    #tbl_global{
        font-size: 120%;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Códigos Generados Para La Ficha </h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="cargando"></div>
                    <div id="contenido" style="display:none;">
                        <div class="table-responsive">
                            <table id="tbl_codigos" class="table table-striped table-bordered table-hover dataTables-example" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Nombre Alumno</th>
                                        <th>RUT</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Código</th>
                                        <th>Nombre Alumno</th>
                                        <th>RUT</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>