<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2018-05-07 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<style>
    #cargando{
        width: 100%;
    }
    #cargando img{
        margin-left: auto;
        margin-right: auto;
        display: block;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Fichas Distancia</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="cargando"></div>
                    <div id="contenido" style="display:none;">
                        <!-- tabla fichas -->
                        <div class="table-responsive">
                            <table id="tbl_fichas" class="table table-striped table-bordered table-hover dataTables-example" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>N° Ficha</th>
                                        <th>Curso</th>
                                        <th>Empresa</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Término</th>
                                        <th>Cant. de Alumnos</th>
                                        <th>Cant. de Alumnos con Reporte</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>N° Ficha</th>
                                        <th>Curso</th>
                                        <th>Empresa</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Término</th>
                                         <th>Cant. de Alumnos</th>
                                         <th>Cant. de Alumnos con Reporte</th>
                                        <th>Estado</th>
                                        <th>Opciones</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- fin tabla fichas -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
