<?php
/**
 * Ver_Curso_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2018-05-07 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<style>
    #cargando {
        width: 100%;
    }

    #cargando img {
        margin-left: auto;
        margin-right: auto;
        display: block;
    }

    .accordion {
        background-color: #1bb394;
        color: #fff;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }

    .active,
    .accordion:hover {
        background-color: #18a689;
    }

    .accordion:after {
        content: '\002B';
        color: #fff;
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2212";
    }

    .panel {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
    #tbl_global{
        font-size: 120%;
    }
    .swal2-container{
        z-index: 2100 !important;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Alumnos Ficha </h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="cargando"></div>
                    <div id="contenido" style="display:none;">
                        <button class="accordion">Ver reporte global del curso</button>
                        <div class="panel">
                            <table id="tbl_global" class="table">
                                <tbody></tbody>
                            </table>
                        </div>
                        <button class="btn btn-danger btn-sm" onclick="getReporteDetalladoByIDFicha();">Exportar
                            reporte detallado a PDF</button>
                        <br>
                        <br>
                        <div class="table-responsive">
                            <table id="tbl_alumnos" class="table table-striped table-bordered table-hover dataTables-example"
                                style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>RUT</th>
                                        <th>Nombre Alumno</th>
                                        <th>O. C.</th>
                                        <th>Código Tablet</th>
                                        <th>Posible reporte</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>RUT</th>
                                        <th>Nombre Alumno</th>
                                        <th>O. C.</th>
                                        <th>Código Tablet</th>
                                        <th>Posible reporte</th>
                                        <th>Opciones</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- fin tabla codigos -->
                        <!-- inicio tabla hidden para reporte detallado -->
                        <table id="tbl_reporte_detallado" class="table hidden">

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL XML -->
<!-- Modal -->
<div id="modalXML" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>