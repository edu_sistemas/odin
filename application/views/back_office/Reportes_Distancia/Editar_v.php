<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:  2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Agregar Categoria</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <form class="form-horizontal" id="frm_categoria_registro"  >
                    <p>Agregar Categoria</p>

                    <div class="form-group">
                        <input type="text" id="id_categoria" name="id_categoria" value="<?php echo $data_categoria[0]['id_categoria']; ?>" hidden>
                        <label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-4">
                            <input type="text"  id="true" requerido="true"  mensaje ="Debe Ingresar el nombre del categoria  placeholder="Ingrese el nombre del categoria" name="nombre_categoria" class="form-control" value="<?php echo $data_categoria[0]['nombre_categoria']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Descripción breve:</label>
                        <div class="col-lg-6">
                            <input type="text"  id="true" requerido="true"  mensaje ="Debe Ingresar una desc. breve  placeholder="Ingrese el nombre del categoria" name="descripcion_breve_categoria" class="form-control" value="<?php echo $data_categoria[0]['descripcion_breve_categoria']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Descripción:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control required"
                                      id="descripcion_categoria" requerido="false"
                                      mensaje ="Debe Ingresar una o más observaciones"
                                      name="descripcion_categoria"
                                      placeholder="Ingrese observaciones o referencias de la categoria a registrar"
                                      maxlength="200"
                                      rows="6"><?php echo $data_categoria[0]['descripcion_categoria']; ?></textarea>
                            <p id="text-out">0/200 caracteres restantes</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_categoria" id="btn_editar_categoria">Guardar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
</div>