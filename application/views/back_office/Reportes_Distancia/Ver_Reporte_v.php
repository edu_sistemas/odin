<?php
/**
 * Ver_Curso_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2018-05-07 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<style>
    #cargando {
        width: 100%;
    }

    #cargando img {
        margin-left: auto;
        margin-right: auto;
        display: block;
    }

    .accordion {
        background-color: #1bb394;
        color: #fff;
        cursor: pointer;
        padding: 18px;
        width: 100%;
        border: none;
        text-align: left;
        outline: none;
        font-size: 15px;
        transition: 0.4s;
    }

    .active,
    .accordion:hover {
        background-color: #18a689;
    }

    .accordion:after {
        content: '\002B';
        color: #fff;
        font-weight: bold;
        float: right;
        margin-left: 5px;
    }

    .active:after {
        content: "\2212";
    }

    .panel {
        padding: 0 18px;
        background-color: white;
        max-height: 0;
        overflow: hidden;
        transition: max-height 0.2s ease-out;
    }
    #tbl_global{
        font-size: 120%;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
<input type="hidden" id="rut" value="<?php echo $rut ?>">
<input type="hidden" id="id_ficha" value="<?php echo $id_ficha ?>">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Reporte de curso Distancia </h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <div id="cargando"></div>
                    <div id="contenido" style="display:block;">
                        <table class="table">
                            <tr><th>Ficha N°: </th><td id="ficha_alumno"></td></tr>
                            <tr><th>Rut: </th><td id="rut_alumno"></td></tr>
                            <tr><th>Nombre: </th><td id="nombre_alumno"></td></tr>
                            <tr><th>Email: </th><td ><a id="email_alumno" href=""></a></td></tr>
                            <tr><th>Telefono: </th><td id="telefono_alumno"></td></tr>
                            <tr><th>Codigo Tablet: </th><td id="codigo_tablet"></td></tr>
                            <tr><th>Nombre Curso: </th><td id="nombre_curso"></td></tr>
                        </table>
                        <table id="tbl_global" class="table">
                            <tbody></tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>