<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2018-05-07 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<style>
    #cargando{
        width: 100%;
    }
    #cargando img{
        margin-left: auto;
        margin-right: auto;
        display: block;
    }
</style>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <!-- content -->
                    <div class="row">
                        <div class="col-lg-4">

                            <select name="select_otic" id="select_otic">
                                <option></option>
                            </select>

                        </div>
                        <div class="col-lg-4">

                        </div>
                        <div class="col-lg-4">

                        </div>
                    </div>
<br>
<br>
<br>
<h3>Formulario</h3>
                    <form id="formulario">
                        <div class="row">
                            <div class="col-lg-2"><label for="">nombre</label></div>
                            <div class="col-lg-10"><input type="text" name="nombre" id="nombre" required></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2"><label for="">apellido</label></div>
                            <div class="col-lg-10"><input type="text" name="apellido" id="apellido" required></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2"><label for="">direccion</label></div>
                            <div class="col-lg-10"><input type="text" name="direccion" id="direccion" required></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2"><label for="">cualquier cosa</label></div>
                            <div class="col-lg-10"><input type="text" name="cualquiercosa" id="cualquiercosa" required></div>
                        </div>
                        <button class="btn btn-success">Enviar</button>  
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
