<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<head>
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Cursos</h5>
                    <div class="ibox-tools">

                    </div>
                </div>
                <div class="ibox-content">

                <!-- INICIO TABLA LISTAR -->
                <div class="table-responsive">
                        <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Ficha</th>
                                    <th>empresa</th>
                                    <th>Inicio</th>
                                    <th>Término</th>
                                    <th>Alumnos</th>
                                    <th>Relator</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Ficha</th>
                                    <th>empresa</th>
                                    <th>Inicio</th>
                                    <th>Término</th>
                                    <th>Alumnos</th>
                                    <th>Relator</th>
                                    <th>Acción</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- FIN TABLA LISTAR -->
                </div>
            </div>
        </div>
    </div>
