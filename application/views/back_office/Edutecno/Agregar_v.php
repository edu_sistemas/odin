<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Edutecno</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_edutecno_registro"  >
                    <p> Ingrese un Nuevo Edutecno </p>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">RUT:</label>
                        <div class="col-lg-4">
                            <input type="number" class="form-control required"
                                   id="rut" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el RUT correspondiente"
                                   placeholder="Ej. 123456789"
                                   name="rut"
                                   maxlength="8"
                                   class="form-control" onkeypress="return solonumero(event)">

                        </div>
                        <p style="width: 0" class="col-lg-1">-</p>
                        <div class="col-lg-1">
                            <input type="text" class="form-control required"
                                   id="dv" 
                                   maxlength="1" 
                                   requerido="true"
                                   mensaje ="Debe Ingresar el digito verificador"
                                   placeholder="Ej. 0"
                                   name="dv"
                                   class="form-control" >
                        </div>
                        <div class="col-lg-3">
                            <p id="out_print" style="color: red"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Razon Social:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="razon_social_edutecno" requerido="true"
                                   mensaje ="Debe Ingresar La razón social"
                                   placeholder="Ingrese Razon Social"
                                   name="razon_social_edutecno"
                                   class="form-control"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Giro:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="giro_edutecno" requerido="true"
                                   mensaje ="Debe Ingresar El giro"
                                   placeholder="Ingrese Giro"
                                   name="giro_edutecno"
                                   class="form-control"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
