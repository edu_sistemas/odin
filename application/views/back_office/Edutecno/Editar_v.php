<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar Edutecno</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <form class="form-horizontal" id="frm_edutecno_registro"  >
                    <p>Modifique Edutecno</p>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">RUT:</label>
                        <div class="col-lg-4">
                            <?php
                            $rut = $data_edutecno[0]['rut_edutecno'];

                            $rut = explode("-", $rut);
                            ?>
                            <input type="hidden" id="id_edutecno" name="id_edutecno"                                   
                                   value="<?php echo $data_edutecno[0]['id_edutecno']; ?>">
                            <input type="number" class="form-control required"
                                   id="rut" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el RUT correspondiente"
                                   placeholder="Ej. 123456789"
                                   name="rut"
                                   maxlength="8"
                                   class="form-control" onkeypress="return solonumero(event)"
                                   value="<?php echo $rut [0] ?>"
                                   >

                        </div>
                        <p style="width: 0" class="col-lg-1">-</p>
                        <div class="col-lg-1">
                            <input type="text" class="form-control required"
                                   id="dv" 
                                   maxlength="1" 
                                   requerido="true"
                                   mensaje ="Debe Ingresar el digito verificador"
                                   placeholder="Ej. 0"
                                   name="dv"
                                   class="form-control" 
                                   value="<?php echo $rut [1] ?>"
                                   >
                        </div>
                        <div class="col-lg-3">
                            <p id="out_print" style="color: red"></p>
                        </div>
                    </div>

                    <!--  -->

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Razon social:</label>
                        <div class="col-lg-6">
                            <input type="text"  

                                   id="razon_social_edutecno"                                   
                                   requerido="true"
                                   mensaje ="Debe Ingresar la razon social"                                    
                                   placeholder="Ingrese razon social"
                                   name="razon_social_edutecno"
                                   class="form-control"
                                   value="<?php echo $data_edutecno[0]['razon_social_edutecno']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Giro:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="giro_edutecno" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el Giro de la empresa"
                                   placeholder="Ingrese Giro  de la empresa"
                                   name="giro_edutecno"
                                   value="<?php echo $data_edutecno[0]['giro_edutecno']; ?>"
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_edutecno" id="btn_editar_edutecno">Actualizar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

