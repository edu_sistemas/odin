<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>

<div class="row">
    <div class="col-lg-12" style="padding-left:0;padding-right:0;">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h1>Gastos/Servicios Adicionales</h1>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
            <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false"><i class="fa fa-list"></i></button>
                            <ul class="dropdown-menu">
                                <li><a href="#" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">Añadir Gasto/Servicio</a></li>
                            </ul>
                        </div>
                <table class="table table-responsive table-striped">
                    <thead>
                        <th>Gasto / Servicio</th>
                        <th>Detalle</th>
                        <th>Fecha Registro</th>
                        <th>Valor</th>
                        <th>Acción</th>
                    </thead>
                    <tbody>
                        <?php
                            for ($i=0; $i < count($data_ficha); $i++) { 
                                if($data_ficha[$i]['nombre'] == null){
                                    echo '';
                                }else{?>
                                <tr><td>
                                <?php
                                echo $data_ficha[$i]['nombre'];
                                ?>
                                </td><td>
                                <?php
                                echo $data_ficha[$i]['observaciones'];
                                ?>
                                </td><td>
                                <?php
                                echo $data_ficha[$i]['fecha_registro'];
                                ?>
                                </td><td>
                                <?php
                                echo $data_ficha[$i]['valor_formateado'];
                                ?>
                                </td><td>

                                <div class="btn-group">
                                        <button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">Acción <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="modalEditarItem(<?php echo $data_ficha[$i]['id_item']; ?>)">Editar</a></li>
                                            <li><a href="#" onclick="eliminarItem(<?php echo $data_ficha[$i]['id_item']; ?>)">Eliminar</a></li>
                                        </ul>
                                    </div>
                            
                                </td></tr>
                                <?php
                                }
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th>Valor Total</th>
                        <th><?php echo $data_ficha[0]['valor_total'];?></th>
                        <th></th>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<!-- MODAL AÑADIR GASTO/SERVICIO -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Añadir Gasto/Servicio a la ficha ID: <?php echo $data_ficha[0]['id_ficha'];?></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="frm_agregar_item">
            <input type="hidden" name="id_ficha" id="id_ficha" value="<?php echo $data_ficha[0]['id_ficha'];?>" />
            <div class="row"> 
                        <div class="col-lg-6">
                            <label for="item" class="control-label">Item</label>
                            <select id="id_item" name="id_item"  class="select2_demo_3 form-control" required>
                            </select>
                        </div>
                <div class="col-lg-6">
                    <label for="detalle" class="control-label">Detalle</label>
                    <textarea class="form-control" name="detalle" id="detalle" cols="30" rows="3" required></textarea>
                </div>
            </div>
            <div class="row"> 
                <div class="col-lg-6">
                    <label for="valor" class="control-label">Valor</label>
                    <input class="form-control" id="valor" name="valor" type="test" onkeypress="return solonumero(event)" required>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-lg-6"><button id="btn_agregar_item" class="btn btn-success">Añadir</button></div>
                <div class="col-lg-6"></div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>

<!-- MODAL EDITAR GASTO/SERVICIO -->
<!-- Modal -->
<div id="modalEditar" class="modal fade" role="dialog">
        <div class="modal-dialog">
      
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Editar Gasto/Servicio a la ficha ID: <?php echo $data_ficha[0]['id_ficha'];?></h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" id="frm_editar_item">
                  <input type="hidden" name="id_item_ficha_editar" id="id_item_ficha_editar"/>
                  <div class="row"> 
                              <div class="col-lg-6">
                                  <label for="id_item_editar" class="control-label">Item</label>
                                  <select id="id_item_editar" name="id_item_editar"  class="select2_demo_3 form-control" required>
                                  </select>
                              </div>
                      <div class="col-lg-6">
                          <label for="detalle_editar" class="control-label">Detalle</label>
                          <textarea class="form-control" name="detalle_editar" id="detalle_editar" cols="30" rows="3" required></textarea>
                      </div>
                  </div>
                  <div class="row"> 
                      <div class="col-lg-6">
                          <label for="valor" class="control-label">Valor</label>
                          <input class="form-control" id="valor_editar" name="valor_editar" type="test" onkeypress="return solonumero(event)" required>
                      </div>
                  </div>
                  <hr>
                  <div class="row">
                      <div class="col-lg-6"><button id="btn_guardar_item_editar" class="btn btn-success">Guardar</button></div>
                      <div class="col-lg-6"></div>
                  </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
      
        </div>
      </div>