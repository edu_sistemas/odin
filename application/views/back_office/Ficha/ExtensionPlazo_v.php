<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Datos del Curso</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">


                <p> Datos de la Venta</p> 
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label"><strong>N° de Ficha:</strong></label>

                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['num_ficha']; ?>">

                    </div>
                    <div class="col-md-4">
                        <label class="  control-label">Tipo de Venta:</label>

                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['categoria']; ?>">

                    </div>
                    <div class="col-md-4">
                        <label class="  control-label"><strong> Estado Rectificación:</strong> </label>

                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['estado']; ?>">

                    </div>
                </div>



                <div class="hr-line-dashed"></div>

                <p> Datos de la Empresa</p> 

                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Holding</label>

                        <input type="text" class="form-control" readonly="" id="holding"
                               value="<?php echo $data_ficha_preview[0]['holding']; ?>">

                    </div>
                    <div class="col-md-6">
                        <label class="  control-label">Empresa</label>

                        <input type="text" class="form-control" readonly=""
                               id="id_empresa"
                               value="<?php echo $data_ficha_preview[0]['rut_empresa'] . " / " . $data_ficha_preview[0]['empresa']; ?>">

                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-3">
                        <label class="control-label">Dirección Empresa</label>
                        <input type="text" class="form-control" readonly=""
                               id="direccion_empresa"
                               value="<?php echo $data_ficha_preview[0]['direccion_empresa']; ?>">

                    </div>

                    <div class="col-lg-3">
                        <label class="control-label">Nombre Contacto</label>

                        <input type="text" class="form-control" readonly=""
                               id="nombre_contacto_empresa"
                               value="<?php echo $data_ficha_preview[0]['nombre_contacto']; ?>">
                    </div>

                    <div class="col-lg-3">
                        <label class="control-label">Teléfono Contacto</label>

                        <input type="text" class="form-control" readonly=""
                               id="telefono_contacto_empresa"
                               value="<?php echo $data_ficha_preview[0]['telefono_contacto']; ?>">
                    </div>                        

                    <div class="col-lg-3">
                        <label class="control-label">Dirección envío Diplomas</label>

                        <input type="text" class="form-control" readonly=""
                               id="lugar_entrega_diplomas" value="<?php echo $data_ficha_preview[0]['direccion_diplomas']; ?>">
                    </div>   
                </div> 

                <div class="hr-line-dashed"></div>
                <p>Datos de la OTIC</p> 

                <div class="row">


                    <div class="col-lg-4">
                        <label class="  control-label">Otic</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['otic']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class=" control-label">Código Sence</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['codigo_sence']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class=" control-label">N° Orden Compra</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['num_orden_compra']; ?>">
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Datos del Curso</p>  
                <div class="row"> 
                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Modalidad</label>
                        <input type="text" class="form-control" readonly=""
                               id="modalidad"
                               value="<?php echo $data_ficha_preview[0]['modalidad']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Curso</label>
                        <input type="text" class="form-control" readonly=""  id="curso_descrip" value="<?php echo $data_ficha_preview[0]['descripcion_producto']; ?>">
                    </div>

                    <div class="col-md-4"> 
                        <label class="col-md-1 control-label">Versión</label>
                        <input type="text" class="form-control" readonly=""  id="version"  value="<?php echo $data_ficha_preview[0]['version']; ?>">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6">
                        <label class=" control-label">Fecha Inicio y Término</label>
                        <div class="input-daterange input-group  col-lg-12 ">
                            <input type="text" readonly="" class="form-control"
                                   name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"
                                   value="<?php echo $data_ficha_preview[0]['fecha_inicio']; ?>"
                                   requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                            <span class="input-group-addon">A</span> 
                            <input type="text"  class="form-control" name="fecha_termino" id="fecha_termino"
                                   placeholder="AAAA-MM-DD" value="<?php echo $data_ficha_preview[0]['fecha_fin']; ?>" readonly=""
                                   requerido="true" mensaje="Fecha Término es un campo requerido" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-md-6 ">
                            <label class="  control-label">Hora Inicio</label>
                            <div class="input-group clockpicker">
                                <input type="time" class="form-control" id="txt_hora_inicio"
                                       name="txt_hora_inicio"
                                       value="<?php echo $data_ficha_preview[0]['hora_inicio']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Inicio" readonly="" />
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <label class=" control-label">Hora Término</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input type="time" class="form-control" id="txt_hora_termino"
                                       name="txt_hora_termino"
                                       value="<?php echo $data_ficha_preview[0]['hora_termino']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Término" readonly="">
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Lugar de Ejecución</p>  
                <div class="row">
                    <div class="col-lg-6"> 
                        <label class=" control-label">Sede</label>
                        <input type="text" class="form-control" readonly="" id="id_sede"
                               value="<?php echo $data_ficha_preview[0]['sede']; ?>">

                    </div>
                    <div class="col-lg-6">
                        <label class=" control-label">Sala</label>
                        <input type="text" class="form-control" readonly="" id="id_sala"
                               value="<?php echo $data_ficha_preview[0]['sala']; ?>">
                    </div>

                </div>
                <div class="row">


                    <div class="col-lg-6">
                        <label class="control-label">Dirección o Lugar Ejecución</label>
                        <input id="lugar_ejecucion" name="lugar_ejecucion" class="form-control" readonly="" 
                               value="<?php echo $data_ficha_preview[0]['lugar_ejecucion']; ?>" 
                               />
                    </div>


                    <input  type="hidden" id="id_dias" value="<?php echo $data_ficha_preview[0]['id_dias']; ?>">
                    <div class="col-lg-6">
                        <label class="control-label">Días</label>
                        <label class="checkbox-inline" for="inlineCheckbox1"> <input
                                type="checkbox" readonly="" id="lunes" name="lunes" value="1"
                                title="Lunes"> Lu
                        </label> <label class="checkbox-inline" for="inlineCheckbox2"> <input
                                type="checkbox" readonly="" id="martes" name="martes" value="2"
                                title="Martes"> Ma
                        </label> <label class="checkbox-inline" for="inlineCheckbox3"> <input
                                type="checkbox" readonly="" id="miercoles" name="miercoles"
                                value="3" title="Miércoles"> Mi
                        </label> <label class="checkbox-inline" for="inlineCheckbox4"> <input
                                type="checkbox" readonly="" id="jueves" name="jueves" value="4"
                                title="Jueves"> Ju
                        </label> <label class="checkbox-inline" for="inlineCheckbox5"> <input
                                type="checkbox" readonly="" id="viernes" name="viernes"
                                value="5" title="Viernes"> Vi
                        </label> <label class="checkbox-inline" for="inlineCheckbox6"> <input
                                type="checkbox" readonly="" id="sabado" name="sabado" value="6"
                                title="Sábado"> Sa
                        </label> <label class="checkbox-inline" for="inlineCheckbox7"> <input
                                type="checkbox" readonly="" id="domingo" name="domingo"
                                value="7" title="Domingo"> Do
                        </label> 
                    </div>
                </div>
                <div class="hr-line-dashed">   </div>
                <p>información Interna</p> 
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Ejecutivo Comercial</label>
                        <input type="text" class="form-control" readonly=""
                               id="id_ejecutivo"
                               value="<?php echo $data_ficha_preview[0]['ejecutivo']; ?>">
                    </div>
                    <div class="col-md-6">
                        <label class="  control-label" for="textarea">Observaciones</label>
                        <textarea readonly="" class="form-control" id="comentarios"
                                  name="comentarios" maxlength="1000"><?php echo $data_ficha_preview[0]['comentario_orden_compra']; ?></textarea>
                        <p id="text-outs">0/1000 caracteres restantes</p>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>

<div class="animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Extensión de plazo O.C.</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content"> 

                    <form  name="frm_carga_extencion_plazo" id="frm_carga_extencion_plazo">
                        <div class="row">
                            <div class="col-lg-6"> 
                                <label for="cbx_orden_compra">Orden Compra</label>
                                <select id="cbx_orden_compra" name="cbx_orden_compra" class="form-control" requerido="true" mensaje="Debe seleccionar Orden de Compra" >
                                    <option value="">Seleccione O.C.</option>
                                    <?php for ($i = 0; $i < count($data_orden_compra); $i++) { ?>
                                        <option value="<?php echo $data_orden_compra[$i]['id'] ?>"><?php echo $data_orden_compra[$i]['text'] ?></option>
                                    <?php }
                                    ?>
                                </select>  
                            </div>
                            <div class="col-lg-6"> 
                                <label for="">Adjuntar Documento</label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>

                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Seleccionar Archivo</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="file_orden_compra" id="file_orden_compra" accept=".xls,.xlsx,.pdf" accesskey=""
                                               requerido="true" mensaje="Debe seleccionar documetno de la rectificacion" 
                                               >
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" id="btn_cleanr_file" data-dismiss="fileinput">
                                        Eliminar
                                    </a>
                                </div>   


                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-6"> 
                                <label for="cbx_orden_compra">Fecha Inicio y Término</label> 
                                <div class="input-datarange input-group">
                                    <input type="text"  readonly=""  class="form-control" name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"  
                                           value="<?php echo $data_ficha_preview[0]['fecha_inicio']; ?>"
                                           requerido="true" mensaje="Fecha Inicio es un campo requerido"/>
                                    <span class="input-group-addon">A</span>
                                    <input type="text" class="form-control" name="fecha_termino" id="fecha_termino" placeholder="AAAA-MM-DD"
                                           value="<?php echo $data_ficha_preview[0]['fecha_fin']; ?>"  readonly="" 
                                           requerido="true" mensaje="Fecha Término es un campo requerido"/>
                                </div>
                            </div>
                            <div class="col-lg-6"> 
                                <label for="cbx_orden_compra">Se realizará una modificación a las fechas del curso. No olvide realizar la modificación en la reserva de sala ya que el sistema no lo hace de manera automática. Avisar también a Docencia para la coordinación con relator. </label> 
                                <div class="input-group date ">
                                    <input type="text" class="form-control" name="fecha_inicio_nueva" id="fecha_inicio_nueva" placeholder="AAAA-MM-DD"  
                                           value="<?php echo $data_ficha_preview[0]['fecha_inicio']; ?>"
                                           requerido="true" mensaje="Fecha Inicio es un campo requerido"/>
                                    <span class="input-group-addon">A</span>
                                    <div class="input-group date  ">
                                        <input type="text" class="form-control" name="fecha_termino_nueva" id="fecha_termino_nueva" placeholder="AAAA-MM-DD"
                                               value="<?php echo $data_ficha_preview[0]['fecha_fin']; ?>"   
                                               requerido="true" mensaje="Fecha Extención es un campo requerido"/>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>

                                <br>
                                <button id="btn_aceptar_extencion"  type="button" class="ladda-button btn btn-block btn-primary" data-style="expand-right"> 
                                    <i class="fa fa-check"></i> Aceptar  
                                </button>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-lg-12">        
                                <label class="control-label" for="textarea">Motivo</label>
                                <textarea class="form-control" id="comentario" name="comentario" maxlength="200" requerido="true" mensaje="Debe ingresar motivo de rectifiación"></textarea>
                                <p id="text-out">0/200 caracteres restantes</p>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
