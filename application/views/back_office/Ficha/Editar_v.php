<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-29 [Lusi Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-20 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 id="h_text_title_ficha">Editar Ficha</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content"> 
                <form class="form-horizontal" id="frm_ficha_registro">
                <input type="hidden" name="id_ficha" id="id_ficha">
                    <p> Datos de la Empresa</p> 
                    <div class="row"> 
                        <div class="col-lg-6">
                            <label class=" control-label">Holding</label>
                            <input type="hidden" id="id_holding" name="id_holding" >
                            <select id="holding" name="holding"  class="select2_demo_3 form-control"                                    
                                    requerido="false" mensaje="Debe seleccionar Holding">
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label class=" control-label">Empresa</label>
                            <input type="hidden" id="id_empresa" name="id_empresa" >
                            <select id="empresa" name="empresa"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Empresa">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-3">
                            <label class="control-label">Dirección Empresa</label>
                            <input type="hidden" id="id_direccion_empresa" name="id_direccion_empresa">
                            <select id="direccion_empresa" name="direccion_empresa"  class="select2_demo_3 form-control"                                    
                                    requerido="true" mensaje="Debe seleccionar Dirección Empresa" 
                                    >
                                <option></option>
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <label class="control-label">Nombre Contacto</label>
                            <input type="hidden" id="id_nombre_contacto_empresa" name="id_nombre_contacto_empresa" >
                            <select id="nombre_contacto_empresa" name="nombre_contacto_empresa"  class="select2_demo_3 form-control"                                    
                                    requerido="true" mensaje="Debe seleccionar Nombre Contacto" 
                                    >
                                <option></option>
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <label class="control-label">Teléfono Contacto</label>
                            <input type="hidden" id="id_telefono_contacto_empresa" name="id_telefono_contacto_empresa" />
                            <select id="telefono_contacto_empresa" name="telefono_contacto_empresa"  class="select2_demo_3 form-control"                                    
                                    requerido="true" mensaje="Debe seleccionar Teléfono Contacto" 
                                    >
                                <option></option>
                            </select>
                        </div>                        

                        <div class="col-lg-3">
                            <label class="control-label">Dirección envío Diplomas</label>
                            <input id="lugar_entrega_diplomas" name="lugar_entrega_diplomas"  class="select2_demo_3 form-control" 
                                   requerido="true" mensaje="Debe ingresar dirección de entrega de Diplomas"
                                 /> 
                        </div>   
                    </div> 

                    <div class="hr-line-dashed"></div>
                    <p>Inscripción sence</p> 

                    <div class="row">
                        <div class="col-lg-1">
                            <label class="control-label" for="txt_num_o_c">Sence?</label>  
                            <input type="checkbox" name="chk_sence" id="chk_sence"/> 
                        </div>
                        <div class="col-lg-3">
                            <label class=" control-label">Código Sence</label>
                            <input type="hidden" id="id_codigo_sence" name="id_codigo_sence" >
                            <select id="cod_sence" name="cod_sence"  class="select2_demo_3 form-control">
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label class=" control-label">Otic</label>
                            <input type="hidden" id="id_otic" name="id_otic">
                            <select id="otic" name="otic"  class="select2_demo_3 form-control"                                    
                                    requerido="true" mensaje="Debe seleccionar OTIC">
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <label class="control-label" for="txt_num_o_c">N° Orden Compra</label>  
                            <input type="hidden" id="id_num_o_c" >
                            <input type="hidden" id="oculto_num_o_c" >
                            <input id="txt_num_o_c" name="txt_num_o_c" type="text" value=""                                   
                                   mensaje="Ingresar N° Orden Compra"
                                   />
                        </div>

                        <div class="col-lg-2">
                            <label class="control-label" for="txt_num_o_c">Sin O.C.?</label>  
                            <input type="checkbox" name="chk_sin_orden_compra" id="chk_sin_orden_compra"/>  
                        </div>

                    </div>


                    <div class="hr-line-dashed"> </div>
                    <p>Datos del Curso</p>  

                    <div class="row" >
                        <div class="col-lg-4">
                            <label class="control-label">Modalidad</label>
                            <input type="hidden" id="id_modalidad" name="id_modalidad">
                            <select id="modalidad" name="modalidad"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Modalidad">
                            </select>
                        </div>                   
                        <div class="col-lg-4">
                            <label class="control-label">Curso</label>
                            <input type="hidden" id="id_curso" name="id_curso" >
                            <select id="curso" name="curso"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Curso">
                            </select>
                        </div>

                        <div class="col-lg-4">
                            <label class="control-label">Versión</label>
                            <input type="hidden" id="id_version" name="id_version">
                            <select id="version" name="version"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Curso"
                                    >
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-lg-6"> 
                            <label class="control-label">Fecha Inicio y Término</label>
                            <div class="input-daterange input-group  col-lg-12 " id="datepicker">    
                                <input type="text" class="input-sm form-control" name="fecha_inicio" id="fecha_inicio" placeholder="DD-MM-AAAA"  
                                       requerido="true" mensaje="Fecha Inicio es un campo requerido"/>

                                <span class="input-group-addon">A</span>
                                <input type="text" class="input-sm form-control" name="fecha_termino" id="fecha_termino" placeholder="DD-MM-AAAA"
                                       requerido="true" mensaje="Fecha Término es un campo requerido"/>
                            </div>
                        </div>

                        <div class="col-lg-6">

                            <div class="col-md-6 "> 
                                <label class="control-label">Hora Inicio</label>
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input type="text" class="form-control" id="txt_hora_inicio" name="txt_hora_inicio"  
                                           requerido="true" mensaje="Ingresar Hora Inicio"  
                                           >
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>

                                <span class="help-block">13:15</span>

                            </div>
                            <div class="col-md-6 "> 
                                <label class="control-label">Hora Término</label>
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input type="text" class="form-control" id="txt_hora_termino" name="txt_hora_termino"  
                                           requerido="true" mensaje="Ingresar Hora Término" >
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                                <span class="help-block">15:00</span>
                            </div>
                        </div>
                    </div> 
                    <div class="hr-line-dashed"> </div>
                    <p>Lugar de Ejecución</p> 
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="control-label">Sede</label>
                            <input type="hidden" id="id_sede" name="id_sede">
                            <select id="sede" name="sede"  class="select2_demo_3 form-control" requerido="true" mensaje="Debe seleccionar Sede">
                            </select>
                        </div>

                        <div class="col-lg-6">

                            <label class="control-label">Sala</label>
                            <input type="hidden" id="id_sala" name="id_sala" >
                            <select id="sala" name="sala"  class="select2_demo_3 form-control" requerido="true" mensaje="Debe seleccionar Sala">
                            </select>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-lg-6">
                            <label class="control-label">Lugar Ejecución</label>

                            <input id="lugar_ejecucion" name="lugar_ejecucion"  class="select2_demo_3 form-control" 
                                   requerido="true" mensaje="Debe ingresar lugar de Ejecución" />
                        </div>

                        <div class="col-lg-6" class="">
                            <input type="hidden" id="id_dias">
                            <label class="">Días</label>
                            <br>
                            <div data-role="controlgroup" data-type="horizontal">
                                <label for="lunes">Lu</label>
                                <input type="checkbox" id="lunes" name="lunes" value="1" title="Lunes" style="margin-right: 1em;">
                                <label for="martes">Ma</label>
                                <input type="checkbox" id="martes" name="martes" value="2" title="Martes" style="margin-right: 1em;">
                                <label for="miercoles">Mi</label>
                                <input type="checkbox" id="miercoles" name="miercoles" value="3" title="Miércoles" style="margin-right: 1em;">
                                <label for="jueves">Ju</label>
                                <input type="checkbox" id="jueves" name="jueves" value="4" title="Jueves" style="margin-right: 1em;">
                                <label for="viernes">Vi</label>
                                <input type="checkbox" id="viernes" name="viernes" value="5" title="Viernes" style="margin-right: 1em;">
                                <label for="sabado">Sa</label>
                                <input type="checkbox" id="sabado" name="sabado" value="6" title="Sábado" style="margin-right: 1em;">
                                <label for="domingo">Do</label>
                                <input type="checkbox" id="domingo" name="domingo" value="7" title="Domingo" style="margin-right: 1em;">
                            </div>
                        </div>  

                    </div>
                    <div class="hr-line-dashed">   </div>
                    <p>Información Interna</p> 

                    <div class="row">

                        <div class="col-lg-6">
                            <label class="control-label">Ejecutivo Comercial</label>
                            <input type="hidden" id="id_ejecutivo" name="id_ejecutivo" >
                            <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Ejecutivo Comercial">
                                <option></option>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label class="control-label">N° Solicitud Tablet</label>
                            <select id="id_solicitud_tablet" name="id_solicitud_tablet" class="select2_demo_3 form-control" disabled>
                                <option></option>
                            </select>
                        </div>


                        <div class="col-lg-6">        
                            <label class="control-label" for="textarea">Observaciones</label>
                            <textarea class="form-control" id="comentario" name="comentario" maxlength="1000"></textarea>
                            <p id="text-out">0/1000 caracteres restantes</p>
                        </div>
                        <div class="col-lg-6">

                            <label class="control-label">Relacionar Con Ficha</label>
                            <input type="hidden" id="id_relacionar_ficha_con" name="id_relacionar_ficha_con">                            
                            <select id="relacionar_ficha_con" name="relacionar_ficha_con"  class="select2_demo_3 form-control" requerido="false" mensaje="Debe seleccionar Ficha a relacionar"  >
                                <option></option>
                            </select>
                        </div>



                    </div>
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Actualizar</button>                        
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


