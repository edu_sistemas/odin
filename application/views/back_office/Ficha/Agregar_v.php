<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-22 [Lusi Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-20 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<style>


    .swal2-container{
        z-index: 2060;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-lg-6">
                        <h5 id="h_text_title_ficha">Nueva Ficha</h5>
                    </div>
                    <div class="col-lg-6" style="text-align: right"><a target="_blank" href="../../documentacion/ficha.html">Necesito ayuda con esto</a></div>
                </div>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_ficha_registro" autocomplete="off">
                    <p> Datos de la Empresa</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class=" control-label">Holding</label>
                            <select id="holding" name="holding" class="select2_demo_3 form-control" >
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label class=" control-label">Empresa</label>

                            
                            <select id="empresa" name="empresa" class="select2_demo_3 form-control" required>
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-3">
                            <label class="control-label">Dirección Empresa</label>
                            <select id="direccion_empresa" name="direccion_empresa" class="select2_demo_3 form-control" required>
                                <option></option>
                            </select>

                            </div>
                            
                            <div class="col-lg-3">
                                <label class="control-label">Nombre Contacto</label>
                                
                                <select id="nombre_contacto_empresa" name="nombre_contacto_empresa" class="select2_demo_3 form-control" required>
                                    <option></option>
                                </select>
                            </div>
                            
                            <div class="col-lg-2">
                                <label class="control-label">Teléfono Contacto</label>
                                <select id="telefono_contacto_empresa" name="telefono_contacto_empresa" class="select2_demo_3 form-control" required>
                                    <option></option>
                                </select>
                            </div>
                            
                            <div class="col-lg-1">
                                <label class="control-label"></label><br>
                                <button id="btn_agregar_c" class="btn btn-success dim" type="button" disabled>
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>


                        <div class="col-lg-3">
                            <label class="control-label">Dirección envío Diplomas</label>
                            <input id="lugar_entrega_diplomas" name="lugar_entrega_diplomas" class="select2_demo_3 form-control" required/>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    <p>Inscripción sence</p>
                    <div class="row">
                        <div class="col-lg-1">
                            <label class="control-label" for="txt_num_o_c">Sence?</label>
                            <input type="checkbox" value="0" name="chk_sence" id="chk_sence" />
                        </div>
                        <div class="col-lg-3">
                            <label class=" control-label">Código Sence</label>
                            <select id="cod_sence" name="cod_sence" class="select2_demo_3 form-control" >
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label class="control-label">Otic</label>
                            <select id="otic" name="otic" class="select2_demo_3 form-control" >
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label class="control-label" for="txt_num_o_c">N° Orden Compra</label>
                            <input id="txt_num_o_c" name="txt_num_o_c" type="text" placeholder="" class="form-control" />
                        </div>
                        <div class="col-lg-2">
                            <label class="control-label" for="txt_num_o_c">Sin O.C.?</label>
                            <input type="checkbox" value="0" name="chk_sin_orden_compra" id="chk_sin_orden_compra" />
                        </div>
                    </div>

                    <div class="hr-line-dashed"> </div>
                    <p>Datos del Curso</p>

                    <div class="row">
                        <div class="col-lg-4">
                            <label class="control-label">Modalidad</label>
                            <select id="modalidad" name="modalidad" class="select2_demo_3 form-control" required accesskey>
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <label class="control-label">Curso</label>
                            <select id="curso" name="curso" class="select2_demo_3 form-control" required>
                                <option></option>
                            </select>
                        </div>

                        <div class="col-lg-4">
                            <label class="control-label">Versión</label>
                            <select id="version" name="version" class="select2_demo_3 form-control" required>
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        
                        <div class="col-lg-6"> 
                                <label class="control-label">Fecha Inicio y Término</label> 
                                <div class="input-datarange input-group">
                                    <input type="text"  class="form-control" name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD" requerido="true" />
                                    <span class="input-group-addon">A</span>
                                    <input type="text" class="form-control" name="fecha_termino" id="fecha_termino" placeholder="AAAA-MM-DD" requerido="true"/>
                                </div>
                            </div>
                        <div class="col-lg-6">
                            <div class="col-md-6 ">
                                <label class="control-label">Hora Inicio</label>
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input type="text" class="form-control" id="txt_hora_inicio" name="txt_hora_inicio" required>
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                                <span class="help-block">13:15</span>
                            </div>
                            <div class="col-md-6 ">
                                <label class="control-label">Hora Término</label>
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input type="text" class="form-control" id="txt_hora_termino" name="txt_hora_termino"  required>
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                                <span class="help-block">15:00</span>
                            </div>
                        </div>
                    </div>

                    <div class="hr-line-dashed"> </div>
                    <p>Lugar de Ejecución</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="control-label">Sede</label>
                            <select id="sede" name="sede" class="select2_demo_3 form-control" required>
                                <option></option>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <label class="control-label">Sala</label>
                            <select id="sala" name="sala" class="select2_demo_3 form-control" required>
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="row hidden" id="requiere_equipos_radiobutton">
                        <div class="col-lg-2">
                            <label class="control-label">¿Requiere Equipos?</label>
                        </div>
                        <div class="col-lg-10">
                            <input type="radio" name="requiere_equipos" id="requiere_equipos_1" value="1">
                            <label for="requiere_equipos_1">Si</label>
                            <input type="radio" name="requiere_equipos" id="requiere_equipos_2" value="0">
                            <label for="requiere_equipos_2">No</label>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="control-label">Dirección o Lugar Ejecución</label>
                            <input id="lugar_ejecucion" name="lugar_ejecucion" class="select2_demo_3 form-control" required />
                        </div>

                        <div class="col-lg-6">
                            <label class="control-label">Días</label>
                            <br>
                            <div data-role="controlgroup" data-type="horizontal">
                                <label for="lunes">Lu</label>
                                <input type="checkbox" id="lunes" name="lunes" value="1" title="Lunes" style="margin-right: 1em;">
                                <label for="martes">Ma</label>
                                <input type="checkbox" id="martes" name="martes" value="2" title="Martes" style="margin-right: 1em;">
                                <label for="miercoles">Mi</label>
                                <input type="checkbox" id="miercoles" name="miercoles" value="3" title="Miércoles" style="margin-right: 1em;">
                                <label for="jueves">Ju</label>
                                <input type="checkbox" id="jueves" name="jueves" value="4" title="Jueves" style="margin-right: 1em;">
                                <label for="viernes">Vi</label>
                                <input type="checkbox" id="viernes" name="viernes" value="5" title="Viernes" style="margin-right: 1em;">
                                <label for="sabado">Sa</label>
                                <input type="checkbox" id="sabado" name="sabado" value="6" title="Sábado" style="margin-right: 1em;">
                                <label for="domingo">Do</label>
                                <input type="checkbox" id="domingo" name="domingo" value="7" title="Domingo" style="margin-right: 1em;">
                            </div>
                        </div>

                    </div>
                    <div class="hr-line-dashed"> </div>
                    <p>Información Interna</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="control-label">Ejecutivo Comercial</label>
                            <select id="ejecutivo" name="ejecutivo" class="select2_demo_3 form-control" required>
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label">N° Solicitud Tablet</label>
                            <select id="id_solicitud_tablet" name="id_solicitud_tablet" class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label" for="textarea">Observaciones</label>
                            <textarea class="form-control" id="comentario" name="comentario" maxlength="1000"></textarea>
                            <p id="text-out">0/1000 caracteres restantes</p>
                        </div>
                        <div class="col-lg-6">
                            <label class="control-label">Relacionar Con Ficha</label>
                            <select id="relacionar_ficha_con" name="relacionar_ficha_con" class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                            <div class="form-group col-lg-6" id="datepicker_envio_carta">
                                <label class="control-label">Fecha Envío Carta Presentación</label>
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" id="fecha_envio_carta" name="fecha_envio_carta" placeholder="dd-mm-aaaa">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class=" col-lg-10 ">
                            <button class="btn btn-sm btn-primary " name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    <div class="modal inmodal" id="modal_agregar_contacto" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Datos de contacto</h4>    

                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="frm_nuevo_contacto_empresa">
                        <input type="hidden" id="id_empresa_agregar" name="id_empresa_agregar" />
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="nombre_contacto_agregar">Nombre</label>  
                            <div class="col-md-6">
                                <input id="nombre_contacto_agregar" name="nombre_contacto_agregar" required 
                                        maxlength="200"
                                        type="text" placeholder="ingrese nombre del contacto" class="form-control input-md">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="tipo_contacto_cbx">Tipo Contacto</label>  
                            <div class="col-md-6">
                                <select id="tipo_contacto_cbx" name="tipo_contacto_cbx" required></select>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="cargo_contacto_agregar">Cargo</label>  
                            <div class="col-md-6">
                                <input id="cargo_contacto_agregar" name="cargo_contacto_agregar" required 
                                        maxlength="200"
                                        type="text" placeholder="ingrese cargo del contacto" class="form-control input-md">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="departamento_contacto_agregar">Departamento</label>  
                            <div class="col-md-6">
                                <input id="departamento_contacto_agregar" name="departamento_contacto_agregar" required 
                                        maxlength="200"
                                        type="text" placeholder="ingrese departamento del contacto" class="form-control input-md">

                            </div>
                        </div>

                         

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="nivel_contacto_cbx">Nivel de Contacto</label>  
                            <div class="col-md-6">
                                <select id="nivel_contacto_cbx" name="nivel_contacto_cbx" required></select>

                            </div>
                        </div>

                        

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="correo_contacto_agregar">Email</label>
                            <div class="col-md-6">
                                <input id="correo_contacto_agregar" name="correo_contacto_agregar" maxlength="200"
                                    type="email" placeholder="correo@dominio.cl" class="form-control input-md">
                        
                            </div>
                        </div>
                        
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="telefono_contacto_agregar">Telefono</label>
                            <div class="col-md-6">
                                <input type="tel" id="telefono_contacto_agregar" name="telefono_contacto_agregar" required placeholder="9 87845621"
                                    class="form-control input-md">
                        
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4" style="text-align: center">
                                <br>
                                <button type="submit" id="btn_agregar_contacto" class="btn btn-primary"><i class="fa fa-check"></i>Aceptar</button>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    </div>

