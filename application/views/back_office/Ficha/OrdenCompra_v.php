<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-02-07 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>
<style>
    #modalLoading{
        background-color: rgba(255, 255, 255, 0.9);
        position: fixed;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        z-index: 8000;
    }
    #cargando{
        width: 600px;
        height: 300px;

        position:absolute;
        left:0; right:0;
        top:0; bottom:0;
        margin:auto;

        max-width:100%;
        max-height:100%;
        overflow:auto;
        text-align: center;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Datos</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <p> Datos de la Venta</p> 
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label"><strong>N° de Ficha:</strong></label>
                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['num_ficha']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class="  control-label">Tipo de Venta:</label>
                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['categoria']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class="  control-label"><strong> Estado Rectificación:</strong> </label>
                        <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['estado']; ?>">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <p> Datos de la Empresa</p> 

                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Holding</label>

                        <input type="hidden" class="form-control" readonly="" id="id_holding" value="<?php echo $data_ficha_preview[0]['id_holding']; ?>">


                        <input type="text" class="form-control" readonly="" id="holding"
                               value="<?php echo $data_ficha_preview[0]['holding']; ?>">

                    </div>
                    <div class="col-md-6">
                        <label class="  control-label">Empresa</label>

                        <input type="text" class="form-control" readonly=""
                               id="id_empresa"
                               value="<?php echo $data_ficha_preview[0]['rut_empresa'] . " / " . $data_ficha_preview[0]['empresa']; ?>">

                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-3">
                        <label class="control-label">Dirección Empresa</label>
                        <input type="text" class="form-control" readonly=""
                               id="direccion_empresa"
                               value="<?php echo $data_ficha_preview[0]['direccion_empresa']; ?>">

                    </div>

                    <div class="col-lg-3">
                        <label class="control-label">Nombre Contacto</label>

                        <input type="text" class="form-control" readonly=""
                               id="nombre_contacto_empresa"
                               value="<?php echo $data_ficha_preview[0]['nombre_contacto']; ?>">
                    </div>

                    <div class="col-lg-3">
                        <label class="control-label">Teléfono Contacto</label>

                        <input type="text" class="form-control" readonly=""
                               id="telefono_contacto_empresa"
                               value="<?php echo $data_ficha_preview[0]['telefono_contacto']; ?>">
                    </div>                        

                    <div class="col-lg-3">
                        <label class="control-label">Dirección envío Diplomas</label>

                        <input type="text" class="form-control" readonly=""
                               id="lugar_entrega_diplomas" value="<?php echo $data_ficha_preview[0]['direccion_diplomas']; ?>">
                    </div>   
                </div> 

                <div class="hr-line-dashed"></div>
                <p>Datos de la OTIC</p> 

                <div class="row">


                    <div class="col-lg-4">
                        <label class="  control-label">Otic</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['otic']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class=" control-label">Código Sence</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['codigo_sence']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class=" control-label">N° Orden Compra</label>
                        <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['num_orden_compra']; ?>">
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Datos del Curso</p>  
                <div class="row"> 
                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Modalidad</label>
                        <input type="text" class="form-control" readonly=""
                               id="modalidad"
                               value="<?php echo $data_ficha_preview[0]['modalidad']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Curso</label>
                        <input type="text" class="form-control" readonly=""  id="curso_descrip" value="<?php echo $data_ficha_preview[0]['descripcion_producto']; ?>">
                    </div>

                    <div class="col-md-4"> 
                        <label class="col-md-1 control-label">Versión</label>
                        <input type="text" class="form-control" readonly=""  id="version"  value="<?php echo $data_ficha_preview[0]['version']; ?>">
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6">
                        <label class=" control-label">Fecha Inicio y Término</label>
                        <div class="input-daterange input-group  col-lg-12 ">
                            <input type="text" readonly="" class="form-control"
                                   name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"
                                   value="<?php echo $data_ficha_preview[0]['fecha_inicio']; ?>"
                                   requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                            <span class="input-group-addon">A</span> 
                            <input type="text"  class="form-control" name="fecha_termino" id="fecha_termino"
                                   placeholder="AAAA-MM-DD" value="<?php echo $data_ficha_preview[0]['fecha_fin']; ?>" readonly=""
                                   requerido="true" mensaje="Fecha Término es un campo requerido" />
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="col-md-6 ">
                            <label class="  control-label">Hora Inicio</label>
                            <div class="input-group clockpicker">
                                <input type="time" class="form-control" id="txt_hora_inicio"
                                       name="txt_hora_inicio"
                                       value="<?php echo $data_ficha_preview[0]['hora_inicio']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Inicio" readonly="" />
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <label class=" control-label">Hora Término</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input type="time" class="form-control" id="txt_hora_termino"
                                       name="txt_hora_termino"
                                       value="<?php echo $data_ficha_preview[0]['hora_termino']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Término" readonly="">
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hr-line-dashed"> </div>
                <p>Lugar de Ejecución</p>  
                <div class="row">
                    <div class="col-lg-6"> 
                        <label class=" control-label">Sede</label>
                        <input type="text" class="form-control" readonly="" id="id_sede"
                               value="<?php echo $data_ficha_preview[0]['sede']; ?>">

                    </div>
                    <div class="col-lg-6">
                        <label class=" control-label">Sala</label>
                        <input type="text" class="form-control" readonly="" id="id_sala"
                               value="<?php echo $data_ficha_preview[0]['sala']; ?>">
                    </div>

                </div>
                <div class="row">


                    <div class="col-lg-6">
                        <label class="control-label">Dirección o Lugar Ejecución</label>
                        <input id="lugar_ejecucion" name="lugar_ejecucion" class="form-control" readonly="" 
                               value="<?php echo $data_ficha_preview[0]['lugar_ejecucion']; ?>" 
                               />
                    </div>

                    <input  type="hidden" id="id_dias" value="<?php echo $data_ficha_preview[0]['id_dias']; ?>">
                    <div class="col-lg-6">
                        <label class="control-label">Días</label>
                        <label class="checkbox-inline" for="inlineCheckbox1"> 
                            <input
                                type="checkbox" readonly="" id="lunes" name="lunes" value="1"
                                title="Lunes"> Lu
                        </label> <label class="checkbox-inline" for="inlineCheckbox2"> <input
                                type="checkbox" readonly="" id="martes" name="martes" value="2"
                                title="Martes"> Ma
                        </label> <label class="checkbox-inline" for="inlineCheckbox3"> <input
                                type="checkbox" readonly="" id="miercoles" name="miercoles"
                                value="3" title="Miércoles"> Mi
                        </label> <label class="checkbox-inline" for="inlineCheckbox4"> <input
                                type="checkbox" readonly="" id="jueves" name="jueves" value="4"
                                title="Jueves"> Ju
                        </label> <label class="checkbox-inline" for="inlineCheckbox5"> <input
                                type="checkbox" readonly="" id="viernes" name="viernes"
                                value="5" title="Viernes"> Vi
                        </label> <label class="checkbox-inline" for="inlineCheckbox6"> <input
                                type="checkbox" readonly="" id="sabado" name="sabado" value="6"
                                title="Sábado"> Sa
                        </label> <label class="checkbox-inline" for="inlineCheckbox7"> <input
                                type="checkbox" readonly="" id="domingo" name="domingo"
                                value="7" title="Domingo"> Do
                        </label> 
                    </div>
                </div>
                <div class="hr-line-dashed">   </div>
                <p>Información Interna</p> 
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Ejecutivo Comercial</label>
                        <input type="text" class="form-control" readonly=""
                               id="id_ejecutivo"
                               value="<?php echo $data_ficha_preview[0]['ejecutivo']; ?>">
                    </div>

                    <div class="col-lg-6">
                        <label class="control-label">N° Solicitud Tablet</label>
                        <input id="id_solicitd_tablet" name="id_solicitd_tablet" onkeypress="return solonumero(event)" class="form-control" 
                               requerido="false" mensaje="Debe ingresar N° Solicitud tablet" maxlength="8"
                               disabled="disabled"
                               value="<?php echo $data_ficha_preview[0]['id_solicitud_tablet']; ?>"

                               />
                    </div>


                    <div class="col-md-12">
                        <label class="  control-label" for="textarea">Observaciones</label>
                        <textarea readonly="" class="form-control" id="comentario"
                                  name="comentario" maxlength="1000"><?php echo $data_ficha_preview[0]['comentario_orden_compra']; ?></textarea>
                        <p id="text-out">0/1000 caracteres restantes</p>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Cargar Alumnos O.C.</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content"> 
                <form  name="frm_carga_orden_compra" id="frm_carga_orden_compra">
                    <div class="row">
                        <div class="col-md-12">  
                            <div class="col-md-6">        
                                <label class="col-md-4 control-label" for="cbx_orden_compra">Orden Compra</label>
                                <div class="col-md-12">

                                    <select id="cbx_orden_compra" name="cbx_orden_compra" class="form-control"   
                                            requerido="true"
                                            mensaje="Debe seleccionar Orden de Compra"
                                            >
                                        <option value="">Seleccione O.C.</option>
                                        <?php for ($i = 0; $i < count($data_orden_compra); $i++) { ?>
                                            <option value="<?php echo $data_orden_compra[$i]['id'] ?>"><?php echo $data_orden_compra[$i]['text'] ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-6">   
                                <div class="form-group">
                                    <!-- Select Basic -->
                                    <label class="col-md-4 control-label" for="">Razón social</label>
                                    <div class="col-md-12">
                                        <input type="hidden" id="id_empresa_original" name="id_empresa_original" value="<?php echo $data_ficha_preview[0]['id_empresa'] ?>">
                                        <select id="empresa" name="empresa"  class="select2_demo_3 form-control"
                                                requerido="true" mensaje="Razón social" 
                                                >
                                            <option></option>
                                        </select>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12"> 
                            <div class="col-md-6"> 
                                <!-- Select Basic -->
                                <div class="col-md-12">

                                    <input type="hidden" id="id_otic_original" name="id_otic_original" value="<?php echo $data_ficha_preview[0]['id_otic'] ?>">
                                    <!--                                    <label class="col-md-4 control-label" for="cbx_orden_compra">Orden Compra</label>-->
                                    <label class="control-label">OTIC</label>

                                    <select id="otic" name="otic"  class="select2_demo_3 form-control"                                    
                                            requerido="true" mensaje="Debe seleccionar OTIC" 
                                            >
                                        <option></option>
                                    </select>

                                </div>

                            </div>

                            <div class="col-md-6">   
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <br>

                                        <label class="checkbox-inline">Comite Bipartito</label>
                                            <label class="checkbox-inline"> 
                                                <input type="checkbox" name="comite_bipartito" value="0" id="inlineCheckbox1"
                                                    mensaje="Debe seleccionar si tiene Comite Bipartito"  
                                                    > No
                                            </label>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" name="comite_bipartito" value="1" id="inlineCheckbox2"
                                                    mensaje="Debe seleccionar si tiene Comite Bipartito"  
                                                    > SI
                                            </label> 

                                        <label id="error_comite"> </label> 
                                    
                                    
                                        
                                            <label class="col-lg-2 control-label">¿Tiene precontrato?</label>
                                            <div class="col-lg-1">
                                                <input id="precontrato" type="checkbox" value="">
                                            </div>
                                    </div>

                                        
                                           

                                     

                                    
                                   
                                    <!-- Select Basic -->
                                    <div class="col-md-6">
                                        <br>


                                        <button id="id_asignar_oc" name="id_asignar_oc" type="button" class="btn btn-primary   ">
                                            Actualizar Datos OC
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <hr>
                        <div class="form-group">
                            <label class="col-md-12 control-label" for="cbx_orden_compra">Plantilla de Alumno</label>
                            <div class="col-md-12"> 


                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">

                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Seleccionar Archivo</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="file_carga_alumnos" id="file_carga_alumnos" accept=".xls,.xlsx" accesskey=""
                                               requerido="true" mensaje="Debe seleccionar Archivo válido" 
                                               >
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" id="btn_cleanr_file" data-dismiss="fileinput">
                                        Eliminar
                                    </a>
                                </div>  
                                <button id="btn_carga"  type="button" class="ladda-button btn btn-primary" data-style="expand-right"> 
                                    <i class="fa fa-upload"></i> Subir  
                                </button>
                                <input type="hidden" value="<?php echo $data_ficha[0]['id_ficha']; ?>" id="id_ficha" name="id_ficha" hidden> 
                                <a href="<?php echo base_url() . "assets/plantillas/PlantillaInternaAlumnos.xlsx"; ?>" target="_blank" >
                                    <button type="button" id="btn_descarga" class="btn btn-file pull-right"><i class="fa fa-download"></i>Descargar Formato de  Carga </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row" id="div_carga_empresa" >
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Informe de carga</h5>

                <div class="ibox-tools">                        
                    <a class="collapse-link" hidden  id="a_tab_tabla_carga">
                        <i class="fa fa-chevron-down"></i>
                    </a>

                </div>
            </div>
            <div class="ibox-content"  > 
                <table id="tbl_orden_compra_carga_alumno" class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <th>Rut</th>                            
                            <th>Nombre</th>
                            <th title="Apellido Paterno">A. Paterno</th>  
                            <th>CC.CC</th>
                            <th>%FQ</th>
<!--                            <th title="Costo Otic">C. Otic</th>
                            <th title="Costo Empresa">C.Empresa</th>
                            <th title="Valor Agregado">Valor Agregado</th>-->
                            <th>V. Total </th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>                                
                            <th>Rut</th>                            
                            <th>Nombre</th>
                            <th title="Apellido Paterno">A. Paterno</th>  
                            <th>CC.CC</th>
                            <th>%FQ</th>
<!--                            <th title="Costo Otic">C. Otic</th>
                            <th title="Costo Empresa">C.Empresa</th>
                            <th title="Valor Agregado">Valor Agregado</th>-->
                            <th>V. Total </th>
                            <th>Estado</th>
                        </tr>
                    </tfoot>
                </table>

                <div class="ibox-tools">

                    <button type="button" id="btn_aprobar_carga" class="btn btn-primary">Aprobar Registros</button>
                    <button type="button" id="btn_cancelar" class="btn btn-danger">Cancelar</button>

                </div>

            </div>
        </div>
    </div>
</div>

<div id="modalLoading">
    <div id="cargando">
        <img src="../../../../assets/img/loading.gif">
        <h1>Cargando</h1>
        <h3>No cierre esta ventana por favor</h3>
    </div>
</div>
