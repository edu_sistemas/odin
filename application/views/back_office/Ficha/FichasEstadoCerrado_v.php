<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<style>
    #modal-select .select2-container{
        z-index: 2060; 
    }
    /* Timeline */
    .timeline,
    .timeline-horizontal {
        list-style: none;
        padding: 20px;
        position: relative;
    }
    .timeline:before {
        top: 40px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        left: 50%;
        margin-left: -1.5px;
    }
    .timeline .timeline-item {
        margin-bottom: 20px;
        position: relative;
    }
    .timeline .timeline-item:before,
    .timeline .timeline-item:after {
        content: "";
        display: table;
    }
    .timeline .timeline-item:after {
        clear: both;
    }
    .timeline .timeline-item .timeline-badge {
        color: #fff;
        width: 54px;
        height: 54px;
        line-height: 52px;
        font-size: 22px;
        text-align: center;
        position: absolute;
        top: 18px;
        left: 50%;
        margin-left: -25px;
        background-color: #7c7c7c;
        border: 3px solid #ffffff;
        z-index: 100;
        border-top-right-radius: 50%;
        border-top-left-radius: 50%;
        border-bottom-right-radius: 50%;
        border-bottom-left-radius: 50%;
    }
    .timeline .timeline-item .timeline-badge i,
    .timeline .timeline-item .timeline-badge .fa,
    .timeline .timeline-item .timeline-badge .glyphicon {
        top: 2px;
        left: 0px;
    }
    .timeline .timeline-item .timeline-badge.primary {
        background-color: #1f9eba;
    }
    .timeline .timeline-item .timeline-badge.info {
        background-color: #5bc0de;
    }
    .timeline .timeline-item .timeline-badge.success {
        background-color: #59ba1f;
    }
    .timeline .timeline-item .timeline-badge.warning {
        background-color: #d1bd10;
    }
    .timeline .timeline-item .timeline-badge.danger {
        background-color: #ba1f1f;
    }
    .timeline .timeline-item .timeline-panel {
        position: relative;
        width: 46%;
        float: left;
        right: 16px;
        border: 1px solid #c0c0c0;
        background: #ffffff;
        border-radius: 2px;
        padding: 20px;
        -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }
    .timeline .timeline-item .timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -16px;
        display: inline-block;
        border-top: 16px solid transparent;
        border-left: 16px solid #c0c0c0;
        border-right: 0 solid #c0c0c0;
        border-bottom: 16px solid transparent;
        content: " ";
    }
    .timeline .timeline-item .timeline-panel .timeline-title {
        margin-top: 0;
        color: inherit;
    }
    .timeline .timeline-item .timeline-panel .timeline-body > p,
    .timeline .timeline-item .timeline-panel .timeline-body > ul {
        margin-bottom: 0;
    }
    .timeline .timeline-item .timeline-panel .timeline-body > p + p {
        margin-top: 5px;
    }
    .timeline .timeline-item:last-child:nth-child(even) {
        float: right;
    }
    .timeline .timeline-item:nth-child(even) .timeline-panel {
        float: right;
        left: 16px;
    }
    .timeline .timeline-item:nth-child(even) .timeline-panel:before {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }
    .timeline-horizontal {
        list-style: none;
        position: relative;
        padding: 20px 0px 20px 0px;
        display: inline-block;
    }
    .timeline-horizontal:before {
        height: 3px;
        top: auto;
        bottom: 26px;
        left: 56px;
        right: 0;
        width: 100%;
        margin-bottom: 20px;
    }
    .timeline-horizontal .timeline-item {
        display: table-cell;
        height: 280px;
        width: 20%;
        min-width: 100px;
        float: none !important;
        padding-left: 0px;
        padding-right: 20px;
        margin: 0 auto;
        vertical-align: bottom;
    }
    .timeline-horizontal .timeline-item .timeline-panel {
        top: auto;
        bottom: 64px;
        display: inline-block;
        float: none !important;
        left: 0 !important;
        right: 0 !important;
        width: 100%;
        margin-bottom: 20px;
    }
    .timeline-horizontal .timeline-item .timeline-panel:before {
        top: auto;
        bottom: -16px;
        left: 28px !important;
        right: auto;
        border-right: 16px solid transparent !important;
        border-top: 16px solid #c0c0c0 !important;
        border-bottom: 0 solid #c0c0c0 !important;
        border-left: 16px solid transparent !important;
    }
    .timeline-horizontal .timeline-item:before,
    .timeline-horizontal .timeline-item:after {
        display: none;
    }
    .timeline-horizontal .timeline-item .timeline-badge {
        top: auto;
        bottom: 0px;
        left: 43px;
    }
    .fuente_roja{
        color:red;
    }
</style>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda <?php // echo 'Versión actual de PHP: ' . phpversion();                ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="datepicker">Ingrese un rango de fechas</label>
                                <div id="data_5">
                                    <div class="input-daterange input-group" id="datepicker" style="width: 100%;">
                                        <input type="text" class="input-sm form-control" id="start" name="start" placeholder="Fecha Inicio" value="" readonly=""/>
                                        <span class="input-group-addon">hasta</span>
                                        <input type="text" class="input-sm form-control" id="end" name="end" placeholder="Fecha Fin" value="" readonly="" />
                                    </div>
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="estado">Estado</label><br>
                                <select id="estado" name="estado"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select>
                                <br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="holding">Holding</label><br>
                                <select id="holding" name="holding"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="empresa">Empresa</label><br>
                                <select id="empresa" name="empresa"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="holding">Ejecutivo</label><br>
                                <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="periodo">Periodo</label><br>
                                <select id="periodo" name="periodo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="num_ficha">N° Ficha</label><br>
                                <input id="num_ficha" name="num_ficha"  class="select2_demo_3 form-control" style="width: 100%"/>

                            </div>
                            <div class="col-lg-6">
                                <br>
                                <br>
                                <button type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                                <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Mis Fichas En estado "Cerrado"</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <button type="button" class="btn btn-primary btn-xs">De 0 a 19 dias de antigüedad</button>
                    <button type="button" class="btn btn-warning btn-xs">De 20 a 29 dias de antigüedad</button>
                    <button type="button" class="btn btn-danger btn-xs">Igual o mayor a 30 dias de antigüedad</button>
                    <div class="table-responsive">


                        <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>

                                    <th title="Opciones permitidas"><i class="fa fa-level-down"></i></th>
                                    <th title="Persona que administra esta Ficha" >Gest.</th>
                                    <th>Estado</th>
                                    <th>N°Ficha</th>
                                    <th class="hidden">factura</th>
                                    <th title="Tipo venta">T.V</th>
                                    <th title="Modalidad -  Familia">Mo. - Fa.</th>
                                    <th title="Fecha Inicio -  Fecha Termino">F. Inicio. - F. Termino</th>
                                    <th>Descripción</th>
                                    <th>Otic</th>                                    
                                    <th>Empresa</th>
                                    <th>Total</th>
                                    <th>Dias desde Cierre</th>
                                    <th>Docs.</th>
                                    <th>PART.</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th title="Opciones permitidas"><i class="fa fa-level-up"></i></th>
                                    <th title="Persona que administra esta Ficha" >Gest.</th>
                                    <th>Estado</th>
                                    <th>N°Ficha</th>
                                    <th class="hidden">factura</th>
                                    <th title="Tipo venta">T.V</th>
                                    <th title="Modalidad -  Familia">Mo. - Fa.</th>
                                    <th title="Fecha Inicio -  Fecha Termino">F. Inicio. - F. Termino</th>
                                    <th>Descripción</th>
                                    <th>Otic</th>                                    
                                    <th>Empresa</th>
                                    <th>Total</th>
                                    <th>Dias desde Cierre</th>
                                    <th>Docs.</th>
                                    <th>PART.</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiddens Modals para mostrar contactos -->

    <div class="modal inmodal" id="modal_documentos_ficha" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight" id="modal-principal" onclick="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Documentos para ficha N°: <h4 class="modal-title" id="modal_id_ficha"></h4></h4>
                </div>
                <div class="modal-body">
                    <div class="btn-group">

                        <button class="btn btn-success" type="button" id="btn_nuevo_contacto" title="Subir Archivos">Subir un nuevo archivo</button> 
                        <br><br><br>
                    </div>
                    <div  id="div_frm_add" style="display: none;"> 

                        <form class="form-horizontal" id="frm_nuevo_contacto_empresa" >
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="control-label">Archivos Adjuntos</label>
                                    <span class="btn btn-file fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Seleccionar archivos</span>
                                        <!-- The file input field used as target for the file upload widget  -->
                                        <input id="fileupload" type="file" name="fileupload[]" multiple="" accept="application/pdf,application/vnd.ms-excel,image/gif, image/jpeg,image/png,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">

                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <div id="progress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <span id="porsncetaje_carga" class="pull-right">0%</span>
                                </div> 
                            </div>
                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_guardar_contacto"></label>
                                <div class="col-md-8">
                                    <button id="btn_cancelar_contacto" type="button" name="btn_cancelar_contacto" class="btn btn-success">Volver</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- <div class="table-responsive" id="div_tbl_contacto_empresa">   -->
                    <table id="tbl_documentos_ficha" class="table-md-4 table-bordered" >
                        <thead>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>        
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>         

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>          
                                <th>Opciones</th>
                            </tr>
                        </tfoot>
                    </table>    
                    <!-- </div> -->
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-white" data-dismiss="modal" id="btn_cerrarmodal_doc">Cerrar</button>

                </div>
            </div>
        </div>
    </div>


    <!-- Modal Motivo Rechazo -->

    <div class="modal inmodal" id="modal_motivo_rechazo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Motivo de Corrección para la Ficha N°: <h4 class="modal-title" id="modal_id_ficha2"></h4></h4>    

                </div>
                <div class="modal-body">

                    <input type="text" id="id_ficha" name="id_ficha" hidden>

                    <div class="form-group">
                        <textarea name="motivo_rechazo" id="motivo_rechazo" class="form-control" cols="30" rows="10" readonly=""></textarea>
                    </div>

                    <div class="form-group">

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>

    <!--     INGRESAR ID ACCION SENCE -->

    <div class="modal inmodal" id="modal_id_sence" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                    <h3>Ingrese ID Acción Sence para la ficha</h3>
                    <h4 id="id_ficha_modal_accion"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="frm_id_accion_sence" name="frm_id_accion_sence">

                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div id="modal-select" class="col-lg-4">
                                <label for="combo_oc">Orden de Compra:</label><br>
                                <select id="combo_oc" name="combo_oc" style="width: 100%" requerido="true">
                                    <option>Seleccione OC</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <label for="id_accion">ID Accion Sence:</label><br>
                                <input type="text" id="id_accion" name="id_accion" requerido="true" mensaje="ingrese el id de accción sence...">
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <br>
                                <button type="button" id="btn_insert_accion_sence" class="btn btn-warning pull-left">Ingresar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>
                    </form>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>

    <!--     INGRESAR ID ACCION SENCE -->

    <div class="modal inmodal" id="modal_change_oc" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>

                    <h3>Modifique la orden de compra de la Ficha</h3>
                    <h4 id="id_ficha_modal_accion"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="frm_change_oc" name="frm_change_oc">

                        <input  type="hidden"  value="" id="id_ficha_frm_change_oc" name="id_ficha_frm_change_oc"/>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div id="modal-select" class="col-lg-4">
                                <label for="">Acciones:</label><br>
                                <select id="combo_decicion" name="combo_decicion" style="width: 100%" requerido="true" mensaje="Seleccione una acción">
                                    <option value="">Seleccione acción</option>
                                    <option value="1">Nueva orden Compra</option>
                                    <option value="2">Cambiar Orden Compra</option>
                                    <option value="3">Eliminar Orden Compra</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>

                        <div class="row hidden" id="div_combo_oc">
                            <div class="col-lg-4">
                            </div>
                            <div id="modal-select" class="col-lg-4">
                                <label for="combo_oc">Orden de Compra:</label><br>
                                <select id="combo_oc" name="combo_oc" style="width: 100%" requerido="true">
                                    <option>Seleccione OC</option>
                                </select>
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>

                        <div class="row hidden" id="div_orden_compra" >
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <label for="" id="label_orden_compra">Cambiar por:</label><br>
                                <input type="text" id="new_orden_compra" name="new_orden_compra" requerido="true" mensaje="ingrese nuevo numero de orden de compra...">
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <br>
                                <button type="button" id="btn_change_orden_compra" class="btn btn-warning pull-left">Aceptar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>
                    </form>



                </div>
                <div class="modal-footer">
                    <button type="button" id="btn_cerrar_modal_orden_compra" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>


    <!--    ------------------------------MODAL TIMELINE---------------------------------- -->

    <div id="modal-timeline" class="modal inmodal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width: 1280px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Información para la ficha N°: <ficha></ficha></h4>
                </div>
                <div class="modal-body">

                    <table id="tb_resumen_ficha" class="table table-striped">

                    </table>

                    <!--TIMELINE START -->

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="page-header">
                                    <h1>Linea de Tiempo y sus Eventos</h1>
                                </div>
                                <div style="display:inline-block;width:100%;overflow-y:auto;">
                                    <ul class="timeline timeline-horizontal" id="eventos-timeline">
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--TIMELINE END -->
                    <h2>Historial de eventos</h2>

                    <table id="tbl_log_timeline" class="table table-striped">
                        <thead class="thead-inverse">
                            <tr>
                                <th>#</th>
                                <th>Fecha</th>
                                <th>Acción</th>
                                <th>Usuario</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>




</div>
