<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-02 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>

<div class="wrapper wrapper-content animated fadeInRight">
<input type="hidden" class="form-control" readonly="" id="id_holding" value="<?php echo $data_ficha_preview[0]['id_holding']; ?>">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Vista Previa</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <p> Datos de la Venta</p> 
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label"><strong>N° de Ficha:</strong></label>
                            <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['num_ficha']; ?>">
                        </div>
                        <div class="col-md-4">
                            <label class="  control-label">Tipo de Venta:</label>
                            <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['categoria']; ?>">
                        </div>
                        <div class="col-md-4">
                            <label class="  control-label"><strong>Estado:</strong> </label>
                            <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha_preview[0]['estado']; ?>">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <p> Datos de la Empresa</p> 

                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Holding</label>

                            <input type="text" class="form-control" readonly="" id="holding"
                                   value="<?php echo $data_ficha_preview[0]['holding']; ?>">

                        </div>
                        <div class="col-md-6">
                            <label class="  control-label">Empresa</label>

                            <input type="text" class="form-control" readonly=""
                                   id="id_empresa"
                                   value="<?php echo $data_ficha_preview[0]['rut_empresa'] . " / " . $data_ficha_preview[0]['empresa']; ?>">

                        </div>
                    </div>
                    <div class="row">

                        <div class="col-lg-3">
                            <label class="control-label">Dirección Empresa</label>
                            <input type="text" class="form-control" readonly=""
                                   id="direccion_empresa"
                                   value="<?php echo $data_ficha_preview[0]['direccion_empresa']; ?>">

                        </div>

                        <div class="col-lg-3">
                            <label class="control-label">Nombre Contacto</label>

                            <input type="text" class="form-control" readonly=""
                                   id="nombre_contacto_empresa"
                                   value="<?php echo $data_ficha_preview[0]['nombre_contacto']; ?>">
                        </div>

                        <div class="col-lg-3">
                            <label class="control-label">Teléfono Contacto</label>

                            <input type="text" class="form-control" readonly=""
                                   id="telefono_contacto_empresa"
                                   value="<?php echo $data_ficha_preview[0]['telefono_contacto']; ?>">
                        </div>                        

                        <div class="col-lg-3">
                            <label class="control-label">Dirección envío Diplomas</label>

                            <input type="text" class="form-control" readonly=""
                                   id="lugar_entrega_diplomas" value="<?php echo $data_ficha_preview[0]['direccion_diplomas']; ?>">
                        </div>   
                    </div> 

                    <div class="hr-line-dashed"></div>
                    <p>Datos de la OTIC</p> 
                    <div class="row">
                        <div class="col-lg-4">
                            <label class="  control-label">Otic</label>
                            <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['otic']; ?>">
                        </div>

                        <div class="col-md-4">
                            <label class=" control-label">Código Sence</label>
                            <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['codigo_sence']; ?>">
                        </div>
                        <div class="col-md-4">
                            <label class=" control-label">N° Orden Compra</label>
                            <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha_preview[0]['num_orden_compra']; ?>">
                        </div>
                    </div>
                    <div class="hr-line-dashed"> </div>
                    <p>Datos del Curso</p>  
                    <div class="row" >
                        <div class="col-md-4"> 
                            <label class="col-md-1 control-label">Modalidad</label>
                            <input type="text" class="form-control"  readonly="" id="modalidad" value="<?php echo $data_ficha_preview[0]['modalidad']; ?>">
                        </div>
                        <div class="col-md-4"> 
                            <label class="col-md-1 control-label">Curso</label>
                            <input type="text" class="form-control"  readonly="" id="curso_descrip" value="<?php echo $data_ficha_preview[0]['descripcion_producto']; ?>">
                        </div>
                        <div class="col-md-4"> 
                            <label class="col-md-1 control-label">Versión</label>
                            <input type="text" class="form-control" readonly=""  id="version"  value="<?php echo $data_ficha_preview[0]['version']; ?>">
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-lg-6">
                            <label class=" control-label">Fecha Inicio y Término</label>
                            <div class="input-daterange input-group  col-lg-12 ">
                                <input type="text" readonly="" class="form-control"
                                       name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"
                                       value="<?php echo $data_ficha_preview[0]['fecha_inicio']; ?>"
                                       requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                                <span class="input-group-addon">A</span> 
                                <input type="text"  class="form-control" name="fecha_termino" id="fecha_termino"
                                       placeholder="AAAA-MM-DD" value="<?php echo $data_ficha_preview[0]['fecha_fin']; ?>" readonly=""
                                       requerido="true" mensaje="Fecha Término es un campo requerido" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="col-md-6 ">
                                <label class="  control-label">Hora Inicio</label>
                                <div class="input-group clockpicker">
                                    <input type="time" class="form-control" id="txt_hora_inicio"
                                           name="txt_hora_inicio"
                                           value="<?php echo $data_ficha_preview[0]['hora_inicio']; ?>"
                                           requerido="true" mensaje="Ingresar Hora Inicio" readonly="" />
                                    <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <label class=" control-label">Hora Término</label>
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input type="time" class="form-control" id="txt_hora_termino"
                                           name="txt_hora_termino"
                                           value="<?php echo $data_ficha_preview[0]['hora_termino']; ?>"
                                           requerido="true" mensaje="Ingresar Hora Término" readonly="">
                                    <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed">   </div>
                    <p>información Interna</p> 
                    <div class="row">
                        <div class="col-md-12">                        
                            <label class=" control-label">Ejecutivo Comercial</label>
                            <input type="text" class="form-control" readonly=""  id="id_ejecutivo" value="<?php echo $data_ficha_preview[0]['ejecutivo']; ?>">    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Rectificación Alumnos O.C.</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content"> 

                    <form  name="frm_carga_orden_compra" id="frm_carga_orden_compra">
                        <div class="row">
                            <div class="col-lg-6"> 
                                <label for="cbx_orden_compra">Orden Compra</label>
                                <select id="cbx_orden_compra" name="cbx_orden_compra" class="form-control" requerido="true" mensaje="Debe seleccionar Orden de Compra" >
                                    <option value="">Seleccione O.C.</option>
                                    <?php for ($i = 0; $i < count($data_orden_compra); $i++) { ?>
                                        <option value="<?php echo $data_orden_compra[$i]['id'] ?>"><?php echo $data_orden_compra[$i]['text'] ?></option>
                                    <?php }
                                    ?>
                                </select>                                 
                                <br>
                                <label for="accion">Acción</label>
                                <select name="accion" id="accion" class="form-control" style="width: 100%"
                                        requerido="true" mensaje="Debe seleccionar una acción a realizar" >
                                    <option value="">Seleccione una acción</option>
                                    <option value="1">Agregar Alumnos</option>
                                    <option value="2">Actualizar Costos de los Alumnos</option>
                                    <option value="3">Eliminar Alumnos</option>
                                </select>

                                <br>
                                <label for="">Ajuntar Documento</label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">

                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Seleccionar Archivo</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="file_orden_compra" id="file_orden_compra"  accesskey=""
                                               requerido="true" mensaje="Debe seleccionar documetno de la rectificacion" 
                                               >
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" id="btn_cleanr_file" data-dismiss="fileinput">
                                        Eliminar
                                    </a>
                                </div>  


                            </div>
                            <div class="col-lg-6"> 
                                <label for="">Subir plantilla</label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">

                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Seleccionar Archivo</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="file_carga_alumnos" id="file_carga_alumnos" accept=".xls,.xlsx" accesskey=""
                                               requerido="true" mensaje="Debe seleccionar Archivo válido" 
                                               >
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" id="btn_cleanr_file" data-dismiss="fileinput">
                                        Eliminar
                                    </a>
                                </div>  
                                <br>


                                <button id="btn_carga"  type="button" class="ladda-button btn btn-block btn-primary" data-style="expand-right"> 
                                    <i class="fa fa-upload"></i> Subir  
                                </button>

                                <input type="hidden" value="<?php echo $data_ficha[0]['id_ficha']; ?>" id="id_ficha" name="id_ficha" hidden> 
                                <a href="<?php echo base_url() . "assets/plantillas/PlantillaInternaAlumnos.xlsx"; ?>" target="_blank" >
                                    <button type="button" id="btn_descarga" class="btn btn-block"><i class="fa fa-download"></i> Descargar Formato (Agregar/Actualizar) </button>
                                </a>
                                <a href="<?php echo base_url() . "assets/plantillas/PlantillaInternaAlumnos_eliminar.xlsx"; ?>" target="_blank" >
                                    <button type="button" id="btn_descarga2" class="btn btn-block"><i class="fa fa-download"></i> Descargar Formato (Eliminar)</button>
                                </a>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-lg-12">        
                                <label class="control-label" for="textarea">Motivo</label>
                                <textarea class="form-control" id="comentario" name="comentario" maxlength="1000" requerido="true" mensaje="Debe ingresar motivo de rectifiación"></textarea>
                                <p id="text-out">0/1000 caracteres restantes</p>
                            </div>

                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-warning" id="btn_agegar_oc"><i class="fa fa-plus-square" style="font-size: 120%;"></i>  Agregar nueva O.C.</button>
                            </div>
                            <div id="oc-container">
                            </div>
                            <!-- <div id="oc-new">
                                <div class="ibox-title">
                                        <h5>Ingresar Nueva O.C. a la Rectificación</h5>
                                        <div class="ibox-tools">
                                        <button class="btn btn-xs btn-danger" type="button" id="btn_eliminar_oc"><i class="fa fa-ban" style="font-size: 120%;"></i>  Eliminar O.C</button>
                                        </div>
                                </div>
                                
                                <div class="col-lg-12" style="margin-top: 10px;">
                                    <label class="col-lg-2 control-label">Razón Social</label>
                                    <div class="col-lg-3">
                                        <select id="razon_social" name="razon_social" data-placeholder="Elija una razón social" class="chosen-select" tabindex="-1"></select>
                                    </div> 
                                    <label class="col-lg-2 control-label">Otic</label>
                                    <div class="col-lg-3">
                                        <select id="otic" name="otic" data-placeholder="Elija una razón social" class="chosen-select" tabindex="-1"></select>
                                    </div>
                                </div>
                                
                                <div class="col-lg-12" style="margin-top: 10px;">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">N° Orden de Compra</label>
                                        <div class="col-lg-3">
                                            <input type="text" class="form-control" id="numero_oc" name="numero_oc">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="margin-top: 10px;">
                                    <div class="form-group">
                                        <label class="col-lg-2 control-label">Observaciones</label>
                                        <div class="col-lg-10">
                                            <textarea class="form-control" name="observaciones" id="observaciones" ></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12" style="margin-top: 10px;">
                                </div>

                            </div>
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="div_carga_empresa"   >
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Informe de carga</h5>
                    <div class="ibox-tools">                        
                        <a class="collapse-link" hidden  id="a_tab_tabla_carga">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content"  > 
                    <table id="tbl_orden_compra_carga_alumno" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>Rut</th>                            
                                <th>Nombre</th>
                                <th title="Apellido Paterno">A. Paterno</th>  
                                <th>CC.CC</th>
                                <th>%FQ</th>
<!--                                <th title="Costo Otic">C. Otic</th>
                                <th title="Costo Empresa">C.Empresa</th>
                                <th title="Valor Agregado">Valor Agregado</th>-->
                                <th>V. Total </th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>                                
                                <th>Rut</th>                            
                                <th>Nombre</th>
                                <th title="Apellido Paterno">A. Paterno</th>  
                                <th>CC.CC</th>
                                <th>%FQ</th>
<!--                                <th title="Costo Otic">C. Otic</th>
                                <th title="Costo Empresa">C.Empresa</th>
                                <th title="Valor Agregado">Valor Agregado</th>-->
                                <th>V. Total </th>
                                <th>Estado</th>
                            </tr>
                        </tfoot>
                    </table>

                    <div class="ibox-tools">
                        <button type="button" id="btn_aprobar_carga" class="btn btn-primary">Aprobar y Enviar</button>
                        <button type="button" id="btn_cancelar" class="btn btn-danger">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
