<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-01-10 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="datepicker">Ingrese un rango de fechas</label>
                                <div id="data_5">
                                    <div class="input-daterange input-group" id="datepicker" style="width: 100%;">
                                        <input type="text" class="input-sm form-control" id="start" name="start" placeholder="Fecha Inicio" value="" readonly=""/>
                                        <span class="input-group-addon">hasta</span>
                                        <input type="text" class="input-sm form-control" id="end" name="end" placeholder="Fecha Fin" value="" readonly="" />
                                    </div>
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="estado">Estado</label><br>
                                <select id="estado" name="estado"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select>
                                <br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="holding">Holding</label><br>
                                <select id="holding" name="holding"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="empresa">Empresa</label><br>
                                <select id="empresa" name="empresa"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="holding">Ejecutivo</label><br>
                                <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="periodo">Periodo</label><br>
                                <select id="periodo" name="periodo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="num_ficha">N° Ficha</label><br>
                                <input id="num_ficha" name="num_ficha"  class="select2_demo_3 form-control" style="width: 100%"/>

                            </div>
                            <div class="col-lg-6">
                                <br>
                                <br>
                                <button type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                                <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Seguimiento Mis Fichas</h5>                   
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">

                        <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th><i class="fa fa-level-down"></i></th>
                                    <th>N° Ficha</th>
                                    <th title="Persona que administra esta Ficha" >Gest.</th>
                                    <th>Ejecutivo</th>
                                    <th title="Tipo venta">T.V</th>
                                    <th title="Modalidad -  Familia">Mo. - Fa.</th>
                                    <th title="Fecha Inicio -  Fecha Termino">F. Inicio. - F. Termino</th>
                                    <th>Descripción</th>
                                    <th>Otic</th>                                    
                                    <th>Empresa</th>
                                    <th>Totales</th>
                                    <th>Estado</th>
                                    <th>Documentos</th>
                                    <th>Alumnos</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th><i class="fa fa-level-up"></i></th>
                                    <th>N° Ficha</th>
                                    <th title="Persona que administra esta Ficha" >Gest.</th>
                                    <th>Ejecutivo</th>
                                    <th title="Tipo venta">T.V</th>
                                    <th title="Modalidad -  Familia">Mo. - Fa.</th>
                                    <th title="Fecha Inicio -  Fecha Termino">F. Inicio. - F. Termino</th>
                                    <th>Descripción</th>
                                    <th>Otic</th>                                    
                                    <th>Empresa</th>
                                    <th>Totales</th>
                                    <th>Estado</th>
                                    <th>Documentos</th>
                                    <th>Alumnos</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiddens Modals para mostrar contactos -->

    <div class="modal inmodal" id="modal_documentos_ficha" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight" id="modal-principal" onclick="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="location.reload();"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Documentos para ficha N°: <h4 class="modal-title" id="modal_id_ficha"></h4></h4>

                </div>
                <div class="modal-body">
                    <div class="btn-group">

                        <button class="btn btn-success" type="button" id="btn_nuevo_contacto" title="Subir Archivos">Subir un nuevo archivo</button> 
                        <br><br><br>
                    </div>
                    <div  id="div_frm_add" style="display: none;"> 

                        <form class="form-horizontal" id="frm_nuevo_contacto_empresa" >


                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="control-label">Archivos Adjuntos</label>
                                    <span class="btn btn-file fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Seleccionar archivos</span>
                                        <!-- The file input field used as target for the file upload widget  -->
                                        <input id="fileupload" type="file" name="fileupload[]" multiple="" accept="application/pdf,application/vnd.ms-excel,image/gif, image/jpeg,image/png,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">

                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <div id="progress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <span id="porsncetaje_carga" class="pull-right">0%</span>
                                </div> 
                            </div>

                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_guardar_contacto"></label>
                                <div class="col-md-8">
                                    <button id="btn_cancelar_contacto" type="button" name="btn_cancelar_contacto" class="btn btn-success">Volver</button>
                                </div>
                            </div>

                        </form>

                    </div>

                    <!-- <div class="table-responsive" id="div_tbl_contacto_empresa">   -->
                    <table id="tbl_documentos_ficha" class="table-md-4 table-bordered" >
                        <thead>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>        
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>                              

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>          
                                <th>Opciones</th>
                            </tr>
                        </tfoot>
                    </table>    





                    <!-- </div> -->



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal" onclick="location.reload();">Cerrar</button>

                </div>
            </div>
        </div>
    </div>

</div>
