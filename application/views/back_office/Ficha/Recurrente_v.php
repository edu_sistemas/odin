<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 id="h_text_title_ficha">Resumen</h5>
            </div>
            <div class="ibox-content">
                <form id="editar" method="post" class="form-horizontal">
                    <input type="hidden" id="id_ficha" name="id_ficha" value="<?php echo $data_ficha[0]['id_ficha']; ?>">
                    <p> Datos de la Ficha</p>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label"><strong>N° de Ficha:</strong></label>
                            <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['num_ficha']; ?>">
                        </div>

                        <div class="col-md-4">
                            <label class=" control-label">N° Orden Compra</label>
                            <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['num_orden_compra']; ?>">
                        </div>

                        <div class="col-md-4">
                            <label class="  control-label"><strong>Estado:</strong> </label>
                            <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['estado']; ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <label class=" control-label">Modalidad</label>
                            <input type="text" class="form-control" readonly="" value="<?php echo $data_ficha[0]['modalidad']; ?>">
                        </div>

                        <div class="col-md-4">
                            <label class="col-md-1 control-label">Curso</label>
                            <input type="text" class="form-control" readonly="" id="curso_descrip" value="<?php echo $data_ficha[0]['descripcion_producto']; ?>">
                        </div>

                        <div class="col-md-4">
                            <label class="col-md-1 control-label">Versión</label>
                            <input type="text" class="form-control" readonly="" id="version" value="<?php echo $data_ficha[0]['version']; ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <label class="  control-label">Empresa</label>
                            <input type="text" class="form-control" readonly="" id="id_empresa" value="<?php echo $data_ficha[0]['rut_empresa'] . " / " . $data_ficha[0]['empresa']; ?>">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <h4>Modoficación de datos recurrentes </h4>


                    <div class="row">
                        <div class="col-lg-6">
                            <label class=" control-label">Fecha Inicio y Término</label>
                            <div class="input-daterange input-group  col-lg-12 ">
                                <input type="text" class="form-control" readonly="" name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD" value="<?php echo $data_ficha[0]['fecha_inicio']; ?>" requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                                <span class="input-group-addon">A</span>
                                <input type="text" class="form-control" readonly="" name="fecha_termino" id="fecha_termino" placeholder="AAAA-MM-DD" value="<?php echo $data_ficha[0]['fecha_fin']; ?>" requerido="true" mensaje="Fecha Término es un campo requerido" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="col-md-6 ">
                                <label class="  control-label">Hora Inicio</label>
                                <div class="input-group clockpicker">
                                    <input type="time" class="form-control" id="txt_hora_inicio" name="txt_hora_inicio" value="<?php echo $data_ficha[0]['hora_inicio']; ?>" requerido="true" mensaje="Ingresar Hora Inicio" />
                                    <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <label class=" control-label">Hora Término</label>
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input type="time" class="form-control" id="txt_hora_termino" name="txt_hora_termino" value="<?php echo $data_ficha[0]['hora_termino']; ?>" requerido="true" mensaje="Ingresar Hora Término">
                                    <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"> </div>
                    <p>Lugar de Ejecución</p>
                    <div class="row">


                        <input type="hidden" class="form-control" id="id_sede" value="<?php echo $data_ficha[0]['id_sede']; ?>">

                        <div class="col-lg-6">
                            <label class="control-label">Sede</label>
                            <select id="sede" name="sede" class="select2_demo_3 form-control" requerido="true" mensaje="Debe seleccionar Sede">
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label class=" control-label">Sala</label>
                            <input type="hidden" class="form-control" id="id_sala" value="<?php echo $data_ficha[0]['id_sala']; ?>">

                            <select id="sala" name="sala" class="select2_demo_3 form-control" requerido="true" mensaje="Debe seleccionar Sala">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="control-label">Dirección o Lugar Ejecución</label>
                            <input id="lugar_ejecucion" requerido="true" mensaje="Ingresar lugar de ejecucción" name="lugar_ejecucion" class="form-control" value="<?php echo $data_ficha[0]['lugar_ejecucion']; ?>" />
                        </div>
                        <input type="hidden" id="id_dias" value="<?php echo $data_ficha[0]['id_dias']; ?>">
                        <div class="col-lg-6">
                            <label class="control-label">Días</label>
                            <label class="checkbox-inline" for="inlineCheckbox1"> <input
                                    type="checkbox" id="lunes" name="lunes" value="1"
                                    title="Lunes"> Lu

                            </label> <label class="checkbox-inline" for="inlineCheckbox2"> <input
                                    type="checkbox" id="martes" name="martes" value="2"
                                    title="Martes"> Ma

                            </label> <label class="checkbox-inline" for="inlineCheckbox3"> <input
                                    type="checkbox" id="miercoles" name="miercoles"
                                    value="3" title="Miércoles"> Mi

                            </label> <label class="checkbox-inline" for="inlineCheckbox4"> <input
                                    type="checkbox" id="jueves" name="jueves" value="4"
                                    title="Jueves"> Ju

                            </label> <label class="checkbox-inline" for="inlineCheckbox5"> <input
                                    type="checkbox" id="viernes" name="viernes"
                                    value="5" title="Viernes"> Vi

                            </label> <label class="checkbox-inline" for="inlineCheckbox6"> <input
                                    type="checkbox" id="sabado" name="sabado" value="6"
                                    title="Sábado"> Sa

                            </label> <label class="checkbox-inline" for="inlineCheckbox7">
                                <input
                                    type="checkbox" id="domingo" name="domingo"
                                    value="7" title="Domingo"> Do

                            </label>
                        </div>
                    </div>
                    <div class="hr-line-dashed"> </div>
                    <p>información Interna</p>
                    <div class="row">

                        <div class="col-md-6">
                            <label class="  control-label" for="textarea">Observaciones</label>
                            <textarea  class="form-control" id="comentario" name="comentario" maxlength="1000" requerido="true" mensaje="Ingresar observación"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
							<p id="text-out">0/1000 caracteres restantes</p>
                        </div>
                        <input type="hidden" value="<?php echo $data_ficha[0]['modalidad']; ?>" id="id_modalidad" name="id_modalidad">
						<!--                        <div class="col-md-6">
													<label class="  control-label" for="textarea">Nueva observación</label>
													<textarea class="form-control" id="comentario_new" requerido="true" mensaje="Ingresar observación" name="comentario_new" maxlength="1000"></textarea>
													<p id="text-out">0/1000 caracteres restantes</p>
												</div>-->

                    </div>

                    <div class="row">



						<?php
						if ($data_ficha[0]['modalidad'] != 'Arriendo') {
							?>

							<div class="col-lg-6">
								<label class="control-label">Relacionar Con Ficha</label>
								<input type="hidden" id="id_relacionar_ficha_con" name="id_relacionar_ficha_con" value="<?php echo $data_ficha[0]['id_ficha_relacion']; ?>">  
								<select id="relacionar_ficha_con" name="relacionar_ficha_con"  class="select2_demo_3 form-control" requerido="false" mensaje="Debe seleccionar Ficha a relacionar"  >
									<option></option>
								</select>
							</div>

							<div class="col-lg-6">

								<br>
								<div class="panel panel-primary">
									<div class="panel-heading">
										Fichas Relacionadas
									</div>
									<div class="panel-body">



										<?php
										$fichas_relacionada = $data_ficha[0]['fichas_relacionada'];

										if ($fichas_relacionada == 'No tiene.') {
											
										} else {

											$fichas_relacionada = $data_ficha[0]['fichas_relacionada'];
											$array_fichas_relacionadas = explode("|", $fichas_relacionada);
											$id_arra_fichas_relacionadas = $array_fichas_relacionadas[0];
											$num_arra_fichas_relacionadas = $array_fichas_relacionadas[1];
											$ids_ficha = explode(",", $id_arra_fichas_relacionadas);
											$nums_ficha = explode(",", $num_arra_fichas_relacionadas);
											for ($i = 0; $i < count($ids_ficha); $i++) {
												if ($data_ficha[0]['id_ficha'] == $ids_ficha[$i]) {
													?> 
													<button type="button" class="btn btn-outline btn-success dim" type="button">  <?php echo $nums_ficha[$i] ?>
														<i class="fa fa-money"></i> 
													</button> 

												<?php } else {
													?>
													<a href="<?php echo $ids_ficha[$i] ?>"  target="_blank">   
														<button type="button" class="btn btn-outline btn-primary dim" type="button">  <?php echo $nums_ficha[$i] ?>
															<i class="fa fa-money"></i> 
														</button>
													</a>
													<?php
												}
											}
										}
										?>


									</div>
								</div> 
							</div>


							<?php
						}
						?>

                    </div>


                    <div class="row">
                        <div class="col-md-2 col-lg-offset-10 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Modificar</button>
                            <button class="btn btn-sm btn-white" id="volver" href="" type="button" name="btn_reset">Volver</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>