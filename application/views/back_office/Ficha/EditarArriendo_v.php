<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-22 [Lusi Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-20 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 id="h_text_title_ficha">Editar Arriendo Sala</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">

                <form class="form-horizontal" id="frm_ficha_registro">
                    <input type="text" hidden="" id="id_ficha" name="id_ficha" value="<?php echo $data_arriendo[0]['id_ficha']; ?>">
                    <p>Edite Solicitud Arriendo de Sala</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <label class="control-label">Holding</label>
                            <select id="holding" name="holding"  class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                        </div>

                        <div class="col-lg-6">
                            <input type="text" hidden="" id="getEmpresaSelected" value="<?php echo $data_arriendo[0]['empresa']; ?>">
                            <label class="control-label">Empresa</label>
                            <select id="empresa" name="empresa"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Empresa"
                                    >
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label for="datepicker">Fecha Inicio y Término</label>
                            <div id="data_5">
                                <div class="input-daterange input-group" id="datepicker" style="width: 100%">
                                    <input type="text" class="input-sm form-control" id="fecha_inicio" name="fecha_inicio" 
                                           value="<?php echo $data_arriendo[0]['fecha_inicio']; ?>"
                                           placeholder="Fecha Inicio" readonly=""/>
                                    <span class="input-group-addon">hasta</span>
                                    <input type="text" class="input-sm form-control" id="fecha_termino" name="fecha_termino" 
                                           value="<?php echo $data_arriendo[0]['fecha_termino']; ?>" 
                                           placeholder="Fecha Fin" readonly="" />
                                </div>
                            </div> 
                            <br>
                        </div>

                        <div class="col-md-3 "> 
                            <label class="control-label">Hora Inicio  </label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input type="text" class="form-control" id="hora_inicio" name="hora_inicio"  value="<?php echo $data_arriendo[0]['hora_inicio']; ?>" 
                                       requerido="true" mensaje="Ingresar Hora Inicio"  
                                       >
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>

                            <span class="help-block"></span>

                        </div>
                        <div class="col-md-3 "> 
                            <label class="control-label">Hora Término</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input type="text" class="form-control" id="hora_termino" name="hora_termino"  value="<?php echo $data_arriendo[0]['hora_termino']; ?>" 
                                       requerido="true" mensaje="Ingresar Hora Término"    
                                       >
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <label>Días</label><br>
                            <input type="hidden" id="id_dias" value="<?php echo $data_arriendo[0]['dias']; ?>">
                            <div class="checkbox checkbox-inline">
                                <label for="lunes">
                                    <input type="checkbox" id="lunes" name="lunes" value="1" title="Lunes">
                                    Lu</label>
                            </div>
                            <div class="checkbox checkbox-success checkbox-inline">
                                <label for="martes">
                                    <input type="checkbox" id="martes" name="martes" value="2" title="Martes">
                                    Ma</label>
                            </div>
                            <div class="checkbox checkbox-inline">
                                <label for="miercoles">
                                    <input type="checkbox" id="miercoles" name="miercoles" value="3" title="Miércoles">
                                    Mi</label>
                            </div>


                            <div class="checkbox checkbox-inline">
                                <label for="jueves">
                                    <input type="checkbox" id="jueves" name="jueves" value="4" title="Jueves">
                                    Ju</label>
                            </div>


                            <div class="checkbox checkbox-inline">
                                <label for="viernes">
                                    <input type="checkbox" id="viernes" name="viernes" value="5" title="Viernes">
                                    Vi</label>
                            </div>


                            <div class="checkbox checkbox-inline">
                                <label for="sabado">
                                    <input type="checkbox" id="sabado" name="sabado" value="6" title="Sábado">
                                    Sa</label>
                            </div>


                            <div class="checkbox checkbox-inline">
                                <label for="domingo">
                                    <input type="checkbox" id="domingo" name="domingo" value="7" title="Domingo">
                                    Do</label>
                            </div>

                        </div>

                        <div class="col-lg-3">
                            <input type="text" hidden="" id="getSedeSelected" value="<?php echo $data_arriendo[0]['sede']; ?>">
                            <label class="control-label">Sede</label>
                            <select id="sede" name="sede"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar sede"
                                    >
                                <option></option>
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <input type="text" hidden="" id="getSalaSelected" value="<?php echo $data_arriendo[0]['sala']; ?>">
                            <label class="control-label">Sala</label>
                            <select id="sala" name="sala"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar sala"
                                    >
                                <option></option>
                            </select>
                        </div>

                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-lg-4">
                            <label for="n_dias">N° de Días</label>
                            <input type="number" class="form-control required"
                                   id="n_dias" 
                                   requerido="false"
                                   name="n_dias"
                                   class="form-control" onkeypress="return solonumero(event)" 
                                   value="<?php echo $data_arriendo[0]['n_dias']; ?>">

                            <label for="valor_dia">Valor por Día</label>
                            <input type="number" class="form-control required"
                                   id="valor_dia" 
                                   requerido="false"
                                   name="valor_dia"
                                   class="form-control" onkeypress="return solonumero(event)" 
                                   value="<?php echo $data_arriendo[0]['valor_dia']; ?>">

                            <label for="total_dia">Total</label>
                            <input type="number" class="form-control required"
                                   id="total_dia" 
                                   requerido="false"
                                   name="total_dia"
                                   class="form-control" readonly="" 
                                   value="<?php echo $data_arriendo[0]['total_dia']; ?>">
                            <input type="text" hidden="" id="getEjecutivoSelected" value="<?php echo $data_arriendo[0]['ejecutivo']; ?>">
                            <label class="control-label">Ejecutivo</label>
                            <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Ejecutivo"
                                    >
                                <option></option>
                            </select>
                        </div>
                        <div class="col-lg-4">
                            <input type="text" hidden="" id="isBreak" value="<?php echo $data_arriendo[0]['break']; ?>">
                            <label class="checkbox-inline"><input type="checkbox" id="break" name="break"  >Break</label><br>
                            <label for="n_alumnos">N° de Alumnos</label>
                            <input type="number" class="form-control required"
                                   id="n_alumnos" 
                                   requerido="false"
                                   name="n_alumnos"
                                   class="form-control" onkeypress="return solonumero(event)" 
                                   value="<?php echo $data_arriendo[0]['n_alumnos']; ?>">

                            <label for="valor_break">Valor Break</label>
                            <input type="number" class="form-control required"
                                   id="valor_break" 
                                   requerido="false"
                                   name="valor_break"
                                   class="form-control" onkeypress="return solonumero(event)" 
                                   value="<?php echo $data_arriendo[0]['valor_break']; ?>">

                            <label for="total_en_break">Total en Break</label>
                            <input type="number" class="form-control required"
                                   id="total_en_break" 
                                   requerido="false"
                                   name="total_en_break"
                                   class="form-control" readonly="" 
                                   value="<?php echo $data_arriendo[0]['total_en_break']; ?>">
                        </div>
                        <div class="col-lg-4">
                            <label for="valorVenta">Valor Venta</label>
                            <input type="number" class="form-control required"
                                   id="valorVenta" 
                                   requerido="false"
                                   name="valorVenta"
                                   class="form-control" readonly="" 
                                   value="<?php echo $data_arriendo[0]['total_venta']; ?>">

                            <label for="iva">Iva</label>
                            <input type="number" class="form-control required"
                                   id="iva" 
                                   requerido="false"
                                   name="iva"
                                   class="form-control" readonly="" 
                                   value="<?php echo $data_arriendo[0]['total_iva']; ?>">

                            <label for="totalVenta">Total Venta</label>
                            <input type="number" class="form-control required"
                                   id="totalVenta" 
                                   requerido="false"
                                   name="totalVenta"
                                   class="form-control" readonly="" 
                                   value="<?php echo $data_arriendo[0]['total_bruto']; ?>"
                                   style="font-weight: bold;">
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-lg-12">        
                            <label class="control-label" for="textarea">Comentario</label>
                            <textarea class="form-control" id="comentario" name="comentario" maxlength="1000"><?php echo $data_arriendo[0]['comentario']; ?></textarea>
                            <p id="text-out">0/1000 caracteres restantes</p>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-lg-12">
                        <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>                        
                        <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
