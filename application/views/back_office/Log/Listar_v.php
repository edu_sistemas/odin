<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">

                    <div class="row">
                        <div class="col-lg-6">
                                <label for="datepicker">Ingrese un rango de fechas</label>
                                <div id="data_5">
                                    <div class="input-daterange input-group" id="datepicker" style="width: 100%">
                                        <input type="text" class="input-sm form-control" name="start" value="" placeholder="Fecha Inicio" readonly=""/>
                                        <span class="input-group-addon">hasta</span>
                                        <input type="text" class="input-sm form-control" name="end" value="" placeholder="Fecha Fin" readonly="" />
                                    </div>
                                </div> 
                                <br>
                        </div>
                        <div class="col-lg-6">
                        <label for="perfil">Perfil</label><br>
                        <select id="perfil" name="perfil"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select>
                                <br>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                        <label for="usuario">Usuario</label>
                            <select id="usuario" name="usuario"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select>
                                <br>
                        </div>
                        <div class="col-lg-6">
                            
                        </div>
                    </div>
                    </form>
                    <div class="row">
                        <div class="col-lg-12">
                        <br>
                            <button type="button" id="btn_buscar_log" name="btn_buscar_log" class="btn btn-success">Buscar</button>
                            <button id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning">Limpiar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Log del Sistema</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">

                        <table id="tbl_log" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Id_Log</th>
                                    <th>Fecha</th> 
                                    <th>Hora</th>
                                    <th>Actividad</th>
                                    <th>Observacion</th>
                                    <th>Usuario</th>
                                    <th>Perfil</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Id_Log</th>
                                    <th>Fecha</th> 
                                    <th>Hora</th>
                                    <th>Actividad</th>
                                    <th>Observacion</th>
                                    <th>Usuario</th>
                                    <th>Perfil</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
