<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2017-01-26 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 id="h_text_title_ficha">Resumen</h5>
                <div class="ibox-tools"></div>
            </div>
            <div class="ibox-content">
                <div class="form-group">
                    <div class="col-md-4">
                        <h4>

                            <table style="width: 70%" cellpadding="30">
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php echo " <strong>- N° de Ficha:</strong>"; ?>
                                        </td>
                                        <td>
                                            <?php echo $data_ficha[0]['num_ficha']; ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php echo " <strong>- Tipo de Venta:</strong> "; ?>   
                                        </td>
                                        <td>
                                            <?php echo $data_ficha[0]['categoria']; ?>    
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php echo " <strong>- Estado:</strong> "; ?>
                                        </td>
                                        <td>
                                            <?php echo $data_ficha[0]['estado']; ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>


                        </h4>
                    </div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4"></div>
                </div>
                <input type="hidden" id="id_ficha" name="id_ficha"
                       value="<?php echo $data_ficha[0]['id_ficha']; ?>">


                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="holding">Holding</label> <input type="text"
                                                                        class="form-control" readonly="" id="holding"
                                                                        value="<?php echo $data_ficha[0]['holding']; ?>">
                        </div>
                        <div class="col-md-6">
                            <label for="id_empresa">Empresa</label> <input type="text"
                                                                           class="form-control" readonly="" id="id_empresa"
                                                                           value="<?php echo $data_ficha[0]['empresa'] . " - " . $data_ficha[0]['direccion_empresa']; ?>">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group">

                        <div class="col-md-6">
                            <label for="datarange">Fecha Inicio y Término</label>
                            <div class="input-datarange input-group" id="datarange">
                                <input type="text" readonly="" class="form-control"
                                       name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"
                                       value="<?php echo $data_ficha[0]['fecha_inicio']; ?>"
                                       requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                                <span class="input-group-addon">A</span> <input type="text"
                                                                                class="form-control" name="fecha_termino" id="fecha_termino"
                                                                                placeholder="AAAA-MM-DD"
                                                                                value="<?php echo $data_ficha[0]['fecha_fin']; ?>" readonly=""
                                                                                requerido="true" mensaje="Fecha Término es un campo requerido" />
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="txt_hora_inicio">Hora Inicio</label>
                            <div class="input-group clockpicker">
                                <input type="time" class="form-control" id="txt_hora_inicio"
                                       name="txt_hora_inicio"
                                       value="<?php echo $data_ficha[0]['hora_inicio']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Inicio" readonly="" />
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label for="txt_hora_termino">Hora Término</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input type="time" class="form-control" id="txt_hora_termino"
                                       name="txt_hora_termino"
                                       value="<?php echo $data_ficha[0]['hora_termino']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Término" readonly="">
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="id_ejecutivo">Ejecutivo Comercial</label> <input
                                type="text" class="form-control" readonly="" id="id_ejecutivo"
                                value="<?php echo $data_ficha[0]['ejecutivo']; ?>">
                        </div>

                        <div class="col-md-3">
                            <label for="id_sede">Sede</label> <input type="text"
                                                                     class="form-control" readonly="" id="id_sede"
                                                                     value="<?php echo $data_ficha[0]['sede']; ?>">
                        </div>

                        <div class="col-md-3">
                            <label for="id_sala">Sala</label> <input type="text"
                                                                     class="form-control" readonly="" id="id_sala"
                                                                     value="<?php echo $data_ficha[0]['sala']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label for="semana">Días</label>
                    <div class="form-group" id="semana">
                        <input type="hidden" id="id_dias"
                               value="<?php echo $data_ficha[0]['id_dias']; ?>">
                        <div class="col-md-6">
                            <label class="checkbox-inline" for="inlineCheckbox1"> <input
                                    type="checkbox" readonly="" id="lunes" name="lunes" value="1"
                                    title="Lunes"> Lu
                            </label> <label class="checkbox-inline" for="inlineCheckbox2"> <input
                                    type="checkbox" readonly="" id="martes" name="martes" value="2"
                                    title="Martes"> Ma
                            </label> <label class="checkbox-inline" for="inlineCheckbox3"> <input
                                    type="checkbox" readonly="" id="miercoles" name="miercoles"
                                    value="3" title="Miércoles"> Mi
                            </label> <label class="checkbox-inline" for="inlineCheckbox4"> <input
                                    type="checkbox" readonly="" id="jueves" name="jueves" value="4"
                                    title="Jueves"> Ju
                            </label> <label class="checkbox-inline" for="inlineCheckbox5"> <input
                                    type="checkbox" readonly="" id="viernes" name="viernes"
                                    value="5" title="Viernes"> Vi
                            </label> <label class="checkbox-inline" for="inlineCheckbox6"> <input
                                    type="checkbox" readonly="" id="sabado" name="sabado" value="6"
                                    title="Sábado"> Sa
                            </label> <label class="checkbox-inline" for="inlineCheckbox7"> <input
                                    type="checkbox" readonly="" id="domingo" name="domingo"
                                    value="7" title="Domingo"> Do
                            </label>


                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-4">
                        <p>
                            <b>N° de Días: </b><?php echo $data_ficha[0]['n_dias']; ?></p>
                        <p>
                            <b>Valor por Día: $ </b><?php echo number_format(floatval($data_ficha[0]['valor_dia']), 0, ',', '.'); ?></p>
                        <p>
                            <b>Total Dias: $ </b><?php echo number_format(floatval($data_ficha[0]['total_dia']), 0, ',', '.'); ?></p>
                    </div>
                    <div class="col-lg-4">
                        <p>
                            <b>Break: </b><?php
                            if ($data_ficha[0]['break'] == "1") {
                                echo 'Si';
                            } else {
                                echo 'No';
                            }
                            ?></p>
                        <p>
                            <b>N° de Alumnos: </b><?php echo $data_ficha[0]['n_alumnos']; ?></p>
                        <p>
                            <b>Valor Break por Alumno: $ </b><?php echo number_format(floatval($data_ficha[0]['valor_break']), 0, ',', '.'); ?></p>
                        <p>
                            <b>Total en Break: $ </b><?php echo number_format(floatval($data_ficha[0]['total_en_break']), 0, ',', '.'); ?></p>
                    </div>
                    <div class="col-lg-4">
                        <p>
                            <b>Valor Venta (neto): $ </b><?php echo number_format(floatval($data_ficha[0]['total_venta']), 0, ',', '.'); ?></p>
                        <p>
                            <b>IVA: $ </b><?php echo number_format(floatval($data_ficha[0]['total_iva']), 0, ',', '.'); ?></p>
                        <br>
                        <p style="font-size: 120%">
                            <b>TOTAL: $ <?php echo number_format(floatval($data_ficha[0]['total_bruto']), 0, ',', '.'); ?></b>
                        </p>
                    </div>


                </div>

                <div class="row">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label for="comentario">Observaciones</label>
                            <textarea readonly="" class="form-control" id="comentario"
                                      name="comentario" maxlength="200"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                            <p id="text-out">0/200 caracteres restantes</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="pull-right">
                        <?php if (intval($data_ficha[0]['id_estado']) == 10 || intval($data_ficha[0]['id_estado']) == 30 || intval($data_ficha[0]['id_estado']) == 50) { ?>
                            <button class="btn btn-warning" id="btn_enviar_ficha">Enviar</button> 
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Documentos Adjuntos</h5>
                <div class="ibox-tools">
                    <a class="collapse-link" hidden="" id="a_tab_tabla_carga"> <i
                            class="fa fa-chevron-down"></i>
                    </a>

                </div>
            </div>
            <div class="ibox-content">
                <table id="tbl_orden_compra_documetnos"
                       class="table table-striped table-bordered table-hover dataTables-example">
                    <thead>
                        <tr>
                            <th>Tipo Documento</th>
                            <th>Documento</th>

                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Tipo Documento</th>
                            <th>Documento</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

<div class="row ibox-content">

    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
    <div class="col-md-3">
        <?php echo $data_ficha[0]['id_estado']; ?>
    </div>
    <div class="col-md-3"></div>
</div>

