<?php
/**
 * Consulta_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:	2017-01-06 [David De Filippi] <dfilippi@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <form role="form" class="form-inline" id="frm" name="frm">

                        <!-- Date Picker -->

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="datepicker">Ingrese un rango de fechas</label>
                                <div id="data_5">
                                    <div class="input-daterange input-group" id="datepicker" style="width: 100%;">
                                        <input type="text" class="input-sm form-control" id="start" name="start" placeholder="Fecha Inicio" value="" readonly=""/>
                                        <span class="input-group-addon">hasta</span>
                                        <input type="text" class="input-sm form-control" id="end" name="end" placeholder="Fecha Fin" value="" readonly="" />
                                    </div>
                                </div>
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="estado">Estado</label><br>
                                <select id="estado" name="estado"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select>
                                <br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="holding">Holding</label><br>
                                <select id="holding" name="holding"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="empresa">Empresa</label><br>
                                <select id="empresa" name="empresa"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="holding">Ejecutivo</label><br>
                                <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="periodo">Periodo</label><br>
                                <select id="periodo" name="periodo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-6">
                                <label for="num_ficha">N° Ficha</label><br>
                                <input id="num_ficha" name="num_ficha"  class="select2_demo_3 form-control" style="width: 100%"/>

                            </div>
                            <div class="col-lg-6">
                                <br>
                                <br>
                                <button type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                                <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Fichas</h5>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">


                    <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>N° Ficha</th>  
                                <th>Ejecutivo</th>
                                <th title="Tipo venta">T.V</th>
                                <th title="Modalidad -  Familia">Mo. - Fa.</th>
                                <th title="Fecha Inicio - Fecha Termino">F. Inicio. - F. Termino</th>
                                <th>Descripción</th>
                                <th>Otic</th>                                    
                                <th>Empresa</th>
                                <th>Estado</th>
                                <th>Totales</th>
                                <th>Documentos</th>
                                <th>Alumnos</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>N° Ficha</th>  
                                <th>Ejecutivo</th>
                                <th title="Tipo venta">T.V</th>
                                <th title="Modalidad - Familia">Mo. - Fa.</th>
                                <th title="Fecha Inicio - Fecha Termino">F. Inicio. - F. Termino</th>
                                <th>Descripción</th>
                                <th>Otic</th>                                    
                                <th>Empresa</th>
                                <th>Estado</th>
                                <th>Totales</th>
                                <th>Documentos</th>
                                <th>Alumnos</th>
                                <th>Acciones</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Hiddens Modals para mostrar contactos -->

<div class="modal inmodal" id="modal_documentos_ficha" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight" id="modal-principal">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                <i class="fa fa-info-circle modal-icon"></i>
                <h2>Ingrese motivo de rechazo para la ficha N°: <h4 class="modal-title" id="modal_id_ficha"></h4></h2>

<!--                    <small class="font-bold" id="" >Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->

            </div>
            <div class="modal-body">

                <form id="frm_rechazo" class="form-horizontal">

                    <input type="text" id="id_ficha" name="id_ficha" hidden>

                    <div class="form-group">
                        <textarea name="motivo_rechazo" id="motivo_rechazo" class="form-control" cols="30" rows="10" requerido="true" mensaje ="Debe Ingresar el motivo del rechazo"></textarea>
                    </div>

                    <div class="form-group">

                    </div>

                </form>
                <button id="btn_aceptar" class="btn btn-warning">Aceptar</button>
                <button class="btn btn-white" data-dismiss="modal">Cancelar</button>

            </div>
        </div>
    </div>
</div>
</div>

</div>
