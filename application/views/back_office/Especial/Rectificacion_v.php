<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-02 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>

<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Rectificación Alumnos O.C.</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content"> 
                    
                    <form  name="frm_carga_orden_compra" id="frm_carga_orden_compra">
                        <div class="row">
                            <div class="col-lg-6"> 
                                <label for="cbx_orden_compra">Orden Compra</label>
                                <select id="cbx_orden_compra" name="cbx_orden_compra" class="form-control" requerido="true" mensaje="Debe seleccionar Orden de Compra" >
                                    <option value="">Seleccione O.C.</option>
                                    <?php for ($i = 0; $i < count($data_orden_compra); $i++) { ?>
                                        <option value="<?php echo $data_orden_compra[$i]['id'] ?>"><?php echo $data_orden_compra[$i]['text'] ?></option>
                                    <?php }
                                    ?>
                                </select>                                 
                                <br>
                                <label for="accion">Acción</label>
                                <select name="accion" id="accion" class="form-control" style="width: 100%"
                                        requerido="true" mensaje="Debe seleccionar una acción a realizar" >
                                    <option value="">Seleccione una acción</option>
                                    <option value="1">Agregar Alumnos</option>
                                    <option value="2">Actualizar Alumnos</option>
                                    <option value="3">Eliminar Alumnos</option>
                                </select>

                                <br>
                                <label for="">Ajuntar Documento</label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">

                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Seleccionar Archivo</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="file_orden_compra" id="file_orden_compra" accept=".xls,.xlsx" accesskey=""
                                               requerido="true" mensaje="Debe seleccionar documetno de la rectificacion" 
                                               >
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" id="btn_cleanr_file" data-dismiss="fileinput">
                                        Eliminar
                                    </a>
                                </div>  


                            </div>
                            <div class="col-lg-6"> 
                                <label for="">Subir plantilla</label>
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">

                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Seleccionar Archivo</span>
                                        <span class="fileinput-exists">Cambiar</span>
                                        <input type="file" name="file_carga_alumnos" id="file_carga_alumnos" accept=".xls,.xlsx" accesskey=""
                                               requerido="true" mensaje="Debe seleccionar Archivo válido" 
                                               >
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" id="btn_cleanr_file" data-dismiss="fileinput">
                                        Eliminar
                                    </a>
                                </div>  
                                <br>


                                <button id="btn_carga"  type="button" class="ladda-button btn btn-block btn-primary" data-style="expand-right"> 
                                    <i class="fa fa-upload"></i> Subir  
                                </button>

                                <input type="hidden" value="<?php echo $data_ficha[0]['id_ficha']; ?>" id="id_ficha" name="id_ficha" hidden> 
                                <a href="<?php echo base_url() . "assets/plantillas/PlantillaInternaAlumnos.xlsx"; ?>" target="_blank" >
                                    <button type="button" id="btn_descarga" class="btn btn-block"><i class="fa fa-download"></i> Descargar Formato (Agregar/Actualizar) </button>
                                </a>
                                <a href="<?php echo base_url() . "assets/plantillas/PlantillaInternaAlumnos_eliminar.xlsx"; ?>" target="_blank" >
                                    <button type="button" id="btn_descarga2" class="btn btn-block"><i class="fa fa-download"></i> Descargar Formato (Eliminar)</button>
                                </a>
                            </div>

                        </div>


                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="div_carga_empresa"   >
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Informe de carga</h5>

                    <div class="ibox-tools">                        
                        <a class="collapse-link" hidden  id="a_tab_tabla_carga">
                            <i class="fa fa-chevron-down"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content"  > 
                    <table id="tbl_orden_compra_carga_alumno" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>Rut</th>                            
                                <th>Nombre</th>
                                <th>Apellido Paterno</th>  
                                <th>%FQ</th>
                                <th>Costo Otic</th>
                                <th>Costo Empresa</th>
                                <th>CC.CC</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>                                
                                <th>Rut</th>                            
                                <th>Nombre</th>
                                <th>Apellido Paterno</th>  
                                <th>%FQ</th>
                                <th>Costo Otic</th>
                                <th>Costo Empresa</th>
                                <th>CC.CC</th>
                                <th>Estado</th>
                            </tr>
                        </tfoot>
                    </table>

                    <div class="ibox-tools">
                        <button type="button" id="btn_aprobar_carga" class="btn btn-primary">Aprobar Registros</button>
                        <button type="button" id="btn_cancelar" class="btn btn-danger">Cancelar</button>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
