<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 id="h_text_title_ficha">Resumen</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form method="" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-1 control-label"></label>
                        <div class="col-md-11"><h3>
                                <?php
                                echo " <strong>N° de Ficha:</strong>" . $data_ficha[0]['num_ficha'] . "&nbsp;&nbsp;&nbsp;&nbsp;";
                                echo " <strong>Tipo de Venta:</strong> " . $data_ficha[0]['categoria'] . "&nbsp;&nbsp;&nbsp;&nbsp;";
                                echo " <strong>Estado:</strong> " . $data_ficha[0]['estado'] . "&nbsp;&nbsp;&nbsp;&nbsp;";
                                ?>
                            </h3>
                        </div>
                    </div>
                    <input type="hidden" id="id_ficha" name="id_ficha" value="<?php echo $data_ficha[0]['id_ficha']; ?>">
                    <div class="row" >
                        <div class="form-group">
                            <label class="col-md-1 control-label">Modalidad</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control"  readonly="" id="modalidad" value="<?php echo $data_ficha[0]['modalidad']; ?>">
                            </div>
                            <label class="col-md-1 control-label">Curso</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control"  readonly="" id="curso_descrip" value="<?php echo $data_ficha[0]['descripcion_producto']; ?>">
                            </div>
                        </div>              
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-1 control-label">Holding</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control"  readonly="" id="holding" value="<?php echo $data_ficha[0]['holding']; ?>"> 
                            </div>
                            <label class="col-md-1 control-label">Empresa</label>
                            <div class="col-md-5">
                                <input type="text"  class="form-control" readonly="" id="id_empresa" value="<?php echo $data_ficha[0]['empresa'] . " " . $data_ficha[0]['direccion_empresa']; ?>">    
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-md-1 control-label">Otic</label>
                            <div class="col-md-5">
                                <input type="text"  class="form-control"  readonly="" id="id_otic" value="<?php echo $data_ficha[0]['otic']; ?>">
                            </div>                    
                            <label class="col-md-1 control-label">Código Sence</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" readonly="" id="id_cod_sence" value="<?php echo $data_ficha[0]['codigo_sence']; ?>">
                            </div>
                        </div>                    
                    </div>

                    <div class="row"> 
                        <div class="form-group">
                            <label class="col-md-1 control-label">Fecha Inicio y Término</label>
                            <div class="col-md-3">
                                <div class="input-datarange input-group">
                                    <input type="text"  readonly=""  class="form-control" name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"  
                                           value="<?php echo $data_ficha[0]['fecha_inicio']; ?>"
                                           requerido="true" mensaje="Fecha Inicio es un campo requerido"/>
                                    <span class="input-group-addon">A</span>
                                    <input type="text" class="form-control" name="fecha_termino" id="fecha_termino" placeholder="AAAA-MM-DD"
                                           value="<?php echo $data_ficha[0]['fecha_fin']; ?>"  readonly="" 
                                           requerido="true" mensaje="Fecha Término es un campo requerido"/>
                                </div>
                            </div>
                            <label class="col-md-1 control-label">Hora Inicio</label>
                            <div class="col-md-3">
                                <div class="input-group clockpicker">
                                    <input type="time" class="form-control" id="txt_hora_inicio" name="txt_hora_inicio"  
                                           value="<?php echo $data_ficha[0]['hora_inicio']; ?>" 
                                           requerido="true" mensaje="Ingresar Hora Inicio"    readonly=""/>
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                            <label class="col-md-1 control-label">Hora Término</label>
                            <div class="col-md-3">
                                <div class="input-group clockpicker" data-autoclose="true">
                                    <input type="time" class="form-control" id="txt_hora_termino" name="txt_hora_termino"  value="<?php echo $data_ficha[0]['hora_termino']; ?>" 
                                           requerido="true" mensaje="Ingresar Hora Término"   
                                           readonly="" 
                                           >
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                        </div>                    

                    </div> 

                    <div class="row">
                        <div class="form-group">                        
                            <label class="col-md-1 control-label">Ejecutivo Comercial</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" readonly=""  id="id_ejecutivo" value="<?php echo $data_ficha[0]['ejecutivo']; ?>">    
                            </div>
                            <label class="col-md-1 control-label">Sede</label>
                            <div class="col-md-5">                            
                                <input type="text" class="form-control" readonly="" id="id_sede" value="<?php echo $data_ficha[0]['sede']; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="form-group">                   
                            <label class="col-md-1 control-label">Días</label>
                            <input type="hidden" id="id_dias" value="<?php echo $data_ficha[0]['id_dias']; ?>">
                            <div class="col-md-5"> 
                                <label class="checkbox-inline" for="inlineCheckbox1">
                                    <input type="checkbox"  readonly=""  id="lunes" name="lunes" value="1" title="Lunes">
                                    Lu
                                </label>

                                <label class="checkbox-inline" for="inlineCheckbox2">
                                    <input type="checkbox"  readonly=""  id="martes" name="martes" value="2" title="Martes">
                                    Ma
                                </label>

                                <label class="checkbox-inline" for="inlineCheckbox3">
                                    <input type="checkbox"  readonly=""  id="miercoles" name="miercoles" value="3" title="Miércoles">
                                    Mi
                                </label>



                                <label class="checkbox-inline" for="inlineCheckbox4">
                                    <input type="checkbox"  readonly=""  id="jueves" name="jueves" value="4" title="Jueves">
                                    Ju
                                </label>
                                <label  class="checkbox-inline" for="inlineCheckbox5">
                                    <input type="checkbox"  readonly=""  id="viernes" name="viernes" value="5" title="Viernes">
                                    Vi
                                </label>   
                                <label class="checkbox-inline" for="inlineCheckbox6">
                                    <input type="checkbox"  readonly=""  id="sabado" name="sabado" value="6" title="Sábado">
                                    Sa
                                </label>
                                <label class="checkbox-inline" for="inlineCheckbox7">
                                    <input type="checkbox"  readonly=""  id="domingo" name="domingo" value="7" title="Domingo">
                                    Do
                                </label>


                            </div>                    
                            <label class="col-md-1 control-label">Sala</label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" readonly=""  id="id_sala" value="<?php echo $data_ficha[0]['sala']; ?>">
                            </div>
                        </div>    
                    </div>

                    <div class="row"> 
                        <div class="form-group">
                            <label class="col-md-1 control-label" for="textarea">Observaciones</label>
                            <div class="col-md-11">                         
                                <textarea  readonly=""  class="form-control" id="comentario" name="comentario" maxlength="200"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                                <p id="text-out">0/200 caracteres restantes</p>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12 pull-right">
                        <?php if (intval($data_ficha[0]['id_estado']) == 10 || intval($data_ficha[0]['id_estado']) == 30 || intval($data_ficha[0]['id_estado']) == 50) { ?>
                            <button class='btn btn-warning' id='btn_enviar_ficha'>Enviar</button>  
                        <?php }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">


                <div class="col-md-6"> 

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cbx_orden_compra">Orden Compra</label>
                        <div class="col-md-4">
                            <select id="cbx_orden_compra" name="cbx_orden_compra" class="form-control"    requerido="true" mensaje="Debe seleccionar Orden de Compra" >
                                <option value="">Todas O.C.</option>
                                <?php for ($i = 0; $i < count($data_orden_compra); $i++) { ?>
                                    <option value="<?php echo $data_orden_compra[$i]['id'] ?>"><?php echo $data_orden_compra[$i]['text'] ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                </div> 
                Seleccione Orden de compra para ver infomación adicional
            </div>
        </div>
    </div>
</div>


<div class="row"  >
    <div class="col-lg-12">
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Alumno</h5>
                    <div class="ibox-tools">                        
                        <a class="collapse-link" hidden  id="a_tab_tabla_carga">
                            <i class="fa fa-chevron-down"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content"  > 
                    <table id="tbl_orden_compra_carga_alumno" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>Rut</th>                            
                                <th>Alumno</th>                             
                                <th>%FQ</th>
                                <th>Costo Otic</th>
                                <th>Costo Empresa</th>
                                <th>CC.CC</th>
                                <th>Total</th>
                                <th>O.C</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>                                
                                <th>Rut</th>                            
                                <th>Alumno</th>                             
                                <th>%FQ</th>
                                <th>Costo Otic</th>
                                <th>Costo Empresa</th>
                                <th>CC.CC</th>
                                <th>Total</th>
                                <th>O.C</th>
                            </tr>
                        </tfoot>
                    </table>
					<table style="width: 30%;">
						<tr>
							<td><label for="valor_final">Total Becados: </label></td>
							<td id="total_becados"></td>
						</tr>
						<tr>
							<td><label for="valor_final">Valor Final: </label></td>
							<td id="valor_final"></td>
						</tr>
					</table>
                </div>
            </div>

        </div>
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Documentos Adjuntos</h5>
                    <div class="ibox-tools">                        
                        <a class="collapse-link" hidden  id="a_tab_tabla_carga">
                            <i class="fa fa-chevron-down"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content"  > 
                    <table id="tbl_orden_compra_documetnos" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>Tipo Documento</th>                            
                                <th>Documento</th>                             

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>                                
                                <th>Tipo Documento</th>                            
                                <th>Documento</th>    
                            </tr>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>

</div>

