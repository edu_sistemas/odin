<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-4">
            <div class="ibox float-e-marfins" id="div_general" name="div_general">
                <div class="ibox-title col-md-12" style="background-color: #1ab394; border-top-left-radius: 10px; border-top-right-radius: 10px;">                    
                    <div class="col-md-4">
                        <h2 class="text-center pull-left" style="color:#fff; text-transform: uppercase;">Fichas</h2>                         
                    </div>
                    <div class="col-md-8">
                        <form id="frmAno" class="form-horizontal ">
                        <div class="form-group">
                            <div class="col-md-9 col-xs-5">
                                <input type="text" name="txtano" id="txtano" class="form-control" requerido="true" value="" mensaje="Campo requerido." placeholder="Año de Inicio" min="1900" onkeypress="return solonumero(event)"/>
                            </div>
                            <div class="col-md-2 col-xs-2">
                                <button type="button" class="btn btn-default" id="btnAno"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>                        
                    </div>
                    
                </div>
                <div class="ibox-content" style="border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;height: 396px; overflow: auto;">
                    <div class="row">
                        <div class="col-md-12"> 
                            <div>
                                <div>                                     
                                    <table id="tblFichas" class="table table-hover" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th width="10%"><input type="checkbox" id="chkTodos" /></th>
                                                <th>Ficha</th>
                                                <th>Rectificada</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            for ($i = 0; $i < count($fichas_aprobadas); $i++) {
                                                echo "<tr bgcolor='" . $fichas_aprobadas[$i]["color"] . "'>"
                                                . "<td>"
                                                . "<input type=\"checkbox\" name=\"chkidficha\" value=\"" . $fichas_aprobadas[$i]["id_ficha"] . "\" />"
                                                . "</td>"
                                                . "<td>"
                                                . $fichas_aprobadas[$i]["nombre_curso"] . " (" . $fichas_aprobadas[$i]["num_ficha"] . ")"
                                                . "</td>"
                                                . "<td>"
                                                . $fichas_aprobadas[$i]["rectificada"]
                                                . "</td>"
                                                . "<td>"
                                                . "<button type=\"button\" onclick=\"seleccionarFicha(" . $fichas_aprobadas[$i]["id_ficha"] . ",".$fichas_aprobadas[$i]["num_ficha"].")\" class=\"btn btn-info btn-xs\"><i class=\"fa fa-eye\"></i></button>"
                                                . "</td>"
                                                . "</tr>";
                                            }
                                            ?>
                                        </tbody>
                                    </table>  
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        </div>  
                    </div> 
                </div>  
                <div class="ibox-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success btn-block" id="btnMdlEnviarMoodle">Inscribir en Moodle</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox float-e-marfins">

                <div class="ibox-title" style="background-color: #1ab394; border-top-left-radius: 10px; border-top-right-radius: 10px;">                    
                    <h2 class="text-center" style="color:#fff; text-transform: uppercase;"><b>Información Ficha</h2> 
                </div>

                <div class="ibox-content" style="border-bottom-right-radius: 10px; border-bottom-left-radius: 10px; min-height: 396px;">
                    <!--  style="background-color: #ececec;" -->
                    <div class="row" id="detalle_ficha" style="display: none;">
                        <div class="col-lg-5 animated fadeInLeft">
                            <b>Ficha: </b><label id="lblFicha"></label>
                            <br>
                            <b>Curso: </b><label id="lblCurso"></label>
                            <br>
                            <b>Fecha Aprobación: </b><label id="lblFechaAprobacion"></label>
                            <br><br>
                            <div id="info_excepcion" class="animated fadeInRight">
                                <div class="alert alert-warning">
                                    En esta ficha se registraron errores durante el proceso de Moodle,
                                    click <button type="button" class="btn btn-warning btn-xs" data-target="#mdlErrores" data-toggle="modal">AQUÍ</button> para ver detalle.
                                </div>
                            </div>
                            <div class="row" id="icon_sombrero">
                                <div class="col-md-12 text-center">
                                    <i class="fa fa-graduation-cap " style="font-size: 180px;color: #f5f5f5; margin-top: 50px;"></i>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-7 animated fadeInRight">
                            <h2>Participantes</h2>						
                            <div class="ibox-content" style="height: 280px; overflow: auto;" >
                                <div class="table-responsive">
                                    <table class="table" id="tblAlumnos">
                                        <thead>
                                            <tr>
                                                <th width="20%">ID</th>
                                                <th width="20%">Rut</th>
                                                <th>Nombre</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
</div>	

<div class="modal inmodal" id="mdlErrores" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-exclamation-circle modal-icon"></i>
                <h4 class="modal-title">Errores Reportados</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="ibox-content" style="height: 500px; overflow: auto;" >
                    <div class="table-responsive">
                        <table class="table" id="tblErrores">
                            <thead>
                                <tr>
                                    <th>Registro</th>
                                    <th>Proceso</th>
                                    <th>Descripción</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal inmodal" id="mdlEnrolados" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-exclamation-circle modal-icon"></i>
                <h4 class="modal-title">Alumnos Existentes</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="ibox-content" style="height: 500px; overflow: auto;" >
                    <div class="table-responsive">
                        <table class="table" id="tblEnrolados">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre de Usuario</th>
                                    <th>Nombre Completo</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
            </form>
        </div>
    </div>
</div>