<?php
/**
 * Crear_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-08-03 [Carlos Aravena] <caravena@edutecno.com>
 * Fecha creacion:  2016-08-03 [Carlos Aravena] <caravena@edutecno.com>
 */
?>
<!-- Advanced login -->
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <img width="150" src="<?php echo base_url('assets/amelia/images/odin_logo.png') ?>" alt="">
        </div>

        <h3>Bienvenido a ODIN</h3>
        <p><small class="display-block">Ingresa tus datos de acceso</small></p>
        <?php
        $hidden = $arr_hidden_config;
        $attributes_submit = array('name' => 'submit', 'value' => $arr_enlaces['label_login'], 'class' => 'btn btn-primary btn-sm btn-block');
        // $attributes_reset   = array('name' => 'reset', 'value' => 'Limpiar', 'class' => 'btn btn-default');
        $attributes_form = array('class' => '', 'role' => 'form');
        echo form_open($arr_enlaces['login'], $attributes_form, $hidden);
        ?>   

    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="text-center">
                <span class="label label-danger label-block"><?php echo $this->session->flashdata('item'); ?></span>
            </p>
            <p class="text-center">
                <span class="label label-danger label-block"><?php echo $this->session->flashdata('item2'); ?></span>
            </p>
            <?php
            foreach ($arr_form_script as $key => $value) {
                echo $value;
            }
            ?>
        </div>

        <div class="col-md-12" align="center">
            <?php echo form_submit($attributes_submit); ?>
        </div>

    </div>
    <p class="m-t"> <small>EDUTECNO Departamento TI Sistemas &copy; 2016</small> </p>

</div>
<!-- /advanced login -->
