<?php
/**
 * Agregar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2017-20-01 [David De Filippi] <dfilippi@edutecno.com>
 * GMaps API Key: AIzaSyCXFAmo7nctNNqsCNWb1bUBx6F2HrLBUMQ
 */
?>
<head>

<style>
html, body {
	height: 100%;
	margin: 0;
	padding: 0;
}

#map {
	width: 100%;
	height: 250px;
}

.controls {
	margin-top: 10px;
	border: 1px solid transparent;
	border-radius: 2px 0 0 2px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	height: 32px;
	outline: none;
	box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
	background-color: #fff;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	margin-left: 12px;
	padding: 0 11px 0 13px;
	text-overflow: ellipsis;
	width: 300px;
}

#pac-input:focus {
	border-color: #4d90fe;
}

.pac-container {
	font-family: Roboto;
}

#type-selector {
	color: #fff;
	background-color: #4d90fe;
	padding: 5px 11px 0px 11px;
}

#type-selector label {
	font-family: Roboto;
	font-size: 13px;
	font-weight: 300;
}

#target {
	width: 345px;
}

/*select2*/
.select2-container {
	z-index: 2060;
}
</style>
</head>
<input type="text" id="id_colaborador"
	value="<?php echo $data_colaborador[0]['id_colaborador']; ?>" hidden>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h2 id="nombre_colaborador"><?php echo $data_colaborador[0]['nombre_completo']; ?></h2>
				<div class="ibox-tools"></div>
			</div>
			<div class="ibox-content">

				<!-- CONTENIDO ACA -->
				<h3>
					Datos Básicos
					<div class="dropdown">
						<a id="datos-basicos" data-toggle="dropdown" aria-expanded="true" href="#"><i
							class="fa fa-edit"></i> Ver más </a>

<!-- 						<ul class="dropdown-menu"> -->
<!-- 							<li><a href="#">Editar Datos Básicos</a></li> -->
<!-- 						</ul> -->
					</div>
				</h3>



				<hr>

				<div class="row">

					<div class="col-lg-4">
						<label for="rut">Rut</label>
						<p id="rut"><?php echo $data_colaborador[0]['rut_completo']; ?></p>
					</div>

					<div class="col-lg-4">
						<label for="rut">Género</label>
						<p id="genero"><?php echo $data_colaborador[0]['genero']; ?></p>
					</div>

					<div class="col-lg-4">
						<label for="rut">Estado Civil</label>
						<p id="estado_civil"><?php echo $data_colaborador[0]['estado_civil']; ?></p>
					</div>

				</div>

				<div class="row">

					<div class="col-lg-4">
						<label for="rut">Nacionalidad</label>
						<p id="nacionalidad"><?php echo $data_colaborador[0]['nacionalidad']; ?></p>
					</div>

					<div class="col-lg-4"></div>

					<div class="col-lg-4"></div>

				</div>
				<hr>
				<h3>
					Datos de Contacto
					<div class="dropdown">
						<a id="datos-de-contacto" data-toggle="dropdown"
							aria-expanded="true" href="#"><i class="fa fa-edit"></i> Ver más
						</a>
						<!-- 						<ul class="dropdown-menu"> -->
						<!-- 							<li><a href="#">Editar Teléfonos</a></li> -->
						<!-- 							<li><a id="EditDireccion" href="#">Editar Domicilio</a></li> -->
						<!-- 							<li><a href="#">Editar Email</a></li> -->
						<!-- 						</ul> -->
					</div>
				</h3>

				<hr>

				<div class="row">

					<div class="col-lg-4">
						<label for="rut">Teléfonos</label>
						<p id="direccion">
							<a
								href="tel:<?php echo $data_colaborador[0]['numero_telefono']; ?>"><?php echo $data_colaborador[0]['numero_telefono']; ?><?php echo $data_colaborador[0]['tipo_telefono']; ?></a>
						</p>
					</div>

					<div class="col-lg-4">
						<label for="rut">Domicilio</label>
						<p id="direccion">
							<a href="<?php echo $data_colaborador[0]['mapa']; ?>"
								target="_blank"><?php echo $data_colaborador[0]['direccion']; ?></a>
						</p>
					</div>

					<div class="col-lg-4">
						<label for="rut">Email</label>
						<p id="correo-e">
							<a
								href="mailto:<?php echo $data_colaborador[0]['correo_electronico']; ?>"><?php echo $data_colaborador[0]['correo_electronico']; ?><?php echo $data_colaborador[0]['tipo_correo']; ?></a>
						</p>
					</div>

				</div>
				<hr>
				<h3>
					Datos de Contratación
					<div class="dropdown">
						<a id="datos-de-contratacion" data-toggle="dropdown"
							aria-expanded="true" href="#"><i class="fa fa-edit"></i>Ver más</a>
						<!-- 						<ul class="dropdown-menu"> -->
						<!-- 							<li><a href="#">Editar Teléfonos</a></li> -->
						<!-- 							<li><a id="EditDireccion" href="#">Editar Domicilio</a></li> -->
						<!-- 							<li><a href="#">Editar Email</a></li> -->
						<!-- 						</ul> -->
					</div>
				</h3>
				<hr>

				<div class="row">

					<div class="col-lg-4">
						<label for="rut">Cargo</label>
						<p id="cargo"><?php echo $data_colaborador[0]['nombre_cargo']; ?></p>
					</div>

					<div class="col-lg-4">
						<label for="rut">Tipo de Contrato</label>
						<p id="tipo_contrato"><?php echo $data_colaborador[0]['tipo_contrato']; ?></p>
					</div>

					<div class="col-lg-4"></div>

				</div>

				<div class="row">

					<div class="col-lg-4">
						<label for="rut">Fecha de Inicio</label>
						<p id="inicio_contrato"><?php echo $data_colaborador[0]['fecha_inicio']; ?></p>
					</div>

					<div class="col-lg-4">
						<label for="rut">Fecha de Termino</label>
						<p id="fin_contrato"><?php echo $data_colaborador[0]['fecha_termino']; ?></p>
					</div>

					<div class="col-lg-4"></div>

				</div>

			</div>

			<!-- 			MODAL Datos Básicos -->
			<div class="modal inmodal" id="modal_colaborador_basico"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content animated bounceInRight">
						<div class="modal-header">
							<h4>Datos Básicos del Colaborador:</h4>
							<h4 id="nom_colaborador_basico"></h4>
							<input type="hidden" id="id_colaborador_hidden">
						</div>
						<div class="modal-body">
						
						<ul class="nav nav-tabs">
						  <li class="active"><a data-toggle="tab" href="#basicos-principales">Datos Principales</a></li>
						  <li><a data-toggle="tab" href="#basicos-prevision">Previsión</a></li>
						  <li><a id="tab-familia" data-toggle="tab" href="#basicos-familia">Familia</a></li>
						  <li><a data-toggle="tab" href="#basicos-beneficios">Beneficios</a></li>
						</ul>
						<br>
						<div class="tab-content">
						  <div id="basicos-principales" class="tab-pane fade in active">
						  	
						    <table style="margin-top: 10px;" id="tbl_basicos"
										class="table table-hover table-bordered">
										<tr><th>Rut:</th><td id="basico-rut"></td></tr>
										<tr><th>Nombre:</th><td id="basico-nombre"></td></tr>
										<tr><th>Fecha de Nacimiento:</th><td id="basico-fecha-nacimiento"></td></tr>
										<tr><th>Género:</th><td id="basico-genero"></td></tr>
										<tr><th>Estado Civil:</th><td id="basico-estado-civil"></td></tr>
										<tr><th>Nacionalidad:</th><td id="basico-nacionalidad"></td></tr>
									</table>
									
							<div id="basico-botones-1" class="row">
								<div class="col-lg-12" style="text-align: center;">
									<button id="btn_basico_editar_datos" class="btn btn-success btn-xs">Editar Datos</button>
								</div>
							</div>
							<div id="basico-botones-2" class="row" style="display: none;">
								<div class="col-lg-12" style="text-align: center;">
									<button id="btn_basico_guardar_datos" class="btn btn-primary btn-xs">Guardar</button>
									<button id="btn_basico_cancelar_datos" class="btn btn-warning btn-xs">Cancelar</button>
								</div>
							</div>
									
									
						  </div>
						  <div id="basicos-prevision" class="tab-pane fade">
						    <h1>Hola2</h1>
						  </div>
						  
						  <div id="basicos-familia" class="tab-pane fade">
						  
						  <div id="familiar-main">
							  <button id="btn_familia_agregar" class="btn btn-success btn-xs">Agregar Familiar</button>
							    <table style="margin-top: 10px;" id="tbl_familia"
											class="table table-hover table-bordered">
											<thead>
												<tr style="text-align: center">
													<th>#</th>
													<th>Rut</th>
													<th>Nombre</th>
													<th>Edad</th>
													<th>Relación</th>
												</tr>
											</thead>
											<tbody>
	
											</tbody>
								</table>
							</div>
							
							<div id="familiar-agregar">
								<h2>Agregar Familiar</h2>
								<form class="form horizontal">
									<div class="row">
										<div class="col-lg-4">
											<label>rut</label>
											<input type="text" id="familiar-rut" name="familiar-rut" onkeypress="return solonumero(event)"> - <input style="width: 20%" type="text" id="familiar-dv" name="familiar-dv">
										</div>
										<div class="col-lg-4">
											<label>Nombres</label>
											<input type="text" id="familiar-nombres" name="familiar-nombres">
										</div>
										<div class="col-lg-4">
											<label>Apellidos</label>
											<input type="text" id="familiar-apellidos" name="familiar-apellidos">
										</div>
									</div>	
									<div class="row">
										<div class="col-lg-4">
											<label>Fecha de Nacimiento</label>
											<input type="date" id="familiar-fecha-nacimiento" name="familiar-fecha-nacimiento">
										</div>
										<div class="col-lg-4">
											<label>Parentesco</label>
											<select id="familiar-parentesco" name="familiar-parentesco"></select>
										</div>
									</div>							
								</form>	
							</div>
							
						  </div>
						  <div id="basicos-beneficios" class="tab-pane fade">
						    <h1>Hola4</h1>
						  </div>
						</div>
						
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-warning"
								data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="modal inmodal" id="modal_direccion_colaborador"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content animated bounceInRight">
						<div class="modal-header">
							<h4>Editando datos colaborador:</h4>
							<h4 id="nombre_colaborador_modal"></h4>
						</div>
						<div class="modal-body">



							<!-- START BOOTSTRAP JS TABS -->
							<ul class="nav nav-tabs">
								<li class="active"><a id="nuev-anim" href="#nuevo">Nueva Dirección</a></li>
								<li><a id="edit-anim" href="#editar">Editar Dirección</a></li>
								<li><a id="tel-anim" href="#telefonos">Teléfonos</a></li>
								<li><a id="mail-anim" href="#correos">Correos Electrónicos</a></li>
							</ul>

							<div class="tab-content">
								<div id="nuevo" class="tab-pane fade in active">
									<form class="form-horizontal" id="frm_direccion_registro">
										<input type="text" id="id_c" name="id_c" hidden>
										<div class="row">
											<div class="col-lg-6">
												<label for="d_calle">Calle</label> <input type="text"
													class="form-control" id="d_calle" requerido="true"
													mensaje="Debe Ingresar la calle"
													placeholder="Ej. Huérfanos" name="d_calle">
											</div>
											<div class="col-lg-6">
												<label for="d_numero">Número</label> <input type="text"
													class="form-control" id="d_numero" requerido="true"
													mensaje="Debe Ingresar el numero" placeholder="Ej. 18569"
													name="d_numero">
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<label for="d_departamento">Departamento (Opcional)</label>
												<input type="text" class="form-control" id="d_departamento"
													requerido="false" mensaje="Debe Ingresar el departamento"
													placeholder="Ej. 35-B" name="d_departamento">

											</div>
											<div class="col-lg-6">
												<label class="control-label">Comuna</label> <select
													id="d_comuna" name="d_comuna"
													class="select2_demo_3 form-control" requerido="true"
													mensaje="Debe seleccionar Comuna">
													<option></option>
												</select>
											</div>
										</div>
										<br> <input type="text" id="latitud" name="latitud" hidden="">
										<input type="text" id="longitud" name="longitud" hidden="">


									</form>
								</div>
								<div id="editar" class="tab-pane fade">
									<form class="form-horizontal" id="frm_direccion_editar">
										<input type="text" id="id_c_e" name="id_c_e" hidden>
										<div class="row">
											<div class="col-lg-6">
												<label class="control-label">Seleccione dirección</label> <select
													id="d_direccion_e" name="d_direccion_e"
													class="select2_demo_3 form-control" requerido="true"
													mensaje="Debe seleccionar Direccion" style="width: 100%">
													<option></option>
												</select>
											</div>

											<div class="col-lg-6"></div>

										</div>

										<div class="row">
											<div class="col-lg-6">
												<label for="d_calle">Calle</label> <input type="text"
													class="form-control" id="d_calle_e" requerido="true"
													mensaje="Debe Ingresar la calle" placeholder="Ej. Bandera"
													name="d_calle_e">
											</div>
											<div class="col-lg-6">
												<label for="d_numero_e">Número</label> <input type="text"
													class="form-control" id="d_numero_e" requerido="true"
													mensaje="Debe Ingresar el numero" placeholder="Ej. 18569"
													name="d_numero_e">
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<label for="d_departamento_e">Departamento (Opcional)</label>
												<input type="text" class="form-control"
													id="d_departamento_e" requerido="false"
													mensaje="Debe Ingresar el departamento"
													placeholder="Ej. 35-B" name="d_departamento_e">

											</div>
											<div class="col-lg-6">
												<label class="control-label">Comuna</label> <select
													id="d_comuna_e" name="d_comuna_e"
													class="select2_demo_3 form-control" requerido="true"
													mensaje="Debe seleccionar Comuna" style="width: 100%;">
													<option></option>
												</select>
											</div>
										</div>
										<br> <input type="text" id="latitud_e" name="latitud_e"
											hidden=""> <input type="text" id="longitud_e"
											name="longitud_e" hidden="">

									</form>
									<div style="text-align: right;">
										<button id="btn_eliminar_direccion"
											class="btn btn-danger btn-xs">Eliminar Dirección</button>
									</div>

								</div>



								<div id="telefonos" class="tab-pane fade">

									<div class="row">
										<form class="form-horizontal" id="frm_telefono">
											<input type="hidden" id="id_c_t" name="id_c_t">
											<div class="col-lg-4">
												<label class="control-label">Tipo Teléfono</label> <select
													id="tipoTelefonoCBX" name="tipoTelefonoCBX"
													class="select2_demo_3 form-control" style="width: 100%;"
													requerido="true" mensaje="Debe seleccionar tipo">
													<option></option>
												</select>
											</div>
											<div class="col-lg-4">
												<label class="control-label">Número</label> <input
													type="tel" id="telefono" name="telefono"
													class="form-control" style="width: 100%;" requerido="true"
													mensaje="Debe ingresar número">
											</div>
										</form>
										<div class="col-lg-4"></div>
									</div>

									<table style="margin-top: 10px;" id="tbl_telefonos"
										class="table table-hover table-bordered">
										<thead>
											<tr style="text-align: center">
												<th>#</th>
												<th>Teléfono</th>
												<th>Tipo</th>
												<th>Acción</th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>


								</div>
								<div id="correos" class="tab-pane fade">
									<div class="row">
										<form class="form-horizontal" id="frm_mail">
											<input type="hidden" id="id_c_m" name="id_c_m">
											<div class="col-lg-4">
												<label class="control-label">Tipo Correo</label> <select
													id="tipoMailCBX" name="tipoMailCBX"
													class="select2_demo_3 form-control" style="width: 100%;"
													requerido="true" mensaje="Debe seleccionar tipo">
													<option></option>
												</select>
											</div>
											<div class="col-lg-4">
												<label class="control-label">Correo Electrónico</label> <input
													type="email" id="email" name="email" class="form-control"
													style="width: 100%;" requerido="true"
													mensaje="Debe ingresar correo">
											</div>
										</form>
										<div class="col-lg-4"></div>
									</div>

									<table style="margin-top: 10px;" id="tbl_mails"
										class="table table-hover table-bordered">
										<thead>
											<tr style="text-align: center">
												<th>#</th>
												<th>Correo</th>
												<th>Tipo</th>
												<th>Acción</th>
											</tr>
										</thead>
										<tbody>

										</tbody>
									</table>
								</div>
							</div>
							<!-- END BOOTSTRAP JS TABS -->
							<div id="elmapa">
								<button class="btn btn-warning" id="btn_confirmar_mapa">Confirmar
									en Mapa</button>

								<input id="pac-input" class="controls" type="text"
									placeholder="" value="" readonly="" requerido="true"
									mensaje="Debe confirmar la direccion en el mapa">
								<div id="map"></div>
							</div>
							<!-- 							<script -->
							<!-- 								src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXFAmo7nctNNqsCNWb1bUBx6F2HrLBUMQ&libraries=places&callback=initAutocomplete" -->
							<!-- 								async defer></script> -->
						</div>
						<div class="modal-footer">
							<button type="button" id="btn_guardar_direccion"
								class="btn btn-success">Guardar</button>
							<button type="button" class="btn btn-warning"
								data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<!-- 			MODAL CONTRATOS -->
			<div class="modal inmodal" id="modal_colaborador_contrato"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content animated bounceInRight">
						<div class="modal-header">
							<h4>Contratos para colaborador:</h4>
							<h4 id="nom_colaborador_contrato"></h4>
							<input type="hidden" id="id_oc_hidden">
						</div>
						<div class="modal-body">

							<div id="contratos-main">
								<button id="btn_agregar_contrato" class="btn btn-primary btn-xs">Agregar
									Contrato</button>
								<table style="margin-top: 10px;" id="tbl_contratos"
									class="table table-hover table-bordered">
									<thead>
										<tr style="text-align: center">
											<th>#</th>
											<th>Tipo</th>
											<th>Fecha Inicio</th>
											<th>Fecha Término</th>
											<th>Acción</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>

							<div id="contratos-agregar">
								<button id="btn_cancelar_agregar_contrato"
									class="btn btn-warning btn-xs">Volver</button>
								<form class="form-horizontal" id="frm_agregar_contrato">
									<div class="row">
										<div class="col-lg-4">
											<label for="contrato-tipo">Tipo de Contrato</label> <select
												id="contrato-tipo" name="contrato-tipo" style="width: 100%"
												requerido="true" mensaje="seleccione opción"></select>
										</div>
										<div class="col-lg-4">
											<label for="contrato-cargo">Cargo</label> <input type="text"
												class="form-control" id="contrato-cargo"
												name="contrato-cargo" style="width: 100%" requerido="true"
												mensaje="ingrese cargo">
										</div>
										<div class="col-lg-4">
											<label for="contrato-departamento">Departamento</label> <select
												id="contrato-departamento" name="contrato-departamento"
												style="width: 100%" requerido="true"
												mensaje="seleccione opción"></select>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-4">
											<label for="contrato-fecha-inicio">Fecha Inicio</label> <input
												type="text" class="form-control" id="contrato-fecha-inicio"
												name="contrato-fecha-inicio" style="width: 100%"
												requerido="true" mensaje="seleccione fecha">
										</div>
										<div class="col-lg-4">
											<label for="contrato-fecha-termino">Fecha Término</label> <input
												type="text" class="form-control" id="contrato-fecha-termino"
												name="contrato-fecha-termino" style="width: 100%"
												requerido="true" mensaje="seleccione fecha">
										</div>
									</div>
								</form>
								<br>
								<button id="btn_guardar_contrato" class="btn btn-primary ">Guardar
									Contrato</button>

							</div>

						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-warning"
								data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>