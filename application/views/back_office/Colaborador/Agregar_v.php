<?php
/**
 * Agregar_v
 *
 * Description...
 *  
 * @version 0.0.1
 *
 * Ultima edicion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Colaborador</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_colaborador_registro"  >
                    <p> Ingrese un nuevo colaborador</p>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Rut</label>
                        <div class="col-lg-3">
                            <input type="text"  class="form-control required" id="rut" requerido="true" onkeypress="return solonumero(event)" mensaje ="Debe Ingresar el Rut" name="rut" size="8" class="form-control"/>
                        </div>

                        <p style="width: 0" class="col-lg-1">-</p>
                        <div class="col-lg-1">
                            <input type="text" class="form-control required" id="dv" requerido="true" mensaje ="" name="dv" size="1" class="form-control"/>
                        </div>

                        <div class="col-lg-3">
                            <p id="out_print" style="color: red"></p>
                        </div>

                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombres</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="nombre" requerido="true" mensaje ="Debe Ingresar la URL del menú" name="nombre" class="form-control" placeholder="Nombres"/>
                        </div>

                        <!-- <label class="col-lg-2 control-label">Segundo nombre</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="seg_nombre" requerido="false" name="seg_nombre" class="form-control" placeholder="Segundo nombre"/>
                        </div> -->
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Apellido Paterno</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="apellido_pat" requerido="true" mensaje ="Debe Ingresar el apellido paterno" name="apellido_pat" placeholder="Apellido Paterno" class="form-control"/>
                        </div>

                        <label class="col-lg-2 control-label">Apellido Materno</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="apellido_mat" requerido="false" mensaje ="Debe Ingresar el apellido materno" name="apellido_mat" placeholder="Apellido Materno" class="form-control"/>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label class="col-lg-2 control-label">Ingrese el nombre de usuario</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="usuario_alumno_v" requerido="false" mensaje ="Debe Ingresar el nombre de usuario con el cual se identificara en el sistema" name="usuario_alumno_v" class="form-control"/>
                        </div>
                    </div> -->

                    <div class="form-group">
                        
                        <label class="col-lg-2 control-label">Nacionalidad</label>
                        <div class="col-lg-3">
                                <select id="nacionalidad" name="nacionalidad"  class="select2_demo_3 form-control" requerido="true" mensaje ="Debe seleccionar una nacionalidad">
                                    <option></option>
                                </select>
                        </div>

                        <label class="col-lg-2 control-label">Género</label>
                        <div class="col-lg-3">
                                <select id="genero" name="genero"  class="select2_demo_3 form-control" requerido="true" mensaje ="Debe Ingresar el género">
                                    <option></option>
                                </select>
                        </div>

                    </div>

                    <div class="form-group">
                    <label class="col-lg-2 control-label">Fecha de Nacimiento</label>
                        <div class="col-lg-3">
                            
                            <input type="date" class="form-control" id="fechaNac" name="fechaNac"
                            requerido="true" mensaje ="Debe Ingresar la fecha de nacimiento">
                        </div>
                    </div>

                    <div class="form-group">
                        
                        <label class="col-lg-2 control-label">Estado Civil</label>
                        <div class="col-lg-3">
                                <select id="estado_civil" name="estado_civil"  class="select2_demo_3 form-control"
                                requerido="true" mensaje ="Debe Ingresar un estado civil">
                                    <option></option>
                                </select>
                        </div>

                        <!-- <label class="col-lg-2 control-label">Género:</label>
                        <div class="col-lg-3">
                                <select id="genero" name="genero"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                        </div> -->
                    </div>
                </form>
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar_colaborador" id="btn_registrar_colaborador">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
            </div>
        </div>
    </div>
</div>
