<?php
/**
 * UsuarioLista_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Docentes</h5>
					<br>
                                        <form class="form-horizontal" id="frm_filtrarxxxx" name="frm_filtrarxxxx">
					<div class="row">
						<div class="col-lg-4 ">
							<label>Filtrar por Skill</label>
							<select id="conocimiento_filtro_cbx" name="conocimiento_filtro_cbx"
								class="select2 form-control">
							</select>
						</div>
						<div class="col-lg-4">
							<label>Nivel</label>
							<select id="nivel_filtro_cbx" name="nivel_filtro_cbx"
								class="select2 form-control">
							</select>
						</div>
					</div>
					</form>
					<div class="row">
					<div class="col-lg-4">
						</div>
						<div class="col-lg-4">
						</div>
						<div class="col-lg-4">
						<br>
							<button id="btn_filtrar" type="button" class="btn btn-primary">Filtrar</button>
						</div>
					</div>

					<div class="ibox-tools">

						<button type="button" id="btn_nuevo"
							class="btn btn-w-m btn-primary">Nuevo</button>

					</div>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">


						<table id="tbl_docentes"
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>Id</th>
									<th>Usuario</th>
									<th>Nombre</th>
									<th>Apellidos</th>
									<th>Rut</th>
									<th>Email</th>
									<th>Dirección</th>
									<th>Telefono</th>
									<th>Estado</th>
									<th>Opciones</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Id</th>
									<th>Usuario</th>
									<th>Nombre</th>
									<th>Apellidos</th>
									<th>Rut</th>
									<th>Email</th>
									<th>Dirección</th>
									<th>Telefono</th>
									<th>Estado</th>
									<th>Opciones</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!--    MODAL SKILL -->

	<div class="modal inmodal fade in" id="ModalSkill" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title" id="nom_relator"></h4>
					<p class="font-bold">El relator debe informar nuevos conocimientos
						y mantenerlos actualizados para una mejor administración.</p>
				</div>

				<div class="modal-body">

					<div id="lista_skills">
						<div class="bs-example">
							<div class="progress">
								<div class="progress-bar progress-bar-danger" style="width: 25%">Básico</div>
								<div class="progress-bar progress-bar-warning"
									style="width: 25%">Intermedio</div>
								<div class="progress-bar progress-bar-primary"
									style="width: 25%">Avanzado</div>
								<div class="progress-bar progress-bar-success"
									style="width: 25%">Macro</div>
							</div>
						</div>
						<button type="button" id="btn_ingresar_skill"
							class="btn btn-primary">Ingresar Skill</button>
						<table id="tbl_skills" class="table">
							<thead>
								<tr>
									<th style="text-align: center;">Conocimiento</th>
									<th style="text-align: center;">Nivel</th>
									<th style="text-align: center;">Opciones</th>
								</tr>
							</thead>
							<tbody></tbody>
							<tfoot>
							</tfoot>
						</table>

					</div>

					<div id="edit_skill">
						<table class="table">
							<tr>
								<td style="width: 1px; white-space: nowrap;"><strong
									id="nombre_conocimiento" style="color: #176FA8;"></strong></td>
								<td><div class="progress">
										<div id="progress_bar_edit" class="progress-bar"
											style="width: 25%"></div>
									</div>
									<button id="btn_left_edit" type="button" class="btn btn-white">
										<i class="fa fa-chevron-left"></i>
									</button>
									<button id="btn_right_edit" type="button" class="btn btn-white">
										<i class="fa fa-chevron-right"></i>
									</button></td>
								<td style="width: 1px; white-space: nowrap;"><button
										id="btn_actualizar_skill" class="btn btn-primary dim"
										type="button">
										<i class="fa fa-save" style="font-size: 120%;"></i><br> <small
											style="font-size: 60%; padding: none;">Guardar</small>
									</button></td>
							</tr>

						</table>
						<input type="hidden" id="id_usuario_edit" value=""> <input
							type="hidden" id="id_skill_edit" value="">
					</div>

					<div id="ingresar_skill">
						<form class="form-horizontal" action="">
							<div class="row">
								<div class="col-lg-5">
									<label>Seleccione Conocimiento</label><br> <select
										id="conocimiento_cbx" name="conocimiento_cbx"></select>
								</div>
								<div class="col-lg-7">
									<div class="progress">
										<div id="progress_bar_ingresar"
											class="progress-bar progress-bar-danger" style="width: 25%">Básico</div>
									</div>
									<button id="btn_left_ingresar" type="button"
										class="btn btn-white">
										<i class="fa fa-chevron-left"></i>
									</button>
									<button id="btn_right_ingresar" type="button"
										class="btn btn-white">
										<i class="fa fa-chevron-right"></i>
									</button>
								</div>
							</div>
						</form>
						<div class="row">
							<br>
							<div class="col-lg-8">
								<button id="btn_guardar_ingresar_skill" class="btn btn-primary">Guardar</button>
								<button id="btn_cancelar_ingresar_skill" class="btn btn-info">Cancelar</button>
							</div>

						</div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="button" id="btn_cerrar_modal" class="btn btn-white"
						data-dismiss="modal">Cerrar</button>
					<!-- 					<button type="button" class="btn btn-primary">Save changes</button> -->
				</div>
			</div>
		</div>
	</div>
</div>
