<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Actualizar Docente</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm_docente_editar">
                        <p> Actualizar docente </p>


                        <div class="form-group">
                            <input type="text" name="id" id="id" value="<?php echo $data_docente[0]['id']; ?>" hidden="">
                            <label class="col-lg-2 control-label">RUT:</label>
                            <div class="col-lg-3">
                                <input type="text" class="form-control required" id="rut_docente" requerido="false" mensaje="Debe Ingresar el RUT del docente"
                                    placeholder="Ej. 9876543" name="rut_docente" class="form-control" value="<?php echo $data_docente[0]['rut']; ?>"
                                    readonly="readonly">
                            </div>
                            <div class="col-lg-1">
                                <input type="text" class="form-control required" id="dv_docente" requerido="false" mensaje="Debe Ingresar el digito verificador"
                                    placeholder="2" name="dv_docente" class="form-control" value="<?php echo $data_docente[0]['dv']; ?>"
                                    readonly="readonly">
                            </div>

                            <label class="col-lg-2 control-label">Nombre de Usuario:</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control required" id="docente" requerido="true" mensaje="Debe Ingresar un nombre de docente válido"
                                    placeholder="Ingrese Nombre identificatorio del docente" name="docente" class="form-control"
                                    value="<?php echo $data_docente[0]['usuario']; ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Nombre:</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control required" id="nombre_docente" requerido="true" mensaje="Debe Ingresar un nombre de docente válido"
                                    placeholder="Ingrese Nombre real del docente" name="nombre_docente" class="form-control"
                                    value="<?php echo $data_docente[0]['nombre']; ?>">
                            </div>

                            <label class="col-lg-2 control-label">Apellidos:</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control required" id="apellidos_docente" requerido="true" mensaje="Debe Ingresar un nombre de docente válido"
                                    placeholder="Ingrese Apellidos reales del docente" name="apellidos_docente" class="form-control"
                                    value="<?php echo $data_docente[0]['apellidos']; ?>">
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="col-lg-2 control-label">Email:</label>
                            <div class="col-lg-4">
                                <input type="email" class="form-control required" id="correo" requerido="true" mensaje="Debe Ingresar un email válido" placeholder="Ej.: jperez@edutecno.com"
                                    name="correo" class="form-control" value="<?php echo $data_docente[0]['correo']; ?>">
                            </div>

                            <label class="col-lg-2 control-label">Telefono:</label>
                            <div class="col-lg-4">
                                <input type="tel" class="form-control required" id="telefono" requerido="true" mensaje="Debe Ingresar un N° de telefono "
                                    placeholder="9 8654 5092" name="telefono" class="form-control" value="<?php echo $data_docente[0]['telefono']; ?>">
                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-lg-2 control-label">Dirección</label>
                            <div class="col-lg-4">
                                <input type="text" class="form-control required" id="direccion" requerido="true" mensaje="Debe Ingresar una dirección válida"
                                    placeholder="Ej: Av. Las Condes xxxx, Las condes, Santiago." name="direccion" class="form-control"
                                    value="<?php echo $data_docente[0]['direccion']; ?>">
                            </div>


                            <label class="col-lg-2 control-label"></label>
                            <div class="col-lg-4">
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-sm btn-primary" type="button" name="btn_editar_docente" id="btn_editar_docente">Actualizar</button>
                                <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>