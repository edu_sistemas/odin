<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar holding</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <form class="form-horizontal" id="frm_holding_registro"  >
                    <p>Modifique Holding</p>

                    <input type="text" id="id_holding" name="id_holding" value="<?php echo $data_holding[0]['id_holding']; ?>" hidden="">

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-6">
                            <input type="text"  id="true" requerido="true"  mensaje ="Debe Ingresar el nombre del holding  placeholder="Ingrese el nombre del holding" name="nombre_holding" class="form-control" value="<?php echo $data_holding[0]['nombre_holding']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Descripción:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control required"
                                      id="descripcion_holding" requerido="false"
                                      mensaje ="Debe Ingresar una o más observaciones"
                                      name="descripcion_holding"
                                      placeholder="Ingrese observaciones o referencias de la holding a registrar. Estas observaciones pueden servir para recordar de que holding se trata en caso que no se recuerde bien..."
                                      maxlength="200"
                                      rows="6"><?php echo $data_holding[0]['descripcion_holding']; ?></textarea>
                                <p id="text-out">0/200 caracteres restantes</p>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Estado</label>
                        <input id="estado" type="text" name="estado" value="<?php echo $data_holding[0]['estado_holding']; ?>" hidden>
                        <div class="col-lg-1">
                            <div class="checkbox checkbox-success">
                                <input id="estado_holding" type="checkbox">
                                <label id="estado_label" for="estado_holding">
                                    Activado
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-9">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_holding" id="btn_editar_holding">Actualizar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
</div>
