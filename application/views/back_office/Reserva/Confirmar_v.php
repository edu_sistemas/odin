<?php
/**
 * Agregar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-13 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-01-03 [Marcelo Romero] <mromero@edutecno.com>
 */
?>

    <head>

        <style>
            /*select2*/

            .select2-container {
                z-index: 2055 !important;
            }

            .select2 {
                width: 100% !important;
                padding: 0;
            }

            #contenedor_select .select2-container {
                z-index: 1 !important;
            }

            #contenedor_select .select2 {
                width: 100% !important;
                padding: 0;
            }

            .popover {
                z-index: 2051;
            }

            .input_show,
            #nombre_reserva {
                width: 100%;
            }

            .swal2-container {
                z-index: 2061;
            }

            #ref_mod,
            #ref_mod_a,
            #ref_mod_dat,
            #ref_mod_dat_a {
                font-size: larger;
            }
        </style>

    </head>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Confirmar reserva</h5>
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" id="frm_dates">
                        <p>Confirmar reserva de salas</p>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Consultar sala</label>
                            <div class="col-md-3" style="padding-left: 0px !important;">
                                <select id="id_sala_v" name="id_sala_v" onchange="desplegarCalendario()" class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                            </div>
                            <!--<div id = "indicador_estado" style="display:inline-block;"-->
                                <div class="col-md-2">
                                    <i style="font-size: 40px !important; width: 40px; color: #1674E9;" class="fa fa-square"></i>Solicitado</a>
                                </div>
                                <div class="col-md-2">
                                    <i style="font-size: 40px !important; width: 40px; color: #19aa8d;" class="fa fa-square"></i>Confirmado</a>
                                </div>
                                <div class="col-md-2">
                                    <i style="font-size: 40px !important; width: 40px; color: #DA2155;" class="fa fa-square"></i>Feriado</a>
                                </div>
                               
                        </div>
                        <hr>
                        <br>
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div id='top'>
                                </div>
                                <div id='calendar'></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="ibox-content">
        <div class="row">
            <form role="form" class="form-inline" id="frm_fichas" name="frm">

                <div class="col-lg-3">
                    <label>Nombre empresa</label>
                    <div id="contenedor_select">
                        <div style="padding-left: 0px !important;">
                            <select style=" width: 100%;" id="id_empresa" name="id_empresa" class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2" style="margin-top: 22px;">
                    <button style="margin-left: 10px;" type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                    <button type="button" id="btn_buscar_empresa" name="btn_buscar_empresa" class="btn btn-success pull-right">Buscar</button>
                </div>
            </form>
        </div>
        <br>
        <br>
        <div class="table-responsive">
            <table id="tbl_reservas" class="table table-striped table-bordered table-hover dataTables-example">
                <thead>
                    <tr>
                        <th>Id reserva</th>
                        <th>Id detalle sala</th>
                        <th>Nombre reserva</th>
                        <th>Fecha de inicio</th>
                        <th>Fecha de termino</th>
                        <th>Hora inicio</th>
                        <th>Hora termino</th>
                        <th>Dias</th>
                        <th>Sala</th>
                        <th>Holding</th>
                        <th>Empresa</th>
                        <th>Ejecutivo</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Id reserva</th>
                        <th>Id detalle sala</th>
                        <th>Nombre reserva</th>
                        <th>Fecha de inicio</th>
                        <th>Fecha de termino</th>
                        <th>Hora inicio</th>
                        <th>Hora termino</th>
                        <th>Dias</th>
                        <th>Sala</th>
                        <th>Holding</th>
                        <th>Empresa</th>
                        <th>Ejecutivo</th>
                        <th>Acción</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal inmodal in" id="modalConfirmacion" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg"  style="z-index: 2050;">

            <!-- Modal content-->
            <div class="modal-content animated fadeIn" style="z-index: 2050;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>

                    <i class="fa fa-clock-o modal-icon"></i>
                    <h4 class="modal-title">Confirmar Reserva</h4>
                    <small>Antes de confirmar, corrobore los datos de la reserva.</small>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal" id="formModal">

                        <div class="row">
                            <div class="col-lg-6 text-left" style="font-size:16px">
                                <b>Confirma: <?php echo $this->session->userdata('nombre_user'); ?></b>
                            </div>
                            <div class="col-lg-6  text-left" style="font-size:16px">
                                <b id="estado"></b>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="col-md-2 control-label" id="LabelP">Registro:</label>
                                <div class="col-md-8" style="margin-left: 30px;" id="LabelP">
                                    <input type='text' class="form-control" id="fecha_registro" name="fecha_registro" readonly />
                                    <input type="hidden" id="id_reserva">
                                    <input type="hidden" id="id_detalle_sala">
                                </div>
                            </div>
                           
                            <div class="col-sm-6">
                                <label class="col-md-2 control-label">Sala:</label>
                                <div class="col-md-8" id="saladef">
                                    <input type='text' class="form-control" id="sala" readonly />
                                </div>
                            </div>
                        </div>
                        <br>


                        <div class="row">
                            <label class="col-sm-2 control-label">Rango de fecha: </label>
                            <div class="input-daterange input-group  col-md-7" style="padding-left: 15px;" id="datepicker2">
                                <input type="text" class="input-sm form-control" name="fecha_inicio" id="fecha_inicio" readonly/>
                                <span class="input-group-addon">Hasta</span>
                                <input type="text" class="input-sm form-control" name="fecha_termino" id="fecha_termino" readonly/>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <label class="col-md-2 control-label">Hora Inicio: </label>
                            <div class="col-md-3">
                                <div class="input-group clockpicker" id="LabelP">
                                    <input type="text" class="form-control" id="hora_inicio" name="hora_inicio" readonly>
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">Hora Término: </label>
                            <div class="col-md-3">
                                <div class="input-group clockpicker" id="LabelP">
                                    <input type="text" class="form-control" id="hora_termino" name="hora_termino" readonly>
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <label class="col-sm-2 control-label">Días seguidos: </label>
                            <div class="col-md-10 form-group">
                                <label class="checkbox-inline i-checks">
                                    <input id="lunesS" name="lunesS" type="checkbox" value=""> Lunes</label>
                                <label class="checkbox-inline i-checks">
                                    <input id="martesS" name="martesS" type="checkbox" value=""> Martes</label>
                                <label class="checkbox-inline i-checks">
                                    <input id="miercolesS" name="miercolesS" type="checkbox" value=""> Miercoles</label>
                                <label class="checkbox-inline i-checks">
                                    <input id="juevesS" name="juevesS" type="checkbox" value=""> Jueves</label>
                                <label class="checkbox-inline i-checks">
                                    <input id="viernesS" name="viernesS" type="checkbox" value=""> Viernes</label>
                                <label class="checkbox-inline i-checks">
                                    <input id="sabadoS" name="sabadoS" type="checkbox" value=""> Sabado</label>
                                <label class="checkbox-inline i-checks">
                                    <input id="domingoS" name="domingoS" type="checkbox" value=""> Domingo</label>
                            </div>
                        </div>
                        <br>
                        <br>
                        <h4>Datos de la reserva</h4>
                        <br>
                           <div class="form-group">
                                <label class="col-sm-2 control-label">Reserva:</label>
                                <div class="col-md-8">
                                    <input type='text' class="input_show" id="nombre_reserva" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Ejecutivo:</label>
                                <div class="col-md-8">
                                    <input type='text'class="input_show"  id="ejecutivo" readonly />
                                </div>
                            </div>
        
                        <br>
                           <div class="form-group">
                                <label class="col-sm-2 control-label">Holding:</label>
                                <div class="col-md-8">
                                    <input type='text' class="input_show" id="holding" readonly />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Empresa:</label>
                                <div class="col-md-8">
                                    <input type='text' class="input_show" id="empresa" readonly />
                                </div>
                            </div>
                        
                        <br>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Detalle Reserva:</label>
                                <div class="col-md-8">
                                    <textArea id="detalle_reserva" class="input_show" cols="80" rows="4" readonly></textArea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Dirección:</label>
                                <div class="col-md-8">
                                    <textArea id="direccion" class="input_show" cols="80" rows="4" readonly></textArea>
                                </div>
                            </div>
                        
                        <br>
                          <div class="form-group">
                                <label class="col-sm-2 control-label">Break:</label>
                                <div class="col-md-8">
                                    <textArea id="break" class="input_show" cols="80" rows="4" readonly></textArea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Computadores:</label>
                                <div class="col-md-8">
                                    <textArea id="computadores" class="input_show"  cols="80" rows="4" readonly></textArea>
                                </div>
                            </div>
                    
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btn_confirmar_bloque" >Confirmar Bloque</button>
                    <button type="button" class="btn btn-primary" id="btn_confirmar_reserva">Confirmar Reserva</button>
                    <button type="button" class="btn btn-primary" id="btn_rechazar">Rechazar Reserva</button>

                </div>
            </div>

        </div>
    </div>

<div class="modal" tabindex="-1" id="modalRechazarReserva" role="dialog" style="z-index: 2050;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" style="font-size:20px;">Rechazo Reserva</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <form method="post" class="form-horizontal" id="formModal" name="formModal">
                <label>Motivo del Rechazo:</label><br>
                <textArea name="motivo_rechazo" id="motivo_rechazo" cols="70" rows="10" required></textArea>
                
            </form>
        </div>
            
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn_rechazar_reserva">Rechazar Reserva</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal" tabindex="-1" id="modalRechazarBloqueReserva" role="dialog" style="z-index: 2050;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" style="font-size:20px;">Rechazo Bloque Reserva</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <form method="post" class="form-horizontal" id="formModal" name="formModal">
                <label>Motivo del Rechazo:</label><br>
                <textArea name="motivo_rechazo_bloque" id="motivo_rechazo_bloque" cols="70" rows="10" required></textArea>
                
            </form>
        </div>
            
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btn_rechazar_bloque_reserva">Rechazar Bloque Reserva</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>