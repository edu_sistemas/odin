<?php
/**
 * Agregar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-13 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-01-03 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Eliminar reserva</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_dates">
                    <p>Eliminar reserva de salas</p>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Consultar sala</label>
                        <div class="col-md-3" style="padding-left: 0px !important;">
                            <select id="id_sala_v" name="id_sala_v" onchange="desplegarCalendario()" class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <i style="font-size: 36px !important; width: 40px; color: #1674E9;" class="fa fa-square"></i>Solicitado</a>
                        </div>
                        <div class="col-md-2">
                            <i style="font-size: 36px !important; width: 40px; color: #19aa8d;" class="fa fa-square"></i>Confirmado</a>
                        </div>
                        <div class="col-md-2">
                            <i style="font-size: 36px !important; width: 40px; color: #DA2155;" class="fa fa-square"></i>Feriado</a>
                        </div>
                    </div>
                    <hr>
                    <br>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div id='top'>
                            </div>
                            <div id='calendar'></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal in" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-minus-square-o modal-icon"></i>
                <h4 class="modal-title">Eliminación de Reserva <span id="id_reserva_e"></span> </h4>
                
                <small>Para la eliminación puede eliminar la capsula actual o toda la reserva.</small>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal">
                    <h3>Reserva:
                        <?php echo $this->session->userdata('nombre_user'); ?>
                    </h3><br>
                    <div class="row">
                        <div class="col-md-6" id="Labelc">
                            <label class="col-md-3" id="LabelP">Fecha de solicitud:</label>
                            <div class="col-md-8" id="LabelP">
                                <div class='input-group date' id='datepicker'>
                                    <input type='text' class="form-control" id="fRegistro" value="<?php echo date(" Y-m-d H:i:s "); ?>" readonly="readonly" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-3" id="Labelc">Sala:</label>
                            <div class="col-md-8" id="LabelP">
                                <div class='input-group' id='datepicker'>
                                    <input type='text' class="form-control" id="numSala" readonly="readonly" />
                                </div>
                                <input type="hidden" id="idSala" value="">
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-3" for="usuario_registro">Realizó la reserva:</label>
                            <div class='input-group col-md-8'>
                                <input type='text' class="form-control" id="usuario_registro" readonly="readonly" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-3" for="ejecutivo">Ejecutivo:</label>
                            <div class='input-group col-md-8'>
                                <input type='text' class="form-control" id="ejecutivo" readonly="readonly" />
                            </div>
                        </div>
                    </div>
                    <br>
                    <h4>Horario del bloque</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-3" id="Labelc">Inicio reserva:</label>
                            <div class="col-md-8 input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                <input id="horario_inicio" value="" class="form-control" size="16" type="text" readonly="readonly">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-3" id="Labelc">Término reserva:</label>
                            <div class="col-md-8 input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                <input id="horario_termino" value="" class="form-control" size="16" type="text" readonly="readonly">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <br><br><br>
                    </div>
                    <h4>Datos de la reserva</h4>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nombre de la reserva:</label>
                        <div class="col-sm-4">
                            <input type="text" size="40" id="nombre_reserva" readonly="readonly" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Detalles Reserva:</label>
                        <div class="col-sm-10">
                            <textarea id="detalle_reserva" readonly cols="80" rows="4"></textarea>
                            <span class="help-block m-b-none">Indeque los detalles de la reserva</span>
                        </div>
                        <label class="col-sm-2 control-label">Detalles del break:</label>
                        <div class="col-sm-10">
                            <textarea id="break" cols="80" readonly rows="4"></textarea>
                            <span class="help-block m-b-none">Indique detalles del break (si tiene o no tiene)</span>
                        </div>
                        <label class="col-sm-2 control-label">Computadores:</label>
                        <div class="col-sm-10">
                            <textarea id="computadores" readonly cols="80" rows="4"></textarea>
                            <span class="help-block m-b-none">Indique cantidad y caracterisitcas de los computadores</span>
                        </div>
                    </div>
                </form>
                <hr>
                <h4>Eliminación de la reserva</h4><br>
                <input type="hidden" id="visible_div" name="visible_div">
                <form class="form-horizontal" id="frm_capsula" name="frm_capsula">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Eliminar Horario Actual</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <button id="capsula" class="btn btn-w-m btn-danger" type="button"><i class="fa fa-trash-o"></i>&nbsp;Eliminar Horario</button>
                                    <input type="hidden" id="id_reserva" >
                                    <input type="hidden" id="id_detalle_sala" >
                                    <input type="hidden" id="id_capsula" >
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Eliminar Blooque <span id="id_detalle_sala_e" ></span></label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <button id="bloque" class="btn btn-w-m btn-danger" type="button"><i class="fa fa-trash-o"></i>&nbsp;Eliminar Bloque</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <hr>
                <form class="form-horizontal">
                    <div class="form-group" id="eliminacion_r">
                        <label class="col-sm-2 control-label">Eliminar Reserva Completa</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <button id="reserva" class="btn btn-w-m btn-danger" type="button"><i class="fa fa-trash-o"></i>&nbsp;Eliminar Reserva</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="modal-footer">
                    <button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>