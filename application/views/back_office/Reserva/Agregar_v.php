<?php
/**
 * Agregar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2016-19-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2018-23-03 [Jessica Roa] <jroa@edutecno.com>
 */
?>
<style>
        /*select2*/
    .select2{
        width: 100% !important;
    }
    .select2-container--open {
        z-index: 2060;
    }

    .swal2-container {
        z-index: 2061;
    }
    
    #ref_mod, #ref_mod_a, #ref_mod_dat, #ref_mod_dat_a {
        font-size: larger;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Realizar nueva reserva</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_sence_registro">
                    <p>Revisar disponibilidad de salas</p>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Consultar sala</label>
                        <div class="col-md-3" style="padding-left: 0px !important;">
                            <select id="id_sala_v" name="id_sala_v" onchange="desplegarCalendario()" class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <i style="font-size: 36px !important; width: 40px; color: #1674E9;" class="fa fa-square"></i>Solicitado</a>
                        </div>
                        <div class="col-md-2">
                            <i style="font-size: 36px !important; width: 40px; color: #19aa8d;" class="fa fa-square"></i>Confirmado</a>
                        </div>
                        <div class="col-md-2">
                            <i style="font-size: 36px !important; width: 40px; color: #DA2155;" class="fa fa-square"></i>Feriado</a>
                        </div>
                    </div>
                    <hr>
                    <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nueva Reserva</button>
                    <br>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <div id='top'>
                            </div>
                            <div id='calendar'></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal in" id="myModal4" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg"  style="z-index: 2050;">
        <div class="modal-content animated fadeIn" style="z-index: 2050;">
            <div class="modal-header" style="">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-clock-o modal-icon"></i>
                <h4 class="modal-title">Reserva de Sala <span id="id_reserva"></span></h4>
                <small>Antes de reservar, confirme la disponibilidad de salas.</small>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal" id="formModal" name="formModal">
               
                    <div class="row">
                        <div class="col-md-6" id="Labelc" style="font-size:16px">
                            <label class="col-md-3" id="LabelP">Reserva:</label>
                            <div class="col-md-5" id="LabelP">
                                <label id="LabelP">
                                    <?php echo $this->session->userdata('nombre_user'); ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="col-sm-2 control-label" id="LabelP">Registro:</label>
                            <div class="col-md-8" id="LabelP"> 
                                <input type='text' class="form-control" id="fRegistro" name="fRegistro" value="<?php echo date(" d-m-Y H:i:s "); ?>" readonly="" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-md-2  control-label">Sala:</label>
                            <div class="col-md-8 input-group" id="saladef">
                                <input type='text' class="form-control" id="numSala" readonly="readonly" />
                            </div>
                            <div class="form-group col-md-8" id="salanodef">
                                <select id="cbx_sala" name="cbx_sala" class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                            </div>
                            <input type="hidden" id="idSala" name="idSala" value="">
                            <input type="hidden" id="idReserva" name="idReserva" value="">
                        </div>
                    </div><br>

                    <div id="detalles_horario">
                        <div class="row">
                            <label class="col-sm-2 control-label">Rango de fecha: </label>
                            <div class="input-daterange input-group  col-md-7" style="padding-left: 15px;" id="datepicker2">
                                <input type="text" class="input-sm form-control" autocomplete="off" name="fecha_inicio" id="fecha_inicio" placeholder="DD-MM-AAAA" requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                                <span class="input-group-addon">Hasta</span>
                                <input type="text" class="input-sm form-control" autocomplete="off" name="fecha_termino" id="fecha_termino" placeholder="DD-MM-AAAA" requerido="true" mensaje="Fecha Término es un campo requerido" />
                            </div><br>
                        </div>

                        <div class="row">
                            <label class="col-sm-2 control-label">Hora Inicio: </label>
                            <div class="col-md-3">
                                <div class="input-group clockpicker" id="LabelP" data-autoclose="true">
                                    <input type="text" class="form-control" autocomplete="off" id="txt_hora_inicio" name="txt_hora_inicio" value="09:30" requerido="true" mensaje="Ingresar Hora Inicio">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">Hora Término: </label>
                            <div class="col-md-3">
                                <div class="input-group clockpicker" id="LabelP" data-autoclose="true">
                                    <input type="text" class="form-control" autocomplete="off" id="txt_hora_termino" name="txt_hora_termino" value="09:30" requerido="true" mensaje="Ingresar Hora Término">
                                    <span class="input-group-addon">
                                        <span class="fa fa-clock-o"></span>
                                    </span>
                                </div>
                            </div>
                        </div><br>

                        <div class="row">
                            <label class="col-sm-2 control-label">Días seguidos: </label>
                            <div class="col-md-10 form-group">
                                <label class="checkbox-inline i-checks"> <input id="lunes" name="lunes" type="checkbox" value=""> Lunes</label>
                                <label class="checkbox-inline i-checks"> <input id="martes" name="martes" type="checkbox" value=""> Martes</label>
                                <label class="checkbox-inline i-checks"> <input id="miercoles" name="miercoles" type="checkbox" value=""> Miercoles</label>
                                <label class="checkbox-inline i-checks"> <input id="jueves" name="jueves" type="checkbox" value=""> Jueves</label>
                                <label class="checkbox-inline i-checks"> <input id="viernes" name="viernes" type="checkbox" value=""> Viernes</label>
                                <label class="checkbox-inline i-checks"> <input id="sabado" name="sabado" type="checkbox" value=""> Sabado</label>
                                <label class="checkbox-inline i-checks"> <input id="domingo" name="domingo" type="checkbox" value=""> Domingo</label>
                            </div>
                        </div>
                    </div>

                    <h4>Datos de la reserva</h4>

                    <br>
                    <!--div class="form-group">
                        <label class="col-sm-2 control-label">Reserva:</label>
                        <div class="col-sm-10">
                            <select id="usuarioComercial" name="usuarioComercial" onchange="datosReserva(this)">
                            </select>
                        </div>
                    </div-->
                    
                    <div class="form-group" id="from_reserva">
                        <label class="col-sm-2 control-label">Reserva:</label>
                        <div class="col-md-8">
                            <select id="cbx_reserva" name="cbx_reserva"  class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ejecutivo:</label>
                        <div class="col-md-6">
                            <select id="cbx_ejecutivo" name="cbx_ejecutivo" class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                            <input type="text" id="nombre_ejecutivo" name="nombre_ejecutivo" class="input_show">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Holding:</label>
                        <div class="col-md-6">
                            <select id="cbx_holding" name="cbx_holding" class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                            <input type="text" id="nombre_holding" name="nombre_holding" class="input_show">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Empresa:</label>
                        <div class="col-md-6">
                            <select id="cbx_empresa" name="cbx_empresa" class="select2_demo_3 form-control">
                                <option></option>
                            </select>
                            <input type="text" id="nombre_empresa" name="nombre_empresa" class="input_show">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nombre Reserva:</label>
                        <div class="col-sm-6">
                            <input type="text" id="nombre_reserva" name="nombre_reserva" width="300px">
                            <span class="help-block m-b-none">Indique nombre de la reserva</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Detalles Reserva:</label>
                        <div class="col-sm-6">
                            <textArea id="detalles_reserva" name="detalles_reserva" cols="80" rows="4"></textarea>
                                <span class="help-block m-b-none">Indique los detalles de la reserva</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label id="ldireccion" class="col-sm-2 control-label">Direccion:</label>
                        <div id="ddireccion" class="col-sm-6">
                            <textArea id="direccion" name="direccion" cols="80" rows="4"></textarea>
                            <span class="help-block m-b-none">Indique direccion donde se realizará la clase</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Break:</label>
                        <div class="col-sm-6">
                            <textArea id="breakSala" name="breakSala" cols="80" rows="4"></textarea>
                            <span class="help-block m-b-none">Indique detalles del break (si tiene o no tiene)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Computadores:</label>
                        <div class="col-sm-6">
                            <textArea id="computadores" name="computadores" cols="80" rows="4"></textarea>
                            <span class="help-block m-b-none">Indique cantidad y detalle de computadores</span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <!--button type="button" id="bt_editar" class="btn btn-primary btn-outline" onclick="validaFormModal(1);">Editar</button-->
                <button type="button" class="btn btn-primary" id="btn_reservar" onclick="validaFormModal(0);">Reservar</button>
            </div>
        </div>
    </div>
</div>
<style>
    .popover{
        z-index: 2051;
    }

    .input_show,
    #nombre_reserva{
        width: 100%;
    }
</style>
 