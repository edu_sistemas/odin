<?php
/**
 * Listar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-26 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-12-26 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Módulo de reserva</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_sence_registro"  >
                    <p>Visualizar disponibilidad de salas</p>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Consultar sala</label>
                        <div class="col-md-3" style="padding-left: 0px !important;">
                            <select id="id_sala_v" name="id_sala_v" onchange="desplegarCalendario()" class="select2_demo_3 form-control">
                                <option></option>
                            </select>

                        </div>
                        
                             <div class="col-md-2" >
                                <i style="font-size: 36px !important; width: 40px; color: #1674E9;" class="fa fa-square"></i>Solicitado</a> 
                            </div>
                            <div class="col-md-2" >
                                <i style="font-size: 36px !important; width: 40px; color: #19aa8d;" class="fa fa-square"></i>Confirmado</a> 
                            </div>
                            <div class="col-md-2" >
                                <i style="font-size: 36px !important; width: 40px; color: #DA2155;" class="fa fa-square"></i>Feriado</a> 
                            </div>
                        
                    </div>
                    <hr>
                    <br>

                    <div class="form-group">
                        <div class="col-lg-12">
                            <div id='top'>
                            </div>
                            <div id='calendar'></div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>


<div class="modal inmodal in" id="infoModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <i class="fa fa-calendar-o modal-icon"></i>
                <h4 class="modal-title">Detalles de reserva</h4>
                <small>La siguiente reserva tiene como estado: Confirmada!</small>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-3" id="Labelc">Sala:</label>
                            <div class="col-md-8" id="LabelP">
                                <div class='input-group' id='datepicker'>
                                    <input type='text' class="form-control" id="numSala" readonly="readonly" />
                                </div>
                                <input type="hidden" id="idSala" value="">
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-3" for="usuario_registro">Realizó la reserva:</label>
                            <div class='input-group col-md-8'>
                                <input type='text' class="form-control" id="usuario_registro" readonly="readonly" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-3" for="ejecutivo">Ejecutivo:</label>
                            <div class='input-group col-md-8'>
                                <input type='text' class="form-control" id="ejecutivo" readonly="readonly" />
                            </div>
                        </div>
                    </div>
                    <br>
                    <h4>Horario del bloque</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <label class="col-md-3" id="Labelc">Inicio reserva:</label>
                            <div class="col-md-8 input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                <input id="horario_inicio" value="" class="form-control" size="16" type="text" readonly="readonly">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-3" id="Labelc">Término reserva:</label>
                            <div class="col-md-8 input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                <input id="horario_termino" value="" class="form-control" size="16" type="text" readonly="readonly">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                        </div>
                        <br><br><br>
                    </div>
                    <h4>Datos de la reserva</h4>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nombre de la reserva:</label>
                        <div class="col-sm-4">
                            <textarea id="nombre_reserva" readonly cols="80" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Detalles Reserva:</label>
                        <div class="col-sm-10">
                            <textarea id="detalle_reserva" readonly cols="80" rows="4"></textarea>
                            <span class="help-block m-b-none"></span>
                        </div>
                        <label class="col-sm-2 control-label">Detalles del break:</label>
                        <div class="col-sm-10">
                            <textarea id="break" cols="80" readonly rows="4"></textarea>
                            <span class="help-block m-b-none"></span>
                        </div>
                        <label class="col-sm-2 control-label">Computadores:</label>
                        <div class="col-sm-10">
                            <textarea id="computadores" readonly cols="80" rows="4"></textarea>
                            <span class="help-block m-b-none"></span>
                              <span class="help-block m-b-none"></span>
                        </div>
                        <label class="col-sm-2 control-label">Cantidad de alumnos:</label>
                        <div class="col-sm-10">
                            <input id="cantalm"  type="text" readonly style="margin-top: 20px;"></input>
                            <span class="help-block m-b-none"></span>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Volver</button>
                </div>

            </div>
        </div>
    </div>
</div>

