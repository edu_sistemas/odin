<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <input type="hidden" value="<?php echo $id_ficha; ?>" id="id_ficha" name="id_ficha"/>
                    <h5>Observaciones</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" style="padding: 20px !important;">
                        <table id="tbl_observaciones" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr> 
                                    <th>F.Registro</th>
                                    <th>Usuario</th>
                                    <th>T.Observación</th>  
                                    <th>Observaciones</th>  
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>F.Registro</th>
                                    <th>Usuario</th>
                                    <th>T.Observación </th>  
                                    <th>Observaciones</th> 
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiddens Modals para mostrar contactos -->



</div>
