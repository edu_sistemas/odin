<?php
/**
 * OrdenFacturacionDetalle_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2017-01-22 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>

<div class="row">

    <div class="ibox float-e-margins">
        <div class="ibox-title">

            <h5 id="h_text_title_ficha">Resumen</h5>


            <div class="ibox-tools">

            </div>
        </div>
        <div class="ibox-content">
            <form method="" class="form-horizontal">
                <input type="hidden" id="id_edutecno_original" name="id_edutecno_original" value="<?php echo $data_ficha[0]['id_edutecno']; ?>">
                <input type="hidden" id="id_ficha" name="id_ficha" value="<?php echo $data_ficha[0]['id_ficha']; ?>">
                <input type="hidden" id="num_ficha" name="num_ficha" value="<?php echo $data_ficha[0]['num_ficha']; ?>">
                <p> Datos de la Venta</p> 
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label"><strong>N° de Ficha:</strong></label>
                        <a href="../../../../Timeline/Timeline/index/<?php echo $data_ficha[0]['id_ficha']; ?>" target="_blank"> 
                            <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['num_ficha']; ?>">
                        </a> 
                    </div>
                    <div class="col-md-4">
                        <label class="  control-label">Tipo de Venta:</label>
                        <a href="../../../../Timeline/Timeline/index/<?php echo $data_ficha[0]['id_ficha']; ?>" target="_blank"> 
                            <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['categoria']; ?>">
                        </a> 
                    </div>
                    <div class="col-md-4">
                        <label class="  control-label"><strong>Estado:</strong> </label>
                        <a href="../../../../Timeline/Timeline/index/<?php echo $data_ficha[0]['id_ficha']; ?>" target="_blank"> 
                            <input type="text" class="form-control" readonly=""  value="<?php echo $data_ficha[0]['estado']; ?>">
                        </a> 
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <p> Datos de la Empresa</p> 
                <div class="row">
                    <div class="col-md-6">
                        <label class="control-label">Holding</label>
                        <input type="text" class="form-control" readonly="" id="holding"
                               value="<?php echo $data_ficha[0]['holding']; ?>">
                    </div>
                    <div class="col-md-6">
                        <label class="  control-label">Empresa</label>

                        <input type="text" class="form-control" readonly=""
                               id="id_empresa"
                               value="<?php echo $data_ficha[0]['rut_empresa'] . " / " . $data_ficha[0]['empresa']; ?>"/>
                    </div>
                </div>

                <div class="row">

                    <div class="col-lg-3">
                        <label class="control-label">Dirección Empresa</label>
                        <input type="text" class="form-control" readonly=""
                               id="direccion_empresa"
                               value="<?php echo $data_ficha[0]['direccion_empresa']; ?>">

                    </div>

                    <div class="col-lg-3">
                        <label class="control-label">Nombre Contacto</label>

                        <input type="text" class="form-control" readonly=""
                               id="nombre_contacto_empresa"
                               value="<?php echo $data_ficha[0]['nombre_contacto']; ?>">
                    </div>

                    <div class="col-lg-3">
                        <label class="control-label">Teléfono Contacto</label>

                        <input type="text" class="form-control" readonly=""
                               id="telefono_contacto_empresa"
                               value="<?php echo $data_ficha[0]['telefono_contacto']; ?>">
                    </div>                        

                    <div class="col-lg-3">
                        <label class="control-label">Dirección envío Diplomas</label>

                        <input type="text" class="form-control" readonly=""
                               id="lugar_entrega_diplomas"
                               value="<?php echo $data_ficha[0]['direccion_diplomas']; ?>">
                    </div>   
                </div> 

                <div class="hr-line-dashed"></div>
                <p>Datos de la OTIC</p> 

                <div class="row">


                    <div class="col-lg-4">
                        <label class="  control-label">Otic</label>
                        <input type="text" class="form-control" readonly="" 
                               value="<?php echo $data_ficha[0]['otic']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class=" control-label">Código Sence</label>
                        <input type="text" class="form-control" readonly=""

                               value="<?php echo $data_ficha[0]['codigo_sence']; ?>">
                    </div>
                    <div class="col-md-4">
                        <label class=" control-label">N° Orden Compra</label>
                        <input type="text" class="form-control" readonly=""

                               value="<?php echo $data_ficha[0]['num_orden_compra']; ?>">
                    </div>


                </div>
                <div class="hr-line-dashed"> </div>
                <p>Datos del Curso</p>  
                <div class="row"> 
                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Modalidad</label>
                        <input type="text" class="form-control" readonly=""
                               id="modalidad"
                               value="<?php echo $data_ficha[0]['modalidad']; ?>">
                    </div>

                    <div class="col-md-4">
                        <label class="col-md-1 control-label">Curso</label>
                        <input type="text" class="form-control" readonly=""  id="curso_descrip" value="<?php echo $data_ficha[0]['descripcion_producto']; ?>">
                    </div>

                    <div class="col-md-4"> 
                        <label class="col-md-1 control-label">Versión</label>
                        <input type="text" class="form-control" readonly=""  id="version"  value="<?php echo $data_ficha[0]['version']; ?>">
                    </div>
                </div>

                <div class="row"> 
                    <div class="col-md-6">
                        <label class=" control-label">Nombre Curso Sence</label>
                        <input type="text" class="form-control" readonly=""
                               id="modalidad"
                               value="<?php echo $data_ficha[0]['nombre_curso_sence']; ?>">
                    </div>




                </div>
                <br>
                <div class="row">
                    <div class="col-lg-6">
                        <label class=" control-label">Fecha Inicio y Término</label>

                        <div class="input-daterange input-group  col-lg-12 ">

                            <input type="text" readonly="" class="form-control"
                                   name="fecha_inicio" id="fecha_inicio" placeholder="AAAA-MM-DD"
                                   value="<?php echo $data_ficha[0]['fecha_inicio']; ?>"
                                   requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                            <span class="input-group-addon">A</span> 
                            <input type="text"  class="form-control" name="fecha_termino" id="fecha_termino"
                                   placeholder="AAAA-MM-DD" value="<?php echo $data_ficha[0]['fecha_fin']; ?>" readonly=""
                                   requerido="true" mensaje="Fecha Término es un campo requerido" />
                        </div>
                    </div>
                    <div class="col-lg-6">

                        <div class="col-md-6 ">
                            <label class="  control-label">Hora Inicio</label>
                            <div class="input-group clockpicker">
                                <input type="time" class="form-control" id="txt_hora_inicio"
                                       name="txt_hora_inicio"
                                       value="<?php echo $data_ficha[0]['hora_inicio']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Inicio" readonly="" />
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6 ">
                            <label class=" control-label">Hora Término</label>
                            <div class="input-group clockpicker" data-autoclose="true">
                                <input type="time" class="form-control" id="txt_hora_termino"
                                       name="txt_hora_termino"
                                       value="<?php echo $data_ficha[0]['hora_termino']; ?>"
                                       requerido="true" mensaje="Ingresar Hora Término" readonly="">
                                <span class="input-group-addon"> <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="hr-line-dashed"> </div>
                <p>Lugar de Ejecución</p>  

                <div class="row">
                    <div class="col-lg-6"> 
                        <label class=" control-label">Sede</label>
                        <input type="text" class="form-control" readonly="" id="id_sede"
                               value="<?php echo $data_ficha[0]['sede']; ?>">

                    </div>
                    <div class="col-lg-6">
                        <label class=" control-label">Sala</label>
                        <input type="text" class="form-control" readonly="" id="id_sala"
                               value="<?php echo $data_ficha[0]['sala']; ?>">
                    </div>

                </div>
                <div class="row">


                    <div class="col-lg-6">
                        <label class="control-label">Dirección o Lugar Ejecución</label>
                        <input id="lugar_ejecucion" name="lugar_ejecucion" class="form-control" readonly="" 
                               value="<?php echo $data_ficha[0]['lugar_ejecucion']; ?>" 
                               />
                    </div>


                    <input  type="hidden" id="id_dias" value="<?php echo $data_ficha[0]['id_dias']; ?>">
                    <div class="col-lg-6">
                        <label class="control-label">Días</label>
                        <label class="checkbox-inline" for="inlineCheckbox1"> <input
                                type="checkbox" readonly="" id="lunes" name="lunes" value="1"
                                title="Lunes"> Lu
                        </label> <label class="checkbox-inline" for="inlineCheckbox2"> <input
                                type="checkbox" readonly="" id="martes" name="martes" value="2"
                                title="Martes"> Ma
                        </label> <label class="checkbox-inline" for="inlineCheckbox3"> <input
                                type="checkbox" readonly="" id="miercoles" name="miercoles"
                                value="3" title="Miércoles"> Mi
                        </label> <label class="checkbox-inline" for="inlineCheckbox4"> <input
                                type="checkbox" readonly="" id="jueves" name="jueves" value="4"
                                title="Jueves"> Ju
                        </label> <label class="checkbox-inline" for="inlineCheckbox5"> <input
                                type="checkbox" readonly="" id="viernes" name="viernes"
                                value="5" title="Viernes"> Vi
                        </label> <label class="checkbox-inline" for="inlineCheckbox6"> <input
                                type="checkbox" readonly="" id="sabado" name="sabado" value="6"
                                title="Sábado"> Sa
                        </label> <label class="checkbox-inline" for="inlineCheckbox7"> <input
                                type="checkbox" readonly="" id="domingo" name="domingo"
                                value="7" title="Domingo"> Do
                        </label> 
                    </div>
                </div>


                <div class="hr-line-dashed">   </div>
                <p>Información Interna</p> 

                <div class="row">


                    <div class="col-md-6">
                        <label class="control-label">Ejecutivo Comercial</label>
                        <input type="text" class="form-control" readonly=""
                               id="id_ejecutivo"
                               value="<?php echo $data_ficha[0]['ejecutivo']; ?>">
                    </div>

                    <div class="col-lg-6">
                        <label class="control-label">N° Solicitud Tablet</label>
                        <input id="id_solicitd_tablet" name="id_solicitd_tablet" onkeypress="return solonumero(event)" class="form-control" 
                               requerido="false" mensaje="Debe ingresar N° Solicitud tablet" maxlength="8"
                               disabled="disabled"
                               value="<?php echo $data_ficha[0]['id_solicitud_tablet']; ?>"/>
                    </div>                   

                    <?php
                    $fichas_relacionada = $data_ficha[0]['fichas_relacionada'];

                    if ($fichas_relacionada == 'No tiene.') {
                        ?>

                        <div class = "col-lg-12">
                            <label class = "  control-label" for = "textarea">Observaciones</label>
                            <textarea readonly = "" class = "form-control" id = "comentario"
                                      name = "comentario" maxlength = "1000"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                            <p id="text-out">0/1000 caracteres restantes</p>
                        </div>
                    <?php } else { ?>
                        <div class = "col-lg-6">
                            <label class = "control-label" for = "textarea">Observaciones</label>
                            <textarea readonly = "" class = "form-control" id = "comentario"
                                      name = "comentario" maxlength = "1000"><?php echo $data_ficha[0]['comentario_orden_compra']; ?></textarea>
                            <p id="text-out">0/1000 caracteres restantes</p>
                        </div>
                        <div class="col-lg-6">

                            <br>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    Fichas Relacionadas
                                </div>
                                <div class="panel-body">
                                    <?php
                                    $fichas_relacionada = $data_ficha[0]['fichas_relacionada'];
                                    $array_fichas_relacionadas = explode("|", $fichas_relacionada);
                                    $id_arra_fichas_relacionadas = $array_fichas_relacionadas[0];
                                    $num_arra_fichas_relacionadas = $array_fichas_relacionadas[1];
                                    $ids_ficha = explode(",", $id_arra_fichas_relacionadas);
                                    $nums_ficha = explode(",", $num_arra_fichas_relacionadas);
                                    for ($i = 0; $i < count($ids_ficha); $i++) {
                                        if ($data_ficha[0]['id_ficha'] == $ids_ficha[$i]) {
                                            ?> 
                                            <button type="button" class="btn btn-outline btn-success dim" type="button">  <?php echo $nums_ficha[$i] ?>
                                                <i class="fa fa-money"></i> 
                                            </button> 

                                        <?php } else {
                                            ?>
                                            <a href="<?php echo $ids_ficha[$i] ?>"  target="_blank">   
                                                <button type="button" class="btn btn-outline btn-primary dim" type="button">  <?php echo $nums_ficha[$i] ?>
                                                    <i class="fa fa-money"></i> 
                                                </button>
                                            </a>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div> 
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </form>

            <div class="row">

                <div class="col-md-6">
                    <label class=" control-label" style="text-align: right;">Empresa Edutecno que Factura:</label>
                    <select id="edutecno_cbx" name="edutecno_cbx"  class="select2_demo_3 form-control" disabled=""></select>
                </div>
                <div class="col-md-6">
                    <label class=" control-label" style="text-align: right;">Centro de Costo:</label>
                    <input type="text" class="form-control" readonly=""
                           id="id_ejecutivo"
                           value="<?php echo $data_ficha[0]['centro_costo']; ?>">

                </div>

            </div>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">


                <div class="col-md-6">

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cbx_orden_compra">Orden
                            Compra</label>
                        <div class="col-md-4">
                            <select id="cbx_orden_compra" name="cbx_orden_compra"
                                    class="form-control" requerido="true"
                                    mensaje="Debe seleccionar Orden de Compra">

                                <?php for ($i = 0; $i < count($data_orden_compra); $i++) { ?>
                                    <option
                                        value="<?php echo $data_orden_compra[$i]['id'] ?>"><?php echo $data_orden_compra[$i]['text'] ?></option>
                                        <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div>
                </div>
                Seleccione Orden de compra para ver infomación adicional sobre el Cliente
            </div>
        </div>
    </div>
</div>


<div class="row">

    <div class="ibox float-e-margins">
        <div class="ibox-title">

            <h5 id="h_text_title_ficha">Datos de Factura</h5>


            <div class="ibox-tools">

            </div>
        </div>
        <div class="ibox-content">



            <div class="row">
                <div class="col-lg-4">
                    <label class="control-label">Razón social</label>
                    <input type="text" class="form-control" readonly=""
                           id="razaon_social_factura"
                           value="">
                    <a id="link_empresa1" href="#" target="_blank"><i class="fa fa-eye"></i></a>
                </div>

                <div class="col-md-4">
                    <label class="control-label">RUT</label>
                    <input type="text" class="form-control" readonly="" id="rut_empresa_factura"
                           value="">
                    <a id="link_empresa2" href="#" target="_blank"><i class="fa fa-eye"></i></a>
                </div>

                <div class="col-md-4">
                    <label class="control-label">Giro o actividad del cliente</label>

                    <input type="text" class="form-control" readonly=""
                           id="giro_empresa_factura"
                           value=""/>
                    <a id="link_empresa3" href="#" target="_blank"><i class="fa fa-eye"></i></a>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-6">
                    <label class="control-label">OTIC</label>
                    <input type="text" class="form-control" readonly=""
                           id="razon_social_otic_factura"
                           value="">

                </div>

                <div class="col-md-6">
                    <label class="control-label">RUT</label>
                    <input type="text" class="form-control" readonly="" id="rut_otic_factura"
                           value="">

                </div>



            </div>

            <div class="row">
                <div class="col-lg-6">
                    <label class="control-label">Comuna</label>
                    <input type="text" class="form-control" readonly=""
                           id="comuna_empresa_factura"
                           value="">

                </div>

                <div class="col-md-6">
                    <label class="control-label">Dirección de factura</label>
                    <input type="text" class="form-control" readonly="" id="direccion_factura_factura"
                           value="">
                </div>



            </div>

            <div class="row">
                <div class="col-md-6">
                    <label class=" control-label">Sence NET</label>
                    <input type="text" class="form-control" readonly=""
                           id="sence_net_factura"
                           value="">
                </div>

                <div class="col-md-6">
                    <label class="control-label">O.C</label>
                    <input type="text" class="form-control" readonly="" id="oc_factura"
                           value="">
                </div>



            </div>

            <div class="row"> 

                <div class="col-lg-3">
                    <label class="control-label">O.C. OTIC</label>

                    <input type="text" class="form-control" readonly=""
                           id="oc_otic_empresa_factura"
                           value="">
                </div>

                <div class="col-lg-3">
                    <label class="control-label">O.C. Interna</label>

                    <input type="text" class="form-control" readonly=""
                           id="oc_interna_empresa_factura"
                           value="">
                </div>                        

                <div class="col-lg-3">
                    <label class="control-label">OTA</label>

                    <input type="text" class="form-control" readonly=""
                           id="ota_empresa_factura"
                           value="">
                </div>   
                <div class="col-lg-3">
                    <label class="control-label">HES</label>

                    <input type="text" class="form-control" readonly=""
                           id="hes_empresa_factura"
                           value="">
                </div>   

                <div class="col-lg-3">
                    <label class="control-label">CUI</label>

                    <input type="text" class="form-control" readonly=""
                           id="cui_empresa_factura"
                           value="">
                </div>
            </div> 

            <div class="hr-line-dashed">   </div>
            <p>Valores</p> 
            <div class="row">
                <div class="col-lg-3">
                    <label class="control-label">Monto OTIC</label>
                    <input type="text" class="form-control" readonly=""
                           id="monto_otic_factura"
                           value="">
                </div>
                <div class="col-lg-3">
                    <label class="control-label">Monto Empresa</label>

                    <input type="text" class="form-control" readonly=""
                           id="monto_empresa_factura"
                           value="">
                </div>
                <div class="col-lg-3">
                    <label class="control-label">Monto Total</label>
                    <input type="text" class="form-control" readonly=""
                           id="monto_total_oc"
                           value="">
                </div>  
                <div class="col-lg-3">
                    <label class="control-label">Tercera Factura</label>

                    <input type="text" class="form-control" readonly=""
                           id="tercera_factura_factura"
                           value="">
                </div>   
            </div> 

            <div class="row">
                <div class="col-lg-3">
                    <label class="control-label">Monto Facturado OTIC</label>
                    <input type="text" class="form-control" readonly=""
                           id="monto_otic_factura_facturado"
                           value="">
                </div>
                <div class="col-lg-3">
                    <label class="control-label">Monto Facturado Empresa</label>

                    <input type="text" class="form-control" readonly=""
                           id="monto_empresa_factura_facturado"
                           value="">
                </div>
                <div class="col-lg-3">
                    <label class="control-label">Monto Total Facturado</label>
                    <input type="text" class="form-control" readonly=""
                           id="monto_total_factura_facturado"
                           value="">
                </div>  
                <div class="col-lg-3">
                    <label class="control-label">Monto Pendiente Facturar</label>
                    <input type="text" class="form-control" readonly=""
                           id="monto_total_factura_pendiente_facturar"
                           value="">
                </div>  
            </div> 
            <div class="row">
                <div class="col-lg-3">

                </div>
                <div class="col-lg-3">
                    <button type="button" id="problemasfactura" class="btn btn-w-m btn-danger" ><i class="fa fa-close"></i> Problemas</button>
                </div>
                <div class="col-lg-3">
                    <button type="button" id="todookfactura" class="btn btn-w-m btn-success" ><i class="fa fa-check"></i> OK</button>
                </div>  

                <div class="col-lg-3">

                    <button type="button" id="agregaFactura" class="btn btn-w-m btn-primary" ><i class="fa fa-money"></i> Agregar Factura</button>



                </div>  
            </div> 

        </div>
        <div class="ibox-title">

            <h5 id="h_text_title_ficha">Documentos</h5>


            <div class="ibox-tools">

            </div>
        </div>
        <div class="ibox-content">

            <table id="tbl_orden_compra_documetnos"
                   class="table table-striped table-bordered table-hover dataTables-example">
                <thead>
                    <tr>
                        <th>Tipo Documento</th>
                        <th>Documento</th>
                        <th>Fecha Adjuntado </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Tipo Documento</th>
                        <th>Documento</th>
                        <th>Fecha Adjuntado </th>
                    </tr>
                </tfoot>
            </table>

        </div>


        <div class="ibox-title">

            <h5 id="h_text_title_ficha">Observaciones</h5>


            <div class="ibox-tools">

            </div>
        </div>
        <div class="ibox-content">

            <table id="tbl_observaciones_fichas"
                   class="table table-striped table-bordered table-hover dataTables-example">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Observación</th>
                        <th>Fecha Registro </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Usuario</th>
                        <th>Observación</th>
                        <th>Fecha Registro </th>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>




</div>

<div class="modal inmodal" id="modal_observacion_problemas_con_ficha" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h4 id="facturacion_texto">Observaciones:</h4>
            </div>
            <div class="modal-body">
                <form class="form" id="form_facturacion_observacion" name="form_facturacion_observacion">
                    <label for="sdc_comentario">Observación:</label>
                    <textarea class="form-control" name="facturacion_observacion_problemas" id="facturacion_observacion_problemas" cols="30" rows="10" requerido="true" mensaje="Debe ingresar observacion"></textarea>
                    <br>
                    <button type="button" id="btn_guardar_observacion_problemas_facturacion" class="btn btn-success">Guardar</button>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
