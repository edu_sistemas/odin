<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2018-05-16 [Veronica Morales] <vmorales@edutecno.com>
 */
?>
<div class=" ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h4>Filtrar Pendientes</h4>
                </div>
                <div class="ibox-content">
                    <form role="frm" class="form-inline" id="frm" name="frm">

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="p_ano">Año</label><br>
                                  <select name="p_ano" id="p_ano"  class="select2_demo_3 form-control" >
                                        <?php  
                                        for($i=$year;$i>=2016;$i--) { 
                                            echo '<option value="'.$i.'">'.$i.'</option>'; 
                                        } 
                                        ?>
                                        <option value="">Todos</option>
                                 </select>
                                <br>
                            </div>
                        </div>
                        
                    </form> 
                </div>
            </div>
        </div>
    </div>
<div class=" ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Facturas Pendientes</h5>
                </div>
                <div class="ibox-content">
                 <form role="form" class="form-inline" id="frm" name="frm">
                    <div id="loading-container" class="text-center"></div>
                        <div class="table-responsive" style="padding: 20px !important;">


                    <div class="row">
                         <table id="tbl_facturas_pendientes" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <tr>
                                        <th>Ficha</th>
                                        <th>OTEC</th>                                    
                                        <th>Orden Compra</th>
                                        <th>Empresa</th>
                                        <th>Total OTIC</th>
                                        <th>M. Fact. OTIC</th>
                                        <th>M. Pend. OTIC</th>
                                        <th>Total Empresa</th>
                                        <th>M. Fact. Empresa</th>
                                        <th>M. Pend Empresa</th>
                                        <th>Total Ficha</th>
                                        <th>Monto Facturado</th>
                                        <th>Monto Pendiente</th>
                                        <th>F. Inicio</th>
                                        <th>F. Termino</th>
                                        <th>Ejecutivo</th>
                                        <th>Dias Transcurrido</th> 
                                        <th>Estado</th>

                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Ficha</th>
                                        <th>OTEC</th>                                    
                                        <th>Orden Compra</th>
                                        <th>Empresa</th>
                                        <th>Total OTIC</th>
                                        <th>M. Fact. OTIC</th>
                                        <th>M. Pend. OTIC</th>
                                        <th>Total Empresa</th>
                                        <th>M. Fact. Empresa</th>
                                        <th>M. Pend Empresa</th>
                                        <th>Total Ficha</th>
                                        <th>Monto Facturado</th>
                                        <th>Monto Pendiente</th>
                                        <th>F. Inicio</th>
                                        <th>F. Termino</th>
                                        <th>Ejecutivo</th>
                                        <th>Dias Transcurrido</th> 
                                        <th>Estado</th>
                                    </tr>
                                </tfoot>
                            </table>
                    </div>
               
                </div>
            </div>
        </div>
    </div>
   
</div>