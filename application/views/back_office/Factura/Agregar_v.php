<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nueva Factura</h5>
                <div class="ibox-tools"></div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_factura_registro">
                    <p>Ingresar una nueva factura</p>

                    <input type="text" id="id_orden_compra" name="id_orden_compra"
                           value="<?php echo $data_factura[0]['id_orden_compra']; ?>" hidden>




                    <div class="form-group">
                        <label class="col-lg-2 control-label">FICHA: </label>
                        <div class="input-group col-md-3">
                            <input type="text" id="" name=""
                                   value="<?php echo $data_factura[0]['ficha']; ?>" readonly="readonly" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">N° ORDEN DE COMPRA: </label>
                        <div class="input-group col-md-3">
                            <input type="text" id="" name=""
                                   value="<?php echo $data_factura[0]['num_orden_compra']; ?>"  readonly="readonly">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Tipo Factura: </label>
                        <div class="input-group col-md-3">
                            <select id="tipo_factura" name="tipo_factura"  class="select2_demo_3 form-control"
                                    requerido="true" mensaje="Debe seleccionar Tipo Factura"
                                    accesskey>
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">FACTURA N°:</label>
                        <div class="input-group col-lg-3">
                            <input type="text" class="form-control required" id="n_factura"
                                   requerido="true" mensaje="Debe Ingresar el n° de factura"
                                   placeholder="" name="n_factura" class="form-control">
                        </div>



                    </div>

                    <!--                    <div class="form-group">
                                            <label class="col-lg-2 control-label">Estado Factura: </label>
                                            <div class="input-group col-md-3">
                                                <select id="estado_factura" name="estado_factura"  class="form-control" requerido="true" mensaje="Debe seleccionar Estado Factura" >
                                                </select>
                                            </div>
                                        </div>-->


                    <!--                    <div class="form-group">
                                            <label class="col-lg-2 control-label"></label>
                                            <div class="col-lg-3">
                                                <p id="out_print" style="color: red"></p>
                                            </div>
                                        </div>
                    -->
                    <div class="form-group" id="data_1">

                        <label class="col-lg-2 control-label">Fecha Emisión:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" id="fecha_emision" name="fecha_emision"
                                   placeholder="02-01-2017" requerido="true"
                                   mensaje="Debe seleccionar una fecha">
                        </div>
                    </div>

                    <div class="form-group" id="data_1">
                        <label class="col-lg-2 control-label">Fecha de Vencimiento:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" id="fecha_vencimiento" name="fecha_vencimiento"
                                   placeholder="02-01-2017" requerido="true"
                                   mensaje="Debe seleccionar una fecha">
                        </div>

                    </div>

                    <!--                    <div class="form-group">
                    
                                            <label class="col-lg-2 control-label">Adjuntar Factura:</label>
                                            <div class="col-md-5" id="LabelP">
                                                <div class="fileinput fileinput-new input-group"
                                                     data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput">
                                                        <i class="glyphicon glyphicon-file fileinput-exists"></i> <span
                                                            class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-addon btn btn-default btn-file"> <span
                                                            class="fileinput-new">Seleccionar archivo</span> <span
                                                            class="fileinput-exists">Cambiar</span> <input type="file"
                                                            name="file" id="file" requerido="true"
                                                            mensaje="Debe subir un archivo" />
                                                    </span> <a href="#"
                                                               class="input-group-addon btn btn-default fileinput-exists"
                                                               data-dismiss="fileinput">Quitar</a>
                                                </div>
                                            </div>
                                        </div>-->
                    <!--                    <div class="form-group">
                                            <label class="col-lg-2 control-label">Tipo de Entrega:</label>
                                            <div class="input-group date col-lg-3">							
                                                <select class="form-control" name="tipo_entrega" id="tipo_entrega"
                                                        requerido="true" mensaje="Debe seleccionar un tipo de entrega">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>-->

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Valor:</label>
                        <div class="input-group  col-lg-3">

                            <input type="text" class="form-control required"
                                   id="monto_factura" requerido="true"
                                   mensaje="Debe Ingresar el valor" maxlength="12" placeholder=""
                                   name="monto_factura" class="form-control" onkeypress="return solonumero(event)">
                            <label class="col-lg-offset-10">Saldo:  <?php echo $data_factura[0]['saldo']; ?> </label>
                        </div>



                    </div>



                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
