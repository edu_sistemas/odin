<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>

<div class=" ">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">

                        <div class="row">
                            <label for="datepicker">Ingrese un rango de fechas</label>
                            <div id="data_5">
                                <div class="input-daterange input-group" id="datepicker" style="width: 100%;">
                                    <input type="text" class="input-sm form-control" id="start" name="start" placeholder="Fecha Inicio" value="" readonly=""/>
                                    <span class="input-group-addon">hasta</span>
                                    <input type="text" class="input-sm form-control" id="end" name="end" placeholder="Fecha Fin" value="" readonly="" />
                                </div>
                            </div>
                            <br>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="holding">Holding</label><br>
                                <select id="holding" name="holding"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="empresa">Empresa</label><br>
                                <select id="empresa" name="empresa"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <label for="num_ficha">N° Ficha</label><br>
                                <input id="num_ficha" name="num_ficha" onkeypress="return solonumero(event)" maxlength="8"  class="select2_demo_3 form-control" style="width: 100%"/>

                            </div>
                            <div class="col-lg-6">
                                <label for="holding">Ejecutivo</label><br>
                                <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-lg-6 pull-right">
                                <br>
                                <button type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                                <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Fichas Pendientes</h5>
                </div>
                <div class="ibox-content">
                <div id="loading-container" class="text-center"></div>
                    <div class="table-responsive" style="padding: 20px !important;">


                        <table id="tbl_fichas" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Acciones</th>
                                    <th>Ficha</th>
                                    <th>Consolidado</th>                                    
                                    <th>Tipo</th>
                                    <th>Ejec.</th>
                                    <th>OC</th>
                                    <th>ID SENCE</th>
                                    <th>Empresa</th>
                                    <th>Otic</th>
                                    <th>Producto</th>
                                    <th title="Fecha de finalización del curso">F. Termino</th>
                                    <th>Restantes</th>
                                    <th>Transcurrido</th>
                                    <th>Gestiones#</th>
                                    <th>Estado Ficha</th>
                                    <th>Estado Finanzas</th>
                                    <th>OBS.</th>
                                    <th>Monto Facturado</th> 
                                    <th>Total O.C</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Acciones</th>
                                    <th>Ficha</th>
                                    <th>Consolidado</th>
                                    <th>Tipo</th>
                                    <th>Ejec.</th>
                                    <th>OC</th>
                                    <th>ID SENCE</th>
                                    <th>Empresa</th>
                                    <th>Otic</th>
                                    <th>Producto</th>
                                    <th title="Fecha de finalización del curso">F. Termino</th>
                                    <th>Restantes</th>
                                    <th>Transcurrido</th>
                                    <th>Gestiones#</th>
                                    <th>Estado Ficha</th>
                                    <th>Estado Finanzas</th>
                                    <th>OBS.</th>
                                    <th>Monto Facturado</th> 
                                    <th>Total O.C</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiddens Modals para mostrar contactos -->

    <!-- 	FACTURAS -->

    <div class="modal inmodal" id="modal_facturas" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Facturas relacionadas a la orden de compra:</h4>
                    <h4 id="oc_modal"></h4>
                    <input type="hidden" id="id_oc_hidden">
                </div>
                <div class="modal-body">
                    <div id="listaFac">
                        <table id="tbl_documentos_factura" class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>N° De Factura</th>
                                    <th>Fecha de Ingreso</th>
                                    <th>Monto Total</th>
                                    <th>Tipo Entrega</th>
                                    <th>Fecha Entrega</th>
                                    <th>Estado Factura</th>
                                    <th>Entrega</th>

                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>N° De Factura</th>
                                    <th>Fecha de Ingreso</th>
                                    <th>Monto Total</th>
                                    <th>Tipo Entrega</th>
                                    <th>Fecha Entrega</th>
                                    <th>Estado Factura</th>
                                    <th>Entrega</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="row" id="formfac" style="display:none">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Entrega de Factura <small>Seleccione el tipo de entrega.</small></h5>                                    
                                </div>
                                <div class="ibox-content">
                                    <form method="get" class="form-horizontal">
                                        <div class="form-group"><label class="col-sm-2 control-label">Tipo de Entrega</label>

                                            <div class="col-sm-10">
                                                <select id="tipo_entrega_f" class="form-control">                                                	
                                                </select>
                                                <input type="hidden" id="id_fact" >
                                            </div>

                                        </div>                                        
                                        <div class="hr-line-dashed"></div>

                                        <div class="col-sm-4 col-sm-offset-2"></div>
                                    </form>  
                                </div>

                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white" onclick="cancelarForm()">Cancelar</button>
                                    <button class="btn btn-primary"  onclick="enviarForm()">Guardar Cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- 	DOCUMENTOS RECTIFICACION -->

    <div class="modal inmodal" id="modal_documentos_rectificacion" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Documentos relacionadas a la Orden de Compra: <ocdoc></ocdoc></h4>
                </div>
                <div class="modal-body">
                    <table id="tbl_documentos_rectificacion"
                           class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre Documento</th>
                                <th>Fecha Subida</th>
                                <th>Archivo</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre Documento</th>
                                <th>Fecha Subida</th>
                                <th>Archivo</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- 	NOTAS DE CREDITO -->


    <div class="modal inmodal" id="modal_nota_credito" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Notas de Crédito relacionadas a la orden de compra:</h4>
                    <h4 id="oc_modal_credito"></h4>
                    <input type="hidden" id="id_oc_hidden">
                </div>
                <div class="modal-body">


                    <div id="sec_tabla">
                        <button class="btn btn-success" id="btn_nueva_nota_credito">Nueva
                            Nota de Crédito</button>
                        <table style="margin-top: 10px;" id="tbl_credito"
                               class="table table-hover table-bordered">
                            <thead>
                                <tr style="text-align: center">
                                    <th>#</th> 
                                    <th>Factura N°</th>
                                    <th>N. de C.</th>
                                    <th>Valor</th>                                    
                                    <th>Tipo Factura</th>
                                    <th>Entregado</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>


                    <div id="sec_nuevo">
                        <h2>Nueva Nota de Crédito</h2>
                        <form id="frm_nota_credito" class="form-horizontal">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">N° Factura</label> <br><select
                                        id="n_factura_cbx" name="n_factura_cbx"  class="form-control"></select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">N° Nota de Crédito</label><br>
                                    <input type="text" id="num_nota_credito" name="num_nota_credito"  class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">Fecha Emisión</label><br>
                                    <input type="text" id="fecha_emision" name="fecha_emision"  class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">Monto</label> <br>
                                    <input type="text" id="monto_credito" name="monto_credito" onkeypress="return solonumero(event)" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="tipo_entrega_cbx">Tipo de Entrega</label><br>
                                    <select id="tipo_entrega_nc" name="tipo_entrega_nc" class="form-control">                                                	
                                    </select>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>

                        </form>
                        <br>
                        <button id="btn_agregar_nota_credito" class="btn btn-success">Guardar</button>
                        <button id="btn_volver_modal_credito" class="btn btn-warning">Volver</button>
                    </div>

                    <div class="row" id="sec_update_tipo_entrega_nc" style="display:none">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Entrega de Nota de Credito <small>Seleccione el tipo de entrega.</small></h5>                                    
                                </div>
                                <div class="ibox-content">
                                    <form method="get" class="form-horizontal">
                                        <div class="form-group"><label class="col-sm-2 control-label">Tipo de Entrega</label>

                                            <div class="col-sm-10">
                                                <select id="tipo_entrega_nc_2" class="form-control">                                                	
                                                </select>
                                                <input type="hidden" id="id_nc" >
                                            </div>

                                        </div>                                        
                                        <div class="hr-line-dashed"></div>

                                        <div class="col-sm-4 col-sm-offset-2"></div>
                                    </form>  
                                </div>

                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white" onclick="cancelarFormNc()">Cancelar</button>
                                    <button class="btn btn-primary"  onclick="enviarFormNc()">Guardar Cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>                    




                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- 	NOTAS DE DEBITO -->
    <div class="modal inmodal" id="modal_nota_debito" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Notas de Débito relacionadas a la orden de compra:</h4>
                    <h4 id="oc_modal_debito"></h4>
                    <input type="hidden" id="id_oc_hidden_debito">
                </div>
                <div class="modal-body">


                    <div id="sec_tabla_debito">
                        <button class="btn btn-success" id="btn_nueva_nota_debito">Nueva
                            Nota de Débito</button>
                        <table style="margin-top: 10px;" id="tbl_debito"
                               class="table table-hover table-bordered">
                            <thead>
                                <tr style="text-align: center">
                                    <th>#</th>
                                    <th>Fecha Emisión</th>
                                    <th>Factura N°</th>
                                    <th>N. de C.</th>
                                    <th>Valor</th>
                                    <th>Tipo Factura</th>
                                    <th>Entregado</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>


                    <div id="sec_nuevo_debito">
                        <h2>Nueva Nota de Débito</h2>
                        <form id="frm_nota_debito" class="form-horizontal">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">N° Factura</label> <br><select
                                        id="n_factura_cbx_debito" name="n_factura_cbx_debito" class="form-control"></select>
                                </div>
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">N° Nota de Débito</label><br>
                                    <input type="text" id="num_nota_debito" name="num_nota_debito" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx_debito">Fecha Emisión</label><br>
                                    <input type="text" id="fecha_emision_debito" name="fecha_emision_debito" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">Monto</label> <br>
                                    <input type="text" id="monto_debito" name="monto_debito" onkeypress="return solonumero(event)" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="tipo_entrega_cbx">Tipo de Entrega</label><br>
                                    <select id="tipo_entrega_nd" name="tipo_entrega_nd" class="form-control">                                                	
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                        </form>
                        <br>
                        <button id="btn_agregar_nota_debito" class="btn btn-success">Guardar</button>
                        <button id="btn_volver_modal_debito" class="btn btn-warning">Volver</button>
                    </div>
                    <div class="row" id="sec_update_tipo_entrega_nd" style="display:none">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Entrega de Nota de Debito <small>Seleccione el tipo de entrega.</small></h5>                                    
                                </div>
                                <div class="ibox-content">
                                    <form method="get" class="form-horizontal">
                                        <div class="form-group"><label class="col-sm-2 control-label">Tipo de Entrega</label>

                                            <div class="col-sm-10">
                                                <select id="tipo_entrega_nd_2" class="form-control">                                                	
                                                </select>
                                                <input type="hidden" id="id_nd" >
                                            </div>

                                        </div>                                        
                                        <div class="hr-line-dashed"></div>

                                        <div class="col-sm-4 col-sm-offset-2"></div>
                                    </form>  
                                </div>

                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white" onclick="cancelarFormNd()">Cancelar</button>
                                    <button class="btn btn-primary"  onclick="enviarFormNd()">Guardar Cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- 	SOLICITUD DE CORRECIÓN -->

    <div class="modal inmodal" id="modal_solicitud_de_correccion" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Solicitud de corrección para la ficha: <numficha></numficha></h4>
                </div>
                <div class="modal-body">
                    <form class="form" id="form_sdc_correccion" name="form_sdc_correccion">
                        <input type="hidden" id="sdc_id_ficha" name="sdc_id_ficha" >
                        <input type="hidden" id="sdc_id_orden_compra" name="sdc_id_orden_compra" >
                        <input type="hidden" id="sdc_num_ficha" name="sdc_num_ficha" >
                        <label for="sdc_comentario">Comentarios:</label>
                        <textarea class="form-control" name="sdc_comentario" id="sdc_comentario" cols="30" rows="10" requerido="true" mensaje="Debe ingresar comentarios de corrección"></textarea>
                        <br>
                        <button type="button" id="btn_enviar_solicitud_de_correccion" class="btn btn-success">Enviar Solicitud</button>

                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- 	mi observacion -->

    <div class="modal inmodal" id="modal_mi_observacion" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4 id="mi_texto">Observación: </h4>
                </div>
                <div class="modal-body">
                    <form class="form" id="form_sdc_mi_observacion" name="form_sdc_mi_observacion">
                        <input type="hidden" id="mi_id_ficha" name="mi_id_ficha" >
                        <input type="hidden" id="mi_id_orden_compra" name="mi_id_orden_compra" >
                        <input type="hidden" id="mi_num_ficha" name="mi_num_ficha" >
                        <label for="sdc_comentario">Observación:</label>
                        <textarea class="form-control" name="mi_obserbacion" id="mi_obserbacion" cols="30" rows="10" requerido="true" mensaje="Debe ingresar observacion"></textarea>
                        <br>
                        <button type="button" id="btn_guardar_mi_observacion" class="btn btn-success">Guardar</button>

                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 	modal enviar a a facturar -->

    <div class="modal inmodal" id="modal_facturacion_observacion" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4 id="facturacion_texto">Observaciones: </h4>
                </div>
                <div class="modal-body">
                    <form class="form" id="form_facturacion_observacion" name="form_facturacion_observacion">
                        <input type="hidden" id="facturacion_id_ficha" name="facturacion_id_ficha" >
                        <input type="hidden" id="facturacion_id_orden_compra" name="facturacion_id_orden_compra" >
                        <input type="hidden" id="facturacion_num_ficha" name="facturacion_num_ficha" >
                        <label for="sdc_comentario">Observación:</label>
                        <textarea class="form-control" name="facturacion_observacion" id="facturacion_observacion" cols="30" rows="10" requerido="true" mensaje="Debe ingresar observacion"></textarea>
                        <br>
                        <button type="button" id="btn_guardar_facturacion_observacion" class="btn btn-success">Guardar</button>

                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 	modal id sence  -->

<div class="modal inmodal" id="modal_id_sence" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h4 id="texto_num_ficha">  </h4>

            </div>
            <div class="modal-body">
                <form class="form" id="form_save_id_sence" name="form_save_id_sence">

                    <input type="hidden" id="sence_id_orden_compra" name="sence_id_orden_compra" />
                    <input type="hidden" id="sence_num_ficha" name="sence_num_ficha" />
                    <input type="hidden" id="eliminar_id_sence" name="eliminar_id_sence" />

                    <label for="sdc_comentario">ID SENCE:</label>
                    <input type="text" id="id_sence" name="id_sence" class="form-control input-md" requerido="true" list="opciones_id_sence" autocomplete="off" 
                    mensaje="Debe ingresar ID SENCE" maxlength="50" placeholder="Ingrese o seleccione un ID sence" />
                        <datalist id="opciones_id_sence">
                        </datalist>
                    <br>
                    <button type="button" id="btn_guardar_ID_sence" class="btn btn-success">Guardar</button>
                    <button type="button" id="btn_eliminar_ID_sence" class="btn btn-danger btn-outline">Eliminar ID sence de la orden de compra</button>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- 	modal  num oc   -->

<div class="modal inmodal" id="modal_num_oc" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h4 id="texto_num_ficha_oc">  </h4>

            </div>
            <div class="modal-body">
                <form class="form" id="form_save_oc" name="form_save_oc">

                    <input type="hidden" id="oc_id_orden_compra" name="oc_id_orden_compra" />

                    <label for="sdc_comentario">N° OC:</label>
                    <input type="text" id="num_oc" name="num_oc" requerido="true" mensaje="Debe ingresar N° OC"/>

                    <br>
                    <button type="button" id="btn_guardar_num_oc" class="btn btn-success">Guardar</button>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>