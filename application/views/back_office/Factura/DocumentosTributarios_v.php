<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">
                  

                        <div class="row">
                        <div class="col-lg-6">
                                <label for="p_ano">Año</label><br>
                                  <select name="p_ano" id="p_ano"  class="select2_demo_3 form-control" >
                                        <?php  
                                        for($i=$year;$i>=2016;$i--) { 
                                            echo '<option value="'.$i.'">'.$i.'</option>'; 
                                        } 
                                        ?>
                                        <option value="">Todos</option>
                                 </select>
                                <br>
                            </div>
                            <div class="col-lg-6">
                                <label for="holding">Holding</label><br>
                                <select id="holding" name="holding"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                            
                        </div>

                        <div class="row">
                        <div class="col-lg-6">
                                <label for="num_ficha">N° Ficha</label><br>
                                <input id="num_ficha" name="num_ficha" onkeypress="return solonumero(event)" maxlength="8"  class="select2_demo_3 form-control" style="width: 100%"/>

                            </div>
                        <div class="col-lg-6">
                                <label for="empresa">Empresa</label><br>
                                <select id="empresa" name="empresa"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                           
                            <div class="col-lg-6">
                                <label for="holding">Ejecutivo</label><br>
                                <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control" style="width: 100%">
                                    <option></option>
                                </select> 
                                <br>
                            </div>
                             

                        </div>
                        <div class="row">

                            <div class="col-lg-6 pull-right">
                                <br>
                                <button type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                                <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Documentos Tributarios</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" style="padding: 20px !important;">


                        <table id="tbl_facturas" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>N° Ficha</th>
                                    <th>Imputable a:</th>
                                    <th>Factura Asociada</th>
                                    <th>Tipo Documento</th>
                                    <th>N° Documento</th>
                                    <th>Fecha Emisión</th>
                                    <th>Fecha Vencimiento</th>
                                    <th>M. Documento</th>
                                    <th>Estado</th>
                                    <th>T.Entrega</th>
                                    <th>Orden Compra</th>
                                    <th>Empresa</th>
                                    <th>Otic</th>  
                                    <th>Otec</th>  
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>N° Ficha</th>
                                   <th>Imputable a:</th>
                                    <th>Factura Asociada</th>
                                    <th>Tipo Documento</th>
                                    <th>N° Documento</th>
                                    <th>Fecha Emisión</th>
                                    <th>Fecha Vencimiento</th>
                                    <th>M. Documento</th>
                                    <th>Estado</th>
                                    <th>T.Entrega</th>
                                    <th>Orden Compra</th>
                                    <th>Empresa</th>
                                    <th>Otic</th>  
                                    <th>Otec</th>  
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiddens Modals para mostrar contactos -->

    <!-- 	DOCUMENTOS FICHA -->

    <div class="modal inmodal" id="modal_documentos_ficha" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Documentos relacionadas a la ficha: <fichadoc></fichadoc></h4>
                </div>
                <div class="modal-body">
                    <table id="tbl_documentos_factura"
                           class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre Documento</th>
                                <th>Fecha Subida</th>
                                <th>Archivo</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre Documento</th>
                                <th>Fecha Subida</th>
                                <th>Archivo</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- 	DOCUMENTOS RECTIFICACION -->

    <div class="modal inmodal" id="modal_documentos_rectificacion" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Documentos relacionadas a la Orden de Compra: <ocdoc></ocdoc></h4>
                </div>
                <div class="modal-body">
                    <table id="tbl_documentos_rectificacion"
                           class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre Documento</th>
                                <th>Fecha Subida</th>
                                <th>Archivo</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre Documento</th>
                                <th>Fecha Subida</th>
                                <th>Archivo</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- 	NOTAS DE CREDITO -->


    <div class="modal inmodal" id="modal_nota_credito" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Notas de Crédito relacionadas a la Factura:</h4>
                    <h4 id="fac_modal_credito"></h4>
                    <input type="hidden" id="id_fac_hidden">
                    <input type="text" id="n_factura_hidden" hidden>
                    <input type="text" id="id_factura_hidden" hidden>
                    
                </div>
                <div class="modal-body">


                    <div id="sec_tabla">
                        <button class="btn btn-success" id="btn_nueva_nota_credito">Nueva
                            Nota de Crédito</button>
                        <table style="margin-top: 10px;" id="tbl_credito"
                               class="table table-hover table-bordered">
                            <thead>
                                <tr style="text-align: center">
                                    <th>#</th>
                                    <th>Fecha Emisión</th>
                                    <th>Factura N°</th>
                                    <th>N. de C.</th>
                                    <th>Valor</th>                                    
                                    <th>Tipo Factura</th>
                                    <th>Entregado</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>


                    <div id="sec_nuevo">
                        <h2>Nueva Nota de Crédito</h2>
                        <form id="frm_nota_credito" class="form-horizontal">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="n_factura">N° Factura</label> <br>
                                     <input type="text" id="n_factura" name="n_factura"  class="form-control" readonly >
                                     <input type="hidden" id="id_factura" name="id_factura"  class="form-control" >
 
                                    
                                </div>
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">N° Nota de Crédito</label><br>
                                    <input type="text" id="num_nota_credito" name="num_nota_credito"  class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">Fecha Emisión</label><br>
                                    <input type="text" id="fecha_emision" name="fecha_emision"  class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="monto_credito">Monto</label> <br>
                                    <input type="text" id="monto_credito" name="monto_credito" onkeypress="return solonumero(event)" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="tipo_entrega_cbx">Tipo de Entrega</label><br>
                                    <select id="tipo_entrega_nc" name="tipo_entrega_nc" class="form-control">                                                	
                                    </select>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>

                            <br>
                            <button id="btn_agregar_nota_credito" class="btn btn-success">Guardar</button>
                            <button id="btn_volver_modal_credito" type="button" class="btn btn-warning">Volver</button>

                        </form>
                    </div>

                    <div class="row" id="sec_update_tipo_entrega_nc" style="display:none">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Entrega de Nota de Credito <small>Seleccione el tipo de entrega.</small></h5>                                    
                                </div>
                                <div class="ibox-content">
                                    <form method="get" class="form-horizontal">
                                        <div class="form-group"><label class="col-sm-2 control-label">Tipo de Entrega</label>

                                            <div class="col-sm-10">
                                                <select id="tipo_entrega_nc_2" class="form-control">                                                	
                                                </select>
                                                <input type="hidden" id="id_nc" >
                                            </div>

                                        </div>                                        
                                        <div class="hr-line-dashed"></div>

                                        <div class="col-sm-4 col-sm-offset-2"></div>
                                    </form>  
                                </div>

                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white" onclick="cancelarFormNc()">Cancelar</button>
                                    <button class="btn btn-primary"  onclick="enviarFormNc()">Guardar Cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>                    




                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>


    <!-- 	NOTAS DE DEBITO -->
    <div class="modal inmodal" id="modal_nota_debito" role="dialog"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4>Notas de Débito relacionadas a la Factura:</h4>
                    <h4 id="fac_modal_debito"></h4>
                    <input type="hidden" id="id_fac_hidden_debito">
                    <input type="text" id="n_factura_hidden_nd"  hidden>
                    <input type="text" id="id_factura_hidden_nd"  hidden>
                </div>
                <div class="modal-body">


                    <div id="sec_tabla_debito">
                        <button class="btn btn-success" id="btn_nueva_nota_debito">Nueva
                            Nota de Débito</button>
                        <table style="margin-top: 10px;" id="tbl_debito"
                               class="table table-hover table-bordered">
                            <thead>
                                <tr style="text-align: center">
                                    <th>#</th>
                                    <th>Fecha Emisión</th>
                                    <th>Factura N°</th>
                                    <th>N. de C.</th>
                                    <th>Valor</th>
                                    <th>Tipo Factura</th>
                                    <th>Entregado</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>


                    <div id="sec_nuevo_debito">
                        <h2>Nueva Nota de Débito</h2>
                        <form id="frm_nota_debito" class="form-horizontal">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="n_factura_nd">N° Factura</label> 
                                    <input type="text" id="n_factura_nd" name="n_factura_nd"  class="form-control" readonly >
                                     <input type="hidden" id="id_factura_nd" name="id_factura_nd"  class="form-control" >
                                </div>
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">N° Nota de Débito</label><br>
                                    <input type="text" id="num_nota_debito" name="num_nota_debito" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">Fecha Emisión</label><br>
                                    <input type="text" id="fecha_emision_debito" name="fecha_emision_debito" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <label for="n_factura_cbx">Monto</label> <br>
                                    <input type="text" id="monto_debito" name="monto_debito" onkeypress="return solonumero(event)" class="form-control">
                                </div>
                                <div class="col-lg-4">
                                    <label for="tipo_entrega_cbx">Tipo de Entrega</label><br>
                                    <select id="tipo_entrega_nd" name="tipo_entrega_nd" class="form-control">                                                	
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                        </form>
                        <br>
                        <button id="btn_agregar_nota_debito" class="btn btn-success">Guardar</button>
                        <button id="btn_volver_modal_debito" class="btn btn-warning">Volver</button>
                    </div>
                    <div class="row" id="sec_update_tipo_entrega_nd" style="display:none">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Entrega de Nota de Debito <small>Seleccione el tipo de entrega.</small></h5>                                    
                                </div>
                                <div class="ibox-content">
                                    <form method="get" class="form-horizontal">
                                        <div class="form-group"><label class="col-sm-2 control-label">Tipo de Entrega</label>

                                            <div class="col-sm-10">
                                                <select id="tipo_entrega_nd_2" class="form-control">                                                	
                                                </select>
                                                <input type="hidden" id="id_nd" >
                                            </div>

                                        </div>                                        
                                        <div class="hr-line-dashed"></div>

                                        <div class="col-sm-4 col-sm-offset-2"></div>
                                    </form>  
                                </div>

                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white" onclick="cancelarFormNd()">Cancelar</button>
                                    <button class="btn btn-primary"  onclick="enviarFormNd()">Guardar Cambios</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</div>
