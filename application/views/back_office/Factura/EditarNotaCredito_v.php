<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar Nota Credito</h5>
                <div class="ibox-tools"></div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_nota_credito_registro">
                    <p>Editar Nota Credito</p>

                    <input type="text" id="id_nota_credito" name="id_nota_credito"
                           value="<?php echo $data_nota_credito[0]['id_nota_credito']; ?>" hidden>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">N° Nota de Credito:</label>
                        <div class="input-group col-lg-3">
                            <input type="text" class="form-control required" id="num_nota_credito"
                                   requerido="true" mensaje="Debe Ingresar el n° de la nota de credito"
                                   value="<?php echo $data_nota_credito[0]['num_nota_credito']; ?>"
                                   placeholder="" name="num_nota_credito" class="form-control">
                        </div>
                    </div>
                    
                      <div class="form-group" id="data_1">

                        <label class="col-lg-2 control-label">Fecha Emisión:</label>
                        <div class="input-group date col-lg-3">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input type="text" class="form-control" id="fecha_emision" name="fecha_emision"
                                   placeholder="02-01-2017" requerido="true"
                                   value="<?php echo $data_nota_credito[0]['fecha_emision']; ?>"
                                   mensaje="Debe seleccionar una fecha">
                        </div>
                    </div>


                      <div class="form-group">
                        <label class="col-lg-2 control-label">Monto:</label>
                        <div class="input-group col-lg-3">
                            <input type="text" class="form-control required" id="monto_credito"
                                   requerido="true" mensaje="Debe Ingresar el monto de la nota de credito"
                                   value="<?php echo $data_nota_credito[0]['monto_credito']; ?>"
                                   placeholder="" name="monto_credito" class="form-control">
                        </div>
                    </div>
                              
                   <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
