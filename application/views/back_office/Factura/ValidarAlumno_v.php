<?php
/**
 * Time Line
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Luis Jarpa] <ljarpa@edutecno.com>
 */
?>


<div class="row">

    <div class="ibox float-e-margins">
        <div class="ibox-title">


            <div class="row m-b-sm m-t-sm">
                <div class="col-md-1">
                    <button type="button" id="btn_refresh" class="btn btn-white btn-sm" >
                        <i class="fa fa-refresh"></i>
                        Actualizar
                    </button>
                </div>
                <div class="col-md-11">
                    <div class="input-group"> 
                        <input type="hidden" id="id_ficha_consulta" name="id_ficha_consulta" value="<?php echo $ficha_seleccionada; ?>"  />
                        <select id="cbx_fichas" name="cbx_fichas" class="input-sm form-control">
                            <?php
                            for ($i = 0; $i < count($fichas_validar); $i++) {
                                if ($ficha_seleccionada == $fichas_validar[$i]['id']) {
                                    ?>
                                    <option value = "<?php echo $fichas_validar[$i]['id']; ?>" selected="selected">
                                        <?php echo $fichas_validar[$i]['text']; ?>
                                    </option>

                                    <?php
                                } else {
                                    ?>
                                    <option value = "<?php echo $fichas_validar[$i]['id']; ?>" >
                                        <?php echo $fichas_validar[$i]['text']; ?>
                                    </option>
                                <?php
                                }
                            }
                            ?>
                        </select>
                        <span class="input-group-btn"> <button type="button" id="btn_buscar" class="btn btn-sm btn-primary"> Buscar !</button> </span>
                    </div>
                </div>
            </div>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="ibox-content">
            <div class="row"> 
                <table id="tb_resumen_ficha" class="table table-striped">
                </table>



            </div> 
        </div>
    </div>

</div>

<div class="row">
    <div class="ibox float-e-margins">

        <div class="ibox-title">
            <h5>Validación</h5>
            <div class="ibox-tools">
                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>

            </div>
        </div>

        <div class="ibox-content">
            <table id="tbl_praticipantes_validacion"  class="table table-striped table-bordered table-hover ">
                <thead>
                    <tr>
                        <th>Rut</th>                            
                        <th>Participante</th>    
                        <th>%FQ</th>
                        <th>CC.CC</th>
                        <th>Promedio</th>
                        <th>Asist. Sence</th>
                        <th>Asist. Int.</th>
                        <th>DJ</th>
                        <th>Estado</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>


                </tbody>
                <tfoot>
                    <tr>
                        <th>Rut</th>                            
                        <th>Participante</th>    
                        <th>%FQ</th>
                        <th>CC.CC</th>
                        <th>Promedio</th>
                        <th>Asist. Sence</th>
                        <th>Asist. Int.</th>
                        <th>DJ</th>
                        <th>Estado</th>
                        <th>Status</th>
                    </tr>
                </tfoot>
            </table>

            <input type="hidden" name="estado_ficha" id="estado_ficha" value=""/>
            <div class="row" id="fila_botones">
                <div class="col-lg-1">
                    <label>comentarios: </label>
                </div>

                <div class="col-lg-9">
                    <textarea 
                        placeholder="Agregar comentarios al correo..." 
                        id="comentarios_email" name="comentarios_email" 
                        rows="5" cols="100" maxlength="2000"
                        ></textarea>
                    <p id="text-out">0/2000 caracteres restantes</p>


                </div>
                <div class="col-md-2">
                    <span class="input-group-btn">
                        <button type="button" id="solicitar_boton" class="btn btn-sm btn-danger">&nbsp;&nbsp;Solicitar&nbsp;&nbsp;</button> 
                        <br>
                        <button type="button" id="todo_ok_boton" class="btn btn-sm btn-primary">TODO OK!</button> 
                    </span>

                </div>

            </div>



        </div>

    </div>
</div>
