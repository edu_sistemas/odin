<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="num_ficha">N° Ficha</label><br>
                                <input id="num_ficha" name="num_ficha"  class="select2_demo_3 form-control" style="width: 100%"/>
                                <br>
                            </div>

                            <div class="col-lg-6">
                                <br>
                                <button type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                                <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>

                            </div>
                        </div>

                    </form> 
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Etiquetas</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">

                        <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>                                   
                                    <th>RUT</th>
                                    <th>Nombre</th>
                                    <th>A. Paterno</th>
                                    <th>A. Materno</th>                                
                                    <th>Promedio</th>
                                    <th>Empresa</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>RUT</th>
                                    <th>Nombre</th>
                                    <th>A. Paterno</th>
                                    <th>A. Materno</th>                                
                                    <th>Promedio</th>
                                    <th>Empresa</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>