<?php
/**
 * UsuarioLista_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Elvi Olado] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Jorge Nitales] <mromero@edutecno.com>
 */
?> 
    <style>
        /*select2*/
        .select2-container  {
            z-index: 2055 !important;
        }

        .select2 {
            width: 100% !important;
            padding: 0;
        }

        #contenedor_select .select2-container  {
            z-index: 1 !important;
        }

        #contenedor_select .select2 {
            width: 100% !important;
            padding: 0;
        }
    </style>




<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cursos</h5><br>



                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_cursos" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Ficha</th>
                                    <th>Curso</th>
                                    <th>F. Inicio</th>
                                    <th>F. Termino</th>                                    
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Ficha</th>
                                    <th>Curso</th>
                                    <th>F. Inicio</th>
                                    <th>F. Termino</th>                                    
                                    <th>Opciones</th>
                                </tr>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal fade in" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display:none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Docentes Asignados a este Curso</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <table id="tbl_docentes" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                            <th>Rut</th>
                            <th>Nombre</th>
                            <th>Apellido</th>                                                                       
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Rut</th>
                            <th>Nombre</th>
                            <th>Apellido</th>                                                                       
                            <th>Opciones</th>
                        </tr>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                
            </div>
        </div>
    </div>
</div>


<div class="modal inmodal fade in" id="myModal2" role="dialog" aria-hidden="true" style="display:none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Docentes Asignados a este Curso</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="ibox-content">
                    <form class="form-horizontal" method="post" action="Listar/Contrato" target="_blank">
                        <p>Confirme los datos del Docente</p>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">RUT</label>
                            <div class="col-lg-2">
                                <input type="hidden" class="form-control" id="relator_ficha" name="relator_ficha">
                                <input type="text" class="form-control" id="rut" name="rut" readonly="readonly">                                     	
                            </div>
                            <label class="col-lg-2 control-label">Nombre</label>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" id="nombre" name="nombre">                                     	
                            </div>                                    
                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label">Domicilio</label>
                            <div class="col-lg-5">
                                <input type="text" class="form-control" id="domicilio" name="domicilio" required="required">                                     	
                            </div>
                            <label class="col-lg-1 control-label">Comuna</label>
                            <div class="col-lg-4" id="contenedor_select">                                                             
                                <select class="select2 form-control" id="comuna" name="comuna" style="width:90%;" required="required">
                                    <option></option>                                           
                                </select>                 	
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Ficha</label>
                            <div class="col-lg-2">
                                <input type="text"  class="form-control" id="ficha" name="ficha" readonly="readonly">
                            </div>
                            <label class="col-lg-2 control-label">Nombre del Curso</label>
                            <div class="col-lg-6">
                                <input type="text"  class="form-control" id="nombre_curso" name="nombre_curso" required="required">
                            </div>
                        </div>                                
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Horas del curso</label>
                            <div class="col-lg-2">
                                <input type="text"  class="form-control" id="horas" name="horas" required="required">
                            </div>
                            <label class="col-lg-2 control-label">Cantidad de alumnos</label>
                            <div class="col-lg-2">
                                <input type="text"  class="form-control" name="alumnos" id="alumnos" required="required">
                            </div>
                            <label class="col-lg-2 control-label">Valor pago por hora</label>
                            <div class="col-lg-2">
                                <input type="number" step="any" class="form-control" id="valor" name="valor" required="required">
                            </div>
                        </div>                                
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Fecha de termino del curso</label>
                            <div class="col-lg-2">
                                <input type="text" class="form-control" id="fecha_termino" name="fecha_termino" required="required">
                            </div>
                            <label class="col-lg-2 control-label">Fecha entrega de boleta</label>
                            <div class="col-lg-2">
                                <input type="text"  class="form-control"  id="fecha_entrega" name="fecha_entrega" required="required">
                            </div>
                            <label class="col-lg-2 control-label">Hora reales pagadas</label>
                            <div class="col-lg-2">
                                <input type="number" step="any" class="form-control" id="horas_pagadas" name="horas_pagadas" required="required">
                            </div>
                        </div>     


                        <div class="form-group">
                            <label class="col-lg-1 control-label">Firma</label> 
                            <div class="col-lg-5">
                                <select class="form-control" id="firma" name="firma" style="width: 200px"  required="required">
                                    <option value="">Seleccione</option>      
                                    <option value="1">Vitalia Linares Oyarzún</option> 
                                    <option value="2">Carlos Linares Oyarzún</option> 
                                </select>                 	
                            </div>

                            <button class="btn btn-w-m btn-success pull-right" type="submit" id="enviar">Generar Contrato</button>

                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>                
            </div>
        </div>
    </div>
</div>


