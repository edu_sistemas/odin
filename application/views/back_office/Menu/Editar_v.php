<?php
/**
 * Agregar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Menú</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_menu_editar"  >
                    <p> Ingrese el nombre de un nuevo Menú</p>

                    <input type="hidden" placeholder="Nombre" name="id_v" class="form-control" value="<?php echo $data_menu[0]['id_menu_item']; ?>">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="nombre_v" requerido="false" mensaje ="Debe Ingresar el Nombre del menú" name="nombre_v" class="form-control" value="<?php echo $data_menu[0]['nombre']; ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Título:</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="titulo_v" requerido="false" mensaje ="Debe Ingresar el Título del menú" name="titulo_v" class="form-control" value="<?php echo $data_menu[0]['titulo']; ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Descripción:</label>
                        <div class="col-lg-3">
                            <input type="text" class="form-control required" id="descripcion_v"  mensaje ="Ingrese Descripcion del menú" name="descripcion_v" class="form-control" value="<?php echo $data_menu[0]['descripcion']; ?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Url:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required" id="url_v" requerido="false" mensaje ="Debe Ingresar la URL del menú" name="url_v" class="form-control" placeholder="Si el menú será padre Url debe ser igual a: #" value="<?php echo $data_menu[0]['url']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Icono:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required" id="icono_v" requerido="false" mensaje ="Debe Ingresar la URL del menú" name="icono_v" class="form-control" value="<?php echo $data_menu[0]['icon']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Seleccione un Módulo para el Menú:</label>
                        <div class="col-lg-7">
                            <div class="col-md-6" id="LabelP">
                                <select id="fk_modulo" name="fk_modulo"  class="select2_demo_3 form-control" requerido=true>
                                    <option></option>
                                </select>
                                <input type="hidden" name="fk_id_modulo_v" id="fk_id_modulo_v" class="form-control" value="<?php echo $data_menu[0]['fk_id_modulo']; ?>">

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Seleccione un Padre para el Menú:</label>
                        <div class="col-lg-7">
                            <div class="col-md-6" id="LabelP">
                                <select id="fk_padre" name="fk_padre"  class="select2_demo_3 form-control">
                                    <option value="">Seleccione</option>
                                </select>
                                <input type="hidden" name="fk_id_menu_item_padre_v" id="fk_id_menu_item_padre_v" class="form-control" value="<?php echo $data_menu[0]['fk_id_menu_item_padre']; ?>">

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Seleccione el Estado del Menú:</label>
                        <div class="col-lg-7">
                            <div class="col-md-6" id="LabelP">
                                <select id="fk_estado" name="fk_estado"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                                <input type="hidden" name="fk_id_menu_item_estado_v" id="fk_id_menu_item_estado_v" class="form-control" value="<?php echo $data_menu[0]['fk_id_menu_item_estado']; ?>">

                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar" id="btn_editar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
