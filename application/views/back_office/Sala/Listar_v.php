<?php
/**
 * UsuarioLista_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Salas</h5><br>
               

                    <div class="ibox-tools">

                        <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo</button>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">  
                        <table id="tbl_docentes" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Numero Sala</th>
                                    <th>Detalle</th>
                                    <th>Capacidad</th>    
                                    <th>Fecha Registro</th>             
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Id</th>
                                    <th>Nombre</th>
                                    <th>Numero Sala</th>
                                    <th>Detalle</th>
                                    <th>Capacidad</th>    
                                    <th>Fecha Registro</th>             
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!--    MODAL SKILL -->

	<div class="modal inmodal fade in" id="modal_programas" tabindex="-1"
		role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title" id="num_sala"></h4>
					<h4 class="modal-title" id="num_sala_hidden" hidden></h4>
					<h4 class="modal-title" id="id_sala" hidden></h4>
					<p class="font-bold">Programas instalados en la sala</p>
				</div>
				<div class="modal-body">
				<div id="sec_tabla">
				<button class="btn btn-primary" id="btn_nuevo_registro">Nuevo Registro</button>
					<table style="margin-top: 10px;" id="tbl_programas_sala" class="table table-hover">
	                                <thead>
	                                <tr>
	                                    <th>#</th>
	                                    <th>Programa</th>
	                                    <th>Registrado por</th>
	                                    <th>Opciones</th>
	                                </tr>
	                                </thead>
	                                <tbody>
	                                
	                                </tbody>
	                            </table>
	                            
					</div>
					
					<div id="sec_comentarios">
						<p></p>
						<div id="sec_comentarios_comentario" style="min-height: 100px;border:2px solid #484848; background-color: #fff ;padding: 10px; margin-bottom: 10px;">
							<p style="color:black;"></p>
						</div>
						<button id="btn_volver" class="btn btn-primary">Volver</button>
					
					</div>
					
					<div id="sec_editar">
					<h1>Editar Registro</h1>
						<form id="frm_editar_programa_sala" name="frm_editar_programa_sala">
							<div class="row" style="margin-bottom: 10px;">
							<input type="hidden" id="hidden_id_programa_sala_editar" name="hidden_id_programa_sala_editar">
								<div class="col-lg-12">
									<label>Programa: </label>
									<select id="programas_cbx_editar" name="programas_cbx_editar" requerido="true" mensaje="debe seleccionar un programa"></select><u><a id="agregar_programa_editar" style="vertical-align: text-bottom; font-size: 85%;" title="agregar programa"> Añadir programa a la lista</a></u>
								</div>
							</div>
							<div class="row" style="margin-bottom: 10px;">
								<div class="col-lg-12">
								<label for="editar_comentario">Comentarios: </label>
								<br>
								<textarea id="editar_comentario" name="editar_comentario" requerido="true" mensaje="debe agregar un comentario" style="width: 100%;" rows="6" cols=""></textarea>
								
								</div>
							</div>
						</form>	
						<button id="btn_editar_programa" class="btn btn-primary">Guardar</button>
						<button id="btn_descartar_editar_programa" class="btn btn-success">Descartar</button>
					
					</div>
					
					
					<div id="sec_agregar_programa">
					<h1>Agregar un programa a la lista de programas</h1>
						<form id="frm_agregar_programa" name="frm_agregar_programa">
							<div class="row" style="margin-bottom: 10px;">
								<div class="col-lg-12">
									<label>Nombre y versión del Programa: </label>
									<input requerido="true"mensaje="debe ingresar un nombre de programa" type="text" id="txt_nombre_programa" name="txt_nombre_programa">
								</div>
							</div>
									
							<div class="row" style="margin-bottom: 10px;">
								
							</div>
						</form>	
						<button id="btn_guardar_agregar_programa" class="btn btn-primary">Guardar</button>
						<button id="btn_descartar_agregar_programa" class="btn btn-success">Descartar</button>
						
					</div>
					
					<div id="sec_nuevo">
					<h1>Agregar Nuevo Registro de Programa</h1>
						<form id="frm_agregar_programa_sala" name="frm_agregar_programa_sala">
							<div class="row" style="margin-bottom: 10px;">
								<div class="col-lg-12">
									<label>Programa: </label>
									<select id="programas_cbx" name="programas_cbx" requerido="true" mensaje="debe seleccionar un programa"></select><u><a id="agregar_programa" style="vertical-align: text-bottom; font-size: 85%;" title="agregar programa"> Añadir programa a la lista</a></u>
								</div>
							</div>
							<div class="row" style="margin-bottom: 10px;">
								<div class="col-lg-12">
								<label for="nuevo_comentario">Comentarios: </label>
								<br>
								<textarea id="nuevo_comentario" name="nuevo_comentario" requerido="true" mensaje="debe agregar un comentario" style="width: 100%;" rows="6" cols=""></textarea>
								<input type="hidden" id="hidden_id_sala" name="hidden_id_sala">
								</div>
							</div>
						</form>	
						<button id="btn_guardar_programa" class="btn btn-primary">Guardar</button>
						<button id="btn_descartar_programa" class="btn btn-success">Descartar</button>
						
					</div>
				
				</div>
				<div class="modal-footer">
					<button type="button" id="btn_cerrar_modal" class="btn btn-white"
						data-dismiss="modal">Cerrar</button>
					<!-- 					<button type="button" class="btn btn-primary">Save changes</button> -->
				</div>
			</div>
		</div>
	</div>
</div>
