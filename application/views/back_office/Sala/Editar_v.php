<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nueva Sala</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_docente_registro"  >
                    <p> Ingrese una nueva sala</p>

                    <input type="hidden" id="id_sala" name="id_sala" value="<?php echo $data_sala[0]['id_sala'];?>">
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control required"
                                   id="nombre" requerido="true"
                                   mensaje ="Debe ingresar nombre de la sala"
                                   placeholder="Ej. Sala 21"
                                   name="nombre" value="<?php echo $data_sala[0]['nombre'];?>"                                    
                                   /><br>
                        </div>                       

                        <label class="col-lg-2 control-label">Numero de Sala:</label>
                        <div class="col-lg-4">
                            <input type="number" class="form-control required"
                                   id="numero" requerido="true"
                                   mensaje ="Debe Ingresar un numero de sala, si no aplica ingrese un 0"
                                   placeholder="Ingrese numero de sala"
                                   name="numero" value="<?php echo $data_sala[0]['numero_sala'];?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="col-lg-2 control-label">Detalle:</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control required"
                                   id="detalle" requerido="true"
                                   mensaje ="Debe Ingresar un nombre de docente válido"
                                   placeholder="Ej: Oficina, Sala, INFO-BUS, etc."
                                   name="detalle" value="<?php echo $data_sala[0]['detalle'];?>"
                                   >
                        </div>
                        
                        
                           <label class="col-lg-2 control-label">Capacidad:</label>
                        <div class="col-lg-4">
                            <input type="number" class="form-control required"
                                   id="capacidad" requerido="true"
                                   mensaje ="Debe Ingresar la capacidad total de la sala"
                                   placeholder="Ej. 10"
                                   name="capacidad" value="<?php echo $data_sala[0]['capacidad'];?>"
                                   >
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label"></label>
                        <div class="col-lg-3">
                            <p id="out_print" style="color: red"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Sede:</label>
                        <div class="col-lg-4">
                            <select id="sede" name="sede" class="form-control required" requerido="true" mensaje ="Debe Ingresar la capacidad total de la sala" 
                            >
                            <option value="" disabled>Seleccione</option>
                            	<?php 
									for($i=0;$i<count($sede);$i++){
										echo "<option value='".$sede[$i]['id']."'>".$sede[$i]['text']."</option>";
										if($data_sala[0]['id_sede']==$sede[$i]['id']){
											echo "<option value='".$sede[$i]['id']."' selected='selected'>".$sede[$i]['text']."</option>";	
										}else{
											echo "<option value='".$sede[$i]['id']."'>".$sede[$i]['text']."</option>";	
										}
									}
								?>
                            </select>
                            
                        </div>                        
                        <div class="col-lg-4">
                            
                        </div>
                    </div>                   

                    

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Editar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
