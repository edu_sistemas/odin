<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-25 [Marcelo Romero] <ljarpa@edutecno.com>
 
 * Fecha creacion:	2016-10-25 [Marcelo Romero] <ljarpa@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline animated" id="filtros_form" name="filtros_form">

                        <!-- Text input-->
                        <div class="form-group">

                            <div class="col-lg-4">
                                <input id="rut" name="rut" type="text" placeholder="1111111-1" class="form-control input-md">
                                <p class="help-block">Rut</p>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">

                            <div class="col-md-4">
                                <input id="nombre" name="nombre" type="text" placeholder="Nombre o Apellido" class="form-control input-md">
                                <p class="help-block">Nombre</p>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <div class="col-md-4">

                                <select id="estado" name="estado" class="form-control">
                                    <option value=""> Todos </option>
                                    <option value="1"> Activo</option>
                                    <option value="2"> Inactivo</option>
                                </select>
                                <p class="help-block">Estado</p>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-lg-4">
                                <input id="num_ficha" name="num_ficha" type="text" placeholder="12345" class="form-control input-md">
                                <p class="help-block">Ficha</p>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <div class="col-md-4">
                                <button type="button" id="btn_buscar" name="btn_buscar" class="btn btn-outline btn-success">
                                    Buscar
                                </button>
                                <p class="help-block">&nbsp; </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Alumno</h5>
                    <div class="ibox-tools">

                        <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo</button>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="tbl_alumnos" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>Rut</th>
                                    <th>Usuario</th>
                                    <th>Nombre</th>
                                    <th>Apellido Paterno</th>
                                    <th>Contacto</th>
                                    <th>Cursos</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>

                            <tfoot>
                                <tr>
                                    <th>Rut</th>
                                    <th>Usuario</th>
                                    <th>Nombre</th>
                                    <th>Apellido Paterno</th>
                                    <th>Contacto</th>
                                    <th>Cursos</th>
                                    <th>Opciones</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="modal_contacto_alummno" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4 class="modal-title" id="modal_nombre_contacto"></h4>

                        <!--                    <small class="font-bold" id="" >Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>-->

                </div>
                <div class="modal-body">
                    <div class="btn-group pull-right">

                        <button class="btn btn-success btn-sm" type="button" id="btn_nuevo_contacto" title="Nuevo Contacto"><i class="fa fa-plus-square-o"></i></button>
                    </div>
                    <div id="div_frm_add" style="display: none;">
                        <form class="form-horizontal" id="frm_nuevo_contacto_alumno">

                            <input type="hidden" id="txt_id_alumno" name="txt_id_alumno" />

                            <input type="hidden" id="txt_id_contacto_alumno" name="txt_id_contacto_alumno" />
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="txt_correo_contacto">Email</label>
                                <div class="col-md-4">
                                    <input id="txt_correo_contacto" name="txt_correo_contacto" requerido="true" mensaje="Debe Ingresar el Email válido" maxlength="200" type="email" placeholder="correo@dominio.cl" class="form-control input-md">

                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="txt_telefono">Telefono</label>
                                <div class="col-md-4">
                                    <input id="txt_telefono" name="txt_telefono" requerido="true" mensaje="Debe Ingresar teletono" type="phone" placeholder="9 87845621" class="form-control input-md">

                                </div>
                            </div>

                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_guardar_contacto"></label>
                                <div class="col-md-8">
                                    <button id="btn_guardar_contacto" type="button" name="btn_guardar_contacto" class="btn btn-success">Guardar</button>
                                    <button id="btn_cancelar_contacto" type="button" name="btn_cancelar_contacto" class="btn btn-default">Cancelar</button>
                                    <button id="btn_limpiar" type="reset" name="btn_limpiar" class="btn btn-success" style="display: none;">limpiar</button>
                                </div>
                            </div>

                        </form>

                    </div>

                    <div class="table-responsive" id="div_tbl_contacto_alumno">

                        <table id="tbl_contacto_alumno" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th>Fecha Registro</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Email</th>
                                    <th>Telefono</th>
                                    <th>Fecha Registro</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>

    <!-- MODAL DE LOS CURSOS -->

    <div class="modal inmodal" id="modal_curso_alummno" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4 class="modal-title" id="modal_nombre_curso_alumno"></h4>

                </div>

                <div class="modal-body">

                    <div class="table-responsive" id="div_tbl_alumno_curso">

                        <table id="tbl_curso_s_alumno" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Número De Ficha</th>
                                    <th>Nombre Curso</th>
                                    <th>Duración</th>
                                    <th>Código sende</th>
                                    <th>Empresa</th>
                                    <th>F. Inicio</th>
                                    <th>F. Fin</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Número De Ficha</th>
                                    <th>Nombre Curso</th>
                                    <th>Duración</th>
                                    <th>Código sende</th>
                                    <th>Empresa</th>
                                    <th>F. Inicio</th>
                                    <th>F. Fin</th>
                                    <th>Estado</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div> 
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>