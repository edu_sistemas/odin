<div class="passwordBox animated fadeInDown">

    <div class="row">

        <div class="col-md-12">
            <div class="ibox-content">
                <div class="pull-right"  >      
                    <img width="100" src="<?php echo base_url('assets/amelia/images/odin_logo.png') ?>" alt="">
                </div>

                <h2 class="font-bold">
                    Olvidaste tu contraseña?
                </h2>
                <p> 
                    Ingrese su usuario. Su contraseña será restablecida y enviada a su correo electrónico.
                </p>
                <div class="row">
                    <div class="col-lg-12">
                        <form class="m-t" role="form"  id="forgot_pass" >
                            <div class="form-group">
                                <input id="user_name" name="user_name"
                                       type="text" class="form-control" 
                                       placeholder="Rut (Sólo números y dígito verificador)"
                                       requerido="true"
                                       mensaje ="Debe Ingresar su Identificador de Usuario"
                                       required="">
                            </div>
                            <button id="btn_forgort_mi_passs" name="btn_forgort_mi_passs" type="button" class="btn btn-primary block full-width m-b">Enviar nueva password</button>
                            <a href="<?php echo base_url(); ?>"> Cancelar </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6">
            EDUTECNO    
        </div>
        <div class="col-md-6 text-right">
            <small>© 2016</small>
        </div>
    </div>
</div>