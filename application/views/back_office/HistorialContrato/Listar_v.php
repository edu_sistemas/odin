<?php
/**
 * UsuarioLista_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Historial Contratos</h5>					
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_contratos"
                               class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>Id Contrato</th>
                                    <th>Ficha</th>									
                                    <th>Nombre</th>								
                                    <th>Rut</th>
                                    <th>Curso</th>
                                    <th>Horas Pagadas</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Id Contrato</th>
                                    <th>Ficha</th>									
                                    <th>Nombre</th>								
                                    <th>Rut</th>
                                    <th>Curso</th>
                                    <th>Horas Pagadas</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" tabindex="-1" class="modal inmodal fade in">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Detalles Contrato</h4>
                <small class="font-bold"></small>
            </div>
            <div class="modal-body">
                <div class="ibox-content">
                    <form class="form-horizontal" method="post" action="Listar/Contrato" target="_blank">
                        <div class="form-group"><label class="col-sm-2 control-label">Id Contrato</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="id_contrato" name="id_contrato"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Numero ficha</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="n_ficha" name="n_ficha"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Fecha emisión</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="f_emision" name="f_emision"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Nombre Relator</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="nombre_relator"  name="nombre_relator"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Rut relator</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="rut_relator" name="rut_relator"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Dirección relator</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="direccion_relator" name="direccion_relator"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Comuna relator</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="comuna_relator" name="comuna_relator"></div>
                        </div>
                        <div class="hr-line-dashed"></div> 
                        <div class="form-group"><label class="col-sm-2 control-label">Curso</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="curso" name="curso"></div>
                        </div>
                        <div class="hr-line-dashed"></div> 
                        <div class="form-group"><label class="col-sm-2 control-label">Alumnos</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="alumnos" name="alumnos"></div>
                        </div>
                        <div class="hr-line-dashed"></div> 
                        <div class="form-group"><label class="col-sm-2 control-label">Horas del Curso</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="hora_curso" name="hora_curso"></div>
                        </div>
                        <div class="hr-line-dashed"></div> 
                        <div class="form-group"><label class="col-sm-2 control-label">Fecha de Termino</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="f_termino" name="f_termino"></div>
                        </div>
                        <div class="hr-line-dashed"></div>  
                        <div class="form-group"><label class="col-sm-2 control-label">Valor hora</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="valor_hora" name="valor_hora"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Fecha_boleta</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="f_boleta" name="f_boleta"></div>
                        </div>
                        <div class="hr-line-dashed"></div> 
                        <div class="form-group"><label class="col-sm-2 control-label">Horas Pagadas</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="horas_pagadas"  name="horas_pagadas"></div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Total</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="total" name="total"></div>
                        </div>
                        <div class="hr-line-dashed"></div>  
                        <div class="form-group"><label class="col-sm-2 control-label">Retencion</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="retencion"  name="retencion"></div>
                        </div>
                        <div class="hr-line-dashed"></div> 
                        <div class="form-group"><label class="col-sm-2 control-label">Total a pagar</label>

                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="a_pagar" name="a_pagar"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Firma</label>
                            <input type="hidden" class="form-control" readonly id="firma" name="firma">
                            <div class="col-sm-10"><input type="text" class="form-control" readonly id="quien_firma" name="quien_firma"></div>
                        </div>
                        <div class="hr-line-dashed"></div> 
                        <button class="btn btn-w-m btn-success" type="submit" id="enviar">Generar Contrato</button>                               
                    </form>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




