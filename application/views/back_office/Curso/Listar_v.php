<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm_busqueda" name="frm">
                        <!-- Text input-->
                        <div class="form-group">
                            <div class="col-md-10">
                                <input id="nombre_curso" name="nombre_curso" type="text" placeholder="Ingrese nombre" class="form-control input-md">
                                <p class="help-block">Nombre del curso</p>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <div class="col-md-6">
                                <select id="cbx_modalidad" name="cbx_modalidad"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                                <p class="help-block">Modalidad</p>
                            </div>
                        </div>

 

                        <!-- Select Basic -->
                        <div class="form-group">
                            <div class="col-md-6">
                                <select id="cbx_version" name="cbx_version"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                                <p class="help-block">Versión</p>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <div class="col-md-6">
                                <select id="cbx_nivel" name="cbx_nivel"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                                <p class="help-block">Nivel</p>
                            </div>
                        </div>
                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="btn_buscar_curso"></label>
                            <div class="col-md-3">
                                <button type="button" id="btn_buscar_curso" name="btn_buscar_curso" class="btn btn-outline btn-success">
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de cursos</h5>
                    <div class="ibox-tools">

                        <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo</button>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_curso" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>

                                    <th>Nombre del curso</th>
                                    <th>Descripción</th> 
                                    <th>Hrs</th>
                                    <th>Modalidad</th>
                                    <th>Nivel</th>
                                    <th>Versión</th>
                                    <th>C. Sence</th>  
                                    <th>Estado</th>
                                    <th>Ultima Modificación</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nombre del curso</th>
                                    <th>Descripción</th> 
                                    <th>Hrs</th>
                                    <th>Modalidad</th>
                                    <th>Nivel</th>
                                    <th>Versión</th>
                                    <th>C. Sence</th> 
                                     <th>Estado</th>
                                     <th>Ultima Modificación</th>
                                    <th>Acción</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
