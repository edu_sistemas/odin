<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-31 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2017-01-31 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-lg-6">
                                <h5>Lista de Cursos</h5>
                            </div>
                            <div class="col-lg-6" style="text-align: right">
                                <a target="_blank" href="../../documentacion/asignar_relator.html">Necesito ayuda con esto</a>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form class="form-horizontal" id="frm_sence_registro">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Seleccionar ficha</label>
                                <div class="col-md-3" style="padding-left: 0px !important;">
                                    <select id="id_ficha_v" name="id_ficha_v" class="select2_demo_1 form-control">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-md-3" style="padding-left: 0px !important;">
                                    <button type="button" name="btn_generar_excel" id="btn_generar_excel" class="btn btn-outline btn-primary   ">
                                        <i class="fa fa-download"></i> Descargar</button>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <div id="loading-container" class="text-center"></div>
                        <div class="table-responsive">
                            <table id="tbl_clase" class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Ficha</th>
                                        <th>Nombre del curso</th>
                                        <th>N° A.</th>
                                        <th>Sala</th>
                                        <th>Inicio</th>
                                        <th>Términio</th>
                                        <th title="Código Sence">C. Sence</th>
                                        <th>ID. Cons.</th>
                                        <th>IDS Sence</th>
                                        <th title="Nombre Relator">N. Relator</th>
                                        <th>Relator</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Ficha</th>
                                        <th>Nombre del curso</th>
                                        <th>N° A.</th>
                                        <th>Sala</th>
                                        <th>Inicio</th>
                                        <th>Términio</th>
                                        <th title="Código Sence">C. Sence</th>
                                        <th>ID. Cons.</th>
                                        <th>IDS Sence</th>
                                        <th title="Nombre Relator">N. Relator</th>
                                        <th>Relator</th>
                                        <th>Acción</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal in" id="docente" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" id="xbutton" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-graduation-cap modal-icon"></i>
                    <h4 class="modal-title">Asignar Relator</h4>
                    <small>Se puede asignar mas de un relator por clase.</small>
                </div>
                <div class="modal-body">
                    <form method="post" class="form-horizontal" id="frmLista" name="frmLista">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label style="margin-left: 15px;"> Encargado de Asignar relator:
                                    </label>
                                    <label>
                                        <?php echo $this->session->userdata('nombre_user'); ?>
                                    </label>
                                    <input type="hidden" name="id_perfil" id="id_perfil" value="<?php echo $this->session->userdata('id_perfil'); ?>" />
                                    <input type="hidden" name="id_ficha" id="id_ficha">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="ibox-content">
                                <form id="form" class="wizard-big">
                                    <div class="row">
                                        <!-- Select Multiple -->
                                        <div class="col-md-4">
                                            Relatores Disponibles
                                            <form action="" class="search-form">
                                                <div class="form-group has-feedback">
                                                    <label for="search" class="sr-only">Buscar</label>
                                                    <input type="text" class="form-control" name="search2"
                                                        id="search2" placeholder="Buscar">
                                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>

                                            </form>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="cbx_relatores_disponibles"> </label>
                                                <div class="col-md-13">
                                                    <select id="cbx_relatores_disponibles_visible" name="cbx_relatores_disponibles_visible[]" class="form-control" multiple="multiple">
                                                    </select>
                                                    <div id="PermiHidnn2" style="display: none;">
                                                        <select id="cbx_relatores_disponibles" name="cbx_relatores_disponibles" class="form-control" multiple="multiple">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <!-- Button -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="btn_derecha"></label>
                                                <div class="derizq" style="margin-top: 70px;">
                                                    <button style="width: 90%; margin-left: 15px;" id="btn_derecha" type="button" name="btn_derecha" class="btn btn-primary btn-lg btn-block glyphicon glyphicon-chevron-right"
                                                        data-toggle="tooltip" title="seleccione un docente de la lista izquierda para asignar"></button>
                                                    <button style="width: 90%; margin-left: 15px;" id="btn_izquierda" type="button" name="btn_izquierda" class="btn btn-primary btn-lg btn-block glyphicon glyphicon-chevron-left"
                                                        data-toggle="tooltip" title="seleccione un docente de la lista derecha para eliminar"></button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Select Multiple -->
                                        <div class="col-md-4">
                                            Relatores Asignados
                                            <form action="" class="search-form">
                                                <div class="form-group has-feedback">
                                                    <label for="search" class="sr-only">Buscar</label>
                                                    <input type="text" class="form-control" name="search1"
                                                        id="search1" placeholder="Buscar">
                                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>

                                            </form>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-13">
                                                    <select id="relatores_asignados_visible" name="relatores_asignados_visible[]" class="form-control" multiple="multiple">
                                                    </select>

                                                    <div id="PermiHidnn" style="display: none;">
                                                        <select id="relatores_asignados" name="relatores_asignados" class="form-control" multiple="multiple">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </form>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="confirmard" onclick="" class="btn btn-primary">Confirmar Docente</button>
                    <br>
                    <br>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal in" id="detalle_reserva" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" id="xbutton" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <i class="fa fa-eye modal-icon"></i>
                    <h4 class="modal-title">Horarios de Curso</h4>
                    <small>Los siguientes horarios corresponden solo a reservas confirmadas.</small>
                </div>

                <div class="modal-body">
                    <div class="ibox-content">
                        <table id="detalle_reserva_tbl" class="table table-hover no-margins">
                            <thead>
                                <tr>
                                    <th>Sala</th>
                                    <th>fecha</th>
                                    <th>Hora inicio</th>
                                    <th>Hora Termino</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- INGRESAR ID ACCION SENCE -->
    <div class="modal inmodal" id="modal_id_sence" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h3>Ingrese ID Acción Sence para la Ficha</h3>
                    <h4 id="id_ficha_modal_accion"></h4>
                    <h4 id="id_a_consolidado"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="frm_id_accion_sence" name="frm_id_accion_sence">
                        <input id="id_ficha_s" name="id_ficha" type="hidden">
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="radio_agregar">
                                    <input id="radio_agregar" type="radio" name="tipo_accion" value="1" checked> Agregar
                                </label>
                                <label class="col-md-4 control-label" for="radio_eliminar">
                                    <input id="radio_eliminar" type="radio" name="tipo_accion" value="2"> Eliminar
                                </label>
                            </div>
                            <div id="agregar_container" class="form-group">
                                <label class="col-md-3 control-label" for="id_accion">ID Acción SENCE</label>
                                <div class="col-md-8">
                                    <input type="text" id="id_accion" class="form-control input-md" name="id_accion" maxlength="50" 
                                    requerido="true" mensaje="ingrese el id correspondiente..."
                                    style="width: 100%">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="id_ingresadas" class="col-md-3 control-label">ID's Ingresadas</label>
                                <div class="col-md-8">
                                    <select id="id_ingresadas" name="id_ingresadas" class="form-control" multiple readonly>
                                    </select>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <div class="col-lg-4">
                                </div>
                                <div class="col-lg-4" style="text-align: center">
                                    <br>
                                    <button type="button" id="btn_insert_accion_sence" class="btn btn-primary pull-left">Ingresar</button>
                                    <button type="button" id="btn_eliminar_accion_sence" class="btn btn-outline btn-danger">Eliminar</button>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>
                        </form>
                    </div>

                <div class="modal-footer">

                    <button type="button" id="cerrarmodal" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>

    <!-- INGRESAR ID ACCION SENCE CONSOLIDADO   -->
    <div class="modal inmodal" id="modal_id_sence_consolidado" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Cerrar</span>
                    </button>
                    <h3>Ingrese ID Sence Consolidado para la Ficha</h3>
                    <h4 id="id_ficha_modal_accion_consolidado"></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" id="frm_id_accion_sence_consolidado" name="frm_id_accion_sence_consolidado">
                        <div class="row">
                            <div class="col-lg-4">
                                <input type="hidden" id="id_ficha_consolidado" name="id_ficha_consolidado" value="">
                            </div>
                            <div class="col-lg-4">
                                <label for="id_accion">ID Consolidado:</label>
                                <br>
                                <input type="text" id="id_accion_consolidado" name="id_accion_consolidado" class="form-control input-md" requerido="true" mensaje="ingrese el id sence consolidado" list="opciones_id_consolidado">
                                <!--datalist id="opciones_id_consolidado">
                                </datalist-->
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <br>

                                <button type="button" id="btn_insert_accion_sence_consolidado" class="btn btn-warning pull-left">Guardar</button>
                            </div>
                            <div class="col-lg-4">
                            </div>

                        </div>
                    </form>
                    
                </div>
                <div class="modal-footer">

                    <button type="button" id="cerrarmodal2" class="btn btn-white" data-dismiss="modal">Cerrar</button>

                </div>
            </div>
        </div>
    </div>

    <?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */