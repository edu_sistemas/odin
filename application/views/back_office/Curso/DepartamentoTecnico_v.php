<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm_busqueda" name="frm_busqueda">
                            <div class="row">
                                <div class="col-lg-3" id="fecha_1" style="padding-right: 0px;">
                                        <div class="input-group date" style="width: 100%">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input name="fecha_reporte" id="fecha_reporte" type="text" class="form-control" value="">
                                        </div>
                                </div>
                                <div class="col-lg-3" style="padding-left: 0px;">
                                    <button type="button" class="btn btn-primary" id="btn_consultar_fecha">Consulta</button>
                                </div>
                            </div>
                            
                            <hr>
                            <div class="row">
                                <div class="col-lg-3">
                                 <a id="btnActividadesVigentes" style="width: 100%;"  target="_blank" href="../ActVigente/Ver/setGenerarpdf/">
                                 <button type="button" id="btn_pdf" style="width: 100%;"  class="btn btn-w-m btn-info btn-rounded">
                                 <i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Actividades vigentes</button></a>	
                                
                                </div>
    
                                <div class="col-lg-3">
                                <a id="btnActividadesDiarias" style="width: 100%;" target="_blank" href="../ActVigente/Ver/setGenerarpdfDiario/"><button type="button" id="btn_pdf" style="width: 100%;" class="btn btn-w-m btn-info btn-rounded "><i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Actividades Diarias</button></a><br>
                                <input type="hidden" id="externos_hidden">
                                <label class="form-control text-center" style="width: 100%;" for="incluir_externos">Incluir Externos <input type="checkbox"  name="incluir_externos" id="incluir_externos"></label>
    
                                </div>

                                <div class="col-lg-3">
                                <a id="btnActividadesDiariasReserva" style="width: 100%;" target="_blank" href="../ActVigente/Ver/setGenerarpdfDiarioReserva/"><button type="button" id="btn_pdf" style="width: 100%;" class="btn btn-w-m btn-info btn-rounded "><i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Actividades Diarias de Reservas</button></a><br>
                                
                                </div>
                            </div>
                    </form> 

                    <div class="row">
                        <form   role="form" class="form-inline hidden" method="POST" id="frm_Actividades_Vigentes" name="frm_Actividades_Vigentes" target="_blank" action="<?php echo base_url(); ?>back_office/ActVigente/Ver">
    
                            <input name="id_empresa" id="id_empresa" value="" >
                            <input name="fecha_start" id="fecha_start" value="" >
                            <input name="fecha_end" id="fecha_end" value="" >
                            <input name="num_num_ficha" id="num_num_ficha" value="" >
                        </form>
    
                        <form role="form" class="form-inline hidden" method="POST" id="frm_Actividades_diarias" name="frm_Actividades_diarias" target="_blank" action="<?php echo base_url(); ?>back_office/Curso/DepartamentoTecnico/generar">
    
                            <input name="id_empresa" id="id_empresa" value="" >
                            <input name="fecha_start" id="fecha_start" value="" >
                            <input name="fecha_end" id="fecha_end" value="" >
                            <input name="num_num_ficha" id="num_num_ficha" value="" >
                        </form>

                        <form role="form" class="form-inline hidden" method="POST" id="frm_Actividades_diarias_reserva" name="frm_Actividades_diarias_reserva" target="_blank" action="<?php echo base_url(); ?>back_office/ActVigente/Ver">
    
                            <input name="id_empresa" id="id_empresa" value="" >
                            <input name="fecha_start" id="fecha_start" value="" >
                            <input name="fecha_end" id="fecha_end" value="" >
                            <input name="num_num_ficha" id="num_num_ficha" value="" >
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--    <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
    
                    <div class="ibox-content">
                        <a class="btn btn-primary btn-rounded btn-block" href="<?php // echo base_url();            ?>back_office/ActVigente/Ver" target="_blank">
                            <i class="fa fa-download"></i> Descargar Actividades Vigentes
                        </a>
                    </div>
                </div>
            </div>
        </div>-->

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Fichas disponibles desde el <fechatitulo></fechatitulo></h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>

                                    <th>N° Ficha</th>
                                    <th>Empresa</th>
                                    <th>F.Inicio</th>
                                    <th>F.Térm.</th>
                                    <th>Curso</th>
                                    <th>Hrs.</th>
                                    <th>Relator</th>
                                    <th>Dias</th>
                                    <th>H.I </th>
                                    <th>H.T </th>
                                    <th>Sala </th>
                                    <th> Al.</th>
                                    <th> SenceNet</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>N° Ficha</th>
                                    <th>Empresa</th>
                                    <th>F.Inicio</th>
                                    <th>F.Térm.</th>
                                    <th>Nombre Curso</th>
                                    <th>Hrs.</th>
                                    <th>Relator</th>
                                    <th>Dias</th>
                                    <th>H.I </th>
                                    <th>H.T </th>
                                    <th>Sala </th>
                                    <th> Al.</th>
                                    <th> SenceNet</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hiddens Modals para mostrar contactos -->

    <div class="modal inmodal" id="modal_documentos_ficha" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight" id="modal-principal" onclick="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Documentos para ficha N°: <h4 class="modal-title" id="modal_id_ficha"></h4></h4>

                </div>
                <div class="modal-body">
                    <div class="btn-group">

                        <button class="btn btn-success" type="button" id="btn_nuevo_contacto" title="Subir Archivos">Subir un nuevo archivo</button> 
                        <br><br><br>
                    </div>
                    <div  id="div_frm_add" style="display: none;"> 

                        <form class="form-horizontal" id="frm_nuevo_contacto_empresa" >


                            <div class="form-group">
                                <div class="col-lg-6">
                                    <label class="control-label">Archivos Adjuntos</label>
                                    <span class="btn btn-file fileinput-button">
                                        <i class="glyphicon glyphicon-plus"></i>
                                        <span>Seleccionar archivos</span>
                                        <!-- The file input field used as target for the file upload widget  -->
                                        <input id="fileupload" type="file" name="fileupload[]" multiple="" accept="application/pdf,application/vnd.ms-excel,image/gif, image/jpeg,image/png,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">

                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <div id="progress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>
                                    <span id="porsncetaje_carga" class="pull-right">0%</span>
                                </div> 
                            </div>

                            <!-- Button (Double) -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="btn_guardar_contacto"></label>
                                <div class="col-md-8">
                                    <button id="btn_cancelar_contacto" type="button" name="btn_cancelar_contacto" class="btn btn-success">Volver</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- <div class="table-responsive" id="div_tbl_contacto_empresa">   -->
                    <table id="tbl_documentos_ficha" class="table-md-4 table-bordered" >
                        <thead>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>        
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>         
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre</th> 
                                <th>Fecha de subida</th>          
                                <th>Opciones</th>
                            </tr>
                        </tfoot>
                    </table>    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal" id="btn_cerrarmodal_doc">Cerrar</button>
                </div>
            </div>
        </div>
    </div> 
    <!-- Modal Motivo Rechazo --> 
    <div class="modal inmodal" id="modal_motivo_rechazo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <i class="fa fa-info-circle modal-icon"></i>
                    <h4>Motivo de rechazo para la ficha N°: <h4 class="modal-title" id="modal_id_ficha2"></h4></h4>   
                </div>
                <div class="modal-body">
                    <input type="text" id="id_ficha" name="id_ficha" hidden>
                    <div class="form-group">
                        <textarea name="motivo_rechazo" id="motivo_rechazo" class="form-control" cols="30" rows="10" readonly=""></textarea>
                    </div>
                    <div class="form-group">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
