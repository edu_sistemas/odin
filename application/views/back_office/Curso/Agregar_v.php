<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Curso</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <form class="form-horizontal" id="frm_agregar_curso"  >
                    <p> Agregar Curso</p>
                    <div class="form-group"><label class="col-lg-2 control-label">Nombre de curso:</label>
                        <div class="col-lg-4">
                            <input type="text" placeholder="Nombre" requerido="true" mensaje="Debe ingresar nombre del perfil" name="nombre_curso_v" id="nombre_curso_v" class="form-control">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Descripción de curso:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control" requerido="true" mensaje="Debe ingresar una descripción sobre el perfil" id="descripcion_curso_v" name="descripcion_curso_v" maxlength="100" placeholder="Descripción"></textarea>
                            <p id="text-out">0/200 caracteres restantes</p>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Duracion del curso:</label>
                        <div class="col-lg-4">
                            <input type="number" onkeypress="return solonumero(event)" placeholder="Ingrese total de horas" requerido="true" mensaje="Debe ingresar nombre del perfil" id="duracion_curso_v" name="duracion_curso_v" class="form-control">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-2 control-label">Modalidad:</label>
                        <div class="col-lg-4">
                            <select id="cbx_modalidad" name="cbx_modalidad"  class="form-control required"
                                    mensaje ="Debe seleccionar una modalidad" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Código Sence:</label>
                        <div class="col-md-4"  >
                            <select id="cbx_codigo_sence" name="cbx_codigo_sence" 
                                    class="form-control required" requerido="true"
                                    mensaje="Debe seleccionar un Codigo Sence"
                                    >
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Familia o Tipo Producto:</label>
                        <div class="col-lg-4">

                            <select id="cbx_familia" name="cbx_familia"  class="form-control required"
                                    mensaje ="Debe seleccionar una Familia o Tipo Producto" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-2 control-label">Nivel:</label>
                        <div class="col-lg-4">

                            <select id="cbx_nivel" name="cbx_nivel"  class="form-control required"
                                    mensaje ="Debe seleccionar un Nivel" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Versión:</label>
                        <div class="col-lg-4">
                            <select class="form-control required" id="cbx_version" name="cbx_version"
                                    multiple="multiple"
                                    mensaje ="Debe seleccionar una Versión" requerido="true"
                                    ></select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Línea Negocio:</label>
                        <div class="col-lg-4">
                            <select id="cbx_sub_linea_negocio" name="cbx_sub_linea_negocio"  class="form-control required"
                                    mensaje ="Debe seleccionar un Línea de Negocio" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                    <label class="col-lg-2 control-label">Grado Bono:</label>
                        <div class="col-lg-4">
                            <select class="form-control" id="cbx_slt_grado_curso" name="cbx_slt_grado_curso"
                                    mensaje ="Debe seleccionar un Grado Bono" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar_curso" id="btn_registrar_curso">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" name="btn_reset" id="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>
