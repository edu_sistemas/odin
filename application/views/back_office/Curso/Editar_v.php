<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-12-02 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar Modalidad</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">

                <form class="form-horizontal" id="frm_curso_editar">

                    <p>Modifique Curso</p>
                    <div class="form-group"><label class="col-lg-2 control-label">Nombre del curso:</label>
                        <div class="col-lg-4">
                            <input type="hidden" name="id_curso_v" id="id_curso_v" class="form-control" value="<?php echo $data_curso[0]['id_curso']; ?>">
                            <input type="text" placeholder="Nombre" requerido="true" mensaje="Debe ingresar nombre del curso" name="nombre_curso_v" id="nombre_curso_v" class="form-control" value="<?php echo $data_curso[0]['nombre_curso']; ?>">
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-2 control-label">Descripción del curso</label>
                        <div class="col-lg-6">
                            <textarea class="form-control" requerido="true" mensaje="Debe ingresar la descripción del curso" id="descripcion_curso_v" name="descripcion_curso_v" maxlength="100" placeholder=" Ingresar descripción"><?php echo $data_curso[0]['descripcion_curso']; ?></textarea>
                            <p id="text-out">0/200 caracteres restantes</p>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Duración del curso:
                        </label>
                        <div class="col-lg-4">
                            <input tipe="number" class="form-control" requerido="true" mensaje="Debe ingresar la cantidad de horas que dura el curso" 
                                   id="duracion_curso_v" name="duracion_curso_v" maxlength="100" placeholder="Ingresar horas" 
                                   value="<?php echo $data_curso[0]['duracion_curso']; ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Modalidad:</label>
                        <div class="col-lg-4">
                            <input type="hidden" id="id_modalidad" value="<?php echo $data_curso[0]['id_modalidad']; ?>">
                            <select id="cbx_modalidad" name="cbx_modalidad"  class="form-control required" mensaje ="Debe seleccionar una modalidad" requerido="true">
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Código Sence:</label>
                        <div class="col-md-4"  >
                            <input type="hidden" id="id_codigo_sence" value="<?php echo $data_curso[0]['id_codigo_sence']; ?>">
                            <select id="cbx_codigo_sence" name="cbx_codigo_sence" 
                                    class="form-control required" requerido="true"
                                    mensaje="Debe seleccionar un Codigo Sence"
                                    >
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-2 control-label">Familia o Tipo Producto:</label>
                        <div class="col-lg-4">
                            <input type="hidden" id="id_familia" value="<?php echo $data_curso[0]['id_familia']; ?>">
                            <select id="cbx_familia" name="cbx_familia"  class="form-control required"
                                    mensaje ="Debe seleccionar una Familia o Tipo Producto" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group"><label class="col-lg-2 control-label">Nivel:</label>
                        <div class="col-lg-4">
                            <input type="hidden" id="id_nivel" value="<?php echo $data_curso[0]['id_nivel']; ?>">
                            <select id="cbx_nivel" name="cbx_nivel"  class="form-control required"
                                    mensaje ="Debe seleccionar un Nivel" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Versión:</label>
                        <div class="col-lg-4">
                            <input type="hidden" id="id_version" value="<?php echo $data_curso[0]['id_version']; ?>">
                            <select class="form-control required" id="cbx_version" name="cbx_version" multiple="multiple"
                                    mensaje ="Debe seleccionar una Versión" requerido="true"
                                    ></select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Línea Negocio:</label>
                        <div class="col-lg-4">
                            <input type="hidden" id="id_sub_linea_negocio" value="<?php echo $data_curso[0]['id_sub_linea_negocio']; ?>">
                            <select id="cbx_sub_linea_negocio" name="cbx_sub_linea_negocio"  class="form-control required"
                                    mensaje ="Debe seleccionar un Línea de Negocio" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Grado Bono:</label>
                        <div class="col-lg-4">
                            <input type="hidden" id="id_slt_grado_curso" value="<?php echo $data_curso[0]['grado_bono']; ?>">
                            <select class="form-control" id="cbx_slt_grado_curso" name="cbx_slt_grado_curso"
                                    mensaje ="Debe seleccionar un Grado Bono" requerido="true">
                                <option></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_modalidad" id="btn_editar_curso">Actualizar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
