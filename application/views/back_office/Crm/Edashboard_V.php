<?php
/**
 * edashboard_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-08-01 [Jimmy Espinoza] <jimmy.espinoza@vmica.com>
 * Fecha creacion:	2017-08-01 [Jimmy Espinoza] <jimmy.espinoza@vmica.com>
 */

?>

<input type="hidden" value="<?php echo $ejecutivo;?>" name="Var_ejecutivo" id="Var_ejecutivo">

<div class="wrapper wrapper-content animated fadeInRight">
	<div id="divEjecutivos">
		<div class="ibox-content" style="padding-top: 15px; padding-bottom: 1px;">
			<div class="row">
				<div class="col-md-12">

					<div style="width: 100%;">
						<div class="row" style="height: 50%; padding: 0px">

							<div class="col-md-5 pull-left">
								<div class="col-sm-2">
									<label class="control-label" for="ejecutivos">Ejecutivo:</label>
								</div>
								<div class="col-sm-6">
									<select class="form-control" id="ejecutivos" name="ejecutivos"></select>
									<input type="hidden" name="equipo" id="equipo" value="0">
								</div>
							</div>

							<div class="col-md-5 pull-right">
								<div class="col-md-3">
									<label class="control-label pull-right" for="ejecutivos">Seleccione Mes:</label>
								</div>
								<div class="col-md-6">
									<div class="form-group" id="data1">									
										<div class="input-group date" style="padding-bottom: 10px;">
											<span class="input-group-addon"><i class="fa fa-calendar" onclick="fecha()";></i></span><input type="text" class="form-control" id="fecha_sdashboard" name="fecha_sdashboard" value="<?php echo date("Y-m");?>"/>
										</div>
									</div>
								</div>
								<div class="col-md-3 pull-right">
									<button type="button" value="button" class="btn btn-primary" id="btn-fecha-sdashboard">Filtrar</button>
								</div>								
							</div>

						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!--nuevo-->
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12" style=" padding-left: 0px; padding-right: 0px;">
				<div class="ibox-content" style="padding-top: 15px; padding-bottom: 1px;">
					<div style="width: 100%;">							
						<div class="row" style="height: 50%; padding: 0px">
							<div class="col-md-4">

								<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">								
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="widget style1 lazur-bg text-center" style="padding:5px;">
											<span id="Var_mes_ano"></span>
											<div class="row">
												<div class="col-xs-12 text-center">
													<span><small>Meta</small></span>
													<h3 id="Var_mensual_meta" style="font-size: 15px;"></h3> 
													<span><small>Venta</small> </span>
													<h3  id="Var_mensual_venta" style="font-size: 15px;"></h3>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">
									<div class="col-md-12" style="padding-bottom:10px;padding-left: 0px;padding-right: 0px;">
										<div id="Var_icon_mes" style="height:108px">
											<div class="row">
												<div class="col-xs-12 text-right pull-right">
													<span id="Var_mes"></span>
													<h3 id="Var_mensual_porcen" style="font-size: 30px;"><small style="color: #fff;">%</small></h3>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">								
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="widget style1 lazur-bg text-center" style="padding:5px;">
											<span id="Var_mes_anox"></span>
											<div class="row">
												<div class="col-xs-12 text-center">
													<span><small>Meta</small></span>
													<h3 id="Var_mensual_metax" style="font-size: 15px;"></h3> 
													<span><small>Venta</small> </span>
													<h3  id="Var_mensual_ventax" style="font-size: 15px;"></h3>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-md-4">

								<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="widget style1 lazur-bg  text-center" style="padding:5px;">
											<span id="Var_ano"></span>
											<div class="row">
												<div class="col-xs-3 text-center">													
												</div>
												<div class="col-xs-12 text-center">
													<span><small>Meta</small></span>
													<h3 id="Var_acum_meta" style="font-size: 15px;"></h3>
													<span><small>Venta</small></span>
													<h3 id="Var_acum_venta" style="font-size: 15px;"></h3> 
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">							
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div id="Var_icon_acum" style="height:108px">
											<div class="row">
												<div class="col-xs-12 text-right pull-right">
													<span id="Var_ano_porcen"></span>
													<h3 id="Var_acum_porcen" style="font-size: 30px;"><small style="color: #fff;">%</small></h3>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="widget style1 lazur-bg text-center" style="padding:5px;">
											<span><small>Dif. Meta vs Venta</small></span>
											<div class="row">
												<div class="col-xs-3 text-center">													
												</div>
												<div class="col-xs-12 text-center">
													<span><small>Dif. Mensual</small></span>
													<h3 id="Var_dif_mes" style="font-size: 15px;"></h3>
													<span><small>Dif. Acum. Anual</small></span>
													<h3 id="Var_dif_ano" style="font-size: 15px;"></h3> 
												</div>
											</div>
										</div>
									</div>
								</div>								
							</div>


							<div class="col-md-4">
								<div class="col-md-6" style="padding-left: 3px;padding-right: 3px;">
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="widget style1 lazur-bg text-center" style="padding:5px; height:108px;  padding-top: 10px;">
											<span><small>Meta Próximo Mes</small></span>
											<div class="row">
												<div class="col-xs-3 text-center">													
												</div>
												<div class="col-xs-12 text-center">
													<h3 id="Var_meta_sig_mes" style="font-size: 20px;"><small style="color: #fff;">%</small></h3>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<div class="col-md-2" style="padding-left: 3px;padding-right: 3px">
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="widget style1 lazur-bg text-center" style="padding:5px; height:108px">
											<div class="row">
												<div class="text-center" style="padding-top: 10px;">													
													<span><small><i class="fa fa-phone fa-3x"></i></small></span>
													<h3 id="Var_q_llamadas" style="font-size: 15px; padding-top: 10px;"></h3> 
													</div>									
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-left: 3px;padding-right: 3px;">
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="widget style1 lazur-bg text-center" style="padding:5px; height:108px">
											<div class="row">
												<div class="text-center" style="padding-top: 10px">													
													<span><small><i class="fa fa-envelope fa-3x"></i></small></span>
													<h3 id="Var_q_correos" style="font-size: 15px; padding-top: 10px;"></h3> 
													</div>									
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2" style="padding-left: 3px;padding-right: 3px;">
									<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
										<div class="widget style1 lazur-bg text-center" style="padding:5px; height:108px">
											<div class="row">
												<div class="text-center" style="padding-top: 10px">													
													<span ><small><i class="fa fa-calendar fa-3x"></i></small></span>
													<h3 id="Var_q_reuniones" style="font-size: 15px; padding-top: 10px;"></h3> 
													</div>									
											</div>
										</div>
									</div>
								</div>																							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>



	<div class="col-md-12">
		<div class="row">
			<!--METAS-->
			<div class="col-md-4"  style="padding: 10px 0px 20px 0px">
				<div class="ibox-title">
					<h5>Metas</h5>
					<button type="button" name="btn_generar_excel_metas" id="btn_generar_excel_metas" class="btn btn-outline btn-primary" style="margin-top:-8px; margin-left: 10px;"><i class="fa fa-download"></i> Descargar</button>

				</div>
				<div class="ibox-content table-responsive" style="height :275px; overflow: auto; cursor: pointer; border-bottom: 20px solid #fff;">
					<table class="table table-hover" style="width: 100%" id="tblclasifMetas">
						<thead>
							<th  style="width: 40%">Empresa (Holding)</th>
							<th  style="width: 30%">Mensual</th>
							<th  style="width: 30%">Acumulado</th>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
			<!--FIN METAS-->

			<!--VENTAS-->
			<div class="col-md-4" style="padding:10px 5px 20px 5px;">
				<div class="ibox-title">
					<h5>Clasificación por Ventas</h5>
				</div>
				<div class="ibox-content" style="height: 275px;">
					<div>
						<canvas id="chartPieClasifVenta" style="cursor:pointer"></canvas>
					</div>
				</div>
			</div>
			<!--FIN VENTAS-->

			<!--GESTION-->
			<div class="col-md-4" style="padding:10px 0px 20px 0px;">
				<div class="ibox-title">
					<h5>Clasificación por Gestión</h5>
				</div>
				<div class="ibox-content" style="height: 275px;">
					<div>
						<canvas id="chartPieClasifGest" style="cursor:pointer"></canvas>
					</div>
				</div>
			</div>
			<!--FIN GESTION-->
		</div>
	</div>

	<div class="col-md-12" style="padding-bottom: 30px;">
		<div class="row">
			<!--COMPROMISO-->
			<div class="col-md-4" style="padding: 0px 0px 20px 0px">
				<div class="ibox-title">
					<h5>Compromisos</h5>
					<button type="button" name="btn_generar_excel_compromisos" id="btn_generar_excel_compromisos" class="btn btn-outline btn-primary" style="margin-top:-8px; margin-left: 10px;"><i class="fa fa-download"></i> Descargar</button>
					<input type="hidden" value="" id="id_compromiso_hidden">

				</div>
				<div class="ibox-content table-responsive" style="height: 255px; width: 100%; overflow: auto; border-bottom: 20px solid #fff;">
					<table class="table table-hover" id="tblclasifCompromiso" style="width: 800px; cursor: pointer;">
						<thead>
							<th  style="width: 20%">Cliente</th>
							<th  style="width: 20%">Descripción</th>
							<th  style="width: 20%">Evento</th>
							<th  style="width: 20%">Fase</th>								
							<th  style="width: 10%">Fecha Compromiso</th>
							<th  style="width: 10%">Fecha Acción</th>
						</thead>
						<tbody></tbody>
					</table>					
				</div>
			</div>
			<!--FIN COMPROMISO-->
			<div class="col-md-8" style="height: 100%; padding-left: 10px; padding-right: 0px; padding-bottom: 10px">
				<div style="height: 100%; padding-left: 0px;">
					<div id="tabla_venta">
						<div class="ibox-title">

							<h5>Empresas (Holding) por Ventas</h5>
							<button type="button" name="btn_generar_excel_empresas_ventas" id="btn_generar_excel_empresas_ventas" class="btn btn-outline btn-primary" style="margin-top:-8px; margin-left: 10px;"><i class="fa fa-download"></i> Descargar</button>

						</div>
						<div class="ibox-content table-responsive" style="height: 255px; overflow: auto; border-bottom: 20px solid #fff;">
							<table class="table table-striped" id="tblclasifVenta">
								<thead>
									<th style="width: 40%">Empresa (Holding)</th>
									<th style="width: 30%">Ultima Venta</th>
									<th style="width: 20%">Fecha</th>
									<th style="width: 10%">Monto</th>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
				<div style="height: 100%; padding-left: 0px; padding-bottom: 10px">
					<div id="tabla_gestion">
						<div class="ibox-title">
							<h5>Empresas (Holding) por Gestiones</h5>
							<button type="button" name="btn_generar_excel_empresas_gestiones" id="btn_generar_excel_empresas_gestiones" class="btn btn-outline btn-primary" style="margin-top:-8px; margin-left: 10px;"><i class="fa fa-download"></i> Descargar</button>

						</div>
						<div class="ibox-content" style="height: 255px; overflow: auto; border-bottom: 20px solid #fff">
							<table class="table table-striped" id="tblclasifGestion">
								<thead>
									<th style="width: 40%">Empresa (Holding)</th>
									<th style="width: 40%">Tipo Gestión</th>
									<th style="width: 20%">Fecha</th>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
