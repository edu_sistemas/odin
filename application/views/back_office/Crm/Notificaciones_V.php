<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>

<div class="wrapper wrapper-content" id="div_load" name="div_load">
<div class="ibox-title">
  <h5>Sus Notificaciones</h5>
</div> <br> <br>
    <div class="row animated fadeInRight">
        <div class="col-lg-12">

          <div class="ibox float-e-margins">

            <div class="ibox-content" id="ibox-content" style="height:500px; overflow:auto;">

                <div id="vertical-timeline" class="vertical-container dark-timeline">

                    <?php 
                        for($i=0;$i<count($notificaciones_todas);$i++){ 

                                        $id_notificacion = $notificaciones_todas[$i]['id_notificacion'];
                                        $fa        = $notificaciones_todas[$i]['fa_view'];
                                        $fondo     = $notificaciones_todas[$i]['fondo_view'];
                                        $link      = base_url('/back_office').'/'.$notificaciones_todas[$i]['link_view'];



                                        $titulo    = $notificaciones_todas[$i]['desc_tipo_notificacion'];
                                        $subtitulo = $notificaciones_todas[$i]['desc_subtipo_notificacion'];
                                        $fecha     = $notificaciones_todas[$i]['fecha_notificacion'];
                                        $estado    = $notificaciones_todas[$i]['estado'];
                                        $url       = $_SERVER["REQUEST_URI"];
                                        $buscarindex = strpos($url, 'index');

                                        // #f5f5f5 --> color estado revisado
                                        // #d2d2d2 --> color estado no visto

                                        $color   = "#d2d2d2";

                                        if ($estado == 1){
                                            $color = "#f5f5f5";                                         
                                        }

                                        //'background: #f5f5f5'


                            $cuerpo = "<div class='vertical-timeline-block'>
                                        <div class='vertical-timeline-icon ".$fondo."'>
                                            <i class=".$fa."></i>
                                        </div>
                                        <a href='#' onclick=SetNotificacion(".$id_notificacion.",0,'".$link."',".$buscarindex.")>                            
                                          <div class='vertical-timeline-content' style='background: ".$color."'>
                                            <h2>".$titulo."</h2>
                                            <p>".$subtitulo."</p>
                                            <span class='vertical-date' style='color: #1ab394'>".$fecha."</span>
                                          </div>
                                        </a>
                                     </div>"; 
                                     echo $cuerpo;  
                        }
                        
                    ?>

                </div>
            </div>  
        </div>
      </div>
    </div>
</div>
			
