<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>

<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12" style="padding-left: 0px; padding-right: 0px;">
					<div class="ibox-content" style="padding-top: 15px; padding-bottom: 1px;">
						<div class="row">
							<div class="col-md-12">

								<div style="width: 100%;">
									<div class="row" style="height: 50%; padding: 0px">
										<!--
										<div class="col-md-5 pull-left">
											<div class="col-sm-2">
												<label class="control-label" for="ejecutivos">Ejecutivo:</label>
											</div>
											<div class="col-sm-6">
												<select class="form-control" id="ejecutivos" name="ejecutivos"></select>
												<input type="hidden" name="equipo" id="equipo" value="0">
											</div>
										</div>
									-->
										<div class="col-md-5 pull-right">
											<div class="col-md-4">
												<label class="control-label pull-right" for="ejecutivos">Seleccione Mes:</label>
											</div>
											<div class="col-md-5">
												<div class="form-group" id="data1">												
													<div class="input-group date" style="padding-bottom: 10px;">
														<span class="input-group-addon"><i class="fa fa-calendar" onclick="fecha()";></i></span><input type="text" class="form-control" id="fecha_sdashboard" name="fecha_sdashboard" value="<?php echo date("Y-m");?>"/>
													</div>											
												</div>
											</div>
											<div class="col-md-3 pull-right">
												<button type="button" value="button" class="btn btn-primary" id="btn-fecha-sdashboard">Filtrar</button>
											</div>								
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>

					<div class="ibox-content">
						<div style="width: 100%;">							
							<div class="row" style="height: 50%; padding: 0px">
								<div class="col-md-4">
									<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">								
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div class="widget style1 lazur-bg text-center" style="padding:5px;">
												<span id="Var_mes_ano"></span>
												<div class="row">
													<div class="col-xs-12 text-center">
														<span><small>Meta</small></span>
														<h3 id="Var_mensual_meta" style="font-size: 15px;"></h3> 
														<span><small>Venta</small> </span>
														<h3  id="Var_mensual_venta" style="font-size: 15px;"></h3>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">
										<div class="col-md-12" style="padding-bottom:10px;padding-left: 0px;padding-right: 0px;">
											<div id="Var_icon_mes" style="height:108px">
												<div class="row">
													<div class="col-xs-12 text-right pull-right">
														<span id="Var_mes"></span>
														<h3 id="Var_mensual_porcen" style="font-size: 30px;"><small style="color: #fff;">%</small></h3>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">								
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div class="widget style1 lazur-bg text-center" style="padding:5px;">
												<span id="Var_mes_anox"></span>
												<div class="row">
													<div class="col-xs-12 text-center">
														<span><small>Meta</small></span>
														<h3 id="Var_mensual_metax" style="font-size: 15px;"></h3> 
														<span><small>Venta</small> </span>
														<h3  id="Var_mensual_ventax" style="font-size: 15px;"></h3>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
								<div class="col-md-4">
									<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div class="widget style1 lazur-bg  text-center" style="padding:5px;">
												<span id="Var_ano"></span>
												<div class="row">
													<div class="col-xs-3 text-center">													
													</div>
													<div class="col-xs-12 text-center">
														<span><small>Meta</small></span>
														<h3 id="Var_acum_meta" style="font-size: 15px;"></h3>
														<span><small>Venta</small></span>
														<h3 id="Var_acum_venta" style="font-size: 15px;"></h3> 
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">							
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div id="Var_icon_acum" style="height:108px">
												<div class="row">
													<div class="col-xs-12 text-right pull-right">
														<span id="Var_ano_porcen"></span>
														<h3 id="Var_acum_porcen" style="font-size: 30px;"><small style="color: #fff;">%</small></h3>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4" style="padding-left: 3px;padding-right: 3px;">
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div class="widget style1 lazur-bg text-center" style="padding:5px;">
												<span><small>Dif. Meta vs Venta</small></span>
												<div class="row">
													<div class="col-xs-3 text-center">													
													</div>
													<div class="col-xs-12 text-center">
														<span><small>Dif. Mensual</small></span>
														<h3 id="Var_dif_mes" style="font-size: 15px;">$ 0</h3>
														<span><small>Dif. Acum. Anual</small></span>
														<h3 id="Var_dif_ano" style="font-size: 15px;">$ 0</h3> 
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>								
								<div class="col-md-4">
									<div class="col-md-6" style="padding-left: 3px;padding-right: 3px;">
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div class="widget style1 lazur-bg text-center" style="padding:5px; height:108px;  padding-top: 10px;">
												<span><small>Meta Próximo Mes</small></span>
												<div class="row">
													<div class="col-xs-3 text-center">													
													</div>
													<div class="col-xs-12 text-center">
														<h3 id="Var_meta_sig_mes" style="font-size: 20px;">$ 0</h3>
													</div>
												</div>
											</div>
										</div>
									</div>	
									<div class="col-md-2" style="padding-left: 3px;padding-right: 3px">
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div class="widget style1 lazur-bg text-center" style="padding:5px; height:108px">
												<div class="row">
													<div class="text-center" style="padding-top: 10px;">													
														<span><small><i class="fa fa-phone fa-3x"></i></small></span>
														<h3 id="Var_q_llamadas" style="font-size: 15px; padding-top: 10px;">0</h3> 
													</div>									
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2" style="padding-left: 3px;padding-right: 3px;">
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div class="widget style1 lazur-bg text-center" style="padding:5px; height:108px">
												<div class="row">
													<div class="text-center" style="padding-top: 10px">													
														<span><small><i class="fa fa-envelope fa-3x"></i></small></span>
														<h3 id="Var_q_correos" style="font-size: 15px; padding-top: 10px;">0</h3> 
													</div>									
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-2" style="padding-left: 3px;padding-right: 3px;">
										<div class="col-md-12" style="padding-left: 0px;padding-right: 0px;">
											<div class="widget style1 lazur-bg text-center" style="padding:5px; height:108px">
												<div class="row">
													<div class="text-center" style="padding-top: 10px">													
														<span><small><i class="fa fa-calendar fa-3x"></i></small></span>
														<h3 id="Var_q_reuniones" style="font-size: 15px; padding-top: 10px;">0</h3> 
													</div>									
												</div>
											</div>
										</div>
									</div>																							
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6" style=" padding-left: 0px; padding-right: 0px;">
					<div class="ibox-title" class="col-md-1">
						<h5>Grupos Ejecutivos<small></small></h5>
						<div class="col-md-8" >
							<button type="button" name="btn_generar_excel_grupos_ejecutivos" id="btn_generar_excel_grupos_ejecutivos" class="btn btn-outline btn-primary" style="margin-top: -8px;">
								<i class="fa fa-download"></i> Descargar</button>
							</div>	
						</div>


						<div class="ibox-content">
							<div class="ibox-content" style="height	:288px; overflow: auto; border-style: none;">
								<table id="tblListGruposEjecutivos" class="table table-hover" style="width: 100%; cursor: pointer; border-bottom: 20px solid #fff;">
									<thead>
										<th style="width: 15%; text-align:right;">Grupo</th>
										<th style="text-align:right;">% Mes</th>
										<th style="text-align:right;">% Acum.</th>
										<th style="text-align:right;"><i class="fa fa-money"></i> Mes</th>
										<th style="text-align:right;"><i class="fa fa-money"></i> Acum.</th>
										<th style="text-align:right;"><i class="fa fa-money"></i> Meta Anual</th>			
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>  
						</div>
					</div>
					<div class="col-md-6" style=" padding-left: 0px; padding-right: 0px;">
						<div class="ibox-title" class="col-md-1">
							<h5>Clasificación por Ejecutivos<small></small></h5>
							<div class="col-md-8" >
								<button type="button" name="btn_generar_excel_clasificacion_ejecutivos" id="btn_generar_excel_clasificacion_ejecutivos" class="btn btn-outline btn-primary" style="margin-top: -8px;">
									<i class="fa fa-download"></i> Descargar</button>
									<input type="hidden" value="" id="id_grupo_hidden" >
								</div>	
							</div>			
							<div class="ibox-content">
								<div class="ibox-content" style="height	:288px; overflow: auto; border-style: none;">
									<table id="tblSelectGruposEjecutivos" class="table table-hover" style="width: 100%; cursor: pointer; border-bottom: 20px solid #fff;">
										<thead>
											<th style="width: 20%; text-align:right;">Ejecutivo</th>
											<th style="text-align:right;">% Mes</th>
											<th style="text-align:right;">% Acum.</th>
											<th style="text-align:right;"><i class="fa fa-money"></i> Mes</th>
											<th style="text-align:right;"><i class="fa fa-money"></i> Acum.</th>
											<th style="text-align:right;"><i class="fa fa-money"></i> Meta Anual</th>	
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-6" style=" padding-left: 0px; padding-right: 0px;">
							<div class="ibox-title" class="col-md-1">
								<h5>Ventas por Rubro<small></small></h5>
								<div class="col-md-8" >
									<button type="button" name="btn_generar_excel_ventas_rubro" id="btn_generar_excel_ventas_rubro" class="btn btn-outline btn-primary" style="margin-top: -8px;">
										<i class="fa fa-download"></i> Descargar</button>
									</div>
								</div>				
								<div class="ibox-content">
									<div style="height: 240px; overflow: auto; border-bottom: 20px solid #fff;">
										<table id="tblRubro" class="table table-hover" style="width: 100%;">
											<thead>
												<tr>
													<th style="width: 40%">Rubro</th>
													<th style="width: 30%;text-align:right;"><i class="fa fa-money"></i> Mes</th>
													<th style="width: 30%;text-align:right;"><i class="fa fa-money"></i> Acum.</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>  
								</div>
							</div>
							<div class="col-md-6" style=" padding-left: 0px; padding-right: 0px;">
								<div class="ibox-title" class="col-md-1">
									<h5>Ventas por Línea de Negocio<small></small></h5>
									<div class="col-md-7">
										<button type="button" name="btn_generar_excel_linea_negocio" id="btn_generar_excel_linea_negocio" class="btn btn-outline btn-primary" style="margin-top: -8px;">
											<i class="fa fa-download"></i> Descargar</button>
										</div>
									</div>				
									<div class="ibox-content">
										<div style="height: 240px; overflow: auto; border-bottom: 20px solid #fff;">
											<table id="tbllinNegocio" class="table table-hover" style="width: 100%;">
												<thead>
													<tr>
														<th>Linea</th>
														<th>Sublinea</th>
														<th style="text-align:right;"><i class="fa fa-money"></i> Mes</th>
														<th style="text-align:right;"><i class="fa fa-money"></i> Acum.</th>
													</tr>
												</thead>
												<tbody>
												</tbody>
											</table>
										</div> 
									</div>
								</div>
							</div>	
						</div>

						<div class="col-md-12" style="padding: 0px 0px 30px 0px;">
							<div class="row">	
								<div class="col-lg-12">
									<div class="ibox float-e-margins">
										<div class="ibox-title">
											<h5>Grafico De Ventas:  
												<strong id="total_venta_acumulada"> </strong>
											</h5>
										</div>
										<div class="ibox-content">
											<div>
												<canvas id="lineChart_ventas" height="60"></canvas>
											</div>
										</div>
									</div>
								</div>	
							</div>	
						</div>

					</div>	
				</div>




