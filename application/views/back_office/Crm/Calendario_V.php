<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<div class="wrapper wrapper-content">
  <div class="row animated fadeInDown">
    <div class="col-lg-12">
      <div class="ibox-title">
        <h5>Programación de Reuniones</h5>
      </div>
      <div id="divEjecutivos">
        <div class="ibox-content">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-1">
                  <label class="control-label" for="ejecutivos">Ejecutivo:</label>
                </div>
                <div class="col-sm-4">
                  <select class="form-control" id="ejecutivos" name="ejecutivos"></select>
                  <input type="hidden" name="equipo" id="equipo" value="0">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="ibox-title">
        <table style="width: 30%">
          <tr>
            <td style="width: 5px">
             <b>Leyenda: </b>   
           </td>
           <td style="text-align: center">
             <i class="fa fa-check-circle-o" style="color:#337ab7"></i><b> : Acción</b>
           </td>
           <td style="text-align: center">
             <i class="fa fa-calendar" style="color:#dd5f1a"></i><b> : Compromiso</b> 
           </td>
           <td style="text-align: center">
             <i class="fa fa-birthday-cake" style="color:#00bcd4"></i><b> : Cumpleaños</b> 
           </td>
         </tr>
       </table>
       <br>
       <div class="clearfix"></div>
       <div>
        <div class="ibox-content" id="div_general">
          <div id="calendar"></div>
        </div>
      </div>
    </div>
  </div>
</div>

