<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre ] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<style>
/*select2*/

.select2 {
    width: 100% !important;
}

.select2-container--open {
    z-index: 2060;
}

.swal2-container {
    z-index: 2061;
}

#ref_mod,
#ref_mod_a,
#ref_mod_dat,
#ref_mod_dat_a {
    font-size: larger;
}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-marfins">

                <div class="ibox-title">
                    <h5>Editor de Acción</h5>
                </div>

                <div class="ibox-content">
                    <div class="row">

                        <div class="col-md-3 b-r">
                            <h4>Cliente</h4>
                            <h1>
                                <?php echo $datos_cliente[0]['razon_social_empresa'] ?>

                            </h1>
                            <h4>Nombre contacto empresa</h4>
                            <h5>
                                <?php echo $datos_cliente[0]['nombre_contacto'] ?>
                            </h5>
                            <h4>Correo Contacto</h4>
                            <h5>
                                <?php echo $datos_cliente[0]['correo_contacto'] ?>
                            </h5>
                            <h4>Telefono Contacto</h4>
                            <h5>
                                <?php echo $datos_cliente[0]['telefono_contacto'] ?>
                            </h5>
                        </div>
                        <div class="col-md-4 b-r">
                            <h3 class="m-t-none m-b">Última Gestión</h3>
                            <form role="form">
                                <label><small> Gestión</small></label><br>
                                <label for="" style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['desc_accion'])){echo $datos_cliente[0]['desc_accion'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Fase</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['fase'])){echo $datos_cliente[0]['fase'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Tipo Acción</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['tipo_accion'])){echo $datos_cliente[0]['tipo_accion'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Curso</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['curso'])){echo $datos_cliente[0]['curso'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Modalidad</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['modalidad'])){echo $datos_cliente[0]['modalidad'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Asunto Gestión</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['asunto_accion'])){echo $datos_cliente[0]['asunto_accion'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Comentario</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['comentario_accion'])){echo $datos_cliente[0]['comentario_accion'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Fecha Gestión</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['fecha_accion'])){echo $datos_cliente[0]['fecha_accion'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Compromiso</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['compromiso_accion'])){echo $datos_cliente[0]['compromiso_accion'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label><br>

                                <label><small> Fecha Compromiso</small></label><br>
                                <label for=""  style="color:#9b9d9eed;">
                                    <?php if (isset($datos_cliente[0]['fecha_compromiso'])){echo $datos_cliente[0]['fecha_compromiso'];}else{echo "<span style='color:#afafaf;'>No asignado</span>";} ?>
                                </label>
                            </form>
                        </div>

                        <div class="col-md-4" class="container-fluid">

                            <div class="col-xs-6 col-md-4">
                                <div class="ibox">
                                    <div>
                                        <div class="text-center">
                                            <button type="button" class="btn btn-outline btn-primary dim" style="height: 83px; width: 100px;" onclick="modalllamada();">
                                                <h4><i class="fa fa-phone text-center"></i><br>Llamada</h4>
                                            </button>
                                        </div>

                                        <div class="modal inmodal" id="myModal1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content animated bounceInRight">

                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            <span aria-hidden="true">&times;</span>
                                                            <span class="sr-only">Close</span>
                                                        </button>
                                                        <i class="fa fa-phone modal-icon"></i>
                                                        <h4 class="modal-title">Ingrese Llamada</h4>
                                                        <small class="font-bold">Ingrese asunto, comentario y fecha.</small>
                                                    </div>

                                                    <div class="modal-body">
                                                        <form class="form-horizontal" id="form_llamada" name="form_llamada">
                                                            <input type="hidden" name="idempresallamada" id="idempresallamada" value="<?php echo $datos_cliente[0]['id_empresa'];?>">
                                                            <div class="col-md-6 col-xs-6">
                                                                <label>
                                                                    <h2>Seleccione Fase</h2>
                                                                </label>
                                                                <select class="form-control" id="llamada_seleccione_fase_cbx" name="llamada_seleccione_fase_cbx" requerido="true" mensaje="Seleccione Fase">
                                                                </select>

                                                                <label>
                                                                    <h2>Tipo de Llamada</h2>
                                                                </label>
                                                                <select class="form-control" id="tipo_llamada_cbx" name="tipo_llamada_cbx" requerido="true" mensaje="Seleccione Tipo Llamada">
                                                                </select>

                                                                <label>
                                                                    <h2>Fecha Llamada</h2>
                                                                </label>
                                                                <div class="form-group" id="data1" style="cursor: pointer; padding-left: 12px;">
                                                                    <div class="input-group date col-md-11">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-calendar" onclick="fecha(1)"></i>
                                                                        </span>
                                                                        <input type="text" id="llamada_fecha_llamada" name="llamada_fecha_llamada" class="form-control" value="<?php echo date("d-m-Y");?>"/>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-xs-6 col-md-6">
                                                                <label for="">
                                                                    <h2>Modalidad</h2>
                                                                </label>
                                                                <select class="form-control" id="modalidad_llamada_cbx" name="modalidad_llamada_cbx" requerido="true" mensaje="Seleccione modalidad">
                                                                </select>
                                                                <label>
                                                                    <h2>Producto</h2>
                                                                </label>
                                                                <select class="form-control" id="seleccione_producto_cbx_llamada" name="seleccione_producto_cbx_llamada" requerido="true"
                                                                mensaje="Seleccione producto">
                                                                <option></option>
                                                            </select>

                                                        </div>
                                                        <div class="col-xs-12 col-md-12">
                                                            <div style="width: 100%">
                                                                <h2 class="text-center">Contacto</h2>
                                                                <select id="llamada_contacto_origen_cbx" name="llamada_contacto_origen_cbx" class="form-control" requerido="true" mensaje="Seleccione Contacto">

                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-md-12">
                                                            <h2 class="text-center">Asunto</h2>
                                                            <textarea name="llamada_asunto_box" id="llamada_asunto_box" cols="75" rows="2" style="width: 100%;" requerido="true" mensaje="Debe ingresar Asunto"></textarea>
                                                            <h2 class="text-center">Comentario</h2>
                                                            <textarea name="llamada_comentario_box" id="llamada_comentario_box" cols="75" rows="2" style="width: 100%;" requerido="true"
                                                            mensaje="Ingrese comentario"></textarea>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-6 col-md-6" style="border-top: 1px solid #e5e5e5; margin-top: 10px; padding-top: 10px;">
                                                                <label>
                                                                    <h2>Compromiso</h2>
                                                                </label>
                                                                <select class="form-control" id="llamada_compromiso_cbx" name="llamada_compromiso_cbx" requerido="true" mensaje="Seleccione Compromiso">

                                                                </select>
                                                            </div>
                                                            <div class="col-xs-6 col-md-6" style="border-top: 1px solid #e5e5e5; margin-top: 10px; padding-top: 10px;">
                                                                <label>
                                                                    <h2>Fecha Compromiso </h2>
                                                                </label>
                                                                <div class="form-group" id="data1" style="cursor: pointer;">
                                                                    <div class="input-group date col-md-11">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-calendar" onclick="fecha(1)"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control" id="llamada_fecha_compromiso" name="llamada_fecha_compromiso" value="<?php echo date("d-m-Y");?>" />
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-xs-12 col-md-12" id="div_otros_llamada" style="display: none;">
                                                              <label><h2>Comentario Compromiso</h2></label>
                                                              <div class="form-group">
                                                                <textarea name="comentario_otros_llamada" id="comentario_otros_llamada" rows="3" style="width: 100%;" requerido="true" mensaje="Debe ingresar comentario compromiso"></textarea>
                                                            </div>
                                                        </div>

                                                    </div>

                                                </form>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                                                <button id="btn-llamada" type="submit" value="submit" class="btn btn-primary" id="btn-llamada">Guardar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>




                    <div class="col-xs-6 col-md-4">
                        <div class="ibox">
                            <div>
                                <div class="text-center">
                                    <button type="button" class="btn btn-outline btn-primary dim" onclick="modalcorreo();" style="height: 83px; width: 100px;">
                                        <h4>
                                            <i class="fa fa-envelope"></i>
                                            <br> Correo </h4>
                                        </button>
                                    </div>
                                    <div class="modal inmodal" id="myModal2" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content animated bounceInRight">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        <span aria-hidden="true">&times;</span>
                                                        <span class="sr-only">Close</span>
                                                    </button>
                                                    <i class="fa fa-envelope modal-icon"></i>
                                                    <h4 class="modal-title">Ingrese Correo</h4>
                                                    <small class="font-bold">Ingrese asunto, comentario y fecha.</small>
                                                </div>
                                                <div class="modal-body">


                                                    <form class="form-horizontal" id="form_correo" name="form_correo">
                                                        <input type="hidden" name="idempresacorreo" id="idempresacorreo" value="<?php echo $datos_cliente[0]['id_empresa'];?>">

                                                        <div class="col-md-12 col-xs-12">
                                                            <div class="row">

                                                                <div class="col-md-6 col-xs-6 b-r">
                                                                    <div class="col-xs-12 col-md-12">
                                                                        <label>
                                                                            <h2>Seleccione Fase</h2>
                                                                        </label>
                                                                        <select class="form-control" id="correo_seleccione_fase_cbx" name="correo_seleccione_fase_cbx" requerido="true" mensaje="Seleccione Fase">
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <label>
                                                                            <h2>Tipo de Correo</h2>
                                                                        </label>
                                                                        <select class="form-control" id="tipo_correo_cbx" name="tipo_correo_cbx" requerido="true" mensaje="Seleccione Tipo Correo">
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-md-12 col-xs-12">
                                                                        <label for="">
                                                                            <h2>Modalidad</h2>
                                                                        </label>
                                                                        <select class="form-control" id="modalidad_correo_cbx" name="modalidad_correo_cbx" requerido="true" mensaje="Seleccione modalidad">
                                                                        </select>
                                                                    </div>


                                                                </div>

                                                                <div class="col-md-6">
                                                                    <label>
                                                                        <h2>Producto</h2>
                                                                    </label>
                                                                    <select class="form-control" id="seleccione_producto_cbx_correo" name="seleccione_producto_cbx_correo" requerido="true" mensaje="Seleccione producto">
                                                                        <option></option>
                                                                    </select>

                                                                    <label><h2>Fecha Correo</h2></label>
                                                                    <div class="form-group" id="data2" style="cursor: pointer;">
                                                                        <div class="input-group date">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-calendar" onclick="fecha(2)"></i>
                                                                            </span>
                                                                            <input type="text" id="fecha_correo" name="fecha_correo" class="form-control" value="<?php echo date("d-m-Y");?>"/>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 col-md-12">
                                                            <div style="width: 100%">
                                                                <h2 class="text-center">Contacto</h2>
                                                                <select id="correo_contacto_origen_cbx" name="correo_contacto_origen_cbx" class="form-control" requerido="true" mensaje="Seleccione Contacto">

                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 col-md-12">
                                                            <h2 class="text-center">Asunto</h2>
                                                            <textarea name="correo_asunto_box" id="correo_asunto_box" cols="75" rows="2" style="width: 100%;" requerido="true" mensaje="Debe ingresar Asunto"></textarea>
                                                            <h2 class="text-center">Comentario</h2>
                                                            <textarea name="correo_comentario_box" id="correo_comentario_box" cols="75" rows="2" style="width: 100%;" requerido="true"
                                                            mensaje="Ingrese comentario"></textarea>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-xs-6 col-md-6" style="border-top: 1px solid #e5e5e5; margin-top: 10px; padding-top: 10px;">
                                                                <label>
                                                                    <h2>Compromiso</h2>
                                                                </label>
                                                                <select class="form-control" name="correo_compromiso_cbx" id="correo_compromiso_cbx" requerido="true" mensaje="Seleccione Compromiso">
                                                                </select>
                                                            </div>

                                                            <div class="col-xs-6 col-md-6" style="border-top: 1px solid #e5e5e5; margin-top: 10px; padding-top: 10px;">
                                                                <label>
                                                                    <h2>Fecha Compromiso </h2>
                                                                </label>
                                                                <div class="form-group" id="data1" style="cursor: pointer;">
                                                                    <div class="input-group date col-md-11">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-calendar" onclick="fecha(1)"></i>
                                                                        </span>
                                                                        <input type="text" class="form-control" id="correo_fecha_compromiso" name="correo_fecha_compromiso" 
                                                                        value="<?php echo date("d-m-Y");?>" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 col-md-12" id="div_otros_correo" style="display: none;">
                                                              <label><h2>Comentario Compromiso</h2></label>
                                                              <div class="form-group">
                                                                <textarea name="comentario_otros_correo" id="comentario_otros_correo" rows="3" style="width: 100%;" requerido="true" mensaje="Debe ingresar comentario compromiso"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button id="btnCerrar" type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                    <button type="button" id="btn-correo" value="button" class="btn btn-primary">Guardar</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>




                    <div class="col-xs-6 col-md-4">
                        <div class="ibox">
                            <div>
                                <div class="text-center">
                                    <button type="button" class="btn btn-outline btn-primary dim" onclick="modalreunion()" style="height: 83px; width: 100px;">
                                        <h4>
                                            <i class="fa fa-wechat"></i>
                                            <br> Reunión </h4>
                                        </button>
                                    </div>
                                    <div class="modal inmodal" id="myModal3" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content animated bounceInRight">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">
                                                        <span aria-hidden="true">&times;</span>
                                                        <span class="sr-only">Close</span>
                                                    </button>
                                                    <i class="fa fa-wechat modal-icon"></i>
                                                    <h4 class="modal-title">Detalle Reunión</h4>
                                                    <small class="font-bold">Ingrese asunto, comentario y fecha.</small>
                                                </div>

                                                <div class="modal-body">
                                                    <form class="form-horizontal" id="form_reunion" name="form_reunion">
                                                        <input type="hidden" name="idempresareunion" id="idempresareunion" value="<?php echo $datos_cliente[0]['id_empresa'];?>">
                                                        <div class="col-xs-12 col-md-12">
                                                            <div class="row">
                                                                <div class="col-xs-12 col-md-6 b-r">
                                                                    <label>
                                                                        <h2>Seleccione Fase</h2>
                                                                    </label>
                                                                    <select class="form-control" id="reunion_seleccione_fase_cbx" name="reunion_seleccione_fase_cbx" requerido="true" mensaje="Seleccione Fase">
                                                                    </select>

                                                                    <label for="">
                                                                        <h2>Tipo de Reunión</h2>
                                                                    </label>
                                                                    <select class="form-control" id="reunion_tipo_reunion_cbx" name="reunion_tipo_reunion_cbx">
                                                                    </select>

                                                                    <label>
                                                                        <h2>Fecha Reunión</h2>
                                                                    </label>
                                                                    <div class="form-group" id="data3" style="cursor: pointer;">
                                                                        <div class="input-group date">
                                                                            <span class="input-group-addon">
                                                                                <i class="fa fa-calendar" onclick="fecha(3)"></i>
                                                                            </span>
                                                                            <input type="text" id="reunion_fecha_reunion" name="reunion_fecha_reunion" class="form-control" value="<?php echo date("d-m-Y");?>"/>
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                                <div class="col-xs-12 col-md-6">
                                                                    <label for="">
                                                                    <h2>Modalidad</h2>
                                                                    </label>
                                                                    <select class="form-control" id="modalidad_reunion_cbx" name="modalidad_reunion_cbx" requerido="true" mensaje="Seleccione modalidad">
                                                                    </select>

                                                                    <label>
                                                                        <h2>Producto</h2>
                                                                    </label>
                                                                    <select class="form-control" id="seleccione_producto_cbx" name="seleccione_producto_cbx" requerido="true" mensaje="Seleccione producto">
                                                                        <option></option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-xs-12 col-md-12">
                                                                    <div style="width: 100%">
                                                                        <h2 class="text-center">Contacto</h2>
                                                                        <select data-placeholder="Contacto" id="reunion_contacto_origen_cbx" name="reunion_contacto_origen_cbx" class="form-control"
                                                                        requerido="true" mensaje="Seleccione Contacto">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-xs-12 col-md-12">
                                                        <h2 class="text-center">Asunto</h2>
                                                        <textarea name="reunion_asunto" id="reunion_asunto" cols="75" rows="2" style="width: 100%;" requerido="true" mensaje="Debe ingresar Asunto"></textarea>
                                                        <h2 class="text-center">Comentario</h2>
                                                        <textarea name="reunion_comentario" id="reunion_comentario" cols="75" rows="2" style="width: 100%;" requerido="true" mensaje="Ingrese comentario"></textarea>
                                                    </div>



                                                    <div class="row">
                                                        <div class="col-xs-6 col-md-6" style="border-top: 1px solid #e5e5e5; margin-top: 10px; padding-top: 10px;">
                                                            <label>
                                                                <h2>Compromiso</h2>
                                                            </label>
                                                            <select class="form-control" id="reunion_compromiso_cbx" name="reunion_compromiso_cbx" requerido="true" mensaje="Seleccione Compromiso">
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-6 co-md-6" style="border-top: 1px solid #e5e5e5; margin-top: 10px; padding-top: 10px;">
                                                            <label>
                                                                <h2>Fecha Compromiso </h2>
                                                            </label>
                                                            <div class="form-group" id="data1" style="cursor: pointer;">
                                                                <div class="input-group date">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-calendar" onclick="fecha(1)"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control" name="reunion_fecha_compromiso" id="reunion_fecha_compromiso" value="<?php echo date("d-m-Y")?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-12 col-md-12" id="div_otros_reunion" style="display: none;">
                                                          <label><h2>Comentario Compromiso</h2></label>
                                                          <div class="form-group">
                                                            <textarea name="comentario_otros_reunion" id="comentario_otros_reunion" rows="3" style="width: 100%;" requerido="true" mensaje="Debe ingresar comentario compromiso"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">

                                                <button id="btnCerrar" type="reset" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                                <button type="button" value="button" class="btn btn-primary" id="btn-reunion">Guardar</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--MODAL COTIZACION-->
                <div class="col-xs-6 col-md-4">
                    <div class="ibox">
                        <div>

                            <div class="text-center">
                                <button type="button" class="btn btn-outline btn-primary dim" onclick="cotizacion();" style="height: 83px; width: 100px; padding: 6px 0px 0px 0px;">
                                    <h4>
                                        <i class="fa fa-file-text text-center"></i>
                                        <br>Cotización</h4>
                                    </button>
                                </div>

                                <div class="modal inmodal" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated bounceInRight">

                                            <!--MODAL HEADER-->
                                            <div class="modal-header">
                                                <input type="hidden" id="idempresacotizacion" name="idempresacotizacion" value="<?php echo $datos_cliente[0]['id_empresa'] ;?>">
                                                <button type="button" class="close" data-dismiss="modal">
                                                    <span aria-hidden="true">&times;</span>
                                                    <span class="sr-only">Close</span>
                                                </button>
                                                <i class="fa fa-file-text modal-icon"></i>
                                                <h4 class="modal-title">Generación de Cotización</h4>
                                                <small class="font-bold">Especifique detalle cotización.</small>
                                            </div>
                                            <!--FIN MODAL HEADER-->

                                            <!--MODAL BODY-->
                                            <div class="modal-body">
                                                <button class="btn btn-success btn-sm btn-block" data-toggle="collapse" data-target="#formulario_cot" style="margin-bottom: 20px">Ingresar Cotización</button>
                                                <div id="formulario_cot" class="collapse">
                                                    <form id="frmCotizacion" name="frmCotizacion">
                                                        <div class="row form-group">

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <label for="">Contactos</label>
                                                                    <select class="form-control" id="contactos_cotizacion_cbx" name="contactos_cotizacion_cbx" style="width: 100%;" requerido="true"
                                                                    mensaje="Campo obligatorio.">
                                                                </select>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label for="">Modalidad</label>
                                                                <select class="form-control" style="width: 100%;" name="modalidad_cotizacion_cbx" id="modalidad_cotizacion_cbx" requerido="true"
                                                                mensaje="Campo obligatorio.">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label>Fecha Inicio</label>
                                                            <div class="form-group" id="data5" style="cursor: pointer;">
                                                                <div class="input-group date" onclick="fecha(5)" style="width: 100%;">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </span>
                                                                    <input type="text" class="form-control"
                                                                    id="fecha_ingreso_cotizacion" name="fecha_ingreso_cotizacion"
                                                                    requerido="true" mensaje="Campo obligatorio."
                                                                    value="<?php echo date(" Y-m-d
                                                                    ")?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <label for="">Curso</label>
                                                            <select class="form-control" style="width: 100%;" name="curso_cotizacion_cbx" id="curso_cotizacion_cbx" requerido="true"
                                                            mensaje="Campo obligatorio.">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>Fecha Término</label>
                                                        <div class="form-group" id="data5" style="cursor: pointer;">
                                                            <div class="input-group date" onclick="fecha(5)" style="width: 100%;">
                                                                <span class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </span>
                                                                <input type="text" class="form-control"
                                                                requerido="true" mensaje="Campo obligatorio."
                                                                id="fecha_termino_cotizacion" name="fecha_termino_cotizacion"
                                                                value="<?php echo date(" Y-m-d
                                                                ")?>">
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">
                                                        <label for="">Cantidad Asistenes</label>
                                                        <br>
                                                        <input type="number" min="0" name="cantidad_asistentes_cotizacion" id="cantidad_asistentes_cotizacion" class="form-control"
                                                        requerido="true" mensaje="Campo obligatorio.">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">

                                                    </div>

                                                    <div class="col-md-6">
                                                        <label for="">Forma de Pago</label>
                                                        <select class="form-control" style="width: 100%;" name="formas_pago_cotizacion_cbx" id="formas_pago_cotizacion_cbx" requerido="true"
                                                        mensaje="Campo obligatorio.">
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <span></span>
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Descuento (%)</label>
                                                    <br>
                                                    <input type="number" id="descuento_cotizacion" name="descuento_cotizacion" min="0" value="0" class="form-control" requerido="true"
                                                    mensaje="Campo obligatorio." />
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">

                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Observación</label>
                                                    <br>
                                                    <textarea name="observacion_cotizacion" cols="30" rows="2" id="observacion_cotizacion" style="width: 100%;"></textarea>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">

                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Valor Total Cotización</label>
                                                    <br>
                                                    <input type="number" id="valor_cotizacion" name="valor_cotizacion" class="form-control">
                                                </div>

                                            </div>

                                            <div class="text-center">
                                                <button type="button" id="btn_add" class="btn btn-w-m btn-primary btn-sm" onclick="genValorTotal()" style="margin-top: 15px">Calcular Valor Total</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                                <hr>
                                <button data-toggle="collapse" data-target="#lista_cot" class="btn btn-success btn-sm btn-block" data-toggle="collapse" data-target="#formulario_cot"
                                style="margin-bottom: 20px">Cotizaciones Emitidas</button>
                                <div id="lista_cot" class="collapse">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="cotizacionesCliente" class="table">
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--FIN MODAL BODY-->

                            <!--MODAL FOOTER-->
                            <div class="modal-footer">
                                <form action="#">
                                    <button type="button" class="btn btn-white" data-dismiss="modal" onclick="limpiarFormularios()">Cerrar</button>
                                    <button type="button" value="Submit" class="btn btn-primary" onclick="guardarCotizacion()">Guardar</button>
                                </form>
                            </div>
                            <!--FIN MODAL FOOTER-->
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--FIN MODAL COTIZACION-->



    <div class="col-xs-6 col-md-4">
        <div class="ibox">
            <div>
                <div class="text-center">
                    <button type="button" class="btn btn-outline btn-primary dim" onclick="modalpedido();" style="height: 83px; width: 100px;">
                        <h4>
                            <i class="fa fa-check-square"></i>
                            <br> Pedido </h4>
                        </button>
                    </div>
                    <div class="modal inmodal" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content animated bounceInRight">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only">Close</span>
                                    </button>
                                    <i class="fa fa-check-square modal-icon"></i>
                                    <h4 class="modal-title">Ingrese Pedido</h4>
                                    <small class="font-bold">Complete formulario.</small>
                                </div>
                                <div class="modal-body">
                                    <!-- FORMULARIO -->


                                    <form id="form_pedido" name="form_pedido">
                                        <div class="row form-group">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="">Cliente</label>
                                                    <input class="form-control" type="text" id="pedido_seleccione_cliente" name="pedido_seleccione_cliente" disabled value="<?php echo $datos_cliente[0]['razon_social_empresa'] ; ?>">
                                                    <input type="hidden" id="idempresapedido" name="idempresapedido" value="<?php echo $datos_cliente[0]['id_empresa'] ;?>">
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Modalidad</label>
                                                    <select id="pedido_modalidad_cbx" name="pedido_modalidad_cbx" class="form-control" requerido="true" mensaje="Seleccione Modalidad">

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Fecha Pedido</label>
                                                    <div class="form-group" id="data1" style="cursor: pointer;">
                                                        <div class="input-group date" onclick="fecha(1)" style="width: 100%;">
                                                            <span class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                            <input type="text" id="pedido_fecha_reunion" name="pedido_fecha_reunion" class="form-control" value="<?php echo date("d-m-Y")?>">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="">Tipos de Curso</label>
                                                    <select id="pedido_tipo_curso_cbx" name="pedido_tipo_curso_cbx" style="width: 100%;" class="form-control" requerido="true"
                                                    mensaje="Seleccione Tipo Curso">
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>

                                            <div class="col-md-6">
                                                <label for=""> Cantidad de Asistentes</label>
                                                <br>
                                                <input id="pedido_cant_asistentes" class="form-control" type="number" min="0" name="pedido_cant_asistentes" style="width: 100%;"
                                                requerido="true" mensaje="Ingrese Cantidad">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>

                                            <div class="col-md-6">
                                                <input type="hidden" id="pedido_identificador" value="0" style="width: 100%;">
                                                <div>
                                                    <label for="">Comentario</label>
                                                    <textarea class="form-control" id="pedido_comentario" name="pedido_comentario" cols="30" rows="2" style="width: 100%;"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">

                                        </div>

                                        <div class="col-md-6">
                                            <button id="btn_guardar_pedido" type="button" class="btn btn-primary btn-sm" style="margin: 15px 0px 15px 0px" data-toggle="tooltip"
                                            title="Agregar Curso">
                                            <i class="fa fa-plus-circle"></i> Agregar Curso</button>
                                        </div>

                                    </div>

                                </form>


                                <!-- FORMULARIO -->


                            </div>

                            <div class="table-responsive">
                                <table id="contenido" class="table table-responsive table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Modalidad</th>
                                            <th>Tipo</th>
                                            <th>Asistentes</th>
                                            <th>Comentario</th>
                                            <th>Fecha</th>
                                            <th>
                                                <span></span>
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                    </tbody>
                                </table>
                            </div>

                            <div class="modal-footer">
                                <form action="#">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                                    <button type="button" value="Submit" class="btn btn-primary" id="btn-pedido">Guardar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-6 col-md-4">
        <div class="ibox">
            <div>
                <div class="text-center">
                    <a href="../../editar_cuenta/index/<?php echo $datos_cliente[0]['id_empresa'];?>">
                        <button type="button" class="btn btn-outline btn-primary dim" data-toggle="modal" data-target="#myModal6" style="height: 83px; width: 100px; padding: 6px 0px 0px 0px;">
                            <h4>
                                <i class="fa fa-spinner text"></i>
                                <br>Actualizar
                                <br>Contacto</h4>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

</div>
</div>
</div>
</div>
</div>