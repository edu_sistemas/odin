<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<input type="hidden" id="RutaBase" value="<?php echo base_url('back_office/Crm/cuenta');?>">
<div class="wrapper wrapper-content animated fadeInRight"> 
	<div class="row">
		<div class="col-md-7">
			<div class="ibox float-e-margins">
				<div class="ibox-content m-b-sm border-bottom" style="height:85px">
					<div class="row">
						<div class="col-md-1 col-xs-3">
							<h4 style="margin-top:10px">Cuenta</h4>
						</div>  
						<div class="col-md-4 col-xs-4">
							<div class="form-group">
								<input type="text" id="filtro_cuenta_nombre" name="filtro_cuenta_nombre" value="" placeholder="Búsqueda" class="form-control">								  
							</div>
						</div>
						<div class="col-md-7">
							<a href="<?php echo base_url('back_office/Crm/nueva_cuenta');?> ">
								<button type="button" class="btn btn-w-m btn-primary btn-sm">Nuevo Cliente</button>
							</a>   
							<button type="button" class="btn btn-w-m btn-info btn-sm" id="btnListarClientes">Todos los Clientes</button>
							<button type="button" class="btn btn-w-m btn-info btn-sm" id="btnCargaMasiva">Carga Masiva</button>
						</div>
					</div>
				</div>

				<input type="hidden" value="<?php echo $ejecutivo;?>" name="Var_ejecutivo" id="Var_ejecutivo">
				<div class="ibox-content m-b-sm border-bottom" style="height:60px">
					<div class="col-md-9 pull-left">
						<div class="col-sm-2">
							<label class="control-label" for="ejecutivos">Ejecutivo:</label>
						</div>
						<div class="col-sm-7">
							<select class="form-control" id="ejecutivos" name="ejecutivos"></select>
							<input type="hidden" name="equipo" id="equipo" value="0">
						</div>
					</div>
				</div>   	
				<div class="ibox-content">
					<div class="table-responsive">
						<table id="tabla_cuenta" class="footable table table-stripped toggle-arrow-tiny" data-page-size="20" data-filter="#filtro_cuenta_nombre" data-limit-navigation="8">
							<thead>
								<tr class="fila_historial">
									<th>Cliente</th>
									<th>Holding</th>
									<th>Status Gestión</th>
									<th>Estado Cuenta</th>
									<th>Rut_Empresa</th>
								</tr>
							</thead>

							<!-- id="div_general" name="div_general"--> 
							<tbody id="div_general" name="div_general">

								<?php								
								for($i=0;$i<count($cliente_crm);$i++){

									$texto_adicional 	=	"";
									$icono_adicional 	= 	"";
									$icono_contacto = "";
									$title = "";										
									$id = $cliente_crm[$i]['ID'];
									$tiene_contacto = $cliente_crm[$i]['tiene_contacto'];
									$tiene_gestion = $cliente_crm[$i]['tiene_gestion'];
									$q_dias = $cliente_crm[$i]['q_dias'];


									switch (($cliente_crm[$i]['estado_empresa'])) {

										case 1:
										$texto_titulo 		= "Potencial";
										break;

										case 2: 
										$texto_titulo 		= "Activo (6+)";										
										break;

										case 3: 
										$texto_titulo 		= "Activo (6-)";										
										break;	
									}											
									
									$color_icon_titulo 	= "#656767";
									
									/*$texto_adicional 	= ", Con contacto.";*/												



													/*ICONO SUGERIDO POR SISTEMA
													<i data-toggle=\"tooltip\" data-placement=\"auto right\" data-html=\"true\" title=\"<i class='fa fa-circle' style='color: #1ab394; font-size: 15px;'></i> <i class='fa fa-envelope' style='color: #ed5565; font-size: 15px;'></i>   
													Cliente <b style='color:#1BB394;'>Nuevo</b>, Con contacto.\" class=\"fa fa-envelope-o\" style=\"color: #ed5565; font-size: 15px; padding-left: 20px;cursor:help;\"></i>"
													
													TOOLTIP STATUS GESTION
													<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i> <i class=\"fa fa-user\" style=\"color: #ed5565; font-size: 15px;\"></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo  </b>$texto_adicional
													
													<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i> <i class=\"fa fa-user\" style=\"color: #ed5565; font-size: 15px;\"></i>   Cliente <b style=\"color: $color_icon_titulo;\">asdf</b>asdf
													*/
													
													
													
													/*$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li><li><a href='#'>Eliminar</a></li><li><a href='cliente_potencial/index/$id'>Editor de Acción</a></li><li><a href='detalle_cuenta/index/$1'>Ver Detalle</a></li>";*/
													
													$urlEditarCu=base_url('back_office/Crm/editar_cuenta/index/'.$id);
													$urlNuevaGes=base_url('back_office/Crm/cliente_potencial/index/'.$id);

													if($tiene_contacto == 1){
														$icono_contacto = "<i class='fa fa-user' data-toggle='tooltip' data-placement='auto right' title='Cliente con contacto.' style='cursor: help; color:#337ab7; font-size:20px;'></i>";	
														$texto_adicional = "Sugerido por Sistema";
														$opciones_accion ="<li><a href='".$urlEditarCu."'>Ver / Editar</a></li><li><a href='".$urlNuevaGes."'>Nueva Gestión</a></li><li id='deleteRegistro' onclick=confirmaEliminaCuenta($id)><a href='#'>Eliminar</a></li>";	


														if($q_dias <= 10){
															$title = "Con gestión";
															$color_icon_titulo = "#1ab394";	
														}
														elseif($q_dias > 15){
															$title = "Sin gestión hace más de 15 días";
															$color_icon_titulo = "#656767";	
														}
														elseif ($q_dias > 10 && $q_dias < 15) {
															$title = "Sin gestión entre los últimos 10 y 15 días";
															$color_icon_titulo = "#ffb000";
														}
														else {
															$title = "Sin gestión en los últimos 10 días";
															$color_icon_titulo = "#656767";	
														}



													}
													else
													{														
														$texto_adicional 	= "Sin gestión";
														$title = "<b>$texto_adicional</b>";
														$opciones_accion ="<li><a href='".$urlEditarCu."'>Ver / Editar</a></li> <li class='disabled'><a href='#'>Nueva Gestión</a></li> <li id='deleteRegistro' onclick=confirmaEliminaCuenta($id)><a href='#'>Eliminar</a></li>";
													}
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
													


													echo "<tr id='tr_".$id."'>
													<td>".$cliente_crm[$i]['nombre_cliente']."</td>
													<td>".$cliente_crm[$i]['holding']."</td>
													<td>												
													$icono_base
													$icono_adicional
													</td>
													<td>".$texto_titulo." &nbsp ".$icono_contacto."</td>
													<td>".$cliente_crm[$i]['rut_empresa']."</td>
													<td>
													<div class='btn-group'>
													<a data-toggle='dropdown' class='dropdown-toggle' href='#'>
													<i class='fa fa-wrench'></i>
													</a>
													<ul class='dropdown-menu pull-right'>
													$opciones_accion
													</ul>
													</div>
													</td>                                                
													</tr>";	
												}												
												?>						


											</tbody>
											<tfoot>
												<tr>
													<td colspan="6">
														<ul class="pagination pull-right"></ul>
													</td>
												</tr>
											</tfoot>
										</table>     
									</div>

								</div>   
							</div>
						</div>  

						<div class="col-md-5 col-xs-12">
							<div class="ibox-content m-b-sm border-bottom">
								<div class="row">
									<div class="col-md-4">
										<h4>Historial de Gestiones</h4>
									</div>  
									<div class="col-md-4">
										<div class="form-group">
											<input type="text" id="empresa_nombre" name="empresa_nombre" value="" placeholder="Filtrar Cliente" class="form-control">
										</div>
									</div>
								</div>
							</div>



							<div class="table-responsive col-md-6 col-xs-12" style="padding:0px;">
								<div class="ibox-content">
									<h5 style="text-align: center;">Exportar Gestiones</h5>
									<table style="display: none" id="tbl_gestion">
										<thead>
											<tr>
												<th>Ejecutivo</th>
												<th>Empresa</th>
												<th>Accion</th>
												<th>Asunto</th>
												<th>Evento</th>
												<th>Fecha Evento</th>
												<th>Compromiso</th>
												<th>Fecha Compromiso</th>
												<th>Comentario</th>
												<th>Tipo accion</th>
												<th>Fase</th>
												<th>Producto</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Ejecutivo</th>
												<th>Empresa</th>
												<th>Accion</th>
												<th>Asunto</th>
												<th>Evento</th>
												<th>Fecha Evento</th>
												<th>Compromiso</th>
												<th>Fecha Compromiso</th>
												<th>Comentario</th>
												<th>Tipo accion</th>
												<th>Fase</th>
												<th>Producto</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>

							<div class="table-responsive col-md-6 col-xs-12" style="padding:0px ">
								<div class="ibox-content">
									<h5 style="text-align: center;">Exportar Dominios sin Asignar</h5>
									<table style="display: none" id="tbl_dominio_sin_asignar">
										<thead>
											<tr>
												<th>Dominio</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Dominio</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>	




							<div class="row">
								<div class="col-lg-12 col-xs-12">
									<div class="ibox">						                
										<div class="ibox-content">
											<div class="table-responsive">										  	
												<table  class="footable table table-stripped toggle-arrow-tiny" data-limit-navigation="5" data-page-size="16" id="tbl_empr" data-filter="#empresa_nombre">
													<thead>
														<tr class="fila_historial">						                    	    

															<th data-toggle="true">Tipo<i style="color: #676a6c; font-size: 15px;"></i></th>
															<th>Cliente<i style="color: #676a6c; font-size: 15px;"></i></th>
															<th>Evento<i style="color: #676a6c; font-size: 15px;"></i></th>
															<th data-hide="all">Asunto</th>
															<th data-hide="all">Fecha<i style="color: #676a6c; font-size: 15px;"></i></th>
															<th data-hide="all">Comentario</th>
															<th data-hide="all">Compromiso</th>
															<th data-hide="all">Tipo acción</th>
															<th data-hide="all">Fase</th>
															<th data-hide="all">Producto</th>
														</tr>
													</thead>
													<tbody>
														<?php		
														
														for($i=0;$i<count($HistorialGestion_crm);$i++){
															$icono_base="";
															$id = $HistorialGestion_crm[$i]['ID'];
															$idaccion = $HistorialGestion_crm[$i]['id_accion'];

															if ($HistorialGestion_crm[$i]['tipo_gestion'] == 1) {
																$texto_gestion 	= "Gestión Manual";
																$tipogestion_fa = "fa fa-tasks";
																$color_icon_gestion_fa	= "#656767";
															}
															else {
																if ($HistorialGestion_crm[$i]['compromiso_accion_color'] == 1 ) {
																	$texto_gestion 	= "Gestión Automática sin Compromiso";
																	$tipogestion_fa = "fa fa-cog";
																	$color_icon_gestion_fa 	= "#FFB000";

																}													
																else{
																	$texto_gestion 	= "Gestión Automática";
																	$tipogestion_fa = "fa fa-cog";
																	$color_icon_gestion_fa	= "#656767";
																}
															}

															$icono_base_gestion = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$texto_gestion' class='$tipogestion_fa' style='color: $color_icon_gestion_fa; font-size: 15px; cursor:help;'></i>";

															switch ($HistorialGestion_crm[$i]['estado_evento']) {
																case 1:
																$color_icon_titulo 	= "#656767";
																$texto_titulo 		= "Nuevo";
																$title = "<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo </b>";
																$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";

																$urlEditarGesE=base_url('back_office/Crm/editar_gestion/index/'.$id.".".$idaccion);
																$urlNuevaGesE=base_url('back_office/Crm/cliente_potencial/index/'.$id);

																if ($HistorialGestion_crm[$i]['id_descripcion_evento'] == 4 or $HistorialGestion_crm[$i]['id_descripcion_evento'] == 5)
																{
																	
																	
																	$opciones_accion ="<li class='disabled'><a href='#'>Editar Gestión</a></li><li><a href='".$urlNuevaGesE."'>Nueva Gestión</a></li><li id='deleteRegistro' onclick=confirmaEliminaGestion($idaccion)><a href='#'>Eliminar</a></li>";
																}
																else
																{
																	$opciones_accion ="<li><a href='".$urlEditarGesE."'>Editar Gestión</a></li><li><a href='".$urlNuevaGesE."'>Nueva Gestión</a></li><li id='deleteRegistro' onclick=confirmaEliminaGestion($idaccion)><a href='#'>Eliminar</a></li>";						
																}									
																break;

															}                                    
															echo "			
															<tr class='fila_historial' id='tr_".$idaccion."'>
															<td style='width:7%; text-align: center'>$icono_base_gestion</td>
															<td>".$HistorialGestion_crm[$i]['razon_social_empresa']."</td>
															<td>".$HistorialGestion_crm[$i]['DESC_ACCION']."</td>
															<td>".$HistorialGestion_crm[$i]['asunto_accion']."<br><br></td>
															<td>".$HistorialGestion_crm[$i]['fecha_evento']."<br><br></td>
															<td>".$HistorialGestion_crm[$i]['comentario_accion']."<br><br></td>
															<td>".$HistorialGestion_crm[$i]['fecha_compromiso_accion']."  (".$HistorialGestion_crm[$i]['DESC_COMPROMISO'].")"."<br><br></td>
															<td>".$HistorialGestion_crm[$i]['tipo_accion']."<br><br></td>	
															<td>".$HistorialGestion_crm[$i]['fase']."<br><br></td>	
															<td>".$HistorialGestion_crm[$i]['id_producto']."</td>	
															

															<!-- <td>
															$icono_base
															</td> -->
															<td>
															<div class='btn-group'>
															<a data-toggle='dropdown' class='dropdown-toggle' href='#'>
															<i class='fa fa-wrench'></i>
															</a>
															<ul class='dropdown-menu pull-right'>
															$opciones_accion
															</ul>
															</div>   
															</td>
															</tr>
															";     
														}  
														?>	
													</tbody>
													<tfoot>
														<tr>
															<td colspan="5">
																<ul class="pagination pull-right"></ul>
															</td>
														</tr>
													</tfoot>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>


			</div>
			<div class="modal inmodal" id="mdlListaClientes" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog  modal-lg">
					<div class="modal-content animated bounceInRight">

						<!--MODAL HEADER-->
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<i class="fa fa-file-text modal-icon"></i>
							<h4 class="modal-title">Clientes Edutecno</h4>
							<small class="font-bold">Detalle de clientes por asignaciones.</small>
						</div>
						<!--FIN MODAL HEADER-->

						<!--MODAL BODY-->
						<div class="modal-body">

							<div class="table-responsive">
								<table  class="table table-striped" style="width: 100%" id="tbl_clientes_ejecutivos">
									<thead>
										<tr>
											<th>Cliente</th>
											<th>Holding</th>
											<th>Ejecutivo</th>
											<th>Rut Empresa</th>
											<th>Gestión</th>
											<th>Nombre Contacto</th>
											<th>Teléfono Contacto</th>
											<th>Dirección Empresa</th>
											<th>Fecha Gestión</th>
											<th>Fase</th>
											<th>Acción</th>
											<th>Compromiso</th>
											<th>Fecha Compromiso</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>
							</div>
						</div>
						<!--FIN MODAL BODY-->

						<!--MODAL FOOTER-->
						<div class="modal-footer">                                            
							<button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>  
						</div>
						<!--FIN MODAL FOOTER-->
					</div>
				</div>
			</div>



			<div class="modal inmodal" id="mdlCargaMasiva" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content animated bounceInRight">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<i class="fa fa-upload modal-icon"></i>
							<h4 class="modal-title">Carga Masiva Empresas</h4>
							<small class="font-bold">Carga Datos.</small>
						</div>
						<div class="modal-body">
							<form class="form-horizontal" id="form-masiva" method="">
								<div class="fileinput fileinput-new input-group" data-provides="fileinput">
									<div class="form-control" data-trigger="fileinput"><i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div>
									<span class="input-group-addon btn btn-default btn-file"><span class="fileinput-new">Seleccione Archivo</span><span class="fileinput-exists"><i class='fa fa-refresh'></i></span><input type="file" name="excel" id="excel" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"></span>
									<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput"><i class='fa fa-close'></i></a>
								</div>
								<button type="submit" class="btn btn-success" id="btn-cmasiva">Cargar</button>
								<br>
								<div class="pull-right">
									<a href="<?php echo base_url('assets/plantillas/masiva_empresas.xlsx');?>"><i class="fa fa-download"></i> Descargar Excel Base</a>
								</div>
							</form>
						</div>
						<div class="modal-footer">                                            
							<button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>  
						</div>
					</div>
				</div>
			</div>



			