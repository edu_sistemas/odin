<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
            <div id="ocultadiv">
			<h3 class="m-t-none m-b">Ingresar Oportunidad</h3>
                <div class="row">

                <div class="col-sm-6 b-r">

                    <div class="col-md-6" id="Esta es la seccion que debe Aparecer tras accion de boton">
                        
                        <form role="form">
                            <div class="form-group"><label>Cliente</label> <input type="text" placeholder="Ingrese Cliente" class="form-control"></div>
                            <div class="form-group">
                                            <label for="">Tipo de Venta</label>
                                            <select class="form-control">
                                              <option>Seleccione</option>
                                              <option>Arriendo </option>
                                              <option>Curso</option>
                                              
                                            </select>
                            </div>

                        </form>
                    </div>

                    <div class="col-md-6">      

                        <form class="form-group" action="" method="post">            
                          <div class="form-group">
                            <label>Fecha Estimada de Cierre</label>
                            <div class="form-group" id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="01/01/2017">
                                </div>
                            </div>
                          </div>   

                          <div class="form-group"><label>Monto Estimado</label> <input type="Text" placeholder="Ingrese Monto" class="form-control"></div>

                        </form>


                    </div>
                </div>

                <div class="col-sm-6 b-r">
                    <div class="col-md-6">
                        
                        <form role="form">
                            <div class="form-group"><label>Probabilidad</label> <input type="text" placeholder="Ingrese Probabilidad (%)" class="form-control "></div>
                            <div class="form-group"><label>Descripción</label> <input type="text" placeholder="Ingrese Detalle" class="form-control "></div>

                            <div>
                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="button" onclick="desaparece();"><strong>Ingresar</strong></button>
                            </div> 
                           
                        </form>
                    </div>

                    
                </div>



                </div>    
</div>    

                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox">
                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="button" onclick="mueatra();"><strong>Nueva Oportunidad</strong></button>
                                    <div class="ibox-content">

                                      <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="5">
                                          <thead>
                                          <tr>

                                              <th data-toggle="true">Cliente</th>
                                              <th data-hide="phone">Monto Estimado</th>
                                              <th data-hide="all">Descripción</th>
                                              <th data-hide="phone">Fecha Estimada de Cierre</th>
                                              <th data-hide="phone,tablet">Tipo de Venta</th>
                                              <th data-hide="phone">Probabilidad</th>
                                              <th data-hide="phone">Status</th>
                                              <th data-hide="phone">Acción</th>

                                          </tr>
                                          </thead>
                                          <tbody>
                                          <tr>
                                              <td>
                                                 Ripley
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Arriendo
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #1ab394; font-size: 15px;"></i></
                                              </td>
                                            <td>
                                                
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <i class="fa fa-wrench"></i>
                                                </a>

                                               <ul class="dropdown-menu dropdown-user">
                                                    <li><a href="#">Editar</a>
                                                    </li>
                                                    <li><a href="#">Eliminar</a>
                                                    </li>
                                                    
                                                </ul>

                                            </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Falabella
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                 Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Arriendo
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #f8ac59; font-size: 15px;"></i></
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                      </li>
                                                      <li><a href="#">Nuevo</a>
                                                      </li>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Santa Isabel
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Arriendo
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #1ab394; font-size: 15px;"></i></
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                      </li>
                                                      <li><a href="#">Nuevo</a>
                                                      </li>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Vmica
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Curso
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #ed5565; font-size: 15px;"></i></
                                              </td>
                                            <td>
                                                
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <i class="fa fa-wrench"></i>
                                                </a>

                                               <ul class="dropdown-menu dropdown-user">
                                                    <li><a href="#">Editar</a>
                                                    </li>
                                                    <li><a href="#">Eliminar</a>
                                                    </li>
                                                    <li><a href="#">Nuevo</a>
                                                    </li>
                                                </ul>

                                            </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Lider
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Curso
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #f8ac59; font-size: 15px;"></i></
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                      </li>
                                                      <li><a href="#">Nuevo</a>
                                                      </li>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Fallabela
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Arriendo
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #ed5565; font-size: 15px;"></i></
                                              </td>                                            
                                              <td>
                                                
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <i class="fa fa-wrench"></i>
                                                </a>

                                               <ul class="dropdown-menu dropdown-user">
                                                    <li><a href="#">Editar</a>
                                                    </li>
                                                    <li><a href="#">Eliminar</a>
                                                    </li>
                                                    <li><a href="#">Nuevo</a>
                                                    </li>
                                                </ul>

                                            </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Santa Isabel
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Curso
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #f8ac59; font-size: 15px;"></i></
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                      </li>
                                                      <li><a href="#">Nuevo</a>
                                                      </li>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Vmica
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Curso
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #1ab394; font-size: 15px;"></i></
                                              </td>                                            
                                              <td>
                                                
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                    <i class="fa fa-wrench"></i>
                                                </a>

                                               <ul class="dropdown-menu dropdown-user">
                                                    <li><a href="#">Editar</a>
                                                    </li>
                                                    <li><a href="#">Eliminar</a>
                                                    </li>
                                                    <li><a href="#">Nuevo</a>
                                                    </li>
                                                </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Ripley
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Arriendo
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                  <i class="fa fa-circle" style="color: #f8ac59; font-size: 15px;"></i></
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                      </li>
                                                      <li><a href="#">Nuevo</a>
                                                      </li>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Santa Isabel
                                              </td>
                                              <td>
                                                  $1.000
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  Arriendo
                                              </td>
                                              <td>
                                                  50%
                                              </td>
                                              <td>
                                                 <i class="fa fa-circle" style="color: #1ab394; font-size: 15px;"></i></
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                      </li>
                                                      <li><a href="#">Nuevo</a>
                                                      </li>
                                                  </ul>

                                              </td>
                                          </tr>
                                             
                                          </tbody>
                                          <tfoot>
                                          <tr>
                                              <td colspan="6">
                                                  <ul class="pagination pull-right"></ul>
                                              </td>
                                          </tr>
                                          </tfoot>
                                      </table>
                                        
                                    </div>
                                </div>  
                            </div>
                         </div>    
                    </div>





                    <div class="col-md-12">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="ibox">
                                    <div class="ibox-content">

                                      <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="5">
                                          <thead>
                                          <tr>

                                              <th data-toggle="true">Cliente</th>
                                              <th data-hide="phone">Acción</th>
                                              <th data-hide="all">Descripción</th>
                                              <th data-hide="phone">Fecha</th>
                                              <th data-hide="phone,tablet" >Quantity</th>
                                              <th data-hide="phone">Status</th>

                                          </tr>
                                          </thead>
                                          <tbody>
                                          <tr>
                                              <td>
                                                 Ripley
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  1000
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Falabella
                                              </td>
                                              <td>
                                                  Correo
                                              </td>
                                              <td>
                                                 Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  4300
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Santa Isabel
                                              </td>
                                              <td>
                                                  Reunión
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  300
                                              </td>
                                              <td>
                                                  <span class="label label-danger">Abierto</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Vmica
                                              </td>
                                              <td>
                                                  Correo
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  2300
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Lider
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  800
                                              </td>
                                              <td>
                                                  <span class="label label-warning">A la Espera</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Fallabela
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  6000
                                              </td>
                                              <td>
                                                  <span class="label label-danger">Abierto</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Santa Isabel
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  700
                                              </td>
                                              <td>
                                                  <span class="label label-danger">Abierto</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Vmica
                                              </td>
                                              <td>
                                                  Reunión
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  5180
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado </span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Ripley
                                              </td>
                                              <td>
                                                  Correo
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  450
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Santa Isabel
                                              </td>
                                              <td>
                                                  Reunión
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  7600
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Falabella
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  1000
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Vmica
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  4300
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Ripley
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  300
                                              </td>
                                              <td>
                                                  <span class="label label-warning">A la Espera</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Santa Isabel
                                              </td>
                                              <td>
                                                  Correo
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  2300
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Falabella
                                              </td>
                                              <td>
                                                  Reunión
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  800
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Vmica
                                              </td>
                                              <td>
                                                  Correo
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  6000
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Ripley
                                              </td>
                                              <td>
                                                 Reunión
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  700
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Santa Isabel
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  5180
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Falabella
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                  Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                  dd/mm/aa
                                              </td>
                                              <td>
                                                  450
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>
                                          <tr>
                                              <td>
                                                  Lider
                                              </td>
                                              <td>
                                                  Llamada
                                              </td>
                                              <td>
                                                 Lorem ipsum es un texto de relleno dolor sit amet, consectetur adipisicing elit. Asperiores, magni. Inventore perferendis voluptatum quas consequuntur laudantium perspiciatis qui, sint omnis veniam doloribus doloremque quis molestiae, deleniti numquam possimus eaque beatae.
                                              </td>
                                              <td>
                                                 dd/mm/aa
                                              </td>
                                              <td>
                                                  7600
                                              </td>
                                              <td>
                                                  <span class="label label-primary">Cerrado</span>
                                              </td>
                                              <td>
                                                  
                                                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                      <i class="fa fa-wrench"></i>
                                                  </a>

                                                 <ul class="dropdown-menu dropdown-user">
                                                      <li><a href="#">Editar</a>
                                                      </li>
                                                      <li><a href="#">Eliminar</a>
                                                  </ul>

                                              </td>
                                          </tr>


                                          </tbody>
                                          <tfoot>
                                          <tr class="fila_historial">
                                              <td colspan="6">
                                                  <ul class="pagination pull-right"></ul>
                                              </td>
                                          </tr>
                                          </tfoot>
                                      </table>
                                        
                                    </div>
                                </div>  
                            </div>
                         </div>    
                    </div>  






            </div>
			
		



