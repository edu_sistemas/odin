<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
    
<div class="wrapper wrapper-content animated fadeIn">
        <div class="ibox-title">
            <h5>Detalle Cuenta</h5>
        </div>
    <div class="row m-t-lg">
        <div class="col-md-6">            
            <div class="tabs-container">

                <div class="tabs-left">
                <ul class="nav nav-tabs">
                    <li class="active"  data-toggle="tooltip" data-placement="bottom" title="Detalle de la Cuenta"><a data-toggle="tab" href="#tab-1"><i class="fa fa-address-card fa-2x"></i></a></li>
                    <li class=""  data-toggle="tooltip" data-placement="bottom" title="Vision General"><a data-toggle="tab" href="#tab-2"><i class="fa fa-eye fa-2x"></i></a></li>
                    <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos para Facturación"><a data-toggle="tab" href="#tab-3"><i class="fa fa-pencil-square-o fa-2x"></i></a></li>
                    <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Entrega Factura"><a data-toggle="tab" href="#tab-4"><i class="fa fa-check-square-o fa-2x" ></i></a></li>
                    <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Finanzas"><a data-toggle="tab" href="#tab-5"><i class="fa fa-area-chart fa-2x" ></i></a></li>
                    <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Entrega de Diplomas"><a data-toggle="tab" href="#tab-6"><i class="fa fa-mortar-board fa-2x" ></i></a></li>
                </ul>
                    <div class="tab-content ">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <strong>Detalle de la Cuenta</strong>

                                <form role="form">
                                    <div class="col-md-6">                                             
                                        <div class="form-group"><label>Nombre de Fantasía</label>
                                             <input type="text" placeholder="Nombre de Fantasía" class="form-control" name="" id="tab1_dc_nombre_fantasia" disabled>
                                        </div>
                                        <div class="form-group"><label>Rut</label> 
                                            <input type="text" placeholder="11.111.111-1" class="form-control" name="" id="tab1_dc_rut" disabled>
                                        </div>
                                        <div class="form-group"><label>Clasificación de la Cuenta</label>
                                            <input type="text" placeholder="Cuenta Tipo 1" class="form-control" name="" id="tab1_dc_clasificacion" disabled>
                                        </div>
                                        <div class="form-group"><label>Dirección Empresa</label>
                                            <input type="text" placeholder="Dirección 123, Comuna. Región " class="form-control" name="" id="tab1_dc_direccion" disabled>
                                        </div>
										<div class="form-group"><label>Ejecutivo Asignado</label>
                                            <input type="text" placeholder="Ejecutivo 1" class="form-control" name="" id="tab1_dc_ejecutivo" disabled>
                                        </div>
                                        <div class="form-group"><label>Descripción del Contacto</label> <textarea name="" id="tab1_dc_descripcion" cols="30" rows="2" readonly="" style="width: 100%" >Lorem ipsum dolor sit amet, consectetur adipisicing elit. In ratione, pariatur repudiandae deleniti voluptates sint aliquam omnis.</textarea></div>
										
										<div class="fileinput fileinput-new" data-provides="fileinput">
											<label for="" required>Seleccione logo</label> <br>
											<span class="btn btn-default btn-file"><span class="fileinput-new">Seleccione Archivo</span><span class="fileinput-exists"> Cambiar</span><input type="file" name="..."></span>
											<span class="fileinput-filename"></span>
											<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
										</div>
									
									</div>

                                    <div class="col-md-6">                                            
                                        <div class="form-group"><label>OTIC Relacionada</label>
										    <input type="text" placeholder="OTIC" class="form-control" name="" id="tab1_dc_otic" disabled>
                                        </div>
                                        <label for="" required>Tipo de Industria</label>
                                        <select class="form-control" disabled="" id="tab1_dc_tipo">
                                          <option>Seleccione</option>
                                          <option selected="">Tipo 1</option>
                                          <option>Tipo 2</option>                                         
                                        </select>
                                        <br>
                                       <label for="" required>Holdding al que Pertenece</label>
                                       <select class="form-control" disabled="" id="tab1_dc_holdding">
                                         <option>Seleccione</option>
                                         <option>Tipo 1</option>
                                         <option selected="">Tipo 2</option>                                         
                                       </select>
									   <div class="form-group"><label>Tamaño de la Empresa</label>
                                            <input type="text" placeholder="Grande" class="form-control" name="" id="tab1_dc_tamaño" disabled>
                                        </div>
										<div class="form-group"><label>Rubro Empresa</label>
                                            <input type="text" placeholder="Rubro" class="form-control" name="" id="tab1_dc_rubro" disabled>
                                        </div>
										<div class="form-group"><label>Subrubro Empresa</label>
                                            <input type="text" placeholder="Subrubro" class="form-control" name="" id="tab1_dc_subrubro" disabled>
                                        </div>
                                    </div>
                                </form>                                   
                            </div>                            
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <strong>Visión General</strong>

                                    <form role="form">
                                        <h5>Datos Persona de contacto</h5>
                                            <div class="col-md-6">                                             
                                                <div class="form-group"><label>Nombre</label> 
                                                    <input type="text" placeholder="Nombre Apellido" class="form-control" name="" id="tab2_vg_nombre" disabled>
                                                </div>
                                                <div class="form-group"><label>Cargo</label>
                                                    <input type="text" placeholder="Ardministrador" class="form-control" name="" id="tab2_vg_cargo" disabled>
                                                </div>
                                                <div class="form-group"><label>Departamento</label>
                                                    <input type="text" placeholder="Administración" class="form-control" name="" id="tab2_vg_dpto" disabled>
                                                </div>
                                                <div class="form-group"><label>Correo Elctronico</label>
                                                    <input type="text" placeholder="correo@cuenta.cl" class="form-control" name="" id="tab2_vg_correo" disabled>
                                                </div>
												<div class="form-group"><label>Cantidad de Hijos</label>
                                                    <input type="text" placeholder="2" class="form-control" name="" id="tab2_vg_fecha" disabled>
                                                </div>
                                                <div class="form-group"><label>Descripción del Contacto</label><textarea name="" id="tab2_vg_descripcion" cols="30" rows="2" readonly="" style="width: 100%">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit eos veniam ex id.</textarea></div>
                                            </div>

                                            <div class="col-md-6">
												<div class="form-group"><label>Nivel</label>
                                                    <input type="text" placeholder="Nivel 1" class="form-control" name="" id="tab2_vg_nivel" disabled>
                                                </div>
                                                <div class="form-group"><label>Estado de Contacto</label>
                                                    <input type="text" placeholder="Activo" class="form-control" name="" id="tab2_vg_estado" disabled>
                                                </div>
                                                <div class="form-group"><label>Teléfono de Oficina</label> 
                                                    <input type="text" placeholder="+56 9 12345678" class="form-control" name="" id="tab2_vg_telefono" disabled>
                                                </div>
                                                <div class="form-group"><label>Teléfono Móvil</label> 
                                                    <input type="text" placeholder="+56 9 12345678" class="form-control" name="" id="tab2_vg_movil" disabled>
                                                </div>
												<div class="form-group"><label>Fecha de Cumpleaños</label> 
                                                    <input type="text" placeholder="dd/mm/aa" class="form-control" name="" id="tab2_vg_fecha" disabled>
                                                </div>
                                                <div class="form-group"><label>Fax</label> 
                                                    <input type="text" placeholder="+56 9 12345678" class="form-control" name="" id="tab2_vg_fax" disabled>
                                                </div>
                                            </div>
                                    </form> 
                                
                                
                            </div>

                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <strong>Datos para Facturación</strong>

                                <form role="form">
                                    <div class="col-md-6">                                             
                                        <div class="form-group"><label>Dirección Despacho Factura</label> 
                                            <input type="text" placeholder="+56 9 12345678" class="form-control" name="" id="tab3_dt_despacho" disabled>
                                        </div>
                                        <form action="">                                            
                                            <div class="form-group"><label>Empresa Trabaja con OC Interna</label>

                                                <div><label> <input type="radio" checked="" value="option1" id="tab3_dt_oc1" name="optionsRadios" disabled="">Si</label></div>
                                                <div><label> <input type="radio" value="option2" id="tab3_dt_oc2" name="optionsRadios" disabled="">No</label></div>

                                            </div>
                                        </form>

                                        <form action="">                                            
                                            <div class="form-group"><label>Empresa Trabaja con OTA</label>

                                                <div><label> <input type="radio" value="option3" id="tab3_dt_ota1" name="optionsRadios" disabled="">Si</label></div>

                                                <div><label> <input type="radio" checked="" value="option4" id="tab3_dt_ota2" name="optionsRadios" disabled="">No</label></div>

                                            </div>
                                        </form>                                        

                                    </div>

                                    <div class="col-md-6">
                                        <form action="">                                            
                                            <div class="form-group"><label>Empresa Trabaja con HES</label>

                                                <div><label> <input type="radio" value="option5" id="tab3_dt_hes1" name="optionsRadios" disabled="">Si</label></div>

                                                <div><label> <input type="radio" checked="" value="option6" id="tab3_dt_hes2" name="optionsRadios" disabled="">No</label></div>

                                            </div>
                                        </form>                                            
                                        <div class="form-group"><label>Otro (Describa)</label> <textarea name="" id="tab3_dt_otro" cols="30" rows="2" readonly="" style="width: 100%">Lorem ipsum dolor sit amet.</textarea></div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <strong>Datos Entrega de Factura</strong>

                                <div class="form-group">
                                    <h5>Seleccione una o más opciones.</h5>

                                        <label class="checkbox-inline"> <input type="checkbox" disabled="" checked="" value="option1" id="tab4_df_sii"> Sólo SII </label> <br>
                                        <label class="checkbox-inline"> <input type="checkbox" disabled="" value="option2" id="tab4_df_mano"> Mano (Timbre) </label> <br>
                                        <label class="checkbox-inline"> <input type="checkbox" disabled="" checked="" value="option3" id="tab4_df_chilexpress"> Chilexpress </label>

                                </div>
                                
                            </div>
                        </div>
                        <div id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                <strong>Datos Finanzas</strong>

                                <form role="form">
                                    <h5>Datos Persona de Contaco para Cobranza</h5>
                                    <div class="col-md-6">                                             
                                        <div class="form-group"><label>Teléfono Oficina</label>
                                            <input type="text" placeholder="+56 9 12345678" class="form-control" name="" id="tab5_c_oficina" disabled>
                                        </div>
                                        <div class="form-group"><label>Teléfono Móvil</label>
                                            <input type="text" placeholder="+56 9 12345678" class="form-control" name="" id="tab5_c_movil" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-6">                                            
                                        <div class="form-group"><label>Correo</label>
                                            <input type="text" placeholder="correo@cuenta.cl" class="form-control" name="" id="tab5_c_correo" disabled>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div id="tab-6" class="tab-pane">
                            <div class="panel-body">
                                <strong>Datos Entrega de Diplomas</strong>

                                <form role="form">
                                    <h5>Datos Persona de contacto</h5>
                                    <div class="col-md-6">                                             
                                        <div class="form-group"><label>Dirección para Entrega Diplomas</label>
                                            <input type="text" placeholder="Dirección 123, Comuna" class="form-control" name="" id="tab6_dd_direccion" disabled>
                                        </div>
                                        <div class="form-group"><label>Teléfono Oficina</label>
                                            <input type="text" placeholder="+56 2 21234567" class="form-control" name="" id="tab6_dd_oficina" disabled>
                                        </div>
                                    </div>

                                    <div class="col-md-6">                                            
                                        <div class="form-group"><label>Teléfono Móvil</label>
                                            <input type="text" placeholder="+56 2 21234567" class="form-control" name="" id="tab6_dd_movil" disabled>
                                        </div>
                                        <div class="form-group"><label>Correo</label>
                                            <input type="text" placeholder="correo@cuenta.cl" class="form-control" name="" id="tab6_dd_correo" disabled>
                                        </div>
                                    </div>
                                </form>
                            </div>                                    
                        </div>
						<br>
                           <a href="editar_cuenta">                               
                              <button type="" class="btn btn-w-m btn-primary pull-right">Editar</button>
                           </a> 
                    </div>

                </div>

            </div>

        </div>

 		<!-- ventana flotant -->
<!-- <style>

    .vFlotante{
        overflow: scroll;
        position: fixed;
        left: 96%;
        top: 22%;
        width: 690px;
        height: 400px;
        transition: all .4s ease-in;
        -webkit-transition: all .4s ease-in;
        background-color: #fff;
        z-index: 1;
        border-radius: 5px;
        border: 5px solid  #3e2c42;
        border-left-width: 60px;
        border-right-width: 0px;
        box-shadow: 7px 5px 20px 1px rgba(0, 0, 0, 0.58);
    }

    .vFlotante:hover{
        left: 49%;
    }

    .titulo{
        padding-left: 15px;  
    }
</style> -->
		
		<div class="row">
            <div class="col-md-6">
                <div class="ibox">
                    <div class="ibox-content">

                      <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="5">
                          <thead>
                          <tr class="fila_historial">

                              <th data-toggle="true">Nivel Contacto</th>
                              <th data-hide="phone">Acción</th>
                              <th data-hide="all"><span></span></th>

                          </tr>
                          </thead>
                          <tbody>
                          <tr class="fila_historial">
                              <td>
                                 <strong>Nivel 1 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                              </td>
                              <td>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                              </td>
                              <td>
                                <table>
                                  <tbody>                                                    
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>                                              
                              </td>
                          </tr>
                          <tr class="fila_historial">
                              <td>
                                 <strong>Nivel 2 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                              </td>
                              <td>
                                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                              </td>
                              <td>
                                <table>
                                  <tbody>                                                    
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>                                              
                              </td>
                          </tr>
                          <tr class="fila_historial">
                              <td>
                                 <strong>Nivel 3 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                              </td>
                              <td>
                                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                              </td>
                              <td>
                                <table>
                                  <tbody>                                                    
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>                                              
                              </td>
                          </tr>
                          <tr class="fila_historial">
                              <td>
                                 <strong>Nivel 4 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                              </td>
                              <td>
                                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                              </td>
                              <td>
                                <table>
                                  <tbody>                                                    
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>                                              
                              </td>
                          </tr>
                          </tbody>                                        
                      </table>                                      
                    </div>
                </div>  
            </div>
         </div>

         
         <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
             <div class="modal-dialog">
                 <div class="modal-content animated bounceInRight">
                     <!-- MODAL HEADER -->
                     <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                         <i class="fa fa-user modal-icon"></i>
                         <h4 class="modal-title">Añadir Nuevo Contacto</h4>
                         <small class="font-bold">Ingrese información.</small>
                     </div>

                     <!-- MODAL BODY -->
                    <div class="modal-body col-md-12">
                        <form role="form">
                            <h5>Datos Persona de Contacto</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nombre(*)</label>
                                    <input type="text" placeholder="Ingrese Nombre" class="form-control" required="">                                    
                                </div>
                                <div class="form-group">
                                    <label>Cargo (*)</label>
                                    <input type="text" placeholder="Ingrese Cargo" class="form-control" required="">                                    
                                </div>
                                <div class="form-group">
                                    <label>Departamento (*)</label>
                                    <input type="text" placeholder="Ingrese Departamento" class="form-control" required="">
                                </div>
                                <div class="form-group">
                                    <label>Correo Electrónico (*)</label> 
                                    <input type="email" placeholder="Ingrese Email" class="form-control" required="">                                   
                                </div>
								<div class="form-group">
									<label>Cantidad de Hijos</label> 
									<input type="number" placeholder="Ingrese Cantidad" class="form-control" required="">
								</div>
                                <div class="form-group">
                                    <label>Descripción del Contacto</label>
                                    <textarea name="" id="" cols="30" rows="2" style="width: 100%;"></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div> <!--MENU SELECT -->
                                    <label for="">Nivel</label>
                                        <select class="form-control" style="width: 100%;" disabled="">
                                            <option>Nivel 1</option>
                                            <option>Nivel 2</option>
                                            <option>Nivel 3</option>
                                        </select>
                                </div> <br>
                                <div class="form-group">
                                    <label>Estado de Contacto (*)</label>
                                    <input type="text" placeholder="Estado de Contacto" class="form-control" required="">                                    
                                </div>
                                <div class="form-group">
                                    <label>Teléfono de Oficina (*)</label>
                                    <input type="text" placeholder="Ingrese Télefono" class="form-control" required="">
                                </div>
                                <div class="form-group">
                                    <label>Teléfono Móvil</label>
                                    <input type="text" placeholder="Ingrese Móvil" class="form-control" required="">
                                </div>
								<div class="form-group" id="data_1">
									<label class="font-normal">Fecha Cumpleaños</label>
									<div class="input-group date">
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="03/04/2014">
									</div>
								</div>	
                                <div class="form-group">
                                    <label>Fax</label>
                                    <input type="text" placeholder="Ingrese Fax" class="form-control">
                                </div>
                            </div>                          

                        </form>   
                    </div>
                     <!-- MODAL FOOTER -->
                     <div class="modal-footer">
                         <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                         <button type="submit" value="Submit" class="btn btn-primary" onclick="guardar(1)">Añadir</button>
                     </div>

                 </div>
             </div>
         </div>
         
    </div>
</div>