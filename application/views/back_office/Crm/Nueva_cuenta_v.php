<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>

<div class="wrapper wrapper-content animated fadeIn">
            <div class="ibox-title">
                <h5>Ingresar Nueva Cuenta</h5>
            </div>
    <div class="row m-t-lg">    
<!-- Cambiar a  COL-MD-6 cuando se habilite ingreso contacto -->    
        <div class="col-md-8">            
            <div class="tabs-container">


	
	
                <div class="tabs-left">
                    <ul class="nav nav-tabs">
                        <li class="active"  data-toggle="tooltip" data-placement="bottom" title="Detalle de la Cuenta"><a data-toggle="tab" href="#tab-1"><i class="fa fa-briefcase fa-2x"></i></a></li>
                        <!--<li class=""  data-toggle="tooltip" data-placement="bottom" title="Vision de la Cuenta"><a data-toggle="tab" href="#tab-2"><i class="fa fa-eye fa-2x"></i></a></li>-->
                        <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos para Facturación"><a data-toggle="tab" href="#tab-3"><i class="fa fa-pencil-square-o fa-2x"></i></a></li>
                        <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Entrega Factura"><a data-toggle="tab" href="#tab-4"><i class="fa fa-check-square-o fa-2x" ></i></a></li>
                        <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Finanzas"><a data-toggle="tab" href="#tab-5"><i class="fa fa-area-chart fa-2x" ></i></a></li>
                        <li class="" data-toggle="tooltip" data-placement="bottom" title="Datos Entrega de Diplomas"><a data-toggle="tab" href="#tab-6"><i class="fa fa-mortar-board fa-2x" ></i></a></li>
                    </ul>
	<form class="" id="form-nueva-cuenta"> 					
                    <div class="tab-content ">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <strong>Detalle de la Cuenta</strong>
									<h5>Ingrese Datos Empresa</h5>
									<hr>
									<!-- PRIMERA COLUMNA -->
                                    <div class="col-md-6">  								

										<div class="form-group">
											<label><small>Nombre de Fantasía</small></label> 
											<input type="text" 
											id="Var_nombre_fantasia" 
											name="Var_nombre_fantasia" 
											placeholder="Ingrese Nombre Fantasía" 
											class="form-control"
											style="border: 1px solid #aaaaaa;">
										</div>
										
										<div class="form-group">
											<label><small>Razón Social (*)</small></label> 
											<input type="text" 
											id="Var_razon_social_empresa" 
											name="Var_razon_social_empresa" 
											placeholder="Ingrese Razon Social" 
											class="form-control" requerido="true" 
											mensaje="Ingrese Razón Social"
											style="border: 1px solid #aaaaaa;">
										</div>
										
										<div class="form-group">
											<label><small>Giro de Empresa</small></label> 
											<input type="text" 
											id="Var_giro_empresa" 
											name="Var_giro_empresa" 
											placeholder="Ingrese Giro de Empresa" 
											class="form-control"
											style="border: 1px solid #aaaaaa;">
										</div>
                                        
										<div class="form-group" style="margin-bottom: 0px;">
											<div class="row">
												 <label class="control-label" style="padding-left: 14px"><small>Rut (*)</small></label>
												<div class="col-md-12">											
												  <div class="row">	
													<div class="col-md-8" style="padding-right: 0">												
														<input type="hidden" id="id_edutecno" name="id_edutecno"                                   
															   value="">													   
														<input type="number" class="form-control required"
															   id="Var_rut_empresa" 
															   requerido="true"
															   mensaje ="Debe Ingresar el RUT correspondiente"
															   placeholder="Ej. 123456789"
															   name="Var_rut_empresa"
															   maxlength="8"
															   min="0"
															   class="form-control" onkeypress="return solonumero(event)"
															   value=""
															   style="border: 1px solid #aaaaaa;">
													</div>
													<p style="width: 0; padding:10px" class="col-md-1">-</p>
														<div class="col-md-3" style="padding-left: 0">
															<input type="text" class="form-control required"
																   id="dv" 
																   maxlength="1" 
																   requerido="true"
																   mensaje ="Debe Ingresar el digito verificador"
																   placeholder="Ej. 0"
																   name="dv"
																   class="form-control" 
																   value=""
																   style="border: 1px solid #aaaaaa;">
														</div>
														<!--
														<div class="col-lg-3">
															<p id="out_print" style="color: red"></p>
														</div>
														-->
												</div>	
											   </div>
											</div>											
										</div>
										
                                        <div class="form-group">
											<label><small>Clasificación de la Cuenta</small></label> 
											<select class="form-control" 
											id="Var_crm_clasificacion_cuenta" 
											name="Var_crm_clasificacion_cuenta">                                                                            
											</select>
										</div>
										
										
                                        <div class="form-group">
											<label><small>Dirección Empresa (*)</small></label> 
											<input type="text" 
											id="Var_direccion_empresa" 
											name="Var_direccion_empresa" 
											placeholder="Ingrese Dirección" 
											class="form-control" 
											requerido="true" 
											mensaje="Ingrese Dirección"
											style="border: 1px solid #aaaaaa;">
										</div>
										
										<div class="form-group">
											<label><small>País</small></label>										
											<select class="form-control" 
											id="Var_crm_id_pais" 
											name="Var_crm_id_pais">												
											</select>
										</div>

                                        <div class="form-group" >
										   <label for=""><small>Comuna</small></label>
										   <select class="form-control" 
										   id="Var_crm_comuna" 
										   name="Var_crm_comuna">
										   </select>
										</div>
										
										<br> 
										
                                        <div class="form-group">
											<label><small>Descripción de la Cuenta</small></label> 
											<textarea name="Var_descripcion_empresa" 
											id="Var_descripcion_empresa" 
											cols="30" rows="2" 
											style="width: 100%; border: 1px solid #aaaaaa;">
											</textarea>
										</div>
										
										<!--<div class="fileinput fileinput-new" data-provides="fileinput">
											<label for="" required>Seleccione logo</label> <br>
											<span class="btn btn-default btn-file"><span class="fileinput-new">Seleccione Archivo</span><span class="fileinput-exists"> Cambiar</span><input type="file" name="..."></span>
											<span class="fileinput-filename"></span>
											<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
									
										</div>-->
                                    </div>
									<!-- FIN PRIMERA COLUMNA -->
									
									<!-- SEGUNDA COLUMNA -->
                                    <div class="col-md-6">
									
                                        <div class="form-group" style="margin-bottom: 0px;">
											<label><small>OTIC Relacionada</small></label> 
											<input type="text" 
											id="Var_crm_otic_cuenta"  
											name="Var_crm_otic_cuenta" 
											placeholder="Ingrese OTIC" 
											class="form-control"
											style="border: 1px solid #aaaaaa;">
										</div>
                                       
										<!--
										   <label for=""><small>Tipo de Industria</small></label>
											<select class="form-control" id="tab1_dc_tipo">
											  <option value="">Seleccione</option>
											  <option value="Tipo 1">Tipo 1</option>
											  <option value="Tipo 2">Tipo 2</option>                                         
											</select>
										-->											
                                       <br>
									   
									   <div class="form-group" style="margin-bottom: 0px;">
										   <label for=""><small>Holding Perteneciente</small></label>
										   <select class="form-control holding" 
										   id="Var_id_holding" 
										   name="Var_id_holding">                                                                            
										   </select>
										</div> 
										
									   <br>
									   
									   <div class="form-group" style="margin-bottom: 0px;">
										   <label for=""><small>Tamaño de la Empresa (*)</small></label>
										   <select class="form-control" 
										   id="Var_crm_tamano_empresa" 
										   name="Var_crm_tamano_empresa" 
										   requerido="true" 
										   mensaje="Seleccione Tamaño Empresa">                                         
										   </select>
									   </div>
									   
									   <br>
									   
										<div class="form-group">
											<label><small>Rubro Empresa (*)</small></label> 
											<select class="form-control" 
											id="tab1_detalle_cuenta_rubro" 
											name="tab1_detalle_cuenta_rubro">                                                                            
											</select>											
										</div>
									
										<div class="form-group">
											<label><small>Subrubro Empresa</small></label> 
											<select class="form-control" 
											id="tab1_detalle_cuenta_subrubro" 
											name="tab1_detalle_cuenta_subrubro">                                                                            
											</select>											
										</div>
                                       
                                    </div>
									<!-- FIN SEGUNDA COLUMNA -->
                                
                            </div>                            
                        </div>
					
					<!--	
						
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <strong>Visión General</strong>                                    
                                        <h5>Datos Persona de Contacto</h5>
										<hr>
                                        <div class="col-md-6">                                             
                                            <div class="form-group"><label><small>Nombre (*)</small></label> <input type="text" id="tab2_vg_nombre" placeholder="Ingrese Nombre" class="form-control" requerido="true" mensaje="Ingrese Nombre de Contacto"></div>
                                            <div class="form-group"><label><small>Cargo</small></label> <input type="text" id="tab2_vg_cargo" placeholder="Ingrese Cargo" class="form-control"></div>
                                            <div class="form-group"><label><small>Departamento</small></label> <input type="text" id="tab2_vg_dpto" placeholder="Ingrese Departamento" class="form-control"></div>
                                            <div class="form-group"><label><small>Correo Electrónico (*)</small></label> <input type="email" id="tab2_vg_correo" placeholder="Ingrese Correo" class="form-control" requerido="true" mensaje="Ingrese Correo"></div>
											<div class="form-group"><label><small>Cantidad de Hijos</small></label> <input type="number" id="tab2_vg_hijos" placeholder="Ingrese Cantidad" class="form-control" min="0"></div>
                                            <div class="form-group"><label><small>Descripción del Contacto</small></label> <textarea name="" id="tab2_vg_descripcion" cols="30" rows="2" style="width: 100%"></textarea></div>
                                        </div>

                                        <div class="col-md-6">      
                                            <div>
                                                <label for=""><small>Nivel (*)</small></label>
                                                    <select id="tab2_vg_nivel" class="form-control" style="width: 100%;" requerido="true" mensaje="Seleccione Nivel Contacto">
													  <option value="">Seleccione</option>
                                                      <option value="Nivel 1">Nivel 1</option>
                                                      <option value="Nivel 2">Nivel 2</option>
                                                      <option value="Nivel 3">Nivel 3</option>               
                                                    </select>
                                            </div>           <br>                           
                                            <div class="form-group"><label><small>Estado de Contacto</small></label> <input type="text" id="tab2_vg_estado" placeholder="Estado Contacto" class="form-control"></div>
                                            <div class="form-group"><label><small>Teléfono de Oficina (*)</small></label> <input type="text" id="tab2_vg_telefono" placeholder="Ingrese Télefono" class="form-control bfh-phone" data-format="+56 (d) ddd-dddd" requerido="true" mensaje="Ingrese Teléfono"></div>
                                            <div class="form-group"><label><small>Teléfono Móvil (*)</small></label> <input type="text" id="tab2_vg_movil" placeholder="Ingrese Móvil" class="form-control bfh-phone" data-format="+56 (d) dddddddd" requerido="true" mensaje="Ingrese Movil"></div>
											
											<div class="form-group" id="data_1">
												<label class="font-normal"><small>Fecha Cumpleaños</small></label>
													<div class="input-group date">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" id="tab2_vg_fax" class="form-control" value="">
													</div>
											</div>
											
                                            <div class="form-group"><label><small>Fax</small></label> <input type="text" id="tab2_vg_fecha" placeholder="Ingrese Fax" class="form-control bfh-phone" data-format="+56 (d) ddd-dddd" ></div>
                                           
                                        </div>
                                   
                                
                                
                            </div>
                        </div>
						
						
					-->	
						
						
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <strong>Datos para Facturación</strong>
									<hr>
                                
                                    <div class="col-md-6"> 
									
                                        <div class="form-group">
											<label><small>Dirección Despacho Factura</small></label> 
											<input type="text" 
											id="Var_direccion_factura" 
											name="Var_direccion_factura" 
											placeholder="Ingrese Dirección"
											style="border: 1px solid #aaaaaa;"											
											class="form-control">
										</div>
                              
                                        <div class="form-group"><label><small>Empresa Trabaja con OC Interna</small></label>										
                                            <div>
												<label> 
													<input type="radio" 
													id="Var_requiere_orden_compra" 
													name="Var_requiere_orden_compra" 
													value="1">Si</label>
											</div>
											
                                            <div>
												<label> 
													<input type="radio" 
													id="Var_requiere_orden_compra" 
													name="Var_requiere_orden_compra" 
													value="0"												
													checked="checked">No</label>
											</div>								
                                        </div>
                                      
									  
                                        <div class="form-group"><label><small>Empresa Trabaja con OTA</small></label>
                                            <div>
												<label> 
													<input type="radio" 
													id="Var_requiere_ota" 
													name="Var_requiere_ota"												
													value="1">Si</label>
											</div>
											
                                            <div>
												<label> 
													<input type="radio" 
													id="Var_requiere_ota"
													name="Var_requiere_ota" 
													value="0" checked="checked">No</label>
											</div>
                                        </div>                                     

                                    </div>

                                    <div class="col-md-6"> 
									
                                            <div class="form-group"><label><small>Empresa Trabaja con HES</small></label>

                                                <div>
													<label> 
														<input type="radio" 
														id="Var_requiere_hess" 
														name="Var_requiere_hess" 
														value="1">Si</label>
												</div>
												
                                                <div>
													<label> 
														<input type="radio" 
														id="Var_requiere_hess" 
														name="Var_requiere_hess" 
														value="0" checked="checked">No</label>
												</div>
												
                                            </div>
											
											<div class="form-group">
												<label><small>Otro (Describa)</small></label> 
												<textarea name="Var_observaciones_glosa_facturacion" 
												id="Var_observaciones_glosa_facturacion" 
												cols="30" rows="2" 
												style="border: 1px solid #aaaaaa; width: 100%"></textarea>
												</div>	
									</div>     	
                            </div>
                        </div>           
						
						<div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <strong>Datos Entrega de Factura</strong>
								<hr>
                                <div class="form-group">
                                    <h5>Seleccione una o más opciones.</h5>

                                        <label class="checkbox-inline"> 
											<input type="checkbox" 
											value="1" 
											id="Var_crm_sii" 
											name="Var_crm_sii"> Sólo SII </label> 
											<br>
											
                                        <label class="checkbox-inline"> 
											<input type="checkbox" 
											value="1" 
											id="Var_crm_mano_timbrada" 
											name="Var_crm_mano_timbrada"> Mano (Timbre) </label>
											<br>
											
                                        <label class="checkbox-inline"> 
										<input type="checkbox" 
										value="1" 
										id="Var_crm_chilexpress" 
										name="Var_crm_chilexpress"> Chilexpress </label>
                                </div>                                
                            </div>
                        </div>
                        	
						<div id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                <strong>Datos Cobranza</strong>                 
                                    <h5>Datos Persona de Contacto para Cobranza</h5>                         
									<hr>
                                    <div class="col-md-6">   
									
                                        <div class="form-group">
											<label><small>Nombre Contacto Cobranza</small></label> 
											<input type="text" 
											id="Var_nombre_contacto_cobranza" 
											name="Var_nombre_contacto_cobranza" 
											placeholder="Ingrese Contacto Cobranza" 
											class="form-control" 
											required=""
											style="border: 1px solid #aaaaaa;">
										</div>
											
                                        <div class="form-group">
											<label><small>Teléfono</small></label> 
											<input type="text" 
											id="Var_telefono_contacto_cobranza" 
											name="Var_telefono_contacto_cobranza" 
											placeholder="Ingrese Teléfono" 
											class="form-control" 
											required=""
											style="border: 1px solid #aaaaaa;">
										</div>
										
                                        <!--<div class="form-group">
												<label><small>Teléfono</small></label> 
												<input type="text" 
												id="tab5_c_movil" 
												placeholder="Ingrese Móvil" 
												class="form-control" 
												required=""
												style="border: 1px solid #aaaaaa;">
											</div>-->
                                    </div>

                                    <div class="col-md-6"> 
									
                                        <div class="form-group">
											<label><small>Correo</small></label> 
											<input type="email" 
											id="Var_mail_contacto_cobranza" 
											name="Var_mail_contacto_cobranza" 
											placeholder="Ingrese Correo" 
											class="form-control" 
											style="border: 1px solid #aaaaaa;">
										</div>
                                       
                                    </div> 
                            </div>
                        </div>
						
						<div id="tab-6" class="tab-pane">
                            <div class="panel-body">
                                <strong>Datos Entrega de Diplomas</strong>
									<hr>
                                    <div class="col-md-6">
									
										<div class="form-group">
											<label><small>Contacto Persona Diploma</small></label> 
											<input type="text" 
											id="Var_crm_contacto_persona_diploma"  
											name="Var_crm_contacto_persona_diploma" 
											placeholder="Ingrese Dirección" 
											class="form-control" 
											required=""
											style="border: 1px solid #aaaaaa;">
										</div>
										
                                        <div class="form-group">
											<label><small>Dirección para Entrega Diplomas</small></label> 
											<input type="text" 
											id="Var_crm_direccion_entrega_diploma" 
											name="Var_crm_direccion_entrega_diploma" 
											placeholder="Ingrese Dirección" 
											class="form-control" 
											style="border: 1px solid #aaaaaa;">
										</div>
										
                                        <div class="form-group">
											<label><small>Teléfono Oficina</small></label> 
											<input type="text" 
											id="Var_crm_telefono_oficina_diploma"  
											name="Var_crm_telefono_oficina_diploma" 
											placeholder="Ingrese Teléfono" 
											class="form-control bfh-phone" 
											data-format="+56 (d) ddd-dddd" 
											style="border: 1px solid #aaaaaa;">
										</div>
										
                                    </div>

                                    <div class="col-md-6">  
									
                                        <div class="form-group">
											<label><small>Teléfono Móvil</small></label> 
											<input type="text" 
											id="Var_crm_telefono_movil_diploma" 
											name="Var_crm_telefono_movil_diploma" 
											placeholder="Ingrese Móvil" 
											class="form-control bfh-phone" 
											data-format="+56 (d) dddddddd" 
											style="border: 1px solid #aaaaaa;">
										</div>
										
                                        <div class="form-group">
											<label><small>Correo</small></label> 
											<input type="email" 
											id="Var_crm_correo_diploma" 
											name="Var_crm_correo_diploma" 
											placeholder="Ingrese Correo" 
											class="form-control" 
											style="border: 1px solid #aaaaaa;">
										</div>
                                        
                                    </div>                                
                            </div>                                    
                        </div>
						<br>
						</form>
						<div  class="form-group">
							<button type="button" class="btn btn-w-m btn-primary demo2 pull-right" id="btn-nueva-cuenta">Guardar</button>	
						</div>

						
                    </div>
                </div>
	
            </div>
        </div>

		<!-- ventana flotant -->
<!-- <style>

    .vFlotante{
        overflow: scroll;
        position: fixed;
        left: 96%;
        top: 22%;
        width: 690px;
        height: 400px;
        transition: all .4s ease-in;
        -webkit-transition: all .4s ease-in;
        background-color: #fff;
        z-index: 1;
        border-radius: 5px;
        border: 5px solid  #3e2c42;
        border-left-width: 60px;
        border-right-width: 0px;
        box-shadow: 7px 5px 20px 1px rgba(0, 0, 0, 0.58);
    }

    .vFlotante:hover{
        left: 49%;
    }

    .titulo{
        padding-left: 15px;  
    }
</style> -->
		
	<!--	<div class="vFlotante">
			<h1 class="titulo" style="padding-left: 15px;">Nuevo contacto</h1>
				<div class="col-md-12">
                    <div class="ibox">
                        <div class="ibox-content">

                          <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="5">
                              <thead>
                              <tr class="fila_historial">

                                  <th data-toggle="true">Nivel Contacto</th>
                                  <th data-hide="phone">Acción</th>
                                  <th data-hide="all"><span></span></th>

                              </tr>
                              </thead>
                              <tbody>
                              <tr class="fila_historial">
                                  <td>
                                     <strong>Nivel 1 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                                  </td>
                                  <td>
                                      <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                                  </td>
                                  <td>
                                    <table>
                                      <tbody>                                                    
                                        <tr>                                                      
                                            <td>Nombre Apellido</td>
                                            <td>  
                                                <a href="detalle_cuenta">                                                    
                                                  <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ;">Detalle</button>
                                                </a>
                                            </td>
                                        </tr>
                                      </tbody>
                                    </table>                                              
                                  </td>
                              </tr>
                              <tr class="fila_historial">
                                  <td>
                                     <strong>Nivel 2 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                                  </td>
                                  <td>
                                      <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                                  </td>
                                  <td>
                                    <table>
                                      <tbody>                                                    
                                        <tr>                                                      
                                            <td>Nombre Apellido</td>
                                            <td>  
                                                <a href="detalle_cuenta">                                                    
                                                  <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ;">Detalle</button>
                                                </a>
                                            </td>
                                        </tr>
                                      </tbody>
                                    </table>                                              
                                  </td>
                              </tr>
                              <tr class="fila_historial">
                                  <td>
                                     <strong>Nivel 3 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                                  </td>
                                  <td>
                                      <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                                  </td>
                                  <td>
                                    <table>
                                      <tbody>                                                    
                                        <tr>                                                      
                                            <td>Nombre Apellido</td>
                                            <td>  
                                                <a href="detalle_cuenta">                                                    
                                                  <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ;">Detalle</button>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>                                                      
                                            <td>Nombre Apellido</td>
                                            <td>  
                                                <a href="detalle_cuenta">                                                    
                                                  <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ;">Detalle</button>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>                                                      
                                            <td>Nombre Apellido</td>
                                            <td>  
                                                <a href="detalle_cuenta">                                                    
                                                  <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ;">Detalle</button>
                                                </a>
                                            </td>
                                        </tr>
                                      </tbody>
                                    </table>                                              
                                  </td>
                              </tr>
                              <tr class="fila_historial">
                                  <td>
                                     <strong>Nivel 4 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                                  </td>
                                  <td>
                                      <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                                  </td>
                                  <td>
                                    <table>
                                      <tbody>                                                    
                                        <tr>                                                      
                                            <td>Nombre Apellido</td>
                                            <td>  
                                                <a href="detalle_cuenta">                                                    
                                                  <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ;">Detalle</button>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>                                                      
                                            <td>Nombre Apellido</td>
                                           <td>  
                                               <a href="detalle_cuenta">                                                    
                                                 <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ;">Detalle</button>
                                               </a>
                                           </td>
                                        </tr>
                                        <tr>                                                      
                                            <td>Nombre Apellido</td>
                                            <td>  
                                                <a href="detalle_cuenta">                                                    
                                                  <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ;">Detalle</button>
                                                </a>
                                            </td>
                                        </tr>
                                      </tbody>
                                    </table>                                              
                                  </td>
                              </tr>
                              </tbody>                                        
                          </table>                                      
                        </div>
                    </div>  
                </div>
		</div>  -->  
		
		<div class="row" style="display:none;">
            <div class="col-md-6">
                <div class="ibox">
                    <div class="ibox-content">

                      <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="5">
                          <thead>
                          <tr class="fila_historial">

                              <th data-toggle="true">Nivel Contacto</th>
                              <th data-hide="phone">Acción</th>
                              <th data-hide="all"><span></span></th>

                          </tr>
                          </thead>
                          <tbody>
                          <tr class="fila_historial">
                              <td style="width: 189px;">
                                 <strong>Nivel 1 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                              </td>
                              <td>
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                              </td>
                              <td>
                                <table>
                                  <tbody>                                                    
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>                                              
                              </td>
                          </tr>
                          <tr class="fila_historial">
                              <td>
                                 <strong>Nivel 2 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                              </td>
                              <td>
                                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                              </td>
                              <td>
                                <table>
                                  <tbody>                                                    
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>                                              
                              </td>
                          </tr>
                          <tr class="fila_historial">
                              <td>
                                 <strong>Nivel 3 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                              </td>
                              <td>
                                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                              </td>
                              <td>
                                <table>
                                  <tbody>                                                    
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>                                              
                              </td>
                          </tr>
                          <tr class="fila_historial">
                              <td>
                                 <strong>Nivel 4 <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></strong>
                              </td>
                              <td>
                                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">Añadir Nuevo</button>
                              </td>
                              <td>
                                <table>
                                  <tbody>                                                    
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>                                                      
                                        <td>Nombre Apellido</td>
                                        <td>
                                            <a href="editar_cuenta">                                                
                                                <button type="button" class="btn-info btn pull-right btn-sm" style="margin: 5px 0px 5px 141px ">Editar</button>
                                            </a>
                                        </td>
                                    </tr>
                                  </tbody>
                                </table>                                              
                              </td>
                          </tr>
                          </tbody>                                        
                      </table>                                      
                    </div>
                </div>  
            </div>
         </div>

		 
        </div>
        <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <!-- MODAL HEADER -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <i class="fa fa-user modal-icon"></i>
                        <h4 class="modal-title">Añadir Nuevo Contacto</h4>
                        <small class="font-bold">Ingrese información.</small>
                    </div>
                    <!-- MODAL BODY -->
                    <div class="modal-body col-md-12">
                        <form role="form">
                            <h5>Datos Persona de Contacto</h5>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nombre(*)</label>
                                    <input type="text" placeholder="Ingrese Nombre" class="form-control" >                                    
                                </div>
                                <div class="form-group">
                                    <label>Cargo (*)</label>
                                    <input type="text" placeholder="Ingrese Cargo" class="form-control" >                                    
                                </div>
                                <div class="form-group">
                                    <label>Departamento (*)</label>
                                    <input type="text" placeholder="Ingrese Departamento" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>Correo Electrónico (*)</label> 
                                    <input type="email" placeholder="Ingrese Email" class="form-control" >                                   
                                </div>
								<div class="form-group">
									<label>Cantidad de Hijos</label> 
									<input type="number" placeholder="Ingrese Cantidad" class="form-control" >
								</div>
                                <div class="form-group">
                                    <label>Descripción del Contacto</label>
                                    <textarea name="" id="" cols="30" rows="2" style="width: 100%;"></textarea>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div> <!--MENU SELECT -->
                                    <label for="">Nivel</label>
                                        <select class="form-control" style="width: 100%;" disabled="">
                                            <option>Nivel 1</option>
                                            <option>Nivel 2</option>
                                            <option>Nivel 3</option>
                                        </select>
                                </div> <br>
                                <div class="form-group">
                                    <label>Estado de Contacto (*)</label>
                                    <input type="text" placeholder="Estado de Contacto" class="form-control" >                                    
                                </div>
                                <div class="form-group">
                                    <label>Teléfono de Oficina (*)</label>
                                    <input type="text" placeholder="Ingrese Télefono" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label>Teléfono Móvil</label>
                                    <input type="text" placeholder="Ingrese Móvil" class="form-control" >
                                </div>
								<div class="form-group" id="data_1">
												<label class="font-normal">Fecha Cumpleaños</label>
													<div class="input-group date">
														<span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" value="<?php echo date("d-m-Y");?>">
													</div>
											</div>
                                <div class="form-group">
                                    <label>Fax</label>
                                    <input type="text" placeholder="Ingrese Fax" class="form-control">
                                </div>
                            </div>                          

                        </form>   
                    </div>
                    <!-- MODAL FOOTER -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                        <button type="submit" value="Submit" class="btn btn-primary" onclick="guardar(1)">Añadir</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
