<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre ] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-md-5">
      <div class="ibox float-e-marfins" id="div_general" name="div_general">

        <div class="ibox-title" style="background-color: #1ab394; border-top-left-radius: 10px; border-top-right-radius: 10px;">                    
         <h2 class="text-center" style="color:#fff; text-transform: uppercase;">Listado</h2> 
       </div>


       <div class="ibox-title">
       <table style="width: 100%">
         <tr>
           <td>
             <h3>Seleccione Minuta</h3>
           </td>
           <td style="text-align: right;">
           <button type="button" onclick="modalllamada();" value="submit" class="btn btn-primary" id="btn_nueva_minuta" name="btn_nueva_minuta">Nueva</button>
           </td>
         </tr>
       </table>
        
      </div>

<div class="ibox-title">
                <table style="width: 50%">
                    <tr>
                        <td style="width: 5px">
                           <b>Leyenda: </b>   
                       </td>
                       <td style="text-align: center">
                           <i class="fa fa-tasks" style="color:#337ab7"></i><b> : Formulario</b>
                       </td>
                       <td style="text-align: center">
                           <i class="fa fa-envelope" style="color:#dd5f1a"></i><b> : Correo</b> 
                       </td>
                   </tr>
               </table>
               </div>


      <div class="ibox-content" style="border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;height: 396px; overflow: scroll;">
        <div class="row">
          <div class="col-md-12"> 
            <div>

              <div>       


              <table id="tbl_minuta" class="table table-hover" style="width: 100%">
                  <thead>
                    <tr>
                      <th>Asunto</th>
                      <th>Origen</th>
                      <th>Fecha</th>
                      <th>Eliminar</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php for ($i=0;$i<count($datos_minuta);$i++) {

                      if($datos_minuta[$i]['id_tipo_origen'] == 1){

                        $icono_contacto = "<i class='fa fa-envelope' data-toggle='tooltip' data-placement='auto right' title='Vía correo.' style='cursor: help; color:#dd5f1a; font-size:20px;'></i>";
                      }
                      else                         
                        $icono_contacto = "<i class='fa fa-tasks' data-toggle='tooltip' data-placement='auto right' title='Manual.' style='cursor: help; color:#337ab7; font-size:20px;'></i>";


                      echo '<tr >

                      <td id="td_'.$datos_minuta[$i]['id'].'" style="cursor: pointer" onclick="listarMinutas('.$datos_minuta[$i]['id'].'); ejesel('.$datos_minuta[$i]['id'].')">
                        <i class="fa fa-user-o pull-left"></i>'.$datos_minuta[$i]['asunto'].'
                      </td>
                      <td>
                        <i fa fa-user-o class="pull-center"></i>'.$icono_contacto.'
                      </td>
                      <td>
                        <i class="fa fa-user-o"></i>'.$datos_minuta[$i]['fecha'].'
                      </td>
                      <td>
                        <button class="btn btn-danger btn-xs" id="id_eliminar_minuta" onclick="confirmaEliminaCuenta('.$datos_minuta[$i]['id'].');"><i class="fa fa-close"></i></button>
                      </td>                      
                    </tr>';

                  } ?>
                </tbody>
              </table>  
              <div class="clearfix"></div>
            </div>

          </div>
        </div>  
      </div> 
    </div>  
  </div>
</div>


<div class="col-md-7">
  <div class="ibox float-e-marfins">

    <div class="ibox-title" style="background-color: #1ab394; border-top-left-radius: 10px; border-top-right-radius: 10px;">                    
     <h2 class="text-center" style="color:#fff; text-transform: uppercase;"><b>Minuta</h2> 
   </div>
 
   <div class="ibox-content" style="border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;">
    <!--  style="background-color: #ececec;" -->
    <div class="row">

      <form class="form-horizontal" id="form_editar_gestion" name="form_editar_gestion" >  
        <div class="col-md-12 col-xs-12" style="padding-top: 20px;">
          <div class="row">
          </div>  
        </div>  

        <div class="col-xs-12 col-md-12">
          <div style="width: 100%; padding-top: 10px;"> 
            <div style="width: 100%;">
            
              <div id="textarea_minuta_auto" name="textarea_minuta_auto"></div>
              <textarea id="textarea_minuta" rows="24" style="width: inherit; border:none;"></textarea>
            </div>

          </div>             
        </div>
      </form>  


      <!-- Formulario de Minuta-->

      <div class="modal inmodal" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content animated bounceInRight">

            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
             <i class="fa fa-file-text modal-icon"></i>
             <h4 class="modal-title">Ingrese Minuta</h4>
             <small class="font-bold">Ingrese asunto, fecha y mensaje.</small>
           </div>

           <div class="modal-body">
            <form class="form-horizontal" id="form_editar_minuta" name="form_editar_minuta" >  
              <div class="col-md-12 col-xs-12" style="padding-top: 20px;">
                <div class="row">

                  <div class="col-md-6 col-xs-6 b-r">
                    <div class="col-md-12 col-xs-12">
                      <label>
                        <h2 id="titulo_asunto" name="titulo_asunto" style="position: relative; left:-15px">Asunto</h2>
                      </label> 
                      <input type="text" class="form-control" id="asunto_minuta" name="asunto_minuta" requerido="true" mensaje="Ingrese Asunto"                   style="position: relative; left:-15px">                                      
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="col-xs-12 col-md-12">
                      <label>
                        <h2 style="position: relative; left:-15px" id="titulo_fecha_minuta" name="titulo_fecha_minuta">Fecha</h2>
                      </label>

                      <div class="form-group" id="data1" style="cursor: pointer;">
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar" onclick="fecha(1)"></i>
                          </span>
                          <input type="text" id="fecha_minuta" name="fecha_minuta" class="form-control" value="<?php echo date("d-m-Y");?>"/>
                        </div>
                      </div>
                    </div>
                  </div>  

                </div>  
              </div>  

              <div class="col-xs-12 col-md-12">
                <div class="col-md-12 col-xs-12">
                  <label>
                    <h2 id="men_minuta" 
                    name="men_minuta"
                    style="position: relative; left:-15px"
                    >Mensaje</h2>
                  </label> 
                  <textarea type="text" class="form-control" id="mensaje_minuta" name="mensaje_minuta" requerido="true" mensaje="Ingrese Asunto" 
                  style="position: relative; left:-15px"> </textarea>                                   
                </div>            
              </div>
              <div class="modal-footer">

              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
                <button type="button" value="submit" class="btn btn-primary" id="btn_guardar_minuta" name="btn_guardar_minuta">Guardar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div> 
  </div> 

































</div> 
</div>  
</div>
</div>
</div>
</div>