<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre ] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
      <div class="ibox float-e-marfins">

        <div class="ibox-title" style="background-color: #1ab394; border-top-left-radius: 10px; border-top-right-radius: 10px;">                    
         <h2 class="text-center" style="color:#fff; text-transform: uppercase;">Cuenta: <b> <?php echo $datos_cliente[0]['razon_social_empresa'] ?></h2> 
         </div>

         <div class="ibox-content" style="border-bottom-right-radius: 10px; border-bottom-left-radius: 10px;">
          <!--  style="background-color: #ececec;" -->
          <div class="row">

            <form class="form-horizontal" id="form_editar_gestion" name="form_editar_gestion" >  
              <input type="hidden" id="id_fase" value="<?php echo $datos_gestion[0]['fase'];?>">
              <input type="hidden" name="hidden_id_accion_gestion" id="hidden_id_accion_gestion" value="<?php echo $datos_gestion[0]['id_accion'];?>">

              <input type="hidden" name="idempresallamada" id="idempresallamada" value="<?php echo $datos_cliente[0]['id_empresa'];?>">


              <div class="col-md-12 col-xs-12" style="padding-top: 20px;">
                <div class="row">

                  <div class="col-md-6 col-xs-12">
                    <div class="col-md-12 col-xs-12">
                      <label><h2 id="tipo_fase" style="position: relative; left:-15px">Fase</h2></label>

                      <input type="hidden" 
                      value="<?php echo $datos_gestion[0]['fase'] ?>" 
                      id="hidden_fase"
                      name="hidden_fase">    

                      <select class="form-control" id="fase" name="fase" requerido="true" mensaje="Seleccione Fase" style="position: relative; left:-15px">
                      </select>
                    </div>
                    <div class="col-md-12 col-xs-12">
                      <label></label><h2 id="tipo_gestion" style="position: relative; left:-15px"></h2>
                      <input type="hidden" 
                      value="<?php echo $datos_gestion[0]['tipo_accion'] ?>" 
                      id="hidden_llamada_tipo_llamada"
                      name="hidden_llamada_tipo_llamada">    

                      <select class="form-control" id="llamada_tipo_llamada" name="llamada_tipo_llamada" requerido="true" mensaje="Seleccione Tipo Llamada" style="position: relative; left:-15px">
                      </select>
                    </div>
                    <div class="col-md-12 col-xs-12">
                      <label><h2 style="position: relative; left:-15px">Modalidad</h2></label>
                      <input type="hidden" 
                      value="<?php echo $datos_gestion[0]['id_modalidad'] ?>" 
                      id="hidden_id_modalidad"
                      name="hidden_id_modalidad">    

                      <select class="form-control" id="id_modalidad" name="id_modalidad" requerido="true" mensaje="Seleccione Modalidad" style="position: relative; left:-15px">
                      </select>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="col-xs-12 col-md-12">
                      <label><h2 style="position: relative; left:-15px;" id="titulo_fecha" name="titulo_fecha"></h2></label>
                      <div class="form-group" id="data1" style="cursor: pointer;">
                        <div class="input-group date">

                          <input type="hidden" 
                          value="<?php echo $datos_gestion[0]['fecha_accion'] ?>"
                          id="hidden_llamada_fecha_llamada">
                          <span class="input-group-addon"><i class="fa fa-calendar" onclick="fecha(1)"></i></span>
                          <input type="text" id="llamada_fecha_llamada" name="llamada_fecha_llamada" class="form-control" value="<?php echo date("d-m-Y");?>"/>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12" id="subtipo_gestion" name="subtipo_gestion">
                      <label><h2 id="titulo_subtipo_correo_reunion" name="titulo_subtipo_correo_reunion" style="position: relative; left:-15px"></h2></label>

                      <input type="hidden" 
                      value="<?php echo $datos_gestion[0]['subtipo_accion'] ?>" 
                      id="hidden_subtipo_correo_reunion"
                      name="hidden_subtipo_correo_reunion">

                      <select class="form-control" id="subtipo_correo_reunion" name="subtipo_correo_reunion" style="position: relative; left:-15px">
                      </select>
                    </div>
                    <div class="col-md-12 col-xs-12">
                      <label><h2 style="position: relative; left:-15px">Producto</h2></label>
                      <input type="hidden" 
                      value="<?php echo $datos_gestion[0]['id_producto'] ?>" 
                      id="hidden_id_producto"
                      name="hidden_id_producto">    

                      <select class="form-control" id="id_producto" name="id_producto" requerido="true" mensaje="Seleccione Producto" style="position: relative; left:-15px">
                      </select>
                    </div>
                  </div>  
                </div>  
              </div>  

              <div class="col-xs-12 col-md-12">
                <div style="width: 100%; padding-top: 20px;"> 
                  <h2 class="text-center">Contacto</h2>

                  <input type="hidden" 
                  value="<?php echo $datos_gestion[0]['contacto_origen'] ?>" 
                  id="hidden_llamada_contacto_origen_cbx">

                  <select id="llamada_contacto_origen_cbx" name="llamada_contacto_origen_cbx" class="form-control" requerido="true" mensaje="Seleccione Contacto">
                  </select>
                </div>             
              </div> 

              <div class="col-xs-12 col-md-12" style="margin-bottom: 20px; padding-top: 20px;">
                <h2 class="text-center">Asunto</h2>
                <input type="hidden" 
                value="<?php echo $datos_gestion[0]['asunto_accion'] ?>" 
                id="hidden_llamada_asunto_box">

                <textarea name="llamada_asunto_box" id="llamada_asunto_box" cols="75" rows="2" style="width: 100%;" requerido="true" mensaje="Debe ingresar Asunto"></textarea>

                <h2 class="text-center" style="padding-top: 20px;">Comentario</h2>
                <input type="hidden" 
                value="<?php echo $datos_gestion[0]['comentario_accion'] ?>" 
                id="hidden_llamada_comentario_box">

                <textarea name="llamada_comentario_box" id="llamada_comentario_box" cols="75" rows="2" style="width: 100%;" requerido="true" mensaje="Ingrese comentario"></textarea>   
              </div>

              <div class="col-md-12">                                  

                <div class="col-xs-12 col-md-6" style="padding-bottom:20px;">
                  <label><h2 style="position: relative; left:-15px">Compromiso</h2></label>

                  <input type="hidden" 
                  value="<?php echo $datos_gestion[0]['compromiso_accion'] ?>"
                  id="hidden_llamada_compromiso_cbx"
                  style="position: relative; left:-15px">

                  <select class="form-control" id="llamada_compromiso_cbx" name="llamada_compromiso_cbx" requerido="true" mensaje="Seleccione Compromiso" style="position: relative; left:-15px"> 
                  </select>                       
                </div>

                <div class="col-xs-12 col-md-6">
                  <label><h2>Fecha Compromiso </h2></label>
                  <div class="form-group" id="data1" style="cursor: pointer;">
                    <div class="input-group date">

                      <input type="hidden"                                             
                      value="<?php echo $datos_gestion[0]['fecha_compromiso_accion'] ?>" 
                      id="hidden_llamada_fecha_compromiso">

                      <span class="input-group-addon"><i class="fa fa-calendar" onclick="fecha(1)"></i></span>
                      <input type="text" class="form-control" id="llamada_fecha_compromiso" name="llamada_fecha_compromiso" value="<?php echo date("d-m-Y");?>" />
                    </div>
                  </div>                              
                </div>    

                <div class="col-xs-12 col-md-12" id="div_otros" >
                  <label><h2>Comentario Compromiso</h2></label>
                  <div class="form-group">
                   <input type="hidden"                                             
                   value="<?php echo $datos_gestion[0]['comentario_otros'] ?>" 
                   id="hidden_comentario_otros">
                   <textarea name="comentario_otros" id="comentario_otros" rows="3" style="width: 100%;" requerido="true" mensaje="Debe ingresar comentario compromiso"><?php echo $datos_gestion[0]['comentario_otros'];?></textarea>
                 </div>
               </div>
               <br>
             </div>




             <input type="hidden" name="hidden_id_gestion_ori" id="hidden_id_gestion_ori" value="<?php echo $id_gestion_ori;?>">
           </form>                           
           <div class="modal-footer">
            <button type="submit" value="submit" class="btn btn-primary" id="btn-llamada">Guardar</button>
          </div>
          <!--LLAMADA-->

        </div> 

      </div>  
    </div>
  </div>
</div>
</div>