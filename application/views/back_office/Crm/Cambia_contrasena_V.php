<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>


<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Cambio Contraseña</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-sm-6">
                                <h1>Outlook</h1>    
                                <p class="text-center">
                                    <i class="fa fa-envelope big-icon"></i>
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <h3 class="m-t-none m-b">Contraseña Nueva</h3>
                                <p>Ingrese nueva contraseña.</p>
                                <form role="form">
                                    <div class="form-group"><label>Email</label> <input type="email" placeholder="Ingrese Email" class="form-control"></div>
                                    <div class="form-group"><label>Contraseña Antigua</label> <input type="text" placeholder="Contraseña Antigua" class="form-control"></div>
                                    <div class="form-group"><label>Contraseña Nueva (*)</label> <input required="" type="password" placeholder="Contraseña Nueva" class="form-control"></div>
                                    <div>
                                        <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Confirmar</strong></button>
                                    </div>
                                </form>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
                
            
                
            
        

        </div>
</div>