<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight"> 
                <div class="row">
                  <div class="col-md-5">
                      <div class="ibox float-e-margins">
					<div class="ibox-content m-b-sm border-bottom" style="height:85px">
                          <div class="row">
                              <div class="col-md-4">
                                  <h4 style="margin-top:10px">Cuenta</h4>
                              </div>  
                              <div class="col-md-4">
                                  <div class="form-group">
								  <input type="text" id="filtro_cuenta_nombre" name="filtro_cuenta_nombre" value="" placeholder="Búsqueda" class="form-control">								  
                                  </div>
                              </div>
							  <a href="nueva_cuenta">
							  <button type="button" class="btn btn-w-m btn-primary">Nuevo Cliente</button>
							    </a>   
                          </div>
                      </div>
                          <div class="ibox-content">
						   <table id="tabla_cuenta" class="footable table table-stripped toggle-arrow-tiny" data-page-size="20" data-filter="#filtro_cuenta_nombre">
                                <thead>
                                <tr class="fila_historial">
                                    <th>Cliente</th>
                                    <th>Status Gestión</th>
                                    <th>Estado Cuenta</th>
                                    <th>Acción</th>
                                </tr>
                                </thead>
								
								<!-- id="div_general" name="div_general"--> 
                                <tbody id="div_general" name="div_general">
                               
                            	<?php								
									for($i=0;$i<count($cliente_crm);$i++){
						 							
										$texto_adicional 	=	"";
										$icono_adicional 	= 	"";
										$icono_contacto = "";
										$title = "";										
										$id = $cliente_crm[$i]['ID'];
										$tiene_contacto = $cliente_crm[$i]['tiene_contacto'];
										$tiene_gestion = $cliente_crm[$i]['tiene_gestion'];
										switch ($cliente_crm[$i]['estado_empresa']) {
											case 4:
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Potencial";
													$title = "<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo </b>";
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
													
													if($tiene_contacto == 1){
													$texto_adicional 	= "Sugerido por Sistema";	
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li><li><a href='cliente_potencial/index/$id'>Nueva Gestión</a></li><li id='deleteRegistro' onclick=confirmaEliminaCuenta($id)><a href='#'>Eliminar</a></li>";												
													}
													else
													{
													$texto_adicional 	= ", Sugerido por Sistema";
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li> <li class='disabled'><a href='#'>Nueva Gestión</a></li> <li id='deleteRegistro' onclick=confirmaEliminaCuenta($id)><a href='#'>Eliminar</a></li>";
													}
												break;
											case 6:
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Activo";
													/*$texto_adicional 	= ", sin gestiones vigentes (ultimos [x] dias)";*/
													$title = "<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo </b>";
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
													
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li><li><a href='#'>Eliminar</a></li><li><a href='detalle_cuenta/index/$1'>Ver Detalle</a></li>";
												break;
											case 3:
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Antiguo";
													$texto_adicional 	= ", sin gestiones vigentes (ultimos [x] dias)";
													$title = "<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo </b>$texto_adicional";
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
													
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li><li><a href='#'>Eliminar</a></li><li><a href='detalle_cuenta/index/$1'>Ver Detalle</a></li>";
												break;											
											case 1:	
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Potencial";
													/*$texto_adicional 	= ", Con contacto.";*/												
													
													 
													
													/*ICONO SUGERIDO POR SISTEMA
													<i data-toggle=\"tooltip\" data-placement=\"auto right\" data-html=\"true\" title=\"<i class='fa fa-circle' style='color: #1ab394; font-size: 15px;'></i> <i class='fa fa-envelope' style='color: #ed5565; font-size: 15px;'></i>   
													Cliente <b style='color:#1BB394;'>Nuevo</b>, Con contacto.\" class=\"fa fa-envelope-o\" style=\"color: #ed5565; font-size: 15px; padding-left: 20px;cursor:help;\"></i>"
													
													TOOLTIP STATUS GESTION
													<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i> <i class=\"fa fa-user\" style=\"color: #ed5565; font-size: 15px;\"></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo  </b>$texto_adicional
													
													<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i> <i class=\"fa fa-user\" style=\"color: #ed5565; font-size: 15px;\"></i>   Cliente <b style=\"color: $color_icon_titulo;\">asdf</b>asdf
													*/
													
													
													
													/*$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li><li><a href='#'>Eliminar</a></li><li><a href='cliente_potencial/index/$id'>Editor de Acción</a></li><li><a href='detalle_cuenta/index/$1'>Ver Detalle</a></li>";*/
													
													if($tiene_contacto == 1){
													$icono_contacto = "<i class='fa fa-user' data-toggle='tooltip' data-placement='auto right' title='Cliente con contacto.' style='cursor: help; color:#337ab7; font-size:20px;'></i>";	
													$texto_adicional 	= "Sugerido por Sistema";	
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li><li><a href='cliente_potencial/index/$id'>Nueva Gestión</a></li><li id='deleteRegistro' onclick=confirmaEliminaCuenta($id)><a href='#'>Eliminar</a></li>";												
														
														if($tiene_gestion == 1){
															$title = "Con gestión.";
															$color_icon_titulo 	= "#1ab394";
														}
														else{
															$title = "Sin gestión.";
														}
													}
													else
													{														
													$texto_adicional 	= ", Sugerido por Sistema";
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li> <li class='disabled'><a href='#'>Nueva Gestión</a></li> <li id='deleteRegistro' onclick=confirmaEliminaCuenta($id)><a href='#'>Eliminar</a></li>";
													}
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
												break;
												
												case 2:	
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Activo";
													/*$texto_adicional 	= ", Con contacto.";*/												
													
													 
													
													/*ICONO SUGERIDO POR SISTEMA
													<i data-toggle=\"tooltip\" data-placement=\"auto right\" data-html=\"true\" title=\"<i class='fa fa-circle' style='color: #1ab394; font-size: 15px;'></i> <i class='fa fa-envelope' style='color: #ed5565; font-size: 15px;'></i>   
													Cliente <b style='color:#1BB394;'>Nuevo</b>, Con contacto.\" class=\"fa fa-envelope-o\" style=\"color: #ed5565; font-size: 15px; padding-left: 20px;cursor:help;\"></i>"
													
													TOOLTIP STATUS GESTION
													<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i> <i class=\"fa fa-user\" style=\"color: #ed5565; font-size: 15px;\"></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo  </b>$texto_adicional
													
													<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i> <i class=\"fa fa-user\" style=\"color: #ed5565; font-size: 15px;\"></i>   Cliente <b style=\"color: $color_icon_titulo;\">asdf</b>asdf
													*/
													
													
													
													/*$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li><li><a href='#'>Eliminar</a></li><li><a href='cliente_potencial/index/$id'>Editor de Acción</a></li><li><a href='detalle_cuenta/index/$1'>Ver Detalle</a></li>";*/
													
													if($tiene_contacto == 1){
													$icono_contacto = "<i class='fa fa-book' data-toggle='tooltip' data-placement='auto right' title='Cliente con cotización.' style='cursor: help; color:#337ab7; font-size:20px;'></i>";	
													$texto_adicional 	= "Sugerido por Sistema";	
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li><li><a href='cliente_potencial/index/$id'>Nueva Gestión</a></li><li id='deleteRegistro' onclick=confirmaEliminaCuenta($id)><a href='#'>Eliminar</a></li>";												
														
														if($tiene_gestion == 1){
															$title = "Con cotización.";
															$color_icon_titulo 	= "#1ab394";
														}
													}
													else
													{	
													$title = "Sin cotización.";
													$texto_adicional 	= ", Sugerido por Sistema";
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Ver / Editar</a></li> <li class='disabled'><a href='#'>Nueva Gestión</a></li> <li id='deleteRegistro' onclick=confirmaEliminaCuenta($id)><a href='#'>Eliminar</a></li>";
													}
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
												break;
										}
										
										
										echo "<tr id='tr_".$id."'>
												<td>".$cliente_crm[$i]['nombre_cliente']."</td>
												<td>												
													$icono_base
													$icono_adicional
												</td>
												<td>".$texto_titulo." &nbsp ".$icono_contacto."</td>
												<td>
												   <div class='btn-group'>
														<a data-toggle='dropdown' class='dropdown-toggle' href='#'>
														<i class='fa fa-wrench'></i>
														</a>
														<ul class='dropdown-menu pull-right'>
															$opciones_accion
														</ul>
												   </div>
												  </td>
											</tr>";	
									}
									
	function getAccion($accion) {
        // carga el combo box Holding
		switch ($accion){
			case 1:
					$resultado="LLamada";
					break;
			case 2:
					$resultado="Correo";
					break;
			case 3:
					$resultado="Reunión";
					break;											
			case 4:
					$resultado="Cotización";
					break;			
			case 5:
					$resultado="Sin Gestión";
					break;			
			case 6:
					$resultado="Agenda";
					break;
		}
        
        return $resultado;
		
    }		
									
									
									
								?>						
                              
							  
                                </tbody>
								 <tfoot>
                                        <tr>
                                            <td colspan="6">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>
                            </table>     

							
                          </div>   
                      </div>
                  </div>  
        
                  <div class="col-md-7">
                      <div class="ibox-content m-b-sm border-bottom">
                          <div class="row">
                              <div class="col-md-4">
                                  <h4>Historial de Gestiones</h4>
                              </div>  
                              <div class="col-md-4">
                                  <div class="form-group">
                                    <input type="text" id="empresa_nombre" name="empresa_nombre" value="" placeholder="Filtrar Cliente" class="form-control">
                                  </div>
                              </div>
                          </div>
                      </div>
				  
					  
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="ibox">
                                  <div class="ibox-content">

                                    <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="5" data-filter="#empresa_nombre">
                                        <thead>
                                        <tr class="fila_historial">

                                            <th data-toggle="true">Cliente <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></th>
                                            <th data-hide="phone">Evento <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></th>
                                            <th data-hide="tablet">Asunto</th>
                                            <th data-hide="phone">Fecha <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></th>
                                            <th data-hide="tablet">Comentario</th>
											<th data-hide="tablet">Compromiso</th>
                                            <th data-hide="phone">Status <i class="fa fa-sort-desc" style="color: #676a6c; font-size: 15px;"></i></th>											
                                        </tr>
                                        </thead>
                                        <tbody>
										
									<?php		
										for($i=0;$i<count($HistorialGestion_crm);$i++){
										$icono_base="";
										$id = $HistorialGestion_crm[$i]['ID'];
										$idaccion = $HistorialGestion_crm[$i]['id_accion'];
										switch ($HistorialGestion_crm[$i]['estado_evento']) {
											case 1:
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Nuevo";
													$title = "<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo </b>";
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
													
													$opciones_accion ="<li><a href='editar_gestion/index/$id.$idaccion'>Editar Gestión</a></li><li><a href='cliente_potencial/index/$id'>Nueva Gestión</a></li><li id='deleteRegistro' onclick=confirmaEliminaGestion($idaccion)><a href='#'>Eliminar</a></li>";												
												break;
											case 2:
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Potencial";
													$title = "<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo </b>";
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
													
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Editar Cuenta</a></li><li><a href='detalle_cuenta/index/$1'>Ver Detalle</a></li><li id='deleteRegistro' onclick='confirmaEliminaGestion($idaccion)'><a href='#'>Eliminar</a></li>";
												break;
											case 3:
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Antiguo";
													$texto_adicional 	= ", sin gestiones vigentes (ultimos [x] dias)";
													$title = "<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo </b>$texto_adicional";
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i>";
													
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Editar Cuenta</a></li><li><a href='detalle_cuenta/index/$1'>Ver Detalle</a></li><li id='deleteRegistro' onclick=confirmaEliminaGestion($idaccion)><a href='#'>Eliminar</a></li>";
												break;											
											case 4:	
													$color_icon_titulo 	= "#656767";
													$texto_titulo 		= "Nuevo";
													$texto_adicional 	= ", Con Contacto";
													$title = "<i class=\"fa fa-circle\" style=\"color: $color_icon_titulo;font-size: 15px; \" ></i> <i class=\"fa fa-envelope\" style=\"color: #ed5565; font-size: 15px;\"></i>   Cliente <b style=\"color: $color_icon_titulo;\">$texto_titulo </b>$texto_adicional";
													$icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='$title' class='fa fa-circle' style='color: $color_icon_titulo; font-size: 15px;cursor:help;'></i> 
													<i data-toggle=\"tooltip\" data-placement=\"auto right\" data-html=\"true\" title=\"<i class='fa fa-circle' style='color: #1ab394; font-size: 15px;'></i> <i class='fa fa-envelope' style='color: #ed5565; font-size: 15px;'></i>   
													Cliente <b style='color:#1BB394;'>Nuevo</b>, Con Contacto\" class=\"fa fa-envelope-o\" style=\"color: #ed5565; font-size: 15px; padding-left: 20px;cursor:help;\"></i>";
													
													$opciones_accion ="<li><a href='editar_cuenta/index/$id'>Editar Cuenta</a></li><li><a href='#'>Editar</a></li><li><a href='cliente_potencial/index/$id'>Editor de Acción</a></li><li><a href='detalle_cuenta/index/$1'>Ver Detalle</a></li><li id='deleteRegistro' onclick=confirmaEliminaGestion($idaccion)><a href='#'>Eliminar</a></li>";
												break;
										}                                    
								echo "			
									 <tr class='fila_historial' id='tr_".$idaccion."'>
                                            <td>".$HistorialGestion_crm[$i]['razon_social_empresa']."</td>
                                            <td>".getAccion($HistorialGestion_crm[$i]['id_descripcion_evento'])."</td>
                                            <td>".$HistorialGestion_crm[$i]['asunto_accion']."<br><br></td>
                                            <td>".$HistorialGestion_crm[$i]['fecha_evento']."</td>
                                            <td>".$HistorialGestion_crm[$i]['comentario_accion']."<br><br></td>
											<td>".$HistorialGestion_crm[$i]['fecha_compromiso_accion']."  (".$HistorialGestion_crm[$i]['DESC_COMPROMISO'].")"."<br><br></td>											
                                            <td>
                                                $icono_base
                                            </td>
                                            <td>
                                             	<div class='btn-group'>
														<a data-toggle='dropdown' class='dropdown-toggle' href='#'>
														<i class='fa fa-wrench'></i>
														</a>
													<ul class='dropdown-menu pull-right'>
														$opciones_accion
													</ul>
												 </div>   
                                            </td>
                                        </tr>
  ";     
										}  
?>	

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="6">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                      
                                  </div>
                              </div>  
                          </div>
</div>
			   
                  </div>  
          

		  </div>

                           

</div>

