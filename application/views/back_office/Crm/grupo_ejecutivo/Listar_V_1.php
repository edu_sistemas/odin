<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-12 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-2016 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Grupos de Ejecutivos</h5>
                    <div class="ibox-tools">

                        <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo</button>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="tbl_grupo_ejecutivo" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre Grupo</th>
                                    <th>Descripción Grupo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for ($i = 0; $i < count($contenido); $i++) { ?>
                                    <tr class="gradeX">
                                        <td><?php echo $contenido[$i]['id']; ?></td>
                                        <td><?php echo $contenido[$i]['nombre_grupo']; ?></td>
                                        <td><?php echo $contenido[$i]['descripcion_grupo']; ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">Acción<span class="caret"></span></button>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="<?php echo base_url() . 'back_office/Crm/grupo_ejecutivo/Editar/index/' . $contenido[$i]['id']; ?>"  >Editar</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre Grupo</th>
                                    <th>Descripción Grupo</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
