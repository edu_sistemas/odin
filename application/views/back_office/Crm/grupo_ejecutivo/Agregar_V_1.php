<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Grupo Ejecutivo</h5>
                <div class="ibox-tools">

                </div>
            </div>

            <div class="ibox-content">


                <form class="form-horizontal" id="frm_grupo_registro"  >
                    <p> Agregar Grupo Ejecutivo </p>
                    <div class="form-group"><label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-6">

                            <input type="text" placeholder="Nombre" requerido="true" mensaje="Debe ingresar nombre del Grupo" name="nombre" id="nombre" class="form-control">

                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Descripción:</label>

                        <div class="col-lg-6">
                            <textarea class="form-control" requerido="true" mensaje="Debe ingresar una descripción sobre el Grupo" id="descripcion" name="descripcion" maxlength="100" placeholder="Descripción"></textarea>

                            <p id="text-out">0/200 caracteres restantes</p>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Responsable Grupo</label>
                        <div class="col-lg-6">
                            <select id="responsable" name="responsable"  class="select2_demo_3 form-control" requerido="true" mensaje="Seleccione Usuario Responsable del Grupo" >

                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" name="btn_reset" id="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
