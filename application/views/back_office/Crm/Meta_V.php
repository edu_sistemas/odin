<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-02 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cargar Archivo</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content"> 

                    <form  name="frm_carga_meta" id="frm_carga_meta">

                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">

                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Seleccionar Archivo</span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input type="file" name="file_carga_meta" id="file_carga_meta" accept=".xls,.xlsx">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">
                                Eliminar
                            </a>
                        </div>  


                        <button id="btn_carga"  type="button" class="ladda-button btn btn-primary" data-style="expand-right"> 

                            <i class="fa fa-upload"></i> Subir  

                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
