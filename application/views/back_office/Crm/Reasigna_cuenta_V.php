<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 */
?>

<div class="wrapper wrapper-content">
	<div class="row">
		<div class="col-lg-3">
			<div class="ibox float-e-margins" id="div_general" name="div_general">
				<div>
					<div class="file-manager">                            
						<div class="space-30"></div>
						<h2>Ejecutivos Origen</h2>
						<div style="padding-left: 8px; padding-right: 20px;">
							<table style="width: 100%">
								<tr>
									<td style="text-align: left; width: 45%">
										<i></i><b>Ejecutivo</b>
									</td>
									<td style="text-align: center; width: 40%">
										<i></i><b>Empresas</b> 
									</td>
									<td style="text-align: right; width: 15%">
										<i></i><b>Gestiones</b> 
									</td>
								</tr>
							</table>
						</div>
						<div style="height	:570px; overflow: auto; overflow-x: hidden; border-bottom: 20px solid #F4F6FA;">
							<table id="tbl_minuta" class="table table-hover" style="width: 100%;">
							<!--
								<thead>
									<tr>
										<th style="text-align: left;">Ejecutivo</th>
										<th style="text-align: center;">Empresas</th>
										<th style="text-align: right;">Gestiones</th>
									</tr>
								</thead>
							-->

							<tbody>
								<?php
								for ($i=0;$i<count($datos_ejecutivos_ori);$i++)
								{


									if($datos_ejecutivos_ori[$i]['id_accion'] > 0){
										$icon_gestion = "<button class='btn btn-primary btn-xs' id='id_listar_gestion' onclick='popupGestiones(".$datos_ejecutivos_ori[$i]['id_ejecutivo'].");'><i class='fa fa-file-text'></i></button>";												
									}
									else
										$icon_gestion = "";								

									echo '<tr style="cursor: pointer" id="tr_'.$datos_ejecutivos_ori[$i]['id_ejecutivo'].'">

									<td style="width: 45%;" onclick="listarEmpresasporEjecutivo('.$datos_ejecutivos_ori[$i]['id_ejecutivo'].'); ejesel('.$datos_ejecutivos_ori[$i]['id_ejecutivo'].')">'.$datos_ejecutivos_ori[$i]['ejecutivo'].'
									</td>
									<td style="text-align: center; width: 40%;">'.$datos_ejecutivos_ori[$i]['Q_empresas'].'</td>
									<td style="text-align: right; width: 15%;">'.$icon_gestion.'
									</td>
								</tr>';
							}

							?>
						</tbody>
					</table>
				</div>                           
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<div class="col-lg-9 animated">
	<div class="ibox-content" >
		<form id="form" class="wizard-big">
			<div class="row" style="height:100%">
				<div class="col-lg-5">
					<h2 style="padding-bottom: 40px">Cuentas de Ejecutivos Origen</h2>
					<div class="form-group">
						<div class="search-form" id="form_search2" name="form_search2">
							<div class="form-group has-feedback">
								<label for="search" class="sr-only">Buscar</label>
								<input type="text" class="form-control" onkeyup="limpiar(this.id,event)" name="search2"  id="search2" placeholder="Buscar">
								<span class="glyphicon glyphicon-search form-control-feedback"></span>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<select id="cuentasorigen_visible"  name="cuentasorigen_visible[]" class="form-control" multiple="multiple" style="height:355px; width:100%">   </select>
									<div id="PermiHidnn2" style="display:none">
										<select id="cuentasorigen" name="cuentasorigen" class="form-control" multiple="multiple"></select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="form-group">
						<label class="col-md-4 control-label" for="btn_derecha"></label>
						<div class="derizq" style="margin-top: 100px; margin-bottom: 100px;">
							
							<div class="col-lg-4 col-xs-12" style="padding:0px ; margin: 0px auto"">
									<h4>Historial</h4>
									<table style="display: none" id="reasignar_bitacora">
										<thead>
											<tr>
												<th>Empresa</th>
												<th>Rut Empresa</th>
												<th>Ejecutivo Destino</th>
												<th>Fecha Inicio</th>
												<th>Fecha Termino</th>
												<th>Venta</th>
												<th>Ejecutivo Cambio</th>
												<th>Fecha Cambio</th>
											</tr>
										</thead>
										<tfoot>
											<tr>
												<th>Empresa</th>
												<th>Rut Empresa</th>
												<th>Ejecutivo Destino</th>
												<th>Fecha Inicio</th>
												<th>Fecha Termino</th>
												<th>Venta</th>
												<th>Ejecutivo Cambio</th>
												<th>Fecha Cambio</th>
											</tr>
										</tfoot>
									</table>
							</div>

							<button id="btn_derecha" type="button" name="btn_derecha" style="height:100%; width:100%; margin-top:30px;" class="btn btn-primary glyphicon glyphicon-chevron-right" data-toggle="tooltip"  title="seleccione empresa(s) de la lista izquierda para otorgar"></button>
							<button id="btn_izquierda" type="button" name="btn_izquierda" style="height:100%; width:100%" class="btn btn-primary glyphicon glyphicon-chevron-left" data-toggle="tooltip" title="seleccione empresa(s) de la lista derecha para quitar"></button>
							<div class="text-center" style="margin-top: 100px;">
								<button type="button" onclick="reasignarcuentas();" class="btn btn-primary" id="btnReasignarCuenta" location.reload();>Reasignar</button>
							</div>
						</div>
					</div>
				</div>
				<!-- Select Multiple -->
				<div class="col-lg-5" style="height:100%">
					<div class="form-group">
						<h2>Ejecutivo Destino</h2>						
						<select class="fadeInRight" style="width: 91%" id="ejecutivodestino" name="ejecutivodestino"></select>										
						<div  class="search-form" id="form_search1" name="form_search1">
							<div class="form-group has-feedback">
								<label for="search" class="sr-only">Buscar</label>
								<input type="text" class="form-control" onkeyup="limpiar(this.id,event)" name="search1" id="search1" placeholder="Buscar">
								<span class="glyphicon glyphicon-search form-control-feedback"></span>
							</div>
						</div>
						<div class="row">						
							<div class="col-md-12">
								<div class="form-group" style="margin-bottom: 100px">
									<select id="cuentasdestino_visible" name="cuentasdestino_visible[]" class="form-control" multiple="multiple" style="height:355px; width:100%"></select>
									<div id="PermiHidnn" style="display:none">
										<select id="cuentasdestino" name="cuentasdestino" class="form-control"  multiple="multiple"></select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>

<div class="modal inmodal" id="myModalReasignacuenta" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content animated bounceInRight">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<i class="fa fa-file-text modal-icon"></i>
				<h4 class="modal-title">Ultimas Gestiones</h4>
				<h3 id="nameejec" class="font-bold"></h3>
				
			</div>

			<div class="modal-body">
				<form class="form-horizontal" id="form_gestiones" name="form_gestiones" >  

					<div class="row">
						<div class="col-md-12">
							<div class="ibox-content table-responsive" style="height:200px; overflow: auto;border-bottom: 20px solid #fff;">       
								<table id="tbl_gestion" class="table table-striped" style="width: 100%">
									<thead>
										<tr>
											<th>Empresa</th>
											<th>Acción</th>
											<th>Fecha Evento</th>
										</tr>
									</thead>
									<tbody>

									</tbody>
								</table>  
								<div class="clearfix"></div>
							</div>

						</div>
					</div>  

					<div class="modal-footer">
						<button type="button" class="btn btn-white" data-dismiss="modal"> Cerrar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
</div>				