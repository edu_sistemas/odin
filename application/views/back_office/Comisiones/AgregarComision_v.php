<?php
/**
 * Agregar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nueva Comisión</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_ingreso_comision"  >



                    <div class="form-group">
                        <label class="col-lg-2 control-label">Usuario:</label>
                        <div class="col-lg-7">
                            <select id="usuario" name="usuario"  class="select2_demo_3 form-control">
                                <option></option>
                            </select>

                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="col-lg-2 control-label">%  Curso:</label>
                        <div class="col-lg-3">
                            <input type="number" placeholder="Ejemplo: 0.05" class="form-control required" id="porcent_comision_curso" 
                                   requerido="true" mensaje ="Debe Ingresar comision por cursos" name="porcent_comision_curso" step=".0001" min="0" max="100"
                                   />

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">%  Arriendo:</label>
                        <div class="col-lg-3">
                            <input type="number" placeholder="Ejemplo: 0.04" class="form-control required" id="porcent_comision_arriendo" 
                                   requerido="true" mensaje ="Debe Ingresar comision por Arriendo" name="porcent_comision_arriendo" step=".0001" min="0" max="100"
                                   />

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">%  Jefatura:</label>
                        <div class="col-lg-3">
                            <input type="number" placeholder="Ejemplo: 0.01" class="form-control required" id="porcent_comision_jefatura" 
                                   requerido="true" mensaje ="Debe Ingresar comision por Jefatura" name="porcent_comision_jefatura" step=".0001" min="0" max="100"
                                   />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">%  Especial (Nicole Complemento):</label>
                        <div class="col-lg-3">
                            <input type="number" placeholder="Ejemplo: 0.01" class="form-control required" id="porcent_comision_especial" 
                                   requerido="true" mensaje ="Debe Ingresar comision por Jefatura" name="porcent_comision_especial" step=".0001" min="0" max="100"
                                   />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Válida Apartir de:</label>
                        
                            <div class="col-lg-3" id="fecha_1"  >
                                <div class="input-group date" style="width: 100%">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input name="comision_valida_apartir_de" id="comision_valida_apartir_de" 
                                           requerido="true" mensaje ="Debe Ingresar Fecha valida apartir de "
                                           type="text" class="form-control" value="">
                                </div>
                            </div>

                        
                    </div>



                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
