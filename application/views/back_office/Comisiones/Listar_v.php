<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 * * Ultima edicion: 2018-01-04 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>


    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Criterios</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">



                            <form role="form" style="text-align:center;" id="frm" name="frm">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <b class="help-block">Tipo</b>
                                        <select id="cbx_tipo_usuario" name="cbx_tipo_usuario" class="select2_demo_3 form-control" style="width: 100%;">
                                <option></option>
                            </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <b class="help-block">Ejecutivo</b>
                                        <select id="cbx_ejecutivo" name="cbx_ejecutivo" class="select2_demo_3 form-control" style="width: 100%;">
                                <option></option>
                            </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <b class="help-block">Mes</b>
                                        <input id="calendario_mes" name="calendario_mes" type="text" class="form-control input-md" readonly="" value="<?php echo date('m') . '-' . date('Y'); ?>" style="width: 100%;">
                                    </div>
                                </div>
                                <br>

                                <!-- Button -->
                                <div class="row">
                                    <div class="col-lg-12">
                                        <button type="button" id="btn_buscar_comision" name="btn_buscar_comision" class="btn btn-success pull-left col-lg-2">
                                Buscar
                            </button>
                            <div class="col-lg-4" id="cargando" style="text-align: left;">
                            </div>
                                    </div>
                                    
                                </div>
                                <br><br>
                            </form>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Comisiones</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_comision" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>Ejecutivo</th>
                                    <th>Total Ventas</th>
                                    <th>Total Eliminados</th>
                                    <th>Total Participantes</th>
                                    <th>Total Descuentos</th>
                                    <th>Total Rectificaciones</th>
                                    <th>% Comision</th>
                                    <th>Comision Neta</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Ejecutivo</th>
                                    <th>Total Ventas</th>
                                    <th>Total Eliminados</th>
                                    <th>Total Participantes</th>
                                    <th>Total Descuentos</th>
                                    <th>Total Rectificaciones</th>
                                    <th>% Comision</th>
                                    <th>Comision Neta</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Detalle Comisión</h5>
                    <div class="ibox-tools">

                        <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <button id="btn_get_informe" class="btn btn-success">Exportar a PDF <i class="fa fa-pdf"></i></button>
                    <h3 id="tit_historico_comisiones"></h3>
                    <div class="table-responsive">


                        <table id="tbl_detalle_comision" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>Ficha</th>
                                    <th>Ejecutivo</th>
                                    <th>Tipo</th>
                                    <th>Empresa</th>
                                    <th>Fecha Venta</th>
                                    <th>Valor Eliminados</th>
                                    <th>Valor Part.</th>
                                    <th>Cant. Participantes</th>
                                    <th>Total Fichas Vigente</th>
                                    <th>Tipo Producto</th>
                                    <th>Descuento Unitario</th>
                                    <th>Total Descuentos</th>
                                    <th>Venta Neta</th>
                                    <th>%Comision</th>
                                    <th>Comision Neta</th>

                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Ficha</th>
                                    <th>Ejecutivo</th>
                                    <th>Tipo</th>
                                    <th>Empresa</th>
                                    <th>Fecha Venta</th>
                                    <th>Valor Eliminados</th>
                                    <th>Valor Part.</th>
                                    <th>Cant. Participantes</th>
                                    <th>Total Fichas Vigente</th>
                                    <th>Tipo Producto</th>
                                    <th>Descuento Unitario</th>
                                    <th>Total Descuentos</th>
                                    <th>Venta Neta</th>
                                    <th>%Comision</th>
                                    <th>Comision Neta</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Descuento Comisión</h5>
                    <div class="ibox-tools">
    
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
    
                    </div>
                </div>
                <div class="ibox-content">
                    <button id="btn_set_descuento" class="btn btn-success">Generar Histórico<i class="fa fa-pdf"></i></button>
                   <h3 id="tit_historico_rectificaciones"></h3>
                    <div class="table-responsive">
                        <table id="tbl_descuentos_productos" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>Ejecutivo</th>
                                    <th>Ficha</th>
                                    <th>Fecha Inicio Curso</th>
                                    <th>Monto <br> Rectificación
                                        <br>
                                        <span class='rango'></span>
                                    </th>
                                    <!--th>Tipo Producto</th-->
                                    <th>Valor Actual</th>
                                    <th>Descuento</th>
                                    <th>Total Descuentos</th>
                                    <th>Venta Neta</th>
                                    <!--th>% Comisión</th-->
                                    <th>Comisión Neta</th>
                                    <th>Comisión Pagada</th>
                                    <th>Diferencia Comisión</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Ejecutivo</th>
                                    <th>Ficha</th>
                                    <th>Fecha Inicio Curso</th>
                                    <th>Monto<br> Rectificación
                                        <br>
                                        <span class='rango'></span>
                                    </th>
                                    <!--th>Tipo Producto</th-->
                                    <th>Valor Actual</th>
                                    <th>Descuento</th>
                                    <th>Total Descuentos</th>
                                    <th>Venta Neta</th>
                                    <!--th>% Comisión</th-->
                                    <th>Comisión Neta</th>
                                    <th>Comisión Pagada</th>
                                    <th>Diferencia Comisión</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>