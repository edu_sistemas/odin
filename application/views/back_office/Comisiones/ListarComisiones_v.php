<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Comisiones Usuarios</h5>
                <div class="ibox-tools">


                    <button type="button" id="btn_nueva_comision" class="btn btn-w-m btn-primary">Nueva Comision</button>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">

                    <table id="tbl_detalle_comision_usuarios" class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Tipo Usuario</th>                                 
                                <th>% Comision Cursos</th>
                                <th>% Comision Arriendos</th>
                                <th>% Comision Jefatura</th>
                                <th>% Comision  Especial Nicole</th>
                                <th>Validad Apartir de:</th>
                                <th>Fecha Termino</th>                              

                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Usuario</th>
                                <th>Tipo Usuario</th>                                 
                                <th>% Comision Cursos</th>
                                <th>% Comision Arriendos</th>
                                <th>% Comision Jefatura</th>
                                <th>% Comision  Especial Nicole</th>
                                <th>Validad Apartir de:</th>
                                <th>Fecha Termino</th>  


                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

