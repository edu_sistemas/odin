<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-22 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-22 [Marcelo Romero] <mromero@edutecno.com>
 */

?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar Módulo</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <form class="form-horizontal" id="frm_modulo_editar">
                    <p>Editar Módulo</p>
                    <div class="form-group"><label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-4">
                            <input type="hidden" placeholder="Nombre" name="id_v" class="form-control" value="<?php echo $data_modulo[0]['id']; ?>">
                            <input type="text" placeholder="Nombre" requerido="true" mensaje="Debe ingresar nombre del modulo" name="nombre_v" class="form-control" value="<?php echo $data_modulo[0]['nombre']; ?>">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Título:</label>
                        <div class="col-lg-6">
                            <input type="text" placeholder="Título" requerido="true" mensaje="Debe ingresar título del modulo" name="titulo_v" class="form-control" value="<?php echo $data_modulo[0]['titulo']; ?>">
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Descripción:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control" requerido="true" mensaje="Debe ingresar el título del módulo" id="descripcion" name="descripcion_v" maxlength="100" placeholder="Descripción"><?php echo $data_modulo[0]['descripcion']; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Icono:</label>

                        <div class="col-lg-6">
                            <input type="text" class="form-control" requerido="true" mensaje="Debe ingresar nombre del icono" id="icon_v" name="icon_v" maxlength="100" placeholder="icono" value="<?php echo $data_modulo[0]['icon']; ?>">
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_modulo" id="btn_editar_modulo">Actualizar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
