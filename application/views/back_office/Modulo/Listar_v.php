<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-11-22 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-11-22 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de módulos</h5>
                    <div class="ibox-tools">

                        <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo Módulo</button>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="tbl_modulo" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>nombre</th>
                                    <th>titulo</th>
                                    <th>descripcion</th>
                                    <th>estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>

                            <tfoot>
                                <tr>
                                    <th>Código</th>
                                    <th>nombre</th>
                                    <th>titulo</th>
                                    <th>descripcion</th>
                                    <th>estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
