<?php
/**
 * Home_v 
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-21-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:	2016-21-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Módulo</h5>
                <div class="ibox-tools">

                </div>
            </div>

            <div class="ibox-content">


                <form class="form-horizontal" id="frm_modulo_registro"  >
                    <p> Agregar Módulo </p>
                    <div class="form-group"><label class="col-lg-2 control-label">Nombre:</label>
                        <div class="col-lg-4">

                            <input type="text" placeholder="Nombre" requerido="true" mensaje="Debe ingresar nombre del nuevo módulo" name="nombre_v" class="form-control">

                        </div>
                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Titulo:</label>

                        <div class="col-lg-6">
                            <input type="text" class="form-control" requerido="true" mensaje="Debe ingresar el titulo del módulo" id="descripcion" name="titulo_v" maxlength="100" placeholder="Titulo">
                        </div>

                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Descripción:</label>

                        <div class="col-lg-6">
                            <textarea class="form-control" requerido="true" mensaje="Debe ingresar una descripción sobre el módulo" id="descripcion" name="descripcion_v" maxlength="100" placeholder="Descripción"></textarea>
                        </div>

                    </div>
                    <div class="form-group"><label class="col-lg-2 control-label">Icono:</label>

                        <div class="col-lg-6">
                            <input type="text" class="form-control" requerido="true" mensaje="Debe ingresar nombre del icono" id="descripcion" name="icon_v" maxlength="100" placeholder="icono">
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" name="btn_reset" id="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>


            </div>
        </div>
    </div>
</div>
