
<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Gestión de permisos para los perfiles</h5>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox">
                                <div class="ibox-title">
                                    <label>Perfil: <?php echo $contenido[0]['descripcion']; ?></label>
                                    <input type="hidden" name="id_perfil" id ="id_perfil" value="<?php echo $contenido[0]['id']; ?>" />
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                            <i class="fa fa-wrench"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-user">
                                            <li><a href="#">Config option 1</a>
                                            </li>
                                            <li><a href="#">Config option 2</a>
                                            </li>
                                        </ul>
                                        <a class="close-link">
                                            <i class="fa fa-times"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <form id="form"   class="wizard-big">
                                        <div class="row">
                                            <!-- Select Multiple -->
                                            <div class="col-md-4">
                                                Permisos no otorgados
                                                <form action="" class="search-form">
                                                    <div class="form-group has-feedback">
                                                        <label for="search" class="sr-only">Buscar</label>
                                                        <input type="text" class="form-control" name="search2"  id="search2" placeholder="Buscar">
                                                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                                </form>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="cbx_permisos_no_otorgados"> </label>
                                                <div class="col-md-13">

                                                    <select id="cbx_permisos_no_otorgados_visible"  name="cbx_permisos_no_otorgados_visible[]" class="form-control" multiple="multiple">   </select>

                                                    <div id="PermiHidnn2" style="display:none;">
                                                        <select id="cbx_permisos_no_otorgados" name="cbx_permisos_no_otorgados" class="form-control" multiple="multiple">
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <!-- Button -->
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="btn_derecha"></label>
                                                <div class="derizq" style="margin-top: 70px;">
                                                    <button id="btn_derecha" type="button" name="btn_derecha" class="btn btn-primary btn-lg btn-block glyphicon glyphicon-chevron-right" data-toggle="tooltip"  title="seleccione permisos de la lista izquierda para otorgar"></button>
                                                    <button id="btn_izquierda" type="button" name="btn_izquierda" class="btn btn-primary btn-lg btn-block glyphicon glyphicon-chevron-left" data-toggle="tooltip" title="seleccione permisos de la lista derecha para quitar"></button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Select Multiple -->
                                        <div class="col-md-4">
                                            Permisos Otorgados
                                            <form  action="" class="search-form">
                                                <div class="form-group has-feedback">
                                                    <label for="search" class="sr-only">Buscar</label>
                                                    <input type="text" class="form-control" name="search1" id="search1" placeholder="Buscar">
                                                    <span class="glyphicon glyphicon-search form-control-feedback"></span>
                                            </form>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-13">
                                                <select id="cbx_permisos_otorgados_visible" name="cbx_permisos_otorgados_visible[]" class="form-control" multiple="multiple">   </select>

                                                <div id="PermiHidnn" style="display:none;">
                                                    <select id="cbx_permisos_otorgados" name="cbx_permisos_otorgados" class="form-control"  multiple="multiple">
                                                    </select>
                                                </div>

                                            </div>
                                        </div>
                                </div>
                            </div>
                            <br>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
