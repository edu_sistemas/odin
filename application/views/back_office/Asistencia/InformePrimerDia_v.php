<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-04-04 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-04-04 [Jessica Roa] <jroa@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h2><?php
                    echo "Clases programadas para hoy " . date("d/m/Y") . "";
                    ?></h2>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="tbl_hoy" class="table table-striped table-bordered table-hover dataTables-example" >
                                <thead>
                                    <th>N° Ficha</th>
                                    <th>Sala</th>
                                    <th>Curso</th>
                                    <th>Empresa</th>
                                    <th>H. Inicio</th>
                                    <th>H. Término</th>
                                    <th>Opciones</th>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <th>N° Ficha</th>
                                    <th>Sala</th>
                                    <th>Curso</th>
                                    <th>Empresa</th>
                                    <th>H. Inicio</th>
                                    <th>H. Término</th>
                                    <th>Opciones</th>
                                </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>Fichas</h3>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
    
    
                        <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>N° Ficha</th>
                                    <th>Modalidad</th>
                                    <th>Curso</th>
                                    <th>F. Inicio</th>
                                    <th>F. Termino</th>
                                    <th>Empresa</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>N° Ficha</th>
                                    <th>Modalidad</th>
                                    <th>Curso</th>
                                    <th>F. Inicio</th>
                                    <th>F. Termino</th>
                                    <th>Empresa</th>
                                    <th>Opciones</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
