<?php
/**
 * Listar_v
 * Description...
 * @version 0.0.1
 * Ultima edicion:  2017-05-12 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-02-07 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-lg-6">
                        <h5>Agregar una nueva clase</h5>
                    </div>
                    <!--   <div class="col-lg-6" style="text-align: right"><a target="_blank" href="../../documentacion/asistencia.html">Necesito ayuda con esto</a></div>-->
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_clase">
                    <div class="row">
                        <label class="col-lg-2">Fecha Inicio y Término</label>
                        <div class="col-lg-6" style="margin-left: -34px;">
                            <div class="input-daterange input-group  col-lg-12 " id="datepicker">
                                <input type="text" class="input-sm form-control" name="fecha_inicio" id="fecha_inicio" placeholder="DD-MM-AAAA" requerido="true" mensaje="Fecha Inicio es un campo requerido" />
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <label class="col-md-2">Hora Inicio: </label>
                        <div class="col-md-2" style="margin-left: -33px">
                            <div class="input-group clockpicker" id="LabelP" data-autoclose="true">
                                <input type="text" class="form-control col-sm-2" id="txt_hora_inicio" name="txt_hora_inicio" value="09:30" requerido="true" mensaje="Ingresar Hora Inicio">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>

                        </div>
                    </div><br>
                    <div class="row">
                        <label class="col-md-2">Hora Término: </label>
                        <div class="col-md-2" style="margin-left: -33px">

                            <div class="input-group clockpicker" id="LabelP" data-autoclose="true">
                                <input type="text" class="form-control col-sm-2" id="txt_hora_termino" name="txt_hora_termino" value="09:30" requerido="true" mensaje="Ingresar Hora Término">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <input type="hidden" id="sala" name="sala" value="<?php echo $sala; ?>"/>	
                        <input type="hidden" id="id_detalle_sala" name="id_detalle_sala" value="<?php echo $id_detalle_sala; ?>"/>	
                        <div class="col-lg-8">
                            <button type="reset" id="reset" class="btn btn-rounded btn-white" data-dismiss="modal">Cancelar</button>
                            <button type="button" id="confirmarR" class="btn btn-rounded btn-primary">Guardar Asistencia</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>