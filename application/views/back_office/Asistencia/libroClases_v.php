<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php
        /**
         * libroClases_v
         *
         * Description...
         *
         * @version 0.0.1
         *
         * Ultima edicion:	2017-01-06 [Felipe Bulboa] <fbulboa@edutecno.com>
         * Fecha creacion:	2017-01-06 [Felipe Bulboa] <fbulboa@edutecno.com>
         */
        ?>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5> Libros de Clases</h5>
                        </div>
                        <div class="ibox-content">
                                <form id="frm_buscar_por_ficha">
                                    <div class="row">
                                        <div class="col-lg-4">
                                        <input type="text" class="form-control" style="float:left;width: 75%;" id="num_ficha"
                                                name="num_ficha" placeholder="Ingrese numero de ficha" onkeypress="return solonumero(event);"
                                                required>
                                            <button class="btn btn-primary" style="float:right;">Buscar</button>
                                        </div>
                                        <div class="col-lg-4"></div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                </form>
                            <div class="table-responsive">

                                <table id="tbl_libros" name="tbl_libros" class="table table-striped table-bordered table-hover dataTables-example" style="font-size: initial">
                                    <thead>
                                        <tr>
                                            <th>N°Ficha</th>
                                            <th>Curso</th>
                                            <th>Inicio</th>
                                            <th>Termino</th> 
                                            <th>Relator</th>   
                                            <th>Sence</th>
                                            <th>Accion</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>N°Ficha</th>
                                            <th>Curso</th>
                                            <th>Inicio</th>
                                            <th>Termino</th> 
                                            <th>Relator</th>   
                                            <th>Sence</th>
                                            <th>Accion</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
