<?php
/**
 * Listar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  
 * Fecha creacion:  2017-02-07 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<style>
.like_button{ width: 40px; height: auto; cursor: pointer; } .input_hidden { display:none; } .selected
{ background-color: #ffa54a; border-radius: 12px; } .checkera{margin:1px;} .centrado{text-align: center;}

</style>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Cumplimiento T&eacute;cnico</h5>
					<div class="ibox-tools"></div>
				</div>
				<div class="ibox-content">
					<form class="form-horizontal" id="frm_sence_registro">
						<p>Cumplimiento T&eacute;cnico</p>

						<div class="form-group">
							<label class="col-lg-2 control-label">Seleccionar ficha</label>
							<div class="col-md-3" style="padding-left: 0px !important;">
								<select id="id_ficha_v" name="id_ficha_v" class="select2_demo_1 form-control">
									<option></option>
								</select>
							</div>
						</div>
						<hr>
						<br>
						<div class="ibox-content">
							<div class="table-responsive">

								<table id="capsulas" class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th></th>
											<th>Nombre del curso</th>
											<th>N° de ficha</th>
											<th>Inicio</th>
											<th>Termino</th>
											<th>Acción</th>
										</tr>
									</thead>
									<tfoot>
										<tr>
											<th></th>
											<th>Nombre del curso</th>
											<th>N° de ficha</th>
											<th>Inicio</th>
											<th>Termino</th>
											<th>Acción</th>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>

				</div>
			</div>

			</form>

		</div>
	</div>

	<div class="modal inmodal in" id="asistencia" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
						<span class="sr-only">Close</span>
					</button>
					<i class="fa fa-book modal-icon"></i>
					<h4 class="modal-title">Cumplimiento T&eacute;cnico - Administrativo</h4>
					<small>La encuesta debe ser realizada despues de la clase programada.
					</small>
				</div>

				<div class="modal-body">
					<form method="post" class="form-horizontal" id="frm_encuesta" name="frmLista">

						<div class="row">
							<div class="col-md-5">
								<div class="form-group">
									<label style="margin-left: 15px;"> Encargado de encuesta: </label>
									<label>
										<?php echo $this->session->userdata('nombre_user'); ?>
									</label>
								</div>
							</div>
							<div class="col-md-7">
								<label> Curso: </label>
								<label id="nombre_curso"></label>
							</div>
						</div>
						<br>


						<div class="row">
							<div class="col-md-5">

								<label id="modal_encuesta_nombre_relator"style="margin-right: 30px;"> Nombre Relator: </label>

							</div>
							<div class="col-md-7">

								<label id="modal_encuesta_sala_curso"style="margin-right: 30px;">  </label>

							</div>
						</div>

						<br>
						<br>

						<div class="ibox-content">
							<input type="hidden" id="numRows" name="numRows" value="">
							<input type="hidden" id="capsulainput" name="capsulainput" value="">
							<input type="hidden" id="id_ficha" name="id_ficha" value="">
							<input type="hidden" id="id_detalle_alm" name="id_detalle_alm" value="">
							<div class="table-responsive">

								<table id="listaAlm" class="table table-bordered table-hover dataTables-example">
									<thead>
										<th colspan="2" style="font-size: 14px;"></th>
										<th style="font-size: 14px; width: 13%;" class="centrado">
											<button id="btn_todos_si" class="btn btn-primary btn-circle" type="button">
											SI</button>
										
											<button id="btn_todos_no" class="btn btn-primary btn-circle" type="button">
											NO</button>
											</th>
									</thead>
									<thead>
										<th colspan="2" style="font-size: 14px;">I. De los Computadores</th>
										<th style="text-align: center">SI / NO</th>
									</thead>
									<tr>
										<td colspan="2" style="font-size: 14px">1. Estaban todos al inicio del curso, con el software adecuado y se iniciaron en forma normal.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r1" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r1" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-size: 14px">2. Si durante el desarrollo del curso tuvo problemas, fue solucionado por el Departamento técnico oportunamente.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r2" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r2" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<thead>
										<th style="width: 300px; font-size: 14px;" colspan="2">II. De la sala
										</th>
										<th style="text-align: center">SI / NO</th>
									</thead>
									<tr>
										<td colspan="2" style="font-size: 14px">1. Estaba limpia apta para hacer clases (mobiliario y orden)</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r3" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r3" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-size: 14px">2. Mantiene la misma sala.
										</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r4" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r4" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-size: 14px">3. La ventilación era la adecuada.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r5" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r5" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<thead>
										<th style="width: 300px; font-size: 14px" colspan="2">III. Del break</th>
										<th style="text-align: center">SI / NO</th>
									</thead>
									<tr>
										<td colspan="2" style="font-size: 14px">1. El break estaba apto (mesa limpia y elementos en cantidad adecuada)</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r6" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r6" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-size: 14px">2. La coordinación del break fue adecuada, se informo con antelación.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r7" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r7" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<thead>
										<th style="width: 300px; font-size: 14px;" colspan="2">IV. Del Material de apoyo</th>
										<th style="text-align: center">SI / NO</th>
									</thead>
									<tr>
										<td colspan="2" style="font-size: 14px">1.Contaba con la pizarra y borrador.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r8" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r8" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-size: 14px">2. si ud. tiene autorizado previamente un data para su curso, estaba en la sala al inicio.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r9" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r9" value="0" class="input_hidden">
											</label>
										</td>
									</tr>

									<thead>
										<th style="width: 300px; font-size: 14px;" colspan="2">V. Del Material (Contestar solo cuando amerite)</th>
										<th style="text-align: center">SI / NO</th>
									</thead>
									<tr>
										<td colspan="2" style="font-size: 14px">1. El manual fue entregado en forma oportuna y el que corresponde.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r10" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r10" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-size: 14px">2. Contó con el material CD, lápices, etc.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r11" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r11" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-size: 14px">3. Le fue entregado el libro de clase, los temarios y test de diagnosticos.</td>
										<td  class="centrado">
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r12" value="1" class="input_hidden">
											</label>
											
											<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
												<input type="radio" name="r12" value="0" class="input_hidden">
											</label>
										</td>
									</tr>
									<tr>
										<td colspan="2" style="font-size: 14px">4. Le fueron entregadas las encuestas de satisfacción del cliente.</td>
										<td  class="centrado">
												
													<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
														<input type="radio" name="r13" value="1" class="input_hidden">
													</label>
													
													<label type="button" class="checkera btn btn-primary btn-circle btn-outline">
														<input type="radio" name="r13" value="0" class="input_hidden">
													</label>
												
										</td>
									</tr>
								</table>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Cancelar</button>
					<button type="button" id="btn_enviar_encuesta" class="btn btn-primary">Enviar Encuesta</button>
					<br>
					<br>
				</div>
				</form>

			</div>
		</div>
	</div>