<?php
/**
 * PersoLibro_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-01-06 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:	2017-01-06 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
?>
<style>
.select2-close-mask{
    z-index: 2099;
}
.select2-dropdown{
    z-index: 3051;
}
</style>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Listado de Libros Personalizados</h5>
                    <div class="ibox-tools"><button class="btn btn-w-m btn-primary" id="nuevaP">Nueva Personalización</button></div>                    
                </div>
                
                <div class="ibox-content">
                    <div class="table-responsive">


                        <table id="tbl_libros" name="tbl_libros" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead>
                                <tr>
                                    <th>N°s de ficha</th>
                                    <th>Nombre</th>
                                    <th>Nombre Curso</th>
                                    <th>Fecha Inicio</th>
                                    <th>Fecha Termino</th> 
                                    <th>Relator</th>                                    
                                    <th>Accion</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>

                                    <th>N°s de fichas</th>
                                    <th>Nombre</th>
                                    <th>Nombre Curso</th>
                                    <th>Fecha Inicio</th>
                                    <th>Fecha Termino</th> 
                                    <th>Relator</th>                                    
                                    <th>Accion</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-----------------MODAL CREAR LIBRO------------------>
<div class="modal inmodal fade in" id="modalNuevoP" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close cerrar-modal" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Nueva Personalización</h4>
                <small class="font-bold">Seleccione las fichas que agrupará</small>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                	<div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                    	<div class="col-sm-10"><input type="text" class="form-control" id="nombre_perso"></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                    	<label class="col-sm-2 control-label">Fichas</label>
                        <div class="col-sm-8">
                            <select class="js-example-basic-single" style="width:100%" id="selectFichas">
                                <option value="AL">Alabama</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        </div>
                        <div class="col-sm-2"><button type="button" class="btn btn-w-m btn-primary" id="btn_agregar">Agregar</button></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                </form>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Fichas Agregadas</h5>
                        <div class="ibox-tools">
                            
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table id="table_fichas" class="table">
                            <thead>
                            <tr>
                                <th>N° Ficha</th>
                                <th>Nombre Curso</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Termino</th>
                                <th>Relator</th>
                                <th>Descartar</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="nothing">
                                <td colspan="6" style="text-align:center"><strong>No hay fichas Agregadas</strong></td>                                
                            </tr>                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white cerrar-modal" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="guardarCambios">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>

<!-----------------MODAL MODIFICAR LIBRO------------------>
<div class="modal inmodal fade in" id="modalModificarP" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Modificar Personalización</h4>
                <small class="font-bold">Agregue/Elimine las fichas que agrupará</small>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                	<div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                    	<div class="col-sm-10"><input type="text" class="form-control" id="nombre_perso_modificar" name="nombre_perso_modificar"></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                    	<label class="col-sm-2 control-label">Fichas</label>
                        <div class="col-sm-8">
                            <select class="js-example-basic-single" style="width:100%" id="selectFichas_modificar">
                                <option value="AL">Alabama</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        </div>
                        <div class="col-sm-2"><button type="button" class="btn btn-w-m btn-primary" id="btn_agregar_modificar">Agregar</button></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                </form>
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Fichas Agregadas</h5>
                        <div class="ibox-tools">
                            
                        </div>
                    </div>
                    <div class="ibox-content">

                        <table id="table_fichas_modificar" class="table">
                            <thead>
                            <tr>
                                <th>N° Ficha</th>
                                <th>Nombre Curso</th>
                                <th>Fecha Inicio</th>
                                <th>Fecha Termino</th>
                                <th>Relator</th>
                                <th>Descartar</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr id="nothing">
                                <td colspan="6" style="text-align:center"><strong>No hay fichas Agregadas</strong></td>                                
                            </tr>                            
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <input type="hidden" id="hiddenId">
                <button type="button" class="btn btn-primary" id="guradarCambios_modificar">Guardar</button>
            </div>
        </div>
    </div>
</div>
