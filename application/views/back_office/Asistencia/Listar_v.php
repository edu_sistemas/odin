<?php
/**
 * Listar_v
 *
 * Description...
 * 
 * @version 0.0.1
 *
 * Ultima edicion:  2017-05-12 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-02-07 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="row">
                    <div class="col-lg-6">
                        <h5>Módulo de asistencia</h5>
                    </div>
					<!--   <div class="col-lg-6" style="text-align: right"><a target="_blank" href="../../documentacion/asistencia.html">Necesito ayuda con esto</a></div>-->
                    <div class="ibox-tools"></div>
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_sence_registro">
                    <p>Asistencia diaria</p>
					<div id="mensaje"></div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Seleccionar ficha</label>
                        <div class="col-md-3" style="padding-left: 0px !important;">
                            <select id="id_ficha_v" name="id_ficha_v" class="select2_demo_1 form-control">
                                <option></option>
                            </select>
                        </div>
						<button type="reset" id="reset_superior" class="btn btn-info col-lg-2" >Volver</button>

                    </div>
                    <hr>

					<div class="ibox-tools">
						<!--<button type="button" id="btn_agregar_alumnos" class="btn btn-rounded btn-danger btn-outline">Agregar Alumnos  <i class="fa fa-group"></i></button>-->
						<!--<input type="text" id="sala" name="sala" />-->
						<input type="hidden" id="sala" name="sala" />	
						<input type="hidden" id="reserva" name="reserva" />
						<button style="margin-left:5px;" type="button" id="btn_agregar_quitar_clase" class="btn btn-rounded btn-danger btn-outline">Agregar Clases <i class="fa fa-tasks"></i></button>
					</div>
					<div class="ibox-content">

						<div class="table-responsive">

							<table id="capsulas" class="table table-striped table-bordered table-hover dataTables-example">
								<thead>
									<tr>
										<th></th>
										<th>Nombre del curso</th>
                                        <th>Sala/Direccion</th>
										<th>N° de ficha</th>
										<th>Inicio</th>
										<th>Termino</th>
                                        <th>Asistencia</th>
										<th>Acción</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th></th>
										<th>Nombre del curso</th>
                                        <th>Sala/Direccion</th>
										<th>N° de ficha</th>
										<th>Inicio</th>
										<th>Termino</th>
                                        <th>Asistencia</th>
										<th>Acción</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal inmodal in" id="asistencia" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
                <i class="fa fa-book modal-icon"></i>
                <h4 class="modal-title">Confirmar asistencia</h4>
				<label id="fecha_clase"></label><br>
				<label id="clase_fin"></label><br>
                <small>La asistencia debe ser realizada 15 minutos despues de
                    comenzar la clase.</small><br><br>
            </div>

            <div class="modal-body">

				<div class="row">
					<div class="col-lg-6 col-lg-offset-3">
						<a id="btn_add_alumno" class="btn btn-primary btn-rounded btn-block" href="#"><i class="fa fa-info-circle"></i> Agregar Alumno</a>
					</div>
				</div><br>
				<div class="row">
					<form method="post" class="form-horizontal" id="frm_Agregar_alumno" name="frm_Agregar_alumno">
						<div id="nuevoAlumno" style="border: solid; border-color: #23c6c8; border: solid; border-color: #23c6c8; padding: 20px 10px 5px 10px;">
							<div class="ibox form-inline" style="margin-left: 38px;">
								<label class="input-group">Rut: </label>
								<div class="input-group"> 
									<input type="text"  id="txt_rut" name="txt_rut" class="form-control required" requerido="false" onkeypress="return solonumero(event)" mensaje ="Debe Ingresar la URL del menú" size="8">
								</div>
								-<div class="input-group"> 
									<input type="text" size="1" id="txt_dv" name="txt_dv" class="form-control required" requerido="false" mensaje ="Debe Ingresar la URL del menú">
								</div>
								<div id="mensajerut">
									<p style="margin-left: 28px; margin-top: 10px; color: red; font-weight: bold;">El Rut no puede estar vacío</p>
								</div>
								<div class="input-group">
									<p id="out_print" style="color: red"></p>
								</div>
							</div>	
							<div class="ibox form-inline">
								<label class="input-group">Nombres: </label>
								<div class="input-group"> 
									<input type="text" size="45" id="txt_nombre" name="txt_nombre" class="form-control"   requerido="true" mensaje ="Debe Ingresar el nombre del Alumno">
								</div>
							</div>
							<div class="ibox form-inline">
								<label class="input-group">Apellidos: </label>
								<div class="input-group"> 
									<input type="text" size="45" id="txt_apellido" name="txt_apellido" class="form-control" requerido="true" mensaje ="Debe Ingresar el apellido del Alumno">
								</div>
							</div>
							<a <button id="guardar_datos" class="btn btn-info btn-rounded" >Guardar Datos</button></a>
						</div>	
						
                        <input type="hidden" id="id_oc" name="id_oc" value="">
						<input type="hidden" id="id_capsula_alm" name="id_capsula_alm" value="">						
						<input type="hidden" id="id_relator_alm" name="id_relator_alm" value="">										
						<input type="hidden" id="id_ficha_alm" name="id_ficha_alm" value="">
						
					</form>
				</div><br>
                <form method="post" class="form-horizontal" id="frmLista" name="frmLista">

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label style="margin-left: 15px;"> Encargado de asistencia: </label>
                                <label><?php echo $this->session->userdata('nombre_user'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <label> Curso: </label> <label id="nombre_curso"></label>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-md-5">

                            <label style="margin-right: 30px;"> Asistencia: </label>

                            <div id="chk" class="i-checks icheckbox_square-green checked">
                                <label style="margin-left: 30px;"> Presente </label>
                                <!--<input type="checkbox" value="">-->
                            </div>

                            <div id="chk" style="margin-left: 100px;" class="i-checks icheckbox_square-green hover">
                                <label style="margin-left: 30px;"> Ausente </label>
                                <!--<input type="checkbox" value="">-->
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-lg-2 control-label" style="margin-left: 30px;">Relator:</label>
                            <div class="col-md-4" style="padding-left: 0px !important;">

                                <select id="select_relator" name="select_relator" class="form-control required" requerido="true" mensaje="Debe seleccionar un docente!!" >
                                    <option></option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div id="mensaje" class="row">

                    </div>

                    <br> <br>

                    <div class="ibox-content">
                        <input type="hidden" id="numRows" name="numRows" value="">
                        <input type="hidden" id="capsulainput" name="capsulainput" value="">

                        <div class="table-responsive">

                            <table id="listaAlm" class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Rut</th>
                                        <th>Nombre</th>
                                        <th>Asistencia</th>
                                        <th>Atrasado</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Rut</th>
                                        <th>Nombre</th>
                                        <th>Asistencia</th>
                                        <th>Atrasado</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="reset" id="reset" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <button type="button" id="confirmarR" class="btn btn-primary">Guardar Asistencia</button>
                </form>
                <br>
                <br>
            </div>

        </div>
    </div>
</div>


<!-- Modal para la edicion del horario-->

<div class="modal inmodal in" id="edidcion_horario" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
                <i class="fa fa-wrench modal-icon"></i>
                <h4 class="modal-title">Editar Horarios de clase</h4>
				<small></small>
				<label id="fecha_clase_editar"></label><br>
				<label id="clase_fin_editar"></label><br>

            </div>

            <div class="modal-body">
                <form method="post" class="form-horizontal" id="frmeditaHorarios" name="frmLista">

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label style="margin-left: 15px;"> Usuario que modifica:  </label>
                                <label><?php echo $this->session->userdata('nombre_user'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-7">

                        </div>
                    </div>
                    <br>
					<input type="hidden" id="capsule_id" name="capsule_id" value="" />
                    <div class="ibox-content">      
						<div class="row">
							<label class="col-lg-2">Fecha Inicio y Término</label>
							<div class="col-lg-6" style="margin-left: -34px;">
								<div class="input-daterange input-group  col-lg-12 " id="datepicker">
									<input type="text" class="input-sm form-control" name="fecha_inicio_edit" id="fecha_inicio_edit" placeholder="DD-MM-AAAA" requerido="true" mensaje="Fecha Inicio es un campo requerido" />
								</div>
							</div>
						</div><br>
						<div class="row">
							<label class="col-md-2">Hora Inicio: </label>
							<div class="col-md-3" style="margin-left: -33px">
								<div class="input-group clockpicker" id="LabelP" data-autoclose="true">
									<input type="text" class="form-control col-sm-2" id="txt_hora_inicio_editar" name="txt_hora_inicio_editar" value="" requerido="true" mensaje="Ingresar Hora Inicio">
									<span class="input-group-addon">
										<span class="fa fa-clock-o"></span>
									</span>
								</div>

							</div>

							<label class="col-md-2">Hora Término: </label>
							<div class="col-md-3" style="margin-left: -33px">

								<div class="input-group clockpicker" id="LabelP" data-autoclose="true">
									<input type="text" class="form-control col-sm-2" id="txt_hora_termino_editar" name="txt_hora_termino_editar" value="" requerido="true" mensaje="Ingresar Hora Término">
									<span class="input-group-addon">
										<span class="fa fa-clock-o"></span>
									</span>
								</div>
							</div>
						</div><br>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="reset" id="reset_edit" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <button type="button" id="confirmarEdit" class="btn btn-primary">Guardar Cambios</button>
                </form>
                <br>
                <br>
            </div>

        </div>
    </div>
</div>

<!-- modal sala -->

<div class="modal inmodal in" id="edicion_sala" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated fadeIn">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
                <i class="fa fa-wrench modal-icon"></i>
                <h4 class="modal-title">Editar Sala de clase</h4>
                <small></small>
                <label id="fecha_clase_editar"></label><br>
                <label id="clase_fin_editar"></label><br>

            </div>

            <div class="modal-body">
                <form method="post" class="form-horizontal" id="frmeditaSalas" name="frmeditaSalas">

                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label style="margin-left: 15px;"> Usuario que modifica:  </label>
                                <label><?php echo $this->session->userdata('nombre_user'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-7">

                        </div>
                    </div>
                    <br>
                    
                    <div class="ibox-content">
                        <div class="row">
                            <label class="col-lg-2">Sala</label>
                            <div class="col-lg-6" style="margin-left: -34px;">
                                <div class="input-daterange input-group  col-lg-12 " id="datepicker">
                                    <select name="sala_edit" id="sala_edit">
                                        <option>(seleccione)</option>
                                    </select>
                                    <input type="hidden" id="detalle_sala" name="detalle_sala" value="">
                                    <input type="hidden" id="disponibilidad_detalle" name="disponibilidad_detalle" value="">
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="reset" id="reset_edit" class="btn btn-white" data-dismiss="modal">Cancelar</button>
                <button type="button" id="confirmarEditSala" class="btn btn-primary">Guardar Cambios</button>
                </form>
                <br>
                <br>
            </div>

        </div>
    </div>
</div>


<style type="text/css">

    .popover{
        z-index: 2051;
    } 
    .dataTables_filter {
        display: none;
    }

</style>