<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:	2017-05-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Busqueda</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" id="frm" name="frm">
                        <div class="row">
                            <div class="col-lg-2">
                                <label for="num_ficha">N° Ficha</label><br>
                                <input id="num_ficha" name="num_ficha" class="select2_demo_3 form-control" style="width: 100%" />
                                <br>
                            </div>
                            <div class="col-lg-5">
                                <label for="empresa">Nombre de empresa</label><br>
                                <select id="empresa_cbx" name="empresa_cbx" class="select2_demo_2 form-control" style="width: 100%; margin-top: 10px !important;">
                                    <option></option>
                                </select>

                            </div>
                            <div class="col-lg-2" style="margin-top: 22px;">
                                <button style="margin-left: 10px;" type="button" id="btn_limpiar_form" name="btn_limpiar_form" class="btn btn-warning pull-right">Limpiar</button>
                                <button type="button" id="btn_buscar_ficha" name="btn_buscar_ficha" class="btn btn-success pull-right">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
	<!--<div id="loading" align="center"><img src="<?php echo base_url('assets/img/loading.gif') ?>" width="84" height="84"><br></div>-->
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Fichas disponibles</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table id="tbl_ficha" class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>N° Ficha</th>
                                    <th>Curso</th>
                                    <th>Empresa</th>
                                    <th>F. Inicio</th>
                                    <th>F. Termino</th>
                                    <th>Detalle</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>N° Ficha</th>
                                    <th>Curso</th>
                                    <th>Empresa</th>
                                    <th>F. Inicio</th>
                                    <th>F. Termino</th>
                                    <th>Detalle</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal inmodal in" id="asistencia" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated fadeIn">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                    </button>
                    <i class="fa fa-book modal-icon"></i>
                    <h4 class="modal-title">Asistencia</h4>
                    <small>El porcentaje de asistencia no depende necesariamente de la duración del curso.</small>
                </div>
                <div class="modal-body">
					<div class="ibox form-inline">
						<div class="col-lg-9 input-group">
							<span class="input-group-addon" style="border-radius: 10px 0px 0px 10px;">Curso</span>
							<input readonly="true" style="border-radius: 0px 10px 10px 0px;" id="txt_nombre_curso" type="text" class="form-control" name="txt_nombre_curso">
						</div>
						<div class="col-lg-2 input-group" style="margin-left: 10px;">
							<span class="input-group-addon" style="border-radius:10px 0px 0px 10px;">Horas</span>
							<input readonly="true" style="border-radius: 0px 10px 10px 0px;" id="txt_duracio_horas" type="text" class="form-control" name="txt_duracio_horas">
						</div> 
					</div>
					<div class="ibox form-inline">
						<div class="col-lg-4 input-group">
							<span class="input-group-addon" style="border-radius: 10px 0px 0px 10px;">Dias</span>
							<input readonly="true" style="border-radius: 0px 10px 10px 0px;" id="txt_nombre_dias" type="text" class="form-control" name="txt_nombre_dias">
						</div>
						<div class="col-lg-3 input-group" style="margin-left: 10px;">
							<span class="input-group-addon" style="border-radius:10px 0px 0px 10px;">N° Alumnos</span>
							<input readonly="true" style="border-radius: 0px 10px 10px 0px;" id="txt_alumnos" type="text" class="form-control" name="txt_alumnos">
						</div> 
						<div class="col-lg-3 input-group" style="margin-left: 10px;">
							<span class="input-group-addon" style="border-radius:10px 0px 0px 10px;">Sala</span>
							<input readonly="true" style="border-radius: 0px 10px 10px 0px;" id="txt_sala" type="text" class="form-control" name="txt_sala">
						</div> 
					</div>
					<div class="ibox form-inline">
						<div class="col-lg-5 input-group">
							<span class="input-group-addon" style="border-radius: 10px 0px 0px 10px;">Fecha Inicio</span>
							<input readonly="true" style="border-radius: 0px 10px 10px 0px;" id="txt_inicio" type="text" class="form-control" name="txt_inicio">
						</div>
						<div class="col-lg-6 input-group" style="margin-left: 10px;">
							<span class="input-group-addon" style="border-radius:10px 0px 0px 10px;">Fecha Termino</span>
							<input readonly="true" style="border-radius: 0px 10px 10px 0px;" id="txt_termino" type="text" class="form-control" name="txt_termino">
						</div> 
					</div>
				</div>
				<div class="ibox-content">
 
					<div class="table-responsive">
						<table id="listaAlm" class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>Rut</th>
									<th>Nombre</th> 
									<th>Asistencia</th>
									<th>Detalle</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Rut</th>
									<th>Nombre</th> 
									<th>Asistencia</th>										
									<th>Detalle</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" id="reset" class="btn btn-white" >Salir</button>
					<br>
				</div>
			</div>
		</div>
	</div>

	<div class="modal inmodal" id="modal_detalle" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog"> 
			<div class="modal-content animated flipInY">
				<div class="modal-header">

					<h4 id="titulo" class="modal-title"></h4>
					<small class="font-bold">Información detallada de Asistencia. (Sólo se muestran las clases que se paso asistencia).</small>
				</div>
				<div class="ibox-content">
					<div class="table-responsive">
						<table id="listaAlmdetalle" class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>Clase</th>
									<th>Estado</th>  
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="close_modal_detalle" class="btn btn-white">Close</button>

				</div>
			</div>
		</div>
	</div> 







