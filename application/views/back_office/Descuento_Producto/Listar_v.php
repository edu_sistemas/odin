<?php
/**
 * UsuarioLista_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-12-26 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2016-12-26 [Jessica Roa] <jroa@edutecno.com>
 */
?>

    <style>
        .select2-container--open {
            z-index: 2060;
        }

        .swal2-container {
            z-index: 2061;
        }
    </style>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Descuentos de Productos</h5>
                        <br>
                        <form class="form-horizontal" id="frm_filtrarxxxx" name="frm_filtrarxxxx">
                            <div class="row">
                                <div class="col-lg-4 ">
                                    <label>Línea de negocio</label>
                                    <select id="linea_negocio_filtro" name="linea_negocio_filtro" class="select2 form-control">
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label>Sub Línea de negocio</label>
                                    <select id="sub_linea_negocio_filtro" name="sub_linea_negocio_filtro" class="select2 form-control">
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                            </div>
                            <div class="col-lg-4">
                                <br>
                                <button id="btn_filtrar" type="button" class="btn btn-primary">Filtrar</button>
                            </div>
                        </div>
                        <div class="ibox-tools">
                            <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo</button>
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">


                            <table id="tbl_descuentos_productos" class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Línea de Negocio</th>
                                        <th>Sub Línea de Negocio</th>
                                        <th>Fecha de Inicio</th>
                                        <th>Valor Descuento</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Línea de Negocio</th>
                                        <th>Sub Línea de Negocio</th>
                                        <th>Fecha de Inicio</th>
                                        <th>Valor Descuento</th>
                                        <th>Opciones</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal inmodal" id="modal_modificar_descuento" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content animated bounceInRight">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Cerrar</span>
                        </button>
                        <h4>Sub Línea de Negocio</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" id="frm_nuevo_descuento">
                            <input type="hidden" id="id_descuentos_producto" name="id_descuentos_producto" />

                            <div class="form-group" id="group_linea_negocio">
                                <label class="col-md-4 control-label" for="sub_linea_negocio">Sub Línea Negocio</label>
                                <div class="col-md-6">
                                    <select id="sub_linea_negocio" name="sub_linea_negocio" required maxlength="200" type="text" placeholder="Seleccione la sub línea negocio"
                                        class="select2 form-control input-md">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="valor_descuento">Valor del descuento</label>
                                <div class="col-md-6">
                                    <input id="valor_descuento" name="valor_descuento" required type="number" placeholder="Ingrese valor descuento" class="form-control input-md">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="fecha_inicio">Fecha de inicio</label>
                                <div class="col-md-6">
                                    <input type="date" id="fecha_inicio" name="fecha_inicio" required maxlength="200" type="text" placeholder="Ingrese valor descuento"
                                        class="form-control input-md">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-4" style="text-align: center">
                                    <br>
                                    <button type="submit" id="btn_editar_descuento" class="btn btn-primary">
                                        <i class="fa fa-check"></i>Guardar</button>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button id="cerrarmodal" type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>