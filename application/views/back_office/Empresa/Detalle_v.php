<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <input type="hidden" id="id_empresa" name="id_empresa"                                   
           value="<?php echo $data_empresa[0]['id_empresa']; ?>">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h1><?php echo $data_empresa[0]['razon_social_empresa']; ?></h1>
                <p>RUT: <?php echo $data_empresa[0]['rut_empresa']; ?></p>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">
                <h3>Datos Básicos y de Contacto</h3>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    Información Básica</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label for="">RUT:</label>
                                        <?php echo $data_empresa[0]['rut_empresa']; ?>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="">Razón Social:</label>
                                        <?php echo $data_empresa[0]['razon_social_empresa']; ?>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="">Nombre Fantasía:</label>
                                        <?php echo $data_empresa[0]['nombre_fantasia']; ?>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <label for="">Giro:</label>
                                        <?php echo $data_empresa[0]['giro_empresa']; ?>
                                    </div>
                                    <div class="col-lg-4">
                                        <label for="">Dirección Principal:</label>
                                        <?php echo $data_empresa[0]['direccion_empresa']; ?>
                                    </div>
                                   
                                    <div class="col-lg-4">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    Información de Contacto</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <table id="tbl_contacto_empresa" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Tipo Contacto</th>
                                                <th>Email</th>
                                                <th>Teléfono</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Juan Perez</td>
                                                <td>Finanzas</td>
                                                <td>juan@perez.cl</td>
                                                <td>0225487951</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    Direcciones</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row">
                                    <table id="tbl_direccion_empresa" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Direccion</th>
                                                <th>Tipo</th>
                                                <th>Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                    Infromación Facturación
                                </a>
                            </h4>
                        </div>

                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row"> 

                                    <div class="col-md-3">
                                        <label  title="Dirección Factura">D.Factura:</label>
                                        <?php echo $data_empresa[0]['direccion_factura']; ?>
                                    </div>

                                    <div class="col-md-3">
                                        <label  title="Comuna Factura" >C. Factura:</label>
                                        <?php echo $data_empresa[0]['comuna_facturacion']; ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label  title="Región Factura" >R. Factura:</label>
                                        <?php echo $data_empresa[0]['region_facturacion']; ?>
                                    </div>

                                    <div class="col-md-3">
                                        <label title="Dirección despacho" >D.Despacho:</label>
                                        <?php echo $data_empresa[0]['direccion_despacho']; ?>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                    Infromación SII
                                </a>
                            </h4>
                        </div>

                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="row"> 

                                    <div class="col-md-4">
                                        <label  title="Mail SII">Mail SII:</label>
                                        <?php echo $data_empresa[0]['mail_sii']; ?>
                                    </div>

                                    <div class="col-md-4">
                                        <label  title="Nombre Contacto Cobranza" >Nombre c.Cobranza</label>
                                        <?php echo $data_empresa[0]['nombre_contacto_cobranza']; ?>
                                    </div>
                                    <div class="col-md-4">
                                        <label  title="Mail contacto cobranza" >@ Contacto Ccobranza</label>
                                        <?php echo $data_empresa[0]['mail_contacto_cobranza']; ?>
                                    </div> 

                                </div>
                                <div class="row"> 
                                    <div class="col-md-3">

                                        <label  title="Observaciones Glosa Facturacion">Obs. Glosa Fac.:</label>
                                    </div>

                                    <div class="col-md-9">

                                        <?php echo $data_empresa[0]['observaciones_glosa_facturacion']; ?>
                                    </div>



                                </div>
                                <div class="row"> 

                                    <div class="col-md-3">
                                        <label  title="Requiere Orden Compra" >Requiere Orden Compra:</label>
                                        <?php echo $data_empresa[0]['requiere_orden_compra']; ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label  title="Requiere Hess" >Req. Hess:</label>
                                        <?php echo $data_empresa[0]['requiere_hess']; ?>
                                    </div> 

                                    <div class="col-md-3">
                                        <label  title="Requiere N° contrato" >Req. N° contrato:</label>
                                        <?php echo $data_empresa[0]['requiere_numero_contrato']; ?>
                                    </div>
                                    <div class="col-md-3">
                                        <label  title="Requiere OTA" >Requiere OTA:</label>
                                        <?php echo $data_empresa[0]['requiere_ota']; ?>
                                    </div> 

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <h3>Datos Financieros</h3>
                <h4>Ventas en los últimos 12 meses</h4>
                <canvas id="myChart" style="width: 100%; height: 300px;"></canvas>

                <table id="tbl_ventas_empresa" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                            <th>N° de Ficha</th>
                            <th>Nombre del Curso</th>
                            <th>Tipo de venta</th>
                            <th>Fecha de Venta</th>
                            <th>Total Venta</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>N° de Ficha</th>
                            <th>Nombre del Curso</th>
                            <th>Tipo de venta</th>
                            <th>Fecha de Venta</th>
                            <th>Total Venta</th>
                        </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
</div>

