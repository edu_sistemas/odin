<?php
/**
 * Home_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-29 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nueva Empresa</h5>
                <div class="ibox-tools">
                </div>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" id="frm_empresa_registro"  >
                    <p> Ingrese una nueva empresa </p>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">RUT:</label>
                        <div class="col-lg-4">
                            <input type="number" class="form-control required"
                                   id="rut" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el RUT de la empresa"
                                   placeholder="Ej. 123456789"
                                   name="rut"
                                   maxlength="8"
                                   class="form-control" onkeypress="return solonumero(event)">

                        </div>
                        <p style="width: 0" class="col-lg-1">-</p>
                        <div class="col-lg-1">
                            <input type="text" class="form-control required"
                                   id="dv" 
                                   maxlength="1" 
                                   requerido="true"
                                   mensaje ="Debe Ingresar el DV de la empresa"
                                   placeholder="Ej. 0"
                                   name="dv"
                                   class="form-control" >
                        </div>
                        <div class="col-lg-3">
                            <p id="out_print" style="color: red"></p>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre Fantasía:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="nombre_fantasia" requerido="true"
                                   mensaje ="Debe Ingresar el nombre de fantasía de la empresa"
                                   placeholder="Ej. 'Edutecno'"
                                   name="nombre_fantasia"
                                   class="form-control"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Razon Social:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="razon" requerido="true"
                                   mensaje ="Debe Ingresar La razón social de la empresa"
                                   placeholder="Ingrese Razon Social Identificador de la empresa"
                                   name="razon"
                                   class="form-control"
                                   >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Holding:</label>
                        <div class="col-lg-7" id="LabelP">
                            <div class="col-md-6">
                                <select id="holding" name="holding"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                            </div> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Descripción:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control required"
                                      id="descripcion" requerido="true"
                                      mensaje ="Debe Ingresar una o más observaciones"
                                      name="descripcion"
                                      placeholder="Ingrese observaciones o referencias de la empresa a registrar. Estas observaciones pueden servir para recordar de que empresa se trata en caso que no se recuerde bien..."
                                      maxlength="200"
                                      rows="6"
                                      ></textarea>
                            <p id="text-out">0/200 caracteres restantes</p>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Giro:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="giro" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el Giro de la empresa"
                                   placeholder="Ingrese Giro  de la empresa"
                                   name="giro"
                                   class="form-control">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Dirección:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="direccion" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el Dirección de la empresa"
                                   placeholder="Ingrese Dirección  de la empresa"
                                   name="direccion"
                                   class="form-control" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Comuna:</label>
                        <div class="col-lg-7" id="LabelP">
                            <div class="col-md-6">
                                <select id="comuna" name="comuna"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                            </div> 
                        </div>
                    </div>
                    
<!--                     <div class="form-group"> -->
<!--                         <label class="col-lg-2 control-label">Ciudad:</label> -->
<!--                         <div class="col-lg-7" id="LabelP"> -->
<!--                             <div class="col-md-6"> -->
<!--                                 <select id="ciudad" name="ciudad"  class="select2_demo_3 form-control"> -->
<!--                                     <option></option> -->
<!--                                 </select> -->
<!--                             </div>  -->
<!--                         </div> -->
<!--                     </div> -->


                    <!-- Select Basic -->
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Teléfono temporal:</label>
                        <div class="col-lg-6">
                            <input type="tel" class="form-control required"
                                   id="telefono_temporal" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el Dirección de la empresa"
                                   placeholder="Ej. 0221234567"
                                   name="telefono_temporal"
                                   class="form-control" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Dirección Factura:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="direccion_factura" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar la Dirección de facturación"
                                   placeholder="Ingrese Dirección facturación"
                                   name="direccion_factura"
                                   class="form-control" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Comuna Factura:</label>
                        <div class="col-lg-7" id="LabelP">
                            <div class="col-md-6">
                                <select id="comuna_factura" name="comuna_factura"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                            </div> 
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Dirección despacho:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="direccion_despacho" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar la Dirección de despacho"
                                   placeholder="Ingrese Dirección despacho"
                                   name="direccion_despacho"
                                   class="form-control" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Email para SII:</label>
                        <div class="col-lg-6">
                            <input type="email" class="form-control required"
                                   id="mail_sii" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el email para SII"
                                   placeholder="Debe Ingresar el email para SII"
                                   name="mail_sii"
                                   class="form-control" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre contacto cobranza:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="nombre_contacto_cobranza" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar un nombre de contacto"
                                   placeholder="Ingrese el nombre del contacto"
                                   name="nombre_contacto_cobranza"
                                   class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Email contacto cobranza:</label>
                        <div class="col-lg-6">
                            <input type="email" class="form-control required"
                                   id="mail_contacto_cobranza" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el email"
                                   placeholder="Ingrese el email e del contacto"
                                   name="mail_contacto_cobranza"
                                   class="form-control" >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Teléfono contacto cobranza:</label>
                        <div class="col-lg-6">
                            <input type="tel" class="form-control required"
                                   id="telefono_contacto_cobranza" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el teléfono"
                                   placeholder="Ingrese el teléfono e del contacto"
                                   name="telefono_contacto_cobranza"
                                   class="form-control" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Ejecutivo al que pertenece:</label>
                        <div class="col-lg-7" id="LabelP">
                            <div class="col-md-6">
                                <select id="ejecutivo" name="ejecutivo"  class="select2_demo_3 form-control required"
                                  requerido="true" mensaje ="Debe a que ejecutivo pertenece la empresa">
                                    <option></option>
                                </select>
                            </div> 
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Observaciones glosa facturación:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control required"
                                      id="observaciones_glosa_facturacion" requerido="true"
                                      mensaje ="Debe Ingresar una o más observaciones"
                                      name="observaciones_glosa_facturacion"
                                      placeholder="Ingrese observaciones correspondientes a la glosa de la factura"
                                      maxlength="200"
                                      rows="6"
                                      ></textarea>
                            <p id="text-out2">0/200 caracteres restantes</p>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                    	<label class="col-lg-2 control-label">Opciones:</label>
                    	
                    	<div class="checkbox-inline">
						 <label><input type="checkbox" id="requiere_orden_compra" name="requiere_orden_compra" value="1">Requiere Orden de Compra</label>
						</div>
						
						<div class="checkbox-inline">
						  <label><input type="checkbox" id="requiere_hess" name="requiere_hess" value="1">Requiere Hess</label>
						</div>
						
						<div class="checkbox-inline">
						  <label><input type="checkbox" id="requiere_numero_contrato" name="requiere_numero_contrato" value="1">Requiere numero contrato</label>
						</div>
						
						<div class="checkbox-inline">
						  <label><input type="checkbox" id="requiere_ota" name="requiere_ota" value="1">Requiere OTA</label>
						</div>
					</div>
						

                    <div class="form-group">
                    <br>
                    <br>
                        <label class="col-lg-2 control-label">Logo de la empresa:</label>     
                        <div class="col-lg-2">
                            <div class="image-crop">
                                <img id="prelogo" src="<?php echo 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAIEA3IDASIAAhEBAxEB/8QAHgABAAMBAAMBAQEAAAAAAAAAAAgJCgcEBQYCAwH/xABTEAEAAQMDAQQECAgICgoDAAAAAQIDBAUGEQcICRIhEzFBURQiOGF2gZG0FRgyUldxhbUWI0Jicpah0xcZR1aCg5WjxNQkNENTY3OSk6WxM0Sz/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB6jd27tG2FtnUtw7i1LH0fRNOs1ZGXnZVfgt2qI9czP9kRHnMzERzMqgO1t3t+7d/Z2bt3o/Xe2ftimarVWvVUxGpZserxUc/8AV6Z9nH8Z6p8VPnTAWvdSeuHT7o9ixf3tvLRdsxVT4qLeo5tFu7cj+ZbmfHX/AKMSjdunvauzrt29XbxNf1fcU0zMTVpekXop5+ab0W+f1x5KK9X1jP3BqWRqOqZ2TqWoZFc3L2Xl3art27V7aqq6pmZn55l4gLrau+l6G03PDG3N+VR+fGnYfH3vl73RO+E7P2q1005V3c+jRPrqzdJiqKf1+iuXJ+xRqA0VbI7fHZ96g127eldU9CsXbnlTb1a5Xp1Uz7v+k02+Z/U7ppup4es4VrM0/LsZ2HejxW8jGuU3Ldce+KqZmJ+plhfW9Peru9uk2pRn7M3XrG2MrxRVVVpmZcs03OPZXTTPhrj5qomAaexwDsF9Uty9aeydsPeO8NQjVdx6jTm05WZFmiz6X0WbkWaJmmiIpifBbo54iOZjn2u/g5V+Nj0Q/TJ0/wD60YP96fjY9EP0ydP/AOtGD/es1gDSn+Nj0Q/TJ0//AK0YP96fjY9EP0ydP/60YP8Aes1gDSn+Nj0Q/TJ0/wD60YP968rSe030e1/VcLTNM6r7H1HUs29RjYuHibjw7t6/drqimi3RRTcmaqqqpiIpiJmZmIhmidV7J3yp+jf0z0b79ZBpTAByr8bHoh+mTp//AFowf70/Gx6Ifpk6f/1owf71msAaU/xseiH6ZOn/APWjB/vXRtB3Bpe6tGxNX0XUsTWNKzLcXcbOwL9N+xfon1VUXKJmmqPniWWZZh3RXbB/gvr3+BLdeb4dJ1S7Ve25kXqvLHyqvOvF5n1U3fOqmPz+Y85ueQW8AAOVfjY9EP0ydP8A+tGD/euqsq4NKf42PRD9MnT/APrRg/3r7XZXULavUrSrup7Q3No+6tNtXpxrmZomfazLNF2KaaptzXbqqiKopronw888VRPthl3XU9yp8ljdP0zyvuOCCf4AAAAAPn969Qtq9NdKtanu/c2j7V027ejGt5mt59rDs13Zpqqi3FdyqmJqmmiufDzzxTM+yXxX42PRD9MnT/8ArRg/3qKvfV/JY2t9M8X7jnKVgaU/xseiH6ZOn/8AWjB/vX3+1t2aHvnQsXW9t6zp+4NFyvF8H1HS8qjJx73hqmirwXKJmmriqmqmeJ8ppmPXDLav+7rj5CfTL9p/vTLBKoAAAAAAcf7THal2P2VtjzuDd+ZVVk5Hit6do+LxVl59yI86bdMz5UxzHirnimnmPbMRNKvaZ7w7qz2kszLw72r3dpbQuTVTb27ol6q1brtz7L9yOKr88cc+LijmOYopBc/1O7afRDo/fu426OpGi42da5i5g4V2rNyKJ91VqxFdVM/0ohwbW++K6A6VemjFo3XrNMf9phaVRTTP/u3aJ/sUdgLs8Pvn+hWVcim5om+MSmZ48d7TcWYj5/i5VU/2OobG7zbs6b6vUY9G/reh5VcxEWtdw72JTH67tVPo4+utn8Aal9v7k0ndmlWdT0PVMLWdNvRzazNPyKL9muP5tdEzE/VL2LMj0l65796F6/TrGxN06jtzL8UVXKMW7/E3+PVF21PNFyPmrpmFvfYi70HQ+vubg7K6hWcXa2/b0xaxMqzM04GqV+ymjxTM2rs+yiZmKp/JnmYoBPMAAAAct7QvaS2N2Y9kV7l3tqfwa3XM28PT8eIry867Ec+js2+Y5n1c1TMU08xzMcwpt7S3egdWuuuXlafoGoXenm0qpqpt6fot+qjKvUez0+THFdUzHrpo8FPE8TE+sFzXU3tK9LOjdVVvee/tC0DKp85wsjMpqyuPfFinm5P1Uo87k73Ps76FcuUYesa5uCKZ4irTdHuUxV+r0/o1FN27Xfu13LldVy5XM1VV1zzNUz65mfbL8guur76XobTc8Mbd35XH58adh8f25fL3mid8J2ftVuU05V3c+jRPrqzdJiqI/X6K5clRqA0XbD7d/QHqRdt2dG6paFRfucRTZ1W7Vp1dU/mxGTTb5n5o5d2xsm1mY9u/Yu0X7FymK6LluqKqaqZ9UxMeUwytumdIO0t1P6DZtF/Yu9NU0K1FXiqwqLvpcS5P8/Hr8Vur9c08+YNLghr3b/bW3N2vdv7wxt2aLp2Bq+1/gMV5+m1VU282Mj0/Ezaq58FVPwfzmKpifH5RTx5zKAB6Dfm/dv8ATHaOp7n3TquPoug6bam9lZuVVxTRT7Ij21VTPERTETMzMRETMxAPfufdTu0F026MWvHvfe+ibbuTHipxszLpjIrj302YmblX1UyqY7Wfezb36oZuboHSq7k7F2lEzb/ClE+HVc2PzvHEz8Hpn2RRPj99fn4YgNqGoZWrZ1/Mzsm9mZl+ubl3IyLk3LlyqfXVVVPnMz75Behufvc+zxoF2ujD1bXdxeGePHpmkXKYq/V6ebT5envo+hlV3wTt/fdNP/eTpuJ4fvXP9ilABfLtLvYezrue9btZW5dT25XX5R+F9JvRTE+6arUXKY/XM8fOkt096ubJ6s6dOfszdej7nxYiJrq0vNt35t/NXTTPNE/NVESzCPabZ3TrOy9axtY2/q2bomrY1Xis52nZFdi9bn3010TEx9oNSgqS7Hve7appedg7V64VRqWmVzTZs7uxrPGRj+yPhVunyuU++uiIqjjmYr55i2PStVwtc0zE1HTsuxn6fl2qb+PlY1yLlq9bqjmmumqPKqmYmJiY9YPKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABS73t3atzuofVS70l0PNqt7U2tcp/CNNqr4ubqPHNXi99NmJ8ER+f6SfPinivl7vfO5sjem9dwbhzK5uZerahkZ96uqeZqru3Kq6pn66pekATI6F91V1p6y6Ria1n4+BsTRcmiLtm7uCuunJu0T6qqceimqqP9Z4OfX7nN+wFgbc1Tti9LcbdVFi5pFeqTMUZPHo6smLNycWJifKeb8WY49s8Q0UAqhxu41za8eKsjrLYtX+PyLW2qq6ef6U5UT/Y+S3h3JXUTTrFy5tnf+3Ncrp84tahYv4NVfzR4Yuxz+uYj51xYDOF1n7GPWToFbvZO8NjahjaVamedXwYjLwoj2TVdtTVFHPur8M/M4o1TV0U3KKqaqYqpqjiaZjmJhCftX91t0566YmZrOysfF6fb3mJrpvYVrwadmV+vi/YpjimZn/tLcRPnMzFfqB9h3XHyE+mX7T/AHplpVOKdjPoprnZ27Nmz+nu5MrT83WtH+GfCL+l3K7mPV6XMv36fBVXRRVPFN2mJ5pjzifXHnPawUA/4rjtO/oy/wDn9L/5lz/rX2M+sXZ22ri7k6hbP/g/ouVm0afZyfwnh5Piv1UXLlNHhs3q6o5ptXJ5mOPi+vmY50fIAd9X8lja30zxfuOcClYAHf8Apb2Cuu3WnYmmby2bsb8M7b1L0vwTN/C+BY9J6O7Xar+Jdv01xxXbrjzpjnjmPKYl2rs9d3D2idjdfemm5Nb6efAtF0fc2mahnZP4b0656Gxayrdy5X4aMiaquKaZnimJmePKJlYr3XHyE+mX7T/emWlUAADKuAA/tg52Rpmbj5mHfuYuXj3Kbtm/Zrmiu3XTPNNVNUecTExExMP4gNBnYC7WmP2qui+Plahet0740KKMLXcaniJrr4/i8mmmPVTdimZ90VU1xHlEcycZveyR2k9X7LPWjSN5af6TJ0yZ+Caxp1FXEZmHVMeko93ijiK6Z9lVMc+XMToq2du/SN/7V0ncmgZ1vUtF1XGt5eJlWp+Lct10xNM/NPn5xPnE8xPnAPcMq7VQyrgLqe5U+Sxun6Z5X3HBUrLqe5U+Sxun6Z5X3HBBP8AAAAAEAO+r+Sxtb6Z4v3HOUrLqe+r+Sxtb6Z4v3HOUrAL/ALuuPkJ9Mv2n+9MtQCv+7rj5CfTL9p/vTLBKoAAAB8P1r6v6B0G6X6/vrct6bWlaRjzdm3Rx6S/cmYpt2aI9tddc00x7PPmeIiZfcKiu+j683tV3ntjpJp2TVTgaVYjWdVooq8rmTdiabFFUe+i34qv9fHuBBPtBde909pHqdqe9d15XpMvKq8GNiW6p9Dg48TPgsWon1U08/rmZmqeZmZc4AHm6Lomobk1fD0rScHJ1PU8y7TZxsPDtVXb165VPFNNFFMTNUzPshPHpH3NnVbe2mWNQ3jrukbBtXqYqjCuU1Z2bRE/n0UTTbp8vZ6SZ98Qlf3VfY703pX0w0/qpuLT6L29tzY/wjAqv0czp2BXH8XFHPqru08V1Vevw1U0+XxuZ9gqP3P3Hu5MTT669u9VtL1XOiOabGp6Pcwrcz7vHRdvTH/pQZ699mXqL2adw29J37t+7pkX+fgmfaqi9h5cR65tXafKZ9UzTPFUcxzEctKz4brT0a2v186c6tsvd2BTnaTn25iK+I9LjXYifBetVfyblMzzE/rieYmYkMyD/AGiuq1XTXRVNFdM8xVTPExPvh911z6Qax0F6tbm2HrnFWfouXNj01NPhpyLUxFVq9THsiu3VRXEezxcPhAXed1/22MjtBbMv7C3lnfCN/bdx4rtZd2fj6pgxMUxdqn23bczTTXP8qKqKvOZq4nYzNdnzrFqXQLrLtTfemVXPSaRm0Xb9m3PHwjHn4t+zPzV25rp+bmJ9jSxousYe4dGwNV0+9Tk4Gdj28rHvU+q5brpiqmqPmmJiQea5717637d7O/SvW99bmuzTgadb/isaiYi7l36vK3Yt/wA6ury90RzM+UTLoSlnvg+0Pf391qxOmWm5VU6Bs+3TXl26J+Je1G7RFVVU+/0duqiiPdNV2PaCI/aD7QG7O0p1K1DeW7sz0uVfn0eLhW5n0GDYiZ8Fm1TPqpjn1+uqZmqeZmZc1ABKPoT3bfXDrxgY2q4W37W1tAyIiu1qm5btWLRdpn+VRaimq7VEx5xV4PDPslNHuzO710jSds6R1e6laVb1HWtQopy9A0bNtxVawrE+dvJuUT5VXa44qoifKimaavypjwWaAqY0buONYv41NWrdXsHCyOPjUYeg15FMT81VWRbmfsep3b3Ie9MHGrr2z1L0LWb8RzTa1PAvYMVfN4qKr3H2LfQGcTrh2L+sXZ6ou5W8dmZlnR6J4/DOBMZeFx7Jqu2+Yt8+yLnhn5nEmqS/Yt5Nm5ZvW6btq5TNFduumKqaqZjiYmJ9cSr87YHdObU6o2c3c3SejE2XuzibtzR4jwaZnVevimmI/wCj1z7JpjwT5c0xzNQOZdxj/ls/Yn/HrVFb/c/9Fd+9F9S60YO+toaxta9duaTax69Tw67VrJm1OdFybNyY8F6mnx0c1UTVHxqfPzhZAD8Xr1vHtV3btdNu1RTNVVdc8RTEeczM+yFC/eJdtXN7UHUm9omhZtdvproGRVb02xRM00592Oaasy5Ht584oifyaJ9UTVUsg71brze6N9mPM0fTMmrH13eV/wDA1mu3VxXbxvD4squPmmji3/ron2KIgAWB91B2O9O60bvz+pe8cCjP2ptrIpsYGDkUeK1m6hxFfNcT5VUWqZoqmmfKaq6OeYiqJDm3Z37sPrF1+0fE1+5jYmydtZVMXLGfr810Xci3PqrtWKYmuYmOJia/BTVE8xMpGZHcb6lTgTXY6w4tzN48rNzbtVFvn3eOMmZ+vwrYYjiH+gz2dpPu+urnZkwbusa5pePr21rcxFWvaFXVfsWeZ8vTUzTTXa9kc1U+HmYiKplGpqhz8DG1TByMLNx7WXh5Fuqzex79EV27tFUcVU1Uz5TExMxMT6+VCPeP9kqx2Xes9F3b9iq1sbc1FzN0miZmYxa6ZiL2LzPnMUTVTNPP8i5THMzEyCJixPuq+23ldOt34PR7eOoTc2jrV/0WiZORVz+Ds2ur4tqJn1WrtU8ceqmuYnyiqqVdj9Wrtdi7RdtV1W7lFUVU10TxNMx6pifZINUw4T2IuutfaI7NOz92Zl702t02J0/Vpn1zl2PiXK593jiKbnHuuQ7sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMN1h2TkdNuq+8dqZVubV7RtXysCaZjjmLd2qmJj5piImJ9sTD5BaJ3unY61GncFfXDaeBXl6fkWqLO5sfHo5qx7lFMUW8viP5E0RTRXP8maaap58UzFXYP3ZvXMa9bvWblVq7bqiui5RMxVTVE8xMTHqmE3+ine69ZOmWBi6ZuW1p3UTTLFMUU3NViqzn+GPVHwij8r+lcorqn2yg6AuY2D31XS7W/R2t2bP3Hta/V+VcxJtahj0frqibdf2W5SZ6cdunoN1VrtWtC6m6JRl3ZimnE1W7Vp96qr82mjIiiap/o8s5wDVNbuUXrdNy3XTXbriKqaqZ5iYn1TEv0zWdGu1N1V6BZlq7sjeup6Ti0VeKrTK7vp8K57/Fj3Obc8+/jmOfKYWr9jXvVtu9b9TwNndSMTF2dvLJqps4mfYrmNN1C5PlFEeKZmzcmfKKapqpqnyiqJmKZCfgACAHfV/JY2t9M8X7jnJ/oAd9X8lja30zxfuOcClYAF/3dcfIT6ZftP8AemWlUir3XHyE+mX7T/emWlUAADKuACY3SDsiT147Ae6N9bcw5vb42juzNuejtU83M/T4wsKu7Z49c1UT4rlEf+ZTETNcIcrqe5U+Sxun6Z5X3HBQq7z7sh/i9dW53Zt3C9DsPdl6u/j0WqeLeBmflXcby8qaZ866I8vKaqYj+LkEK1mPdFdsH+DGu/4Et15vh0rVLtV7beReq8sfKq87mLzPqpu+dVMfn+KPObkcVnP74Odk6XnY+Zh37mLl49ym9Zv2a5ort10zzTVTVHnExMRMTHuBqhZV2g7sB9rPG7VfRfHzM+9bp3vocUYWu41PETXXx/F5NNMeqm7FMz7oqiumPKI5z4gLqe5U+Sxun6Z5X3HBUrLqe5U+Sxun6Z5X3HBBP8AAAAAEAO+r+Sxtb6Z4v3HOUrLqe+r+Sxtb6Z4v3HOUrAL/ALuuPkJ9Mv2n+9MtQCv+7rj5CfTL9p/vTLBKoAAABm67Y+/LnUrtTdUNfruTet3deycaxXVPPNixV6Cz/u7VDSKyya5qFer63qGdcqmu5lZFy/VVPtmqqZmf7QeE+26IdP56rdY9k7O+NFGuaziYF2qn10W7l2mmur6qZqn6nxKTfdpaZb1btwdLrFyPFTRk5l+I+e3g5FyP7aIBoHwsOxp2HYxMW1Rj41i3TatWrccU0UUxxTTEeyIiIh/YAAAVEd9r0xsaXv7p9v3GtRTXq+Df0rMqpjjmvHqprtVT75mm/XHPutx7oVnLm++x06i72dNl58x/GWN127FM+6K8PJqn/wDnCmQBoN7trflzqB2MenWTkXZu5Wm413SLvinnwxjXq7VqP/aptM+S7fuZdRrzeybq1mqqaqcTdeZYpj82Jx8W5x9tyZ+sE6dRz7GlaflZuTX6PGxrVV67XP8AJopiZmfsiWYHqNvTL6j9QNy7rz6qqs3W9SyNRu+KeZiq7cqrmPq8XH1NH/aT1KvRuzr1T1C1VNF3E2rqt+mqPXE04l2qJ/sZngHZexz0dtdee0tsPZmXb9LpmZnxf1Cj2VYtmmq9epmfZ4qLdVMT76ocaTu7mfSbWo9rTU8i5TzXgbWzMm3Puqm/jWufsu1faC7e1aosWqLduim3boiKaaKI4imI9URHsh+xxTtmda9c7O3Zs3h1C23i6fm61o/wP4PY1S3Xcx6vS5lixV46aK6Kp4pu1THFUecR648pDtYpW/x1fW//ADW6f/7Ozv8AnD/HV9b/APNbp/8A7Ozv+cBdSKVv8dX1v/zW6f8A+zs7/nD/AB1fW/8AzW6f/wCzs7/nAXUiFXdvdtbfHbB/wifwy0rb+mfwd/B3wT8BY9+14/hHwrx+k9Leuc8ego4449dXPPlxNUFMnfU78ua11+2jtWi5NWJoWgxkzRz5U38m9X4/L+hZsq8kt+9X1CvN7cO+rNVU1U4mPptimPzYnBsXOPtrmfrRIAaLuwn0ys9J+yZ020ai1FrJyNKt6pl+Xxpv5Uenrir3zT6SKP1URHsZ0WpnQdMt6Loenafajw2sTGt2KY90U0xTH/0DzwAEMu9n6Y2N+dkPWNYi1FeobVzsbVbFcR8bwVVxYu08+7wXpqmP/Dj3JmuOdsrTqNU7JvWCzcjxU07U1K/EfPbx67kf20wDN0AC2PuQN+XL+gdUNl3bs+ixsnE1fGt8+XNymu1en/c2FoqmTuTtRrtdoveuDFUxbv7UuX5p980ZmNET/vJ+1c2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+eRj2suxcsX7dF6zdpmiu3cpiqmumY4mJifXEx7FeHag7n/afUXNzNwdKdSsbG1m9M3K9EyqKq9Lu1z+ZNMTXj8z7KYrp9URTTCxMBnO6udhnrh0VuX69w9P8AVL+nWuZnVNIt/D8Xw/nTXa8Xgj+nFM/M4TVTNFU01RNNUTxMTHnEtU74XfvQnpx1Riud3bF29uO7VHHp9R0yzdvR+q5NPipn54mAZkRe51D7pbs+73pu16Zo2q7My64mfS6HqNc0+L3+jv8ApKYj5qYp+pXX2yu7V3h2WtGu7t0vVKN5bEouU272fbsTZycCapiKPT2uZjwzMxTFymeOZjmKeY5CG5EzE8x5SAL3O637Tmf2gug17SNxZlWduzZ923p+VlXavFcycaumZxr1c+uauKLlEzPnM2vFMzNUpmKau5M1y/j9f986PTVMYuXticuun2TXay7FFM/Zfr+1cqAgB31fyWNrfTPF+45yf6AHfV/JY2t9M8X7jnApWABf93XHyE+mX7T/AHplpVIq91x8hPpl+0/3plpVAAAyrgAup7lT5LG6fpnlfccFLntB9D9B7RfSTX9ibgoinF1Gz/EZUUxVXh5FPnav0fPTVxPHtjxUz5VSiN3KnyWN0/TPK+44Kf4MwPVPppr3RzqFr2y9zYs4et6NlVY2RR5+Grjzpron20V0zTVTPtpqiXyq5zvaeyH/AITthU9WdsYXj3RtnHmnVbNmn42Zp0czNfl66rPM1f0Jr9fhphTGDs/ZH7Smr9lnrRpO8cD0mTpcz8E1jTqKuIzMOqY8dHu8VPEV0z7KqY58uYnjAALqe5U+Sxun6Z5X3HBUrLqe5U+Sxun6Z5X3HBBP8AAAAAEAO+r+Sxtb6Z4v3HOUrLqe+r+Sxtb6Z4v3HOUrAL/u64+Qn0y/af70y1AK/wC7rj5CfTL9p/vTLBKoAAABlq3RpVzQtzavpt2nw3cPMvY9dPumiuaZj+xqVZze3T08udMe1t1P0aqzNmxd1i7qWNTx8X0OVxkUeH5oi74f9GY9gOEpLd23q9vRO250tyLtUU015mTjRMzx53cO/aj+2uEaX1PSnfd/pf1O2nvDGpmu/oOq4upU0U/y/RXaa5p+uKZj6wafx6/b+u4O6dB03WdLyKMvTNRxreXi5Fueabtq5TFdFUfNNMxP1vYAAArr77PV7dnoBsbS5qiLuTueMmmn2zFrEv0zP++j7VNaxrvqOq9jcfWDZ2wsS/F2nbWnXMzMpon8jIyppmKKo98WrVur9V1XKAu87mvSrmn9kjOyK6eKc/c+ZkUT74izj2ufttz9ikNog7vnp5c6Z9jzpppeRZmzmZWnzqt+Ko4q8WVcqyKfF88UXKKePZ4QdI7Q2k3Nf6A9S9MtU+O7m7Y1PGop9814tymI/tZl2qPJxrWZjXce/RF2zdom3XRV6qqZjiYn6mYjq3sLJ6W9Ud27PzKKqMjQ9UydPnx+uqLdyqmmr54mIiYn2xMA+TTg7njcVrRO2BGHcqimvWNv52Dbif5VVNVq/wAR9ViqfqQfdF7OvVm90L647K33aprro0XUrd/It2/yrmPPNF+iPnqtVXKfrBpjRV70f5CfU39mfvTESc0LXMDc2iafrGlZVvO0zUMe3l4uVZnmi9arpiqiumfdNMxP1vRdUulu2OtOxNT2bvLTPwztvUvRfC8L4Rdsek9Hdou0fHtVU1xxXbonyqjnjifKZgGYIX/f4rjsxfoy/wDn9U/5lz/tC93D2dtjdAupe5NE6efAta0fbOp6hg5P4b1G56G/axbly3X4a8iaauKqYniqJiePOJgFIIALVO4x/wAtn7E/49aoqr7jH/LZ+xP+PWqAoU717Srmn9tzeV+uninOw9OyKJ98Rh2rXP225+xEJZB32XTy5pfV7Ym9bdmYxdY0evTbldMeU3sa7Nfn880ZFMR74o+aVb4DUptbV7e4NsaRqlqqKrWdh2cmiqJ5iYroiqP/ALZa2hju8erFjq72Rtg51N6Lufo+HToOdRzzVRdxYi3T4vnqtRar/wBOASQAAcV7aur29E7JHV7Iu1RTTXtjPxomffds1Woj7a4dqQb737qvY2P2WKtrUX4p1PeGo2cOi1E8V/B7NdN+9XHzRVRZon/zYBR2ACxLuTNKuXu0BvnUop5tY+2Kseqr3Tcy7FUR/up+xcsrN7kbp5c0/YHUfe9+zMU6pqGPpWNXVHn4ce3VcucfNM5FEc++j5pWZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOVdrH5LHWT6Gaz9xvOquVdrH5LHWT6Gaz9xvApW7IXeG9QOytcs6NVP8LdhzXzc0DOvTE43M81VYt3zm1M+uaeJonmfixM+Jb92fe3R0f7R+NjWtu7ns6br93iKtva1VTi50Vfm0UzPhu/rt1VfPwzrkTNMxMTxMeqYBqoGcrpl24eunSKzZx9udSdZpwbXEUYWpV059imn82mi/FcUx/R4d40bvlOvWmWKbeTg7O1eqPXdzNLvU1Vfr9Ffoj7IBd6jv2/+oW3On/ZK6kfwhyse3VrGj5Ok6fjXao8eRl3rc0Wot0+uqaaqornj1RRM+xWBuHviOv+tWK7eH/BbQK6o4i7p2k1V1U/PHp7tyPtiUUuqvWjfHW/cP4c33ufUNy6lETTbrzLnxLNM+um1bjii3T7fDRTEfMD4sHv9g7C1/qhvHStrbX0y/q+u6peixi4linmquqfXMz6qaYjmZqniKYiZmYiJBY13Iewcm/vXqTvau3NOJiafY0a1cn1V13bnpq4j+jFi3z/AE4W3uM9kbs56f2XOh2ibJxblvK1GnnM1bOtxMRlZtyI9JXHP8mIiminnz8NFPPny7MAgB31fyWNrfTPF+45yqv8bHrf+mTqB/WjO/vXz+9et/UXqVpVrTN37+3RurTbV6Mm3h63rOTmWaLsU1Uxcii5XVEVRTXXHi454qmPbIPigAX/AHdcfIT6ZftP96ZaVTMttbtC9U9jaFi6JtvqXvDb+i4vi+D6dpevZWNj2fFVNdXgt0XIpp5qqqqniPOapn1y7B2ZO031h1/tJdKNM1PqvvjUdNzd26TjZWHl7jzLtm/arzLVNduuiq5MVU1UzMTTMTExMxINBIAMq4ALqe5U+Sxun6Z5X3HBT/QA7lT5LG6fpnlfccFP8H4u2qL9qu1dopuW66ZpqorjmKon1xMe2FBHeLdkivsv9aLt/RsWqjYW5Krmbo9dMfExquebuJM/+HNUTT76KqPOZipf05D2q+zro/ag6L61snU/R2My7T8J0vUK6eZws2iJ9Fd9/HnNNUR66K6o9vIM2o91vXZusdPN3axtncGFc07WtJyrmHl4tz127lE8T5+2PbEx5TExMeUvSgLqe5U+Sxun6Z5X3HBUrLqe5U+Sxun6Z5X3HBBP8AAAAAEAO+r+Sxtb6Z4v3HOUrLqe+r+Sxtb6Z4v3HOUrAL/u64+Qn0y/af70y1AIDVQMq4DVQMq4DVQqx76Ls93smztnrHpWLNdvHojRNbqoj8imapqxbs/N4qrluap9s2oWnPQb92NonUzZms7V3Hg0ajoer41eJl41z+VRVHrifZVE8TFUecTETHnAMuw7r2vuybubsmdTcjQdUou523suqq7ouuRb4t5tjn1TPqpu0cxFdHsniY+LVTM8KBbb3Unbg0vL2xgdFN8albwdVwapt7azsqvw0ZVmZ5+CTVPquUzM+CJ/KpmKY86YiqztlXpqmmqKqZmJieYmPYlT0j7zTr50h02xpljdNrdGl2KYptYu5sf4ZNER6oi9zTdmOPLia5iPZwC/9xztR9qHaXZX6bZe5dx5Nu9qNyiqjStFouRGRqGRx5UUx64oiZiaq+OKY98zETU7ujviuv24NPrxsGjam27tUcRl6XpVdd2n54+EXbtPP+iiB1B6lbq6r7lv7g3hr+fuPWb0cVZeoX5uVRT7KaefKmmOZ4ppiIj2QB1I6g611X37r28NxZPwvWtay68zJuR5U+KqfyaY9lNMcU0x7IpiPY+bHm6Jomobk1fC0rScLI1LU829TYxsPFtzcu3rlU8U0U0x5zMzPERAOqdknoLmdpHr5tbZVi1XVp1/IjJ1W9RzHoMG3MVX6ufZM0/Ep/nV0x7WkLExbODi2cbHtUWMezRFu3atxxTRTEcRER7IiIRP7vHsX2uyn01u52u27N/qHuCii7ql6jiqMO1HnRiUVe2KZnmuY8qqvfFNMpagKbe+O7Ot/Z/VXTerGl4k/gTdFujD1K5RHxbWoWqOKZn3eks0U8e+bVyZ9a5J8V1m6Q7d67dNdc2RunF+E6RqtibdVVPHpLFyPOi9bmfVXRVEVRPvjz5iZgGYwde7TvZj3d2WOpGVtfc+PVdxa5quaZrFq3MY+o2OfKuifZVHMRVRzzTPl5xMTPIQWLd3H3j2J0Z07F6YdT8q7GzormNI1yYm5OlzVPM2bsRzM2ZmZmKo5miZmJiaZ+JcHoWv6ZujSMXVdG1HF1bS8uiLuPm4N6m9ZvUT6qqK6ZmKo+eJZZnQulPaF6k9Dsqq9sXemr7bprq8dzGxciZxrtXvrs1c265/pUyDTM5V2sfksdZPoZrP3G8qJ233wfaC0OxRbzb22dw10xxN3UtJ8FVXzz6C5aj7IfjqP3uHWTqbsHce0dS0HZeJpuvabk6Xl3cLAyqb0Wb9qq1XNE15NURV4a54mYnz9gISgAtU7jH/AC2fsT/j1qiqvuMf8tn7E/49aoCLfeQdnu92g+zJrWLpeLOVubb9ca3pduiOa7tVqmqLtqPbM12qrkRT7aooZ+2qhSv3nfYUy+j+7c/qlsnTqruw9Yvzd1HExbflo+VXPxpmI9Vi5VPNM+qmqZo8omjkK/0zO7U7ZuP2Yeo+XoO6siu30/3NXRTl3uJqjT8mnyt5PEfyeJ8NfHnx4Z8/BETDMBqf03U8PWdOxs/T8qznYOVbpvWMnGuRct3aKo5pqpqjmKomJiYmPW8lnF6FdtDrB2c7FOFszd+RY0WKpqnRs+inKwuZnmfDbuRPo5mfOZtzTM+2Ug8jvm+u97AnHo0nZVi9xx8Lt6ZkTc/XxVkzTz/ogub3rvfQenG1tR3JubVcbRND0+1N7Jzcuvw0W6Y/+5mfKKY5mZmIiJmYhn57cnary+1l1pydfs0XsTaum0TgaFg3vKqjHirmbtceqLlyr40+6PDTzPh5fF9bu0/1P7RWdbv7+3bm63Ys1+OxgR4bOJYn1c0WLcU0RVxPHi48Ux65ly0B5ekaTma/q2FpmnY1zN1DNv0Y2NjWafFXdu11RTRRTHtmZmIiPneItW7qnsKZeDm4PW3f2nVY800ePbGlZdviuZqj/r1dM+qOJmLcT6+fH5cUTIT57KnRGz2d+gOz9i0xbnN0/Di5qF23PMXcy5M3L9UT7Y8dVURP5tNPudZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8t1V2NT1P6X7w2bXmTp1G4tGzNInMpt+kmxF+xXa9J4OY8Xh8fPHMc8ccw+pAUWdZu6W639M7l7J27iYXUTSKZmab2i3Yt5UU/z8e5MTz81ubiI27ti7k6f6pVpu59v6ptzUKeecXVcO5jXfL+bXES1GPX65t7Stz4FWDrOmYerYVf5WNnY9F63P66aomAZZxo13H2GugO6blVed0l2vbrqnmZwMGnD5n/U+B8dkd2J2Zcmqaq+mNqJnz/i9Z1GiPspyIBn7f7RRVdrpoopmuuqeKaaY5mZ90NCGl9212bNHuRXY6W4NyqPZlZ+ZkR9ly9VDrmxeg/TfpjXTc2lsPbm3L9P/wCxpul2bN6f13KafFP1yCjDoF3c3Wrr3lY1+ztq9tLb1yYmvW9x0VYtvwe+3amPSXeY54mmnw8+uqPWuC7JXYh2F2SdErnRbVWt7sy7cW8/cmdbiL92PKZt2qY5iza5jnwxMzPEeKqriOJEAAAMq41UAMq41UAMq7qvZO+VP0b+mejffrLSmAAAyrjVQAgB3KnyWN0/TPK+44Kf4AAArL73rsh/wk0OnrbtfC8WqaZbox9x2LNPnfxY+LbyuI9dVvyoqn8yaZ8otyqLaqAGVddT3KnyWN0/TPK+44Kf4AAAAAACAHfV/JY2t9M8X7jnKVmqgBlXGqgBlXGqgBlXGqgAAB8V1e6ObQ67bIzNp720azrWjZPxvBc5puWbkRPhu2q4+NRXHM8VRPtmPOJmJqA7TXdI9R+l2Zl6r029J1E2vE1V041qKaNUxqfzarXlF7jyjm18afP4lK7QBll1vQtS21qd7TtX0/L0rULE+G7iZtiqzdtz7qqKoiY+uHgtQm9Omm0eo+JGLuza2jbmx4iaabWr4FrKppj5ouUzx9Timt93R2cdfuzcyulel2qp9mFkZOJT9lq7TAM8w0EYfdldmfBuRXb6YY9UxPPF7VtQux9lWRMOn7G7LfSDpteov7a6a7Y0rLomJpy7emWqsimY912qJr/tBRJ0G7DPWPtD5WPXtzaWTgaJdmPFr2tU1YmDTTP8qmuqObv6rdNc/MuA7HHd7bG7KNi3rNyuN17/ALluaLuv5VnwU40VRxVRjW+Z9HEx5TVMzXVzPnET4UrAAAAAHwvWTolszr7srJ2rvjRLOs6Tdnx0ePmm7j3OJiLtq5HxqK45nzifVMxPMTMTUf2ke6F6i9OcrK1Tple/whbc5qrpwpmizqmPT7qqJmKb3EeXNuYqmf5ELqgGWzc209c2Vq13S9w6Nn6FqVr/APJh6li1496n9dFcRMfY9U1H7o2Xt7fGB8B3HoWma/hef/RtUw7eTb8/X8WuJhxXcHd+9nfc1yqvM6UaFZqqnmY0+m5hR9UWK6Ij6gZ2hoDjuwezLTc8f+DG34vdOtalMfZ8I4fUbe7BPZ72xcorw+k23b1VPq/CFirNj64v1VxP1gzwaRo2oa/n2sHS8HJ1LNuzxbxsSzVduVz81NMTMpH9Le7d7QPVSuzcx9h5O28C5xzm7mrjT6aIn2zbr/jpj+jblfntjZW3tk4XwPbug6ZoGJ/3Gl4dvGt/+mimIe6BEfu++w/qXY20bdtes7oxdw6tub4FN+zg41VuxifB/T8RTXVPiueL08+c00ceGPJLgAHi6npeHrem5Wn6hi2c7AyrVVjIxcm3Fy1et1RxVRVTPlVTMTMTE+U8vKAVW9rTue7mXm5u5uh2RZt03Jm7c2fqF7wRTPtjFv1TxEfzLkxEefFfHFMVqdROlG8ukms16VvPbGqbZz6appi1qWLXaivj20VTHFcfzqZmJ97T68DW9B0zcunXMDV9OxNVwLn5eLm2Kb1qv9dNUTEgyzDRZufsFdnzd125dz+k+3rVdc81Tp1irB8/1WKqOHy9Pdh9mWi76SOmNrxe6da1GY+z4RwDP26D0l7P3UbrpqlGDsXZ+qbirmrwVZGPYmnGtT/4l+ri3b/0qoX8bS7EXQXZF63e0rpTtn01v8i7nYUZldM++Jv+OYn5/W7Rg4GNpmJaxMPHtYmLZp8NuxYoiiiiPdFMeUR+oFdnY+7pPRem+dg7t6vX8TdWv2ZpvY+3seJr0/Frjzib1U8enqjy+LxFETz+X5TFjMRFMRERERHlEQ/0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//9k='; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-lg-2"></div>
                        <div class="col-lg-10">
                            <label title="Subir una nueva imagen" for="inputImage" class="btn btn-primary" style="top:0px;">
                                <input type="file" id="inputImage" requerido="false" mensaje="Debe subir una imagen" name="file" accept="image/*" class="hide">
                                Subir una nueva imagen
                            </label>
                            <textarea id="logo" name="logo" hidden=""><?php echo 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD//gATQ3JlYXRlZCB3aXRoIEdJTVD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCAIEA3IDASIAAhEBAxEB/8QAHgABAAMBAAMBAQEAAAAAAAAAAAgJCgcEBQYCAwH/xABTEAEAAQMDAQQECAgICgoDAAAAAQIDBAUGEQcICRIhEzFBURQiOGF2gZG0FRgyUldxhbUWI0Jicpah0xcZR1aCg5WjxNQkNENTY3OSk6WxM0Sz/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ALUwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB6jd27tG2FtnUtw7i1LH0fRNOs1ZGXnZVfgt2qI9czP9kRHnMzERzMqgO1t3t+7d/Z2bt3o/Xe2ftimarVWvVUxGpZserxUc/8AV6Z9nH8Z6p8VPnTAWvdSeuHT7o9ixf3tvLRdsxVT4qLeo5tFu7cj+ZbmfHX/AKMSjdunvauzrt29XbxNf1fcU0zMTVpekXop5+ab0W+f1x5KK9X1jP3BqWRqOqZ2TqWoZFc3L2Xl3art27V7aqq6pmZn55l4gLrau+l6G03PDG3N+VR+fGnYfH3vl73RO+E7P2q1005V3c+jRPrqzdJiqKf1+iuXJ+xRqA0VbI7fHZ96g127eldU9CsXbnlTb1a5Xp1Uz7v+k02+Z/U7ppup4es4VrM0/LsZ2HejxW8jGuU3Ldce+KqZmJ+plhfW9Peru9uk2pRn7M3XrG2MrxRVVVpmZcs03OPZXTTPhrj5qomAaexwDsF9Uty9aeydsPeO8NQjVdx6jTm05WZFmiz6X0WbkWaJmmiIpifBbo54iOZjn2u/g5V+Nj0Q/TJ0/wD60YP96fjY9EP0ydP/AOtGD/es1gDSn+Nj0Q/TJ0//AK0YP96fjY9EP0ydP/60YP8Aes1gDSn+Nj0Q/TJ0/wD60YP968rSe030e1/VcLTNM6r7H1HUs29RjYuHibjw7t6/drqimi3RRTcmaqqqpiIpiJmZmIhmidV7J3yp+jf0z0b79ZBpTAByr8bHoh+mTp//AFowf70/Gx6Ifpk6f/1owf71msAaU/xseiH6ZOn/APWjB/vXRtB3Bpe6tGxNX0XUsTWNKzLcXcbOwL9N+xfon1VUXKJmmqPniWWZZh3RXbB/gvr3+BLdeb4dJ1S7Ve25kXqvLHyqvOvF5n1U3fOqmPz+Y85ueQW8AAOVfjY9EP0ydP8A+tGD/euqsq4NKf42PRD9MnT/APrRg/3r7XZXULavUrSrup7Q3No+6tNtXpxrmZomfazLNF2KaaptzXbqqiKopronw888VRPthl3XU9yp8ljdP0zyvuOCCf4AAAAAPn969Qtq9NdKtanu/c2j7V027ejGt5mt59rDs13Zpqqi3FdyqmJqmmiufDzzxTM+yXxX42PRD9MnT/8ArRg/3qKvfV/JY2t9M8X7jnKVgaU/xseiH6ZOn/8AWjB/vX3+1t2aHvnQsXW9t6zp+4NFyvF8H1HS8qjJx73hqmirwXKJmmriqmqmeJ8ppmPXDLav+7rj5CfTL9p/vTLBKoAAAAAAcf7THal2P2VtjzuDd+ZVVk5Hit6do+LxVl59yI86bdMz5UxzHirnimnmPbMRNKvaZ7w7qz2kszLw72r3dpbQuTVTb27ol6q1brtz7L9yOKr88cc+LijmOYopBc/1O7afRDo/fu426OpGi42da5i5g4V2rNyKJ91VqxFdVM/0ohwbW++K6A6VemjFo3XrNMf9phaVRTTP/u3aJ/sUdgLs8Pvn+hWVcim5om+MSmZ48d7TcWYj5/i5VU/2OobG7zbs6b6vUY9G/reh5VcxEWtdw72JTH67tVPo4+utn8Aal9v7k0ndmlWdT0PVMLWdNvRzazNPyKL9muP5tdEzE/VL2LMj0l65796F6/TrGxN06jtzL8UVXKMW7/E3+PVF21PNFyPmrpmFvfYi70HQ+vubg7K6hWcXa2/b0xaxMqzM04GqV+ymjxTM2rs+yiZmKp/JnmYoBPMAAAAct7QvaS2N2Y9kV7l3tqfwa3XM28PT8eIry867Ec+js2+Y5n1c1TMU08xzMcwpt7S3egdWuuuXlafoGoXenm0qpqpt6fot+qjKvUez0+THFdUzHrpo8FPE8TE+sFzXU3tK9LOjdVVvee/tC0DKp85wsjMpqyuPfFinm5P1Uo87k73Ps76FcuUYesa5uCKZ4irTdHuUxV+r0/o1FN27Xfu13LldVy5XM1VV1zzNUz65mfbL8guur76XobTc8Mbd35XH58adh8f25fL3mid8J2ftVuU05V3c+jRPrqzdJiqI/X6K5clRqA0XbD7d/QHqRdt2dG6paFRfucRTZ1W7Vp1dU/mxGTTb5n5o5d2xsm1mY9u/Yu0X7FymK6LluqKqaqZ9UxMeUwytumdIO0t1P6DZtF/Yu9NU0K1FXiqwqLvpcS5P8/Hr8Vur9c08+YNLghr3b/bW3N2vdv7wxt2aLp2Bq+1/gMV5+m1VU282Mj0/Ezaq58FVPwfzmKpifH5RTx5zKAB6Dfm/dv8ATHaOp7n3TquPoug6bam9lZuVVxTRT7Ij21VTPERTETMzMRETMxAPfufdTu0F026MWvHvfe+ibbuTHipxszLpjIrj302YmblX1UyqY7Wfezb36oZuboHSq7k7F2lEzb/ClE+HVc2PzvHEz8Hpn2RRPj99fn4YgNqGoZWrZ1/Mzsm9mZl+ubl3IyLk3LlyqfXVVVPnMz75Behufvc+zxoF2ujD1bXdxeGePHpmkXKYq/V6ebT5envo+hlV3wTt/fdNP/eTpuJ4fvXP9ilABfLtLvYezrue9btZW5dT25XX5R+F9JvRTE+6arUXKY/XM8fOkt096ubJ6s6dOfszdej7nxYiJrq0vNt35t/NXTTPNE/NVESzCPabZ3TrOy9axtY2/q2bomrY1Xis52nZFdi9bn3010TEx9oNSgqS7Hve7appedg7V64VRqWmVzTZs7uxrPGRj+yPhVunyuU++uiIqjjmYr55i2PStVwtc0zE1HTsuxn6fl2qb+PlY1yLlq9bqjmmumqPKqmYmJiY9YPKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABS73t3atzuofVS70l0PNqt7U2tcp/CNNqr4ubqPHNXi99NmJ8ER+f6SfPinivl7vfO5sjem9dwbhzK5uZerahkZ96uqeZqru3Kq6pn66pekATI6F91V1p6y6Ria1n4+BsTRcmiLtm7uCuunJu0T6qqceimqqP9Z4OfX7nN+wFgbc1Tti9LcbdVFi5pFeqTMUZPHo6smLNycWJifKeb8WY49s8Q0UAqhxu41za8eKsjrLYtX+PyLW2qq6ef6U5UT/Y+S3h3JXUTTrFy5tnf+3Ncrp84tahYv4NVfzR4Yuxz+uYj51xYDOF1n7GPWToFbvZO8NjahjaVamedXwYjLwoj2TVdtTVFHPur8M/M4o1TV0U3KKqaqYqpqjiaZjmJhCftX91t0566YmZrOysfF6fb3mJrpvYVrwadmV+vi/YpjimZn/tLcRPnMzFfqB9h3XHyE+mX7T/AHplpVOKdjPoprnZ27Nmz+nu5MrT83WtH+GfCL+l3K7mPV6XMv36fBVXRRVPFN2mJ5pjzifXHnPawUA/4rjtO/oy/wDn9L/5lz/rX2M+sXZ22ri7k6hbP/g/ouVm0afZyfwnh5Piv1UXLlNHhs3q6o5ptXJ5mOPi+vmY50fIAd9X8lja30zxfuOcClYAHf8Apb2Cuu3WnYmmby2bsb8M7b1L0vwTN/C+BY9J6O7Xar+Jdv01xxXbrjzpjnjmPKYl2rs9d3D2idjdfemm5Nb6efAtF0fc2mahnZP4b0656Gxayrdy5X4aMiaquKaZnimJmePKJlYr3XHyE+mX7T/emWlUAADKuAA/tg52Rpmbj5mHfuYuXj3Kbtm/Zrmiu3XTPNNVNUecTExExMP4gNBnYC7WmP2qui+Plahet0740KKMLXcaniJrr4/i8mmmPVTdimZ90VU1xHlEcycZveyR2k9X7LPWjSN5af6TJ0yZ+Caxp1FXEZmHVMeko93ijiK6Z9lVMc+XMToq2du/SN/7V0ncmgZ1vUtF1XGt5eJlWp+Lct10xNM/NPn5xPnE8xPnAPcMq7VQyrgLqe5U+Sxun6Z5X3HBUrLqe5U+Sxun6Z5X3HBBP8AAAAAEAO+r+Sxtb6Z4v3HOUrLqe+r+Sxtb6Z4v3HOUrAL/ALuuPkJ9Mv2n+9MtQCv+7rj5CfTL9p/vTLBKoAAAB8P1r6v6B0G6X6/vrct6bWlaRjzdm3Rx6S/cmYpt2aI9tddc00x7PPmeIiZfcKiu+j683tV3ntjpJp2TVTgaVYjWdVooq8rmTdiabFFUe+i34qv9fHuBBPtBde909pHqdqe9d15XpMvKq8GNiW6p9Dg48TPgsWon1U08/rmZmqeZmZc4AHm6Lomobk1fD0rScHJ1PU8y7TZxsPDtVXb165VPFNNFFMTNUzPshPHpH3NnVbe2mWNQ3jrukbBtXqYqjCuU1Z2bRE/n0UTTbp8vZ6SZ98Qlf3VfY703pX0w0/qpuLT6L29tzY/wjAqv0czp2BXH8XFHPqru08V1Vevw1U0+XxuZ9gqP3P3Hu5MTT669u9VtL1XOiOabGp6Pcwrcz7vHRdvTH/pQZ699mXqL2adw29J37t+7pkX+fgmfaqi9h5cR65tXafKZ9UzTPFUcxzEctKz4brT0a2v186c6tsvd2BTnaTn25iK+I9LjXYifBetVfyblMzzE/rieYmYkMyD/AGiuq1XTXRVNFdM8xVTPExPvh911z6Qax0F6tbm2HrnFWfouXNj01NPhpyLUxFVq9THsiu3VRXEezxcPhAXed1/22MjtBbMv7C3lnfCN/bdx4rtZd2fj6pgxMUxdqn23bczTTXP8qKqKvOZq4nYzNdnzrFqXQLrLtTfemVXPSaRm0Xb9m3PHwjHn4t+zPzV25rp+bmJ9jSxousYe4dGwNV0+9Tk4Gdj28rHvU+q5brpiqmqPmmJiQea5717637d7O/SvW99bmuzTgadb/isaiYi7l36vK3Yt/wA6ury90RzM+UTLoSlnvg+0Pf391qxOmWm5VU6Bs+3TXl26J+Je1G7RFVVU+/0duqiiPdNV2PaCI/aD7QG7O0p1K1DeW7sz0uVfn0eLhW5n0GDYiZ8Fm1TPqpjn1+uqZmqeZmZc1ABKPoT3bfXDrxgY2q4W37W1tAyIiu1qm5btWLRdpn+VRaimq7VEx5xV4PDPslNHuzO710jSds6R1e6laVb1HWtQopy9A0bNtxVawrE+dvJuUT5VXa44qoifKimaavypjwWaAqY0buONYv41NWrdXsHCyOPjUYeg15FMT81VWRbmfsep3b3Ie9MHGrr2z1L0LWb8RzTa1PAvYMVfN4qKr3H2LfQGcTrh2L+sXZ6ou5W8dmZlnR6J4/DOBMZeFx7Jqu2+Yt8+yLnhn5nEmqS/Yt5Nm5ZvW6btq5TNFduumKqaqZjiYmJ9cSr87YHdObU6o2c3c3SejE2XuzibtzR4jwaZnVevimmI/wCj1z7JpjwT5c0xzNQOZdxj/ls/Yn/HrVFb/c/9Fd+9F9S60YO+toaxta9duaTax69Tw67VrJm1OdFybNyY8F6mnx0c1UTVHxqfPzhZAD8Xr1vHtV3btdNu1RTNVVdc8RTEeczM+yFC/eJdtXN7UHUm9omhZtdvproGRVb02xRM00592Oaasy5Ht584oifyaJ9UTVUsg71brze6N9mPM0fTMmrH13eV/wDA1mu3VxXbxvD4squPmmji3/ron2KIgAWB91B2O9O60bvz+pe8cCjP2ptrIpsYGDkUeK1m6hxFfNcT5VUWqZoqmmfKaq6OeYiqJDm3Z37sPrF1+0fE1+5jYmydtZVMXLGfr810Xci3PqrtWKYmuYmOJia/BTVE8xMpGZHcb6lTgTXY6w4tzN48rNzbtVFvn3eOMmZ+vwrYYjiH+gz2dpPu+urnZkwbusa5pePr21rcxFWvaFXVfsWeZ8vTUzTTXa9kc1U+HmYiKplGpqhz8DG1TByMLNx7WXh5Fuqzex79EV27tFUcVU1Uz5TExMxMT6+VCPeP9kqx2Xes9F3b9iq1sbc1FzN0miZmYxa6ZiL2LzPnMUTVTNPP8i5THMzEyCJixPuq+23ldOt34PR7eOoTc2jrV/0WiZORVz+Ds2ur4tqJn1WrtU8ceqmuYnyiqqVdj9Wrtdi7RdtV1W7lFUVU10TxNMx6pifZINUw4T2IuutfaI7NOz92Zl702t02J0/Vpn1zl2PiXK593jiKbnHuuQ7sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMN1h2TkdNuq+8dqZVubV7RtXysCaZjjmLd2qmJj5piImJ9sTD5BaJ3unY61GncFfXDaeBXl6fkWqLO5sfHo5qx7lFMUW8viP5E0RTRXP8maaap58UzFXYP3ZvXMa9bvWblVq7bqiui5RMxVTVE8xMTHqmE3+ine69ZOmWBi6ZuW1p3UTTLFMUU3NViqzn+GPVHwij8r+lcorqn2yg6AuY2D31XS7W/R2t2bP3Hta/V+VcxJtahj0frqibdf2W5SZ6cdunoN1VrtWtC6m6JRl3ZimnE1W7Vp96qr82mjIiiap/o8s5wDVNbuUXrdNy3XTXbriKqaqZ5iYn1TEv0zWdGu1N1V6BZlq7sjeup6Ti0VeKrTK7vp8K57/Fj3Obc8+/jmOfKYWr9jXvVtu9b9TwNndSMTF2dvLJqps4mfYrmNN1C5PlFEeKZmzcmfKKapqpqnyiqJmKZCfgACAHfV/JY2t9M8X7jnJ/oAd9X8lja30zxfuOcClYAF/3dcfIT6ZftP8AemWlUir3XHyE+mX7T/emWlUAADKuACY3SDsiT147Ae6N9bcw5vb42juzNuejtU83M/T4wsKu7Z49c1UT4rlEf+ZTETNcIcrqe5U+Sxun6Z5X3HBQq7z7sh/i9dW53Zt3C9DsPdl6u/j0WqeLeBmflXcby8qaZ866I8vKaqYj+LkEK1mPdFdsH+DGu/4Et15vh0rVLtV7beReq8sfKq87mLzPqpu+dVMfn+KPObkcVnP74Odk6XnY+Zh37mLl49ym9Zv2a5ort10zzTVTVHnExMRMTHuBqhZV2g7sB9rPG7VfRfHzM+9bp3vocUYWu41PETXXx/F5NNMeqm7FMz7oqiumPKI5z4gLqe5U+Sxun6Z5X3HBUrLqe5U+Sxun6Z5X3HBBP8AAAAAEAO+r+Sxtb6Z4v3HOUrLqe+r+Sxtb6Z4v3HOUrAL/ALuuPkJ9Mv2n+9MtQCv+7rj5CfTL9p/vTLBKoAAABm67Y+/LnUrtTdUNfruTet3deycaxXVPPNixV6Cz/u7VDSKyya5qFer63qGdcqmu5lZFy/VVPtmqqZmf7QeE+26IdP56rdY9k7O+NFGuaziYF2qn10W7l2mmur6qZqn6nxKTfdpaZb1btwdLrFyPFTRk5l+I+e3g5FyP7aIBoHwsOxp2HYxMW1Rj41i3TatWrccU0UUxxTTEeyIiIh/YAAAVEd9r0xsaXv7p9v3GtRTXq+Df0rMqpjjmvHqprtVT75mm/XHPutx7oVnLm++x06i72dNl58x/GWN127FM+6K8PJqn/wDnCmQBoN7trflzqB2MenWTkXZu5Wm413SLvinnwxjXq7VqP/aptM+S7fuZdRrzeybq1mqqaqcTdeZYpj82Jx8W5x9tyZ+sE6dRz7GlaflZuTX6PGxrVV67XP8AJopiZmfsiWYHqNvTL6j9QNy7rz6qqs3W9SyNRu+KeZiq7cqrmPq8XH1NH/aT1KvRuzr1T1C1VNF3E2rqt+mqPXE04l2qJ/sZngHZexz0dtdee0tsPZmXb9LpmZnxf1Cj2VYtmmq9epmfZ4qLdVMT76ocaTu7mfSbWo9rTU8i5TzXgbWzMm3Puqm/jWufsu1faC7e1aosWqLduim3boiKaaKI4imI9URHsh+xxTtmda9c7O3Zs3h1C23i6fm61o/wP4PY1S3Xcx6vS5lixV46aK6Kp4pu1THFUecR648pDtYpW/x1fW//ADW6f/7Ozv8AnD/HV9b/APNbp/8A7Ozv+cBdSKVv8dX1v/zW6f8A+zs7/nD/AB1fW/8AzW6f/wCzs7/nAXUiFXdvdtbfHbB/wifwy0rb+mfwd/B3wT8BY9+14/hHwrx+k9Leuc8ego4449dXPPlxNUFMnfU78ua11+2jtWi5NWJoWgxkzRz5U38m9X4/L+hZsq8kt+9X1CvN7cO+rNVU1U4mPptimPzYnBsXOPtrmfrRIAaLuwn0ys9J+yZ020ai1FrJyNKt6pl+Xxpv5Uenrir3zT6SKP1URHsZ0WpnQdMt6Loenafajw2sTGt2KY90U0xTH/0DzwAEMu9n6Y2N+dkPWNYi1FeobVzsbVbFcR8bwVVxYu08+7wXpqmP/Dj3JmuOdsrTqNU7JvWCzcjxU07U1K/EfPbx67kf20wDN0AC2PuQN+XL+gdUNl3bs+ixsnE1fGt8+XNymu1en/c2FoqmTuTtRrtdoveuDFUxbv7UuX5p980ZmNET/vJ+1c2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD+eRj2suxcsX7dF6zdpmiu3cpiqmumY4mJifXEx7FeHag7n/afUXNzNwdKdSsbG1m9M3K9EyqKq9Lu1z+ZNMTXj8z7KYrp9URTTCxMBnO6udhnrh0VuX69w9P8AVL+nWuZnVNIt/D8Xw/nTXa8Xgj+nFM/M4TVTNFU01RNNUTxMTHnEtU74XfvQnpx1Riud3bF29uO7VHHp9R0yzdvR+q5NPipn54mAZkRe51D7pbs+73pu16Zo2q7My64mfS6HqNc0+L3+jv8ApKYj5qYp+pXX2yu7V3h2WtGu7t0vVKN5bEouU272fbsTZycCapiKPT2uZjwzMxTFymeOZjmKeY5CG5EzE8x5SAL3O637Tmf2gug17SNxZlWduzZ923p+VlXavFcycaumZxr1c+uauKLlEzPnM2vFMzNUpmKau5M1y/j9f986PTVMYuXticuun2TXay7FFM/Zfr+1cqAgB31fyWNrfTPF+45yf6AHfV/JY2t9M8X7jnApWABf93XHyE+mX7T/AHplpVIq91x8hPpl+0/3plpVAAAyrgAup7lT5LG6fpnlfccFLntB9D9B7RfSTX9ibgoinF1Gz/EZUUxVXh5FPnav0fPTVxPHtjxUz5VSiN3KnyWN0/TPK+44Kf4MwPVPppr3RzqFr2y9zYs4et6NlVY2RR5+Grjzpron20V0zTVTPtpqiXyq5zvaeyH/AITthU9WdsYXj3RtnHmnVbNmn42Zp0czNfl66rPM1f0Jr9fhphTGDs/ZH7Smr9lnrRpO8cD0mTpcz8E1jTqKuIzMOqY8dHu8VPEV0z7KqY58uYnjAALqe5U+Sxun6Z5X3HBUrLqe5U+Sxun6Z5X3HBBP8AAAAAEAO+r+Sxtb6Z4v3HOUrLqe+r+Sxtb6Z4v3HOUrAL/u64+Qn0y/af70y1AK/wC7rj5CfTL9p/vTLBKoAAABlq3RpVzQtzavpt2nw3cPMvY9dPumiuaZj+xqVZze3T08udMe1t1P0aqzNmxd1i7qWNTx8X0OVxkUeH5oi74f9GY9gOEpLd23q9vRO250tyLtUU015mTjRMzx53cO/aj+2uEaX1PSnfd/pf1O2nvDGpmu/oOq4upU0U/y/RXaa5p+uKZj6wafx6/b+u4O6dB03WdLyKMvTNRxreXi5Fueabtq5TFdFUfNNMxP1vYAAArr77PV7dnoBsbS5qiLuTueMmmn2zFrEv0zP++j7VNaxrvqOq9jcfWDZ2wsS/F2nbWnXMzMpon8jIyppmKKo98WrVur9V1XKAu87mvSrmn9kjOyK6eKc/c+ZkUT74izj2ufttz9ikNog7vnp5c6Z9jzpppeRZmzmZWnzqt+Ko4q8WVcqyKfF88UXKKePZ4QdI7Q2k3Nf6A9S9MtU+O7m7Y1PGop9814tymI/tZl2qPJxrWZjXce/RF2zdom3XRV6qqZjiYn6mYjq3sLJ6W9Ud27PzKKqMjQ9UydPnx+uqLdyqmmr54mIiYn2xMA+TTg7njcVrRO2BGHcqimvWNv52Dbif5VVNVq/wAR9ViqfqQfdF7OvVm90L647K33aprro0XUrd/It2/yrmPPNF+iPnqtVXKfrBpjRV70f5CfU39mfvTESc0LXMDc2iafrGlZVvO0zUMe3l4uVZnmi9arpiqiumfdNMxP1vRdUulu2OtOxNT2bvLTPwztvUvRfC8L4Rdsek9Hdou0fHtVU1xxXbonyqjnjifKZgGYIX/f4rjsxfoy/wDn9U/5lz/tC93D2dtjdAupe5NE6efAta0fbOp6hg5P4b1G56G/axbly3X4a8iaauKqYniqJiePOJgFIIALVO4x/wAtn7E/49aoqr7jH/LZ+xP+PWqAoU717Srmn9tzeV+uninOw9OyKJ98Rh2rXP225+xEJZB32XTy5pfV7Ym9bdmYxdY0evTbldMeU3sa7Nfn880ZFMR74o+aVb4DUptbV7e4NsaRqlqqKrWdh2cmiqJ5iYroiqP/ALZa2hju8erFjq72Rtg51N6Lufo+HToOdRzzVRdxYi3T4vnqtRar/wBOASQAAcV7aur29E7JHV7Iu1RTTXtjPxomffds1Woj7a4dqQb737qvY2P2WKtrUX4p1PeGo2cOi1E8V/B7NdN+9XHzRVRZon/zYBR2ACxLuTNKuXu0BvnUop5tY+2Kseqr3Tcy7FUR/up+xcsrN7kbp5c0/YHUfe9+zMU6pqGPpWNXVHn4ce3VcucfNM5FEc++j5pWZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOVdrH5LHWT6Gaz9xvOquVdrH5LHWT6Gaz9xvApW7IXeG9QOytcs6NVP8LdhzXzc0DOvTE43M81VYt3zm1M+uaeJonmfixM+Jb92fe3R0f7R+NjWtu7ns6br93iKtva1VTi50Vfm0UzPhu/rt1VfPwzrkTNMxMTxMeqYBqoGcrpl24eunSKzZx9udSdZpwbXEUYWpV059imn82mi/FcUx/R4d40bvlOvWmWKbeTg7O1eqPXdzNLvU1Vfr9Ffoj7IBd6jv2/+oW3On/ZK6kfwhyse3VrGj5Ok6fjXao8eRl3rc0Wot0+uqaaqornj1RRM+xWBuHviOv+tWK7eH/BbQK6o4i7p2k1V1U/PHp7tyPtiUUuqvWjfHW/cP4c33ufUNy6lETTbrzLnxLNM+um1bjii3T7fDRTEfMD4sHv9g7C1/qhvHStrbX0y/q+u6peixi4linmquqfXMz6qaYjmZqniKYiZmYiJBY13Iewcm/vXqTvau3NOJiafY0a1cn1V13bnpq4j+jFi3z/AE4W3uM9kbs56f2XOh2ibJxblvK1GnnM1bOtxMRlZtyI9JXHP8mIiminnz8NFPPny7MAgB31fyWNrfTPF+45yqv8bHrf+mTqB/WjO/vXz+9et/UXqVpVrTN37+3RurTbV6Mm3h63rOTmWaLsU1Uxcii5XVEVRTXXHi454qmPbIPigAX/AHdcfIT6ZftP96ZaVTMttbtC9U9jaFi6JtvqXvDb+i4vi+D6dpevZWNj2fFVNdXgt0XIpp5qqqqniPOapn1y7B2ZO031h1/tJdKNM1PqvvjUdNzd26TjZWHl7jzLtm/arzLVNduuiq5MVU1UzMTTMTExMxINBIAMq4ALqe5U+Sxun6Z5X3HBT/QA7lT5LG6fpnlfccFP8H4u2qL9qu1dopuW66ZpqorjmKon1xMe2FBHeLdkivsv9aLt/RsWqjYW5Krmbo9dMfExquebuJM/+HNUTT76KqPOZipf05D2q+zro/ag6L61snU/R2My7T8J0vUK6eZws2iJ9Fd9/HnNNUR66K6o9vIM2o91vXZusdPN3axtncGFc07WtJyrmHl4tz127lE8T5+2PbEx5TExMeUvSgLqe5U+Sxun6Z5X3HBUrLqe5U+Sxun6Z5X3HBBP8AAAAAEAO+r+Sxtb6Z4v3HOUrLqe+r+Sxtb6Z4v3HOUrAL/u64+Qn0y/af70y1AIDVQMq4DVQMq4DVQqx76Ls93smztnrHpWLNdvHojRNbqoj8imapqxbs/N4qrluap9s2oWnPQb92NonUzZms7V3Hg0ajoer41eJl41z+VRVHrifZVE8TFUecTETHnAMuw7r2vuybubsmdTcjQdUou523suqq7ouuRb4t5tjn1TPqpu0cxFdHsniY+LVTM8KBbb3Unbg0vL2xgdFN8albwdVwapt7azsqvw0ZVmZ5+CTVPquUzM+CJ/KpmKY86YiqztlXpqmmqKqZmJieYmPYlT0j7zTr50h02xpljdNrdGl2KYptYu5sf4ZNER6oi9zTdmOPLia5iPZwC/9xztR9qHaXZX6bZe5dx5Nu9qNyiqjStFouRGRqGRx5UUx64oiZiaq+OKY98zETU7ujviuv24NPrxsGjam27tUcRl6XpVdd2n54+EXbtPP+iiB1B6lbq6r7lv7g3hr+fuPWb0cVZeoX5uVRT7KaefKmmOZ4ppiIj2QB1I6g611X37r28NxZPwvWtay68zJuR5U+KqfyaY9lNMcU0x7IpiPY+bHm6Jomobk1fC0rScLI1LU829TYxsPFtzcu3rlU8U0U0x5zMzPERAOqdknoLmdpHr5tbZVi1XVp1/IjJ1W9RzHoMG3MVX6ufZM0/Ep/nV0x7WkLExbODi2cbHtUWMezRFu3atxxTRTEcRER7IiIRP7vHsX2uyn01u52u27N/qHuCii7ql6jiqMO1HnRiUVe2KZnmuY8qqvfFNMpagKbe+O7Ot/Z/VXTerGl4k/gTdFujD1K5RHxbWoWqOKZn3eks0U8e+bVyZ9a5J8V1m6Q7d67dNdc2RunF+E6RqtibdVVPHpLFyPOi9bmfVXRVEVRPvjz5iZgGYwde7TvZj3d2WOpGVtfc+PVdxa5quaZrFq3MY+o2OfKuifZVHMRVRzzTPl5xMTPIQWLd3H3j2J0Z07F6YdT8q7GzormNI1yYm5OlzVPM2bsRzM2ZmZmKo5miZmJiaZ+JcHoWv6ZujSMXVdG1HF1bS8uiLuPm4N6m9ZvUT6qqK6ZmKo+eJZZnQulPaF6k9Dsqq9sXemr7bprq8dzGxciZxrtXvrs1c265/pUyDTM5V2sfksdZPoZrP3G8qJ233wfaC0OxRbzb22dw10xxN3UtJ8FVXzz6C5aj7IfjqP3uHWTqbsHce0dS0HZeJpuvabk6Xl3cLAyqb0Wb9qq1XNE15NURV4a54mYnz9gISgAtU7jH/AC2fsT/j1qiqvuMf8tn7E/49aoCLfeQdnu92g+zJrWLpeLOVubb9ca3pduiOa7tVqmqLtqPbM12qrkRT7aooZ+2qhSv3nfYUy+j+7c/qlsnTqruw9Yvzd1HExbflo+VXPxpmI9Vi5VPNM+qmqZo8omjkK/0zO7U7ZuP2Yeo+XoO6siu30/3NXRTl3uJqjT8mnyt5PEfyeJ8NfHnx4Z8/BETDMBqf03U8PWdOxs/T8qznYOVbpvWMnGuRct3aKo5pqpqjmKomJiYmPW8lnF6FdtDrB2c7FOFszd+RY0WKpqnRs+inKwuZnmfDbuRPo5mfOZtzTM+2Ug8jvm+u97AnHo0nZVi9xx8Lt6ZkTc/XxVkzTz/ogub3rvfQenG1tR3JubVcbRND0+1N7Jzcuvw0W6Y/+5mfKKY5mZmIiJmYhn57cnary+1l1pydfs0XsTaum0TgaFg3vKqjHirmbtceqLlyr40+6PDTzPh5fF9bu0/1P7RWdbv7+3bm63Ys1+OxgR4bOJYn1c0WLcU0RVxPHi48Ux65ly0B5ekaTma/q2FpmnY1zN1DNv0Y2NjWafFXdu11RTRRTHtmZmIiPneItW7qnsKZeDm4PW3f2nVY800ePbGlZdviuZqj/r1dM+qOJmLcT6+fH5cUTIT57KnRGz2d+gOz9i0xbnN0/Di5qF23PMXcy5M3L9UT7Y8dVURP5tNPudZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB8t1V2NT1P6X7w2bXmTp1G4tGzNInMpt+kmxF+xXa9J4OY8Xh8fPHMc8ccw+pAUWdZu6W639M7l7J27iYXUTSKZmab2i3Yt5UU/z8e5MTz81ubiI27ti7k6f6pVpu59v6ptzUKeecXVcO5jXfL+bXES1GPX65t7Stz4FWDrOmYerYVf5WNnY9F63P66aomAZZxo13H2GugO6blVed0l2vbrqnmZwMGnD5n/U+B8dkd2J2Zcmqaq+mNqJnz/i9Z1GiPspyIBn7f7RRVdrpoopmuuqeKaaY5mZ90NCGl9212bNHuRXY6W4NyqPZlZ+ZkR9ly9VDrmxeg/TfpjXTc2lsPbm3L9P/wCxpul2bN6f13KafFP1yCjDoF3c3Wrr3lY1+ztq9tLb1yYmvW9x0VYtvwe+3amPSXeY54mmnw8+uqPWuC7JXYh2F2SdErnRbVWt7sy7cW8/cmdbiL92PKZt2qY5iza5jnwxMzPEeKqriOJEAAAMq41UAMq41UAMq7qvZO+VP0b+mejffrLSmAAAyrjVQAgB3KnyWN0/TPK+44Kf4AAArL73rsh/wk0OnrbtfC8WqaZbox9x2LNPnfxY+LbyuI9dVvyoqn8yaZ8otyqLaqAGVddT3KnyWN0/TPK+44Kf4AAAAAACAHfV/JY2t9M8X7jnKVmqgBlXGqgBlXGqgBlXGqgAAB8V1e6ObQ67bIzNp720azrWjZPxvBc5puWbkRPhu2q4+NRXHM8VRPtmPOJmJqA7TXdI9R+l2Zl6r029J1E2vE1V041qKaNUxqfzarXlF7jyjm18afP4lK7QBll1vQtS21qd7TtX0/L0rULE+G7iZtiqzdtz7qqKoiY+uHgtQm9Omm0eo+JGLuza2jbmx4iaabWr4FrKppj5ouUzx9Timt93R2cdfuzcyulel2qp9mFkZOJT9lq7TAM8w0EYfdldmfBuRXb6YY9UxPPF7VtQux9lWRMOn7G7LfSDpteov7a6a7Y0rLomJpy7emWqsimY912qJr/tBRJ0G7DPWPtD5WPXtzaWTgaJdmPFr2tU1YmDTTP8qmuqObv6rdNc/MuA7HHd7bG7KNi3rNyuN17/ALluaLuv5VnwU40VRxVRjW+Z9HEx5TVMzXVzPnET4UrAAAAAHwvWTolszr7srJ2rvjRLOs6Tdnx0ePmm7j3OJiLtq5HxqK45nzifVMxPMTMTUf2ke6F6i9OcrK1Tple/whbc5qrpwpmizqmPT7qqJmKb3EeXNuYqmf5ELqgGWzc209c2Vq13S9w6Nn6FqVr/APJh6li1496n9dFcRMfY9U1H7o2Xt7fGB8B3HoWma/hef/RtUw7eTb8/X8WuJhxXcHd+9nfc1yqvM6UaFZqqnmY0+m5hR9UWK6Ij6gZ2hoDjuwezLTc8f+DG34vdOtalMfZ8I4fUbe7BPZ72xcorw+k23b1VPq/CFirNj64v1VxP1gzwaRo2oa/n2sHS8HJ1LNuzxbxsSzVduVz81NMTMpH9Le7d7QPVSuzcx9h5O28C5xzm7mrjT6aIn2zbr/jpj+jblfntjZW3tk4XwPbug6ZoGJ/3Gl4dvGt/+mimIe6BEfu++w/qXY20bdtes7oxdw6tub4FN+zg41VuxifB/T8RTXVPiueL08+c00ceGPJLgAHi6npeHrem5Wn6hi2c7AyrVVjIxcm3Fy1et1RxVRVTPlVTMTMTE+U8vKAVW9rTue7mXm5u5uh2RZt03Jm7c2fqF7wRTPtjFv1TxEfzLkxEefFfHFMVqdROlG8ukms16VvPbGqbZz6appi1qWLXaivj20VTHFcfzqZmJ97T68DW9B0zcunXMDV9OxNVwLn5eLm2Kb1qv9dNUTEgyzDRZufsFdnzd125dz+k+3rVdc81Tp1irB8/1WKqOHy9Pdh9mWi76SOmNrxe6da1GY+z4RwDP26D0l7P3UbrpqlGDsXZ+qbirmrwVZGPYmnGtT/4l+ri3b/0qoX8bS7EXQXZF63e0rpTtn01v8i7nYUZldM++Jv+OYn5/W7Rg4GNpmJaxMPHtYmLZp8NuxYoiiiiPdFMeUR+oFdnY+7pPRem+dg7t6vX8TdWv2ZpvY+3seJr0/Frjzib1U8enqjy+LxFETz+X5TFjMRFMRERERHlEQ/0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//9k='; ?></textarea>           
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_registrar" id="btn_registrar">Guardar</button>
                            <button class="btn btn-sm btn-white" type="reset" id="btn_reset" name="btn_reset">Limpiar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
