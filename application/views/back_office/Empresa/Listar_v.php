<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-10-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>

    <head>
        <style>
            /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */

            #map {
                height: 100%;
            }

            /* Optional: Makes the sample page fill the window. */

            html,
            body {
                height: 100%;
                margin: 0;
                padding: 0;
            }

            /*select2*/

            .modal .select2 {
                width: 100% !important;
            }

            .select2-container--open {
                z-index: 2060;
            }

            .swal2-container {
                z-index: 2061;
            }
        </style>
    </head>
    <div class="wrapper wrapper-content animated fadeInRight">

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Busqueda</h5>
                        <div class="ibox-tools">
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" class="form-inline" id="frm" name="frm">

                            <!-- Text input-->
                            <div class="form-group">

                                <div class="col-md-3">
                                    <input id="txt_rut_empresa" name="txt_rut_empresa" type="text" placeholder="78 895 485 - 5" class="form-control input-md">
                                    <p class="help-block">Rut</p>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">

                                <div class="col-md-3">
                                    <input id="txt_nombre_empresa" name="txt_nombre_empresa" type="text" placeholder="Santander" class="form-control input-md">
                                    <p class="help-block">Nombre</p>
                                </div>
                            </div>
                            <!-- Select Basic -->
                            <div class="form-group">
                                <div class="col-md-3">

                                    <select id="cbx_holding" name="cbx_holding" class="select2_demo_3 form-control">
                                        <option></option>
                                    </select>


                                    <p class="help-block">Holding</p>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="btn_buscar_empresa"></label>
                                <div class="col-md-3">
                                    <button type="button" id="btn_buscar_empresa" name="btn_buscar_empresa" class="btn btn-outline btn-success">
                                        Buscar
                                    </button>
                                </div>
                            </div>
                        </form>



                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Listado de empresas</h5>
                        <div class="ibox-tools">

                            <button type="button" id="btn_nuevo" class="btn btn-w-m btn-primary">Nuevo</button>
                            <!--                         <button type="button" id="btn_carga" class="btn btn-w-m btn-w-m">Carga</button> -->

                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">


                            <table id="tbl_empresa" class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Rut</th>
                                        <th>Razon Social</th>
                                        <th>Giro</th>
                                        <th>Direccion</th>
                                        <th>Holding</th>
                                        <th>Ventas</th>
                                        <th>Contacto</th>
                                        <th>direcciones</th>
                                        <th>Estado</th>
                                        <th>Opción</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Id</th>
                                        <th>Rut</th>
                                        <th>Razon Social</th>
                                        <th>Giro</th>
                                        <th>Direccion</th>
                                        <th>Holding</th>
                                        <th>Ventas</th>
                                        <th>Contacto</th>
                                        <th>direcciones</th>
                                        <th>Estado</th>
                                        <th>Opción</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- MODAL TABLA CONTACTOS EMPRESA -->

    <!-- Modal -->
    <div class="modal fade" id="modal_contactos_empresa" role="dialog"data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">

                    <div id="contactos_empresa">
                        <button class="btn btn-success" id="btn_nuevo_contacto">Agregar Contacto</button>
                        <table class="table table-responsive table-striped" id="tbl_contactos_empresa">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Cargo</th>
                                    <th>Departamento</th>
                                    <th>Email</th>
                                    <th>Teléfono</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div id="agregar_contacto_empresa">
                        <h4>Agregar Nuevo Contacto</h4>
                        <form class="form-horizontal" id="frm_agregar_contacto_empresa">
                            <input type="hidden" id="id_empresa" name="id_empresa">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nombre</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="nombre_contacto_empresa" name="nombre_contacto_empresa" placeholder="Ingrese Nombre de Contacto"
                                        required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tipo Contacto</label>
                                <div class="col-lg-6">
                                    <select id="tipo_contacto_empresa" name="tipo_contacto_empresa" class="select2_demo_3 form-control" required>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cargo</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="cargo_contacto_empresa" name="cargo_contacto_empresa" placeholder="Ingrese cargo">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Departamento</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="departamento_contacto_empresa" name="departamento_contacto_empresa" placeholder="Ingrese departamento">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nivel</label>
                                <div class="col-lg-6">
                                    <select id="nivel_contacto_empresa" name="nivel_contacto_empresa" class="select2_demo_3 form-control" required>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Correo</label>
                                <div class="col-lg-6">
                                    <input type="email" class="form-control" id="correo_contacto_empresa" name="correo_contacto_empresa" placeholder="Ingrese correo electrónico">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="telefono_contacto_empresa" name="telefono_contacto_empresa" placeholder="Ingrese teléfono">
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success">Guardar</button>
                                <button type="button" class="btn btn-danger" id="btn_cancelar_contacto">Cancelar</button>
                            </div>

                        </form>
                    </div>

                    <div id="editar_contacto_empresa">
                        <h4>Editar Contacto</h4>
                        <form class="form-horizontal" id="frm_editar_contacto_empresa">
                            <input type="hidden" id="id_contacto" name="id_contacto">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nombre</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="nombre_contacto_empresa_editar" name="nombre_contacto_empresa_editar" placeholder="Ingrese Nombre de Contacto"
                                        required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tipo Contacto</label>
                                <div class="col-lg-6">
                                    <select id="tipo_contacto_empresa_editar" name="tipo_contacto_empresa_editar" class="select2_demo_3 form-control" required>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Cargo</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="cargo_contacto_empresa_editar" name="cargo_contacto_empresa_editar" placeholder="Ingrese cargo">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Departamento</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="departamento_contacto_empresa_editar" name="departamento_contacto_empresa_editar"
                                        placeholder="Ingrese departamento">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Nivel</label>
                                <div class="col-lg-6">
                                    <select id="nivel_contacto_empresa_editar" name="nivel_contacto_empresa_editar" class="select2_demo_3 form-control" required>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Correo</label>
                                <div class="col-lg-6">
                                    <input type="email" class="form-control" id="correo_contacto_empresa_editar" name="correo_contacto_empresa_editar" placeholder="Ingrese correo electrónico">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Teléfono</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control" id="telefono_contacto_empresa_editar" name="telefono_contacto_empresa_editar" placeholder="Ingrese teléfono">
                                </div>
                            </div>
                            <div class="form-group" style="align-content: center">
                                
                                    <button class="btn btn-success">Guardar</button>
                                    <button type="button" class="btn btn-danger" id="btn_cancelar_contacto_editar">Cancelar</button>
                                
                            </div>

                        </form>
                    </div>
                </div>
                <div class="modal-footer" >
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal inmodal fade in" id="lista_direcciones" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">Direcciones</h4>
                    <small class="font-bold" id="direc">TARAN!!</small>
                </div>
                <div class="modal-body">

                    <div class="table-responsive" id="div_tbl_direccion_empresa">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <table id="tbl_direcciones_empresa" class="table table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Direccion</th>
                                            <th>Tipo</th>
                                            <th>Fecha Registro</th>
                                            <th>Ver</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Direccion</th>
                                            <th>Tipo</th>
                                            <th>Fecha Registro</th>
                                            <th>Ver</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </tfoot>
                                </table>




                                <input type="button" value="Nueva Dirección" id="agregar_direccion" class="btn btn-w-m btn-primary">
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive" id="div_agregar_direccion" style="display:none">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Nueva Dirección
                                    <small>Formulario para agregar una nueva dirección de esta Empresa</small>
                                </h5>
                            </div>
                            <div class="ibox-content">
                                <form method="post" class="form-horizontal" id="form_agregar">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Direccion</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="direccion" required="required">
                                        </div>
                                        <input type="hidden" id="id_empresa_direccion">
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Tipo Direccion</label>

                                        <div class="col-sm-10">
                                            <div class="selector-pais">
                                                <select class="form-control" id="tipo"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Fecha de Registro</label>

                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" value="<?php echo date('Y-m-d H:m:s') ?>" readonly="readonly" id="fecha">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <input type="button" value="Cancelar" id="cancelar_direccion" class="btn btn-w-m btn-primary">
                        <input type="button" value="Agregar" id="Agregar_form" class="btn btn-w-m btn-primary">
                    </div>

                    <div class="table-responsive" id="div_mapa_ql" style="display:none">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>
                                    <span id="dirr">asd</span>
                                </h5>
                            </div>
                            <div class="ibox-content">
                                <div id="map" style="width: 100%; height: 400px"></div>
                                </form>
                            </div>
                        </div>
                        <input type="button" value="Cancelar" id="cancelar_mapa" class="btn btn-w-m btn-primary">
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade in" id="modal_venta_empresa" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="nombre_venta_empresa"></h4>

                </div>
                <div class="modal-body">

                    <div class="table-responsive" id="div_tbl_direccion_empresa">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <table id="tbl_venta_empresa" class="table table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th>N° de ficha</th>
                                            <th>Nombre del curso</th>
                                            <th>Tipo de venta</th>
                                            <th>Fecha de venta</th>
                                            <th>Total venta</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>N° de ficha</th>
                                            <th>Nombre del curso</th>
                                            <th>Tipo de venta</th>
                                            <th>Fecha de venta</th>
                                            <th>Total venta</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>