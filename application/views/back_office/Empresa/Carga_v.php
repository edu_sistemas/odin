<?php
/**
 * Listar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-12-02 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:	2016-10-11 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Cargar Archivo</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content"> 

                    <form  name="frm_carga_empresa" id="frm_carga_empresa">

                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">

                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Seleccionar Archivo</span>
                                <span class="fileinput-exists">Cambiar</span>
                                <input type="file" name="file_carga_empresa" id="file_carga_empresa" accept=".xls,.xlsx">
                            </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">
                                Eliminar
                            </a>
                        </div>  


                        <button id="btn_carga"  type="button" class="ladda-button btn btn-primary" data-style="expand-right"> 

                            <i class="fa fa-upload"></i> Subir  

                        </button>



                        <a href="<?php echo base_url() . "assets/plantillas/carga_empresas.xlsx"; ?>" target="_blank" >
                            <button type="button" id="btn_descarga" class="btn btn-file pull-right"><i class="fa fa-download"></i>Descargar Formato de  Carga </button>
                        </a> 

                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="div_carga_empresa"   >
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Datos Cargados</h5>

                    <div class="ibox-tools">                        
                        <a class="collapse-link" hidden  id="a_tab_tabla_carga">
                            <i class="fa fa-chevron-down"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content"  > 
                    <table id="tbl_empresa_carga" class="table table-striped table-bordered table-hover dataTables-example">
                        <thead>
                            <tr>
                                <th>Rut</th> 
                                <th>Razon Social</th>
                                <th>Giro</th>
                                <th>Dirección</th>    
                                <th>Descripción</th>   
                                <th>Mensaje</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>                                
                                <th>Rut</th> 
                                <th>Razon Social</th>
                                <th>Giro</th>
                                <th>Dirección</th>    
                                <th>Descripción</th>   
                                <th>Mensaje</th>
                            </tr>
                        </tfoot>
                    </table>

                    <div class="ibox-tools">

                        <button type="button" id="btn_aprobar_carga" class="btn btn-primary">Aprobar Registros</button>
                        <button type="button" id="btn_cancelar" class="btn btn-danger">Cancelar</button>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
