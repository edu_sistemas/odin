<?php
/**
 * Editar_v
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-10-14 [Marcelo Romero] <mromero@edutecno.com>
 */
?>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Editar Empresa</h5>
                <div class="ibox-tools">
                </div>
            </div>

            <div class="ibox-content">

                <form class="form-horizontal" id="frm_empresa_registro"  >
                    <input type="hidden" id="id_empresa" name="id_empresa"                                   
                           value="<?php echo $data_empresa[0]['id_empresa']; ?>">
                    <p>Modifique Empresa</p>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">RUT:</label>
                        <div class="col-lg-4">
                            <?php
                            $rut = $data_empresa[0]['rut_empresa'];

                            $rut = explode("-", $rut);

                            if ($rut[0] == 'undefined') {
                                $rut[0] = "";
                            }
                            ?>
                            <input type="number" class="form-control required"
                                   id="rut" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el RUT de la empresa"
                                   placeholder="Ej. 123456789"
                                   name="rut"
                                   maxlength="8"
                                   value="<?php echo $rut[0] ?>" readonly="readonly"
                                   class="form-control" onkeypress="return solonumero(event)">

                        </div>
                        <p style="width: 0" class="col-lg-1">-</p>
                        <div class="col-lg-1">
                            <input type="text" class="form-control required"
                                   id="dv" 
                                   maxlength="1" 
                                   requerido="true"
                                   mensaje ="Debe Ingresar el DV de la empresa"
                                   placeholder="Ej. 0"
                                   name="dv" readonly="readonly"
                                   value="<?php echo $rut[1] ?>"
                                   class="form-control" >
                        </div>
                        <div class="col-lg-3">
                            <p id="out_print" style="color: red"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre Fantasía:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="nombre_fantasia" requerido="true"
                                   mensaje ="Debe Ingresar el nombre de fantasía de la empresa"
                                   placeholder="Ej. 'Edutecno'"
                                   name="nombre_fantasia"
                                   class="form-control"
                                   value="<?php echo $data_empresa[0]['nombre_fantasia']; ?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Razon Social:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="razon" requerido="true"
                                   mensaje ="Debe Ingresar La razón social de la empresa"
                                   placeholder="Ingrese Razon Social Identificador de la empresa"
                                   name="razon"
                                   class="form-control"
                                   value="<?php echo $data_empresa[0]['razon_social_empresa']; ?>"
                                   >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Holding:</label>
                        <div class="col-lg-7" id="LabelP">
                            <div class="col-md-6">
                                <input type="hidden" id="id_holding_seleccionado" name="id_holding_seleccionado"                                   
                                       value="<?php echo $data_empresa[0]['id_holding']; ?>"/>
                                <select id="holding" name="holding"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                            </div> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Descripción:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control required"
                                      id="descripcion" requerido="true"
                                      mensaje ="Debe Ingresar una o más observaciones"
                                      name="descripcion"
                                      placeholder="Ingrese observaciones o referencias de la empresa a registrar. Estas observaciones pueden servir para recordar de que empresa se trata en caso que no se recuerde bien..."
                                      maxlength="200"
                                      rows="6"
                                      ><?php echo $data_empresa[0]['descripcion_empresa']; ?></textarea>
                            <p id="text-out">0/200 caracteres restantes</p>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Giro:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="giro" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el Giro de la empresa"
                                   placeholder="Ingrese Giro  de la empresa"
                                   name="giro"
                                   class="form-control"
                                   value="<?php echo $data_empresa[0]['giro_empresa']; ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Dirección:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="direccion" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el Dirección de la empresa"
                                   placeholder="Ingrese Dirección  de la empresa"
                                   name="direccion"
                                   class="form-control"
                                   value="<?php echo $data_empresa[0]['direccion_empresa']; ?>" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Comuna:</label>
                        <div class="col-lg-7" id="LabelP">
                            <div class="col-md-6">
                                <input type="hidden" id="id_comuna_seleccionado" name="id_comuna_seleccionado"                                   
                                       value="<?php echo $data_empresa[0]['id_comuna']; ?>"/>
                                <select id="comuna" name="comuna"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                            </div> 
                        </div>
                    </div>

                    <!--                     <div class="form-group"> -->
                    <!--                         <label class="col-lg-2 control-label">Ciudad:</label> -->
                    <!--                         <div class="col-lg-7" id="LabelP"> -->
                    <!--                             <div class="col-md-6"> -->
                    <!--                                 <select id="ciudad" name="ciudad"  class="select2_demo_3 form-control"> -->
                    <!--                                     <option></option> -->
                    <!--                                 </select> -->
                    <!--                             </div>  -->
                    <!--                         </div> -->
                    <!--                     </div> -->


                    <!-- Select Basic -->

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Teléfono temporal:</label>
                        <div class="col-lg-6">
                            <input type="tel" class="form-control required"
                                   id="telefono_temporal" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el Dirección de la empresa"
                                   placeholder="Ej. 0221234567"
                                   name="telefono_temporal"
                                   class="form-control"
                                   value="<?php echo $data_empresa[0]['telefono_temporal']; ?>" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Dirección Factura:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="direccion_factura" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar la Dirección de facturación"
                                   placeholder="Ingrese Dirección facturación"
                                   name="direccion_factura"
                                   class="form-control"
                                   value="<?php echo $data_empresa[0]['direccion_factura']; ?>" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Comuna Factura:</label>
                        <div class="col-lg-7" id="LabelP">
                            <div class="col-md-6">
                                <input type="hidden" id="id_comuna_factura_seleccionado" name="id_comuna_factura_seleccionado"                                   
                                       value="<?php echo $data_empresa[0]['id_comuna_facturacion']; ?>"/>
                                <select id="comuna_factura" name="comuna_factura"  class="select2_demo_3 form-control">
                                    <option></option>
                                </select>
                            </div> 
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Dirección despacho:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="direccion_despacho" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar la Dirección de despacho"
                                   placeholder="Ingrese Dirección despacho"
                                   name="direccion_despacho"
                                   class="form-control" 
                                   value="<?php echo $data_empresa[0]['direccion_despacho']; ?>" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Email para SII:</label>
                        <div class="col-lg-6">
                            <input type="email" class="form-control required"
                                   id="mail_sii" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el email para SII"
                                   placeholder="Debe Ingresar el email para SII"
                                   name="mail_sii"
                                   class="form-control" 
                                   value="<?php echo $data_empresa[0]['mail_sii']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Nombre contacto cobranza:</label>
                        <div class="col-lg-6">
                            <input type="text" class="form-control required"
                                   id="nombre_contacto_cobranza" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar un nombre de contacto"
                                   placeholder="Ingrese el nombre del contacto"
                                   name="nombre_contacto_cobranza"
                                   class="form-control" 
                                   value="<?php echo $data_empresa[0]['nombre_contacto_cobranza']; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label">Email contacto cobranza:</label>
                        <div class="col-lg-6">
                            <input type="email" class="form-control required"
                                   id="mail_contacto_cobranza" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el email"
                                   placeholder="Ingrese el email e del contacto"
                                   name="mail_contacto_cobranza"
                                   class="form-control" 
                                   value="<?php echo $data_empresa[0]['mail_contacto_cobranza']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">Teléfono contacto cobranza:</label>
                        <div class="col-lg-6">
                            <input type="tel" class="form-control required"
                                   id="telefono_contacto_cobranza" 
                                   requerido="false"
                                   mensaje ="Debe Ingresar el teléfono"
                                   placeholder="Ingrese el teléfono e del contacto"
                                   name="telefono_contacto_cobranza"
                                   class="form-control" 
                                   value="<?php echo $data_empresa[0]['telefono_contacto_cobranza']; ?>">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Observaciones glosa facturación:</label>
                        <div class="col-lg-6">
                            <textarea class="form-control required"
                                      id="observaciones_glosa_facturacion" requerido="true"
                                      mensaje ="Debe Ingresar una o más observaciones"
                                      name="observaciones_glosa_facturacion"
                                      placeholder="Ingrese observaciones correspondientes a la glosa de la factura"
                                      maxlength="200"
                                      rows="6"
                                      ><?php echo $data_empresa[0]['observaciones_glosa_facturacion']; ?></textarea>
                            <p id="text-out2">0/200 caracteres restantes</p>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-lg-2 control-label">Opciones:</label>

                        <div class="checkbox-inline">
                            <input type="hidden" id="id_requiere_orden_compra" name="id_requiere_orden_compra"                                   
                                   value="<?php echo $data_empresa[0]['requiere_orden_compra']; ?>"/>
                            <label><input type="checkbox" id="requiere_orden_compra" name="requiere_orden_compra" value="1">Requiere Orden de Compra</label>
                        </div>

                        <div class="checkbox-inline">
                            <input type="hidden" id="id_requiere_hess" name="id_requiere_hess"                                   
                                   value="<?php echo $data_empresa[0]['requiere_hess']; ?>"/>
                            <label><input type="checkbox" id="requiere_hess" name="requiere_hess" value="1">Requiere Hess</label>
                        </div>

                        <div class="checkbox-inline">
                            <input type="hidden" id="id_requiere_numero_contrato" name="id_requiere_numero_contrato"                                   
                                   value="<?php echo $data_empresa[0]['requiere_numero_contrato']; ?>"/>
                            <label><input type="checkbox" id="requiere_numero_contrato" name="requiere_numero_contrato" value="1">Requiere numero contrato</label>
                        </div>

                        <div class="checkbox-inline">
                            <input type="hidden" id="id_requiere_ota" name="id_requiere_ota"                                   
                                   value="<?php echo $data_empresa[0]['requiere_hess']; ?>"/>
                            <label><input type="checkbox" id="requiere_ota" name="requiere_ota" value="1">Requiere OTA</label>
                        </div>
                    </div>


                    <div class="form-group">
                        <br>
                        <br>
                        <label class="col-lg-2 control-label">Logo de la empresa:</label>     
                        <div class="col-lg-2">
                            <div class="image-crop">
                                <img id="prelogo" src="<?php echo $data_empresa[0]['url_logo_empresa']; ?>">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-10">
                            <label title="Subir una nueva imagen" for="inputImage" class="btn btn-primary" style="top:0px;">
                                <input type="file" id="inputImage" requerido="false" mensaje="Debe subir una imagen" name="file" accept="image/*" class="hide">
                                Subir una nueva imagen
                            </label>
                            <textarea id="logo" name="logo" hidden=""><?php echo $data_empresa[0]['url_logo_empresa']; ?></textarea>           
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-sm btn-primary" type="button" name="btn_editar_empresa" id="btn_editar_empresa">Actualizar</button>
                            <button class="btn btn-sm btn-white" type="button" name="btn_back" id="btn_back">Atrás</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

