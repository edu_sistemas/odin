<?php

/**
 * ResumenDashboard_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-01-11 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-01-11 [Jessica Roa] <jroa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ResumenDashboardEjecutivo_model extends MY_Model {
        function get_arr_datos_select_grupos_ejecutivo($data) {  // nuevo

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_ejecutivos_grupo_dashboard(?,?,?)";  
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }       
      
    function get_arr_datos_select_mejores_ejecutivo($data) {  // nuevo

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_mejores_ejecutivos_dashboard(?,?,?)";  
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }    

// ************ F.Q ************

}
