<?php

/**
 * ResumenDashboard_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-01-08 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-01-08 [Jessica Roa] <jroa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ResumenDashboard_model extends MY_Model {
        function get_arr_datos_select_grupos_ejecutivos($data) {  // nuevo

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_ejecutivos_por_grupo_dashboard(?,?,?)";  
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }        


        function get_arr_rubros($data) { // nuevo

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_rubro_por_grupo_dashboard(?,?,?)";
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }    

        function get_arr_linea_negocio($data) { // nuevo

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = "CALL sp_ventas_linea_por_grupo_dashboard(?,?,?)";
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }          

    function get_arr_empresas($data) { // nuevo

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_empresas_por_grupo_dashboard(?,?,?)";
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }
      
      function get_arr_consultar_venta_acumulada($data) { // nuevo
          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = "CALL sp_venta_anual_acumulada_por_grupo(?,?);";
          $query = $this->db->query($sql, array($user_id, $user_perfil));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }

// ************ F.Q ************

}
