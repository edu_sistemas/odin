<?php

/**
 * Director_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-03-06 [Felipe Bulboa] <fbulbo@edutecno.com>
 * Fecha creacion:  2017-03-06 [Felipe Bulboa] <fbulbo@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Director_model
 */
class Ejecutivo_model extends MY_Model {

    function get_arr_consultar_venta_mensual_ejecutivo($data) {

        /*
          Ultimo usuario  modifica:   Luis Jarpa
          Descripcion : Lista el total de venta mensual
          Fecha Actualiza: 2016-12-26
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_dashboard_venta_mensuales_ejecutivo(?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_venta_acumulada_ejecutivo($data) {

        /*
          Ultimo usuario  modifica:   Luis Jarpa
          Descripcion : Lista el total de venta por periodo
          Fecha Actualiza: 2016-12-26
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_dashboard_venta_anual_ejecutivo(?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

    
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
