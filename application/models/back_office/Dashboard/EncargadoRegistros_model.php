<?php

/**
 * Director_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <lajrpa@edutecno.com>
 * Fecha creacion:  2017-02-16 [Luis Jarpa] <lajrpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Director_model
 */
class EncargadoRegistros_model extends MY_Model {

    function get_arr_listar_fichas_pendientes($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_ficha_factura_select_pendientes_dashboard(?,?);";
        $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }





    

    function get_arr_consultar_venta_mensual($data) {

        /*
          Ultimo usuario  modifica:   Luis Jarpa
          Descripcion : Lista el total de venta mensual
          Fecha Actualiza: 2016-12-26
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_dashboard_venta_mensuales_all_ejecutivo(?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_venta_acumulada($data) {

        /*
          Ultimo usuario  modifica:   Luis Jarpa
          Descripcion : Lista el total de venta mensual
          Fecha Actualiza: 2016-12-26
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_dashboard_venta_anual_acomulada(?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
