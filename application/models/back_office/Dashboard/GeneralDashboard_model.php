<?php

/**
 * GeneralDashboard_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2018-05-23 [Veronica Morales] <vmorales@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class GeneralDashboard_model extends MY_Model {
        function get_arr_datos_select_ejecutivos($data) { 

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_ejecutivos_dashboard(?,?,?)";  
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }        


        function get_arr_rubros($data) { 

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_rubro_dashboard(?,?,?)";
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }    

        function get_arr_linea_negocio($data) { 

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = "CALL sp_ventas_linea_dashboard(?,?,?)";
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }          

    function get_arr_empresas($data) { 

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_empresas_dashboard(?,?,?)";
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }
      
      function get_arr_consultar_venta_acumulada($data) { 
          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = "CALL sp_venta_anual_acumulada(?,?);";
          $query = $this->db->query($sql, array($user_id, $user_perfil));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }

    function get_arr_ventas_empresas_excel($data) { 
        $fecha = $data['fecha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        
        $sql = " CALL sp_ventas_empresas_dashboard_excel(?,?,?)";
        $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

     function get_arr_datos_select_ranking_ejecutivos_excel($data) { 

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_ventas_ejecutivos_dashboard_excel(?,?,?)";  
          $query = $this->db->query($sql, array($fecha,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }   



}
