<?php

/**
 * Docente_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  0000-00-00 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-14 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *  Docente_model
 */
class HorasRelator_model extends MY_Model {

    function get_arr_listar_docente_by_fechas($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
		$hora_inicio = $data['finicio'];
		$hora_termino = $data['Ftermino'];		
        $sql = " CALL sp_docente_select_horas(?,?,?,?)";
        $query = $this->db->query($sql, array($hora_inicio,$hora_termino,$user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	function get_arr_listar_docente_by_fechas_id($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
		$id_relator = $data['id_relator'];
		$hora_inicio = $data['finicio'];
		$hora_termino = $data['Ftermino'];		
        $sql = " CALL sp_docente_select_horas_by_id(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_relator,$hora_inicio,$hora_termino,$user_id, $user_perfil));
        $result = $query->result_array();
		$this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    } 
	function get_arr_listar_docente_by_fechas_id_total($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
		$id_relator = $data['id_relator'];
		$hora_inicio = $data['finicio'];
		$hora_termino = $data['Ftermino'];		
        $sql = " CALL sp_docente_select_horas_totales_by_id(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_relator,$hora_inicio,$hora_termino,$user_id, $user_perfil));
        $result = $query->result_array();
		$this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }      
    
}
