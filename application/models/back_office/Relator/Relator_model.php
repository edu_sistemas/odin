<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Relator_Model
 */
class Relator_Model extends MY_Model {

      function set_arr_agregar_relator($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade un nuevo relator
          Fecha Actualiza: 2016-06-12
         */
        $rut_relator             = $data['rut_relator'];
        $dv_relator               = $data['dv_relator'];
        $usuario_relator          = $data['usuario_relator'];
        $nombre_relator          = $data['nombre_relator'];
        $seg_nombre_relator      = $data['seg_nombre_relator'];
        $apellido_paterno_relator = $data['apellido_paterno_relator'];
        $apellido_materno_relator = $data['apellido_materno_relator'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_relator_insert(?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql,array($rut_relator,$dv_relator,$usuario_relator,$nombre_relator,$seg_nombre_relator,
                                              $apellido_paterno_relator,$apellido_materno_relator,$user_id,$user_perfil));
        $result = $query->result_array();

        return $result;
    }


    function get_arr_relator_listar($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los relatores
          Fecha Actualiza: 2016-10-25
         */

        $rut = $data['rut'];
        $nombre = $data['nombre'];
        $estado = $data['estado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_relator_select(?,?,?,?,?);";

        $query = $this->db->query($sql,array($rut,$nombre,$estado,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        
        return $result;
    }
    
    function get_arr_relator_cursos($data) {
    	/*
    	 Ultimo usuario  modifica: Marcelo Romero
    	 Descripcion : Lista los relatores
    	 Fecha Actualiza: 2016-10-25
    	 */
    
    	$id_relator = $data['id_relator'];
    	$user_id = $data['user_id'];
    	$user_perfil = $data['user_perfil'];
    
    	//listar altiro null
    
    	$sql = "CALL sp_relator_select_cursos(?,?,?);";
    
    	$query = $this->db->query($sql,array($id_relator,$user_id,$user_perfil));
    
    	$result = $query->result_array();
    	$query->next_result();
    	$query->free_result();
    
    	return $result;
    }

        function set_arr_editar_relator($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2016-12-06
         */
        $id_relator               = $data['id_relator'];
        $rut_relator              = $data['rut_relator'];
        $dv_relator               = $data['dv_relator'];
        $usuario_relator          = $data['usuario_relator'];
        $nombre_relator           = $data['nombre_relator'];
        $seg_nombre_relator       = $data['seg_nombre_relator'];
        $apellido_paterno_relator = $data['apellido_paterno_relator'];
        $apellido_materno_relator = $data['apellido_materno_relator'];
        $user_id                 = $data['user_id'];
        $user_perfil             = $data['user_perfil'];

        $sql = "CALL sp_relator_update(?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql,array( $id_relator,$rut_relator,$dv_relator,$usuario_relator,$nombre_relator,$seg_nombre_relator,
                                              $apellido_paterno_relator,$apellido_materno_relator,$user_id,$user_perfil ));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function sp_relator_select_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupera curso desde BD
          Fecha Actualiza: 2016-12-05
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_relator_select_by_id('" . $id . "','" . $user_id . "','" . $user_perfil . "');";

        $query = $this->db->query($sql);
        $row_count = $query->num_rows();
        $result = false;
        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

  
    function set_sp_relator_change_status($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia el estado de los usuarios
          Fecha Actualiza: 2016-11-25
         */
        $id_relator = $data['id_relator'];


        $estado = $data['estado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_relator_change_status(?,?,?,?);";

        $query = $this->db->query($sql,array($id_relator,$estado,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
