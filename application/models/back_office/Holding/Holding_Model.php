<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Holding_Model
 */
class Holding_model extends MY_Model {

    function get_arr_listar_holding_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los holding model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_holding_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_select_holding($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Selecciona el holding de la lista
          Fecha Actualiza: 2016-11-29
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_holding_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_holding($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Inserta un nuevo holding
          Fecha Actualiza: 2016-11-29
         */

        $nombre_holding = $data['nombre_holding'];
        $descripcion_holding = $data['descripcion_holding'];
        $estado = $data['estado'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_holding_insert(?,?,?,?,?);";

        $query = $this->db->query($sql, array($nombre_holding, $descripcion_holding, $estado, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_update_holding($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Actualiza un holding existente
          Fecha Actualiza: 2016-11-29
         */

        $id_holding = $data['id_holding'];
        $nombre_holding = $data['nombre_holding'];
        $descripcion_holding = $data['descripcion_holding'];
        $estado = $data['estado'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_holding_update(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_holding, $nombre_holding, $descripcion_holding, $estado, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_holding($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_holding_select(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_holding_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_holding = $data['id_holding'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_holding_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id_holding, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
