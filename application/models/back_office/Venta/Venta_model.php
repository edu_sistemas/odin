<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-16 [Luis Jarpa] <lajrpa@edutecno.com>
 * Fecha creacion:  2017-02-16 [Luis Jarpa] <lajrpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Venta_models
 */
class Venta_model extends MY_Model {
    /* ventas globales */

    function get_arr_consultar_ventas($data) {

        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $ejecutivo = $data['ejecutivo'];
        $periodo = $data['periodo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ventas_select_global(?,?,?,?);";

        $query = $this->db->query($sql, array(
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_ventas_por_ejecutivos($data) {

        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $ejecutivo = $data['ejecutivo'];
        $periodo = $data['periodo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ventas_select_global_by_ejecutivo_periodo(?,?,?,?);";

        $query = $this->db->query($sql, array(
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
                )
        );

//  echo $this->db->last_query();


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_ventas_anual($data) {

        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $ejecutivo = $data['ejecutivo'];
        $periodo = $data['periodo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ventas_select_global_anual(?,?,?,?);";

        $query = $this->db->query($sql, array(
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_ventas_por_ejecutivos_anual($data) {

        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $ejecutivo = $data['ejecutivo'];
        $periodo = $data['periodo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ventas_select_global_by_ejecutivo_periodo_anual(?,?,?,?);";

        $query = $this->db->query($sql, array(
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* end ventas globales */
    /* Informes ventas */

    function get_arr_consultar_informe_venta_cursos($data) {

        /*
          Ultimo usuario  modifica:  , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data['fecha_inicio'];
        $end = $data['fecha_termino'];
        $opcion = $data['opcion'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_venta_cursos(?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $opcion,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_informe_venta_arriendo_salas($data) {

        /*
          Ultimo usuario  modifica:  , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data['fecha_inicio'];
        $end = $data['fecha_termino'];
        $opcion = $data['opcion'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_venta_arriendo_salas(?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $opcion,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* end Informes ventas */
}
