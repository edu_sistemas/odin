<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:   
 * Fecha creacion:  2017-13-15 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class InformeDistancia_model extends MY_Model {

    function getFichas($data) {
        /*
          Ultimo usuario  modifica:
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2017-03-15
         */

        $num_ficha = $data['num_ficha'];
        $empresa = $data['empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_informe_distancia(?,?,?,?)";
        $query = $this->db->query($sql, array($num_ficha, $empresa, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_empresa_select_cbx_informe_distancia($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_cbx_sticker(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getInformes($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_distancia_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getDatosInformed($data) {

        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $id_centro_costo = $data['id_centro_costo'];



        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_distancia_get_datos(?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $id_ficha, $id_orden_compra, $id_centro_costo,
            $user_id, $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getAlumnosInforme($data) {

        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $id_centro_costo = $data['id_centro_costo'];
        $id_centro_costo2 = $data['id_centro_costo2'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_distancia_get_alumnos(?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_orden_compra,
            $id_centro_costo,
            $id_centro_costo2,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function sp_informe_distancia_get_datos_dejota($data) {

        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $id_centro_costo = $data['id_centro_costo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_distancia_get_datos_dejota(?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_orden_compra,
            $id_centro_costo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}

/*InformeDistancia_model.php
 * el modelo
 */