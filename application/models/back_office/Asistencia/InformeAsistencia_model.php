<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-11 [David De Filippi] <mromero@edutecno.com>
 * Fecha creacion:  2017-01-11 [David De Filippi] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Log_model
 */
class InformeAsistencia_model extends MY_Model {

	function get_arr_listar_fichas($data) {
		/*
		  Ultimo usuario  modifica: Marcelo Romero
		  Descripcion : Lista las capsulas por ficha
		  Fecha Actualiza: 2017-01-12
		 */

		$nombre_empresa = $data['nombre_empresa'];
		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = " CALL sp_select_xcen_asistencia_alm(?,?,?,?)";
		$query = $this->db->query($sql, array($nombre_empresa, $id_ficha, $user_id, $user_perfil));

		$result = $query->result_array();

		$query->next_result();
		$query->free_result();

		return $result;
	}

	function get_arr_listar_asistencia($data) {
		/*
		  Ultimo usuario  modifica: Marcelo Romero
		  Descripcion : Lista las capsulas por ficha
		  Fecha Actualiza: 2017-01-12
		 */

		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = " CALL sp_calculo_xcen_asistencia_alm(?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

		$result = $query->result_array();

		$query->next_result();
		$query->free_result();

		return $result;
	}

	function get_arr_listar_asistencia_detalle($data) {
		/*
		  Ultimo usuario  modifica: Marcelo Romero
		  Descripcion : Lista las capsulas por ficha
		  Fecha Actualiza: 2017-01-12
		 */

		$id_ficha = $data['id_ficha'];
		$id_alumno = $data['id_alumno'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = " CALL sp_dtl_asistencia_by_id_ficha(?,?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $id_alumno, $user_id, $user_perfil));


		$result = $query->result_array();

		$query->next_result();
		$query->free_result();

		return $result;
	}

	function get_arr_empresas_cbx($data) {
		/*
		  lista los usuarios
		  Fecha Actualiza: 2016-12-26
		 */
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = " CALL sp_empresa_select_cbx(?,?)";
		$query = $this->db->query($sql, array($user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function get_lista_detalle_excel($data) {
		/*
		  Descarga excel
		 */

		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = "CALL sp_dtl_asistencia_excel_dwl(?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}
	
		function get_lista_detalle_excel_id_alm($data) {
		/*
		  Descarga excel
		 */

		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = "CALL sp_dtl_asistencia_excel_dwl_data_alummnos(?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}
	
	
	function get_lista_detalle_dias_excel($data) {
		/*
		  Descarga excel
		 */

		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = "CALL sp_dtl_asistencia_excel_dwl_capsula(?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}
	
		function get_lista_detalle_dias_dtl_excel($data) {
		/*
		  Descarga excel
		 */

		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = "CALL sp_dtl_asistencia_excel_dwl_asistencia(?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}
	
	
	
	

}
