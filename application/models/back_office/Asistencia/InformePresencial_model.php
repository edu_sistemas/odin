<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class InformePresencial_model extends MY_Model {

    function getFichas($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $num_ficha = $data['num_ficha'];
        $empresa = $data['empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_informe_presencial(?,?,?,?)";
        $query = $this->db->query($sql, array($num_ficha, $empresa, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getInformes($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_empresa_select_cbx_informe_presencial($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_select_cbx_informe_presencial(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getDatosInforme($data) {

        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $id_centro_costo = $data['id_centro_costo'];
        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_get_datos(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $id_orden_compra, $id_centro_costo, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getAlumnosInforme($data) {

        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $id_centro_costo = $data['id_centro_costo'];
        $id_centro_costo2 = $data['id_centro_costo2'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_get_alumnos(?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_orden_compra,
            $id_centro_costo,
            $id_centro_costo2,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}

/*InformePresencial.php
 * el modelo
 */