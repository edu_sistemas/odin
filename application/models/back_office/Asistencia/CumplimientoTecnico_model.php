<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-11 [David De Filippi] <mromero@edutecno.com>
 * Fecha creacion:  2017-01-11 [David De Filippi] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * CumplimientoTecnico_model
 */
class CumplimientoTecnico_model extends MY_Model {

    function get_arr_listar_ficha_reserva_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los numero deficha que están dentro de la tabla ficha_reserva
          Fecha Actualiza: 2017-01-12
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_reserva_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));



        $result = $query->result_array();


        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_capsulas($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */

        $id_ficha = $data['id_ficha'];
    



        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_capsula_select_by_ficha(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_alumnos($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */

        $id_ficha = $data['id_ficha'];
        $id_capsula = $data['id_capsula'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_alumnos_select_by_ficha_id(?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $id_capsula, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_registrar_encuesta($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Registra el cumplimiento tecnico en la clase correspondiente.
          Fecha Actualiza: 2017-01-12
         * 
         */
    	$id_ficha = $data['id_ficha'];
    	$id_capsula = $data['id_capsula'];
    	$r1 = $data['r1'];
    	$r2 = $data['r2'];
    	$r3 = $data['r3'];
    	$r4 = $data['r4'];
    	$r5 = $data['r5'];
    	$r6 = $data['r6'];
    	$r7 = $data['r7'];
    	$r8 = $data['r8'];
    	$r9 = $data['r9'];
    	$r10 = $data['r10'];
    	$r11 = $data['r11'];
    	$r12 = $data['r12'];
    	$r13 = $data['r13'];
    	$user_id = $data['user_id'];
    	$user_perfil = $data['user_perfil'];
    	$sql = "CALL sp_cumplimiento_tecnico_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    	$query = $this->db->query($sql, array(
    			$id_ficha, 
    			$id_capsula, 
    			$r1,
    			$r2,
    			$r3,
    			$r4,
    			$r5,
    			$r6,
    			$r7,
    			$r8,
    			$r9,
    			$r10,
    			$r11,
    			$r12,
    			$r13,
    			$user_id, 
    			$user_perfil));
    	
    	$result = $query->result_array();
    	$query->next_result();
    	$query->free_result();
    	return $result;
    }

}
