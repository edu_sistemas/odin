<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class LibroClases_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function getFichas() {
        $query = $this->db->query('SELECT `id_ficha`, `num_ficha` FROM `tbl_ficha`');

        return $query->result_array();
    }

    function getLibros($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_libro_select(?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function GetRelatores($data) {

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_relatores_asignados_inf(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getDatosLibro($data) {

        $id_ficha = $data['id_ficha'];
        $opcion_sence = $data['opcion_sence'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_libro_get_datos(?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $opcion_sence, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getAlumnosLibro($data) {

        $id_ficha = $data['id_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_libro_get_alumnos(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}

/*libroClases.php
 * el modelo
 */