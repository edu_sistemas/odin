<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-04-04 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2018-04-04 [Jessica Roa] <jroa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class InformePrimerDia_model extends MY_Model {

    function getFichas($data) {
        /*
          Ultimo usuario  modifica:  
          Descripcion : Lista las fichas para generar informe de primer dia
          Fecha Actualiza: 2018-04-04
         */
        $num_ficha = $data['num_ficha'];
        $id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_informe_primer_dia(?,?,?,?)";
        $query = $this->db->query($sql, array($num_ficha, $id_empresa, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function getFichasHoy($data) {
        /*
          Ultimo usuario  modifica:  
          Descripcion : Lista las fichas para generar informe de primer dia
          Fecha Actualiza: 2018-04-04
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_clases_hoy(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getInformes($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getDatosInforme($data) {

        $id_ficha = $data['id_ficha'];        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getAlumnosInforme($data) {

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumnos_select_by_ficha(?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}

/*InformePresencial.php
 * el modelo
 */