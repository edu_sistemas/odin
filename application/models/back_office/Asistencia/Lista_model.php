<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-11 [David De Filippi] <mromero@edutecno.com>
 * Fecha creacion:  2017-01-11 [David De Filippi] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class Lista_model extends MY_Model {

    function get_arr_listar_ficha_reserva_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los numero deficha que están dentro de la tabla ficha_reserva
          Fecha Actualiza: 2017-01-12
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_reserva_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_capsulas_editar_horario($data) {
        $id_capsula = $data['id_capsula'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_capsula_select_by_id_edit(?,?,?)";
        $query = $this->db->query($sql, array($id_capsula, $user_id, $user_perfil));

        $row_count = $query->num_rows();
        $result = false;


        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_capsulas_editar_horario_save($data) {

        $id_capsula = $data['id_capsula'];
        $fecha_inicio = $data['fInicio'];
        $hora_inicio = $data['hInicio'];

        $hora_termino = $data['htermino'];

        $inicio_completa = $fecha_inicio . ' ' . $hora_inicio . ':00';
        $termino_completa = $fecha_inicio . ' ' . $hora_termino . ':00';


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_capsula_hf_update(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_capsula, $inicio_completa, $termino_completa, $user_id, $user_perfil));

        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_capsulas($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_capsula_select_by_ficha(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_relatores($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_relator_select_by_ficha_id_cbx(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_registrar_alumnos_new($data) {


        $id_orden_compra = $data['orden_compra'];
        $id_ficha = $data['id_ficha'];
        $id_capsula = $data['id_capsula'];
        $id_relator = $data['id_relator'];
        $rut = $data['rut'];
        $dv = $data['dv'];
        $rut_completo = $data['usuario_alumno'];
        $nombre = $data['nombre'];
        $apellido = $data['apellido'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumno_insert_extra(?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_orden_compra, $id_ficha, $id_capsula, $id_relator, $rut, $dv,
            $rut_completo, $nombre, $apellido, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_alumnos($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */

        $id_ficha = $data['id_ficha'];
        $id_capsula = $data['id_capsula'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumnos_select_by_ficha_id(?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $id_capsula, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_registrar_asistencia($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Registra la asistencia en la tabla asistencia, recorre ciclo for con los datos que lista el datatable
          Fecha Actualiza: 2017-01-12
         * 
         */
        $result = false;
        $numRows = $data['numRows'];
        $arr = array();
        $arratr = array();
        $arr_dtla = array();


        $arr_input_data = array(
            'chk' => $arr,
            'chk_atrs' => $arratr,
            'numRows' => $this->input->post('numRows'),
            'capsulainput' => $this->input->post('capsulainput'),
            'id_alm' => $arr_dtla
        );


        for ($inst = 0; $inst < $numRows; $inst++) {

            $a = $data['chk'][$inst];
            $b = $data['id_alm'][$inst];
            $c = $data['chk_atrs'][$inst];



            $capsulainput = $data['capsulainput'];
            $numRows = $data['numRows'];

            $id_dtl_relator_ficha = $data['id_dtl_relator_ficha'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

            $sql = " CALL sp_asistencia_insert(?,?,?,?,?,?,?)";
            $query = $this->db->query($sql, array($a, $b, $c, $capsulainput, $id_dtl_relator_ficha, $user_id, $user_perfil));


            $result = $query->result_array();

            $query->next_result();
            $query->free_result();
        }
        return $result;
    }

    function set_arr_insert_new_class($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */

        $id_sala = $data['id_sala'];
        $id_detalle_sala = $data['id_detalle_sala'];
        $f_inicio = $data['fecha_inicio'];
        $f_termino = $data['fecha_termino'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = "CALL sp_insert_nueva_clase_ma(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($f_inicio, $f_termino, $id_sala, $id_detalle_sala, $user_id, $user_perfil));
        //echo  $this->db->last_query();
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_sala_editar_horario($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_sala_select_edit(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    /*function set_sala_editar($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $id_sala = $data['sala'];
        $id_detalle_sala = $data['detalle_sala'];
        $id_disponibilidad_detalle = $data['disponibilidad_detalle'];

        $sql = " CALL sp_sala_asistencia_edit(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_sala, $id_detalle_sala, $id_disponibilidad_detalle, $user_id, $user_perfil));

        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }*/

    function set_sala_eliminar_bloque($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $id_capsula = $data['id_capsula'];

        $sql = " CALL sp_sala_asistencia_delete(?,?,?)";
        $query = $this->db->query($sql, array($id_capsula, $user_id, $user_perfil));

        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
