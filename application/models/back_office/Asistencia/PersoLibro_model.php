<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class PersoLibro_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function getFichas() {
        $query = $this->db->query('SELECT `id_ficha`, `num_ficha` FROM `db_odin`.`tbl_ficha`');

        return $query->result_array();
    }

    function getLibros($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_libro_perso_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getFichasPresencial($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_reserva_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getDatosFicha($data) {
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_libro_ficha_select(?,?,?)";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getIdPerso($data) {
        $name = $data['name'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_libro_perso_insert(?,?,?)";
        $query = $this->db->query($sql, array($name, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function insertLibroFicha($data) {
        $idPerso = $data['idPerso'];
        $ficha = $data['ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_libro_ficha_insert(?,?,?,?)";
        $query = $this->db->query($sql, array($idPerso, $ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_select_libro_personalizado_by_id($data) {
        $id_libro_personalizado = $data['id_libro_personalizado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_select_libro_personalizado_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_libro_personalizado, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    //seccion dirigida a libro clases pdf
    function getDatosLibro($data) {

        $id_ficha = $data['id_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_libro_get_datos(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_ficha_by_id_libro($data) {
        $id_ficha = $data['id_ficha'];
        $id_libro = $data['id_libro'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_insert_ficha_libro(?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $id_libro, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getAlumnosLibro($data) {

        $id_ficha = $data['id_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_libro_get_alumnos(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_delete_ficha_libro($data) {
        $id_ficha_libro = $data['id_ficha_libro'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_delete_ficha_libro(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha_libro, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_delete_libro_personalizado($data) {
        $id_libro_personalizado = $data['id_libro_personalizado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_delete_libro_personalizado(?,?,?)";
        $query = $this->db->query($sql, array($id_libro_personalizado, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
