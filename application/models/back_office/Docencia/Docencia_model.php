<?php

/**
 * Edutecno_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-02 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-12-02 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Docencia_model extends MY_Model {

    function get_arr_consultar_ficha_data($data) {

        $start = $data['star'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_select_jd_filter(?,?,?)";

        $query = $this->db->query($sql, array($start, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
