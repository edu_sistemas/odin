<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Empresa_contacto_model
 */
class Empresa_contacto_model extends MY_Model {

    function get_arr_empresa_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_select_contact(?,?,?)";
        
        $query = $this->db->query($sql,array($id_empresa, $user_id, $user_perfil));
        

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_empresa_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Agrega un nuevo contacto  al empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_empresa = $data['id_empresa'];
        $nombre_contacto_empresa = $data['nombre_contacto_empresa'];
        $tipo_contacto_empresa = $data['tipo_contacto_empresa'];
        $cargo_contacto_empresa = $data['cargo_contacto_empresa'];
        $departamento_contacto_empresa = $data['departamento_contacto_empresa'];
        $nivel_contacto_empresa = $data['nivel_contacto_empresa'];
        $correo_contacto_empresa = $data['correo_contacto_empresa'];
        $telefono_contacto_empresa = $data['telefono_contacto_empresa'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_insert_contact(?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql,array(
            $id_empresa,
            $nombre_contacto_empresa,
            $tipo_contacto_empresa,
            $cargo_contacto_empresa,
            $departamento_contacto_empresa,
            $nivel_contacto_empresa,
            $correo_contacto_empresa,
            $telefono_contacto_empresa,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    function update_empresa_contact($data) {
        /*
          Ultimo usuario  modifica: Yo
          Descripcion : Agrega un nuevo contacto  al empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto = $data['id_contacto'];
        $nombre_contacto_empresa = $data['nombre_contacto_empresa'];
        $tipo_contacto_empresa = $data['tipo_contacto_empresa'];
        $cargo_contacto_empresa = $data['cargo_contacto_empresa'];
        $departamento_contacto_empresa = $data['departamento_contacto_empresa'];
        $nivel_contacto_empresa = $data['nivel_contacto_empresa'];
        $correo_contacto_empresa = $data['correo_contacto_empresa'];
        $telefono_contacto_empresa = $data['telefono_contacto_empresa'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_update_contact(?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql,array(
            $id_contacto,
            $nombre_contacto_empresa,
            $tipo_contacto_empresa,
            $cargo_contacto_empresa,
            $departamento_contacto_empresa,
            $nivel_contacto_empresa,
            $correo_contacto_empresa,
            $telefono_contacto_empresa,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_status_empresa_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : descativa  un  contacto  al empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_empresa = $data['id_contacto_empresa'];
        $estado = $data['estado']; 
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_desactivar_contact(?,?,?,?)";

        $query = $this->db->query($sql,array(            
            $id_contacto_empresa,     
            $estado,       
            $user_id,
            $user_perfil
            ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_update_empresa_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_empresa = $data['id_contacto_empresa'];
        $nombre_contacto = $data['nombre_contacto'];
        $id_tipo_contacto_empresa = $data['id_tipo_contacto_empresa'];
        $cargo_contacto = $data['cargo_contacto'];
        $departamento_contacto = $data['departamento_contacto'];
        $id_nivel_contacto = $data['id_nivel_contacto'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_update_contact(?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql,array(
            $id_contacto_empresa,
            $nombre_contacto, 
            $id_tipo_contacto_empresa,
            $cargo_contacto, 
            $departamento_contacto, 
            $id_nivel_contacto,  
            $correo_contacto,  
            $telefono_contacto,  
            $user_id,
            $user_perfil
            ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_delete_empresa_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al empresa
          Fecha Actualiza: 2016-11-28
         */
        
        $id_contacto_empresa = $data['id_contacto_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_delete_contact(?,?,?);";

        $query = $this->db->query($sql,array(
            
            $id_contacto_empresa, 
            $user_id,
            $user_perfil
            ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
	function get_arr_empresa_direccion($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_select_direccion(?,?,?)";
        
        $query = $this->db->query($sql,array($id_empresa, $user_id, $user_perfil));
        

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
	function get_arr_tipo_empresa($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2016-11-28
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_select_tipo_direccion(?,?)";
        
        $query = $this->db->query($sql,array($user_id, $user_perfil));       

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
		$data = '';
		$data .= '<option value=0>Seleccionar';
		for($i=0;$i<count($result);$i++){
			$data .= '<option value='.$result[$i]['id_tipo'].'>'.$result[$i]['descripcion'].' ';			
		}
        return $data;
    }
	
	function set_direccion_empresa($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2016-11-28
         */
		$direccion = $data['direccion'];
		$tipo = $data['tipo'];
		$fecha = $data['fecha'];
		$id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_insert_direccion(?,?,?,?,?,?)";
        
        $query = $this->db->query($sql,array($direccion,$tipo,$fecha,$id_empresa,$user_id, $user_perfil));       

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
		
        return $result;
    }
	function delete_direccion_empresa($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2016-11-28
         */
		
		$id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_delete_direccion(?,?,?)";
        
        $query = $this->db->query($sql,array($id_empresa,$user_id, $user_perfil));       
		$this->db->last_query();
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
		
        return $result;
    }
    
    function get_arr_listar_TipoContacto_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_TipoContacto_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_nivel_contacto_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_nivel_contacto_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_contacto_empresa($data) {
        $id_contacto_empresa = $data['id_contacto_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_empresa_select_contact_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_contacto_empresa, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
}
