<?php

/**
 * PanelDeControl_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-09-21 Luis Jarpa <e-mail>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_model extends MY_Model {

    // carga  TITULOS DEL menu  usuario
    function get_arr_user($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $result = array();

        $sql = "CALL sp_g_get_modulos('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        $query->next_result();
        $query->free_result();
        array_push($result, $res);

        $sql2 = "CALL sp_g_get_sub_modulo('" . $user_id . "','" . $user_perfil . "')";
        $query2 = $this->db->query($sql2);
        $res = $query2->result_array();
        $query2->next_result();
        $query2->free_result();
        array_push($result, $res);

        $sql3 = "CALL sp_g_get_sub_modulo_item('" . $user_id . "','" . $user_perfil . "')";
        $query3 = $this->db->query($sql3);
        $res = $query3->result_array();
        $query3->next_result();
        $query3->free_result();
        array_push($result, $res);

        return $result;
    }

}

/* End of file Menu_model.php */
/* Location: ./application/models/hito_uno/Menu_model.php */
