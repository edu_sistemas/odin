<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2019-01-20 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Nacionalidad_Model
 */
class Nacionalidad_Model extends MY_Model {

    function get_arr_listar_nacionalidad_cbx($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Lista las nacionalidades para rellenar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_nacionalidad_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
