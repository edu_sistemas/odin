<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Alumno_contacto_model
 */
class Alumno_contacto_model extends MY_Model {

    function get_arr_alumno_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el alumno
          Fecha Actualiza: 2016-11-28
         */

        $id_alumno = $data['id_alumno'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumno_select_contact(?,?,?)";

        $query = $this->db->query($sql,array($id_alumno,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_alumno_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Agrega un nuevo contacto  al alumno
          Fecha Actualiza: 2016-11-28
         */

        $id_alumno = $data['id_alumno'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumno_insert_contact(?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_alumno,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_status_alumno_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : descativa  un  contacto  al alumno
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_alumno = $data['id_contacto_alumno'];
        $estado = $data['estado']; 
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumno_desactivar_contact(?,?,?,?)";

        $query = $this->db->query($sql,array($id_contacto_alumno,$estado,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_update_alumno_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al alumno
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_alumno = $data['id_contacto_alumno'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumno_update_contact(?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_contacto_alumno,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_delete_alumno_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al alumno
          Fecha Actualiza: 2016-11-28
         */
        
        $id_alumno = $data['id_alumno'];
        $id_contacto_alumno = $data['id_contacto_alumno'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumno_delete_contact(?,?,?,?,?,?);";

        $query = $this->db->query($sql,array($id_contacto_alumno,$id_alumno,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
