<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:   
 * Fecha creacion:  2017-13-15 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sticker_model extends MY_Model {

    function getFichas($data) {
        /*
          Ultimo usuario  modifica:
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2017-03-15
         */

        $num_ficha = $data['num_ficha'];
        $empresa = $data['empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_sticker_cbx(?,?,?,?)";
        $query = $this->db->query($sql, array(
            $num_ficha,
            $empresa,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_empresa_select_cbx_sticker($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_select_cbx_informe_elearning(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getAlumnosStick($data) {

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_sticker_doc(?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}

/*InformeDistancia_model.php
 * el modelo
 */