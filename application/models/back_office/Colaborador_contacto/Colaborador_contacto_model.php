<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:   [] <@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Colaborador_contacto_model
 */
class Colaborador_contacto_model extends MY_Model {

    function get_arr_colaborador_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el colaborador
          Fecha Actualiza: 2016-11-28
         */

        $id_colaborador = $data['id_colaborador'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_colaborador_select_contact(?,?,?)";

        $query = $this->db->query($sql,array($id_colaborador,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_colaborador_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Agrega un nuevo contacto  al colaborador
          Fecha Actualiza: 2016-11-28
         */
        $id_colaborador = $data['id_colaborador'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_colaborador_insert_contact(?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_colaborador,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_status_colaborador_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : descativa  un  contacto  al colaborador
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_colaborador = $data['id_contacto_colaborador'];
        $estado = $data['estado']; 
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_colaborador_desactivar_contact(?,?,?,?)";

        $query = $this->db->query($sql,array($id_contacto_colaborador,$estado,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_update_colaborador_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al colaborador
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_colaborador = $data['id_contacto_colaborador'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_colaborador_update_contact(?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_contacto_colaborador,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_delete_colaborador_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al colaborador
          Fecha Actualiza: 2016-11-28
         */
        
        $id_colaborador = $data['id_colaborador'];
        $id_contacto_colaborador = $data['id_contacto_colaborador'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_colaborador_delete_contact(?,?,?,?,?,?);";

        $query = $this->db->query($sql,array($id_contacto_colaborador,$id_colaborador,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
