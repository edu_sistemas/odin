<?php

/**
 * Empresa_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion: 
 * Fecha creacion:  2017-03-07 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Code_sence_model
 */
class Code_sence_model extends MY_Model {

    function get_arr_listar_sence_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : L
          Fecha Actualiza: 2017-03-07
         * MODALIDAD
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_comercial_sence_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_nivel_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion :
          Fecha Actualiza: 2017-03-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_nivel_comercial_sence_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_suite_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion :
          Fecha Actualiza: 2017-03-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_suite_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_version_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion :
          Fecha Actualiza: 2017-03-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_version_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_code_buscar_comercial($data) {

        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */

        $p_rcodigo = $data['codigo'];
        $p_nombre = $data['nombre'];
        $p_modalidad = $data['modalidad'];
        $p_version = $data['version'];
        $p_nivel = $data['nivel'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_code_comercial_sence_buscar_select(?,?,?,?,?,?,?)";



        $query = $this->db->query($sql, array(
            $p_rcodigo,
            $p_nombre,
            $p_modalidad,
            $p_version,
            $p_nivel,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_code_buscar($data) {

        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */

        $p_rcodigo = $data['codigo'];
        $p_nombre = $data['nombre'];
        $p_modalidad = $data['modalidad'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_code_sence_buscar_select(?,?,?,?,?)";



        $query = $this->db->query($sql, array(
            $p_rcodigo,
            $p_nombre,
            $p_modalidad,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_code_sence($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade un nuevo Alumno
          Fecha Actualiza: 2016-06-12
         */

        $codigo_sence = $data['codigo_sence'];
        $fecha_solicitud = $data['fecha_solicitud'];
        $fecha_acreditacion = $data['fecha_acreditacion'];
        $fecha_vigencia = $data['fecha_vigencia'];
        $nombre_curso_code_sence = $data['nombre_curso_code_sence'];
        $cantidad_hora_sence = $data['cantidad_hora_sence'];
        $valor_hora_sence = $data['valor_hora_sence'];
        $id_modalidad_sence = $data['id_modalidad_sence'];
        $nombre_archivo = $data['nombre_archivo'];
        $ruta_archivo = $data['ruta_archivo'];
        $id_otec = $data['otec'];
        $resolucion_excenta = $data['resolucion_excenta'];
        $valor_efectivo_participante = $data['valor_efectivo_participante'];
        $valor_total_curso = $data['valor_total_curso'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_code_sence_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $codigo_sence,
            $fecha_solicitud,
            $fecha_acreditacion,
            $fecha_vigencia,
            $nombre_curso_code_sence,
            $cantidad_hora_sence,
            $valor_hora_sence,
            $id_modalidad_sence,
            $nombre_archivo,
            $ruta_archivo,
            $id_otec,
            $resolucion_excenta,
            $valor_efectivo_participante,
            $valor_total_curso,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_sence($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista de sence
          Fecha Actualiza: 2016-12-09
         */
    	

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_code_sence_select(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
    function get_arr_listar_sence_alerta($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista de sence
          Fecha Actualiza: 2016-12-09
         */
    	

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_code_sence_select_alerta(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_sence_consulta($data) {
    	/*
    	 Ultimo usuario  modifica: Marcelo Romero
    	 Descripcion : Lista de sence
    	 Fecha Actualiza: 2016-12-09
    	 */
    	$cod_sence = $data['cod_sence'];
    	$num_ficha = $data['num_ficha'];
    	$user_id = $data['user_id'];
    	$user_perfil = $data['user_perfil'];
    	
    	//listar altiro null
    	
    	$sql = "CALL sp_code_sence_select_consulta(?,?,?,?);";
    	
    	$query = $this->db->query($sql, array($cod_sence, $num_ficha, $user_id, $user_perfil));
    	
    	$result = $query->result_array();
    	$query->next_result();
    	$query->free_result();
    	
    	return $result;
    }

    function set_arr_editar_sence_estado($datos) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Modificar un módulo
          Fecha Actualiza: 2016-11-22
         */
        $id_code_sence = $datos['id_code_sence_m'];
        $estado_code_sence = $datos['estado_code_sence_m'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];


        $sql = "CALL sp_sence_code_estado_change(?,?,?,?);";

        $query = $this->db->query($sql, array($id_code_sence, $estado_code_sence, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_code_sence_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupera curso desde BD
          Fecha Actualiza: 2016-12-12
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_code_sence_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_code_sence($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2016-12-12
         */
        $id_code_sence = $data['id_code_sence'];
        $nombre_curso_code_sence = $data['nombre_curso_code_sence'];
        $cantidad_hora_sence = $data['cantidad_hora_sence'];
        $valor_hora_sence = $data['valor_hora_sence'];
        $fecha_solicitud = $data['fecha_solicitud'];
        $fecha_acreditacion = $data['fecha_acreditacion'];
        $fecha_vigencia = $data['fecha_vigencia'];
        $id_otec = $data['id_otec'];
        $id_modalidad_sence = $data['id_modalidad_sence'];
        $resolucion_excenta = $data['resolucion_excenta'];
        $valor_efectivo_participante = $data['valor_efectivo_participante'];
        $valor_total_curso = $data['valor_total_curso'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_code_sence_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_code_sence,
            $nombre_curso_code_sence,
            $cantidad_hora_sence,
            $valor_hora_sence,
            $fecha_solicitud,
            $fecha_acreditacion,
            $fecha_vigencia,
            $id_otec,
            $id_modalidad_sence,
            $resolucion_excenta,
            $valor_efectivo_participante,
            $valor_total_curso,
            $user_id,
            $user_perfil
        ));

        // var_dump($query);
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_sence_docs($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2017-01-26
         */

        $codigo_sence = $data['codigo_sence'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_sence_select_docs(?,?,?)";

        $query = $this->db->query($sql, array($codigo_sence, $user_id, $user_perfil));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_document_sence($data) {

        $id_code_sence = $data['id_code_sence'];
        $nombre_archivo = $data['nombre_archivo'];
        $ruta_archivo = $data['ruta_archivo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_document_by_sence_code(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_code_sence, $nombre_archivo, $ruta_archivo, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_codigo_sence_by_cursoCBX_curso($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista codigos sence de los cursos
          Fecha Actualiza: 2017-01-12
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = "CALL sp_code_sence_by_curso(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_codigo_sence_by_cursoFicha($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista codigos sence de los cursos
          Fecha Actualiza: 2017-01-12
         */
        $id_curso = $data['id_curso'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = "CALL sp_code_sence_by_curso_ficha(?,?,?);";

        $query = $this->db->query($sql, array($id_curso, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
