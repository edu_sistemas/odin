<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-08-23 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-08-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Negocio_model
 */
class Negocio_model extends MY_Model {

    function get_arr_listar_sub_lineas_negocio($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las modalidades
          Fecha Actualiza: 2016-12-01
         */
          $id_sub = "";
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_sub_linea_negocio_select_cbx(?,?,?);";
        $query = $this->db->query($sql, array($id_sub,$user_id, $user_perfil)
        );
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
