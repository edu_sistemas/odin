<?php

/**
 * Password_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-03-28 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-03-28 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Password_model
 */
class Password_model extends MY_Model {

    // 2016-09-23  
    // verifica la existencia del usaurio
    function set_reset_password($data) {
 
        $user_name = $data['rut_usuario'];
        
        $rut = explode("-", $user_name);

        $sql = "CALL sp_g_recuperar_password(?);";
        $query = $this->db->query($sql, array($rut[0]));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
