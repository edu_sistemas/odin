<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 * Ultima edicion:	2018-01-04 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2017-25-01 [Don  Luis Jarpa] <lajrpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Comisiones_model
 */
class Comisiones_model extends MY_Model {

    function get_arr_listar_tipo_usuarios($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_tipo_usuario_comision(?,?)";
        $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_usuarios_comision($data) {

        $id_tipo_usuario = $data['id_tipo_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_usuario_comision(?,?,?)";
        $query = $this->db->query($sql, array(
            $id_tipo_usuario,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* comisiones jefges ejecutivos */

    function get_arr_listar_comision_jefe_ejecutivo($data) {

        $id_usuario = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_comision_jefe_ejecutivo(?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array($id_usuario, $mes, $anio, $fecha_ant, $fecha_m, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_detalle_comision_jefe_ejecutivo($data) {

        $id_usuario = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_comision_detalle_jefe_ejecutivo(?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $id_usuario,
            $mes,
            $anio,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* fin comisiones jefes ejecutivos */


    /* comisiones marketing */

    function get_arr_listar_comision_marketing($data) {

        $id_usuario = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_comision_marketing(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_usuario, $mes, $anio, $fecha_ant, $fecha_m, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_detalle_comision_marketing($data) {

        $id_usuario = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_comision_detalle_marketing(?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $id_usuario,
            $mes,
            $anio,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* fin comisiones marketing */

    /* inicio comisiones ejecutivos */

    function get_arr_listar_comision_ejecutivo($data) {

        $id_ejecutivo = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_comision_ejecutivo(?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array($id_ejecutivo, $mes, $anio, $fecha_ant, $fecha_m, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_detalle_comision_ejecutivo($data) {

        $id_ejecutivo = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_comision_detalle_ejecutivo(?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $id_ejecutivo,
            $mes,
            $anio,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* fin comisiones ejecutivos */

    /* inicio comisiones backoffices */

    function get_arr_listar_comision_back_office($data) {
        $id_ejecutivo = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_comision_back_office(?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array($id_ejecutivo, $mes, $anio, $fecha_ant, $fecha_m, $user_id, $user_perfil));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_detalle_comision_back_office($data) {

        $id_ejecutivo = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_comision_detalle_back_office(?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $id_ejecutivo,
            $mes,
            $anio,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* fin comisiones backoffices */




    /* consultas comisiones */

    function get_arr_listar_comision_usuarios($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        
        $sql = "CALL sp_select_comisiones(?,?)";
        $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function insert_new_comision($data){

        $c_curso = $data['c_curso'];
        $c_arriendo = $data['c_arriendo'];
        $c_jefatura = $data['c_jefatura'];
        $c_otro = $data['c_otro'];
        $f_inicio = $data['f_inicio'];
        $id_user = $data['id_user'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $date = new DateTime($f_inicio);
        
        
        $sql = "CALL sp_comision_insert(?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $c_curso ,
            $c_arriendo ,
            $c_jefatura ,
            $c_otro ,
            $date->format('Y-m-d'),            
            $id_user,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_grupo_comercial($data) {


        $id_usuario = $data['id_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_grupo_comercial_by_usuario(?,?,?)";
        $query = $this->db->query($sql, array(
            $id_usuario,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    /*lista los descuentos de comisiones back office*/    
function get_arr_listar_rectificaciones_back_office($data) {
    $cbx_tipo_usuario = $data['cbx_tipo_usuario'];
    $id_ejecutivo = $data['id_ejecutivo'];
    $fecha_ant = $data['fecha_ant'];
    $fecha_m = $data['fecha_m'];
    $user_id = $data['user_id'];
    $user_perfil = $data['user_perfil'];
    $sql = " CALL sp_filtro_rectificaciones_back_office_select(?,?,?,?,?)";
    $query = $this->db->query($sql, array( $fecha_ant, $fecha_m, $id_ejecutivo, $user_id, $user_perfil));
    $result = $query->result_array();
    $query->next_result();
    $query->free_result();
    return $result;
}
    /*lista los descuentos de comisiones ejecutivo*/    
    function get_arr_listar_rectificaciones_ejecutivo($data) {
        $cbx_tipo_usuario = $data['cbx_tipo_usuario'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_filtro_rectificaciones_ejecutivo_select(?,?,?,?,?)";
        $query = $this->db->query($sql, array( $fecha_ant, $fecha_m, $id_ejecutivo, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

        /*lista los descuentos de comisiones ejecutivo*/    
    function get_arr_listar_rectificaciones_jefe_ejecutivo($data) {
        $cbx_tipo_usuario = $data['cbx_tipo_usuario'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_filtro_rectificaciones_jefe_ejecutivo_select(?,?,?,?,?)";
        $query = $this->db->query($sql, array( $fecha_ant, $fecha_m, $id_ejecutivo, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /*lista los descuentos de comisiones ejecutivo*/    
    function get_arr_listar_rectificaciones_marketing($data) {
        $cbx_tipo_usuario = $data['cbx_tipo_usuario'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_filtro_rectificaciones_marketing_select(?,?,?,?,?)";
        $query = $this->db->query($sql, array( $fecha_ant, $fecha_m, $id_ejecutivo, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_historico_back_office($data) {
        $id_ejecutivo = $data['id_ejecutivo'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_historico_back_office(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array( $id_ejecutivo, $fecha_ant, $fecha_m, $mes, $anio, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_historico_ejecutivo($data) {
        $id_ejecutivo = $data['id_ejecutivo'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_historico_ejecutivo(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array( $id_ejecutivo, $fecha_ant, $fecha_m, $mes, $anio, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_historico_jefe_ejecutivo($data) {
        $id_ejecutivo = $data['id_ejecutivo'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_historico_jefe_ejecutivo(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_ejecutivo, $fecha_ant, $fecha_m, $mes, $anio, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_historico_marketing($data) {
        $cbx_tipo_usuario = $data['id_tipo'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_historico_marketing(?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($cbx_tipo_usuario,$id_ejecutivo, $fecha_ant, $fecha_m, $mes, $anio, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_historico_comisiones($data) {
        $cbx_tipo_usuario = $data['id_tipo'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_historico_comisiones(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($cbx_tipo_usuario,$id_ejecutivo, $mes, $anio, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_historico_rectificaciones($data) {
        $cbx_tipo_usuario = $data['id_tipo'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_historico_rectificaciones(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($cbx_tipo_usuario,$id_ejecutivo, $mes, $anio, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_consulta_ultimo_historico_comisiones($data){
        $cbx_tipo_usuario = $data['id_tipo'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_ultimo_historico_comisiones(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($cbx_tipo_usuario,$id_ejecutivo, $mes, $anio, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;  
    }

    function get_consulta_ultimo_historico_rectificaciones($data){
        $cbx_tipo_usuario = $data['id_tipo'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $mes = $data['mes'];
        $anio = $data['anio'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_ultimo_historico_rectificaciones(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($cbx_tipo_usuario,$id_ejecutivo, $mes, $anio, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;  
    }
}
