<?php

/**
 * Response Model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-10-12 [Christopher Villarroel] <cvillarroel@edutecno.com>
 * Fecha creacion:  2018-10-12 [Christopher Villarroel] <cvillarroel@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class ResponseModel extends MY_Model
{
	public $result; 
    public $response; 
    public $message; 
    public $href; 
    public $function; 

 	function __construct() {
 		$this->response = false;
 		$this->message = "Ops, something was wrong... try again";
 	} 

 	/* Return response and/or message */
 	public function setResponse($r, $m){
 		$this->response = $r;
 		$this->message = $m;
 		
 		if(!$m){
 			$this->message = "Tu acción ha sido completada exitosamente";
 		}

 		if(!$r && !$m){
 			$this->message = "Ops, algo ha salido mal...";
 		}
 	}

 	/* Return something like var or obj without a message */
 	public function setResult($r, $rt){
 		$this->response = $r;
 		$this->result = $rt;
 		$this->message = null;
 	}

}
