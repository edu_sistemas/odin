<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-02 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-12-02 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Perfil_Model
 */
class Curso_model extends MY_Model {

    function get_arr_listar_curso_cbx_ficha($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */


        $id_modalidad = $data['id_modalidad'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_curso_select_cbx_ficha(?,?,?)";
        $query = $this->db->query($sql, array($id_modalidad, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_curso_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = " CALL sp_curso_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_curso($data) {

        /*
          Ultimo usuario  modifica:  Marcelo Romero
          Descripcion : Lista de los Cursos
          Fecha Actualiza: 2016-12-05
         */

        $nombre_curso = $data['nombre_curso'];
        $id_modalidad = $data['id_modalidad'];
        $id_nivel = $data['id_nivel'];
        $id_version = $data['id_version'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_curso_select(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($nombre_curso, $id_modalidad, $id_nivel, $id_version, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_curso_cbx($data) {
        /*
          lista los usuarios
          Descripcion : Lista los perfiles y la descripcion en un cbx
          Fecha Actualiza: 2016-11-09
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_curso_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_curso($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una nueva  Modalidad
          Fecha Actualiza: 2016-12-01
         */
        $nombre_curso = $data['nombre_curso'];
        $descripcion_curso = $data['descripcion_curso'];
        $duracion_curso = $data['duracion_curso'];
        $id_modalidad = $data['id_modalidad'];
        $id_codigo_sence = $data['id_codigo_sence'];
        $id_familia = $data['id_familia'];
        $id_nivel = $data['id_nivel'];
        $sub_linea_negocio = $data['sub_linea_negocio'];
        $id_grado_bono = $data['id_grado_bono'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_curso_insert(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $nombre_curso,
            $descripcion_curso,
            $duracion_curso,
            $id_codigo_sence,
            $id_modalidad,
            $id_familia,
            $id_nivel,
            $sub_linea_negocio,
            $user_id,
            $user_perfil,
            $id_grado_bono
                )
        );



        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_select_curso_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los titulos del menu padre para el cbx
          Fecha Actualiza: 2016-12-07
         */

        $id_modalidad = $data['id_modalidad'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_code_sence_select_cbx(?,?,?);";
        $query = $this->db->query($sql, array($id_modalidad, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_curso_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupera curso desde BD
          Fecha Actualiza: 2016-12-05
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_curso_select_by_id(?,?,?);";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_curso($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2016-11-02
         */
        $id_curso = $data['id_curso'];
        $nombre_curso = $data['nombre_curso'];
        $descripcion_curso = $data['descripcion_curso'];
        $id_codigo_sence = $data['id_codigo_sence'];
        $duracion_curso = $data['duracion_curso'];
        $id_modalidad = $data['id_modalidad'];
        $id_familia = $data['id_familia'];
        $id_nivel = $data['id_nivel'];
        $sub_linea_negocio = $data['sub_linea_negocio'];
        $id_grado_bono = $data['id_grado_bono'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_curso_update(?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_curso,
            $nombre_curso,
            $descripcion_curso,
            $duracion_curso,
            $id_codigo_sence,
            $id_modalidad,
            $id_familia,
            $id_nivel,
            $sub_linea_negocio,
            $user_id,
            $user_perfil,
            $id_grado_bono
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_curso_estado($datos) {


        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Modificar un módulo
          Fecha Actualiza: 2016-11-22
         */


        $id_curso = $datos['id_curso_m'];
        $estado_curso = $datos['estado_curso_m'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_curso_estado_change(?,?,?,?);";
        $query = $this->db->query($sql, array($id_curso, $estado_curso, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_grado_bonos() {
        $sql = "CALL sp_cc_grado_bono_select_cbx()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
