<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-31 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-01-31 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Perfil_Model
 */
class Clase_model extends MY_Model {

    function get_arr_listar_ficha_curso_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los numero deficha que están dentro de la tabla ficha_reserva
          Fecha Actualiza: 2017-01-12
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_curso_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_clase($data) {
        /*
          Ultimo usuario  modifica: Marcelo
          Descripcion : Lista los datos necesario de la clase para asignar un relator
          Fecha Actualiza: 2017-01-31
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];



        $sql = "CALL sp_ficha_select_variante(?,?,?);";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_clase_excel($data) {
        /*
          Ultimo usuario  modifica: Marcelo
          Descripcion : Lista los datos necesario de la clase para asignar un relator
          Fecha Actualiza: 2017-01-31
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];



        $sql = "CALL sp_ficha_select_curso_asignado(?,?,?);";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_reserva_by_id($data) {
        /*
          Ultimo usuario  modifica: Marcelo
          Descripcion : Lista los datos necesario de la clase para asignar un relator
          Fecha Actualiza: 2017-01-31
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = "CALL sp_select_reserva_asg_relator(?,?,?);";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_docente_disponible($data) {
        /*
          Ultimo usuario  modifica: Marcelo
          Descripcion : Lista los datos necesario de la clase para asignar un relator
          Fecha Actualiza: 2017-02-01
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = " CALL sp_usuario_docente_listar_lbx(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_docente_asignado($data) {
        /*
          Ultimo usuario  modifica: Marcelo
          Descripcion : Lista los datos necesario de la clase para mostrar los docentes asignados
         * Fecha Actualiza: 2017-02-02
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = " CALL sp_usuario_docente_listar_asignado_lbx(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_asignar_docenete_clase($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade docentes a la clase.
          Fecha Actualiza: 2017-02-02
         */
        $id_ficha = $data['id_ficha'];
        $id_usuario = $data['id_usuario'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_docente_asignacion_insert(?,?,?,?);";
        $query = $this->db->query($sql, array($id_ficha, $id_usuario, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_remover_docente($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Elimina los docentes que estan en el lbx de asignados.
          Fecha Actualiza: 2017-02-02
         */

        $id_ficha = $data['id_ficha'];
        $id_usuario = $data['id_usuario'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_relator_delete(?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $id_usuario, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_confirmar_docente_estado($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : comfirmna ha los docentes que se asignaron al curso
          Fecha Actualiza: 2017-02-03
         */
        $id_ficha = $data['id_ficha'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_relator_ficha_update(?,?,?);";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
