<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class DepartamentoTecnico_model extends MY_Model {

// 	function __construct()
// 	{
// 		parent::__construct();
// 	}	

    function getFichas($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_informe_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getInformes($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getDatosInforme($data) {


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $num_ficha = $data['num_ficha'];
        $empresa = $data['empresa'];

        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];

        $sql = "CALL sp_informe_get_datos(?,?,?,?,?,?)";

        $query = $this->db->query($sql, array($num_ficha, $empresa, $fecha_inicio, $fecha_termino, $user_id, $user_perfil));
        
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getAlumnosInforme($data) {

        $id_ficha = $data['id_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_informe_get_alumnos(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_curso_buscar($data) {

        /*
          Ultimo usuario  modifica:  Marcelo Romero
          Descripcion : Lista de los Cursos
          Fecha Actualiza: 2016-12-05
         */



        $fecha_reporte = $data['fecha_reporte'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_actividades_vigente_select(?,?,?);";

        $query = $this->db->query($sql, array($fecha_reporte,$user_id,$user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

}

/*InformePresencial.php
 * el modelo
 */