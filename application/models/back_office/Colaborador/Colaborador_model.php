<?php

/**
 * Colaborador_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-01-20 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Perfil_Model
 */
class Colaborador_Model extends MY_Model {

      function set_arr_agregar_colaborador($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Añade un nuevo Colaborador
          Fecha Actualiza: 2017-01-19
         */
        $rut              = $data['rut'];
        $dv               = $data['dv'];
        $nombre           = $data['nombre'];
        //$seg_nombre       = $data['seg_nombre'];
        $apellido_pat     = $data['apellido_pat'];
        $apellido_mat     = $data['apellido_mat'];
        $nacionalidad     = $data['nacionalidad'];
        $genero           = $data['genero'];
        $fechaNac         = $data['fechaNac'];
        $estado_civil      = $data['estado_civil'];

        $user_id          = $data['user_id'];
        $user_perfil      = $data['user_perfil'];

        $sql = " CALL sp_colaborador_insert(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql,array(
          $rut,              
          $dv,               
          $nombre,           
          //$seg_nombre,       
          $apellido_pat,     
          $apellido_mat,     
          $nacionalidad,     
          $genero,           
          $fechaNac,        
          $estado_civil,      
          $user_id,
          $user_perfil ));

        $result = $query->result_array();

        return $result;
    }


    function get_arr_colaborador_listar($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Lista los Colaboradores
          Fecha Actualiza: 2017-01-19
         */

        $rut = $data['rut'];
        $nombre = $data['nombre'];
        $estado = $data['estado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_colaborador_select_filtrar(?,?,?,?,?);";

        $query = $this->db->query($sql,array($rut,$nombre,$estado,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

        function set_arr_editar_colaborador($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2016-12-06
         */
        	
        $id_colaborador = $data['id_colaborador'];
        $rut = $data['rut'];
        $dv = $data['dv'];
        $nombre = $data['nombre'];
        $apellido_paterno = $data['apellido_paterno'];
        $apellido_materno = $data['apellido_materno'];
        $fecha_nacimiento = $data['fecha_nacimiento'];
        $genero = $data['genero'];
        $estado_civil = $data['estado_civil'];
        $nacionalidad = $data['nacionalidad'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_colaborador_update(?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql,array(
        		$id_colaborador,
        		$rut,
        		$dv,
        		$nombre,
        		$apellido_paterno,
        		$apellido_materno,
        		$fecha_nacimiento,
        		$genero,
        		$estado_civil,
        		$nacionalidad,
        		$user_id,
        		$user_perfil
        ));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function sp_colaborador_select_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupera curso desde BD
          Fecha Actualiza: 2016-12-05
         */
        $id = $data['id_colaborador'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_colaborador_select_by_id('" . $id . "','" . $user_id . "','" . $user_perfil . "');";

        $query = $this->db->query($sql);
        $row_count = $query->num_rows();
        $result = false;
        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }
  
    function set_sp_colaborador_change_status($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia el estado de los usuarios
          Fecha Actualiza: 2016-11-25
         */
        $id_colaborador = $data['id_colaborador'];


        $estado = $data['estado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_colaborador_change_status(?,?,?,?)";

        $query = $this->db->query($sql,array($id_colaborador,$estado,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_direccion_colaborador($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Añade una nueva direccion a un colaborador
          Fecha Actualiza: 2017-01-19
         */
        $id_colaborador             = $data['id_c'];
        $calle                      = $data['d_calle'];
        $numero                     = $data['d_numero'];
        $departamento               = $data['d_departamento'];
        $comuna                     = $data['d_comuna'];
        $latitud                    = $data['latitud'];
        $longitud                   = $data['longitud'];

        $user_id          = $data['user_id'];
        $user_perfil      = $data['user_perfil'];

        $sql = " CALL sp_colaborador_direccion_insert(?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql,array(
        $id_colaborador,
        $calle,
        $numero,
        $departamento,
        $comuna,
        $latitud,
        $longitud,
        $user_id,
        $user_perfil));

        $result = $query->result_array();

        return $result;
    }
    
    
    
    function set_sp_colaborador_insert_telefono($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-19
    	 */
    	$id_colaborador             = $data['id_colaborador'];
    	$tipo                      	= $data['tipo'];
    	$numero                     = $data['numero'];
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_sp_colaborador_insert_telefono(?,?,?,?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$id_colaborador,
    			$tipo,
    			$numero,
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }
    
    function set_sp_colaborador_insert_mail($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-19
    	 */
    	$id_colaborador             = $data['id_colaborador'];
    	$tipo                      	= $data['tipo'];
    	$email                    = $data['email'];
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_sp_colaborador_insert_mail(?,?,?,?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$id_colaborador,
    			$tipo,
    			$email,
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }
    
    function set_sp_colaborador_delete_telefono($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-19
    	 */
    	$id_telefono             = $data['id_telefono'];
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_colaborador_delete_telefono(?,?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$id_telefono,
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }
    
    function set_sp_colaborador_delete_mail($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-19
    	 */
    	$id_email             = $data['id_email'];
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_colaborador_delete_mail(?,?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$id_email,
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }
    
    
    
    function set_sp_colaborador_update_direccion($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-19
    	 */
    	$id_direccion     = $data['id_direccion'];
    	$calle            = $data['calle'];
    	$numero           = $data['numero'];
    	$departamento     = $data['departamento'];
    	$id_comuna        = $data['id_comuna'];
    	$latitud          = $data['latitud'];
    	$longitud         = $data['longitud'];
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_colaborador_update_direccion(?,?,?,?,?,?,?,?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$id_direccion,
    			$calle,
    			$numero,
    			$departamento,
    			$id_comuna,
    			$latitud,
    			$longitud,
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }
    
    function set_sp_colaborador_delete_direccion($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-19
    	 */
    	$id_direccion            = $data['id_direccion'];
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_colaborador_delete_direccion(?,?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$id_direccion,
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }

    function get_arr_listar_direcciones_by_id_cbx($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Añade una nueva direccion a un colaborador
          Fecha Actualiza: 2017-01-26
         */
        $id_colaborador             = $data['id_colaborador'];

        $user_id          = $data['user_id'];
        $user_perfil      = $data['user_perfil'];

        $sql = " CALL sp_colaborador_select_direcciones_ById_CBX(?,?,?);";

        $query = $this->db->query($sql,array(
        $id_colaborador,
        $user_id,
        $user_perfil));

        $result = $query->result_array();

        return $result;
    }
    
    
    
    function get_arr_listar_parentesco_cbx($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-26
    	 */    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_colaborador_select_parentesco_CBX(?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }
    
    
    
    function get_sp_colaborador_select_telefonos($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-26
    	 */
    	$id_colaborador   = $data['id_colaborador'];
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_colaborador_select_telefonos(?,?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$id_colaborador,
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }
    
    function get_sp_colaborador_select_mails($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-26
    	 */
    	$id_colaborador   = $data['id_colaborador'];
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_colaborador_select_mails(?,?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$id_colaborador,
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }
    
    function get_sp_tipo_dato_colaborador_cbx($data) {
    	/*
    	 Ultimo usuario  modifica: David De Filippi
    	 Descripcion : Añade una nueva direccion a un colaborador
    	 Fecha Actualiza: 2017-01-26
    	 */
    	
    	$user_id          = $data['user_id'];
    	$user_perfil      = $data['user_perfil'];
    	
    	$sql = " CALL sp_tipo_dato_colaborador_cbx(?,?);";
    	
    	$query = $this->db->query($sql,array(
    			$user_id,
    			$user_perfil));
    	
    	$result = $query->result_array();
    	
    	return $result;
    }

    function get_arr_direccion_by_id($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Añade una nueva direccion a un colaborador
          Fecha Actualiza: 2017-01-26
         */
        $id_direccion             = $data['id_direccion'];

        $user_id          = $data['user_id'];
        $user_perfil      = $data['user_perfil'];

        $sql = " CALL sp_colaborador_select_direccion_ById(?,?,?);";

        $query = $this->db->query($sql,array(
        $id_direccion,
        $user_id,
        $user_perfil));

        $result = $query->result_array();

        return $result;
    }

}
