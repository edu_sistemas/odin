<?php

/**
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-06-20 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2017-06-20 [Luis Jarpa] <ljarpa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Timeline_model
 */
class Timeline_model extends MY_Model {

    function Timeline_get_fichas_time_line($data) {
        /*
          Ultimo usuario  modifica:    Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_timeline_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function Timeline_get_sp_ficha_select_eventos_by_id($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_preview_eventos_select(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function Timeline_get_sp_ficha_hitos_by_id($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_time_line_hitos(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function Timeline_get_sp_ficha_select_log_by_id($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_log_ficha_time_line_by_id(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
