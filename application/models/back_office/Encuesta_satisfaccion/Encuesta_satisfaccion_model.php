<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Empresa_contacto_model
 */
class Encuesta_satisfaccion_model extends MY_Model {

    function get_arr_listar_fichas($data){

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_listar_encuesta_satisfaccion(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        
        return $result;
    }

    function get_arr_listar_encuestas_by_ficha($data){
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_listar_encuestas_by_ficha(?,?,?)";

        $query = $this->db->query($sql,array($id_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_encuesta_satisfaccion($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_encuesta_satisfaccion($data){
            $id_ficha = $data['id_ficha'];
            $id_relator = $data['id_relator'];
            $respuesta1 = $data['respuesta1'];
            $respuesta2 = $data['respuesta2'];
            $respuesta3 = $data['respuesta3'];
            $respuesta4 = $data['respuesta4'];
            $respuesta5 = $data['respuesta5'];
            $respuesta6 = $data['respuesta6'];
            $respuesta7 = $data['respuesta7'];
            $respuesta8 = $data['respuesta8'];
            $respuesta9 = $data['respuesta9'];
            $respuesta10 = $data['respuesta10'];
            $respuesta11 = $data['respuesta11'];
            $respuesta12 = $data['respuesta12'];
            $respuesta13 = $data['respuesta13'];
            $respuesta14 = $data['respuesta14'];
            $respuesta15 = $data['respuesta15'];
            $respuesta16 = $data['respuesta16'];
            $respuesta17 = $data['respuesta17'];
            $respuesta18 = $data['respuesta18'];
            $respuesta19 = $data['respuesta19'];
            $respuesta20 = $data['respuesta20'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

            $sql = "CALL sp_encuesta_satisfaccion_insertar(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            $query = $this->db->query($sql,array(+
                $id_ficha,
                $id_relator,
                $respuesta1,
                $respuesta2,
                $respuesta3,
                $respuesta4,
                $respuesta5,
                $respuesta6,
                $respuesta7,
                $respuesta8,
                $respuesta9,
                $respuesta10,
                $respuesta11,
                $respuesta12,
                $respuesta13,
                $respuesta14,
                $respuesta15,
                $respuesta16,
                $respuesta17,
                $respuesta18,
                $respuesta19,
                $respuesta20,
                $user_id,
                $user_perfil
            ));
    
            $result = $query->result_array();
            $query->next_result();
            $query->free_result();
    
            return $result;
    }

    function get_relatores_by_ficha($data){
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_select_by_ficha_id_cbx(?,?,?)";

        $query = $this->db->query($sql,array($id_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
    function get_encuesta_by_id($data){
        $id_encuesta = $data['id_encuesta'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion_by_id(?,?,?)";

        $query = $this->db->query($sql,array($id_encuesta,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
}
