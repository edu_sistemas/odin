<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
* Ultima edicion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class Log_model extends MY_Model {

    function get_arr_listar_log($data) {

        /*
          Ultimo usuario  modifica:  David De Filippi
          Descripcion : Lista las log
          Fecha Actualiza: 2016-12-02
         */

        $p_fecha_inicio = $data['start'];
        $p_fecha_fin = $data['end'];
        $p_id_usuario = $data['usuario'];
        $p_xd_perfil = $data['perfil'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_log_select(?,?,?,?,?,?);";



        $query = $this->db->query($sql,array(
            $p_fecha_inicio,
            $p_fecha_fin,
            $p_id_usuario,
            $p_xd_perfil,
            $user_id,
            $user_perfil
            ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
 

    function get_arr_log_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_log = $data['id_log'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_log_select_by_id('" . $id_log . "','" . $user_id . "','" . $user_perfil . "');";

        $query = $this->db->query($sql);
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_log($data) {
        /*
          Ultimo usuario modifica: David De Filippi
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-12-02
         */

        $id_log = $data['id_log'];  
        $p_rut_log = $data['rut_log'];
        $p_razon_social_log = $data['razon_social_log'];
        $p_giro_log = $data['giro_log'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_log_update( '" . $id_log. "',   
                                        '" . $p_rut_log . "',                                        
                                        '" . $p_razon_social_log . "',
                                        '" . $p_giro_log . "',
                                        '" . $user_id . "',
                                        '" . $user_perfil . "');";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
 
}
