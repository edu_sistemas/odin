<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 [Luis Jarpa] <lajrpa@edutecno.com>
 * Fecha creacion:  2017-01-20 [Luis Jarpa] <lajrpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Version_model
 */
class Version_model extends MY_Model {

    function get_arr_listar_version_cbx($data) {


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_version_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_version_cbx_by_curso($data) {

        $id_curso = $data['id_curso'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        
        $sql = "CALL sp_version_select_cbx_by_curso(?,?,?);"; 
        $query = $this->db->query($sql, array(
            $id_curso,
            $user_id,
            $user_perfil
                )
        );
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_insert_version_curso($data) {

        $id_version = $data['id_version'];
        $id_curso = $data['id_curso'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_version_insert_curso(?,?,?,?);";
        $query = $this->db->query($sql, array(
            $id_version,
            $id_curso,
            $user_id,
            $user_perfil
                )
        );
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_delete_version_curso($data) {


        $id_curso = $data['id_curso'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_version_delete_curso(?,?,?);";
        $query = $this->db->query($sql, array(
            $id_curso,
            $user_id,
            $user_perfil
                )
        );
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
