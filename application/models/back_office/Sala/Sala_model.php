<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 [Luis Jarpa] <lajrpa@edutecno.com>
 * Fecha creacion:  2017-01-20 [Luis Jarpa] <lajrpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Sala_model
 */
class Sala_model extends MY_Model {

    function get_arr_listar_Sala($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sala_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_sede_cbx($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sede_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_sala($data) {

        $nombre = $data['nombre'];
        $nSala = $data['nSala'];
        $detalle = $data['detalle'];
        $capacidad = $data['capacidad'];
        $sede = $data['sede'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_sala_insert(?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $nombre,
            $nSala,
            $detalle,
            $capacidad,
            $sede,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_programa_agregar($data) {

        $nombre_programa = $data['nombre_programa'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_programa_agregar(?,?,?);";

        $query = $this->db->query($sql, array(
            $nombre_programa,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_programa_sala_agregar($data) {

        $id_programa = $data['id_programa'];
        $id_sala = $data['id_sala'];
        $observacion = $data['observacion'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_programa_sala_agregar(?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_programa,
            $id_sala,
            $observacion,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_programa_sala_update($data) {

        $id_programa = $data['id_programa'];
        $id_programa_sala = $data['id_programa_sala'];
        $observacion = $data['observacion'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_programa_sala_update(?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_programa,
            $id_programa_sala,
            $observacion,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_sala($data) {
        $id_sala = $data['id_sala'];
        $nombre = $data['nombre'];
        $nSala = $data['nSala'];
        $detalle = $data['detalle'];
        $capacidad = $data['capacidad'];
        $sede = $data['sede'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_sala_update(?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_sala,
            $nombre,
            $nSala,
            $detalle,
            $capacidad,
            $sede,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_sala_ficha_cbx($data) {



        $id_sede = $data['id_sede'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sala_select_by_ficha_cbx('" . $id_sede . "','" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_programa_sala_desactivar($data) {

        $id_programa_sala = $data['id_programa_sala'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];



        $sql = "CALL sp_programa_sala_desactivar(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_programa_sala,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_sala_change_status($data) {

        $id_sala = $data['id_sala'];
        $estado = $data['estado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sala_estado_change('" . $id_sala . "','" . $estado . "','" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_sala_select_programas_by_id($data) {

        $id_sala = $data['id_sala'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sala_select_programas_by_id('" . $id_sala . "','" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_programa_select_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_programa_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_sala_by_id($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */
        $id_sala = $data['id_sala'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sala_select_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_sala, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
