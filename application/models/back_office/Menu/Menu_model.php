<?php

/**
 * Empresa_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-24-11 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Empresa_model
 */
class Menu_model extends MY_Model {

    function get_arr_listar_menu($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-25
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_menu_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_agregar_menu($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade un nuevo menú
          Fecha Actualiza: 2016-24-11
         */
        $nombre = $data['nombre'];
        $titulo = $data['titulo'];
        $descripcion = $data['descripcion'];
        $url = $data['url'];
        $icon = $data['icono'];
        $fk_id_menu_item_padre = $data['padre'];
        $fk_id_menu_item_estado = $data['estado'];
        $fk_id_modulo = $data['modulo'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_menu_item_insert(?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($nombre, $titulo, $descripcion, $url, $icon, $fk_id_menu_item_padre, $fk_id_menu_item_estado, $fk_id_modulo, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_menupadre_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los titulos del menu padre para el cbx
          Fecha Actualiza: 2016-11-24
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_menupadre_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_estado_menu_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los estados que puede tener el menu
          Fecha Actualiza: 2016-11-24
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_estado_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_editar_menu($datos) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Modificar un Menú
          Fecha Actualiza: 2016-11-25
         */

        $id_menu_item = $datos['id_m'];
        $nombre = $datos['nombre_m'];
        $titulo = $datos['titulo_m'];
        $descripcion = $datos['descripcion_m'];
        $url = $datos['url_m'];
        $icon = $datos['icon_m'];
        $fk_id_menu_item_padre = $datos['fk_padre_m'];
        $fk_id_menu_item_estado = $datos['fk_estado_m'];
        $fk_id_modulo = $datos['fk_modulo_m'];

        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];

        $sql = "CALL sp_menu_item_update(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_menu_item, $nombre, $titulo, $descripcion, $url, $icon, $fk_id_menu_item_padre, $fk_id_menu_item_estado, $fk_id_modulo, $user_id, $user_perfil));
 

        $result = $query->result_array();


        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_select_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera el módulo seleccionado según el id
          Fecha Actualiza: 2016-11-22
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_menu_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;
        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
