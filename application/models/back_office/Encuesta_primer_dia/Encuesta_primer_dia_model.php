<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Empresa_contacto_model
 */
class Encuesta_primer_dia_model extends MY_Model {

    function get_arr_listar_fichas($data){

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_listar_encuesta_satisfaccion(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_alumnos_by_ficha($data){
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumnos_select_by_ficha(?,?,?)";

        $query = $this->db->query($sql,array($id_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_encuesta_satisfaccion($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_encuesta_primer_dia($data){
            $id_ficha = $data['id_ficha'];
            $id_alumno = $data['id_alumno'];
            $id_encuestador = $data['id_encuestador'];
            $relator = $data['relator'];
            $equipos = $data['equipos'];
            $curso = $data['curso'];
            $comentarios = $data['comentarios'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

            $sql = "CALL sp_encuesta_primer_dia_insertar(?,?,?,?,?,?,?,?,?)";
            
            $query = $this->db->query($sql,array(
                $id_ficha,
                $id_alumno,
                $id_encuestador,
                $relator,
                $equipos,
                $curso,
                $comentarios,
                $user_id,
                $user_perfil
            ));
    
            $result = $query->result_array();
            $query->next_result();
            $query->free_result();
    
            return $result;
    }

    function get_relatores_by_ficha($data){
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_select_by_ficha_id_cbx(?,?,?)";

        $query = $this->db->query($sql,array($id_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
    function get_encuesta_by_id($data){
        $id_encuesta = $data['id_encuesta'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion_by_id(?,?,?)";

        $query = $this->db->query($sql,array($id_encuesta,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
}
