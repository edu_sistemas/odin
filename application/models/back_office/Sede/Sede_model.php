<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-13 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:   2017-01-13 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Sede Model
 */
class Sede_model extends MY_Model {

    function get_arr_listar_sede_cbx($data) {

        /*
          lista las sedes
          Descripcion : Lista los perfiles y la descripcion en un cbx
          Fecha Actualiza: 2017-01-13
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sede_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    
     function get_arr_lugar_ejecucion($data) {

        /*
          lista las sedes
          Descripcion : Lista los perfiles y la descripcion en un cbx
          Fecha Actualiza: 2017-01-13
         */

     $id_sede = $data['id_sede'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
      
        
        
        $sql = " CALL sp_sede_select_direccion(?,?,?)";
        $query = $this->db->query($sql, array($id_sede,$user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
