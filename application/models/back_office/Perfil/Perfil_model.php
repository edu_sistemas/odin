<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Perfil_Model
 */
class Perfil_Model extends MY_Model {

    function get_arr_listar_perfil($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-10-13
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_perfil_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_perfil_cbx($data) {
        /*
          lista los usuarios
          Descripcion : Lista los perfiles y la descripcion en un cbx
          Fecha Actualiza: 2016-11-09
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_perfil_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_perfil_rol($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-11-09
          HACE UN UPDATE A LA TABLA USUARIO
         */
        $id = $data['id'];
        $fk_id_perfil = $data['fk_id_perfil'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_usuario_asignacion_perfil(?,?,?,?);";

        $query = $this->db->query($sql, array($id, $fk_id_perfil, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_perfil($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Añade un nuevo  perfil
          Fecha Actualiza: 2016-10-13
         */

        $nombre = $data['nombre'];
        $descripcion = $data['descripcion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_perfil_insert(?,?,?,?)";
        $query = $this->db->query($sql, array($nombre, $descripcion, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_perfil_by_id($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-13
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_perfil_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;
        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_perfil($datos) {
        /*
          Ultimo usuario modifica: Luis Jarpa
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-10-13
         */
        $id = $datos['id'];
        $nombre = $datos['nombre'];
        $descripcion = $datos['descripcion'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];

        $sql = "CALL sp_perfil_update(?,?,?,?,?);";

        $query = $this->db->query($sql, array($id, $nombre, $descripcion, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
