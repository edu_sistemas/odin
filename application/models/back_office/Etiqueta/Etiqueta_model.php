<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-12 [Luis Jarpa] <lajrpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Etiqueta_model
 */
class Etiqueta_model extends MY_Model {

    function get_arr_etiquetas($data) {

        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_informacion_etiquetas(?,?,?);";

        $query = $this->db->query($sql, array(
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
