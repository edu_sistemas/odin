<?php

/**
 * Categoria_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Categoria_Model
 */
class Reportes_Distancia_model extends MY_Model {

    function getFichas($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_tablet_select(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }

    function getCodigosbyIDFicha($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_codigo_tablet_select_by_id_ficha(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }

    function getAlumnosbyIDFicha($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumnos_select_by_ficha_distancia(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function getReporteAlumno($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */

        $id_ficha = $data['id_ficha'];
        $rut_alumno = $data['rut_alumno'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumno_select_reporte(?,?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $rut_alumno, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }
    function getXMLbyRUT($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */

        $rut = $data['rut'];
        $rut_con_puntos = $data['rut_con_puntos'];

        $sql = "CALL sp_get_xml_distancia_by_rut(?,?);";

        $query = $this->db->query($sql, array($rut, $rut_con_puntos));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }

    function getUnidadesByIDFicha($data) {

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_curso_select_reporte_unidad_by_id_ficha(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }

    function getReporteDetalladoByIDFicha($data) {

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_curso_distancia_select_reporte_full_by_id_ficha(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function getUnidadesReporteAlumno($data) {

        $codigo_tablet = $data['codigo_tablet'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_curso_select_reporte_unidad_by_codigo_tablet(?,?,?);";

        $query = $this->db->query($sql, array($codigo_tablet, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }
    
    function getModulosByFichaYUnidad($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */

        $id_ficha = $data['id_ficha'];
        $id_unidad = $data['id_unidad'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_curso_select_modulo_by_id_ficha_y_id_unidad(?,?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $id_unidad, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }

    function getModulosReporteAlumno($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */
        $codigo_tablet = $data['codigo_tablet'];
        $id_unidad = $data['id_unidad'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_curso_select_modulo_by_codigo_alumno_y_id_unidad(?,?,?,?);";

        $query = $this->db->query($sql, array($codigo_tablet, $id_unidad, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }

    function generarReporteExcel($data)
    {
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_reporte_distancia_select_excel_by_ficha_id(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
}
