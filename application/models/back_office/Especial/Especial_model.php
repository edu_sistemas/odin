<?php

/**
 * Edutecno_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Ficha_Control_model
 */
class Especial_model extends MY_Model {

    function get_arr_listar_ficha_especial($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_select(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_ficha_especial($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data['start'];
        $end = $data['end'];
        $estado = $data['estado'];
        $empresa = $data['empresa'];
        $ejecutivo = $data['ejecutivo'];
        $periodo = $data['periodo'];
        $num_ficha = $data['ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_select_consulta(?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
                )
        );




        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id_especial($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_edutecno = $data['id_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_ficha_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_oc_especial($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_oc(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_datos_orden_compra_especial($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_alumno(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_documentos_resumen_orden_compra_especial($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_orden_compra_select_by_id_documentos(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id_resumen_especial($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de la ficha
          Fecha Actualiza: 2017-01-16
         */
        $id_edutecno = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id_resumen(?,?,?)";
        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_orden_compra_by_ficha_id_especial($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de las ordenes de compra
          Fecha Actualiza: 2017-01-16
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_ficha_id(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_orden_compra_accion_insert_alumnos_especial($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2017-01-17
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_insert_especial(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_orden_compra_accion_actualizar_alumnos_especial($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2017-01-17
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_accion_actualizar_especial(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();


        return $result;
    }

    function set_arr_orden_compra_accion_eliminar_alumnos_especial($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion :  elimina alumnos de una orden de cmopra (eliminado logico)
          Fecha Actualiza: 2017-01-17
         */ 
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_orden_compra_carga_alumno_accion_eliminar_especial(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
