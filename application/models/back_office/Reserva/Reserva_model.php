<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-03-22 [Jessica Roa] <jroa@edutecno.com> 
 * Fecha creacion:  2016-12-01 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Reserva_model
 */
class Reserva_model extends MY_Model {
    function get_arr_completar_calendar_by_sala($data) {
        /*
          Fecha Actualiza: 2016-12-26
         */
        $sala = $data['id_sala_h'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_reserva_select_by_sala(?,?,?)";
        $query = $this->db->query($sql, array($sala, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_calendario_get_feriados($data) {
        /*
          Fecha Actualiza: 2016-12-26
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_calendario_get_feriados()";
        $query = $this->db->query($sql, array());
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_completar_calendar_visualizacion($data) {
        /*
          Fecha Actualiza: 2016-12-26
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_reserva_select_visualizacion(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_completar_calendar_visualizacion_sin_confirmar($data) {
        /*
        Fecha creación: 22-03-2018
          Funcion que traer las reservas sin confirmar
          Jessica Roa jroa@edutecno.com
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sin_confirmar_reserva_select_visualizacion(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_completar_calendar_visualizacion_confirmadas($data) {
        /*
          Fecha Actualiza: 2016-12-26
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_reserva_confirmadas_select_visualizacion(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_salas_calendar_visualizacion($data) {
        /*
        Fecha actualizacion: 22-03-2018
          Funcion que traer las salas disponibles
          Jessica Roa jroa@edutecno.com
         */
        $id_sede = $data['id_sede'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_salas_select_visualizacion(?,?,?)";
        $query = $this->db->query($sql, array($id_sede, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_sala_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sala_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_empresas_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_empresa_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
       
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_reservas_por_conf($data) {
        $id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_reserva_select_por_confirmar(?,?,?)";
        $query = $this->db->query($sql, array($id_empresa, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_reserva_by_id_user($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_reserva_select_by_id(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();

        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_reserva_by_id_user_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_reserva_select_by_id_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();

        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_reserva_by_id($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_reserva = $data['id_reserva'];
        $sql = " CALL sp_reserva_select_by_id_reserva(?,?,?)";
        $query = $this->db->query($sql, array($id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_holding_by_id($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $sql = " CALL sp_holding_select_cbx_by_user(?,?,?)";
        $query = $this->db->query($sql, array($id_ejecutivo, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_empresa_by_id($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $id_holding = $data['id_holding'];
        $sql = " CALL sp_empresa_select_cbx_by_holding_by_user(?,?,?,?)";
        $query = $this->db->query($sql, array($id_ejecutivo,$id_holding, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_reserva_by_reserva($data) {
        /** Jessica Roa 27-03-2018
         * funcion para obtener detalles de una reserva
         * 
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_reserva = $data['id_reserva'];
        $id_detalle_sala = $data['id_detalle_sala'];
        $sql = " CALL sp_reserva_select_by_reserva(?,?,?,?)";
        $query = $this->db->query($sql, array($id_reserva, $id_detalle_sala, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }


    function get_bol_reserva_existe($data) {
        $finicio = $data['fInicio'];
        $ftermino = $data['fTermino'];
        $id_sala = $data['idSala'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_reserva_select_existe(?,?,?,?,?)";
        $query = $this->db->query($sql, array($finicio, $ftermino, $id_sala, $user_id, $user_perfil));
        $result = $query->result_array();

        $res = 0;
        $query->next_result();
        $query->free_result();
        if (count($result) > 0) {
            $res = 1;
        }
        return $res;
    }

    function get_id_insert_reserva($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $nReserva = $data['nReserva'];
        $dReserva = $data['dReserva'];
        $sql = " CALL sp_reserva_insert(?,?,?,?)";
        $query = $this->db->query($sql, array($nReserva, $dReserva, $user_id, $user_perfil));
        $result = $query->result_array();
 
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_id_insert_detalle_sala($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $direccion = $data['direccion'];
        $break = $data['breakIn'];
        $computadores = $data['computadores'];
        $idReserva = $data['id_reserva'];
        $sql = " CALL sp_reserva_insert_detalle_sala(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($direccion, $break, $computadores, $idReserva, $user_id, $user_perfil));
 
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_id_insert_capsula($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $horario_inicio = $data['horario_inicio'];
        $horario_termino = $data['horario_termino'];
        $estado = $data['estado'];
        $sql = " CALL sp_reserva_insert_capsula(?,?,?,?,?)";
        $query = $this->db->query($sql, array($horario_inicio, $horario_termino, $estado, $user_id, $user_perfil));
        $result = $query->result_array();
 
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    //para el insert full
    function get_id_insert_reservaCpasulaFull($data) {
    /* 22-03-2018 Jessica Roa modificado para guardar ejecutivo que realiza la reserva */
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];
        $lunes = $data['lunes'];
        $martes = $data['martes'];
        $miercoles = $data['miercoles'];
        $jueves = $data['jueves'];
        $viernes = $data['viernes'];
        $sabado = $data['sabado'];
        $domingo = $data['domingo'];
        $estado = $data['estado'];
        $id_sala = $data['id_sala'];
        $nombre_reserva = $data['nombre_reserva'];
        $detalle_reserva = $data['detalle_reserva'];
        $break = $data['break'];
        $computadores = $data['computadores'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $id_holding = $data['id_holding'];
        $id_empresa = $data['id_empresa'];
        $id_reserva = $data['id_reserva'];
        $direccion = $data['direccion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_reserva_full(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(
            $fecha_inicio,
            $fecha_termino,
            $lunes,
            $martes,
            $miercoles,
            $jueves,
            $viernes,
            $sabado,
            $domingo,
            $estado,
            $id_sala,
            $nombre_reserva,
            $detalle_reserva,
            $break,
            $computadores,
            $id_ejecutivo,
            $id_holding,
            $id_empresa,
            $id_reserva,
            $direccion,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }


    function upd_reserva($data) {
        $estado = 1;
        $nombre_reserva = $data['nombre_reserva'];
        $detalle_reserva = $data['detalle_reserva'];
        $id_holding = $data['id_holding'];
        $id_empresa = $data['id_empresa'];
        $id_reserva = $data['id_reserva'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_reserva_update(?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(
            $estado,
            $nombre_reserva,
            $detalle_reserva,
            $id_holding,
            $id_empresa,
            $id_reserva,
            $user_id,
            $user_perfil
            )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function upd_detalle_reserva($data) {
        $id_sala = $data['id_sala'];
        $break = $data['break'];
        $computadores = $data['computadores'];
        $id_reserva = $data['id_reserva'];
        $direccion = $data['direccion'];
        $id_detalle_reserva = $data['id_detalle_reserva'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        
        $sql = "CALL sp_detalle_reserva_update(?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(
            $id_sala,
            $break,
            $computadores,
            $direccion,
            $id_detalle_reserva,
            $id_reserva,
            $user_id,
            $user_perfil
            )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    
    function upd_horario_reserva($data) {
    /* 22-03-2018 Jessica Roa modificado para guardar ejecutivo que realiza la reserva */
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];
        $lunes = $data['lunes'];
        $martes = $data['martes'];
        $miercoles = $data['miercoles'];
        $jueves = $data['jueves'];
        $viernes = $data['viernes'];
        $sabado = $data['sabado'];
        $domingo = $data['domingo'];
        $id_detalle_reserva = $data['id_detalle_reserva'];
        $id_reserva = $data['id_reserva'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_horario_reserva_update(?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(
            $fecha_inicio,
            $fecha_termino,
            $lunes,
            $martes,
            $miercoles,
            $jueves,
            $viernes,
            $sabado,
            $domingo,
            $id_detalle_reserva,
            $id_reserva,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_disponibilidad_reservaCpasulaFull($data) {
    /* 22-03-2018 Jessica Roa modificado para guardar ejecutivo que realiza la reserva */
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];
        $fecha_inicio_hora_termino = $data['fecha_inicio_hora_termino'];
        $lunes = $data['lunes'];
        $martes = $data['martes'];
        $miercoles = $data['miercoles'];
        $jueves = $data['jueves'];
        $viernes = $data['viernes'];
        $sabado = $data['sabado'];
        $domingo = $data['domingo'];
        $id_sala = $data['id_sala'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_reserva_disponibilidad(?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(
            $fecha_inicio,
            $fecha_inicio_hora_termino,
            $fecha_termino,
            $lunes,
            $martes,
            $miercoles,
            $jueves,
            $viernes,
            $sabado,
            $domingo,
            $id_sala,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_id_disponibilidad($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $hinicio = $data['hInicio'];
        $htermino = $data['hTermino'];
        $idSala = $data['idSala'];
        $sql = "CALL sp_reserva_select_disponibilidad(?,?,?,?,?)";
        $query = $this->db->query($sql, array($hinicio, $htermino, $idSala, $user_id, $user_perfil));
        $result = $query->result_array();

        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_id_insert_detalle_disponibilidad($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $idDisponibilidad = $data['idDisponibilidad'];
        $idDetalleSala = $data['idDetalleSala'];
        $idCapsula = $data['idCapsula'];
        $sql = " CALL sp_reserva_inser_detalle_disponibilidad(?,?,?,?,?)";
        $query = $this->db->query($sql, array($idDisponibilidad, $idDetalleSala, $idCapsula, $user_id, $user_perfil));
        $result = $query->result_array();

        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_bol_reserva_existe_disponibilidad($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $hinicio = $data['hInicio'];
        $htermino = $data['hTermino'];
        $idSala = $data['idSala'];
        $sql = " CALL sp_reserva_select_existe_disponibilidad(?,?,?,?,?)";
        $query = $this->db->query($sql, array($hinicio, $htermino, $idSala, $user_id, $user_perfil));
        $result = $query->result_array();

        $this->db->last_query();
        $res = 0;
        $query->next_result();
        $query->free_result();
        if (count($result) >= 1) {
            $res = 1;
        }
        return $res;
    }

    function get_arr_fechas($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Fecha Actualiza: 2017-07-04
         */
        $id_capsula = $data['id_capsula'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_capsula_select_by_id(?,?,?);";
        $query = $this->db->query($sql, array($id_capsula, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function get_arr_fechas_bloques($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Fecha Actualiza: 2017-07-04
         */
        $id_detalle_sala = $data['id_detalle_sala'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_capsula_select_by_id_detalle_sala(?,?,?);";
        $query = $this->db->query($sql, array($id_detalle_sala, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_cant_alumnos($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Fecha Actualiza: 2017-07-04
         */
        $id_ficha = $data['id_ficha'];

        $sql = "SELECT f_get_cantidad_alumnos_centro_costo(?,NULL) AS 'cantidad';";
        $query = $this->db->query($sql, array($id_ficha));
        $result = $query->result_array();
        return $result;
    }

    function get_arr_detalles_reserva($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2017-07-04
         */

        $id_capsula = $data['id_capsula'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_reserva_detalle_capsula_select_by_id_capsula(?,?,?);";
        $query = $this->db->query($sql, array($id_capsula, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_detalles_reserva_by_idreserva($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2017-07-04
         */
        $id_reserva = $data['id_reserva'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_get_detalle_reserva_by_id_reserva(?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_cbx($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2017-01-05
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_modalidad_select_by_ficha(?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_reserva_sala_cbx($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2017-01-05
         */

        $id_sala = $data['id_sala'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_confirm_select_reserva_fichacbx(?,?,?);";
        $query = $this->db->query($sql, array($id_sala, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

       function get_arr_reserva_por_confirmar($data) {
        $id_reserva = $data['id_reserva'];
        $id_detalle_sala = $data['id_detalle_sala'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_reserva_select_datos_reserva_por_confirmar(?,?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $id_detalle_sala, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_elimina_capsula($datos) {
        $id_capsula = $datos['id_capsula'];
        $id_detalle_sala = $datos['id_detalle_sala'];
        $id_reserva = $datos['id_reserva'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_reserva_delete_capsula(?,?,?,?,?);";
        $query = $this->db->query($sql, array($id_capsula, $id_detalle_sala, $id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();
 
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_datos_eliminacion_capsula_email($datos) {
        $id_capsula = $datos['id_capsula'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_select_datos_capsula_eliminada_email(?,?,?);";
        $query = $this->db->query($sql, array($id_capsula, $user_id, $user_perfil));
        $result = $query->result_array();
       //var_dump($result);
       //die();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_elimina_bloque($datos) {
        $id_detalle_sala = $datos['id_detalle_sala'];
        $id_reserva = $datos['id_reserva'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_reserva_delete_bloque(?,?,?,?);";
        $query = $this->db->query($sql, array($id_detalle_sala, $id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();
        
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_datos_eliminacion_bloque_email($datos) {
        $id_detalle_sala = $datos['id_detalle_sala'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_select_datos_bloque_eliminado_email(?,?,?);";
        $query = $this->db->query($sql, array($id_detalle_sala, $user_id, $user_perfil));
        $result = $query->result_array();
       //var_dump($result);
       //die();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_elimina_reserva($datos) {
        $id_reserva = $datos['id_reserva'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        //buscar todas las capsulas de una reserva
        $sql = "CALL sp_reserva_delete_reserva_completa(?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_datos_eliminacion_reserva_email($datos) {
        $id_reserva = $datos['id_reserva'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_select_datos_reserva_eliminada_email(?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();
       //var_dump($result);
       //die();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_elimina_detalle_reserva($datos) {
        $id_reserva = $datos['id_reserva'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        //buscar todas las capsulas de una reserva
        $sql = "CALL sp_reserva_delete_detalle_reserva(?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_reservas_por_confirmar($data) {
        /*
        Ultimo usuario modifica: Marcelo Romero
        Descripcion : Edita perfil desde BD
        Fecha Actualiza: 2016-10-14
        */
        //Completar con los datos la reserva
        $id_reserva = $data['id_reserva'];
        $rut = $data['rut'];
        $nombre_fantasia = $data['nombre_fantasia'];
        $razon = $data['razon'];
        $holding = $data['holding'];
        $descripcion = $data['descripcion'];
        $giro = $data['giro'];
        $direccion = $data['direccion'];
        $comuna = $data['comuna'];
        $telefono_temporal = $data['telefono_temporal'];
        $direccion_factura = $data['direccion_factura'];
        $comuna_factura = $data['comuna_factura'];
        $direccion_despacho = $data['direccion_despacho'];
        $mail_sii = $data['mail_sii'];
        $nombre_contacto_cobranza = $data['nombre_contacto_cobranza'];
        $mail_contacto_cobranza = $data['mail_contacto_cobranza'];
        $telefono_contacto_cobranza = $data['telefono_contacto_cobranza'];
        $observaciones_glosa_facturacion = $data['observaciones_glosa_facturacion'];
        $requiere_orden_compra = $data['requiere_orden_compra'];
        $requiere_hess = $data['requiere_hess'];
        $requiere_numero_contrato = $data['requiere_numero_contrato'];
        $requiere_ota = $data['requiere_ota'];
        $logo = $data['logo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_empresa_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_empresa,
            $rut,
            $nombre_fantasia,
            $razon,
            $holding,
            $descripcion,
            $giro,
            $direccion,
            $comuna,
            $telefono_temporal,
            $direccion_factura,
            $comuna_factura,
            $direccion_despacho,
            $mail_sii,
            $nombre_contacto_cobranza,
            $mail_contacto_cobranza,
            $telefono_contacto_cobranza,
            $observaciones_glosa_facturacion,
            $requiere_orden_compra,
            $requiere_hess,
            $requiere_numero_contrato,
            $requiere_ota,
            $logo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_confirmar_bloque_reserva($datos) {
        $id_reserva = $datos['id_reserva'];
        $id_detalle_sala = $datos['id_detalle_sala'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_confirmar_bloque(?,?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $id_detalle_sala, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_datos_confirmaciion_bloque_email($datos) {
        $id_reserva = $datos['id_reserva'];
        $id_detalle_sala = $datos['id_detalle_sala'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_select_datos_bloque_confirmado_email(?,?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $id_detalle_sala, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_confirmar_reserva($datos) {
        $id_reserva = $datos['id_reserva'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_confirmar_reserva(?,?,?);";
        $query = $this->db->query($sql, array($id_reserva,$user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_datos_confirmaciion_reserva_email($datos) {
        $id_reserva = $datos['id_reserva'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_select_datos_reserva_confirmada_email(?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_rechazar_reserva($datos) {
        $id_reserva = $datos['id_reserva'];
        $motivo_rechazo = $datos['motivo_rechazo'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_rechazar_reserva(?,?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $motivo_rechazo, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_datos_rechazo_email($datos) {
        $id_reserva = $datos['id_reserva'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_select_datos_reserva_rechazada_email(?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_rechazar_bloque_reserva($datos) {
        $id_reserva = $datos['id_reserva'];
        $id_detalle_sala = $datos['id_detalle_sala'];
        $motivo_rechazo_bloque = $datos['motivo_rechazo_bloque'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
        $sql = "CALL sp_rechazar_bloque_reserva(?,?,?,?,?);";
        $query = $this->db->query($sql, array($id_reserva, $id_detalle_sala, $motivo_rechazo_bloque, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    

    

    
}
