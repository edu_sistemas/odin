<?php

/**
 * Modulo_Model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-21-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-21-11 [Marcelo Romero <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Modulo_Model
 */
class Modulo_model extends MY_Model {

    function set_arr_agregar_modulo($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Añade un nuevo  perfil
          Fecha Actualiza: 2016-21-11
         */

        $nombre = $data['nombre_m'];
        $titulo = $data['titulo_m'];
        $descripcion = $data['descripcion_m'];
        $icon = $data['icon_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_modulo_insert(?,?,?,?,?,?)";

        $query = $this->db->query($sql,array($nombre,$titulo,$descripcion,$icon,$user_id,$user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_modulo($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los módulos registrados en la BD
          Fecha Actualiza: 2016-11-22
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_modulo_select(?,?)";
        $query = $this->db->query($sql,array($user_id,$user_perfil));


        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_modulo_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera el módulo seleccionado según el id
          Fecha Actualiza: 2016-11-22
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_modulo_select_by_id(?,?,?);";

        $query = $this->db->query($sql,array($id,$user_id,$user_perfil));

        $result = $query->result_array();


        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_modulo($datos) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Modificar un módulo
          Fecha Actualiza: 2016-11-22
         */
        $id = $datos['id_m'];
        $nombre = $datos['nombre_m'];
        $titulo = $datos['titulo_m'];
        $descripcion = $datos['descripcion_m'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];
		$icono = $datos['icono_m'];



        $sql = "CALL sp_modulo_update(?,?,?,?,?,?,?);";

        $query = $this->db->query($sql,array($id,$nombre,$titulo,$descripcion,$user_id,$user_perfil,$icono));

        $result = $query->result_array();
    $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_modulo_estado($datos) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Modificar un módulo
          Fecha Actualiza: 2016-11-22
         */
        $id = $datos['id_m'];
        $estado = $datos['estado_m'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];

        $sql = "CALL sp_modulo_estado_change(?,?,?,?);";

        $query = $this->db->query($sql,array($id,$estado,$user_id,$user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_modulo_menu_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los estados que puede tener el menu
          Fecha Actualiza: 2016-11-24
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_modulo_item_select_cbx(?,?)";
        $query = $this->db->query($sql,array($user_id,$user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
