<?php

/**
 * Categoria_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Categoria_Model
 */
class Categoria_model extends MY_Model {

    function get_arr_listar_categoria_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_categoria_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // function get_arr_listar_categoria($data) {
    //     /*
    //       Ultimo usuario  modifica: David De Filippi
    //       Descripcion : Lista los categoria para data table
    //       Fecha Actualiza: 2016-10-27
    //      */
    //     $user_id = $data['user_id'];
    //     $user_perfil = $data['user_perfil'];
    //     $sql = " CALL sp_categoria_select_cbx('" . $user_id . "','" . $user_perfil . "')";
    //     $query = $this->db->query($sql);
    //     $row_count = $query->num_rows();
    //     $result = false;
    //     if ($row_count > 0) {
    //         $result = $query->result_array();
    //     }
    //     return $result;
    // }

    function get_arr_select_categoria($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Selecciona el categoria de la lista
          Fecha Actualiza: 2016-11-29
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_categoria_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_categoria($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Inserta un nuevo categoria
          Fecha Actualiza: 2016-11-29
         */

        $nombre_categoria = $data['nombre_categoria'];
        $descripcion_breve_categoria = $data['descripcion_breve_categoria'];
        $descripcion_categoria = $data['descripcion_categoria'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_categoria_insert(?,?,?,?,?);";

        $query = $this->db->query($sql, array($nombre_categoria, $descripcion_breve_categoria, $descripcion_categoria, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_update_categoria($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Actualiza un categoria existente
          Fecha Actualiza: 2016-11-29
         */

        $id_categoria = $data['id_categoria'];
        $nombre_categoria = $data['nombre_categoria'];
        $descripcion_breve_categoria = $data['descripcion_breve_categoria'];
        $descripcion_categoria = $data['descripcion_categoria'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_categoria_update(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_categoria, $nombre_categoria, $descripcion_breve_categoria, $descripcion_categoria, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_categoria($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_categoria_select(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }

    function get_arr_categoria_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_categoria = $data['id_categoria'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_categoria_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id_categoria, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_categoria_change_status($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia el estado de los usuarios
          Fecha Actualiza: 2016-11-25
         */
        $id_categoria = $data['id_categoria'];
        $estado = $data['estado'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_categoria_change_status(?,?,?,?)";

        $query = $this->db->query($sql, array($id_categoria, $estado, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
