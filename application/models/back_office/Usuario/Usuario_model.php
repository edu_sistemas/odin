<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-03-21  [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Usuario_model
 */
class Usuario_model extends MY_Model {

    function get_arr_usuario_by_ejecutivo_comercial($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Selecciona todos los usauruios tipo ejecutivo comercial
          Fecha Actualiza: 2017-01-12
         * 
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_usuario_select_tipo_eje_comercial_cbx(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
      function get_arr_ejecutivo_comercial($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        

        $sql = "CALL sp_ejecutivo_comercial_select_cbx(?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        
        $result = $query->result_array();
        
        $query->next_result();
        
        $query->free_result();
        
        return $result;
       

    }
    

    function get_arr_usuario_by_ejecutivo_comercial_user($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Selecciona todos los usauruios tipo ejecutivo comercial
          Fecha Actualiza: 2017-01-12
         * 
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_usuario_select_tipo_eje_comercial_cbx(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_usuario_muro_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-11-08
         */
        $id = $data['id_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_usuario_muro_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_usuario_skills($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-11-08
         */
        $id_usuario = $data['id_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_usuario_select_skills_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id_usuario, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_skill_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-11-08
         */
        $id_skill = $data['id_skill'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_usuario_get_skill_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id_skill, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function update_usuario_skill($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-11-08
         */
        $id_skill = $data['id_skill'];
        $id_nivel_conocimiento = $data['id_nivel_conocimiento'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_relator_update_skill(?,?,?,?);";

        $query = $this->db->query($sql, array($id_skill, $id_nivel_conocimiento, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function insert_usuario_skill($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-11-08
         */
        $id_conocimiento = $data['id_conocimiento'];
        $id_nivel_conocimiento = $data['id_nivel_conocimiento'];
        $id_usuario = $data['id_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_usuario_insert_skill(?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_conocimiento, $id_nivel_conocimiento, $id_usuario, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_conocimientos_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_conocimientos_select_cbx(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_departamento_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_departamento_select_cbx(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_nivel_conocimientos_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_nivel_conocimientos_select_cbx(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function delete_usuario_skill($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-11-08
         */
        $id_skill = $data['id_skill'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_relator_delete_skill(?,?,?);";

        $query = $this->db->query($sql, array($id_skill, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_imagenes_usr($data) {
        /*
          Llama las imagenes del usuario
          Descripcion : Lista las imagenes
          Fecha Actualiza: 2016-11-25
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_usuario_muro_select_images (?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_imagenes_usr($data) {
        /*
          Llama las imagenes del usuario
          Descripcion : Lista las imagenes
          Fecha Actualiza: 2016-11-25
         */
        //$fk_user_id = $data['fk_id_usuario'];
        $id_img = $data['id_img'];
        $imagen = $data['imagen'];
        $seleccionada = $data['seleccionada'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = " CALL sp_usuario_muro_insert_update_images  (?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_img, $imagen, $seleccionada, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_cambiar_password($data) {
        /*
          Cambia el password del usuario
          Descripcion : Lista las imagenes
          Fecha Actualiza: 2016-11-29
         */
        $pass_actual = $data['pass_actual'];
        $pass_nueva = $data['pass_nueva'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = " CALL sp_usuario_muro_update_password(?,?,?,?);";

        $query = $this->db->query($sql, array($pass_actual, $pass_nueva, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_usuario($data) {
        /*
          lista los usuarios
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_usuario_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_usuario_by_id($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-11-08
         */

        $id = $data['id_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_usuario_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_usuario($data) {

        $usuario = $data['usuario'];
        $correo = $data['correo'];
        $anexo_edutecno = $data['anexo_edutecno'];
        $nombre_usuario = $data['nombre_usuario'];
        $apellidos_usuario = $data['apellidos_usuario'];
        $rut_usuario = $data['rut_usuario'];
        $dv_usuario = $data['dv_usuario'];
        $perfil = $data['perfil'];
       // $departamento = $data['departamento'];
        $imagen = $data['imagen'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        //$sql = " CALL sp_usuario_insert(?,?,?,?,?,?,?,?,?,?,?,?);"; comentado para sacar el departamento
        $sql = " CALL sp_usuario_insert(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($usuario, $correo, $anexo_edutecno, $nombre_usuario, $apellidos_usuario, $rut_usuario, $dv_usuario, $perfil, $imagen, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_usuario($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id = $data['id'];
        $usuario = $data['usuario'];
        $correo = $data['correo'];
        $anexo_edutecno = $data['anexo_edutecno'];
        $nombre_usuario = $data['nombre_usuario'];
        $apellidos_usuario = $data['apellidos_usuario'];
        $rut_usuario = $data['rut_usuario'];
        $dv_usuario = $data['dv_usuario'];
        $perfil = $data['perfil'];
        //$departamento = $data['departamento'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_usuario_update(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($id, $usuario, $correo, $anexo_edutecno, $nombre_usuario, $apellidos_usuario, $rut_usuario, $dv_usuario, $perfil, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_usuario_reset_pass($data) {
        /*
          Ultimo usuario modifica: Jessica Roa
          Descripcion : Reestablece la contraseña del usuario en la BD
          Fecha Actualiza: 2018-03-21
         */

        $id = $data['id_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_usuario_by_id_reset_password(?,?,?);";

        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_usuario_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_usuario_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_usuario_change_status($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia el estado de los usuarios
          Fecha Actualiza: 2016-11-25
         */
        $id_usuario = $data['id_usuario'];
        $estado = $data['estado'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_usuario_desactivar_activar(
            '" . $id_usuario . "',   
            '" . $estado . "', 
            '" . $user_id . "',
            '" . $user_perfil .
                "')";

        $query = $this->db->query($sql);

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_usuario_comision_cbx($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_usuario_comision_select_cbx(?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
