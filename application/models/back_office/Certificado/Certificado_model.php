<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 * Fecha creacion:  2017-01-19 [Felipe Bulboa] <fbulboa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class Certificado_model extends MY_Model {

    function getFichas($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $num_ficha = $data['num_ficha'];
        $empresa = $data['empresa'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_informe_select(?,?,?,?);";

        $query = $this->db->query($sql, array($num_ficha, $empresa, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_tipo_entrega_cbx($data) { /*
      lista los usuarios
      Descripcion : Lista la modalidad del curso
      Fecha Actualiza: 2016-12-09
     */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_select_tipo_entrega_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_oc_by_id_ficha($data) { /*
      lista los usuarios
      Descripcion : Lista la modalidad del curso
      Fecha Actualiza: 2017-12-04
     */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_ficha = $data['id_ficha'];

        $sql = " CALL sp_certificado_oc_by_id_ficha_select(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha,$user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_tipo_entrega($data) { /*
      lista los usuarios
      Descripcion : Lista la modalidad del curso
      Fecha Actualiza: 2016-12-09
     */
        $id_ficha = $data['id_ficha'];
        $id_tipo = $data['id_tipo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_tipo_entrega_update_ficha(?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $id_tipo, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getPromedios($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ficha = $data['id_ficha'];
        $orden = $data['id_orden'];
        $sql = "CALL sp_nota_promedio(?,?,?,?)";
        $query = $this->db->query($sql, array($ficha,$orden, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        $this->db->last_query();
        return $result;
    }

    function getPromedioAlumno($data) {
        $ficha = $data['id_ficha'];
        $orden = $data['id_orden'];
        $id_detalle_alumno = $data['id_detalle_alumno'];
        // var_dump($data);
        // die();
        $sql = "CALL sp_nota_promedio_alumno(?,?,?)";
        $query = $this->db->query($sql, array($ficha,$orden, $id_detalle_alumno));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        $this->db->last_query();
        return $result;
    }

    function get_datos_diplomas($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $alumno = $data['id_detalle_alumno'];
        $participacion = $data['participacion'];


        $sql = "CALL sp_certificado_datos(?,?,?,?)";
        $query = $this->db->query($sql, array($alumno, $participacion, $user_id, $user_perfil));

        $result = $query->result_array();
        // $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function setDiplomas($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ficha = $data['id_ficha'];

        $sql = "CALL sp_ficha_set_diploma(?,?,?)";
        $query = $this->db->query($sql, array($ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
