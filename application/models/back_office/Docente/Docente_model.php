<?php

/**
 * Docente_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  0000-00-00 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-14 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *  Docente_model
 */
class Docente_model extends MY_Model {

    function get_arr_listar_docente($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */
        $conocimiento_filtro_cbx = $data['conocimiento_filtro_cbx'];
        $nivel_filtro_cbx = $data['nivel_filtro_cbx'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_docente_select(?,?,?,?)";
        $query = $this->db->query($sql, array($conocimiento_filtro_cbx, $nivel_filtro_cbx, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_docente_by_id($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-11-08
         */

        $id = $data['id_docente'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_docente_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_docente($data) {

        $usuario = $data['usuario'];
        $correo = $data['correo'];
        $nombre_docente = $data['nombre_docente'];
        $apellidos_docente = $data['apellidos_docente'];
        $rut_docente = $data['rut_docente'];
        $dv_docente = $data['dv_docente'];
        $direccion = $data['direccion'];

        $imagen = $data['imagen'];
        $telefono = $data['telefono'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_docente_insert(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $usuario,
            $correo,
            $telefono,
            $nombre_docente,
            $apellidos_docente,
            $rut_docente,
            $dv_docente,
            $imagen,
            $direccion,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_docente($data) {
        /*
          Ultimo docente modifica: Marcelo Romero
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id = $data['id'];
        $usuario = $data['usuario'];
        $correo = $data['correo'];
        $nombre_docente = $data['nombre_docente'];
        $apellidos_docente = $data['apellidos_docente'];
        $rut_docente = $data['rut_docente'];
        $dv_docente = $data['dv_docente'];
        $telefono = $data['telefono'];
        $direccion = $data['direccion'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_docente_update(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($id, $usuario, $correo, $telefono, $nombre_docente, $apellidos_docente, $rut_docente, $dv_docente, $direccion, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_docente_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_docente_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_docente_change_status($data) {
        /*
          Ultimo docente  modifica: Luis Jarpa
          Descripcion : Actualzia el estado de los docentes
          Fecha Actualiza: 2016-11-25
         */
        $id_docente = $data['id_docente'];
        $estado = $data['estado'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_docente_desactivar_activar(
            '" . $id_docente . "',   
            '" . $estado . "', 
            '" . $user_id . "',
            '" . $user_perfil .
                "')";

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
