<?php

/**
 * Edutecno_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-06-12 [David De Filippi] <mromero@edutecno.com>
 * Fecha creacion:  2016-06-27 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Ficha_Control_model
 */
class Ficha_Control_Learn_model extends MY_Model {

    function get_correo_descarte($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_send_email_ce(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_correo__sin_descarte($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_send_email_ce_sin_descartados(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data['start'];
        $end = $data['end'];
        $num_ficha = $data['ficha'];
        $empresa = $data['empresa'];
        $holding = $data['holding'];
        $ejecutivo = $data['ejecutivo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_select_consulta_elearning(?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array($start, $end, $empresa, $holding, $num_ficha, $ejecutivo, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_descartar_alm($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Cambia el estado de los alumnos
          Fecha Actualiza: 2017-06-21
         */


        $fichaff = $data['ftc'];
        $total = $data['cantidad'];
        $check = array();
        $id_alm = array();
        $motivo = array();

        $arr_data = array(
            'estado' => $check,
            'id_alumno' => $id_alm,
            'motivo' => $motivo,
            $total => $this->input->post('cantidad')
        );

        for ($x = 0; $x < $total; $x++) {

            $j = $data['estado'][$x];
            $k = $data['id_alumno'][$x];
            $l = $data['motivo'][$x];


            $total = $data['cantidad'];
            $fichaff = $data['ftc'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

            $sql = "CALL sp_descarte_insert_ce(?,?,?,?,?,?);";

            $query = $this->db->query($sql, array($j, $k, $l, $fichaff, $user_id, $user_perfil));


            $result = $query->result_array();
            $query->next_result();
            $query->free_result();
        }
        return $result;
    }

    function get_arr_listar_datos_orden_compraFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_alumno_c_e(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_excel_generate($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de las ordenes de compra
          Fecha Actualiza: 2017-01-16
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_doc_excel_generacion_ce_elearning(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_excel_generate_alumnos($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera datos de la ficha para carga un excel
          Fecha Actualiza: 2017-06-14
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_excel_generacion_almce_elearning(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_orden_compra_by_ficha_idFC($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de las ordenes de compra
          Fecha Actualiza: 2017-01-16
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_ficha_id(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id_resumenFC($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de la ficha
          Fecha Actualiza: 2017-01-16
         */
        $id_edutecno = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id_resumen(?,?,?)";
        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_by_id_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_alumno_select_by_id_ficha_ficha_control(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_doc_by_id_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_document_alumno_select_by_id_ficha_fc(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_extension_by_id_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_extensiones_select_by_id_ficha_ficha_control(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_extension_doc_by_id_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_document_extens_select_by_id_ficha_fc(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function insert_log_ficha_c_e($data) {
        /*
          Ultimo usuario  modifica:  Marcelo Romero
          Descripcion : Registra en el log
          Fecha Actualiza: 2016-27-07
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $p_hito = $data['hito'];
        $p_id_estado_edutecno = $data['id_estado_edutecno'];
        $p_observacion = $data['observacion'];
        $p_responsable = $data['responsable'];

        $sql = "CALL sp_g_registra_log_ficha(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil, $p_hito, $p_id_estado_edutecno, $p_observacion, $p_responsable));

//	    $result = $query->result_array();
//		$query->next_result();
//		$query->free_result();

        return $query;
    }

    function insert_log_ficha_alum_c_e($data) {
        /*
          Ultimo usuario  modifica:  Marcelo Romero
          Descripcion : registra en el log
          Fecha Actualiza: 2016-27-07
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $p_hito = $data['hito'];
        $p_id_estado_edutecno = $data['id_estado_edutecno'];
        $p_observacion = $data['observacion'];
        $p_responsable = $data['responsable'];

        $sql = "CALL sp_g_registra_log_ficha(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil, $p_hito, $p_id_estado_edutecno, $p_observacion, $p_responsable));

//	    $result = $query->result_array();
//		$query->next_result();
//		$query->free_result();

        return $query;
    }

}
