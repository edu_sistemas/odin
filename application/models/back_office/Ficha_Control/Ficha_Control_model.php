<?php

/**
 * Edutecno_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2018-04-18 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Ficha_Control_model
 */
class Ficha_Control_model extends MY_Model {

    function get_arr_listar_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_select(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data['start'];
        $end = $data['end'];
        $estado = $data['estado'];
        $empresa = $data['empresa'];
        $holding = $data['holding'];

        $ejecutivo = $data['ejecutivo'];
        $periodo = $data['periodo'];
        $num_ficha = $data['ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_select_consulta(?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $holding,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
                )
        );




        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

function get_arr_consultar_fichaFC_objetados($data) {
        /*
          Ultimo usuario  modifica:  Jessica Roa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2018-04-18
         */

        $start = $data['start'];
        $end = $data['end'];
        $estado = $data['estado'];
        $empresa = $data['empresa'];
        $holding = $data['holding'];

        $ejecutivo = $data['ejecutivo'];
        $periodo = $data['periodo'];
        $num_ficha = $data['ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_select_consulta_objetados(?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $holding,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }


    function get_arr_ficha_by_idFC($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_edutecno = $data['id_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_ficha_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_oc($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_oc(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_editar_ficha($data) {
        /*
          Ultimo usuario modifica: David De Filippi
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-12-02
         */


        $id_ficha = $data['id_ficha'];
        $id_curso = $data['id_curso'];
        $id_empresa = $data['id_empresa'];
        $id_otic = $data['id_otic'];
        $comentario = $data['comentario'];
        $num_orden_compra = $data['num_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert(?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array($id_ficha, $id_curso, $id_empresa, $id_otic, $num_orden_compra, $comentario, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_datos_orden_compraFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_alumno(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_rechazar_fichaFC($data) {

        $id_ficha = $data['id_ficha_rechazo'];
        $motivo_rechazo = $data['motivo_rechazo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_rechazar(?,?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $motivo_rechazo, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_anular_fichaFC($data) {

        $id_ficha = $data['id_ficha_anulacion'];
        $motivo_anulacion = $data['motivo_anulacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_anular(?,?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $motivo_anulacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_aprobar_fichaFC($data) {


        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_aprobar(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_nueva_orden_compra($data) {

        $id_ficha = $data['id_ficha'];
        $id_curso = $data['id_curso'];
        $id_empresa = $data['id_empresa'];
        $id_otic = $data['id_otic'];
        $comentario = $data['comentario'];
        $num_orden_compra = $data['num_orden_compra'];
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];
        $fecha_cierre = $data['fecha_cierre'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_orden_compra(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $id_curso, $id_empresa, $id_otic, $num_orden_compra, $comentario, $fecha_inicio, $fecha_termino, $fecha_cierre, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id_resumenFC($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de la ficha
          Fecha Actualiza: 2017-01-16
         */
        $id_edutecno = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_select_by_id_resumen(?,?,?)";
        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_orden_compra_by_ficha_idFC($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de las ordenes de compra
          Fecha Actualiza: 2017-01-16
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_ficha_id(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* Rectificaciones */

    function get_arr_listar_rectificacionesFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_select_ficha_control(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_historico_rectificacionesFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_historico_rectificacion_select_ficha_control(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_estado_resctificacionesFC($data) {
        /*
         * Ultimo usuario modifica: David De Filippi , Luis Jarpa
         * Descripcion : Lista las edutecno
         * Fecha Actualiza: 2016-12-26
         */
        $id_rectificacion = $data ['id_rectificacion'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_get_estado_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array(
            $id_rectificacion,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_aprobar_resctificacionesFC($data) {

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_aprobarFC(?,?,?)";

        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_rechazar_resctificacionesFC($data) {


        $id_rectificacion = $data ['id_rectificacion'];
        $motivo_rechazo = $data ['motivo_rechazo'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_rectificacion_rechazar_FC(?,?,?,?)";
        $query = $this->db->query($sql, array(
            $id_rectificacion,
            $motivo_rechazo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* Datos Actuales */

    function get_arr_listar_datos_documentos_resumen_orden_compraFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_orden_compra_select_by_id_documentos(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_documentos_actualesFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $p_id_ficha = $data['id_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_document_actual_FC(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_last_documentos_actualesFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $p_id_ficha = $data['id_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_last_document_ficha_FC(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* END Datos Actuales */

    /* Rectificaciones */

    function get_arr_listar_rectificaciones_by_id_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_alumno_select_by_id_ficha_ficha_control(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_doc_by_id_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_document_alumno_select_by_id_ficha_fc(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /*  end Rectificaciones */

    /* Extensiones */

    function get_arr_listar_rectificaciones_extension_by_id_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_extensiones_select_by_id_ficha_ficha_control(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_extension_doc_by_id_fichaFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_document_extens_select_by_id_ficha_fc(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* END Extensiones */
    /* Fin rectificaciones */

    function set_arr_edutecno_ficha($data) {

        $id_ficha = $data['id_ficha'];
        $edutecno_cbx = $data['edutecno_cbx'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_control_insert_edutecno(?,?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $edutecno_cbx, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_adicionales_ficha_by_id($data){
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_select_items_ficha(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_facturas($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $empresa = $data['empresa'];
        $holding = $data['holding'];

        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_factura_select_completo(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $this->db->last_query();
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function get_arr_listar_nc($data) {
        /*
         * lista las notas de credito
         * Descripcion : Saludos
         * Fecha Actualiza: 2017-11-09
         */
        
        $num_ficha = $data['num_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_get_nc_byficha(?,?,?)";
        $query = $this->db->query($sql, array(            
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $this->db->last_query();
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_nd($data) {
        /*
         * lista las notas de credito
         * Descripcion : Saludos
         * Fecha Actualiza: 2017-11-09
         */
        
        $num_ficha = $data['num_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_get_nd_byficha(?,?,?)";
        $query = $this->db->query($sql, array(            
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $this->db->last_query();
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

   

    


}
