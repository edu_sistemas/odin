<?php

/**
 * Edutecno_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Ficha_JC_model
 */
class Ficha_JC_model extends MY_Model {

    function get_arr_listar_fichaJC($data) {


        /*
         * Ultimo usuario modifica: David De Filippi , Luis Jarpa
         * Descripcion : Lista las edutecno
         * Fecha Actualiza: 2017-02-09
         */


        $empresa = $data['empresa'];
        $holding = $data['holding'];

        $ejecutivo = $data['ejecutivo'];
        $periodo = $data['periodo'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_jc_select(?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_fichaJC($data) {
        /*
         * Ultimo usuario modifica: David De Filippi , Luis Jarpa
         * Descripcion : Lista las edutecno
         * Fecha Actualiza: 2016-12-26
         */
        $start = $data ['start'];
        $end = $data ['end'];
        $estado = $data ['estado'];
        $empresa = $data ['empresa'];
        $holding = $data ['holding'];
        $ejecutivo = $data ['ejecutivo'];
        $periodo = $data ['periodo'];
        $num_ficha = $data ['ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_jc_select_consulta(?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $holding,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_adicionales_ficha_by_id($data){
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_select_items_ficha(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_idJC($data) {

        /*
         * Ultimo usuario modifica: Marcelo Romero
         * Descripcion : Recupeera perfil desde BD
         * Fecha Actualiza: 2016-10-14
         */
        $id_edutecno = $data ['id_ficha'];

        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = " CALL sp_ficha_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_edutecno,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_historico_rectificacionesFC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_historico_rectificacion_select_ficha_control(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_rechazar_fichaJC($data) {

        $id_ficha = $data ['id_ficha_rechazo'];
        $motivo_rechazo = $data ['motivo_rechazo'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_ficha_rechazar(?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $motivo_rechazo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_aprobar_fichaJC($data) {
        $id_ficha = $data ['id_ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_ficha_aprobar(?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id_resumenJC($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de la ficha
          Fecha Actualiza: 2017-01-16
         */
        $id_edutecno = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id_resumen(?,?,?)";
        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_orden_compra_by_ficha_idJC($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de las ordenes de compra
          Fecha Actualiza: 2017-01-16
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_ficha_id(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_orden_compraJC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_alumno(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_documentos_resumen_orden_compraJC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_orden_compra_select_by_id_documentos(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_resctificacionesJC($data) {
        /*
         * Ultimo usuario modifica: David De Filippi , Luis Jarpa
         * Descripcion : Lista las edutecno
         * Fecha Actualiza: 2016-12-26
         */
        $p_id_ficha = $data ['id_ficha'];
        $p_orden_compra = $data ['orden_compra'];

        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_rectificacion_select_jc(?,?,?,?)";
        $query = $this->db->query($sql, array(
            $p_id_ficha,
            $p_orden_compra,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    //  solo rectificacion 

    function get_estado_resctificacionesJC($data) {
        /*
         * Ultimo usuario modifica: David De Filippi , Luis Jarpa
         * Descripcion : Lista las edutecno
         * Fecha Actualiza: 2016-12-26
         */
        $id_rectificacion = $data ['id_rectificacion'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_get_estado_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array(
            $id_rectificacion,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_aprobar_resctificacionesJC($data) {
        /*
         * Ultimo usuario modifica: David De Filippi , Luis Jarpa
         * Descripcion : Lista las edutecno
         * Fecha Actualiza: 2016-12-26
         */
        $id_rectificacion = $data ['id_rectificacion'];

        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_rectificacion_aprobar(?,?,?)";
        $query = $this->db->query($sql, array(
            $id_rectificacion,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_rechazar_resctificacionesJC($data) {
        /*
         * Ultimo usuario modifica: David De Filippi , Luis Jarpa
         * Descripcion : Lista las edutecno
         * Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data ['id_rectificacion'];
        $motivo_rechazo = $data ['motivo_rechazo'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_rectificacion_rechazar(?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_rectificacion,
            $motivo_rechazo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id_rectificacionJC($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de la ficha mediante una rectificacion
          Fecha Actualiza: 2017-01-16
         */
        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_orden_compra_by_rectificacion_ficha_idJC($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de las ordenes de compra mediante una rectificacion
          Fecha Actualiza: 2017-01-16
         */

        $id_ficha = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_rectificacion_ficha_id(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_orden_compra_by_rectificacionJC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_alumno_rectificacion_jc(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_documentos_resumen_orden_compra_by_rectificacionJC($data) {
        /*
          Ultimo usuario  modifica:    Luis Jarpa
          Descripcion : Lista los documetnos de una ficha por id rectificacion
          Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_documentos_by_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_rectificacion_orden_compra_by_rectificacionJC($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_rectificacion_select_by_id_alumno_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_documentos_rectificacion_resumen_orden_compra_by_rectificacionJC($data) {
        /*
          Ultimo usuario  modifica:    Luis Jarpa
          Descripcion : Lista los documetnos de una ficha por id rectificacion
          Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_documentos_rect_by_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_solicitud_tablet_by_id_solicitud($data){
        $id_solicitud_tablet = $data['id_solicitud_tablet'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_tablet_solicitud_tablet_by_id(?,?,?)";

        $query = $this->db->query($sql, array(
            $id_solicitud_tablet,
            $user_id,
            $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function setCodigoTablet($data){
        $codigoFinal = $data['codigoFinal'];
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_codigo_tablet_insert(?,?,?,?)";

        $query = $this->db->query($sql, array(
            $codigoFinal,
            $id_ficha,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
