<?php

/**
 * Categoria_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Categoria_Model
 */
class Trivias_Edutecno_model extends MY_Model {

    function setUsuario($data) {

            $id_trivia = $data['id_trivia'];
            $tipo_usuario = $data['tipo_usuario'];
            $rut = $data['rut'];
            $dv = $data['dv'];
            $id_ficha = $data['id_ficha'];
            $id_dtl_alumno = $data['id_dtl_alumno'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_trivia_insert_usuario(?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_trivia,
            $tipo_usuario,
            $rut,
            $dv,
            $id_ficha,
            $id_dtl_alumno,
            $user_id, 
            $user_perfil
        ));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }

    function getAlumnosByFicha($data) {

            $id_ficha = $data['id_ficha'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumnos_select_by_ficha(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $user_id, 
            $user_perfil
        ));
        $row_count = $query->num_rows();

        $result = $query->result_array();

        return $result;
    }
}
