<?php

/**
 * Descuento_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2018-01-03 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2018-01-03 [Jessica Roa] <jroa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *  Descuento_model
 */
class Descuento_model extends MY_Model {

    function get_arr_listar_rectificaciones($data) {

        $fecha_ant = $data['fecha_ant'];
        $fecha_m = $data['fecha_m'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_filtro_rectificaciones_select(?,?,?,?)";
        $query = $this->db->query($sql, array($fecha_ant, $fecha_m, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
}
