<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class moodle_model extends MY_Model {

//WS MOODLE
    private $token = '53ee9021dd39ee9d3d220c382fdb4bcf';
    private $domainname = 'http://192.168.15.19';

    function add_error($data) {
        /*
          lista los docentes
          Descripcion :
          Fecha Actualiza:
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_cliente = $data['id_cliente'];
        $nombre_metodo = $data['nombre_metodo'];
        $msg_excepcion = $data['msg_excepcion'];
        $id_ficha = $data['id_ficha'];
        $id_curso = $data['id_curso'];
        $estado = $data['estado'];

        $sql = " CALL sp_moodle_registrar_error(?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil, $id_cliente, $nombre_metodo, $msg_excepcion, $id_ficha, $id_curso, $estado));

        //$result = $query->result_array();
        //$query->next_result();
        //$query->free_result();
        return true;
    }

    function get_curso_evaluacion_pendientes() {
        /*
          lista los docentes
          Descripcion :
          Fecha Actualiza:
         */

        $sql = " CALL sp_moodle_cursos_evaluacion_pendientes_select()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_alumnos_envio_carta() {
        /*
          lista los docentes
          Descripcion :
          Fecha Actualiza:
         */


        $sql = " CALL sp_moodle_get_alumnos_envio_carta()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_alumno_envio_carta($data) {
        /*
          lista los docentes
          Descripcion :
          Fecha Actualiza:
         */

        $id_ficha = $data['id_ficha'];
        $id_alumno = $data['id_alumno'];

        $sql = " CALL sp_moodle_ficha_alumno_insert(?,?)";
        $query = $this->db->query($sql, array($id_ficha, $id_alumno));
        return true;
    }

    function get_alumnos_by_id_ficha($data) {
        /*
          lista los docentes
          Descripcion :
          Fecha Actualiza:
         */

        $id_ficha = $data['id_ficha'];

        $sql = " CALL sp_moodle_alumnos_ficha_select_by_id(?)";
        $query = $this->db->query($sql, array($id_ficha));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_errores_by_id_ficha($data) {
        /*
          lista los docentes
          Descripcion :
          Fecha Actualiza:
         */

        $id_ficha = $data['id_ficha'];
        $estado = $data['estado'];

        $sql = " CALL sp_moodle_errores_by_id_ficha(?,?)";
        $query = $this->db->query($sql, array($id_ficha, $estado));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_fichas_aprobadas($ano) {
        /*
          lista los docentes
          Descripcion :
          Fecha Actualiza:
         */

        $sql = " CALL sp_moodle_fichas_aprobadas(?)";
        $query = $this->db->query($sql,array($ano));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_curso_by_id_ficha($data) {
        /*
          lista los docentes
          Descripcion :
          Fecha Actualiza:
         */
        $id_ficha = $data['id_ficha'];
        $sql = " CALL sp_moodle_curso_by_id_ficha(?)";
        $query = $this->db->query($sql, array($id_ficha));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    /**
     * 
     * @param type $id_ficha
     * @param type $id_curso
     * @param type $shortName
     * @return int id del curso en caso de existencia
     */
    public function existeCursoMoodle($id_ficha, $id_curso, $id_curso_moodle) {
        $functionname = 'core_course_get_courses_by_field';
        $restformat = 'json';

        $params = array('field' => 'id', 'value' => $id_curso_moodle);

        $serverurl = $this->domainname . '/webservice/rest/server.php' . '?wstoken=' . $this->token . '&wsfunction=' . $functionname;
        require_once dirname(__FILE__) . '/curl.php';
        $curl = new curl;
        $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
        $resp = $curl->get($serverurl . $restformat, $params);
        $json = json_encode($resp);

        $obj = json_decode($resp);

        if (strpos($resp, 'exception') !== false) {
            $data = array(
                'user_id' => 1,
                'user_perfil' => 1,
                'id_cliente' => 1,
                'nombre_metodo' => $functionname,
                'msg_excepcion' => $obj->exception,
                'id_ficha' => $id_ficha,
                'id_curso' => $id_curso,
                'estado' => 0
            );
            $this->modelo->add_error($data);
            return 0;
        }

        if (empty($obj->courses) || is_null($obj->courses)) {
            return 0;
        }
        return $obj->courses[0]->id;
    }

    public function getCursoMoodleByShortname($shortName) {
        $functionname = 'core_course_get_courses_by_field';
        $restformat = 'json';

        $params = array('field' => 'shortname', 'value' => $shortName);

        $serverurl = $this->domainname . '/webservice/rest/server.php' . '?wstoken=' . $this->token . '&wsfunction=' . $functionname;
        require_once dirname(__FILE__) . '/curl.php';
        $curl = new curl;
        $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
        $resp = $curl->get($serverurl . $restformat, $params);
        $json = json_encode($resp);

        $obj = json_decode($resp);

        if (strpos($resp, 'exception') !== false) {
            return 0;
        }

        if (empty($obj->courses) || is_null($obj->courses)) {
            return 0;
        }
        return $obj->courses[0]->id;
    }

    /**
     * 
     * @param type $usuario shortname del usuario a consultar
     * @return type int id del usuario
     */
    public function existeUsuarioMoodle($id_ficha, $id_curso, $usuario) {
        $functionname = 'core_user_get_users_by_field';
        $restformat = 'json';

        $params = array('field' => 'username', 'values' => array($usuario));

        $serverurl = $this->domainname . '/webservice/rest/server.php' . '?wstoken=' . $this->token . '&wsfunction=' . $functionname;
        require_once dirname(__FILE__) . '/curl.php';
        $curl = new curl;
        $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
        $resp = $curl->get($serverurl . $restformat, $params);
        $json = json_encode($resp);

        $obj = json_decode($resp);
        if (strpos($resp, 'exception') !== false) {
            $data = array(
                'user_id' => 1,
                'user_perfil' => 1,
                'id_cliente' => 1,
                'nombre_metodo' => $functionname,
                'msg_excepcion' => $obj->exception,
                'id_ficha' => $id_ficha,
                'id_curso' => $id_curso,
                'estado' => 0
            );
            $this->add_error($data);
            return 0;
        }


        if (empty($obj) || is_null($obj)) {
            return 0;
        }
        return $obj[0]->id;
    }

    public function crearCurso($id_ficha, $id_curso, $fullname, $shortname, $categoryid) {

        $functionname = 'core_course_create_courses';


        $restformat = 'json';

        $curso = new stdClass();
        $curso->fullname = $fullname;
        $curso->shortname = $shortname;
        $curso->categoryid = $categoryid;
        $cursos = array($curso);
        $params = array('courses' => $cursos);

        $serverurl = $this->domainname . '/webservice/rest/server.php' . '?wstoken=' . $this->token . '&wsfunction=' . $functionname;
        require_once dirname(__FILE__) . '/curl.php';
        $curl = new curl;
        $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
        $resp = $curl->post($serverurl . $restformat, $params);
        $json = json_encode($resp);

        $obj = json_decode($resp);
        if (strpos($resp, 'exception') !== false) {
            $data = array(
                'user_id' => 1,
                'user_perfil' => 1,
                'id_cliente' => 1,
                'nombre_metodo' => $functionname,
                'msg_excepcion' => $obj->exception . ' | ' . $obj->message,
                'id_ficha' => $id_ficha,
                'id_curso' => $id_curso,
                'estado' => 0
            );
            $this->add_error($data);
            return 0;
        }

        return $obj[0]->id;
    }

    public function crearAlumno($id_ficha, $id_curso, $username, $password, $firstname, $lastname, $email) {

        $functionname = 'core_user_create_users';

        $restformat = 'json';

        $user1 = new stdClass();
        $user1->username = $username;
        $user1->password = $password;
        $user1->firstname = $firstname;
        $user1->lastname = $lastname;
        $user1->email = $email;

        $users = array($user1);
        $params = array('users' => $users);


        $serverurl = $this->domainname . '/webservice/rest/server.php' . '?wstoken=' . $this->token . '&wsfunction=' . $functionname;
        require_once dirname(__FILE__) . '/curl.php';
        $curl = new curl;
        $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
        $resp = $curl->post($serverurl . $restformat, $params);
        $json = json_encode($resp);

        $obj = json_decode($resp);
        if (strpos($resp, 'exception') !== false) {
            $data = array(
                'user_id' => 1,
                'user_perfil' => 1,
                'id_cliente' => 1,
                'nombre_metodo' => $functionname,
                'msg_excepcion' => 'Error en el usuario ' . $firstname . ' ' . $lastname . '<br>Tipo: ' . $obj->exception . '<br>Mensaje: ' . $obj->message . '<br>DebugInfo: ' . $obj->debuginfo,
                'id_ficha' => $id_ficha,
                'id_curso' => $id_curso,
                'estado' => 0
            );
            $this->add_error($data);
            return 0;
        }
        return $obj[0]->id;
    }
    
    

    public function enrolarAlumnos($id_ficha, $id_curso, $user_id, $course_id) {

        $role_id = 5;

        $functionname = 'enrol_manual_enrol_users';

        $restformat = 'json';
        $enrolment = array();
        $enrolments = array();
        $params = array();
        if (is_array($user_id)) {
            $alumnos = $this->getAlumnosCurso($course_id);
            for ($i = 0; $i < count($user_id); $i++) {
                if (!$this->existeAlumnoEnCurso($alumnos, $user_id[$i])) {
                    $enrolment[] = array('roleid' => $role_id, 'userid' => $user_id[$i], 'courseid' => $course_id);
                }
            }
            $enrolments = $enrolment;
        } else {
            $alumnos = $this->getAlumnosCurso($course_id);
            if (!$this->existeAlumnoEnCurso($alumnos, $user_id)) {
                $enrolment = array('roleid' => $role_id, 'userid' => $user_id, 'courseid' => $course_id);
            }
            $enrolments = array($enrolment);
        }
        if (count($enrolments) > 0) {
            $params = array('enrolments' => $enrolments);


            $serverurl = $this->domainname . '/webservice/rest/server.php' . '?wstoken=' . $this->token . '&wsfunction=' . $functionname;
            require_once dirname(__FILE__) . '/curl.php';
            $curl = new curl;
            $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
            $resp = $curl->post($serverurl . $restformat, $params);
            $json = json_encode($resp);

            $obj = json_decode($resp);

            if (strpos($resp, 'exception') !== false) {
                $data = array(
                    'user_id' => 1,
                    'user_perfil' => 1,
                    'id_cliente' => 1,
                    'nombre_metodo' => $functionname,
                    'msg_excepcion' => 'Tipo: ' . $obj->exception . '<br>Mensaje: ' . $obj->message . '<br>DebugInfo: ',
                    'id_ficha' => $id_ficha,
                    'id_curso' => $id_curso,
                    'estado' => 0
                );
                $this->modelo->add_error($data);
                return false;
            }
            return true;
        }
        return false;
    }

    public function getAlumnosCurso($idCurso) {

        $functionname = 'core_enrol_get_enrolled_users';
        $restformat = 'json';

        $params = array('courseid' => $idCurso);

        $serverurl = $this->domainname . '/webservice/rest/server.php' . '?wstoken=' . $this->token . '&wsfunction=' . $functionname;
        require_once dirname(__FILE__) . '/curl.php';
        $curl = new curl;
        $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
        $resp = $curl->post($serverurl . $restformat, $params);
        return json_decode($resp);
    }

    public function existeAlumnoEnCurso($lista, $idAlumno) {
        $lista = json_decode(json_encode($lista, true));
        for ($i = 0; $i < count($lista); $i++) {
            if ($lista[$i]->id == $idAlumno)
                return true;
        }
        return false;
    }

    public function cursos_evaluacion($courseid, $if_ficha) {
        try {
            $functionname = 'gradereport_user_get_grade_items';
            $restformat = 'json';

            $params = array('courseid' => $courseid);

            $serverurl = $this->domainname . '/webservice/rest/server.php' . '?wstoken=' . $this->token . '&wsfunction=' . $functionname;
            require_once dirname(__FILE__) . '/curl.php';
            $curl = new curl;
            $restformat = ($restformat == 'json') ? '&moodlewsrestformat=' . $restformat : '';
            $resp = $curl->post($serverurl . $restformat, $params);

            $obj = json_decode($resp);
            for ($i = 0; $i < count($obj->usergrades); $i++) {
                for ($j = 0; $j < count($obj->usergrades[$i]->gradeitems); $j++) {
                    /* $sql = "INSERT INTO `tbl_moodle_curso_evaluacion`
                      (`curso`,`userid`,`evaluacion`,`itemname`,`itemtype`,`itemmodule`,
                      `iteminstance`,`itemnumber`,`categoryid`,`outcomeid`,`scaleid`,
                      `cmid`,`weightraw`,`weightformatted`,`graderaw`,`gradedatesubmitted`,
                      `gradedategraded`,`gradehiddenbydate`,`gradeneedsupdate`,
                      `gradeishidden`,`gradeformatted`,`grademin`,`grademax`,`rangeformatted`,
                      `percentageformatted`,`feedback`,`feedbackformat`, fecha_registro)
                      VALUES
                      (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"; */

                    $sql = "CALL sp_moodle_curso_evaluacion_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                    $this->db->query($sql, array($if_ficha,
                        $courseid, $obj->usergrades[$i]->userid, $obj->usergrades[$i]->gradeitems[$j]->id, $obj->usergrades[$i]->gradeitems[$j]->itemname, $obj->usergrades[$i]->gradeitems[$j]->itemtype, $obj->usergrades[$i]->gradeitems[$j]->itemmodule,
                        $obj->usergrades[$i]->gradeitems[$j]->iteminstance, $obj->usergrades[$i]->gradeitems[$j]->itemnumber, $obj->usergrades[$i]->gradeitems[$j]->categoryid, $obj->usergrades[$i]->gradeitems[$j]->outcomeid, $obj->usergrades[$i]->gradeitems[$j]->scaleid,
                        property_exists($obj->usergrades[$i]->gradeitems[$j], 'cmid') ? $obj->usergrades[$i]->gradeitems[$j]->cmid : null, property_exists($obj->usergrades[$i]->gradeitems[$j], 'weightraw') ? $obj->usergrades[$i]->gradeitems[$j]->weightraw : null, property_exists($obj->usergrades[$i]->gradeitems[$j], 'weightformatted') ? $obj->usergrades[$i]->gradeitems[$j]->weightformatted : null, $obj->usergrades[$i]->gradeitems[$j]->graderaw, $obj->usergrades[$i]->gradeitems[$j]->gradedatesubmitted,
                        $obj->usergrades[$i]->gradeitems[$j]->gradedategraded, $obj->usergrades[$i]->gradeitems[$j]->gradehiddenbydate, $obj->usergrades[$i]->gradeitems[$j]->gradeneedsupdate,
                        $obj->usergrades[$i]->gradeitems[$j]->gradeishidden, $obj->usergrades[$i]->gradeitems[$j]->gradeformatted, $obj->usergrades[$i]->gradeitems[$j]->grademin, $obj->usergrades[$i]->gradeitems[$j]->grademax, $obj->usergrades[$i]->gradeitems[$j]->rangeformatted,
                        $obj->usergrades[$i]->gradeitems[$j]->percentageformatted, $obj->usergrades[$i]->gradeitems[$j]->feedback, $obj->usergrades[$i]->gradeitems[$j]->feedbackformat));
                }
            }
        } catch (Exception $e) {
            echo 'Excepción capturada: '.  $e->getMessage(). "\n";
            return false;
        }
        return true;
    }

}
