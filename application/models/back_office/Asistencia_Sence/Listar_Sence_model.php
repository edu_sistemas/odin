<?php

/**
 * Asistencia_Sencen_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-01-11 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class Listar_Sence_model extends MY_Model {

    function get_arr_listar_ficha_reserva_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los numero deficha que están dentro de la tabla ficha_reserva
          Fecha Actualiza: 2017-01-12
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_reserva_select_cbx_senceAst(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listado_ficha_sence($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los numero deficha que están dentro de la tabla ficha_reserva
          Fecha Actualiza: 2017-01-12
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_sence_listado_by_ficha_id(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_alumnos($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumnos_sence_select_by_ficha_id(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_registrar_asistencia($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Registra la asistencia en la tabla asistencia, recorre ciclo for con los datos que lista el datatable
          Fecha Actualiza: 2017-01-12
         * 
         */
        $result = false;
        $numRows = $data['numRows'];
        $largo = $data['largo'];
        $arr_asistencia = array();
        $arr_id_asis = array();
        $arr_dtla = array();



        for ($inst = 0; $inst < $numRows; $inst++) {
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

            $a = $data['id_alm'][$inst];
            $asistencia = $data['asistencia'][$inst];
            if ($largo > 0) {
                $arr_id_asis = $data['id_asistencia'][$inst];
            } else {
                $arr_id_asis = '';
            }

            $sql = " CALL sp_asistencia_sence__insert(?,?,?,?,?)";
            $query = $this->db->query($sql, array($arr_id_asis, $a, $asistencia, $user_id, $user_perfil));

            $result = $query->result_array();

            $query->next_result();
            $query->free_result();
        }
        return $result;
    }

}
