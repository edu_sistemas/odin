<?php

/**
 * Docente_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  0000-00-00 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-14 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *  Docente_model
 */
class Contratos_model extends MY_Model {

    function get_arr_listar_contratos($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_contratos_docente(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_docentes($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ficha = $data['ficha'];
        $sql = " CALL sp_contratos_docente_lista(?,?,?)";
        $query = $this->db->query($sql, array($ficha, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_docentes($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ficha = $data['ficha'];
        $usuario = $data['usuario'];
        $sql = "CALL sp_docente_contrato_by_ficha(?,?,?,?)";
        $query = $this->db->query($sql, array($usuario, $ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_comunas($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_comuna_select_name_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_contrato($data) {

        $relator_ficha = $data['relator_ficha'];
        $fecha_emision = $data['fecha_emision'];
        $nombre_relator = $data['nombre_relator'];
        $rut_relator = $data['rut_relator'];
        $direccion_relator = $data['direccion_relator'];
        $comuna_relator = $data['comuna_relator'];
        $curso_relator = $data['curso_relator'];
        $horas_curso = $data['horas_curso'];
        $fecha_termino = $data['fecha_termino'];
        $valor_hora = $data['valor_hora'];
        $fecha_boleta = $data['fecha_boleta'];
        $horas_pagadas = $data['horas_pagadas'];
        $alumnos = $data['alumnos'];
        $firma = $data['firma'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];



        $sql = "CALL sp_contrato_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(
            $relator_ficha,
            $fecha_emision,
            $nombre_relator,
            $rut_relator,
            $direccion_relator,
            $comuna_relator,
            $curso_relator,
            $horas_curso,
            $fecha_termino,
            $valor_hora,
            $fecha_boleta,
            $horas_pagadas,
            $alumnos,
            $firma,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
