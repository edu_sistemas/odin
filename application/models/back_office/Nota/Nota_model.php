<?php
/**
 * nota_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-02-27 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2017-02-07 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Nota_model extends MY_Model {

    function get_arr_listar_ficha_reserva_cbx($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_reserva_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_alumnos($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_alumnos_nota_select(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_registrar_notas($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Registra de las notas de los alumnos
          Fecha Actualiza: 2017-01-12
         */
        $result = false;
        $total = $data['total'];        //Total de alumnos
        $columnas = $data['columnas'];  //Cantidad de Columnas
        $largo = $data['largo'];
        $x = 0;
        $y = 0;
       // var_dump($data);
        for ($var = 0; $var < $total * $columnas; $var++) {
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

          /*  if ($var < $columnas) {
                $detalle_alumno = $data['dtl'][$var];
            } else {
                $detalle_alumno = $data['dtl'][$x];
                $x++;

                if ($x == $total) {

                    $x = 0;
                }
            }

            $nota = $data['Nota'][$var];          
            $nuevo = str_replace(".", "", $nota);

            if ($largo > 0) {
                $id_nota = $data['notas_id'][$var];
            } else {

                $id_nota = '';
            }*/

                if((($var - $total*($columnas - $largo)) < 0) || (($columnas - $largo) == 0)){
                    if(($columnas - $largo) == 0){
                        $largo = 0;
                    }
                    if ($y < ($columnas - $largo)) {
                        $detalle_alumno = $data['dtl'][$x];
                        $detalle_alumno1 = $data['dtl'][$x]  . 'ify '. $x;
                        $y++;
                        if (($x == ($total - 1)) && ($y == ($columnas - $largo))) {
                            $x = 0;
                        }
                        
                    } else {
                        $x++;
                        $y = 1;
                        $detalle_alumno = $data['dtl'][$x];
                        $detalle_alumno1 = $data['dtl'][$x] . 'elsey '. $x;
                        if (($x == ($total - 1)) && ($y == ($columnas - $largo))) {
                            $x = 0;
                        }
                      }
                } else {
                    $detalle_alumno = $data['dtl'][$x];
                    $detalle_alumno1 = $data['dtl'][$x] . 'elsetoal ' . $x;
                    $x++;
                    if ($x == $total) {
                        $x = 0;
                    }
                }
           /* } else {
                $id_nota = '';
                $detalle_alumno = $data['dtl'][$x];
                $x++;
                if ($x == $total) {
                    $x = 0;
                }

            }*/

            $nota = $data['Nota'][$var];          
            $nuevo = str_replace(".", "", $nota);

            $id_nota = $data['notas_id'][$var];
            
      //   var_dump($detalle_alumno1, $id_nota, $nuevo, $user_id);
            $sql = "CALL sp_nota_insert(?,?,?,?,?)";
            $query = $this->db->query($sql, array($id_nota, $detalle_alumno, $nuevo, $user_id, $user_perfil));
            $result = $query->result_array();
            $query->next_result();
            $query->free_result();
        }
        return $result;
    }

    function set_arr_cerrar_curso($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Cierra un curso es el paso previo para sacar el promedio de las notas
          Fecha Actualiza: 2017-01-12
         */
        
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        
        $sql = "CALL sp_nota_curso_close(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        
        return $result;
    }
    
        function set_arr_calcular_promedio($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Calcula los promedios
          Fecha Actualiza: 2017-02-03
         */
        
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        
        $sql = "CALL sp_nota_promedio(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        
        return $result;
    }

}
