<?php

/**
 * Edutecno_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2018-01-10 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Ficha_model
 */
class Ficha_model extends MY_Model {

    function set_dia_ficha($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $pdia = $data['dia'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_dia(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $pdia, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_estado_cbx($data) {
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_estado_edutecno_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_codeSence_cbx($data) {
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_code_sence_select_ficha_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_delete_dia_ficha($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_delete_dia(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_ficha_select_eventos_by_id($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_eventos_by_id(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_ficha_select_log_by_id($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_log_by_id(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_ficha($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data ['start'];
        $end = $data ['end'];
        $estado = $data ['estado'];
        $empresa = $data ['empresa'];
        $holding = $data ['holding'];

        $ejecutivo = $data ['ejecutivo'];
        $periodo = $data ['periodo'];
        $num_ficha = $data ['ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_select(?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $holding,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
        ));

        // echo $this->db->last_query();

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_ficha_variante($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data ['start'];
        $end = $data ['end'];
        $estado = $data ['estado'];
        $empresa = $data ['empresa'];
        $ejecutivo = $data ['ejecutivo'];
        $periodo = $data ['periodo'];
        $num_ficha = $data ['ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_select_variante(?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
        ));

        // echo $this->db->last_query();

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_ficha_timeline($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data ['start'];
        $end = $data ['end'];
        $estado = $data ['estado'];
        $empresa = $data ['empresa'];
        $ejecutivo = $data ['ejecutivo'];
        $periodo = $data ['periodo'];
        $num_ficha = $data ['ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_select_timeline(?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
        ));

        // echo $this->db->last_query();

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_ficha_estado_cerrado($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $start = $data ['start'];
        $end = $data ['end'];
        $estado = $data ['estado'];
        $empresa = $data ['empresa'];
        $holding = $data ['holding'];

        $ejecutivo = $data ['ejecutivo'];
        $periodo = $data ['periodo'];
        $num_ficha = $data ['ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_select_estado_cerrado(?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $holding,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
        ));

        // echo $this->db->last_query();

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_seguimiento_ficha($data) {
        /*
          Ultimo usuario  modifica:    Luis Jarpa
          Descripcion : el seguimiento de los ejecutivos y back office
          Fecha Actualiza: 2016-12-26
         */
        $start = $data ['start'];
        $end = $data ['end'];
        $estado = $data ['estado'];
        $empresa = $data ['empresa'];
        $holding = $data ['holding'];

        $ejecutivo = $data ['ejecutivo'];
        $periodo = $data ['periodo'];
        $num_ficha = $data ['ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];


        $sql = "CALL sp_ficha_select_seguimiento(?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $start,
            $end,
            $estado,
            $empresa,
            $holding,
            $num_ficha,
            $ejecutivo,
            $periodo,
            $user_id,
            $user_perfil
        ));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_resctificaciones($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_select(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_periodo_venta($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_consulta_periodo_venta(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_oc_by_ficha_cbx($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_ficha_id_accion(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getIDSenceByFicha($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_id_sence_select_by_ficha(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_periodo_anio_venta($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_consulta_anio_venta(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_orden_compra($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_alumno(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_documentos_resumen_orden_compra($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $p_orden_compra = $data['orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_orden_compra_select_by_id_documentos(?,?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $p_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_valida_envio_ficha($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         * Modificación 2017-07-14 se agrega el rechazo
         */
        $p_id_ficha = $data['id_ficha'];
        $respuesta = $data['respuesta'];
        $rechazo = $data['rechazo'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = "CALL sp_ficha_valida_envio_ficha(?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $p_id_ficha,
            $respuesta,
            $rechazo,
            $user_id,
            $user_perfil
                )
        );
        // echo $this->db->last_query();

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de la ficha
          Fecha Actualiza: 2017-01-16
         */
        $id_edutecno = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id_preview($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de la ficha
          Fecha Actualiza: 2017-01-16
         */
        $id_edutecno = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id_preview(?,?,?)";
        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_by_id_resumen($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera datos de la ficha
          Fecha Actualiza: 2017-01-16
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id_resumen(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ficha_arriendo_by_id($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_arriendo_select_by_id(?,?,?);";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function cargar_alumnos_tbl_temporal($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_orden_compra_carga_alumno_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_edit_orden_compra($data) {
        /*
          Ultimo usuario modifica: Luis Jarpa
          Descripcion : Actualiza orden de compra
          Fecha Actualiza: 2016-12-02
         */

        $id_ficha = $data['id_ficha'];
        $id_curso = $data['id_curso'];
        $id_empresa = $data['id_empresa'];
        $id_otic = $data['id_otic'];
        $comentario = $data['comentario'];
        $num_orden_compra = $data['num_orden_compra'];
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];
        $fecha_cierre = $data['fecha_cierre'];
        $txt_hora_inicio = $data['hora_inicio'];
        $txt_hora_termino = $data['hora_termino'];

        $chk_sin_orden_compra = $data['sin_orden_compra'];
        $ejecutivo = $data['ejecutivo'];

        $id_solicitud_tablet = $data['id_solicitud_tablet'];
        $id_relacionar_ficha_con = $data['id_relacionar_ficha_con'];
        $id_relacionar_ficha_con_hide = $data['id_relacionar_ficha_con_hide'];

        $sala = $data['sala'];

        $lugar_ejecucion = $data['lugar_ejecucion'];

        $lugar_entrega_diplomas = $data['lugar_entrega_diplomas'];


        $chk_sence = $data['chk_sence'];
        $codi_sence = $data['code_sence'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $id_version = $data['id_version'];
        $direccion_empresa = $data['direccion_empresa'];
        $nombre_contacto_empresa = $data['nombre_contacto_empresa'];
        $telefono_contacto_empresa = $data['telefono_contacto_empresa'];




        $sql = "CALL sp_ficha_update_orden_compra(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_curso,
            $id_empresa,
            $id_otic,
            $num_orden_compra,
            $comentario,
            $fecha_inicio,
            $fecha_termino,
            $fecha_cierre,
            $txt_hora_inicio,
            $txt_hora_termino,
            $ejecutivo,
            $sala,
            $lugar_ejecucion,
            $lugar_entrega_diplomas,
            $chk_sence,
            $codi_sence,
            $chk_sin_orden_compra,
            $id_version,
            $direccion_empresa,
            $nombre_contacto_empresa,
            $telefono_contacto_empresa,
            $id_solicitud_tablet,
            $id_relacionar_ficha_con,
            $id_relacionar_ficha_con_hide,
            $user_id,
            $user_perfil
        ));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_delete_orden_compra($data) {


        $id_ficha = $data['id_ficha'];
//  $id_orden_compra = $data['id_orden_compra'];
        $num_orden_compra = $data['num_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_delete_orden_compra(?,?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $num_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_ingresa_id_accion_sence($data) {
        $id_ficha = $data['id_ficha'];
        $id_accion_sence = $data['id_accion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_insert_id_sence(?,?,?,?);";

        $query = $this->db->query($sql, array($id_ficha,$id_accion_sence, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function del_arr_ingresa_id_accion_sence($data) {
        $id_ficha = $data['id_ficha'];
        $id_accion_sence = $data['id_accion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_delete_id_accion_sence(?,?,?,?);";

        $query = $this->db->query($sql, array($id_ficha,$id_accion_sence, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_id_accion_sence_consolidado($data) {

        $id_ficha_consolidado = $data['id_ficha_consolidado'];
        $id_accion_consolidado = $data['id_accion_consolidado'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_id_sence_consolidado(?,?,?,?);";

        $query = $this->db->query($sql, array($id_ficha_consolidado, $id_accion_consolidado, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_change_num_orden_compra($data) {


        $id_oc = $data['id_oc'];
        $new_orden_compra = $data['new_orden_compra'];
        $combo_decicion = $data['combo_decicion'];
        $id_ficha_frm_change_oc = $data['id_ficha_frm_change_oc'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];



        $sql = "CALL sp_orden_compra_change_num_oc(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_oc, $new_orden_compra, $combo_decicion, $id_ficha_frm_change_oc, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_modalidad_by_codeSence($data) {
        $id_code_sence = $data['id_code_sence'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_modalidad_select_by_code_sence(?,?,?);";

        $query = $this->db->query($sql, array($id_code_sence, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_nueva_ficha($data) {

        $id_categoria = $data['id_categoria'];
        $ejecutivo = $data['ejecutivo'];
        $id_solicitud_tablet = $data['id_solicitud_tablet'];


        $sala = $data['sala'];
        $lugar_ejecucion = $data['lugar_ejecucion'];
        $lugar_entrega_diplomas = $data['lugar_entrega_diplomas'];

        $direccion_empresa = $data['direccion_empresa'];
        $nombre_contacto_empresa = $data['nombre_contacto_empresa'];
        $telefono_contacto_empresa = $data['telefono_contacto_empresa'];

        $id_relacionar_ficha_con = $data['id_relacionar_ficha_con'];
        $requiere_equipos = $data['requiere_equipos'];
        
        $fecha_envio_carta = $data['fecha_envio_carta'];



        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_categoria,
            $ejecutivo,
            $sala,
            $lugar_ejecucion,
            $lugar_entrega_diplomas,
            $direccion_empresa,
            $nombre_contacto_empresa,
            $telefono_contacto_empresa,
            $id_solicitud_tablet,
            $id_relacionar_ficha_con,
            $requiere_equipos,
            $fecha_envio_carta,
            $user_id,
            $user_perfil
                )
        );
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_nuevo_arriendo_sala($data) {

        $id_categoria = $data['id_categoria'];
        $empresa = $data['empresa'];
        $hora_inicio = $data['hora_inicio'];
        $hora_termino = $data['hora_termino'];
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];
        $sala = $data['sala'];
        $n_dias = $data['n_dias'];
        $valor_dia = $data['valor_dia'];
        $total_dia = $data['total_dia'];
        $ejecutivo = $data['ejecutivo'];
        $break = $data['break'];
        $n_alumnos = $data['n_alumnos'];
        $valor_break = $data['valor_break'];
        $total_en_break = $data['total_en_break'];
        $total_venta = $data['valorVenta'];
        $total_iva = $data['iva'];
        $total_bruto = $data['totalVenta'];
        $comentario = $data['comentario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = "CALL sp_arriendo_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_categoria,
            $empresa,
            $hora_inicio,
            $hora_termino,
            $fecha_inicio,
            $fecha_termino,
            $sala,
            $n_dias,
            $valor_dia,
            $total_dia,
            $ejecutivo,
            $break,
            $n_alumnos,
            $valor_break,
            $total_en_break,
            $total_venta,
            $total_iva,
            $total_bruto,
            $comentario,
            $user_id,
            $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function update_arriendo_sala($data) {

        $id_ficha = $data['id_ficha'];
        $empresa = $data['empresa'];
        $hora_inicio = $data['hora_inicio'];
        $hora_termino = $data['hora_termino'];
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];

        $sala = $data['sala'];
        $n_dias = $data['n_dias'];
        $valor_dia = $data['valor_dia'];
        $total_dia = $data['total_dia'];
        $ejecutivo = $data['ejecutivo'];
        $break = $data['break'];
        $n_alumnos = $data['n_alumnos'];
        $valor_break = $data['valor_break'];
        $total_en_break = $data['total_en_break'];
        $total_venta = $data['valorVenta'];
        $total_iva = $data['iva'];
        $total_bruto = $data['totalVenta'];
        $comentario = $data['comentario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_arriendo_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $empresa,
            $hora_inicio,
            $hora_termino,
            $fecha_inicio,
            $fecha_termino,
            $sala,
            $n_dias,
            $valor_dia,
            $total_dia,
            $ejecutivo,
            $break,
            $n_alumnos,
            $valor_break,
            $total_en_break,
            $total_venta,
            $total_iva,
            $total_bruto,
            $comentario,
            $user_id,
            $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_delete_ficha($data) {
        $id_categoria = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_delete(?,?,?);";

        $query = $this->db->query($sql, array($id_categoria, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_nueva_orden_compra($data) {

        $id_ficha = $data['id_ficha'];
        $id_curso = $data['id_curso'];

        $id_version = $data['id_version'];
        $id_empresa = $data['id_empresa'];
        $id_otic = $data['id_otic'];
        $comentario = $data['comentario'];
        $num_orden_compra = $data['num_orden_compra'];
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];
        $fecha_cierre = $data['fecha_cierre'];
        $txt_hora_inicio = $data['hora_inicio'];

        $txt_hora_termino = $data['hora_termino'];

        $codi_sence = $data['code_sence'];
        $aplica_sence = $data['aplica_sence'];
        $chk_sin_orden_compra = $data['sin_orden_compra'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_orden_compra(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_curso,
            $id_version,
            $id_empresa,
            $id_otic,
            $num_orden_compra,
            $comentario,
            $fecha_inicio,
            $fecha_termino,
            $fecha_cierre,
            $txt_hora_inicio,
            $txt_hora_termino,
            $codi_sence,
            $aplica_sence,
            $chk_sin_orden_compra,
            $user_id,
            $user_perfil
                )
        );
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_subir_alumnos_orden_compra($data) {
        /*
          Ultimo usuario  modifica: David De Filippi , Luis Jarpa
          Descripcion : Carga ALumnos a tabla temporal
          Fecha Actualiza: 2017-01-16
         */







        $id_orden_compra = $data['id_orden_compra'];
        $rut = $data['rut'];
        $dv = $data['dv'];
        $nombre = $data['nombre'];
        $segundo_nombre = $data['segundo_nombre'];
        $apellido_paterno = $data['apellido_paterno'];
        $apellido_materno = $data['apellido_materno'];
        $email = $data['email'];
        $telefono = $data['telefono'];
        $fq = $data['fq'];
        $valor_fq = $data['valor_fq'];
        $valor_adicional = $data['valor_adicional'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        //$sql = "CALL sp_orden_compra_carga_alumno_insert(?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $rut,
            $dv,
            $nombre,
            $segundo_nombre,
            $apellido_paterno,
            $apellido_materno,
            $email,
            $telefono,
            $fq,
            $valor_fq,
            $valor_adicional,
            $user_id,
            $user_perfil
        ));




        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_orden_compra_insert_alumnos($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2017-01-17
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_insert_tbl_alumno_a_orden(?,?)";
        $query = $this->db->query($sql, array(
            $user_id, $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_eliminar_alumnos_temporal($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_orden_compra_carga_alumno_delete(?,?);";

        $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_orden_compra_by_ficha_id($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de las ordenes de compra
          Fecha Actualiza: 2017-01-16
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_ficha_id(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_aprobar_ficha($data) {


        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_enviar(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_info_llevar_equipos_ficha($data) {

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_get_info_llevar_equipos(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_subir_insert_alumnos_orden_compra_rectificacion($data) {
        /*
          Ultimo usuario  modifica: David De Filippi , Luis Jarpa
          Descripcion : Carga ALumnos a tabla temporal
          Fecha Actualiza: 2017-05-06
         */



        $id_accion = $data['id_accion'];
        $tipo_rectificacion = $data['tipo_rectificacion'];
        $id_orden_compra = $data['id_orden_compra'];
        $rut = $data['rut'];
        $dv = $data['dv'];
        $nombre = $data['nombre'];
        $segundo_nombre = $data['segundo_nombre'];
        $apellido_paterno = $data['apellido_paterno'];
        $apellido_materno = $data['apellido_materno'];
        $email = $data['email'];
        $telefono = $data['telefono'];
        $fq = $data['fq'];
        $valor_fq = $data['valor_fq'];
        $valor_adicional = $data['valor_adicional'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_accion_agregar(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_accion,
            $tipo_rectificacion,
            $id_orden_compra,
            $rut,
            $dv,
            $nombre,
            $segundo_nombre,
            $apellido_paterno,
            $apellido_materno,
            $email,
            $telefono,
            $fq,
            $valor_fq,
            $valor_adicional,
            $user_id,
            $user_perfil
        ));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_subir_alumnos_orden_compra_rectificacion($data) {
        /*
          Ultimo usuario  modifica: David De Filippi , Luis Jarpa
          Descripcion : Carga ALumnos a tabla temporal par ala rectificacion
          Fecha Actualiza: 2017-05-06
         */


        $id_accion = $data['id_accion'];
        $tipo_rectificacion = $data['tipo_rectificacion'];
        $id_orden_compra = $data['id_orden_compra'];
        $rut = $data['rut'];
        $dv = $data['dv'];
        $nombre = $data['nombre'];
        $segundo_nombre = $data['segundo_nombre'];
        $apellido_paterno = $data['apellido_paterno'];
        $apellido_materno = $data['apellido_materno'];
        $email = $data['email'];
        $telefono = $data['telefono'];
        $fq = $data['fq'];
        $valor_fq = $data['valor_fq'];
        $valor_adicional = $data['valor_adicional'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_accion_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_accion,
            $tipo_rectificacion,
            $id_orden_compra,
            $rut,
            $dv,
            $nombre,
            $segundo_nombre,
            $apellido_paterno,
            $apellido_materno,
            $email,
            $telefono,
            $fq,
            $valor_fq,
            $valor_adicional,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_orden_compra_accion_actualizar_alumnos($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2017-01-17
         */


        $nombre_archivo_plantilla = $data['nombre_archivo_plantilla'];
        $ruta_archivo_plantilla = $data['ruta_archivo_plantilla'];


        $nombre_archivo_adjunto = $data['nombre_archivo_adjunto'];
        $ruta_archivo_adjunto = $data['ruta_archivo_adjunto'];
        $comentario = $data['comentario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_accion_actualizar_tbl_alumno(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $nombre_archivo_plantilla, $ruta_archivo_plantilla,
            $nombre_archivo_adjunto, $ruta_archivo_adjunto, $comentario,
            $user_id, $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_subir_alumnos_orden_compra_eliminar_rectificacion($data) {
        /*
          Ultimo usuario  modifica: David De Filippi , Luis Jarpa
          Descripcion : Carga ALumnos a tabla temporal par ala rectificacion
          Fecha Actualiza: 2017-01-18
         */

        $id_accion = $data['id_accion'];
        $tipo_rectificacion = $data['tipo_rectificacion'];
        $id_causa = $data['id_causa'];
        $id_orden_compra = $data['id_orden_compra'];
        $rut = $data['rut'];
        $dv = $data['dv'];
        $nombre = $data['nombre'];
        $apellido_paterno = $data['apellido_paterno'];
        $apellido_materno = $data['apellido_materno'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_accion_eliminar(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_accion,
            $tipo_rectificacion,
            $id_causa,
            $id_orden_compra,
            $rut,
            $dv,
            $nombre,
            $apellido_paterno,
            $apellido_materno,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_orden_compra_accion_eliminar_alumnos($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2017-01-17
         */

        $nombre_archivo_plantilla = $data['nombre_archivo_plantilla'];
        $ruta_archivo_plantilla = $data['ruta_archivo_plantilla'];

        $nombre_archivo_adjunto = $data['nombre_archivo_adjunto'];
        $ruta_archivo_adjunto = $data['ruta_archivo_adjunto'];
        $comentario = $data['comentario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_accion_eliminar_tbl_alumno(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $nombre_archivo_plantilla, $ruta_archivo_plantilla,
            $nombre_archivo_adjunto, $ruta_archivo_adjunto, $comentario,
            $user_id, $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_orden_compra_accion_insert_alumnos($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2017-01-17
         */
        $nombre_archivo_plantilla = $data['nombre_archivo_plantilla'];
        $ruta_archivo_plantilla = $data['ruta_archivo_plantilla'];


        $nombre_archivo_adjunto = $data['nombre_archivo_adjunto'];
        $ruta_archivo_adjunto = $data['ruta_archivo_adjunto'];
        $comentario = $data['comentario'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_carga_alumno_insert_tbl_alumno(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $nombre_archivo_plantilla, $ruta_archivo_plantilla,
            $nombre_archivo_adjunto, $ruta_archivo_adjunto, $comentario,
            $user_id, $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_motivo_rechazo_ficha($data) {
        $p_id_ficha = $data['id_ficha'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_motivo_rechazo(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_motivo_anulacion_ficha($data) {
        $p_id_ficha = $data['id_ficha'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_motivo_anulacion(?,?,?)";
        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* Rectificacion */

    function get_arr_ficha_by_id_rectificacion($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de la ficha mediante una rectificacion
          Fecha Actualiza: 2017-01-16
         */
        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_orden_compra_by_rectificacion_ficha_id($data) {

        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Recupeera datos de las ordenes de compra mediante una rectificacion
          Fecha Actualiza: 2017-01-16
         */

        $id_ficha = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_rectificacion_ficha_id(?,?,?);";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_estado_resctificaciones($data) {
        /*
         * Ultimo usuario modifica: David De Filippi , Luis Jarpa
         * Descripcion : Lista las edutecno
         * Fecha Actualiza: 2016-12-26
         */
        $id_rectificacion = $data ['id_rectificacion'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_get_estado_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array(
            $id_rectificacion,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_alumno_by_ficha($data) {
      
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_select_alumnos_by_id_oc(?,?,?)";
        $query = $this->db->query($sql, array($num_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_orden_compra_by_rectificacion($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_alumno_rectificacion_jc(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_documentos_resumen_orden_compra_by_rectificacion($data) {
        /*
          Ultimo usuario  modifica:    Luis Jarpa
          Descripcion : Lista los documetnos de una ficha por id rectificacion
          Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_documentos_by_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_rectificacion_orden_compra_by_rectificacion($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_rectificacion_select_by_id_alumno_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_datos_documentos_rectificacion_resumen_orden_compra_by_rectificacion($data) {
        /*
          Ultimo usuario  modifica:    Luis Jarpa
          Descripcion : Lista los documetnos de una ficha por id rectificacion
          Fecha Actualiza: 2016-12-26
         */

        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_select_by_id_documentos_rect_by_rectificacion(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_decomentos_rectificacion($data) {
        $id_rectificacion = $data['id_rectificacion'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_documentos(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_motivo_rechazo_rectificacion($data) {
        $id_rectificacion = $data['id_rectificacion'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_motivo_rechazo(?,?,?)";
        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_aprobar_rectificacion($data) {


        $id_rectificacion = $data['id_rectificacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_enviar(?,?,?)";

        $query = $this->db->query($sql, array($id_rectificacion, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_agregar_oc_rectificacion($data) {


        $id_ficha = $data['x_id_ficha'];
        $razon_social = $data['razon_social'];
        $otic = $data['otic'];
        $numero_oc = $data['numero_oc'];
        $observaciones = $data['observaciones'];
        $comite_bipartito = $data['comite_bipartito'];



        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_orden_compra_insert(?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $razon_social,
            $otic,
            $numero_oc,
            $observaciones,
            $comite_bipartito,
            $user_id,
            $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_document_rectificacion($data) {


        $id_rectificacion = $data['id_rectificacion'];

        $nombre_documento = $data['nombre_documento'];
        $ruta_documento = $data['ruta_documento'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_insert_doc(?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_rectificacion,
            $nombre_documento,
            $ruta_documento,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_rectificacion_extencion_plazo($data) {
        /*
          Ultimo usuario  modifica: Lusi Jarpa
          Descripcion : Añade una nueva rectificaicon de tipo extencion de plazo
          Fecha Actualiza: 2017-02-07
         */


        $id_orden_compra = $data['id_orden_compra'];
        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termnino = $data['fecha_termnino'];
        $ruta_archivo = $data['ruta_archivo'];
        $nombre_archivo = $data['nombre_archivo'];
        $comentario = $data['comentario'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_rectificacion_extension_plazo(?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $fecha_inicio,
            $fecha_termnino,
            $nombre_archivo,
            $ruta_archivo,
            $comentario,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_aprobar_ficha_facturacion($data) {

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_enviar_facturacion(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_FichasRelacionar_cbx($data) {
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_ficha_relacionadas_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_adicionales_ficha_by_id($data) {
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_select_items_ficha(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_adicionales_ficha_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_select_items_cbx(?,?)";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function getSolicitudesTabletCBX($data) {
        $id_curso = $data['id_curso'];
        $id_ejecutivo_comercial = $data['id_ejecutivo_comercial'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_select_solicitudes_tablet_cbx(?,?,?,?)";

        $query = $this->db->query($sql, array($id_curso, $id_ejecutivo_comercial, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_item_ficha($data) {
        $id_ficha = $data['id_ficha'];
        $id_item = $data['id_item'];
        $detalle = $data['detalle'];
        $valor = $data['valor'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_item(?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_item,
            $detalle,
            $valor,
            $user_id,
            $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function editar_item_ficha($data) {
        $id_item_ficha_editar = $data['id_item_ficha_editar'];
        $id_item_editar = $data['id_item_editar'];
        $detalle_editar = $data['detalle_editar'];
        $valor_editar = $data['valor_editar'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_update_item_ficha(?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_item_ficha_editar,
            $id_item_editar,
            $detalle_editar,
            $valor_editar,
            $user_id,
            $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_item_ficha($data) {
        $id_item = $data['id_item'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_select_item_ficha_by_id_item(?,?,?)";

        $query = $this->db->query($sql, array(
            $id_item,
            $user_id,
            $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function eliminar_item_ficha($data) {
        $id_item = $data['id_item'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_delete_item_ficha(?,?,?)";

        $query = $this->db->query($sql, array(
            $id_item,
            $user_id,
            $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
}
