<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 *   Relator_Model
 */
class Recurrente_model extends MY_Model {

	function get_arr_ficha_by_id_resumen_recurrentes($data) {
		/*
		  Ultimo usuario  modifica: Luis Jarpa
		  Descripcion : Recupeera datos de la ficha para editar datos recurrentes
		  Fecha Actualiza: 2017-07-06
		 */
		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = " CALL sp_ficha_select_by_id_resumen_datos_recurrentes(?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function get_arr_ficha_by_id_resumen_recurrentes_correo($data) {
		/*
		  Ultimo usuario  modifica: Luis Jarpa
		  Descripcion : Recupeera datos de la ficha para editar datos recurrentes
		  Fecha Actualiza: 2017-07-06
		 */
		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = " CALL sp_ficha_select_by_id_resumen_datos_recurrentes_correo(?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function set_delete_dia_ficha($data) {
		/*
		  Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
		  Descripcion : Lista las edutecno
		  Fecha Actualiza: 2016-12-26
		 */

		$p_id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = "CALL sp_ficha_delete_dia(?,?,?)";
		$query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));

		//echo $this->db->last_query();
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function set_edit_orden_compra($data) {
		/*
		  Ultimo usuario modifica: Luis Jarpa
		  Descripcion : Actualiza orden de compra
		  Fecha Actualiza: 2016-12-02
		 */

		$id_ficha = $data['id_ficha'];
		$hora_inicio = $data['hora_inicio'];
		$hora_termino = $data['hora_termino'];
		$id_relacionar_ficha_con = $data['id_relacionar_ficha_con'];
		$id_relacionar_ficha_con_hide = $data['id_relacionar_ficha_con_hide'];

		$sala = $data['sala'];
		$sede = $data['sede'];
		$lugar = $data['lugar'];
		$observacion = $data['observacion'];
		$pdia = $data['dia_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];


		$sql = "CALL sp_datos_recurrentes_insert(?,?,?,?,?,?,?,?,?,?,?,?);";

		$query = $this->db->query($sql, array(
			$id_ficha,
			$observacion,
			$hora_inicio,
			$hora_termino,
			$sede,
			$sala,
			$lugar,
			$pdia,
			$id_relacionar_ficha_con,
			$id_relacionar_ficha_con_hide,
			$user_id,
			$user_perfil
		));


		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function set_arr_modificar_recurrentes($data) {

		$p_dia = $data['diax'];
		$p_id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = "CALL sp_datos_recurrentes_insert_dias(?,?,?,?);";
		$query = $this->db->query($sql, array($p_id_ficha, $p_dia, $user_id, $user_perfil));

		// echo $this->db->last_query();
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function set_delete_reserva_sala_ficha($data) {

		$id_reserva = $data['id_reserva'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = "CALL sp_reserva_delete_reserva(?,?,?);";
		$query = $this->db->query($sql, array($id_reserva, $user_id, $user_perfil));

		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function set_insert_reserva_recurrente($data) {

		$id_ficha = $data['id_ficha'];
		$fecha_inicio = $data['fecha_inicio'];
		$fecha_termino = $data['fecha_termino'];
		$lunes = $data['lunes'];
		$martes = $data['martes'];
		$miercoles = $data['miercoles'];
		$jueves = $data['jueves'];
		$viernes = $data['viernes'];
		$sabado = $data['sabado'];
		$domingo = $data['domingo'];
		$estado = $data['estado'];
		$id_sala = $data['id_sala'];
		$nombre_reserva = $data['nombre_reserva'];
		$detalle_reserva = $data['detalle_reserva'];
		$break = $data['break'];
		$computadores = $data['computadores'];
		$ejecutivo = $data['ejecutivo'];
		$holding = $data['holding'];
		$empresa = $data['empresa'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = "CALL sp_reserva_full_automatico(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
		$query = $this->db->query($sql, array(
			$id_ficha,
			$fecha_inicio,
			$fecha_termino,
			$lunes,
			$martes,
			$miercoles,
			$jueves,
			$viernes,
			$sabado,
			$domingo,
			$estado,
			$id_sala,
			$nombre_reserva,
			$detalle_reserva,
			$break,
			$computadores,
			$ejecutivo,
			$holding,
			$empresa,
			$user_id,
			$user_perfil
				)
		);
		 
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

}
