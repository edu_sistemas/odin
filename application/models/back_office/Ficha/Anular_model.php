<?php

/**
 * Edutecno_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-16 [Luis Jarpa] <lajrpa@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Ficha_model
 */
class Anular_model extends MY_Model {

	function get_arr_anulacion_by_id_ficha($data) {
		/*
		  Ultimo usuario  modifica: Luis Jarpa
		  Descripcion : Recupeera datos de la ficha
		  Fecha Actualiza: 2017-01-16
		 */
		$id_edutecno = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = " CALL sp_anulacion_select_by_id_ficha(?,?,?)";
		$query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function get_anulacion_by_id_ficha_view($data) {
		/*
		  Ultimo usuario  modifica: Luis Jarpa
		  Descripcion : Recupeera datos de la ficha
		  Fecha Actualiza: 2017-01-16
		 */
		$id_edutecno = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		$sql = " CALL sp_anulacion_select_by_id_resumen(?,?,?)";
		$query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function get_arr_orden_compra_by_ficha_id($data) {

		/*
		  Ultimo usuario  modifica: Luis Jarpa
		  Descripcion : Recupeera datos de las ordenes de compra
		  Fecha Actualiza: 2017-01-16
		 */

		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = "CALL sp_orden_compra_select_by_ficha_id(?,?,?);";

		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	//-----------	Funciones para los documentos

	function set_document_ficha($data) {

		$id_ficha = $data['id_ficha'];
		$nombre_documento = $data['nombre_documento'];
		$fichero_subido = $data['ruta_documento'];
		$motivo = $data['motivo'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = "CALL sp_anulacion_insert_doc_by_ficha(?,?,?,?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $nombre_documento, $fichero_subido, $motivo, $user_id, $user_perfil));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function get_arr_anulacion_docs($data) {
		/*
		  Ultimo usuario  modifica: Luis Jarpa
		  Descripcion : Lista los los contactos que puede tener el empresa
		  Fecha Actualiza: 2016-11-28
		 */

		$id_ficha = $data['id_ficha'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = "CALL sp_anulacion_select_docs(?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

		$result = $query->result_array();
		$query->next_result();
		$query->free_result();

		return $result;
	}

	// FIN FUNCIONES DE DOCUMENTOS

	function set_motivo_anulacion($data) {

		$id_ficha = $data['id_ficha'];
		$motivo = $data['motivo'];
		$estado_edu = $data['estado_edu'];
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];

		$sql = "call sp_update_anulacion_ficha_edu(?,?,?,?,?)";
		$query = $this->db->query($sql, array($id_ficha, $motivo, $estado_edu, $user_id, $user_perfil));
 
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();

		return $result;
	}

}
