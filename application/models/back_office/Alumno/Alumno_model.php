<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Perfil_Model
 */
class Alumno_Model extends MY_Model {

      function set_arr_agregar_alumno($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Añade un nuevo Alumno
          Fecha Actualiza: 2017-09-17
         */
        $rut_alumno              = $data['rut_alumno'];
        $dv_alumno               = $data['dv_alumno'];
        $usuario_alumno          = $data['usuario_alumno'];
        $nombre_alumno           = $data['nombre_alumno'];
        $seg_nombre_alumno       = $data['seg_nombre_alumno'];
        $apellido_paterno_alumno = $data['apellido_paterno_alumno'];
        $apellido_materno_alumno = $data['apellido_materno_alumno'];
        $fecha_nacimiento        = $data['fecha_nacimiento'];
        $genero = $data['genero'];
        $telefono_alumno = $data['telefono_alumno_v'];
        $correo_alumno = $data['correo_alumno'];
        $nivel_educacional = $data['nivel_educacional'];
        $cargo_alumno = $data['cargo_alumno_v'];
        $profesion_alumno = $data['profesion_alumno_v'];
        $direccion_alumno = $data['direccion_alumno_v'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_alumno_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql,array(
            $rut_alumno,
            $dv_alumno,
            $usuario_alumno,
            $nombre_alumno,
            $seg_nombre_alumno,
            $apellido_paterno_alumno,
            $apellido_materno_alumno,
            $fecha_nacimiento,
            $genero,
            $telefono_alumno,
            $correo_alumno,
            $nivel_educacional,
            $cargo_alumno,
            $profesion_alumno,
            $direccion_alumno,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();

        return $result;
    }

    function get_arr_genero_alumno($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_genero_select_cbx(?,?);";

        $query = $this->db->query($sql,array(
            $user_id,$user_perfil,
        ));
        $result = $query->result_array();

        return $result;
    }

    function get_arr_nivel_educacional_alumno($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_nivel_educacional_select_cbx(?,?);";

        $query = $this->db->query($sql,array(
            $user_id,$user_perfil,
        ));
        $result = $query->result_array();

        return $result;
    }
    
   

    function get_arr_alumno_listar($data) {
       // var_dump($data);
        //die();
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los Alumnos
          Fecha Actualiza: 2016-10-25
         */

        $rut = $data['rut'];
        $nombre = $data['nombre'];
        $estado = $data['estado'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_alumno_select_filtrar(?,?,?,?,?,?);";

        $query = $this->db->query($sql,array($rut,$nombre,$estado,$num_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

        function set_arr_editar_alumno($data) {
        /*
          Ultimo usuario modifica: David De Filippi
          Descripcion : Actualiza los datos del alumno en la BD
          Fecha Actualiza: 2017-09-07
         */
        $id_alumno               = $data['id_alumno'];
        $rut_alumno              = $data['rut_alumno'];
        $dv_alumno               = $data['dv_alumno'];
        $usuario_alumno          = $data['usuario_alumno'];
        $nombre_alumno           = $data['nombre_alumno'];
        $seg_nombre_alumno       = $data['seg_nombre_alumno'];
        $apellido_paterno_alumno = $data['apellido_paterno_alumno'];
        $apellido_materno_alumno = $data['apellido_materno_alumno'];
        $fecha_nacimiento        = $data['fecha_nacimiento'];
        $genero = $data['genero'];
        $telefono_alumno = $data['telefono_alumno_v'];
        $correo_alumno = $data['correo_alumno'];
        $nivel_educacional = $data['nivel_educacional'];
        $cargo_alumno = $data['cargo_alumno_v'];
        $profesion_alumno = $data['profesion_alumno_v'];
        $direccion_alumno = $data['direccion_alumno_v'];

        $user_id                 = $data['user_id'];
        $user_perfil             = $data['user_perfil'];

        $sql = "CALL sp_alumno_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql,array( 
            $id_alumno,
            $rut_alumno,
            $dv_alumno,
            $usuario_alumno,
            $nombre_alumno,
            $seg_nombre_alumno,
            $apellido_paterno_alumno,
            $apellido_materno_alumno,
            $fecha_nacimiento,
            $genero,
            $telefono_alumno,
            $correo_alumno,
            $nivel_educacional,
            $cargo_alumno,
            $profesion_alumno,
            $direccion_alumno,
            $user_id,
            $user_perfil ));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        return $result;
    }

    function sp_alumno_select_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupera curso desde BD
          Fecha Actualiza: 2016-12-05
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_alumno_select_by_id('" . $id . "','" . $user_id . "','" . $user_perfil . "');";

        $query = $this->db->query($sql);
        $row_count = $query->num_rows();
        $result = false;
        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

  
    function set_sp_alumno_change_status($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia el estado de los usuarios
          Fecha Actualiza: 2016-11-25
         */
        $id_alumno = $data['id_alumno'];


        $estado = $data['estado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_alumno_change_status(?,?,?,?);";

        $query = $this->db->query($sql,array($id_alumno,$estado,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
     function get_listaAlumnosCursos($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia el estado de los usuarios
          Fecha Actualiza: 2016-11-25
         */
        $id_alumno = $data['id_alumno'];

 
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        //listar altiro null

        $sql = "CALL sp_alumno_curso_s_select(?,?,?);";

        $query = $this->db->query($sql,array($id_alumno,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
