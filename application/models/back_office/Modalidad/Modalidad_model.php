<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-01 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Modalidad_model
 */
class Modalidad_model extends MY_Model {

    function set_arr_agregar_modalidad($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una nueva  Modalidad
          Fecha Actualiza: 2016-12-01
         */

        $nombre_modalidad = $data['nombre_moda'];
        $descripcion_modalidad = $data['descripcion_moda'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_modalidad_insert(?,?,?,?)";
        $query = $this->db->query($sql, array($nombre_modalidad, $descripcion_modalidad, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    function get_arr_listar_modalidad($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las modalidades
          Fecha Actualiza: 2016-12-01
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_modalidad_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_modalidad_by_id($data) {

        /*
          SHACER EL SELECT BY ID DESDE EL EDITAR
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupera modalidad desde BD
          Fecha Actualiza: 2016-11-01
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_modalidad_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;
        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_modalidad($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita la modalidad desde la bd
          Fecha Actualiza: 2016-11-02
         */
        $id_modalidad = $data['id_modalidad'];
        $nombre_modalidad = $data['nombre_modalidad'];
        $descripcion_modalidad = $data['descripcion_modalidad'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_modalidad_update(?,?,?,?,?);";

        $query = $this->db->query($sql, array($id_modalidad, $nombre_modalidad, $descripcion_modalidad, $user_id, $user_perfil));
        $row_count = $query->num_rows();
        $result = false;

        if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_modalidad_estado($datos) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Modificar un módulo
          Fecha Actualiza: 2016-11-22
         */
        $id_modalidad = $datos['id_modalidad_m'];
        $estado_modalidad = $datos['estado_modalidad_m'];
        $user_id = $datos['user_id'];
        $user_perfil = $datos['user_perfil'];

        $sql = "CALL sp_modalidad_estado_change(?,?,?,?);";

        $query = $this->db->query($sql, array($id_modalidad, $estado_modalidad, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_modalidad_cbx($data) { /*
      lista los usuarios
      Descripcion : Lista la modalidad del curso
      Fecha Actualiza: 2016-12-09
     */
    	
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_modalidad_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    
    function get_arr_otec_cbx($data) { /*
      lista los usuarios
      Descripcion : Lista las otec que actualmente estan operando
      Fecha Actualiza: 2017-01-26
     */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_otec_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
