<?php

/**
 * Otic_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-02 [David De Filippi] <dfilippi@otic.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@otic.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Otic_model
 */
class Otic_model extends MY_Model {

  function get_arr_listar_otic_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_otic_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_otic($data) {

        /*
          Ultimo usuario  modifica:  David De Filippi
          Descripcion : Lista las otic
          Fecha Actualiza: 2016-12-02
         */

        $p_rut_otic = $data['rut_otic'];
        $p_nombre_otic = $data['nombre_otic'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = "CALL sp_otic_select(?, ?, ?, ?);";

        $query = $this->db->query($sql, array($p_rut_otic, $p_nombre_otic, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_otic($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva otic
          Fecha Actualiza: 2016-10-13
         */
        $p_rut_otic = $data['rut_otic'];
        $p_nombre_otic = $data['nombre_otic'];
        $p_nombre_corto_otic = $data['nombre_corto_otic'];
        $p_direccion_otic = $data['direccion_otic'];
        $p_descripcion_otic = $data['descripcion_otic'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_otic_insert(?, ?, ?, ?, ?, ?, ?);";

        $query = $this->db->query($sql, array($p_rut_otic, $p_nombre_otic, $p_nombre_corto_otic, $p_direccion_otic, $p_descripcion_otic, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_otic_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_otic = $data['id_otic'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_otic_select_by_id(?, ?, ?);";

        $query = $this->db->query($sql, array($id_otic, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_otic($data) {
        /*
          Ultimo usuario modifica: David De Filippi
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-12-02
         */

        $id_otic = $data['id_otic'];
        $p_rut_otic = $data['rut_otic'];
        $p_nombre_otic = $data['nombre_otic'];
        $p_nombre_corto_otic = $data['nombre_corto_otic'];
        $p_direccion_otic = $data['direccion_otic'];
        $p_descripcion_otic = $data['descripcion_otic'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];



        $sql = "CALL sp_otic_update(?, ?, ?, ?, ?, ?, ?, ?);";
        $query = $this->db->query($sql, array($id_otic, $p_rut_otic, $p_nombre_otic, $p_nombre_corto_otic, $p_direccion_otic, $p_descripcion_otic, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
