<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * CrearCicloEmpresa_model
 */
class Login_model extends MY_Model {

    private $tabla_mantenida = '';
    private $identificador_principal = '';

    // 2016-09-23  
    // verifica la existencia del usaurio
    function get_arr_user_login($data) {
        $user_name = $data['user_name'];
        $user_password = $data['user_password'];

        $sql = "CALL sp_g_val_user('" . $user_name . "','" . $user_password . "')";
        $query = $this->db->query($sql);

        $result = false;

        $array = $query->row_array();

        if ($array["id_user"] != NULL) {
            $result = $array;
        }
        return $result;
    }

}
