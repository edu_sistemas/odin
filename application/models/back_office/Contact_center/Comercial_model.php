<?php

class Comercial_model extends MY_Model {

    // SOLICITUD COMERCIAL //
    public function getComboFiltrosCbx($data) {

        $filtro = $data['filtro'];
        $user_id = $data['user_id'];
        $perfil_id = $data['perfil_id'];

        $sql = "CALL sp_cc_filtro_select_cbx(?,?,?)";

        $query = $this->db->query($sql, array($filtro, $user_id, $perfil_id));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function getTablaBusqueda($data) {

        $empresa = $data['empresa'];
        $ficha = $data['ficha'];
        $oc = $data['oc'];
        $modalidad = $data['modalidad'];
        $column = $data['column'];
        $dir = $data['dir'];


        $sql = "CALL sp_cc_resultados_alumnos_comercial_select(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($empresa, $ficha, $oc, $modalidad, $column, $dir));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    public function getTablaBusquedaPagging($data) {

        $empresa = $data['empresa'];
        $ficha = $data['ficha'];
        $oc = $data['oc'];
        $modalidad = $data['modalidad'];
        $start = $data['start'];
        $length = $data['length'];

        $sql = "CALL sp_cc_resultados_alumnos_comercial_select_pagging(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($empresa, $ficha, $oc, $modalidad, $start, $length));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function set_arr_generar_notificacion($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $selectAccion = $data['selectAccion'];
        $inputAlumno = $data['inputAlumno'];
        $inputFicha = $data['inputFicha'];
        $inputFecha = $data['inputFecha'];
        $textComentario = $data['textComentario'];


        $sql = " CALL sp_cc_generar_notificacion_insert(?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil, $selectAccion, $inputAlumno, $inputFicha, $inputFecha, $textComentario));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // FIN SOLICITUD COMERCIAL //
    // <editor-fold defaultstate="collapsed" desc="Funciones Avance Curso">

    public function get_avances_curso($data) {
        $id_empresa = $data["id_empresa"] == "" ? 0 : $data["id_empresa"];
        $id_curso = $data["id_curso"] == "" ? 0 : $data["id_curso"];
        $fecha_desde = $data["fecha_desde"] == "" ? 0 : $data["fecha_desde"];
        $fecha_hasta = $data["fecha_hasta"] == "" ? 0 : $data["fecha_hasta"];
        $num_ficha = $data["num_ficha"] == "" ? 0 : $data["num_ficha"];
        $num_oc = $data["num_oc"] == "" ? '' : $data["num_oc"];
        $holding = $data["holding"] == "" ? '' : $data["holding"];
        $id_ejec_comercial = $data["id_ejec_comercial"] == "" ? 0 : $data["id_ejec_comercial"];
        $ano_proceso = $data["ano_proceso"] == "" ? 0 : $data["ano_proceso"];

        $sql = "CALL sp_cc_avance_curso_select(?,?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_empresa, $id_curso, $fecha_desde, $fecha_hasta, $num_ficha, $num_oc, $holding, $ano_proceso, $id_ejec_comercial));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_cursos_avances_curso() {
        $sql = "CALL sp_cc_cursos_select_cbx()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_empresas_avances_curso($data) {
        $id_user = $data["id_user"];
        $id_perfil = $data["id_perfil"];

        $sql = "CALL sp_empresa_select_cbx(?,?)";
        $query = $this->db->query($sql, array($id_user, $id_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_ejecutivos_avances_curso() {
        $sql = "CALL sp_crm_reasignar_cuenta_listar_cbx()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_reporte_completo_avance_curso($data) {
        $p_id_empresa = $data["p_id_empresa"] == "" ? 0 : $data["p_id_empresa"];
        $p_id_curso = $data["p_id_curso"] == "" ? 0 : $data["p_id_curso"];
        $p_fecha_desde = $data["p_fecha_desde"];
        $p_fecha_hasta = $data["p_fecha_hasta"];
        $p_num_ficha = $data["p_num_ficha"] == "" ? 0 : $data["p_num_ficha"];
        $p_num_oc = $data["p_num_oc"];
        $p_holding = $data["p_holding"];
        $p_ano_proceso = $data["p_ano_proceso"] == "" ? 0 : $data["p_ano_proceso"];
        $p_id_ejec_comercial = $data["p_id_ejec_comercial"] == "" ? 0 : $data["p_id_ejec_comercial"];
        $p_cts = $data["p_cts"] == "true" ? 1 : 0;
        $p_correo = $data["p_correo"] == "true" ? 1 : 0;
        $p_costo = $data["p_costo"] == "true" ? 1 : 0;
        $p_curso = $data["p_curso"] == "true" ? 1 : 0;
        $p_cod_sence = $data["p_cod_sence"] == "true" ? 1 : 0;
        $p_dj = $data["p_dj"] == "true" ? 1 : 0;
        $p_empresa = $data["p_empresa"] == "true" ? 1 : 0;
        $p_evaluacion = $data["p_evaluacion"] == "true" ? 1 : 0;
        $p_ficha = $data["p_ficha"] == "true" ? 1 : 0;
        $p_fecha_inicio = $data["p_fecha_inicio"] == "true" ? 1 : 0;
        $p_fecha_termino = $data["p_fecha_termino"] == "true" ? 1 : 0;
        $p_fecha_cierre = $data["p_fecha_cierre"] == "true" ? 1 : 0;
        $p_fus = $data["p_fus"] == "true" ? 1 : 0;
        $p_fono = $data["p_fono"] == "true" ? 1 : 0;
        $p_chk_holding = $data["p_chk_holding"] == "true" ? 1 : 0;
        $p_hrs_curso = $data["p_hrs_curso"] == "true" ? 1 : 0;
        $p_id_sence = $data["p_id_sence"] == "true" ? 1 : 0;
        $p_indicador = $data["p_indicador"] == "true" ? 1 : 0;
        $p_nombre = $data["p_nombre"] == "true" ? 1 : 0;
        $p_oc = $data["p_oc"] == "true" ? 1 : 0;
        $p_primer_acceso = $data["p_primer_acceso"] == "true" ? 1 : 0;
        $p_rut = $data["p_rut"] == "true" ? 1 : 0;
        $p_registro_sence = $data["p_registro_sence"] == "true" ? 1 : 0;
        $p_sucursal = $data["p_sucursal"] == "true" ? 1 : 0;
        $p_situacion = $data["p_situacion"] == "true" ? 1 : 0;
        $p_seguimiento = $data["p_seguimiento"] == "true" ? 1 : 0;
        $p_tiempo_conexion = $data["p_tiempo_conexion"] == "true" ? 1 : 0;
        $p_tutor = $data["p_tutor"] == "true" ? 1 : 0;
        $p_ultimo_acceso = $data["p_ultimo_acceso"] == "true" ? 1 : 0;
        $p_version = $data["p_version"] == "true" ? 1 : 0;
        $p_prctje_avance_video = $data["p_prctje_avance_video"] == "true" ? 1 : 0;
        $p_prctje_eval_realizada = $data["p_prctje_eval_realizada"] == "true" ? 1 : 0;
        $p_prctje_franquicia = $data["p_prctje_franquicia"] == "true" ? 1 : 0;
        $p_prctje_registro_sence = $data["p_prctje_registro_sence"] == "true" ? 1 : 0;
        $p_prctje_tiempo_conexion = $data["p_prctje_tiempo_conexion"] == "true" ? 1 : 0;

        $sql = "CALL sp_cc_reporte_completo_avance_curso(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $p_id_empresa,
            $p_id_curso,
            $p_fecha_desde,
            $p_fecha_hasta,
            $p_num_ficha,
            $p_num_oc,
            $p_holding,
            $p_ano_proceso,
            $p_id_ejec_comercial,
            $p_cts,
            $p_correo,
            $p_costo,
            $p_curso,
            $p_cod_sence,
            $p_dj,
            $p_empresa,
            $p_evaluacion,
            $p_ficha,
            $p_fecha_inicio,
            $p_fecha_termino,
            $p_fecha_cierre,
            $p_fus,
            $p_fono,
            $p_chk_holding,
            $p_hrs_curso,
            $p_id_sence,
            $p_indicador,
            $p_nombre,
            $p_oc,
            $p_primer_acceso,
            $p_rut,
            $p_registro_sence,
            $p_sucursal,
            $p_situacion,
            $p_seguimiento,
            $p_tiempo_conexion,
            $p_tutor,
            $p_ultimo_acceso,
            $p_version,
            $p_prctje_avance_video,
            $p_prctje_eval_realizada,
            $p_prctje_franquicia,
            $p_prctje_registro_sence,
            $p_prctje_tiempo_conexion
        ));

        $result = $query->result_array();
        $fields = $query->list_fields();

        $query->next_result();
        $query->free_result();
        return array("result" => $result, "fields" => $fields);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Funciones Seguimientos Programados (TAREAS)">

    public function get_tareas_comercial_by_filtro($data) {

        $id_empresa = $data["id_empresa"] == "" ? 0 : $data["id_empresa"];
        $id_ejecutivo = $data["id_ejecutivo"] == "" ? 0 : $data["id_ejecutivo"];
        $num_ficha = $data["num_ficha"] == "" ? 0 : $data["num_ficha"];
        $tipo = $data["tipo"];


        $sql = "CALL sp_cc_tareas_select_comercial_filtro(?,?,?,?)";
        $query = $this->db->query($sql, array($tipo, $id_empresa, $id_ejecutivo, $num_ficha));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_tareas($tipo) {
        $sql = "CALL sp_cc_tareas_select(?)";
        $query = $this->db->query($sql, array($tipo));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }


        public function get_ficha_oc_cbx($data) {
        
        $p_parametro = $data['p_parametro'];
        $p_id_tutor = $data['p_id_tutor'];
        
        $sql = "CALL sp_cc_ficha_oc_tareas_tutor(?,?)";
        $query = $this->db->query($sql,array($p_parametro,$p_id_tutor));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // </editor-fold>
}

?>