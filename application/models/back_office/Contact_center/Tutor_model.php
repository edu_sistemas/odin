<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tutor_model extends MY_Model {

    public function CalculoBono($datos) {
        $p_parametro = $datos["p_parametro"];
        $p_id_tutor = $datos["p_id_tutor"];
        $p_periodo = $datos["p_periodo"];

        $sql = "CALL sp_cc_calculo_bonos(?,?,?)";
        $query = $this->db->query($sql, array($p_parametro, $p_id_tutor, $p_periodo));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    public function get_data_cbx($data) {


        $filtro = $data['filtro'];
        $user_id = $data['user_id'];
        $perfil_id = $data['perfil_id'];

        $sql = "CALL sp_cc_filtro_select_cbx(?,?,?)";

        $query = $this->db->query($sql, array($filtro, $user_id, $perfil_id));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    //<editor-fold defaultstate="collapsed" desc="Funciones para resultados tutor">
    public function get_empresas_resultados_tutor($data) {
        $id_user = $data["id_user"];
        $id_perfil = $data["id_perfil"];

        $sql = "CALL sp_empresa_select_cbx(?,?)";
        $query = $this->db->query($sql, array($id_user, $id_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_resultados_tutor($data) {
        $id_empresa = $data["id_empresa"] == "" ? 0 : $data["id_empresa"];
        $num_ficha = $data["num_ficha"] == "" ? 0 : $data["num_ficha"];
        $num_oc = $data["num_oc"] == "" ? 0 : $data["num_oc"];
        $fecha_cierre_desde = $data["fecha_cierre_desde"];
        $fecha_cierre_hasta = $data["fecha_cierre_hasta"];
        $numero_tabla = $data["numero_tabla"];
        $id_user = $data["id_user"];
        $id_perfil = $data["id_perfil"];

        $sql = "CALL sp_cc_resultados_select(?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_user, $id_perfil, $id_empresa, $num_ficha, $num_oc, $fecha_cierre_desde, $fecha_cierre_hasta, $numero_tabla));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Funciones para tareas">

    public function get_tarea_by_id($data) {
        $id_tarea = $data['id_tarea'];
        $oc = $data['oc'];
        $ficha = $data['ficha'];
        
        $sql = "CALL sp_cc_tareas_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_tarea, $oc, $ficha));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_empresas_tareas($data) {
        $id_user = $data["id_user"];
        $id_perfil = $data["id_perfil"];

        $sql = "CALL sp_empresa_select_cbx(?,?)";
        $query = $this->db->query($sql, array($id_user, $id_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_modalidad_cbx() {
        $sql = "CALL sp_cc_modalidad_ofimatica_select_cbx()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_costos_cbx() {
        $sql = "CALL sp_cc_costos_ofimatica_select_cbx()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_ficha_oc_cbx($data) {
        
        $p_parametro = $data['p_parametro'];
        $p_id_tutor = $data['p_id_tutor'];
        
        $sql = "CALL sp_cc_ficha_oc_tareas_tutor(?,?)";
        $query = $this->db->query($sql,array($p_parametro,$p_id_tutor));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_estadisticas_tareas($data) {
        $id_tutor = $data["id_tutor"];

        $sql = "CALL sp_cc_tareas_select_tutor_filtro_porcen(?)";
        $query = $this->db->query($sql, array($id_tutor));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_tareas($data) {
        $empresa = $data['empresa'];
        $modalidad = $data['modalidad'];
        $costo = $data['costo'];
        $ficha = $data['ficha'] == "" ? 0 : $data['ficha'];
        $oc = $data['oc'] == "" ? 0 : $data['oc'];
        $curso = $data['curso'];
        $tipo_tarea = $data['tipo_tarea'];
        $tutor = $data['tutor'];

        $sql = "CALL sp_cc_tareas_select_tutor_filtro(?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($tipo_tarea, $tutor, $empresa, $modalidad, $costo, $ficha, $oc, $curso));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function enviomail_get_plantillas() {
        $directorio = FCPATH . "/" . $this->config->item('path_dir_plantillas');
        $result = array_diff(scandir($directorio), array('.', '..'));
        return $result;
    }

    // </editor-fold>
}

?>