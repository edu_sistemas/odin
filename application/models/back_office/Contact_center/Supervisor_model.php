<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Supervisor_model extends MY_Model {


    // <editor-fold defaultstate="collapsed" desc="Funciones para asignacion de tutor">

    public function carga_inicial_detalle_ficha_select() {
        $sql = "CALL sp_cc_sup_detalle_ficha_select()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function carga_alumnos_by_ficha($id_ficha) {
        $sql = "CALL sp_cc_sup_get_alumnos_by_ficha(?)";
        $query = $this->db->query($sql, array($id_ficha));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function carga_tutores() {
        $sql = "CALL sp_cc_tutor_select()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    function asignar_tutor_aux($is_asignacion, $id_tutor, $id_ficha, $id_usuario_supervisor, $data_rut_oc, $data_id_asignacion){
        $sql = "CALL sp_cc_asignar_tutor_aux(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($is_asignacion, $id_tutor, $id_ficha, $id_usuario_supervisor, $data_rut_oc, $data_id_asignacion));
        return "CALL sp_cc_asignar_tutor_aux($is_asignacion, $id_tutor, $id_ficha, $id_usuario_supervisor, $data_rut_oc, $data_id_asignacion)";
    }

    function asignar_tutor($data, $rut_alumno, $id_orden_compra, $id_asignacion) {
        $id_tutor = $data["id_tutor"];
        $id_usuario_supervisor = $data["id_usuario_supervisor"];
        $id_ficha = $data["id_ficha"];

        $sql = "CALL sp_cc_asignar_tutor(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_tutor, $rut_alumno, $id_usuario_supervisor, $id_ficha, $id_orden_compra, $id_asignacion));

//        $sqlArray = [];
//        $sqlArray[] = "CALL sp_cc_asignar_tutor($id_tutor, $rut_alumno, $id_usuario_supervisor, $id_ficha, $id_orden_compra, $id_asignacion)";
//
//        return $sqlArray;
        
        return true;
    }

    public function set_clasif_ficha($data) {
        $id_ficha = $data["id_ficha"];
        $clas = $data["clas"];

        $sql = "CALL sp_cc_set_clasif_ficha(?,?)";
        $query = $this->db->query($sql, array($id_ficha, $clas));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();


        return $result[0]["filas_afectadas"];
    }

    public function filtrar_fichas($data) {
        $id_modalidad = $data["id_modalidad"];
        $fecha_fin = $data["fecha_fin"];
        $id_cc = $data["id_cc"];
        $ficha = $data["ficha"];
        $oc = $data["oc"];

        $sql = "CALL sp_cc_sup_filtrar_asignar_tutor(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_modalidad, $fecha_fin, $id_cc, $ficha, $oc));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function alumnos_asignados_tutor() {
        $sql = "CALL sp_cc_get_alumnos_asignados_tutor()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function alumnos_asignados_empresa() {
        $sql = "CALL sp_cc_get_alumnos_asignados_empresa()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_asignacion_by_ids($id_asignacion) {
//        $sql = "CALL sp_cc_get_asignacion_by_ids(?)";
//        $query = $this->db->query($sql, $id_asignacion);
//
//        $result = $query->result_array();
//
//        $query->next_result();
//        $query->free_result();
//        return $result;
        
        return "CALL sp_cc_get_asignacion_by_ids($id_asignacion)";
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Funciones para reasignacion de tutor">

    public function get_alumnos_reasignar() {
        $sql = "CALL sp_cc_get_alumnos_reasignar()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_empresas() {
        $sql = "CALL sp_empresa_select_cbx(0,0)";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_alumnos_reasignar_by_filtros($data) {

        $rut = $data["rut"];
        $ficha = $data["ficha"];
        $oc = $data["oc"];
        $empresa = $data["empresa"];

        $sql = "CALL sp_cc_get_alumnos_reasignar_filtro(?,?,?,?)";
        $query = $this->db->query($sql, array($rut, $ficha, $oc, $empresa));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Funciones para parametros variables">

    public function get_parametros_generales_otics() {
        $sql = "CALL sp_cc_parametros_otic_select()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function upd_parametros_variables_otic($data) {
        $id_otic = $data["id_otic"];
        $dj = $data["dj"];
        $conex = $data["conex"];

        $sql = "CALL sp_cc_parametros_generales_otic_update(?,?,?)";
        $query = $this->db->query($sql, array($id_otic, $dj, $conex));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_parametros_generales_bono() {
        $sql = "CALL sp_cc_parametros_generales_bono_select()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function insertar_grado_bono($data) {
        $grado = $data["grado"];
        $noBeca = $data["noBeca"];
        $beca = $data["beca"];


        $sql = "CALL sp_cc_parametros_generales_bono_insert(?,?,?)";
        $query = $this->db->query($sql, array($grado, $noBeca, $beca));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function upd_grado_bono($data) {
        $id_grado = $data["idgrado"];
        $grado = $data["grado"];
        $noBeca = $data["noBeca"];
        $beca = $data["beca"];


        $sql = "CALL sp_cc_parametros_generales_bono_update(?,?,?,?)";
        $query = $this->db->query($sql, array($id_grado, $grado, $noBeca, $beca));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_parametros_categoria() {
        $sql = "CALL sp_cc_parametros_generales_categoria_select()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function upd_parametro_categoria($data) {
        $id_parametro_categoria = $data["id_parametro_categoria"];
        $porque = $data["porque"];
        $script = $data["script"];
        $pauta = $data["pauta"];

        $sql = "CALL sp_cc_parametros_generales_categoria_update(?,?,?,?)";
        $query = $this->db->query($sql, array($id_parametro_categoria, $porque, $script, $pauta));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_parametros_generales_kpi() {
        $sql = "CALL sp_cc_parametros_generales_kpi_select()";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function upd_parametro_kpi($data) {
        $id_parametro_kpi = $data["id_parametro_kpi"];
        $objetivo = $data["objetivo"];

        $sql = "CALL sp_cc_parametros_generales_kpi_update(?,?)";
        $query = $this->db->query($sql, array($id_parametro_kpi, $objetivo));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Funciones para resultados supervisor">
    public function get_empresas_resultados_supervisor($data) {
        $id_user = $data["id_user"];
        $id_perfil = $data["id_perfil"];

        $sql = "CALL sp_empresa_select_cbx(?,?)";
        $query = $this->db->query($sql, array($id_user, $id_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function get_resultados_supervisor($data) {
        $id_empresa = $data["id_empresa"] == "" ? 0 : $data["id_empresa"];
        $num_ficha = $data["num_ficha"] == "" ? 0 : $data["num_ficha"];
        $num_oc = $data["num_oc"] == "" ? 0 : $data["num_oc"];
        $fecha_cierre_desde = $data["fecha_cierre_desde"];
        $fecha_cierre_hasta = $data["fecha_cierre_hasta"];
        $numero_tabla = $data["numero_tabla"];
        $id_user = $data["id_user"];
        $id_perfil = $data["id_perfil"];

        $sql = "CALL sp_cc_resultados_select(?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_user, $id_perfil, $id_empresa, $num_ficha, $num_oc, $fecha_cierre_desde, $fecha_cierre_hasta, $numero_tabla));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Funciones para tareas supervisor">

    public function get_tareas($tipo) {
        $sql = "CALL sp_cc_tareas_select(?)";
        $query = $this->db->query($sql, array($tipo));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function upd_tareas_cambio($data) {
        $tselect = $data["tselect"];
        $oselect = $data["oselect"];
        $treemplazo = $data["treemplazo"];
        $oreemplazo = $data["oreemplazo"];

        $sql = "CALL sp_cc_tarea_cambio(?,?,?,?)";
        $query = $this->db->query($sql, array($tselect,$oselect,$treemplazo,$oreemplazo));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // </editor-fold>

    public function CalculoBono($datos) {
        $p_parametro = $datos["p_parametro"];
        $p_id_tutor = $datos["p_id_tutor"];
        $p_periodo = $datos["p_periodo"];

        $sql = "CALL sp_cc_calculo_bonos(?,?,?)";
        $query = $this->db->query($sql, array($p_parametro, $p_id_tutor, $p_periodo));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    public function get_data_cbx($data) {


        $filtro = $data['filtro'];
        $user_id = $data['user_id'];
        $perfil_id = $data['perfil_id'];

        $sql = "CALL sp_cc_filtro_select_cbx(?,?,?)";

        $query = $this->db->query($sql, array($filtro, $user_id, $perfil_id));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Funciones para dashboard">
    
    public function get_tutores_dashboard() {
        $sql = "";
        $query = $this->db->query($sql);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    // </editor-fold>

}

?>