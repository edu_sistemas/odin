<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Comun_model extends MY_Model {

    // <editor-fold defaultstate="collapsed" desc="Funciones para el modulo EnvioMail">

    public function enviomail_get_fichas($idUser, $idPerfil) {
        $sql = " CALL sp_cc_tut_enviomail_fichas_select(?,?)";
        $query = $this->db->query($sql, array($idUser, $idPerfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function enviomail_get_alumnos($data) {
        $idFicha = $data["idFicha"];

        $sql = "CALL sp_alumnos_select_by_ficha(?,?,?)";
        $query = $this->db->query($sql, array($idFicha, 0, 0));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function enviomail_get_plantillas() {
        $directorio = FCPATH . "/" . $this->config->item('path_dir_plantillas');
        $result = array_diff(scandir($directorio), array('.', '..'));
        return $result;
    }

    public function enviomail_get_plantilla($nombre) {
        $directorio = FCPATH . "/" . $this->config->item('path_dir_plantillas');
        $html = file_get_contents($directorio . "/" . $nombre);
        return $html;
    }

    public function enviomail_get_contacto_alumnos($ids) {
        $sql = "CALL sp_cc_enviomail_get_mail_alumno(?)";
        $query = $this->db->query($sql, $ids);

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function enviomail_add_dejar_gestion($data) {
        $categoria1 = $data["categoria1"];
        $categoria2 = $data["categoria2"];
        $categoria3 = $data["categoria3"];
        $comentario = $data["comentario"];
        $noContactar = $data["noContactar"];
        $idAlumno = $data["idAlumno"];
        $idUser = $data["idUser"];


        $sql = "CALL sp_cc_dejar_gestion_envimail_insert(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($categoria1, $categoria2, $categoria3, $comentario, $noContactar, $idAlumno, $idUser));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // </editor-fold>
    // Gestion Alumno //

    public function Gestion_alumnos($datos) {
        $p_parametro = $datos['p_parametro'];

        $p_valor1 = $datos['p_valor1'];
        $p_valor2 = $datos['p_valor2'];
        $p_valor3 = $datos['p_valor3'];

        $sql = "CALL sp_cc_Gestion_alumnos(?,?,?,?)";

        $query = $this->db->query($sql, array($p_parametro, $p_valor1, $p_valor2, $p_valor3));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function GuardarSeguimiento($datos) {
        $id_usuario_tutor = $datos['id_usuario_tutor'];
        $id_tarea_id = $datos['id_tarea'];
        $tarea_nuevo = $datos['tarea_nuevo'];
        $id_categoria_origen = $datos['id_categoria_origen'];
        $id_categoria_motivo = $datos['id_categoria_motivo'];
        $id_categoria_respuesta = $datos['id_categoria_respuesta'];
        $comentario = $datos['comentario'];
        $telefono = $datos['telefono'];
        $rut_alumno = $datos['rut_alumno'];
        $id_seguimiento = $datos['id_seguimiento'];
        $accion = $datos['accion'];

        $sql = "CALL sp_cc_guardar_seguimiento(?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_usuario_tutor,
            $id_tarea_id, 
            $tarea_nuevo, 
            $id_categoria_origen,
            $id_categoria_motivo,
            $id_categoria_respuesta,
            $comentario,
            $telefono,
            $rut_alumno,
            $id_seguimiento,
            $accion));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }


    public function GuardarEmailAlumno($datos) {
        $rut_alumno = $datos['rut_alumno'];
        $email = $datos['email'];

        $sql = "CALL sp_cc_guardar_email_alumno(?,?)";

        $query = $this->db->query($sql, array($rut_alumno,$email));
        $result = $query->row();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function cargaCombocategoriaCbx($data) {

        $categoria = $data['categoria'];
        $valor = $data['valor'];
        $valor2 = $data['valor2'];


        $sql = "CALL sp_cc_categoria_select_cbx(?,?,?)";


        $query = $this->db->query($sql, array($categoria, $valor, $valor2));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // Fin Gestion Alumno //
//------------------------------------------------------------------//
    // DETALLE GRAFICOS //
    public function getDatosGraficos($data, $user_id) {

        $sql = "CALL sp_cc_carga_graficos_supervisor(?,?)";

        $query = $this->db->query($sql, array($data, $user_id));
        $result = $query->row();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function getComboFiltrosCbx($data) {

        $filtro = $data['filtro'];
        $user_id = $data['user_id'];
        $perfil_id = $data['perfil_id'];

        $sql = "CALL sp_cc_filtro_select_cbx(?,?,?)";

        $query = $this->db->query($sql, array($filtro, $user_id, $perfil_id));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function getDetalleAlumno() {

        $sql = " CALL sp_cc_detalle_alumno_listar()";

        $query = $this->db->query($sql, array());
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function getIndicadoresTutor($data) {

        $user_id = $this->session->userdata('id_user');
        $id_perfil = $this->session->userdata('id_perfil');
        $parametro = $data['parametro'];

        $sql = " CALL sp_cc_indicadores_tutor_listar(?,?,?)";

        $query = $this->db->query($sql, array($user_id, $id_perfil, $parametro));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function getIndicadoresEmpresa($data) {

        $parametro = $data['parametro'];

        $sql = " CALL sp_cc_indicadores_empresa_listar(?)";

        $query = $this->db->query($sql, array($parametro));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    // FIN DETALLE GRAFICO DECLARACIONES JURADAS //
    //Combobox Busqueda//

    public function Buscar_alumnos_vista($datos) {
        $parametro = $datos['parametro'];
        $tutor = $datos['tutor'];
        $perfil = $datos['perfil'];

        $sql = "CALL sp_cc_busqueda_alumno_vista(?,?,?)";
        $query = $this->db->query($sql, array($parametro, $tutor, $perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    //Fin Combobox Busqueda//
    //Buscar Alumno//

    public function BuscarAlumnos($datos) {
        $parametro = $datos['parametro'];
        $rut = $datos['rut'];
        $nombre = $datos['nombre'];
        $email = $datos['email'];
        $oc = $datos['oc'];
        $ficha = $datos['ficha'];
        $empresa = $datos['empresa'];


        $sql = "CALL sp_cc_busqueda_alumno(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($parametro, $rut, $nombre, $email, $oc, $ficha, $empresa));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    public function DatosDetalleAlumno($datos) {
        $p_parametro = $datos['p_parametro'];
        $p_valor1 = $datos['p_valor1'];
        $p_valor2 = $datos['p_valor2'];
        $p_valor3 = $datos['p_valor3'];

        $sql = "CALL sp_cc_datos_detalle_alumno(?,?,?,?)";
        $query = $this->db->query($sql, array($p_parametro, $p_valor1, $p_valor2, $p_valor3));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    //Fin Buscar Alumno//
//Solicitud Alumno
    public function set_arr_generar_solicitud($data) {

        //$user_id         = $data['user_id']; 
        //$user_perfil     = $data['user_perfil']; 
        $rutAlumno = $data['rutAlumno'];
        $fechaSolicitud = $data['fechaSolicitud'];
        $horaSolicitud = $data['horaSolicitud'];
        $comentarioSol = $data['comentarioSol'];


        $sql = " CALL sp_cc_generar_solicitud_insert(?,?,?,?)";

        $query = $this->db->query($sql, array($rutAlumno, $fechaSolicitud, $horaSolicitud, $comentarioSol));
        //$result = $query->affected_rows();
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

//Fin solicitud alumno

    public function selectSolicitudAlumno($data) {
        //$user_id         = $data['user_id']; 
        //$id_perfil       = $data['user_perfil']; 
        //$user_id = $this->session->userdata('user_id');
        //$id_perfil = $this->session->userdata('user_perfil');
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_cc_solicitud_alumnos_select(?,?);";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    public function get_detalle_graficos($data) {
        $id_grafico = $data["id_grafico"];
        $id_user = $data["id_user"];
        $id_holding = $data["id_holding"] == "" ? 0 : $data["id_holding"];
        $id_empresa = $data["id_empresa"] == "" ? 0 : $data["id_empresa"];
        $num_ficha = $data["num_ficha"] == "" ? 0 : $data["num_ficha"];
        $num_oc = $data["num_oc"] == "" ? 0 : $data["num_oc"];
        $id_curso = $data["id_curso"];
        $fecha_cierre = $data["fecha_cierre"];
        
        $this->load->library('MySQLMultiResult','mysqlmultiresult');
        $sql = "CALL sp_cc_carga_graficos_dashboard_detalle(".$id_grafico.",".$id_user.",".$id_holding.",".$id_empresa.",".$num_ficha.",".$num_oc.",'".$id_curso."','".$fecha_cierre."')";
        $result = $this->mysqlmultiresult->GetMultiResults($sql);
        return $result;
    }

}

?>