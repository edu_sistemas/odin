<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2016-09-23 [--] <-->
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Relator_Model
 */
class Ver_Model extends MY_Model {

    function get_arr_actividades($data) {


        $num_ficha = $data['num_ficha'];
        $empresa = $data['empresa'];

        $fecha_inicio = $data['fecha_inicio'];
        $fecha_termino = $data['fecha_termino'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_actividades_vigente_select(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array($num_ficha, $empresa, $fecha_inicio, $fecha_termino, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_act_vig($data) {
        $fecha_reporte = $data['fecha_reporte'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_sala_act_vigentes_dpto_tec(?,?,?);";

        $query = $this->db->query($sql, array($fecha_reporte,$user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_act_vig_diaria($data) {

        $fecha_reporte = $data['fecha_reporte'];
        $externos = $data['externos'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_sala_act_vigentes_dia_dpto_tec(?,?,?,?);";
        $query = $this->db->query($sql, array($fecha_reporte,$externos, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

        function get_arr_act_vig_diaria_reserva($data) {

        $fecha_reporte = $data['fecha_reporte'];
        $externosReserva = $data['externosReserva'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_sala_act_vigentes_dia_reserva_dpto_tec(?,?,?,?);";
        $query = $this->db->query($sql, array($fecha_reporte,$externosReserva, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

}
