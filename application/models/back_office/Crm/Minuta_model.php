<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 
 * Fecha creacion:  2017-01-20 [Luis Jarpa] <lajrpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Sala_model
 */
class Minuta_model extends MY_Model {

    

	    function get_arr_listar_minutas_origen($data) {
        /*
          lista los docentes
          Descripcion : 
          Fecha Actualiza: 
         */
          $id_usuario = $data;

          $sql = " CALL sp_crm_minuta_listar(?)";
          $query = $this->db->query($sql, array($id_usuario));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }   



	  	    function get_arr_listar_minutas_filtrado($data) {

		  $id_usuario = $data['id_usuario'];
			
          $sql = " CALL sp_crm_minuta_listar_filtrado(?)";
          $query = $this->db->query($sql, array($id_usuario));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }


          function get_arr_eliminar_minuta($data) {

          $id = $data['id'];
          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
      
          $sql = " CALL sp_crm_minuta_eliminar(?,?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil,$id));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }      





      function set_arr_guardar_minuta($data) {


          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $asunto_minuta = $data['asunto_minuta'];
          $fecha_minuta = $data['fecha_minuta'];
          $mensaje_minuta = $data['mensaje_minuta'];

          $sql = " CALL sp_crm_agrega_minuta_insert(?,?,?,?,?);";

          $query = $this->db->query($sql, array($user_id, $user_perfil,$asunto_minuta,$fecha_minuta, $mensaje_minuta));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }










}


