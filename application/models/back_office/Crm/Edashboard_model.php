<?php

/**
 * edashboard_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-08-02 [Jimmy Espinoza] <jimmy.espinoza@vmica.com>
 * Fecha creacion:  2017-08-02 [Jimmy Espinoza] <jimmy.espinoza@vmica.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   edashboard_model
 */
class edashboard_model extends MY_Model {

     function get_arr_listar_ejecutivos_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];

        $sql = " CALL sp_crm_ejecutivos_dashboard_select_cbx(?,?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil,$ejecutivo));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

/*
     function get_arr_consultar_ranking_venta($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];
        $desde = $data['desde'];
        $hasta = $data['hasta'];

        $sql = "CALL sp_crm_ranking_ventas_edashboard(?,?,?,?,?);";
        $query = $this->db->query($sql, array($ejecutivo,$desde,$hasta,$user_id, $user_perfil));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    */

    function get_arr_consultar_comparativa_venta($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];
        $anio = $data['anio'];

        $sql = "CALL sp_crm_comparativa_ventas_edashboard(?,?,?,?);";
        $query = $this->db->query($sql, array($ejecutivo,$anio,$user_id, $user_perfil));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_filtro_eventos_kpi($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];
        $fecha = $data['fecha'];

        $sql = "CALL sp_crm_eventos_kpi(?,?,?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil, $ejecutivo, $fecha));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_consultar_distribucion_cliente($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];

        $sql = "CALL sp_crm_distribucion_cliente_edashboard(?,?,?);";
        $query = $this->db->query($sql, array($ejecutivo,$user_id, $user_perfil));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

//*************************
    function get_arr_consultar_distribucion_gestion($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];

        $sql = "CALL sp_crm_distribucion_gestion_edashboard(?,?,?);";
        $query = $this->db->query($sql, array($ejecutivo,$user_id, $user_perfil));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }

//************************        

    function get_arr_consultar_historico_gestion($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];

        $sql = "CALL sp_crm_historico_gestion_edashboard(?,?,?);";
        $query = $this->db->query($sql, array($ejecutivo,$user_id, $user_perfil));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }


// ************ F.Q ************
/*
        function get_arr_listar_metas_global() {


          $sql = " CALL sp_crm_metas_listar()";
          $query = $this->db->query($sql, array());

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }       
*/
       function get_arr_datos_filtrados_cliente_venta($data) {

          $user_id = $data['user_id'];
          $ejecutivo = $data['ejecutivo'];
          $user_perfil = $data['user_perfil'];
          $filtradoVentas = $data['filtradoVentas'];

          $sql = " CALL sp_crm_ejec_clasificacion_venta_select(?,?,?,?)";
          $query = $this->db->query($sql, array($user_id,$ejecutivo,$user_perfil, $filtradoVentas));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }  


        function get_arr_datos_filtrados_cliente_gestion($data) {
            $ejecutivo = $data['ejecutivo'];
            $filtradoGestion = $data['filtradoGestion'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];
          

          $sql = " CALL sp_crm_ejec_clasificacion_por_gestion_select(?,?,?,?)";
          $query = $this->db->query($sql, array($ejecutivo,$filtradoGestion,$user_id,$user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }  

        function get_arr_datos_filtrados_tabla_compromisos($data) {

          $user_id = $data['user_id'];
          $ejecutivo = $data['ejecutivo'];
          $user_perfil = $data['user_perfil'];
          $empresa = $data['empresa'];

          $sql = " CALL sp_crm_ejec_compromiso_select(?,?,?,?)";
          $query = $this->db->query($sql, array($user_id,$ejecutivo,$user_perfil,$empresa));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }    

        function get_arr_datos_filtrados_tabla_metas($data) {

          $user_id = $data['user_id'];
          $ejecutivo = $data['ejecutivo'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_ventas_dashboard(?,?,?,?)";
          $query = $this->db->query($sql, array($user_id,$ejecutivo,$user_perfil,$fecha));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      } 


        function get_arr_e_metas_globales($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];
          $ejecutivo = $data['ejecutivo'];

          $sql = " CALL sp_crm_e_metas_globales(?,?,?,?)";
          $query = $this->db->query($sql, array($user_id,$user_perfil,$fecha,$ejecutivo));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }            
      
    function get_arr_datos_metas_excel($data) {
        $ejecutivo = $data['ejecutivo'];
        $fecha = $data['fecha'];  
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_crm_ventas_dashboard_excel(?,?,?,?)";
        $query = $this->db->query($sql, array($ejecutivo, $fecha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    } 

    function get_arr_datos_compromisos_excel($data) {
        $ejecutivo = $data['ejecutivo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_crm_ejec_compromiso_select_excel(?,?,?)";
        $query = $this->db->query($sql, array($ejecutivo, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    } 

    function get_arr_datos_ventas_empresas_excel($data) {
        $datos = array();
        $titulo="";
        
        $ejecutivo = $data['ejecutivo'];
        $filtradoV = $data['filtradoV'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $titulo = "";

        if($filtradoV==1){
            $titulo ="Empresas con Ventas";
            
        }elseif ($filtradoV==2) {
            $titulo = "Empresas Con Potenciales Ventas";
            
        }elseif ($filtradoV==3) {
             $titulo = "Empresas Sin Ventas";
            
        }
        
        $sql = " CALL sp_crm_ejec_clasificacion_venta_select_excel(?,?,?,?)";
        $query = $this->db->query($sql, array($ejecutivo, $filtradoV, $user_id, $user_perfil));
        $result = $query->result_array();
        
        $query->next_result();
        $query->free_result();
       array_push($result,$titulo);
      
        return $result;
    } 

      function get_arr_datos_gestiones_empresas_excel($data) {
        $datos = array();
        $titulo="";
        $ejecutivo = $data['ejecutivo'];
        $filtradoG = $data['filtradoG'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
          $titulo = "";
        if($filtradoG==1){
            $titulo ="Empresas Con Gestión";
        }elseif ($filtradoG==2) {
            $titulo = "Empresas Sin Gestión";
        }
       
        
        $sql = " CALL sp_crm_ejec_clasificacion_por_gestion_select_excel(?,?,?,?)";
        $query = $this->db->query($sql, array($ejecutivo, $filtradoG, $user_id, $user_perfil));
        $result = $query->result_array();
       
        $query->next_result();
        $query->free_result();
        array_push($result,$titulo);
         
        return $result;
    } 

    

   

// ************ F.Q ************

}
