<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

/**
 * CrearCicloEmpresa_model
 */
class reasigna_cuenta_model extends MY_Model {
	
 function get_arr_listar_datos_ejecutivos_origen() {
        /*
          lista los docentes
          Descripcion : 
          Fecha Actualiza: 
         */
          $sql = " CALL sp_crm_reasignar_cuenta_listar()";
          $query = $this->db->query($sql, array());

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }    


        function get_arr_listar_datos_ejecutivos_filtrado($data) {

          $id_ejecutivo = $data['id_ejecutivo'];

          $sql = " CALL sp_crm_reasignar_cuenta_listar_filtrado(?)";
          $query = $this->db->query($sql, array($id_ejecutivo));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }    

        function get_arr_listar_gestiones($data) {

         $user_id = $data['user_id'];
         $user_perfil = $data['user_perfil'];
         $id_ejecutivo = $data['id_ejecutivo'];
         

         $sql = " CALL sp_crm_historial_gestion_select(?,?,?)";
         $query = $this->db->query($sql, array($user_id,$user_perfil,$id_ejecutivo));

         $result = $query->result_array();

         $query->next_result();
         $query->free_result();
         return $result;
       }       

       function get_arr_listar_ejecutivo_destino_cbx($data) {
         $user_id = $data['user_id'];
         $user_perfil = $data['user_perfil'];
         /* revisar si se requiere variables de sesion */
         $sql = " CALL sp_crm_reasignar_cuenta_listar_cbx ()";
         $query = $this->db->query($sql);
         $result = $query->result_array();
         $query->next_result();
         $query->free_result();
         return $result;
       } 

       function get_arr_reasignar_cuentas($data) {
         $user_id = $data['user_id'];
         $user_perfil = $data['user_perfil'];
         $id_nuevo_ejecutivo = $data['id_nuevo_ejecutivo'];
         $id_empresa = $data['id_empresa'];	   
         /* revisar si se requiere variables de sesion */
         $sql = " CALL sp_crm_reasignar_cuenta_update (?,?,?,?)";
         $query = $this->db->query($sql, array($user_id, $user_perfil, $id_nuevo_ejecutivo, $id_empresa));
         $result = $query->result_array();
         $query->next_result();
         $query->free_result();
         return $result;
       } 


       // ------------------ EXPORTAR ------------------//

           function get_arr_bitacora_reasignar_download($data) {

          $ano = $data['ano'];
          //$user_perfil = $data['user_perfil'];
          $sql = " CALL sp_crm_reasigna_cuenta_bitacora(0)";
          $query = $this->db->query($sql, array($ano/*, $user_perfil*/));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }  


     }