<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 
 * Fecha creacion:  
 */
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

/**
 *   Sala_model
 */
class Cuenta_model extends MY_Model {

  function get_arr_listar_clientes($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $ejecutivo = $data['ejecutivo'];


          $sql = " CALL sp_crm_cliente_select(?,?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil, $ejecutivo));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result(); 
          return $result;
        }

/*
    function get_arr_listar_clientes_filtro($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_crm_cliente_select_filtro(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result(); 
        return $result;
    }
*/    


    function get_arr_listar_historial_gestion($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $ejecutivo = $data['ejecutivo'];
          $sql = " CALL sp_crm_historial_gestion_select(?,?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil, $ejecutivo));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        } 


        function get_arr_listar_gestiones_download($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $ejecutivo = $data['ejecutivo'];
          $sql = " CALL sp_crm_historial_gestion_excel(?,?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil, $ejecutivo));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }

        function get_arr_listar_dominios_download($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = " CALL sp_crm_historial_dominio_select(?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }       

        function eliminar_cuenta($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

        //$user_perfil = $data['user_perfil'];
          $id_empresa = $data['id_empresa'];
          $user_id = $data['user_id'];
          $sql = " CALL sp_crm_agregar_cuenta_delete(?,?)";
          $query = $this->db->query($sql, array($id_empresa, $user_id));

          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;
        }  	

        function eliminar_gestion($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */

        //$user_perfil = $data['user_perfil'];
          $id_gestion = $data['id_gestion'];
          $user_id = $data['user_id'];
          $sql = " CALL sp_crm_acciones_cliente_delete(?,?)";
          $query = $this->db->query($sql, array($id_gestion, $user_id));

          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;
        } 

        function get_arr_listar_clientes_ejecutivos($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = " CALL sp_crm_clientes_con_ejecutivo()";
          $query = $this->db->query($sql, array($user_id, $user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }   


        function get_arr_listar_ejecutivos_cbx($data) {
          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $ejecutivo = $data['ejecutivo'];

          $sql = " CALL sp_crm_ejecutivos_dashboard_select_cbx(?,?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil,$ejecutivo));
          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }     

        function get_arr_existe_empresa($rut) {
          $sql = " CALL sp_crm_existe_empresa(?)";
          $query = $this->db->query($sql, array($rut));
          $result = $query->row();

          $query->next_result();
          $query->free_result();
          return $result;
        }   

        function get_arr_auxiliar_empresa($datos) 
        {
          $accion=$datos['accion'];
          $valor=$datos['valor'];

          $sql = " CALL sp_crm_auxiliares_empresa(?,?)";
          $query = $this->db->query($sql, array($accion,$valor));
          $result = $query->row();

          $query->next_result();
          $query->free_result();
          return $result;
        }  


         function set_arr_set_empresa($datos) 
        {
          $id=$datos['id'];
          $rut=$datos['rut'];
          $razon=$datos['razon'];
          $giro=$datos['giro'];
          $fantasia=$datos['fantasia'];
          $direccion=$datos['direccion'];
          $comuna=$datos['comuna'];
          $descripcion=$datos['descripcion'];
          $holding=$datos['holding'];
          $otic=$datos['otic'];
          $rubro=$datos['rubro'];
          $ejecutivo=$datos['ejecutivo'];

          $sql = " CALL sp_crm_empresa_set(?,?,?,?,?,?,?,?,?,?,?,?)";
          $query = $this->db->query($sql, array($id,$rut,$razon,$giro,$fantasia,$direccion,$comuna,$descripcion,$holding,$otic,$rubro,$ejecutivo));
          $result = $query->row();

          $query->next_result();
          $query->free_result();
          return $result;
        }    


      }
