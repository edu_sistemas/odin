<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 
 * Fecha creacion:  2017-01-20 [Luis Jarpa] <lajrpa@edutecno.com>
 */
if (!defined('BASEPATH'))
  exit('No direct script access allowed');

/**
 *   Sala_model
 */
class Editar_gestion_model extends MY_Model {

  function get_arr_get_datos_cliente($data) {
        /*
          lista los docentes
          Descripcion : 
          Fecha Actualiza: 
         */


          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $id_cliente = $data['id_cliente'];

          $sql = " CALL sp_crm_contacto_select(?,?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil,$id_cliente));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }     

        function get_arr_get_datos_compromiso_cbx($accion) {
        /*
          lista los docentes
          Descripcion : 
          Fecha Actualiza: 
         */

          $sql = " CALL sp_crm_compromiso_cbx_select(?)";
          $query = $this->db->query($sql,array($accion));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }	


        function get_arr_get_datos_contacto_origen_cbx($data) {

          $id_empresa = $data['id_empresa'];


          $sql = " CALL sp_crm_contacto_origen_cbx_select('".$id_empresa."')";
          $query = $this->db->query($sql);

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        } 	

        function get_arr_get_tipos_accion_cbx($data) {
        /*
          lista los docentes
          Descripcion : 
          Fecha Actualiza: 
         */
          $accion = $data['accion'];
          $idfase = $data['idfase'];

          $sql = " CALL sp_crm_tipos_accion_cbx_select(?,?)";
          $query = $this->db->query($sql, array($accion,$idfase));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
        }     


        function set_arr_agregar_accion_cliente($data) {


          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $id_gestion = $data['id_gestion'];
          $empresa_id = $data['empresa_id'];
          $id_accion = $data['id_accion_gestion'];
          $tipo_accion = $data['tipo_accion'];
          $subtipo_accion = $data['subtipo_accion'];
          $fecha_accion = $data['fecha_accion'];
          $asunto_accion = $data['asunto_accion'];
          $comentario_accion = $data['comentario_accion'];
          $compromiso_accion = $data['compromiso_accion'];
          $fecha_compromiso_accion = $data['fecha_compromiso_accion'];
          $contacto_origen = $data['contacto_origen'];  
          $id_producto = $data['id_producto'];  
          $fase = $data['fase'];
          $modalidad = $data['modalidad']; 
          $comentario_otros = $data['comentario_otros'];           

          $sql = " CALL sp_crm_acciones_cliente_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

          $query = $this->db->query($sql, array($user_id, $user_perfil,$id_gestion,$empresa_id,$id_accion, $tipo_accion, $subtipo_accion, $fecha_accion, $asunto_accion,$comentario_accion,$compromiso_accion,$fecha_compromiso_accion, $contacto_origen,$id_producto,$fase,$modalidad,$comentario_otros));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;

        }

        function get_fecha_cierre_compromiso($data){
          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $id_compromiso = $data['id_compromiso'];
          /* revisar si se requiere variables de sesion */
          $sql = " SELECT date_format(f_crm_calculo_fecha_compromiso(".$id_compromiso."),'%d-%m-%Y') as Fecha";
          $query = $this->db->query($sql);
          $result = $query->result_array();
        //$query->next_result();
          $query->free_result();
          return $result;
        }
/*
      function set_arr_agregar_accion_llamada($data) {


          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $empresa_id = $data['empresa_id'];
          $id_accion = $data['id_accion'];
          $tipo_accion = $data['tipo_accion'];
          $fecha_accion = $data['fecha_accion'];
          $asunto_accion = $data['asunto_accion'];
          $comentario_accion = $data['comentario_accion'];
          $compromiso_accion = $data['compromiso_accion'];
          $fecha_compromiso_accion = $data['fecha_compromiso_accion'];
          $contacto_origen = $data['contacto_origen'];    			


          $sql = " CALL sp_crm_accion_llamada_insert(?,?,?,?,?,?,?,?,?,?,?);";

          $query = $this->db->query($sql, array($user_id, $user_perfil,$empresa_id,$id_accion, $tipo_accion, $fecha_accion, $asunto_accion,$comentario_accion,$compromiso_accion,$fecha_compromiso_accion, $contacto_origen));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }

      function set_arr_agregar_accion_correo($data) {


          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $empresa_id = $data['empresa_id'];
          $id_accion = $data['id_accion'];
          $tipo_accion = $data['tipo_accion'];
          $fecha_accion = $data['fecha_accion'];
          $asunto_accion = $data['asunto_accion'];
          $comentario_accion = $data['comentario_accion'];
          $compromiso_accion = $data['compromiso_accion'];
          $fecha_compromiso_accion = $data['fecha_compromiso_accion'];
          $contacto_origen = $data['contacto_origen'];              


          $sql = " CALL sp_crm_accion_llamada_insert(?,?,?,?,?,?,?,?,?,?,?);";

          $query = $this->db->query($sql, array($user_id, $user_perfil,$empresa_id,$id_accion, $tipo_accion, $fecha_accion, $asunto_accion,$comentario_accion,$compromiso_accion,$fecha_compromiso_accion, $contacto_origen));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }


      function set_arr_agregar_accion_reunion($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $reunion_seleccione_fase_cbx = $data['reunion_seleccione_fase_cbx'];
          $reunion_tipo_reunion_cbx = $data['reunion_tipo_reunion_cbx'];
          $reunion_fecha_reunion = $data['reunion_fecha_reunion'];
          $reunion_asunto = $data['reunion_asunto'];
          $reunion_comentario = $data['reunion_comentario'];
          $reunion_compromiso_cbx = $data['reunion_compromiso_cbx'];
          $reunion_contacto_origen_cbx = $data['reunion_contacto_origen_cbx'];	
          $reunion_fecha_compromiso = $data['reunion_fecha_compromiso'];


          $sql = " CALL sp_crm_accion_reunion_insert(?,?,?,?,?,?,?,?,?,?);";

          $query = $this->db->query($sql, array($user_id, $user_perfil, $reunion_seleccione_fase_cbx, $reunion_tipo_reunion_cbx, $reunion_fecha_reunion, $reunion_asunto, $reunion_comentario, $reunion_compromiso_cbx, $reunion_contacto_origen_cbx, $reunion_fecha_compromiso));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }	


      function set_arr_agregar_accion_pedido($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $id_cliente_pedido = $data['id_cliente_pedido'];
          $pedido_fecha_reunion = $data['pedido_fecha_reunion'];
          $pedido_modalidad_cbx = $data['pedido_modalidad_cbx'];
          $pedido_tipo_curso_cbx = $data['pedido_tipo_curso_cbx'];
          $pedido_cant_asistentes = $data['pedido_cant_asistentes'];
          $pedido_comentario = $data['pedido_comentario'];

          $sql = " CALL sp_crm_accion_pedido_insert(?,?,?,?,?,?,?,?);";

          $query = $this->db->query($sql, array($user_id, $user_perfil, $id_cliente_pedido, $pedido_fecha_reunion, $pedido_modalidad_cbx, $pedido_tipo_curso_cbx, $pedido_cant_asistentes, $pedido_comentario));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }		

*/

      function get_arr_listar_fases_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_fase_reunion_select_cbx () ";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
      }

      function get_arr_listar_tipos_reunion_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $idfase = $data['idfase'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_detalle_reunion_select_cbx (".$idfase.") ";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
      }	


    function get_arr_listar_modalidades_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_modalidad_curso_select_cbx() ";
                  
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_productos_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_modalidad = $data['id_modalidad'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_productos_select_cbx (".$id_modalidad.") ";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }


//------------------------PRUEBA--------------------------
  function get_arr_get_tipos_correo_interno_cbx($data) {
    $user_id = $data['user_id'];
    $user_perfil = $data['user_perfil'];
        //$idfase = $data['idfase'];
    /* revisar si se requiere variables de sesion */
    $sql = " CALL sp_crm_tipo_correo_interno_cbx () ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $query->next_result();
    $query->free_result();
    return $result;
  }	
//------------------------FIN PRUEBA-----------------------

// NECESARIO

  function get_arr_listar_datos_gestion($data) {
   $user_id = $data['user_id'];
   $user_perfil = $data['user_perfil'];
   $id = $data['id'];

   $sql = " CALL sp_crm_acciones_cliente_select(?,?,?)";
   $query = $this->db->query($sql, array($user_id, $user_perfil, $id));

   $result = $query->result_array();

   $query->next_result();
   $query->free_result();
   return $result;
 }


}


