<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 
 * Fecha creacion:  
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Sala_model
 */
class Notificaciones_model extends MY_Model {

//---Para NOTIFICACIONES_V.---// 

	function get_arr_listar_notificaciones($data){
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		//$id_gestion = $data['id_gestion'];
		$sql = " CALL sp_crm_notificaciones_select(?,?,?)";
		$query = $this->db->query($sql, array($user_id, $user_perfil,1));

		$result = $query->result_array();

		$query->next_result();
		$query->free_result(); 
		return $result;
	}

	function get_arr_listar_notificaciones_res($data){
		$user_id = $data['user_id'];
		$user_perfil = $data['user_perfil'];
		//$id_gestion = $data['id_gestion'];
		$sql = " CALL sp_crm_notificaciones_select(?,?,?)";
		$query = $this->db->query($sql, array($user_id, $user_perfil,0));

		$result = $query->result_array();

		$query->next_result();
		$query->free_result(); 
		return $result;
	}
//---Fin para NOTIFICACIONES_V.---// 

//---Marcar notificaciones una por una.---// 

	function set_notificacion($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */
        
		$id_notificacion = $data['id_notificacion'];		
        $user_id = $data['user_id'];
        $sql = " CALL sp_crm_notificaciones_set(?,?,?)";
        $query = $this->db->query($sql, array($user_id, $id_notificacion,1));

          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;
    }	

//---Fin marcar notificaciones una por una.---//


//---Marcar 10  últimas como leídas.---//

    function set_notificacion_todas($data){
        $user_id = $data['user_id'];
        $id_notificacion = $data['id_notificacion'];        
        $sql = " CALL sp_crm_notificaciones_set(?,?,?)";
        $query = $this->db->query($sql, array($user_id, $id_notificacion,0));

        $row_count = $query->num_rows();
        $result = false;
        if ($row_count > 0) {
          $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }
//---Fin marcar 10  últimas como leídas.---//

}