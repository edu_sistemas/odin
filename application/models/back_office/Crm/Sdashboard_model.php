<?php

/**
 * edashboard_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-08-02 [Jimmy Espinoza] <jimmy.espinoza@vmica.com>
 * Fecha creacion:  2017-08-02 [Jimmy Espinoza] <jimmy.espinoza@vmica.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   edashboard_model
 */
class sdashboard_model extends MY_Model {

        function get_arr_metas_globales($data) {

          $user_id = $data['user_id'];
          //$ejecutivo = $data['ejecutivo'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_metas_globales(?,?,?)";
          $query = $this->db->query($sql, array($user_id,$user_perfil,$fecha));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }  

        function get_arr_datos_list_grupos_ejecutivos($data) {

          $user_id = $data['user_id'];
          $ejecutivo = $data['ejecutivo'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_ventas_grupo_sdashboard(?,?,?)";
          $query = $this->db->query($sql, array($user_id,$user_perfil,$fecha));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }    

        function get_arr_datos_select_grupos_ejecutivos($data) {

          $user_id = $data['user_id'];
          $id_grupo = $data['id_grupo'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_ventas_ejecutivos_sdashboard(?,?,?,?,?)";  
          $query = $this->db->query($sql, array($user_id,$user_perfil,$user_perfil,$id_grupo,$fecha));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }        


        function get_arr_rubros($data) {

          $user_id = $data['user_id'];
          //$ejecutivo = $data['ejecutivo'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_ventas_rubro_sdashboard(?,?,?)";
          $query = $this->db->query($sql, array($user_id,$user_perfil,$fecha));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }    

        function get_arr_linea_negocio($data) {

          $user_id = $data['user_id'];
          //$ejecutivo = $data['ejecutivo'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_ventas_linea_sdashboard(?,?,?)";
          $query = $this->db->query($sql, array($user_id,$user_perfil,$fecha));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }          
              
      function get_arr_consultar_venta_acumulada($data) {

          /*
            Ultimo usuario  modifica:   Luis Jarpa
            Descripcion : Lista el total de venta mensual
            Fecha Actualiza: 2016-12-26
           */

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = "CALL sp_supervisor_venta_anual_acomulada(?,?);";
          $query = $this->db->query($sql, array($user_id, $user_perfil));
          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }

      function get_arr_datos_list_grupos_ejecutivos_excel($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
         $fecha = $data['fecha'];

        $sql = " CALL sp_crm_ventas_grupo_sdashboard_excel(?,?,?)";
        $query = $this->db->query($sql, array($user_id,$user_perfil,$fecha));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_datos_clasificacion_ejecutivos_excel($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $id_grupo = $data['id_grupo'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_ventas_ejecutivos_sdashboard_excel(?,?,?,?,?)";  
          $query = $this->db->query($sql, array($user_id, $user_perfil, $user_perfil,$id_grupo,$fecha));

          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }  

        function get_arr_datos_ventas_rubro_excel($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_ventas_rubro_sdashboard_excel(?,?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil,$fecha));

          $result = $query->result_array();
         

          $query->next_result();
          $query->free_result();
          return $result;
      }  

    function get_arr_linea_negocio_excel($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $fecha = $data['fecha'];

          $sql = " CALL sp_crm_ventas_linea_sdashboard_excel(?,?,?)";
          $query = $this->db->query($sql, array($user_id,$user_perfil,$fecha));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }    

       function get_arr_filtro_eventos_kpi($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];
        $fecha = $data['fecha'];

        $sql = "CALL sp_crm_eventos_kpi(?,?,?,?);";
        $query = $this->db->query($sql, array($user_id, $user_perfil, $ejecutivo, $fecha));
        $result = $query->result_array();
        $this->db->last_query();
        $query->next_result();
        $query->free_result();
        return $result;
    }


// ************ F.Q ************

}
