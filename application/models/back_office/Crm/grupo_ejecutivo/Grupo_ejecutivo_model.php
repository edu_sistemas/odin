<?php

/**
 * grupo_ejecutivo_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   grupo_ejecutivo_model
 */
class grupo_ejecutivo_model extends MY_Model {

    function get_arr_listar_grupos($data) {
        /*
          Ultimo usuario  modifica: Jimmy Espinoza
          Descripcion : Lstado de Grupo Ejecutivo
          Fecha Actualiza: 2017-07-24
         */

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = " CALL sp_crm_grupo_ejecutivo_select(?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil));
          $row_count = $query->num_rows();
          $result = false;

          if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_grupos_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_crm_grupo_ejecutivo_select_cbx('" . $user_id . "','" . $user_perfil . "')";
        //$sql = " CALL sp_crm_grupo_ejecutivo_select_cbx(?,?)";
        $query = $this->db->query($sql);
        //$query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    
    function set_arr_agregar_grupo($data) {
        /*
          Ultimo usuario  modifica: Jimmy Espinoza
          Descripcion : Añade un nuevo grupo ejecutivo
          Fecha Actualiza: 2017-07-24
         */

          $nombre = $data['nombre'];
          $descripcion = $data['descripcion'];
          $responsable = $data['responsable'];
          
          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];

          $sql = " CALL sp_crm_grupo_ejecutivo_insert(?,?,?,?,?)";
          $query = $this->db->query($sql, array($nombre, $descripcion, $responsable, $user_id, $user_perfil));
          $row_count = $query->num_rows();
          $result = false;

          if ($row_count > 0) {
            $result = $query->result_array();
        }
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_grupo_by_id($data) {

        /*
          Ultimo usuario  modifica: Jimmy Espinoza
          Descripcion : Recupeera grupo desde BD
          Fecha Actualiza: 2017-07-24
         */
          $id = $data['id'];
          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];

          $sql = " CALL sp_crm_grupo_ejecutivo_select_by_id(?,?,?);";

          $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_grupo($datos) {
        /*
          Ultimo usuario modifica: Jimmy Espinoza
          Descripcion : Edita grupo ejecutivo desde BD
          Fecha Actualiza: 2017-07-24
         */
          $id = $datos['id'];
          $nombre = $datos['nombre'];
          $descripcion = $datos['descripcion'];
          $responsable = $datos['responsable'];

          $user_id = $datos['user_id'];
          $user_perfil = $datos['user_perfil'];

          $sql = "CALL sp_crm_grupo_ejecutivo_update(?,?,?,?,?,?);";

          $query = $this->db->query($sql, array($id, $nombre, $descripcion, $responsable, $user_id, $user_perfil));
          $row_count = $query->num_rows();
          $result = false;

          if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
