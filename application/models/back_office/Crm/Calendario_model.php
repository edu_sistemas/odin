<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 
 * Fecha creacion:  
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Sala_model
 */
class Calendario_model extends MY_Model {

    function get_arr_listar_datos_calendarios_acciones($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $ejecutivo = $data['ejecutivo'];

          $sql = " CALL sp_crm_calendario_accion_select(?,?,?)";
          $query = $this->db->query($sql, array($user_id,$user_perfil,$ejecutivo));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }

     function get_arr_listar_ejecutivos_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $ejecutivo = $data['ejecutivo'];

        $sql = " CALL sp_crm_ejecutivos_dashboard_select_cbx(?,?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil,$ejecutivo));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }       

}
