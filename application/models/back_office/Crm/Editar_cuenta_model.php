<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 
 * Fecha creacion:  
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 *   Editar Cuenta
 */
class Editar_cuenta_model extends MY_Model {


	function get_arr_datos_by_id($data) {
        /*
          Ultimo usuario  modifica: Jimmy Espinoza
          Descripcion : Recupeera empresa desde BD
          Fecha Actualiza: 2016-11-08
         */


          $id = $data['id_empresa'];
          $id_sugerido =$data['id_sugerido'];
          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];



          $sql = " CALL sp_crm_agregar_cuenta_select(?,?,?,?);";

          $query = $this->db->query($sql, array($id,$id_sugerido, $user_id, $user_perfil));
          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;

        }




        function set_arr_editar_cuenta($data) {
          /* 0  */  $user_id                                = $data['user_id']; 
          /* 0  */  $user_perfil                            = $data['user_perfil']; 
          /* 0  */  $Var_id_empresa                         = $data['Var_id_empresa']; 
          /*	1	*/	$Var_rut_empresa 						= $data['Var_rut_empresa'];													
          /*	2	*/	$Var_razon_social_empresa               = $data['Var_razon_social_empresa'];
          /*	3	*/	$Var_giro_empresa                       = $data['Var_giro_empresa'];
          /*	4	*/	$Var_nombre_fantasia                    = $data['Var_nombre_fantasia'];
          /*	5	*/	$Var_direccion_empresa                  = $data['Var_direccion_empresa'];
          /*	6	*/	$Var_id_comuna                          = $data['Var_id_comuna'];
          /*	7	*/	$Var_id_ciudad_empresa                  = $data['Var_id_ciudad_empresa'];
          /*	8	*/	$Var_descripcion_empresa                = $data['Var_descripcion_empresa'];
          /*	9	*/	$Var_id_holding                         = $data['Var_id_holding'];
          /*	10		$Var_fecha_registro                     = $data['Var_fecha_registro'];		*/
          /*	11	*/	$Var_telefono_temporal                  = $data['Var_telefono_temporal'];
          /*	12	*/	$Var_direccion_factura                  = $data['Var_direccion_factura'];
          /*	13	*/	$Var_id_comuna_facturacion              = $data['Var_id_comuna_facturacion'];
          /*	14	*/	$Var_id_ciudad_facturacion              = $data['Var_id_ciudad_facturacion'];
          /*	15	*/	$Var_direccion_despacho                 = $data['Var_direccion_despacho'];
          /*	16	*/	$Var_mail_sii                           = $data['Var_mail_sii'];
          /*	17	*/	$Var_nombre_contacto_cobranza           = $data['Var_nombre_contacto_cobranza'];
          /*	18	*/	$Var_mail_contacto_cobranza             = $data['Var_mail_contacto_cobranza'];
          /*	19	*/	$Var_telefono_contacto_cobranza         = $data['Var_telefono_contacto_cobranza'];
          /*	20	*/	$Var_requiere_orden_compra              = $data['Var_requiere_orden_compra'];
          /*	21	*/	$Var_requiere_hess                      = $data['Var_requiere_hess'];
          /*	22	*/	$Var_requiere_numero_contrato           = $data['Var_requiere_numero_contrato'];
          /*	23	*/	$Var_requiere_ota                       = $data['Var_requiere_ota'];
          /*	24	*/	$Var_observaciones_glosa_facturacion    = $data['Var_observaciones_glosa_facturacion'];
          /*	25	*/	$Var_url_logo_empresa                   = $data['Var_url_logo_empresa'];
          /*	26	*/	$Var_estado_empresa                     = $data['Var_estado_empresa'];
          /*	27	*/	$Var_crm_clasificacion_cuenta           = $data['Var_crm_clasificacion_cuenta'];
          /*	28	*/	$Var_crm_pais_cuenta                    = $data['Var_crm_pais_cuenta'];
          /*	29	*/	$Var_crm_otic_cuenta                    = $data['Var_crm_otic_cuenta'];
          /*	30	*/	$Var_crm_industria                      = $data['Var_crm_industria'];
          /*	31	*/	$Var_crm_descripcion_cuenta             = $data['Var_crm_descripcion_cuenta'];
          /*	32	*/	$Var_crm_oc_interna                     = $data['Var_crm_oc_interna'];
          /*	33	*/	$Var_crm_otro                           = $data['Var_crm_otro'];
          /*	34	*/	$Var_crm_sii                            = $data['Var_crm_sii'];
          /*	35	*/	$Var_crm_mano_timbrada                  = $data['Var_crm_mano_timbrada'];
          /*	36	*/	$Var_crm_chilexpress                    = $data['Var_crm_chilexpress'];
          /*	37	*/	$Var_crm_direccion_entrega_diploma      = $data['Var_crm_direccion_entrega_diploma'];
          /*	38	*/	$Var_crm_contacto_persona_diploma       = $data['Var_crm_contacto_persona_diploma'];
          /*	39	*/	$Var_crm_telefono_oficina_diploma       = $data['Var_crm_telefono_oficina_diploma'];
          /*	40	*/	$Var_crm_telefono_movil_diploma         = $data['Var_crm_telefono_movil_diploma'];
          /*	41	*/	$Var_crm_correo_diploma                 = $data['Var_crm_correo_diploma'];	
		  /*	42	*/	$Var_crm_id_ejecutivo                   = $data['Var_crm_id_ejecutivo'];
		  /*	43	*/	$Var_crm_tamano_empresa                 = $data['Var_crm_tamano_empresa'];
		  /*	44	*/	$Var_crm_rubro                 		    = $data['Var_crm_rubro'];
		  /*	45	*/	$Var_crm_subrubro                 		= $data['Var_crm_subrubro'];
		  
		  

          $sql = " CALL sp_crm_agregar_cuenta_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

          $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil,
            $Var_id_empresa,
            $Var_rut_empresa, 													
            $Var_razon_social_empresa,              
            $Var_giro_empresa,                       
            $Var_nombre_fantasia,                    
            $Var_direccion_empresa,                  
            $Var_id_comuna,                          
            $Var_id_ciudad_empresa,                  
            $Var_descripcion_empresa,                
            $Var_id_holding,                                            
            $Var_telefono_temporal,                  
            $Var_direccion_factura,                  
            $Var_id_comuna_facturacion,              
            $Var_id_ciudad_facturacion,              
            $Var_direccion_despacho,                 
            $Var_mail_sii,                           
            $Var_nombre_contacto_cobranza,          
            $Var_mail_contacto_cobranza,             
            $Var_telefono_contacto_cobranza,         
            $Var_requiere_orden_compra,              
            $Var_requiere_hess,                      
            $Var_requiere_numero_contrato,           
            $Var_requiere_ota,                       
            $Var_observaciones_glosa_facturacion,   
            $Var_url_logo_empresa,                   
            $Var_estado_empresa,                     
            $Var_crm_clasificacion_cuenta,           
            $Var_crm_pais_cuenta,                    
            $Var_crm_otic_cuenta,                    
            $Var_crm_industria,                      
            $Var_crm_descripcion_cuenta,             
            $Var_crm_oc_interna,                     
            $Var_crm_otro,                           
            $Var_crm_sii,                            
            $Var_crm_mano_timbrada,                 
            $Var_crm_chilexpress,                    
            $Var_crm_direccion_entrega_diploma,      
            $Var_crm_contacto_persona_diploma,       
            $Var_crm_telefono_oficina_diploma,       
            $Var_crm_telefono_movil_diploma,         
            $Var_crm_correo_diploma,
			$Var_crm_id_ejecutivo,
			$Var_crm_tamano_empresa,
			$Var_crm_rubro,
			$Var_crm_subrubro		
            ));  

          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
        }     



        /********************************************************/

        function get_arr_contacto_empresa($data) {

          $id_empresa  = $data['id_empresa'];
          $id_contacto = $data['id_contacto'];

          $sql = " CALL sp_crm_agregar_contacto_select(?,?);";

          $query = $this->db->query($sql, array($id_empresa, $id_contacto));
          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;

        }

/*
        function get_arr_carga_modal($data) {

          $id_empresa = $data['id_empresa'];
          $id_contacto = $data['id_contacto'];

          $sql = " CALL sp_crm_agregar_contacto_select(?,?);";

          $query = $this->db->query($sql, array($id_empresa, $id_contacto));
          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;

        }
*/

        function set_arr_editar_contacto($data)
        {

          $user_id =$data['user_id'];
          $user_perfil =$data['user_perfil'];
          $Var_id_contacto_empresa=$data['Var_id_contacto_empresa'];
          $Var_id_empresa=$data['Var_id_empresa'];
          $Var_id_tipo_contacto_empresa=$data['Var_id_tipo_contacto_empresa'];
          $Var_nombre_contacto=$data['Var_nombre_contacto'];
          $Var_correo_contacto=$data['Var_correo_contacto'];
          $Var_telefono_contacto=$data['Var_telefono_contacto'];
          $Var_estado_contacto=$data['Var_estado_contacto'];
          $Var_id_nivel_contacto=$data['Var_id_nivel_contacto'];
          $Var_crm_cargo_contacto=$data['Var_crm_cargo_contacto'];
          $Var_crm_departamento_contacto=$data['Var_crm_departamento_contacto'];
          $Var_crm_cantidad_hijos_contacto=$data['Var_crm_cantidad_hijos_contacto'];
          $Var_crm_nivel_contacto=$data['Var_crm_nivel_contacto'];
          $Var_crm_telefono_oficina_contacto=$data['Var_crm_telefono_oficina_contacto'];
          $Var_crm_telefono_movil_contacto=$data['Var_crm_telefono_movil_contacto'];
          $Var_crm_fecha_cumpleanos_contacto=$data['Var_crm_fecha_cumpleanos_contacto'];
          $Var_crm_fax_contacto=$data['Var_crm_fax_contacto'];
          $Var_crm_descripcion_contacto=$data['Var_crm_descripcion_contacto'];

          $sql = " CALL sp_crm_agregar_contacto_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

          $query = $this->db->query($sql, array(
            $user_id ,
            $user_perfil ,
            $Var_id_contacto_empresa,
            $Var_id_empresa,
            $Var_id_tipo_contacto_empresa,
            $Var_nombre_contacto,
            $Var_correo_contacto,
            $Var_telefono_contacto,
            $Var_estado_contacto,
            $Var_id_nivel_contacto,
            $Var_crm_cargo_contacto,
            $Var_crm_departamento_contacto,
            $Var_crm_cantidad_hijos_contacto,
            $Var_crm_nivel_contacto,
            $Var_crm_telefono_oficina_contacto,
            $Var_crm_telefono_movil_contacto,
            $Var_crm_fecha_cumpleanos_contacto,
            $Var_crm_fax_contacto,
            $Var_crm_descripcion_contacto
            ));

          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;
        }


        function set_arr_agregar_contacto($data)
        {

          $user_id =$data['user_id'];
          $user_perfil =$data['user_perfil'];
          $Var_id_contacto_empresa=$data['Var_id_contacto_empresa'];
          $Var_id_empresa=$data['Var_id_empresa'];
          $Var_id_tipo_contacto_empresa=$data['Var_id_tipo_contacto_empresa'];
          $Var_nombre_contacto=$data['Var_nombre_contacto'];
          $Var_correo_contacto=$data['Var_correo_contacto'];
          $Var_telefono_contacto=$data['Var_telefono_contacto'];
          $Var_estado_contacto=$data['Var_estado_contacto'];
          $Var_id_nivel_contacto=$data['Var_id_nivel_contacto'];
          $Var_crm_cargo_contacto=$data['Var_crm_cargo_contacto'];
          $Var_crm_departamento_contacto=$data['Var_crm_departamento_contacto'];
          $Var_crm_cantidad_hijos_contacto=$data['Var_crm_cantidad_hijos_contacto'];
          $Var_crm_nivel_contacto=$data['Var_crm_nivel_contacto'];
          $Var_crm_telefono_oficina_contacto=$data['Var_crm_telefono_oficina_contacto'];
          $Var_crm_telefono_movil_contacto=$data['Var_crm_telefono_movil_contacto'];
          $Var_crm_fecha_cumpleanos_contacto=$data['Var_crm_fecha_cumpleanos_contacto'];
          $Var_crm_fax_contacto=$data['Var_crm_fax_contacto'];
          $Var_crm_descripcion_contacto=$data['Var_crm_descripcion_contacto'];

          $sql = " CALL sp_crm_agregar_contacto_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

          $query = $this->db->query($sql, array(
            $user_id ,
            $user_perfil ,
            $Var_id_contacto_empresa,
            $Var_id_empresa,
            $Var_id_tipo_contacto_empresa,
            $Var_nombre_contacto,
            $Var_correo_contacto,
            $Var_telefono_contacto,
            $Var_estado_contacto,
            $Var_id_nivel_contacto,
            $Var_crm_cargo_contacto,
            $Var_crm_departamento_contacto,
            $Var_crm_cantidad_hijos_contacto,
            $Var_crm_nivel_contacto,
            $Var_crm_telefono_oficina_contacto,
            $Var_crm_telefono_movil_contacto,
            $Var_crm_fecha_cumpleanos_contacto,
            $Var_crm_fax_contacto,
            $Var_crm_descripcion_contacto
            ));

          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;
        }


        function set_arr_eliminar_contacto($data)
        {
          $user_id =$data['user_id'];
          $user_perfil =$data['user_perfil'];
          $Var_id_contacto_empresa=$data['Var_id_contacto_empresa'];

          $sql = " CALL sp_crm_agregar_contacto_delete(?,?,?);";
          $query = $this->db->query($sql, array($user_id,$user_perfil,$Var_id_contacto_empresa));

          $row_count = $query->num_rows();
          $result = false;
          if ($row_count > 0) {
            $result = $query->result_array();
          }

          $query->next_result();
          $query->free_result();
          return $result;

        }
		
//------------------------PRUEBA HOLDING--------------------------
    function get_arr_combo_holding_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_holding_listar_cbx ()";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }	
//------------------------FIN PRUEBA HOLDING-----------------------

//------------------------PRUEBA CLASIFICACIO CUENTA--------------------------
    function get_arr_combo_clasificacion_cuenta_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_clasificacion_cuenta_listar_cbx ()";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }	
//------------------------FIN PRUEBA CLASIFICACIO CUENTA-----------------------    

//------------------------PRUEBA EJECUTIVO ASIGNADO--------------------------
    function get_arr_ejecutivo_asignado_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_usuario_listar_cbx ()";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }	
//------------------------FIN PRUEBA EJECUTIVO ASIGNADO--------------------------    

//------------------------PRUEBA RUBRO EMPRESA--------------------------
    function get_arr_rubro_empresa_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_rubro_empresa_cbx ()";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }	
//------------------------FIN PRUEBA RUBRO EMPRESA-------------------------- 

//------------------------PRUEBA SUBRUBRO EMPRESA--------------------------
    function get_arr_subrubro_empresa_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_subrubro_empresa_cbx ()";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }	
//------------------------FIN PRUEBA SUBRUBRO EMPRESA--------------------------  

//------------------------PRUEBA TAMAÑO EMPRESA--------------------------
    function get_arr_tamano_empresa_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_tamano_empresa_cbx()";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }	
//------------------------FIN PRUEBA TAMAÑO EMPRESA--------------------------  

//------------------------PRUEBA PAIS CUENTA--------------------------
    function get_arr_pais_cuenta_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_crm_pais_cuenta_cbx()";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }	

        function get_arr_combo_comuna_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        /* revisar si se requiere variables de sesion */
        $sql = " CALL sp_comuna_select_cbx (?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    } 
//------------------------FIN PRUEBA PAIS CUENTA--------------------------  

}
