<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 
 * Fecha creacion:  2017-01-20 [Luis Jarpa] <lajrpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Sala_model
 */
class Meta_model extends MY_Model {

        
    function get_arr_subir_meta($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2016-10-17
         */
        $ANO = $data['ANO'];
        $Mes = $data['Mes'];
        $Monto = $data['Monto'];
        $Empresa = $data['Empresa'];
        $Producto = $data['Producto'];
        $Ejecutivo = $data['Ejecutivo'];
		$Estado = $data['Estado'];

        $sql = " CALL sp_meta_insert(?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
        	$ANO,
            $Mes,
            $Monto,
            $Empresa,
            $Producto,
            $Ejecutivo,
            $Estado
        ));
      
        return ($query)?1:0;
        
    }


    public function borarano($ano){

    	$this->db->query("DELETE FROM tbl_metas_anuales WHERE ANO = $ano;");

    }

}

