<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 
 * Fecha creacion:  
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Sala_model
 */
class CuentaSupervisor_model extends MY_Model {

      function get_arr_usuario_by_ejecutivo_comercial($data) {

        /*
         * Ultimo usuario modifica: Luis Jarpa
         * Descripcion : Selecciona todos los usauruios tipo ejecutivo comercial
         * Fecha Actualiza: 2017-01-12
         *
         */
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_usuario_select_tipo_eje_comercial_cbx(?,?);";

        $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_clientes($data) {
        $id_ejecutivo = $data['id_ejecutivo'];
        $id_empresa = $data['id_empresa'];
        $fecha = $data['fecha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_supervisor_cliente_select(?,?,?,?,?)";
        
        $query = $this->db->query($sql, array(
            $id_ejecutivo,
            $id_empresa, 
            $fecha, 
            $user_id, 
            $user_perfil
        ));
         $result = $query->result_array();
        $query->next_result();
        $query->free_result(); 
        
        return $result;
    }      
	
	function get_arr_listar_historial_gestion($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_supervisor_historial_gestion_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        
        $result = $query->result_array();
        
        $query->next_result();
        $query->free_result();
              
        return $result;
    } 


    function get_arr_listar_gestiones_download($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = " CALL sp_supervisor_historial_gestion_excel(?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
      }

    function get_arr_listar_dominios_download($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = " CALL sp_supervisor_historial_dominio_select(?,?)";
          $query = $this->db->query($sql, array($user_id, $user_perfil));

          $result = $query->result_array();

          $query->next_result();
          $query->free_result();
          return $result;
    }       

	function get_arr_listar_clientes_ejecutivos($data) {

          $user_id = $data['user_id'];
          $user_perfil = $data['user_perfil'];
          $sql = " CALL sp_supervisor_clientes_con_ejecutivo()";
          $query = $this->db->query($sql, array($user_id, $user_perfil));

          $result = $query->result_array();
          $query->next_result();
          $query->free_result();
          return $result;
      }  

      
      
      function get_arr_datos_gestiones_ejecutivos_excel($data) {
            
            $id_ejecutivo = $data['id_ejecutivo'];
            $id_empresa =  $data['id_empresa'];
            $fecha = $data['fecha'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];
            
            $sql = " CALL sp_supervisor_cliente_select_excel(?,?,?,?,?)";
        
            $query = $this->db->query($sql, array(
                $id_ejecutivo,
                $id_empresa,
                $fecha, 
                $user_id, 
                $user_perfil
            ));
            $result = $query->result_array();
            $query->next_result();
            $query->free_result(); 
        
            return $result;
      }  
    

}
