<?php

/**
 * Descuento_Producto_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2017-12-26 [Jessica Roa] <jroa@edutecno.com>
 * Fecha creacion:	2017-12-26 [Jessica Roa] <jroa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *  Descuento_Producto_model
 */
class Descuento_Producto_model extends MY_Model {

    function get_arr_listar_descuento_producto($data) {
        /*
          lista los descuentos productos
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2017-12-26
         */
        $sub_linea_negocio_filtro = $data['sub_linea_negocio_filtro'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_descuento_producto_select(?,?,?)";
        $query = $this->db->query($sql, array($sub_linea_negocio_filtro, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_linea_negocio($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_linea_negocio_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_sub_linea_negocio($data) {
       // $linea_negocio_filtro = $data['linea_negocio_filtro'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_linea = $data['id_linea'];
        
        $sql = " CALL sp_sub_linea_negocio_select_cbx(?,?,?)";
        $query = $this->db->query($sql, array($id_linea,$user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function post_arr_editar_descuento($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $id_descuentos_producto = $data['id_descuentos_producto'];
        $sub_linea_negocio = $data['sub_linea_negocio'];
        $valor_descuento = $data['valor_descuento'];
        $fecha_inicio = $data['fecha_inicio'];
        
        $sql = " CALL sp_editar_descuento_producto(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_descuentos_producto,$sub_linea_negocio,$valor_descuento,$fecha_inicio,$user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
}
