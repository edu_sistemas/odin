<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Empresa_contacto_model
 */
class Informe_Encuestas_model extends MY_Model {

    function get_arr_listar_fichas($data){

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_listar_encuesta_satisfaccion(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_resultados_encuesta_satisfaccion_por_ficha($data){
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_encuesta_satisfaccion_select_resultador_por_ficha(?,?,?)";

        $query = $this->db->query($sql,array($num_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function comprobarFicha($data){
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_by_num_ficha(?,?,?)";

        $query = $this->db->query($sql,array($num_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_resultados_encuesta_satisfaccion_por_ficha_elearning($data){
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_encuesta_satisfaccion_select_resultador_por_ficha_elearning(?,?,?)";

        $query = $this->db->query($sql,array($num_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_encuesta_satisfaccion($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_encuesta_satisfaccion($data){
            $id_ficha = $data['id_ficha'];
            $id_relator = $data['id_relator'];
            $respuesta1 = $data['respuesta1'];
            $respuesta2 = $data['respuesta2'];
            $respuesta3 = $data['respuesta3'];
            $respuesta4 = $data['respuesta4'];
            $respuesta5 = $data['respuesta5'];
            $respuesta6 = $data['respuesta6'];
            $respuesta7 = $data['respuesta7'];
            $respuesta8 = $data['respuesta8'];
            $respuesta9 = $data['respuesta9'];
            $respuesta10 = $data['respuesta10'];
            $respuesta11 = $data['respuesta11'];
            $respuesta12 = $data['respuesta12'];
            $respuesta13 = $data['respuesta13'];
            $respuesta14 = $data['respuesta14'];
            $respuesta15 = $data['respuesta15'];
            $respuesta16 = $data['respuesta16'];
            $respuesta17 = $data['respuesta17'];
            $respuesta18 = $data['respuesta18'];
            $respuesta19 = $data['respuesta19'];
            $respuesta20 = $data['respuesta20'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

            $sql = "CALL sp_encuesta_satisfaccion_insertar(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            $query = $this->db->query($sql,array(+
                $id_ficha,
                $id_relator,
                $respuesta1,
                $respuesta2,
                $respuesta3,
                $respuesta4,
                $respuesta5,
                $respuesta6,
                $respuesta7,
                $respuesta8,
                $respuesta9,
                $respuesta10,
                $respuesta11,
                $respuesta12,
                $respuesta13,
                $respuesta14,
                $respuesta15,
                $respuesta16,
                $respuesta17,
                $respuesta18,
                $respuesta19,
                $respuesta20,
                $user_id,
                $user_perfil
            ));
    
            $result = $query->result_array();
            $query->next_result();
            $query->free_result();
    
            return $result;
    }
    

    function get_resultados_encuesta_satisfaccion_por_fecha_de_cierre($data){
        $start = $data['start'];
        $end = $data['end'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_encuesta_satisfaccion_select_fichas_por_fecha_de_cierre(?,?,?,?)";

        $query = $this->db->query($sql,array($start,$end,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_relatores_by_ficha($data){
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_select_by_ficha_id_cbx(?,?,?)";

        $query = $this->db->query($sql,array($id_ficha,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
    function get_encuesta_by_id($data){
        $id_encuesta = $data['id_encuesta'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion_by_id(?,?,?)";

        $query = $this->db->query($sql,array($id_encuesta,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_relatores_cbx($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_select_cbx(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
/* Revisar perfil docente y perfil tutor, este ultimo no se encuentra en bd*/
      function get_tutores_cbx($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_tutor_select_cbx(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_edutecno_cbx($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_edutecno_select_cbx_encuestas(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_empresa_cbx($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_select_cbx(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
    function get_ejecutivo_cbx($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_usuario_select_tipo_eje_comercial_cbx(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
    function get_sede_cbx($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_sede_select_cbx(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_resultados_encuesta_satisfaccion_por_relator($data){
        $id_relator = $data['id_relator'];
        $mes = $data['mes'];
        $year = $data['year'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion_by_id_relator(?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_relator,$mes, $year, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
    function get_resultados_anual_encuesta_satisfaccion($data){
        $year = $data['year'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion_by_year(?,?,?)";

        $query = $this->db->query($sql,array($year,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
    
    function get_resultados_mensual_encuesta_satisfaccion($data){
        $mes = $data['mes'];
        $year = $data['year'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion_by_mes(?,?,?,?)";

        $query = $this->db->query($sql,array($mes,$year,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_resultados_encuesta_satisfaccion_por_edutecno($data){
        $id_edutecno = $data['id_edutecno'];
        $mes = $data['mes'];
        $year = $data['year'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion_by_id_edutecno(?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_edutecno,$mes, $year, $user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_resultados_encuesta_satisfaccion_personalizada($data){
        $id_relator = $data['id_relator'];
        $id_empresa = $data['id_empresa'];
        $codigo_curso = $data['codigo_curso'];
        $id_ejecutivo = $data['id_ejecutivo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_encuesta_satisfaccion_personalizada(?,?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_relator,$id_empresa,$codigo_curso,$id_ejecutivo,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function getCodeSenceCBX($data) {
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_code_sence_select_ficha_cbx('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function getSalaCBX($data) {
        $id_sede = $data['id_sede'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_sala_select_by_ficha_cbx('" . $id_sede . "','" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_resultados_cumplimiento_tecnico($data){
        $year = $data['year'];
        $mes = $data['mes'];
        $id_empresa = $data['id_empresa'];
        $id_edutecno = $data['id_edutecno'];
        $codigo_curso = $data['codigo_curso'];
        $id_sede = $data['id_sede'];
        $id_sala = $data['id_sala'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_cumplimiento_tecnico_select_resultado(?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql,array(
            $year,
            $mes,
            $id_empresa,
            $id_edutecno,
            $codigo_curso,
            $id_sede,
            $id_sala,
            $num_ficha ,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
}
