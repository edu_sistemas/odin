<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Ficha_documento_model
 */
class Ficha_documento_model extends MY_Model {

    function get_arr_ficha_docs($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_select_docs(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_ficha_doc($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Agrega un nuevo contacto  al empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_ficha = $data['id_ficha'];
        $nombre_contacto = $data['nombre_contacto'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_contact(?,?,?,?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $nombre_contacto, $correo_contacto,
            $telefono_contacto, $user_id, $user_perfil)
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_update_ficha_doc($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_empresa = $data['id_contacto_empresa'];
        $nombre_contacto = $data['nombre_contacto'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_update_contact(?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_contacto_empresa,
            $nombre_contacto,
            $correo_contacto,
            $telefono_contacto,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_delete_ficha_doc($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al empresa
          Fecha Actualiza: 2016-11-28
         */

        $id_documento = $data['id_documento'];
        $nombre_documento = $data['nombre_documento'];
        $fecha_registro = $data['fecha_registro'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_delete_doc(?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_documento,
            $nombre_documento,
            $fecha_registro,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_document_ficha($data) {

        $id_ficha = $data['id_ficha'];
        $nombre_documento = $data['nombre_documento'];
        $fichero_subido = $data['ruta_documento'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_document_by_ficha(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $nombre_documento, $fichero_subido, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    

    function get_document_ficha($data) {

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_select_document_by_ficha(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
