<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Fecha creacion:  2016-09-23 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *  Relator_contacto_model
 */
class Relator_contacto_model extends MY_Model {

    function get_arr_relator_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista los los contactos que puede tener el relator
          Fecha Actualiza: 2016-11-28
         */

        $id_relator = $data['id_relator'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_select_contact(?,?,?)";

        $query = $this->db->query($sql,array($id_relator,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_relator_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Agrega un nuevo contacto  al relator
          Fecha Actualiza: 2016-11-28
         */

        $id_relator = $data['id_relator'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_insert_contact(?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_relator,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_status_relator_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : descativa  un  contacto  al relator
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_relator = $data['id_contacto_relator'];
        $estado = $data['estado']; 
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_desactivar_contact(?,?,?,?)";

        $query = $this->db->query($sql,array($id_contacto_relator,$estado,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_update_relator_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al relator
          Fecha Actualiza: 2016-11-28
         */

        $id_contacto_relator = $data['id_contacto_relator'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_update_contact(?,?,?,?,?)";

        $query = $this->db->query($sql,array($id_contacto_relator,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_delete_relator_contact($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Actualzia un  contacto  al relator
          Fecha Actualiza: 2016-11-28
         */
        
        $id_relator = $data['id_relator'];
        $id_contacto_relator = $data['id_contacto_relator'];
        $correo_contacto = $data['correo_contacto'];
        $telefono_contacto = $data['telefono_contacto'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_relator_delete_contact(?,?,?,?,?,?);";

        $query = $this->db->query($sql,array($id_contacto_relator,$id_relator,$correo_contacto,$telefono_contacto,$user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

}
