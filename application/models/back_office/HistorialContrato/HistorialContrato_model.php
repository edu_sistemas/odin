<?php

/**
 * Docente_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  0000-00-00 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-12-14 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *  Docente_model
 */
class HistorialContrato_model extends MY_Model {

    function get_arr_listar_contratos($data) {
        /*
          lista los contratos
          Descripcion : Lista los contratos
          Fecha Actualiza: 2016-11-07         */
       
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_contrato_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	function set_arr_estado_contratos($data) {
        /*
          lista los contratos
          Descripcion : Lista los contratos
          Fecha Actualiza: 2016-11-07         */
       	$id =  $data['id'];
		$estado =  $data['estado'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_contrato_estado(?,?,?,?)";
        $query = $this->db->query($sql, array($id,$estado,$user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	function get_arr_detalle_contratos($data) {
        /*
          lista los contratos
          Descripcion : Lista los contratos
          Fecha Actualiza: 2016-11-07         */
       	$id =  $data['id'];		
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_contrato_select_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id,$user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    

}
