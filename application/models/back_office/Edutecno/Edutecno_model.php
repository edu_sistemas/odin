<?php

/**
 * Edutecno_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 * Fecha creacion:  2016-12-02 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Edutecno_model
 */
class Edutecno_model extends MY_Model {

    function get_arr_listar_edutecno($data) {

        /*
          Ultimo usuario  modifica:  David De Filippi
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-02
         */

        $p_rut_edutecno = $data['rut_edutecno'];
        $p_razon_social_edutecno = $data['razon_social_edutecno'];
        $p_giro_edutecno = $data['giro_edutecno'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_edutecno_select(?,?,?,?,?);";

        $query = $this->db->query($sql, array($p_rut_edutecno, $p_razon_social_edutecno, $p_giro_edutecno, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_edutecno($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva edutecno
          Fecha Actualiza: 2016-10-13
         */
        $p_rut_edutecno = $data['rut_edutecno'];
        $p_razon_social_edutecno = $data['razon_social_edutecno'];
        $p_giro_edutecno = $data['giro_edutecno'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_edutecno_insert(?,?,?,?,?);";

        $query = $this->db->query($sql, array($p_rut_edutecno, $p_razon_social_edutecno, $p_giro_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    function get_arr_edutecno_cbx($data) {
    
    	/*
    	 Ultimo usuario  modifica: Marcelo Romero
    	 Descripcion : Añade una  nueva edutecno
    	 Fecha Actualiza: 2016-10-13
    	 */
    
    	$user_id = $data['user_id'];
    	$user_perfil = $data['user_perfil'];
    
    	$sql = " CALL sp_edutecno_select_cbx(?,?);";
    
    	$query = $this->db->query($sql, array($user_id, $user_perfil));
    	$result = $query->result_array();
    
    	$query->next_result();
    	$query->free_result();
    	return $result;
    }

    function get_arr_edutecno_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_edutecno = $data['id_edutecno'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_edutecno_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array($id_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_edutecno($data) {
        /*
          Ultimo usuario modifica: David De Filippi
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-12-02
         */

        $id_edutecno = $data['id_edutecno'];
        $p_rut_edutecno = $data['rut_edutecno'];
        $p_razon_social_edutecno = $data['razon_social_edutecno'];
        $p_giro_edutecno = $data['giro_edutecno'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_edutecno_update(?,?,?,?,?,?);";
        $query = $this->db->query($sql, array($id_edutecno, $p_rut_edutecno, $p_razon_social_edutecno, $p_giro_edutecno, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }
 
}
