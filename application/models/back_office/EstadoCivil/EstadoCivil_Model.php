<?php

/**
 * EstadoCivil_Model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-20 [David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   EstadoCivil_Model
 */
class EstadoCivil_Model extends MY_Model {

    function get_arr_listar_estadoCivil_cbx($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Lista los estados civilñes para rellenar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_estadoCivil_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
