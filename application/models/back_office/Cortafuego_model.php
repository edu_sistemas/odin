<?php

/**
 * PanelDeControl_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	YYYY-MM-DD [Nombre apellido] <e-mail>
 * Fecha creacion:	2016-09-21 Luis Jarpa <e-mail>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cortafuego_model extends MY_Model {

    // carga  TITULOS DEL menu  usuario
    function get_arr_permiso($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $result = array();

        $sql = "CALL sp_get_permiso('" . $user_id . "','" . $user_perfil . "')";
        $query = $this->db->query($sql);
        $res = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $res;
    }

}

/* End of file Menu_model.php */
/* Location: ./application/models/hito_uno/Menu_model.php */
