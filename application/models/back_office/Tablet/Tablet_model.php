<?php
/**
 * Docente_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  0000-00-00 [Luis Jarpa] <ljarpa@edutecno.com>
 * Ultima edicion:  2018-03-06 [Jessica Roa] <jroa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tablet_model extends MY_Model {

    function get_arr_inventario_tablet($data) {
        /*
        lista el inventario actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_inventario(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_modelo_tablet_by_id($data) {
        /*
        lista el inventario actual de tablet
        */       
        $id_modelo = $data['id_modelo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_inventario_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_modelo, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_cantidad_tablet_by_id($data) {
        /*
        lista el inventario actual de tablet
        */       
        $id_cantidad = $data['id_cantidad'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_cantidad_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_cantidad, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_modelo_eliminar($data){
        $id_modelo = $data['id_modelo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_delete(?,?,?)";
        $query = $this->db->query($sql, array($id_modelo, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_solicitudes_tablet($data) {
        /*
        lista las solicitudes actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_aceptadas(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_solicitudes_tablet_ingresadas($data) {
        /*
        lista las solicitudes actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_ingresadas(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function get_arr_solicitudes_tablet_byID($data) {
        /*
        lista las solicitudes actual de tablet
        */

        $id_solicitud= $data['id_solicitud'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_tablet_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_solicitud, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function descontarStockTablet($data) {
        /*
        lista las solicitudes actual de tablet
        */

        $id_solicitud= $data['id_solicitud'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_descontar_stock(?,?,?)";
        $query = $this->db->query($sql, array($id_solicitud, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function get_arr_solicitudes_tablet_historico($data) {
        /*
        lista las solicitudes actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_historico(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_solicitudes_tablet_historico_control($data) {
        /*
        lista las solicitudes actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_historico_control(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_solicitudes_tablet_by_user($data) {
        /*
        lista las solicitudes actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_usuario(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_ingresos_tablet($data) {
        /*
        lista el ingresos actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_ingresos(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_modelos_tablet($data) {
        /*
        lista los modelos actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_modelos_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function get_arr_gamas_tablet($data) {
        /*
        lista los modelos actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_select_gama(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    function setGamas($data) {
        /*
        lista los modelos actual de tablet
        */        
        $nombre = $data['nombre'];
        $descuento = $data['descuento'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_insert_gama(?,?,?,?)";
        $query = $this->db->query($sql, array($nombre, $descuento, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function desactivarGama($data) {
        /*
        lista los modelos actual de tablet
        */        
        $id_gama_tablet = $data['id_gama_tablet'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_desactivar_gama(?,?,?)";
        $query = $this->db->query($sql, array($id_gama_tablet, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_cursos_tablet($data) {
        /*
        lista los modelos actual de tablet
        */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_curso_tablet_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    
    function get_arr_estados_tablet($data) {
        /*
        lista los modelos actual de tablet
        */ 
        $id_solicitud = $data['id_solicitud'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_estados_tablet_select_cbx(?,?,?)";
        $query = $this->db->query($sql, array($id_solicitud, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    function get_arr_cambio_solicitudes_tablet($data) {
        /*
        muestra la solicitud de tablet que fue modificada para enviar correo
        */ 
        $id_cambio_solicitud = $data['id_cambio_solicitud'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_by_id(?,?,?)";
        $query = $this->db->query($sql, array($id_cambio_solicitud, $user_id, $user_perfil));
        
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_modelos_tablet($data) {
        /*
        lista los modelos actual de tablet
        */      
        $marca = $data['marca'];
        $modelo = $data['modelo'];
        $id_gamma = $data['id_gamma'];
        $detalles = $data['detalles'];
        $descripcion_tecnica_tablet = $data['descripcion_tecnica_tablet'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_insert(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($marca, $modelo, $id_gamma, $detalles, $descripcion_tecnica_tablet, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_ingresos_tablet($data) {
        /*
        lista los modelos actual de tablet
        */        
        $id_modelo = $data['id_modelo'];
        $cantidad = $data['cantidad'];
        $fecha_ingreso = $data['fecha_ingreso'];
        $descripcion = $data['descripcion'];
        $id_proveedor = $data['id_proveedor'];
        $precio = $data['precio'];
        $numero_guia = $data['numero_guia'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_ingreso_insert(?,?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_modelo, $cantidad, $fecha_ingreso, $descripcion, $id_proveedor, $precio, $numero_guia, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_solicitud_tablet($data) {
        /*
        lista los modelos actual de tablet
        */        
        $modelo = $data['modelo'];
        $motivo_solicitud = $data['motivo_solicitud'];
        $ejecutivo = $data['ejecutivo'];
        $cantidad = $data['cantidad'];
        $direccion_entrega = $data['direccion_entrega'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_ingreso_solicitud_insert(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($modelo, $motivo_solicitud, $ejecutivo, $cantidad, $direccion_entrega, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    
    function set_arr_aplicacion_tablet($data) {
        /*
        lista los modelos actual de tablet
        */        
        $id_solicitud_tablet = $data['id_solicitud_tablet'];
        $id_curso = $data['id_curso'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_tablet_ingreso_solicitud_insert_aplicacion(?,?,?,?)";
        $query = $this->db->query($sql, array($id_solicitud_tablet, $id_curso, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    

    function del_arr_ingresos_tablet($data) {
        /*
        lista los modelos actual de tablet
        */        
        $id_ingreso = $data['id_ingreso'];
        $descripcion = $data['descripcion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_ingreso_delete(?,?,?,?)";
        $query = $this->db->query($sql, array($id_ingreso, $descripcion, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

        function set_direccion_solicitud($data) {
        /*
        edita direccion de solicitud
        */        
        $id_solicitud = $data['id_solicitud'];
        $direccion_editar = $data['direccion_editar'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_update(?,?,?,?)";
        $query = $this->db->query($sql, array($id_solicitud, $direccion_editar, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function del_arr_solicitud_tablet($data) {
        /*
        anula solicitud
        */   
        $id_solicitud = $data['id_solicitud'];
        $observacion = $data['observacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_delete(?,?,?,?)";
        $query = $this->db->query($sql, array($id_solicitud, $observacion, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function upd_arr_solicitudes_tablet($data) {
        /*
        anula solicitud
        */   
        $id_solicitud = $data['id_solicitud'];
        $cbx_estado = $data['cbx_estado'];
        $observacion = $data['observacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_solicitud_update_estado(?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_solicitud, $cbx_estado, $observacion, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }










/* CODIGO VIEJO */


    function get_arr_listar_solicitudTablet($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_select(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

	function get_arr_detalle_solicitudTablet($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
		$id = $data['id'];
        $sql = "CALL sp_tablet_select_detalle(?,?,?)";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	function get_arr_detalle_entregaTablet($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
		$id = $data['id'];
        $sql = "CALL sp_tablet_select_entrega(?,?,?)";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	function set_arr_detalle_entregaTablet($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */       
		$id =  $data['id']; 
		$fecha =  $data['fecha'];
		$cantidad =  $data['cantidad'];
		$modelo =  $data['modelo'];
		$comentario =  $data['comentario'];
		
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];		
        $sql = "CALL sp_tablet_set_detalle(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id,$fecha,$cantidad,$modelo,$comentario,$user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	function set_arr_detalle_devolucionTablet($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */       
		$id =  $data['id']; 
		$fecha =  $data['fecha'];
		$cantidad =  $data['cantidad'];
		$modelo =  $data['modelo'];
		$motivo =  $data['motivo'];
		$comentario =  $data['comentario'];
		
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];		
        $sql = "CALL sp_tablet_insert_devolucion(?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id,$fecha,$cantidad,$modelo,$motivo,$comentario,$user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	function get_arr_detalle_devTablet($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
		$id = $data['id'];
        $sql = "CALL sp_tablet_select_devol(?,?,?)";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	function delete_arr_dev($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
		$id = $data['id'];
        $sql = "CALL sp_tablet_delete_devol(?,?,?)";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	
	function get_arr_listar_solicitudTabletByUser($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_select_by_user(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
	
	function get_arr_listar_motivos($data){
		$user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_select_motivos(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
	}
	
	function get_arr_listar_deptos($data){
				
		$user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_select_deptos(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
	
	}
	
	function set_arr_ingresar_solicitud($data){
				
		$motivo = $data['motivo'];
		$departamento = $data['departamento'];
		$fechaSolicitud = $data['fechaSolicitud'];
		$cantidad = $data['cantidad'];
		$modelo =  $data['modelo'];
		$fechaRequerida = $data['fechaRequerida'];
		$aplicaciones = $data['aplicaciones'];
		$comentarios = $data['comentarios'];
		
		$user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_insert_solicitud(?,?,?,?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($motivo,$departamento,$fechaSolicitud,$cantidad,$modelo,$fechaRequerida,$aplicaciones,$comentarios,$user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
	
	}
	function delete_arr_sol($data) {
        /*
          lista los docentes
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */        
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
		$id = $data['id'];
        $sql = "CALL sp_tablet_delete_sol(?,?,?)";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_gamma_cbx($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_tablet_select_gamma_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_proveedor_cbx($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_proveedor_tablet_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
