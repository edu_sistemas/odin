<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 * Fecha creacion:  2017-25-01 [Don David De Filippi] <dfilippi@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Comuna_model
 */
class Comuna_model extends MY_Model {

    function get_arr_listar_comuna_cbx($data) {
        /*
          Ultimo usuario  modifica: David De Filippi
          Descripcion : Lista las comunas para rellenar un cbx
          Fecha Actualiza: 2017-01-25
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_comuna_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
}
