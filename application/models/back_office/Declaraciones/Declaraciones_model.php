<?php

/**
 * Log_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-11 [David De Filippi] <mromero@edutecno.com>
 * Fecha creacion:  2017-01-11 [David De Filippi] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Log_model
 */
class Declaraciones_model extends MY_Model {

    function get_arr_listar_cbx($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista los numero deficha que están dentro de la tabla ficha_reserva
          Fecha Actualiza: 2017-01-12
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_dj_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_alumnos($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */

        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_alumnos_dj_by_id_ficha(?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_add_dj($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Registra la asistencia en la tabla asistencia, recorre ciclo for con los datos que lista el datatable
          Fecha Actualiza: 2017-01-12
         */
        $result = false;
        $numRows = $data['numRows'];
        $arr = array();
        $arr_dtla = array();

        $arr_input_data = array(
            'chk' => $arr,
            'id_alm' => $arr_dtla,
            'numRows' => $this->input->post('numRows')
        );

        for ($inst = 0; $inst < $numRows; $inst++) {

            $a = $data['chk'][$inst];
            $b = $data['id_alm'][$inst];
            $numRows = $data['numRows'];
            $user_id = $data['user_id'];
            $user_perfil = $data['user_perfil'];

            $sql = " CALL sp_asistencia_dj_existe(?,?,?,?)";
            $query = $this->db->query($sql, array($a, $b, $user_id, $user_perfil));

            //echo $this->db->last_query();
            $result = $query->result_array();
            $query->next_result();
            $query->free_result();
        }
        return $result;
    }

}
