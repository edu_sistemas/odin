<?php

/**
 * Categoria_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-27 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *   Categoria_Model
 */
class Grupo_Comercial_model extends MY_Model {

    function get_grupos_comerciales_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_select_grupo_comercial_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    function get_usuarios_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_usuario_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_grupocomercial_resumen($data){
     
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_grupo_comercial_select_todo(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;


    }

    function get_grupo_comercial($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */
        $id_grupo_comercial = $data['id_grupo_comercial'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_select_grupo_comercial_by_id(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_grupo_comercial,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function create_grupo_comercial($data){
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_grupo_comercial_insert(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_usuario_grupo_comercial($data){
        $id_grupo_comercial = $data['id_grupo_comercial'];
        $id_usuario = $data['id_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_usuario_grupo_comercial_insert_especial(?,?,?,?)";
        $query = $this->db->query($sql, array($id_grupo_comercial, $id_usuario, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_usuario_grupo_comercial_comun($data){

        $id_grupo_comercial = $data['id_grupo_comercial'];
        $id_usuario = $data['id_usuario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_usuario_grupo_comercial_insert(?,?,?,?)";
        $query = $this->db->query($sql, array($id_grupo_comercial, $id_usuario, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function quitar_usuario_grupo_comercial($data){
        $id_usuario = $data['id_usuario'];
        $id_grupo_comercial = $data['id_grupo_comercial'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_usuario_grupo_comercial_delete(?,?,?,?)";
        $query = $this->db->query($sql, array($id_usuario,$id_grupo_comercial,$user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function marcar_jefe_grupo_comercial($data){
        $id_usuario = $data['id_usuario'];
        $id_grupo_comercial = $data['id_grupo_comercial'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_usuario_grupo_update_jefe(?,?,?,?)";
        $query = $this->db->query($sql, array($id_usuario,$id_grupo_comercial,$user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
