<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 */
class Permiso_model extends MY_Model {

    function get_arr_listar_permiso_lbx($data) {
        /*
          lista los usuarios taxman
          Descripcion : Lista los perfiles
          Fecha Actualiza: 2016-11-07
         */
        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_item_listar_lbx(?,?,?)";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));


        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_permiso_perfil($data) {
        /*
          Descripcion :lista los permisos que ya se le otorgaron previasmente al perfil
          Fecha Actualiza: 2016-11-07
         */

        $id = $data['id'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_permiso_perfil(?,?,?)";
        $query = $this->db->query($sql, array($id, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_arr_otorgar_permiso($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade permisos al perfil que se selecciono.
          Fecha Actualiza: 2016-11-18
         */

        $fk_id_perfil = $data['fk_id_perfil'];
        $fk_id_item = $data['fk_id_item'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_permiso_insert(?,?,?,?);";
        $query = $this->db->query($sql, array($fk_id_perfil, $fk_id_item, $user_id, $user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_quitar_permiso($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Elimina los permisos que tiene un perfil (items)
          Fecha Actualiza: 2016-11-21
         */

        $fk_id_perfil = $data['fk_id_perfil'];
        $fk_id_item = $data['fk_id_item'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_permiso_delete(?,?,?,?)";
        $query = $this->db->query($sql, array($fk_id_perfil, $fk_id_item, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

}
