<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-12 [Luis Jarpa] <lajrpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * OrdenFactura_model
 */
class OrdenDespacho_model extends MY_Model {

    function get_arr_listar_fichas_despacho($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $empresa = $data['empresa'];
        $holding = $data['holding'];

        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_ficha_factura_select_despacho(?,?,?,?,?,?);";
        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_factura_by_oc($data) {
        /*
         * Ultimo usuario modifica: Marcelo Romero
         * Descripcion : Recupeera perfil desde BD
         * Fecha Actualiza: 2016-11-08
         */
        $id_orden_compra = $data ['id_orden_compra'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_ficha_factura_select_by_oc(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_factura_by_oc_cbx($data) {
        /*
         * Ultimo usuario modifica: Marcelo Romero
         * Descripcion : Recupeera perfil desde BD
         * Fecha Actualiza: 2016-11-08
         */
        $id_orden_compra = $data ['id_orden_compra'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_ficha_factura_select_cbx(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_estado_despacho_factura_ok($data) {


        $id_ficha = $data ['id_ficha'];
        $id_orden_compra = $data ['id_orden_compra'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_update_estado_despacho_ok(?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_estado_despacho_factura_error($data) {



        $id_ficha = $data ['id_ficha'];
        $id_orden_compra = $data ['id_orden_compra'];
        $comentario_rechazo = $data ['comentario_rechazo'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_update_estado_despacho_error(?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_orden_compra,
            $comentario_rechazo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_comentario_rechazo_despacho($data) {

        $comentario_rechazo = $data ['comentario_rechazo'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $id_factura = $data ['id_factura'];

        $sql = "CALL sp_finanza_insert_observacion_error(?,?,?,?);";

        $query = $this->db->query($sql, array(
            $comentario_rechazo,
            $user_id,
            $user_perfil,
            $id_factura
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
