<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2017-01-12 [Luis Jarpa] <lajrpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Factura_model
 */
class Factura_model extends MY_Model {

    function get_arr_usuario_by_ejecutivo_comercial($data) {

        /*
         * Ultimo usuario modifica: Luis Jarpa
         * Descripcion : Selecciona todos los usauruios tipo ejecutivo comercial
         * Fecha Actualiza: 2017-01-12
         *
         */
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_usuario_select_tipo_eje_comercial_cbx(?,?);";

        $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_usuario_muro_by_id($data) {
        /*
         * Ultimo usuario modifica: Marcelo Romero
         * Descripcion : Recupeera perfil desde BD
         * Fecha Actualiza: 2016-11-08
         */
        $id = $data ['id_usuario'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_usuario_muro_select_by_id(?,?,?);";

        $query = $this->db->query($sql, array(
            $id,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_imagenes_usr($data) {
        /*
         * Llama las imagenes del usuario
         * Descripcion : Lista las imagenes
         * Fecha Actualiza: 2016-11-25
         */
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_usuario_muro_select_images (?,?)";
        $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_imagenes_usr($data) {
        /*
         * Llama las imagenes del usuario
         * Descripcion : Lista las imagenes
         * Fecha Actualiza: 2016-11-25
         */
        // $fk_user_id = $data['fk_id_usuario'];
        $id_img = $data ['id_img'];
        $imagen = $data ['imagen'];
        $seleccionada = $data ['seleccionada'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_usuario_muro_insert_update_images(?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_img,
            $imagen,
            $seleccionada,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_cambiar_password($data) {
        /*
         * Cambia el password del usuario
         * Descripcion : Lista las imagenes
         * Fecha Actualiza: 2016-11-29
         */
        $pass_actual = $data ['pass_actual'];
        $pass_nueva = $data ['pass_nueva'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_usuario_muro_update_password(?,?,?,?);";

        $query = $this->db->query($sql, array(
            $pass_actual,
            $pass_nueva,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_fichas($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $empresa = $data['empresa'];
        $holding = $data['holding'];
        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_factura_select(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_fichas_pendientes($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $start = $data['inicio'];
        $end = $data['fin'];
        $empresa = $data['empresa'];
        $holding = $data['holding'];
        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_factura_select_pendientes(?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $start,
            $end,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

       function get_arr_listar_facturas_pendientes($data) {
        $p_ano = $data['p_ano'];
        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_factura_informe_by_pendiente(?,?,?);";

        $query = $this->db->query($sql, array(
            $p_ano,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    

    function get_arr_listar_fichas_facturas_informe($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $empresa = $data['empresa'];
        $holding = $data['holding'];
        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_factura_select_informe(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_fichas_facturas_informe_by_ficha($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $empresa = $data['empresa'];
        $holding = $data['holding'];
        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_factura_select_informe_by_ficha(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_fichas_realizadas($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $empresa = $data['empresa'];
        $holding = $data['holding'];
        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_factura_realizadas_select(?,?,?,?,?,? )";

        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_fichas_historico($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $empresa = $data ['empresa'];
        $holding = $data ['holding'];
        $ejecutivo = $data ['ejecutivo'];
        $num_ficha = $data ['num_ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_factura_historico_select(?,?,?,?,?,? )";

        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_factura_by_oc($data) {
        /*
         * Ultimo usuario modifica: Marcelo Romero
         * Descripcion : Recupeera perfil desde BD
         * Fecha Actualiza: 2016-11-08
         */
        $id_orden_compra = $data ['id_orden_compra'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_factura_select_by_oc(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_factura_by_id_factura($data) {
        /*
         * Ultimo usuario modifica: Marcelo Romero
         * Descripcion : Recupeera perfil desde BD
         * Fecha Actualiza: 2016-11-08
         */
        $id_factura = $data ['id_factura'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_factura_get_by_id(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_factura,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_nota_debito_by_id_nota_debito($data) {
        $id_nota_debito = $data ['id_nota_debito'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_nota_debito_get_by_id(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_nota_debito,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_nota_credito_by_id_nota_credito($data) {
        $id_nota_credito = $data ['id_nota_credito'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_nota_credito_get_by_id(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_nota_credito,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }


    function set_arr_ubdate_factura($data) {
        $id_factura = $data['id_factura'];
        $tipo_factura = $data['tipo_factura'];
        $n_factura = $data['n_factura'];
        $monto_factura = $data['monto_factura'];
        $fecha_emision = $data['fecha_emision'];
        $fecha_vencimiento = $data['fecha_vencimiento'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_factura_update(?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_factura,
            $tipo_factura,
            $n_factura,
            $monto_factura,
            $fecha_emision,
            $fecha_vencimiento,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_update_nota_debito($data) {
        $id_nota_debito = $data['id_nota_debito'];
        $num_nota_debito = $data['num_nota_debito'];
        $fecha_emision = $data['fecha_emision'];
        $monto_debito = $data['monto_debito'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_nota_debito_update(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_nota_debito,
            $num_nota_debito,
            $fecha_emision,
            $monto_debito,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

      function set_arr_update_nota_credito($data) {
        $id_nota_credito = $data['id_nota_credito'];
        $num_nota_credito = $data['num_nota_credito'];
        $fecha_emision = $data['fecha_emision'];
        $monto_credito = $data['monto_credito'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_nota_credito_update(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_nota_credito,
            $num_nota_credito,
            $fecha_emision,
            $monto_credito,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_factura_select_by_oc_cbx($data) {
        $id_orden_compra = $data ['id_orden_compra'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_factura_select_by_oc_cbx(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_ficha_select_docs($data) {
        $id_ficha = $data ['id_ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_ficha_select_docs(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_rectificacion_select_docs_by_oc($data) {
        $id_orden_compra = $data ['id_orden_compra'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_rectificacion_select_docs_by_oc(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_tipo_facturacion_cbx($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_factura_tipo_facturacion_cbx(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_estado_facturacion_cbx($data) {
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_factura_estado_facturacion_cbx(?,?);";

        $query = $this->db->query($sql, array(
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

   

    function set_arr_agregar_factura($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $tipo_factura = $data['tipo_factura'];
        $n_factura = $data['n_factura'];
        $monto_factura = $data['monto_factura'];
        $fecha_emision = $data['fecha_emision'];
        $fecha_vencimiento = $data['fecha_vencimiento'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_factura_insert(?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $tipo_factura,
            $n_factura,
            $monto_factura,
            $fecha_emision,
            $fecha_vencimiento,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_nota_credito_insert($data) {
        $id_factura = $data ['id_factura'];
        $num_nota_credito = $data ['num_nota_credito'];
        $fecha_emision = $data ['fecha_emision'];
        $monto_credito = $data ['monto_credito'];
        $tipo_entrega = $data ['tipo_entrega'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_nota_credito_insert(?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_factura,
            $num_nota_credito,
            $fecha_emision,
            $monto_credito,
            $tipo_entrega,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_nota_debito_insert($data) {
        $id_factura_nd = $data ['id_factura_nd'];
        $num_nota_debito = $data ['num_nota_debito'];
        $fecha_emision = $data ['fecha_emision'];
        $monto_debito = $data ['monto_debito'];
        $tipo_entrega = $data ['tipo_entrega'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_nota_debito_insert(?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_factura_nd,
            $num_nota_debito,
            $fecha_emision,
            $monto_debito,
            $tipo_entrega,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_usuario($data) {
        /*
         * Ultimo usuario modifica: Marcelo Romero
         * Descripcion : Edita perfil desde BD
         * Fecha Actualiza: 2016-10-14
         */
        $id = $data ['id'];
        $usuario = $data ['usuario'];
        $correo = $data ['correo'];
        $nombre_usuario = $data ['nombre_usuario'];
        $apellidos_usuario = $data ['apellidos_usuario'];
        $rut_usuario = $data ['rut_usuario'];
        $dv_usuario = $data ['dv_usuario'];
        $perfil = $data ['perfil'];
        $telefono = $data ['telefono'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_usuario_update(?,?,?,?,?,?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id,
            $usuario,
            $correo,
            $telefono,
            $nombre_usuario,
            $apellidos_usuario,
            $rut_usuario,
            $dv_usuario,
            $perfil,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_entregado_cbx($data) {
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_tipo_entrega_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_entregado_cbx2($data) {
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_usuario_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_usuario_change_status($data) {
        /*
         * Ultimo usuario modifica: Luis Jarpa
         * Descripcion : Actualzia el estado de los usuarios
         * Fecha Actualiza: 2016-11-25
         */
        $id_usuario = $data ['id_usuario'];
        $estado = $data ['estado'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        // listar altiro null
        $sql = "CALL sp_usuario_desactivar_activar(?,?,?,?)";
        $query = $this->db->query($sql, array($id_usuario, $estado, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_sp_factura_update_estado($data) {
        /*
         * Ultimo usuario modifica: Luis Jarpa
         * Descripcion : Actualzia el estado de los usuarios
         * Fecha Actualiza: 2016-11-25
         */
        $id_factura = $data ['id_factura'];
        $id_estado_factura = $data ['id_estado_factura'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        // listar altiro null
        $sql = "CALL sp_factura_update_estado(?,?,?,?)";

        $query = $this->db->query($sql, array($id_factura, $id_estado_factura, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_sp_factura_select_notas($data) {
        $id_factura = $data ['id_factura'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_factura_select_notas_credito(?,?,?)";

        $query = $this->db->query($sql, array($id_factura, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

        function get_sp_factura_select_notas_by_id_factura($data) {
        $id_factura = $data ['id_factura'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_factura_select_notas_credito_by_id_factura(?,?,?)";

        $query = $this->db->query($sql, array($id_factura, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

       function get_sp_factura_select_notas_debito($data) {
        $id_factura_nd = $data ['id_factura_nd'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_factura_select_notas_debito(?,?,?)";

        $query = $this->db->query($sql, array($id_factura_nd, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }
    
    function get_sp_factura_select_notas_debito_by_id_factura($data) {
        $id_factura_nd = $data ['id_factura_nd'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_factura_select_notas_debito_by_id_factura(?,?,?)";

        $query = $this->db->query($sql, array($id_factura_nd, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_tipo_entrega_fact($data) {
        $id_factura = $data ['id_factura'];
        $id_tipo_entrega = $data ['id_tipo'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_factura_tipo_entrega(?,?,?,?)";

        $query = $this->db->query($sql, array($id_factura, $id_tipo_entrega, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_tipo_entrega_nc($data) {
        $id_nc = $data ['id_nc'];
        $id_tipo_entrega = $data ['id_tipo'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_nota_credito_tipo_entrega(?,?,?,?)";

        $query = $this->db->query($sql, array($id_nc, $id_tipo_entrega, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_tipo_entrega_nd($data) {
        $id_n = $data ['id_nd'];
        $id_tipo_entrega = $data ['id_tipo'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = " CALL sp_nota_debito_tipo_entrega(?,?,?,?)";
        
        $query = $this->db->query($sql, array($id_n, $id_tipo_entrega, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    /* get info oc */

    function get_arr_ficha_by_id_orden_compra_resumen($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $opcion_ficha = $data['opcion_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_ficha_select_by_id_orden_compra_resumen(?,?,?,?)";

        $query = $this->db->query($sql, array($id_orden_compra, $opcion_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_orden_compra_by_id_orden_compra($data) {
        $id_orden_compra = $data ['id_orden_compra'];
        $opcion_ficha = $data['opcion_ficha'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_orden_compra_select_by_ficha_id_orden_compra(?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_orden_compra,
            $opcion_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_by_id_orden_compra($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_rectificacion_alumno_select_by_id_orden_compra(?,?,?)";

        $query = $this->db->query($sql, array($id_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_doc_by_id_orden_compra($data) {
        $id_orden_compra = $data ['id_orden_compra'];
        $user_id = $data ['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_rectificacion_document_alumno_select_by_id_orden_compra(?,?,?)";

        $query = $this->db->query($sql, array($id_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_rectificaciones_extension_by_id_orden_compra($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $id_orden_compra = $data ['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_rectificacion_extensiones_select_by_id_orden_compra(?,?,?)";

        $query = $this->db->query($sql, array($id_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_rectificaciones_extension_doc_by_id_orden_compra($data) {
        /*
          Ultimo usuario  modifica:  David De Filippi , Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $id_orden_compra = $data['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_rectificacion_document_extens_select_by_id_orden_compra(?,?,?)";

        $query = $this->db->query($sql, array($id_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_getFacturaByOC($data) {
        /*
          Ultimo usuario  modifica:  D  Luis Jarpa
          Descripcion : las facturas asociadas a las ordend e compra
          Fecha Actualiza: 2016-12-26
         */
        $id_orden_compra = $data['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_factura_select_by_id_orden_compra(?,?,?)";

        $query = $this->db->query($sql, array($id_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_data_estado_factura($data) {

        /*
          Ultimo usuario  modifica:  D  Luis Jarpa
          Descripcion : las facturas asociadas a las ordend e compra
          Fecha Actualiza: 2016-12-26
         */

        $id_factura = $data['id_factura'];
        $estado_factura = $data['estado_factura'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_factura_update_estado(?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_factura,
            $estado_factura,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_documentos_tributarios($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $p_ano = $data['p_ano'];
        $empresa = $data['empresa'];
        $holding = $data['holding'];
        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];
        $sql = "CALL sp_factura_select_completo_separado(?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $p_ano,
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_facturas($data) {
        /*
         * lista las facturas
         * Descripcion : Lista los perfiles
         * Fecha Actualiza: 2016-11-07
         */
        $empresa = $data['empresa'];
        $holding = $data['holding'];

        $ejecutivo = $data['ejecutivo'];
        $num_ficha = $data['num_ficha'];

        $user_id = $data['user_id'];
        $user_perfil = $data ['user_perfil'];

        $sql = "CALL sp_factura_select_completo(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array(
            $empresa,
            $holding,
            $ejecutivo,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function Validar_get_fichas_validar($data) {
        /*
          Ultimo usuario  modifica:    Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_validar_select(?,?)";

        $query = $this->db->query($sql, array($user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function Validar_get_sp_ficha_select_preview_by_id($data) {
        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las edutecno
          Fecha Actualiza: 2016-12-26
         */

        $p_id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_preview_validar_select(?,?,?)";

        $query = $this->db->query($sql, array($p_id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_validar_alumnos($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_validar_alumnos_select(?,?,?)";

        $query = $this->db->query($sql, array($id_ficha, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_participantes_ficha_correo($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */
        $ids_participantes = $data['ids_participantes'];
        $observaciones = $data['observaciones'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_validar_alumnos_mensaje_solicitar(?,?,?,?)";

        $query = $this->db->query(
                $sql, array(
            $ids_participantes,
            $observaciones,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_ficha_correo($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */
        $id_ficha = $data['id_ficha'];
        $comentarios = $data['observaciones'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_validar_get_ficha_solicitar(?,?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_ficha,
            $comentarios,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_validacion_todo_ok($data) {
        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Lista las capsulas por ficha
          Fecha Actualiza: 2017-01-12
         */
        $id_ficha = $data['id_ficha'];
        $comentarios = $data['observaciones'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_validar_set_ficha_todo_ok(?,?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_ficha,
            $comentarios,
            $user_id,
            $user_perfil
                )
        );

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function enviarFacturaAFacturacion($data) {
        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $observacion = $data['observacion'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_enviar_a_facturacion(?,?,?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_ficha,
            $id_orden_compra,
            $observacion,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function updateIDSENCEBD($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $num_ficha = $data['num_ficha'];
        $id_sence = $data['id_sence'];
        $elim_sence = $data['elim_sence'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_factura_update_id_sence(?,?,?,?,?,?);";
        $query = $this->db->query(
                $sql, array(
            $id_orden_compra,
            $num_ficha,
            $id_sence,
            $elim_sence,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function getIDSENCEBD($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $num_ficha = $data['num_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_get_id_sence(?,?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_orden_compra,
            $num_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function getIDSENCEBDV($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_factura_get_id_sence(?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function updatenumOCBD($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $num_oc = $data['num_oc'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_factura_update_oc(?,?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_orden_compra,
            $num_oc,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function getnumOCBD($data) {
        $id_orden_compra = $data['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_factura_get_oc(?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function enviarFacturaADespacho($data) {
        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_enviar_a_despacho(?,?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_ficha,
            $id_orden_compra,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function procesoFicahTerminadoDB($data) {
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_proceso_facturacion_terminado(?,?,?);";

        $query = $this->db->query(
                $sql, array(
            $id_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function enviarSolicitudDeCorrecionF($data) {
        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $num_ficha = $data['num_ficha'];
        $comentario = $data['comentario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_facturacion_solicitud_correccion(?,?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $id_orden_compra,
            $num_ficha,
            $comentario,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function savemiobservacionFact($data) {
        $id_ficha = $data['id_ficha'];
        $id_orden_compra = $data['id_orden_compra'];
        $comentario = $data['comentario'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_finanza_insert_observacion(?,?,?,?,?);";

        $query = $this->db->query($sql, array(
            $comentario,
            $user_id,
            $user_perfil,
            $id_ficha,
            $id_orden_compra
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_array_observaciones_ficha($data) {
        $id_ficha = $data['id_ficha'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_ficha_get_observacion_ficha_finanza(?,?,?);";

        $query = $this->db->query($sql, array(
            $id_ficha,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function set_document_factura($data) {
        $id_ficha = $data['id_factura'];
        $nombre_documento = $data['nombre_documento'];
        $fichero_subido = $data['ruta_documento'];
        $tipo_documento = $data['tipo_documento'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_ficha_insert_document_by_factura(?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_ficha, $nombre_documento, $fichero_subido, $tipo_documento, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
