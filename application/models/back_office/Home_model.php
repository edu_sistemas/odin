<?php

/**
 * Login_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-09-23 [Luis Jarpa] <ljarpa@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * CrearCicloEmpresa_model
 */
class Home_model extends MY_Model {

    function comprobarCodigoSENCEvencimiento($data){

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_code_sence_select_expirados_nuevos(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        
        return $result;
    }

    function setCodeSenceNotificado($data){

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_code_sence_marcar_como_notificado(?,?)";

        $query = $this->db->query($sql,array($user_id,$user_perfil));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        
        return $result;
    }
    
}
