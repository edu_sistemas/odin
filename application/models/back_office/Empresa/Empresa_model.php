<?php

/**
 * Empresa_model
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:  2016-11-10 [Marcelo Romero] <ljarpa@edutecno.com>
 * Fecha creacion:  2016-11-10 [Marcelo Romero] <mromero@edutecno.com>
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Empresa_model
 */
class Empresa_model extends MY_Model {

    function buscarEmpresa($data){
        $busqueda = $data['busqueda'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_empresa_select_busqueda(?,?,?)";
        $query = $this->db->query($sql, array($busqueda ,$user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_empresa_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_empresa_select_cbx(?,?)";
        $query = $this->db->query($sql, array($user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_direcciones_by_empresa_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_empresa_select_direccion_cbx(?,?,?)";
        $query = $this->db->query($sql, array($id_empresa, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_nombre_contacto_by_empresa_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_empresa_select_contact_nombre_by_cbx(?,?,?)";

        $query = $this->db->query($sql, array($id_empresa, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_telefono_contacto_by_empresa_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */

        $id_contacto = $data['id_contacto'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = "CALL sp_empresa_select_contact_telefono_by_cbx(?,?,?)";

        $query = $this->db->query($sql, array($id_contacto, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_empresa_by_holding_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Lista las categorias model para rellnar un cbx
          Fecha Actualiza: 2016-10-27
         */


        $id_holding = $data['id_holding'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_empresa_select_cbx_by_holding(?,?,?)";
        $query = $this->db->query($sql, array($id_holding, $user_id, $user_perfil));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_listar_empresa($data) {

        /*
          Ultimo usuario  modifica:  Luis Jarpa
          Descripcion : Lista las empresas
          Fecha Actualiza: 2016-10-27
         */

        $p_rut_empresa = $data['rut_empresa'];
        $p_nombre_empresa = $data['nombre_empresa'];
        $p_id_holding = $data['id_holding'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_select(?,?,?,?,?)";



        $query = $this->db->query($sql, array(
            $p_rut_empresa,
            $p_nombre_empresa,
            $p_id_holding,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_agregar_emrpesa($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2016-10-13
         */
        $rut = $data['rut'];
        $nombre_fantasia = $data['nombre_fantasia'];
        $razon = $data['razon'];
        $holding = $data['holding'];
        $descripcion = $data['descripcion'];
        $giro = $data['giro'];
        $direccion = $data['direccion'];
        $comuna = $data['comuna'];
        $telefono_temporal = $data['telefono_temporal'];
        $direccion_factura = $data['direccion_factura'];
        $comuna_factura = $data['comuna_factura'];
        $direccion_despacho = $data['direccion_despacho'];
        $mail_sii = $data['mail_sii'];
        $nombre_contacto_cobranza = $data['nombre_contacto_cobranza'];
        $mail_contacto_cobranza = $data['mail_contacto_cobranza'];
        $telefono_contacto_cobranza = $data['telefono_contacto_cobranza'];
        $ejecutivo = $data['ejecutivo'];
        $observaciones_glosa_facturacion = $data['observaciones_glosa_facturacion'];
        $requiere_orden_compra = $data['requiere_orden_compra'];
        $requiere_hess = $data['requiere_hess'];
        $requiere_numero_contrato = $data['requiere_numero_contrato'];
        $requiere_ota = $data['requiere_ota'];
        $logo = $data['logo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_empresa_insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $rut,
            $nombre_fantasia,
            $razon,
            $holding,
            $descripcion,
            $giro,
            $direccion,
            $comuna,
            $telefono_temporal,
            $direccion_factura,
            $comuna_factura,
            $direccion_despacho,
            $mail_sii,
            $nombre_contacto_cobranza,
            $mail_contacto_cobranza,
            $telefono_contacto_cobranza,
            $ejecutivo,
            $observaciones_glosa_facturacion,
            $requiere_orden_compra,
            $requiere_hess,
            $requiere_numero_contrato,
            $requiere_ota,
            $logo,
            $user_id,
            $user_perfil
        ));


        $result = $query->result_array();

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_empresa_by_id($data) {

        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Recupeera perfil desde BD
          Fecha Actualiza: 2016-10-14
         */
        $id_empresa = $data['id_empresa'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_select_by_id(?,?,?)";

        $query = $this->db->query($sql, array(
            $id_empresa,
            $user_id,
            $user_perfil
        ));


        $row_count = $query->num_rows();
        $result = false;
        if ($row_count > 0) {
            $result = $query->result_array();
        }

        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_editar_empresa($data) {
        /*
          Ultimo usuario modifica: Marcelo Romero
          Descripcion : Edita perfil desde BD
          Fecha Actualiza: 2016-10-14
         */

        $id_empresa = $data['id_empresa'];
        $rut = $data['rut'];
        $nombre_fantasia = $data['nombre_fantasia'];
        $razon = $data['razon'];
        $holding = $data['holding'];
        $descripcion = $data['descripcion'];
        $giro = $data['giro'];
        $direccion = $data['direccion'];
        $comuna = $data['comuna'];
        $telefono_temporal = $data['telefono_temporal'];
        $direccion_factura = $data['direccion_factura'];
        $comuna_factura = $data['comuna_factura'];
        $direccion_despacho = $data['direccion_despacho'];
        $mail_sii = $data['mail_sii'];
        $nombre_contacto_cobranza = $data['nombre_contacto_cobranza'];
        $mail_contacto_cobranza = $data['mail_contacto_cobranza'];
        $telefono_contacto_cobranza = $data['telefono_contacto_cobranza'];
        $observaciones_glosa_facturacion = $data['observaciones_glosa_facturacion'];
        $requiere_orden_compra = $data['requiere_orden_compra'];
        $requiere_hess = $data['requiere_hess'];
        $requiere_numero_contrato = $data['requiere_numero_contrato'];
        $requiere_ota = $data['requiere_ota'];
        $logo = $data['logo'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $id_empresa,
            $rut,
            $nombre_fantasia,
            $razon,
            $holding,
            $descripcion,
            $giro,
            $direccion,
            $comuna,
            $telefono_temporal,
            $direccion_factura,
            $comuna_factura,
            $direccion_despacho,
            $mail_sii,
            $nombre_contacto_cobranza,
            $mail_contacto_cobranza,
            $telefono_contacto_cobranza,
            $observaciones_glosa_facturacion,
            $requiere_orden_compra,
            $requiere_hess,
            $requiere_numero_contrato,
            $requiere_ota,
            $logo,
            $user_id,
            $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    /* ------------Funciones para la carga masiva-------------------- */

    function get_arr_subir_emrpesa($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2016-10-17
         */
        $rut_empresa = $data['rut_empresa'];
        $razon_social = $data['razon_social'];


        $giro = $data['giro'];
        $direccion = $data['direccion'];
        $descripcion = $data['descripcion'];


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];


        $sql = " CALL sp_empresa_carga_insert(?,?,?,?,?,?,?)";

        $query = $this->db->query($sql, array(
            $rut_empresa,
            $razon_social,
            $giro,
            $direccion,
            $descripcion,
            $user_id,
            $user_perfil
        ));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function cargar_empresas_tbl_temporal($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_carga_select(?,?)";

        $query = $this->db->query($sql, array(
            $user_id, $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_arr_empresa_venta($data) {


        $id_empresa = $data['id_empresa'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_venta_s_select(?,?,?)";

        $query = $this->db->query($sql, array(
            $id_empresa, $user_id, $user_perfil
        ));

        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function get_eliminar_empresas_temporal($data) {

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = "CALL sp_empresa_carga_delete(?,?);";

        $query = $this->db->query($sql, array(
            $user_id, $user_perfil
        ));



        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }


    function set_arr_agregar_final_emrpesa($data) {



        /*
          Ultimo usuario  modifica: Marcelo Romero
          Descripcion : Añade una  nueva empresa
          Fecha Actualiza: 2016-10-13
         */


        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];

        $sql = " CALL sp_empresa_carga_insert_tbl_empresa(?,?)";
        $query = $this->db->query($sql, array(
            $user_id, $user_perfil
        ));


        $result = $query->result_array();
        $query->next_result();
        $query->free_result();

        return $result;
    }

    function get_arr_listar_empresa_by_orden_compra_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : optine la empresa asociada a la orden de compra
          Fecha Actualiza: 2016-10-27
         */

        $id_orden_compra = $data['id_orden_compra'];
        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_empresa_select_cbx_by_orden_compra(?,?,?)";
        $query = $this->db->query($sql, array($id_orden_compra, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

    function set_arr_listar_empresa_by_orden_compra_cbx($data) {
        /*
          Ultimo usuario  modifica: Luis Jarpa
          Descripcion : optine la empresa asociada a la orden de compra
          Fecha Actualiza: 2016-10-27
         */


        $id_orden_compra = $data['id_orden_compra'];
        $id_empresa = $data['id_empresa'];
        $id_otic = $data['id_otic'];
        $aplica_bipartito = $data['bipartito'];
        $precontrato = $data['precontrato'];

        $user_id = $data['user_id'];
        $user_perfil = $data['user_perfil'];
        $sql = " CALL sp_empresa_update_empresa_orden_compra(?,?,?,?,?,?,?)";
        $query = $this->db->query($sql, array($id_orden_compra, $id_empresa, $id_otic, $aplica_bipartito, $precontrato, $user_id, $user_perfil));
        $result = $query->result_array();
        $query->next_result();
        $query->free_result();
        return $result;
    }

}
