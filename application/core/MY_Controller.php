<?php

/**
 * MY_Controller
 *
 * Description...
 *
 * @version 0.0.1
 *
 * Ultima edicion:	2016-06-09 [Cristián Aguirre] <caguirre@edutecno.cl>
 * Fecha creacion:	2016-06-08 [César Hinojosa] <chinojosa@edutecno.com>
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $arr_cortaFuego = '';

    public function getCBX($sp_name) {
        $arr_sesion = array(
            'sp_name' => $sp_name,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelcore->getCBX($arr_sesion);
        echo json_encode($datos);
    }
    public function getCBXByID($sp_name) {
        $arr_sesion = array(
            'sp_name' => $sp_name,
            'id' => $this->input->post('id'),
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelcore->getCBXByID($arr_sesion);
        echo json_encode($datos);
    }

    public function enviarFormulario($sp_name) {
        $data_form = $this->input->post();
        $arr_sesion = array(
            'sp_name' => $sp_name,
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelcore->enviarFormulario($arr_sesion, $data_form);
        echo json_encode($datos);
    }

    function __construct() {

        parent::__construct();
        // Carga del modelo para menú
        $this->load->library('session');
        $this->load->library('My_PHPMailer');

        //$this->load->library('form_validation');


        $this->load->library('ControlDeSesion_lib', 'controldesesion_lib');
        $this->controldesesion_lib->is_logged_in_back_office($this->session->userdata('logged_in'));

        // modelo para permiso perfil
        $this->load->model('back_office/cortafuego_model', 'cortafuego');
        $this->load->model('MY_Model', 'modelcore');
        $this->arr_cortaFuego = $this->cortafuego->get_arr_permiso(array('user_id' => $this->session->userdata('id_user'), 'user_perfil' => $this->session->userdata('id_perfil')));

        function in_array_r($needle, $haystack, $strict = false) {
            foreach ($haystack as $item) {
                if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_r($needle, $item, $strict))) {
                    return true;
                }
            }

            return false;
        }

    }


}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
