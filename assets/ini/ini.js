var dataResult;
$(document).ready(function() {    

    console.log('ini ready!');

    $("body").on('click', 'button', function() {

        if ($(this).data('ajax') == undefined) return;

        if ($(this).data('ajax') != true) return;

        var form = $(this).closest("form");
        var inputs = $("input", form);

        var button = $(this);
        var url = form.attr('action');
        

        if (button.data('confirm') != undefined) {
            if (button.data('confirm') == '') {
                if (!confirm('¿Are you sure to do it?')) return false;
            } else {
                if (!confirm(button.data('confirm'))) return false;
            }
        }

        var block = $('<div class="block-loading" />');
        form.prepend(block);

        $(".alert", form).remove();


        form.ajaxSubmit({
            dataType: 'JSON',
            type: 'POST',
            url: url,

            success: function(r) {
                block.remove();

                // deja en variable global todo el contenido de odin
                dataResult = r.result;

                if (r.response) {

                    if (!button.data('reset') != undefined) {
                        if (button.data('reset')) form.reset();
                    } else {
                        form.find('input:file').val('');
                    }
                }

                // Mostrar mensaje
                if (r.message != null) {
                    if (r.message.length > 0) {
                        showMessage(r.response, r.message, form);
                    }
                }

                // Ejecutar funciones
                if (r.function != null) {
                    setTimeout(r.function, 0);
                }

                // Redireccionar
                if (r.href != null) {
                    if (r.href == 'self') window.location.reload(true);
                    else window.location.href = r.href;
                }
            },
            error: function(re) {
                block.remove();
                var errorString = '';
       
                $.each(re.responseJSON.errors, function(key, value) {
                    errorString += '<li>' + value + '</li>';
                });

                form.prepend('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + errorString + '</b></div>');
                window.location.href = '#';
            }
        });

        return false;
    })
})

function showMessage(response, message, form) {
    var css = "alert-danger";
    if (response) css = "alert-success";
    var message = '<div class="alert ' + css + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + message + '</div>';
    form.prepend(message);
    // Remove div alert
    $('.alert-dismissable').delay(5000).queue(function(n) {
        $(this).remove();
        n();
    });
}

jQuery.fn.reset = function() {
    $("input:password,input:file,input:text,textarea", $(this)).val('');
    $("input:checkbox:checked", $(this)).click();
    $("select").each(function() {
        $(this).val($("option:first", $(this)).val());
    })
};