
$(document).ready(function () {

    $("#btn_editar_categoria").click(function () {
        if (validarRequeridoForm($("#frm_categoria_registro"))) {
            EditarCategoria();
        }
    });


    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });


    contarCaracteres("descripcion_categoria", "text-out", 200);
    $("#descripcion_categoria").trigger("change");



// $('#estado').val();
    var estado = document.getElementById("estado_categoria").value;

    if (estado == "1") {
        document.getElementById("estado").checked = true;
    } else {
        document.getElementById("estado").checked = false;
    }

    $("#estado").click(function () {
        var shekx = $('#estado').is(':checked');

        if (shekx) {
            $("#estado_categoria").val("1");
            $("#estado_label").html("Activado");
        } else {
            $("#estado_categoria").val("0");
            $("#estado_label").html("Desactivado");
        }

    });

    var estado = false;

    if ($("#estado_categoria").val() == 1) {
        estado = true;

    }


    $("#estado_categoria").prop("checked", estado);

});

// function checkIt() {
//     if(document.getElementById("estado").checked == true){
//         document.getElementById("estado").checked = false ;
//         document.getElementById("estado").value = "0";
//     }else{
//         document.getElementById("estado").checked = true ;
//         document.getElementById("estado").value = "1";
//     }



// }


function EditarCategoria() {

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_categoria_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Categoria', jqXHR.responseText, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "../EditarCategoria"
    });




}
