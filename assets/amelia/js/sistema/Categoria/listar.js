$(document).ready(function () {

    llamaDatosCategoria();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

});



function llamaDatosCategoria() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaCategoria(result);
        },
        url: "Listar/buscarCategoria"
    });
}



function cargaTablaCategoria(data) {

    if ($.fn.dataTable.isDataTable('#tbl_categoria')) {
        $('#tbl_categoria').DataTable().destroy();
    }

    $('#tbl_categoria').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_categoria"},
            {"mDataProp": "nombre_categoria"},
            {"mDataProp": "descripcion_breve_categoria"},
            {"mDataProp": "descripcion_categoria"},
            {"mDataProp": "estado_categoria"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    var x = "Editar/index/" + o.id_categoria;
                    html += "<li><a href='" + x + "'>Modificar</a></li>";
                    if (o.estado == 1) {
                        html += "<li><a    onclick='EditarCategoriaEstado(" + o.id_categoria + ",0);'>Desactivar</a></li>";
                    } else {
                        html += "<li><a    onclick='EditarCategoriaEstado(" + o.id_categoria + ",1);'>Activar</a></li> ";
                    }

                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Categoria'
            }, {
                extend: 'pdf',
                title: 'Categoria'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });



}

function EditarCategoriaEstado(id_categoria, tipo_estado) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id: id_categoria, estado: tipo_estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("CAtegoria", errorThrown, "error");
        },
        success: function (result) {

            llamaDatosCategoria();
        },
        url: "Listar/CambiarEstadoCategoria"
    });

}


// function cargaCombo(midata) {

//     $("#cbx_categoria").select2({
//         placeholder: "Todos",
//         allowClear: true,
//         data: midata
//     });


// }
