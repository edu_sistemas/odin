$(document).ready(function () {



    $('#tbl_perfil').DataTable({
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Perfiles'
            }, {
                extend: 'pdf',
                title: 'Perfiles'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]

    });

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

});

function Desactivar(id) {
// esta function esta de ejemplo : se reemprazo por un href dentro de la generacion de la tabla
    var resultado = "";
    $.ajax({
        url: "Listar/DesactivarPerfil",
        method: "POST",
        data: {id_perfil: id},
        dataType: "json",
        cache: false,
        type: "json",
        async: false,
        success: function (result) {
            // console.log(result);
        }
    });

    if (resultado != "Registro Exitoso") {
        alerta("Perfil", resultado, "error");
    } else {
        alerta("Perfil", resultado, "success");
        $("#btn_reset").trigger("click");
    }
}
