
$(document).ready(function () {
    $("#btn_registrar").click(function () {

        if (validarRequeridoForm($("#frm_perfil_registro"))) {
            RegistraPerfil();
        }

    });

    contarCaracteres("descripcion", "text-out", 200);
});

function RegistraPerfil() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_perfil_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Perfil', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }
        },
        url: "Agregar/RegistraPerfil"
    });

}
