$(document).ready(function () {
    recargaLaPagina();
    buscarFiltro();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_nuevo_contacto").click(function () {
        $("#div_frm_add").css("display", "block");
        $("#div_tbl_contacto_alumno").css("display", "none");
        $("#btn_nuevo_contacto").css("display", "none");
    });


    $("#btn_guardar_contacto").click(function () {
        if (validarRequeridoForm($("#frm_nuevo_contacto_alumno"))) {
            RegistarNuevoContacto();
        }
    });

    $("#btn_cancelar_contacto").click(function () {
        $("#div_frm_add").css("display", "none");
        $("#div_tbl_contacto_alumno").css("display", "block");
        $("#btn_nuevo_contacto").css("display", "block");
    });

    $("#btn_buscar").click(function () {
        buscarFiltro();
    });
});

function RegistarNuevoContacto() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    var url = "Listar/guardarDatosContacto";


    if ($("#btn_guardar_contacto").text() == "Actualizar") {
        url = "Listar/actualzarDatosContacto";
    }


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_nuevo_contacto_alumno').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Nuevo Contacto', errorThrown, 'error');
        },
        success: function (result) {

            //  alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {

                $("#div_frm_add").css("display", "none");
                $("#div_tbl_contacto_alumno").css("display", "block");
                $("#btn_nuevo_contacto").css("display", "block");

                ContactoAlumno($("#txt_id_alumno").val(), $("#modal_nombre_contacto").text());

                $("#btn_limpiar").click();
                $("#btn_guardar_contacto").text("Guardar");

            }
        },
        url: url
    });


}



function buscarFiltro() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#filtros_form').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Alumno", errorThrown, "error");
        },
        success: function (result) {
            cargaBusqueda(result);
        },
        url: "Listar/buscarPorFiltro"
    });

}


// Crea tabla del listado pro Jquery luis
function cargaBusqueda(data) {

    if ($.fn.dataTable.isDataTable('#tbl_alumnos')) {
        $('#tbl_alumnos').DataTable().destroy();
    }

    $('#tbl_alumnos').DataTable({
        "aaData": data,
        "aoColumns": [{
                "mDataProp": "rut_completo"
            }, {
                "mDataProp": "usuario_alumno"
            }, {
                "mDataProp": "nombre_alumno"
            }, {
                "mDataProp": "apellido_paterno_alumno"
            }, {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var nombre_completo = "";
                    var parametros = "";
                    nombre_completo = o.nombre_alumno + " " + o.apellido_paterno_alumno;
                    parametros = '"' + o.id_alumno + '","' + nombre_completo + '"';
                    html += o.contacto_alumno;
                    return html;
                }
            }
            , {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var nombre_completo = "";
                    var parametros = "";
                    nombre_completo = o.nombre_alumno + " " + o.apellido_paterno_alumno;
                    parametros = '"' + o.id_alumno + '","' + nombre_completo + '"';
                    html += "  <button class='btn btn-default' title='Cursos del  Alumno' onclick='CursosAlumno(" + parametros + ");' type='button'>&nbsp;&nbsp;<i class='fa fa-book'></i>&nbsp;&nbsp;&nbsp;</button>";
                    return html;
                }}
            , {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    var x = "Editar/index/" + o.id_alumno;
                    html += "<li><a href='" + x + "'>Modificar</a></li>";
                    if (o.estado == 1) {
                        html += "<li><a    onclick='EditarAlumnoEstado(" + o.id_alumno + ",2);'>Inactivo</a></li>";
                    } else {
                        html += "<li><a    onclick='EditarAlumnoEstado(" + o.id_alumno + ",1);'>Activo</a></li> ";
                    }

                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Perfiles'
            }, {
                extend: 'pdf',
                title: 'Perfiles'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

}

function cargaTblCursoAlumno(data) {

    if ($.fn.dataTable.isDataTable('#tbl_curso_s_alumno')) {
        $('#tbl_curso_s_alumno').DataTable().destroy();
    }

    $('#tbl_curso_s_alumno').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "Duración"},
            {"mDataProp": "Código sende"},
            {"mDataProp": "razon_social_empresa"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "fecha_fin"},
            {"mDataProp": "estado_curso"}

        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'cursos'
            }, {
                extend: 'pdf',
                title: 'cursos'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

}

function EditarAlumnoEstado(id_alumno, tipo_estado) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id: id_alumno, estado: tipo_estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Alumno", errorThrown, "error");
        },
        success: function (result) {

            buscarFiltro();
        },
        url: "Listar/CambiarEstadoAlumno"
    });

}

function ContactoAlumno(id, nombre_alumno) {
    $("#modal_nombre_curso_alumno").text(nombre_alumno);
    $("#txt_id_alumno").val(id);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_alumno: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Alumno", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosContactoAlumno(result);
            $("#modal_contacto_alummno").modal("show");
        },
        url: "Listar/verDatosContacto"
    });
}

function CursosAlumno(id, nombre_alumno) {
    $("#modal_nombre_curso_alumno").text(nombre_alumno);

    $("#txt_id_alumno").val(id);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_alumno: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Alumno", errorThrown, "error");
        },
        success: function (result) {
            cargaTblCursoAlumno(result);
            $("#modal_curso_alummno").modal("show");
        },
        url: "Listar/listaAlumnosCursos"
    });
}

function cargaDatosContactoAlumno(data) {
    var html = "";
    $('#tbl_contacto_alumno tbody').html(html);
    $.each(data, function (i, item) {
        html += '        <tr>';
        html += '        <td>' + item.correo_contacto + '</td> ';
        html += '        <td>' + item.telefono_contacto + '</td> ';
        html += '        <td>' + item.fecha_registro + '</td> ';
        html += '        <td>';

        if (item.estado_contacto == 1) {
            html += '<button class="btn btn btn-primary readonly btn-circle"  title="Activo" type="button"><i class="fa fa-check"></i></button>';
        } else {
            html += '<button class="btn btn-danger btn-circle" readonly title="Inactivo" type="button"><i class="fa fa-times"></i></button>';
        }

        html += '</td>';
        html += '<td>';

        if (item.estado_contacto == 1) {
            html += '<button class="btn btn-warning btn-circle btn-sm" type="button" title="desactivar" onclick="desactivarContactoAlumno(' + item.id_contacto_alumno + ',0)" ><i class="fa fa-minus-circle"></i></button>';
        } else {
            html += '<button class="btn btn-primary btn-circle btn-sm" type="button" title="Activar" onclick="desactivarContactoAlumno(' + item.id_contacto_alumno + ',1)" ><i class="fa fa-minus-circle"></i></button>';
        }

        var datos = "";
        datos = "'" + item.id_contacto_alumno + "','" + item.correo_contacto + "','" + item.telefono_contacto + "','" + item.id_alumno + "'";

        html += '<button onclick="ubicarCamposUpdateContactoAlumno(' + datos + ')" class="btn btn-info btn-circle btn-sm" type="button" title="Editar" ><i class="fa fa-edit"></i>  </button>';
        html += '<button   onclick="EliminarContactoAlumno(' + datos + ')" class="btn btn-danger btn-circle btn-sm" type="button" title="Elimiar" ><i class="fa fa-times-circle-o"></i>  </button>';
        html += '</td> ';
        html += '</tr>';
    });

    $('#tbl_contacto_alumno tbody').html(html);
}

function desactivarContactoAlumno(id_contacto_alumno, estatus) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto: id_contacto_alumno,
            estado: estatus
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Alumno", errorThrown, "error");
        },
        success: function (result) {

            ContactoAlumno($("#txt_id_alumno").val(), $("#modal_nombre_contacto").text());
        },
        url: "Listar/desactivarDatosContacto"
    });
}

function ubicarCamposUpdateContactoAlumno(id_contacto_alumno, correo_contacto, telefono_contacto, txt_id_alumno) {
    $("#div_frm_add").css("display", "block");
    $("#div_tbl_contacto_alumno").css("display", "none");
    $("#btn_nuevo_contacto").css("display", "none");
    $("#txt_id_contacto_alumno").val(id_contacto_alumno);
    $("#txt_id_alumno").val(txt_id_alumno);
    $("#txt_correo_contacto").val(correo_contacto);
    $("#txt_telefono").val(telefono_contacto);
    $("#btn_guardar_contacto").text("Actualizar");
}


function EliminarContactoAlumno(id_contacto_alumno, correo_contacto, telefono_contacto, id_alumno) {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto_alumno: id_contacto_alumno,
            correo_contacto: correo_contacto,
            telefono_contacto: telefono_contacto,
            id_alumno: id_alumno
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Alumno", errorThrown, "error");
        },
        success: function (result) {

            ContactoAlumno(id_alumno, $("#modal_nombre_contacto").text());
        },
        url: "Listar/eliminarDatosContacto"
    });

}

