
$(document).ready(function () {

    llamaDatosControl();
  
    $("#btn_editar_alumno").click(function () {
        if (validarRequeridoForm($("#frm_alumno_editar"))) {
            EditarAlm();
        }
    });


    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });

});

function llamaDatosControl(){
    var id_genero = $("#hidden_genero").val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);

            cargaCBXSelected("genero", result, id_genero);
        },
        url: "../getGeneroCBX"
    });

    var id_nivel_educacional = $("#hidden_nivel_educacional").val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);

            cargaCBXSelected("nivel_educacional", result, id_nivel_educacional);
        },
        url: "../getNivelEducacionalCBX"
    });
}

function EditarAlm() {

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success
    if ($("#genero").val() == '') {
        swal({
            title: "Genero",
            text: "Debe Seleccionar un genero",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('genero').focus();
            })
        return false;
    }

    if ($("#nivel_educacional").val() == '') {
        swal({
            title: "Nivel Educacional",
            text: "Debe Seleccionar un nivel educacional",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('nivel_educacional').focus();
            })
        return false;
    }
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_alumno_editar').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Categoria', jqXHR.responseText, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "../EditarAlumno"
    });




}
