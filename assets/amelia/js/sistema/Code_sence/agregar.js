
$(document).ready(function () {

    $("#btn_registrar_sence").click(function () {


        if (validarRequeridoForm($("#frm_sence_registro"))) {

            send();
            // setTimeout(function(){history.back();}, 3000);

        }

    });

    $('#btn_cerrarmodal_doc').click(function () {
        history.back();
    });


    llamaDatosModalidad();
    llamaDatosOtec();

    $('#data_1 input').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });


    $(".select2_demo_3").select2({
        placeholder: "Selecciona un curso",
        allowClear: true
    });

});


function send() {

    $.ajax({
        cache: false,
        async: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_sence_registro")[0]),
        error: function (jqXHR, textStatus, errorThrown) {

            //console.log(jqXHR);
            // console.log(textStatus);
            // console.log(errorThrown);

            //  console.log(jqXHR.responseText);

        },
        success: function (result) {
            // console.log(result);
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }

        },
        url: 'Agregar/AgregarSenceCode'
    });

}




function llamaDatosModalidad() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'id_modalidad_sence');
        },
        url: "Agregar/getModalidadCBX"
    });
}

function getModalidadCBX(midata) {

    cargaCBX("id_modalidad_sence", midata);

}


function llamaDatosOtec() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'otec');
        },
        url: "Agregar/getOtecCBX"
    });
}

function cargaCombo(midata, id) {

    cargaCBX(id, midata);

}

