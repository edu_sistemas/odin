$(document).ready(function () {
    llamaDatosSence();
    llamaDatosControles();
    llamaDatosNivel();
    llamaDatosVersion();
    $("#btn_buscar_codigo").click(function () {
        llamaDatosSence();
        llamaDatosControles();
        llamaDatosNivel();
        llamaDatosVersion();
    });
    $("#cbx_modalidad").select2({
        placeholder: "Todos",
        allowClear: true
    });

    $("#cbx_nivel").select2({
        placeholder: "Todos",
        allowClear: true
    });
    
     $("#cbx_version").select2({
        placeholder: "Todos",
        allowClear: true
    });
});

function llamaDatosSence() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            TablaSence(result);
        },
        url: "Listar_comercial/senceListar"
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            TablaSenceAlerta(result);
        },
        url: "Listar_comercial/senceListarAlerta"
    });
}

function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result);
        },
        url: "Listar_comercial/getSenceCbx" //MODALIDAD
    });
}

function cargaCombo(midata) {
    $("#cbx_modalidad").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    
   
}

function llamaDatosNivel() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo2(result);
        },
        url: "Listar_comercial/getNivelCbx"
    });
}

function cargaCombo2(midata) {
    $("#cbx_nivel").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    
 
}

function llamaDatosVersion() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo4(result);
        },
        url: "Listar_comercial/getVerCbx"
    });
}

function cargaCombo4(midata) {
    $("#cbx_version").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}

function TablaSence(data) {
    if ($.fn.dataTable.isDataTable('#tbl_sence')) {
        $('#tbl_sence').DataTable().destroy();
    }



    $('#tbl_sence').DataTable({
        "aaData": data,
        "aoColumns": [{
                "mDataProp": "codigo_sence"
            },
            {
                "mDataProp": "nombre_curso_code_sence"
            },
            {
                "mDataProp": "n_resolucion_excenta"
            },
            {
                "mDataProp": "cantidad_hora_sence"
            },

            {
                "mDataProp": "descripcion_version"
            },
            
            {
                "mDataProp": "nombre_nivel"
            },
            {
                "mDataProp": "nombre_modalidad"
            }, {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<a href="../../' + o.ruta_archivo + '" target="_blank">' + o.nombre_archivo + '</a>';
                    return html;
                }
            }


        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Sence'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }]
    });
}

function TablaSenceAlerta(data) {

    if ($.fn.dataTable.isDataTable('#tbl_sence_alerta')) {
        $('#tbl_sence_alerta').DataTable().destroy();
    }

    $('#tbl_sence_alerta').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "codigo_sence" },
            { "mDataProp": "nombre_curso_code_sence" },
            { "mDataProp": "fecha_solicitud" },
            { "mDataProp": "fecha_acreditacion" },
            { "mDataProp": "fecha_vigencia" },
            { "mDataProp": "n_resolucion_excenta" },
            { "mDataProp": "cantidad_hora_sence" },
            { "mDataProp": "valor_hora_sence" },
            { "mDataProp": "nombre_modalidad" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = "<a href='#' title='Subir o editar documentos' onclick='DocsSence(" + o.id_code_sence + ");'> " + o.cant_documento + " Doc.</a>";
                    return html;
                }
            },
            // {"mDataProp": "estado_code_sence"}
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    if (o.estado_code_sence == 'Activo') {
                        return ' <td><div class="infont col-md-3 col-sm-4"><a><i style="color:green" class="fa fa-check"></i></a></div>Activo</td>';
                    } else {
                        return '<td><div class="infont col-md-3 col-sm-4"><a><i style="color:red" class="fa fa-times"></i></a></div>Desactivo</td>';
                    }
                }
            },
            { "mDataProp": "vigencia_restante" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {

                    var html = "";

                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu pull-right'>";
                    var x = "Editar/index/" + o.id_code_sence;
                    html += "<li><a href='" + x + "'>Modificar</a></li>";
                    if (o.estado_code_sence == 'Activo') {
                        html += "<li><a    onclick='EditarsenceEstado(" + o.id_code_sence + ",0);'>Desactivar</a></li>";
                    } else {
                        html += "<li><a    onclick='EditarsenceEstado(" + o.id_code_sence + ",1);'>Activar</a></li> ";
                    }
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 4,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Sence'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        "order": []
    });
}