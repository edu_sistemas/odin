$(document).ready(function () {
    recargaLaPagina();
    llamaDatosSence();

    $("#btn_buscar").click(function () {
        llamaDatosSence();
    });

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#fileupload").click(function (event) {
        $(function () {
            'use strict';
            $('#fileupload').fileupload({
                url: "Consulta_sence/AgregarSenceCode",
                dataType: 'json',
                type: 'POST',
                cache: false,
                maxFileSize: 20000000,
                formData: {
                    id_code_sence: $("#modal_id_sence").text()
                },
                done: function (e, data) {
                    // $('<p/>').text(data.files[0].name).appendTo('#files');
                    $("#modal_id_sence").text(data.result.id_code_sence);
                    DocsSence($("#modal_id_sence").text());
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width', progress + '%');
                    $('#porsncetaje_carga').text(progress + '%');
                }
            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
    });
    $("#btn_nuevo_contacto").click(function () {
        $("#div_frm_add").css("display", "block");
        $("#div_tbl_contacto_empresa").css("display", "none");
        $("#btn_nuevo_contacto").css("display", "none");
    });
});

function llamaDatosSence() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            TablaSence(result);
        },
        url: "Consulta_sence/senceListar"
    });
}

function EditarsenceEstado(id, estado) {
    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_code_sence: id, estado_code_sence: estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(  textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result);
            llamaDatosSence();
        },
        url: "Consulta_sence/EditarEstadoSence"
    });
}

function TablaSence(data) {

    if ($.fn.dataTable.isDataTable('#tbl_sence')) {
        $('#tbl_sence').DataTable().destroy();
    }

    $('#tbl_sence').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "codigo_sence"},
            {"mDataProp": "nombre_curso_code_sence"},
            {"mDataProp": "nombre_curso_edutecno"},
            {"mDataProp": "fecha_acreditacion"},
            {"mDataProp": "fecha_inicio_curso"},
            {"mDataProp": "fecha_cierre_curso"},
            {"mDataProp": "num_ficha"}
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Sence'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}



function DocsSence(id) {

    $("#modal_id_sence").text(id);
    $("#modal_docs_sence").show();

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_code_sence: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Sence", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosDocsSence(result, id);
            $("#modal_docs_sence").modal("show");
            $("#div_frm_add").css("display", "none");
            $("#div_tbl_contacto_empresa").css("display", "block");
            $("#btn_nuevo_contacto").css("display", "block");
        },
        url: "Consulta_sence/verDocsSence"
    });
}

function cargaDatosDocsSence(data, id) {


    var html = "";



    $('#modal_id_sence').text(data[0].id_code_sence);

    $('#tbl_docs_sence tbody').html(html);

    $.each(data, function (i, item) {
        html += '        <tr>';
        if (item.nombre_archivo % 2 == 0) {
            html += ' <td><a href="../../' + item.ruta_archivo + '" target="_blank">' + item.nombre_archivo.substring(0, item.nombre_archivo.length / 2) + '</a></td> ';
        } else {
            html += ' <td><a href="../../' + item.ruta_archivo + '" target="_blank">' + item.nombre_archivo.substring(0, (item.nombre_archivo.length / 2) + 0.5) + '...' + item.nombre_archivo.slice(-6) + '</a></td> ';
        }

        html += '   <td>' + item.fecha_registro + '</td> ';
        html += '<td>';
        var datos = "";
        datos = "'" + item.id_doc_sence + "','" + item.nombre_archivo + "','" + item.fecha_registro + "','" + id + "'";

        html += '</td> ';
        html += '</tr>';
    });
    $('#tbl_docs_sence tbody').html(html);
}
