
$(document).ready(function () {

    $("#btn_editar_sence").click(function () {
        if (validarRequeridoForm($("#frm_sence_editar"))) {
            EditarCodeSence();
        }
    });

    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });



    llamaDatosModalidad();
    llamaDatosOtec();


    $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true

    });

});


function EditarCodeSence() {

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_sence_editar').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(  textStatus + errorThrown);
            alerta('Sence', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }

        },
        url: "../EditarCodeSence"
    });

}


function llamaDatosModalidad() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCBXSelected2(result);
        },
        url: "../getModalidad"
    });
}

function cargaCBXSelected2(midata) {

    $("#id_modalidad_sence").select2({
        allowClear: true,
        data: midata
    });


    $("#id_modalidad_sence option[value='0']").remove();
    $("#id_modalidad_sence").val($("#cbx_selected2").val()).trigger("change");

}



function llamaDatosOtec() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCBXSelected3(result);
        },
        url: "../getOtec"
    });
}

function cargaCBXSelected3(midata) {

    $("#otec").select2({
        allowClear: true,
        data: midata
    });


    $("#otec option[value='0']").remove();
    $("#otec").val($("#cbx_selected3").val()).trigger("change");

}