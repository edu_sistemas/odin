
$(document).ready(function () {

    $("#frm_menu_registro").submit(function () {
        RegistraMenu();
        event.preventDefault();
       
    });


    llamaDatosControles();
    llamaDatosControlesEstado();
    llamaDatosControlesModulo();
});


function RegistraMenu() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_menu_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Menu', jqXHR.responseText, 'error');
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        history.back();
                    } 
                });

           /* alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }/*/
        },
        url: "Agregar/AgregarMenu"
    });
}

function llamaDatosControles() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargarcbxpadre(result);
        },
        url: "Agregar/getPadreCBX"
    });

}

function cargarcbxpadre(midata) {

    cargaCBX("fk_padre", midata);
    $("#fk_padre").append('<option value="0">Padre</option>');

}


function llamaDatosControlesEstado() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargarcbxestado(result);
        },
        url: "Agregar/getEstadoCBX"
    });
}

function cargarcbxestado(midata) {

    cargaCBX("fk_estado", midata);


}


function llamaDatosControlesModulo() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            getModuloCBX(result);
        },
        url: "Agregar/getModuloCBX"
    });
}

function getModuloCBX(midata) {

    cargaCBX("fk_modulo", midata);


}
