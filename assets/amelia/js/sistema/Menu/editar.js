
$(document).ready(function () {

    $("#btn_editar").click(function () {
        if (validarRequeridoForm($("#frm_menu_editar"))) {
            EditarMenu();
        }

    });

    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });


    llamaDatosControles();
    llamaDatosControlesEstado();
    llamaDatosControlesModulo();

});


function EditarMenu() {

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_menu_editar').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(  textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                cancelButtonText: "Cancelar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        history.back();
                    } 
                });

          /*  alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }/*/

        },
        url: "../EditarMenu"
    });
}



function llamaDatosControles() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);

            alerta('Menu', jqXHR.responseText, 'error');
        },
        success: function (result) {
           // $("#fk_padre").append('<option value="0">Padre</option>');
            cargaComboPadre("fk_padre", result, $("#fk_id_menu_item_padre_v").val());
            
        },
        url: "../getPadreCBX"
    });
}
function cargaComboPadre(idcbx, data, seleccionado) {
    var html = "";
     html += '<option value="0" >Padre</option>';
    $.each(data, function (i, item) {
        if (item.id == seleccionado) {
            html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '" selected>' + item.text + '</option>';

        } else {
            html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
        }
    });
    $("#" + idcbx).html(html);
}

function llamaDatosControlesEstado() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta('Menu', jqXHR.responseText, 'error');
        },
        success: function (result) {

            cargaCBXSelected("fk_estado", result, $("#fk_id_menu_item_estado_v").val());
        },
        url: "../getEstadoCBX"
    });
}



function llamaDatosControlesModulo() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta('Menu', jqXHR.responseText, 'error');
        },
        success: function (result) {

            cargaCBXSelected("fk_modulo", result, $("#fk_id_modulo_v").val());
        },
        url: "../getModuloCBX"
    });
}
