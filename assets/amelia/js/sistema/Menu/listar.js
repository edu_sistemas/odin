$(document).ready(function () {

    llamaDatosMenu();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

});


function llamaDatosMenu() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaMenu(result);
        },
        url: "Listar/menuListar"
    });
}

function cargaTablaMenu(data) {

    if ($.fn.dataTable.isDataTable('#tbl_menu')) {
        $('#tbl_menu').DataTable().destroy();
    }

    $('#tbl_menu').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_menu_item"},
            {"mDataProp": "nombre"},
            {"mDataProp": "titulo"},
            {"mDataProp": "url"},
            {"mDataProp": "icon"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {

                    if (o.estado == 'Activo') {
                        return ' <td><div class="infont col-md-3 col-sm-4"><a><i style="color:green" class="fa fa-check"></i></a></div>Habilitado</td>';
                    } else if (o.estado == 'Deshabilitado') {
                        return '<td><div class="infont col-md-3 col-sm-4"><a><i style="color:red" class="fa fa-times"></i></a></div>Deshabilitado</td>';
                    } else if (o.estado == 'Oculto') {
                        return '<td><div class="infont col-md-3 col-sm-4"><a><i style="color:yellow" class="fa fa-exclamation-triangle"></i></a></div>Oculto</td>';
                    } else {
                        return '<td><div class="infont col-md-3 col-sm-4"><a><i style="color:red" class="fa fa-minus-circle"></i></a></div>No Aplica</td>';
                    }
                }},
            {"mDataProp": "titulo"},

            {"mDataProp": null, "bSortable": false, "mRender": function (a) {


                    var html = "";

                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu pull-right'>";
                    var x = "Editar/index/" + a.id_menu_item;
                    html += "<li><a href='" + x + "'>Modificar</a></li>";

                    html += "</ul>";
                    html += "</div>";


                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });

}



