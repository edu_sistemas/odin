$(document).ready(function () {

    $('#btn_aprobar_carga').prop("disabled", true);
    $('#btn_cancelar').prop("disabled", true);
    $("#btn_carga").click(function () {
        if (validarRequeridoForm($("#frm_carga_orden_compra"))) {
            send_file();
        }
    });
    $("#btn_cancelar").click(function () {
        swal({
            title: "Estas Seguro?",
            text: "Eliminará todos los registros previamente cargados",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy Seguro!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                limpia_carga();
                mostrar_lista_carga_empresa();
            }
        });
    });
    $("#btn_aprobar_carga").click(function () {


        if (validarRequeridoForm($("#frm_carga_orden_compra"))) {
            swal({
                title: "Estas Seguro?",
                text: "Solo se cargaran los registros que no contiene errores",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Si, Estoy Seguro!',
                cancelButtonText: "Cancelar!",
                showLoaderOnConfirm: true,
                preConfirm: function (email) {

                    return new Promise(function (resolve, reject) {

                        setTimeout(function () {
                            aceptar_carga();
                            resolve();
                        }, 2000);
                    });
                },
                allowOutsideClick: false
            }).then(function (email) {
                resetForm();
                swal({
                    title: "Orden de Compra",
                    text: "Alumnos Actualizados!",
                    type: "success"
                });
            })
        }

    });


});
function resetForm() {

    $("#cbx_orden_compra").val('').change();
    $("#btn_cleanr_file").trigger("click");
    $('#btn_aprobar_carga').prop("disabled", true);
    $('#btn_cancelar').prop("disabled", true);
}


function send_file() {

    $('#btn_carga').prop("disabled", true);
    // $('#file_carga_empresa').prop("disabled", true);
    limpia_carga();
    $('#btn_carga').html('<i class="fa fa-upload"></i> Cargando..');
    var l = $('#btn_carga').ladda();
    l.ladda('start');

    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_carga_orden_compra")[0]),
        error: function (jqXHR, textStatus, errorThrown) {

            /* console.log(jqXHR);
             console.log(textStatus);
             console.log(errorThrown);*/

            alerta('Alumnos', jqXHR.responseText, 'error');
            // console.log(jqXHR.responseText);


        },
        success: function (result) {
            l.ladda('stop');
            $('#btn_carga').prop("disabled", false);
            $('#btn_carga').html('<i class="fa fa-upload"></i> Subir ');
            if (result.valido) {
                mostrar_lista_carga_orden_compra();
                $('#btn_aprobar_carga').prop("disabled", false);
                $('#btn_cancelar').prop("disabled", false);
                $('#file_carga_empresa').prop("disabled", false);
            } else {
                alerta('Orden de Compra', result.mensaje, 'error');
            }
        },
        url: '../cargar_archivo_datos_alumnos'
    });
}


function limpia_carga()
{

    /*
     
     Descripcion : Limpia la tabla temporal antes de cargar el archivo excel
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (result) {

        },
        url: "../limpiar_carga_anterior"
    });
}

function  mostrar_lista_carga_orden_compra() {

    /*Muestra el docuemento cargado con mensaje dependiendo del tipo de validacion */

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Orden de Compra', jqXHR.responseText, 'error');
        },
        success: function (result) {

            cagar_tabla_temporal(result);
        },
        url: "../listar_carga_orden_de_compra"
    });
}


function cagar_tabla_temporal(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut_alumno"},
            {"mDataProp": "nombre_alumno"},
            {"mDataProp": "apellido_paterno"},
            {"mDataProp": "porcentaje_franquicia"},
            {"mDataProp": "costo_otic"},
            {"mDataProp": "costo_empresa"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "mensaje"}
        ],
        pageLength: 25,
        responsive: true

    });
}


function aceptar_carga() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#frm_carga_orden_compra").serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Rectificación', jqXHR.responseText, 'error');
        },
        success: function (result) {

            limpia_carga();
            mostrar_lista_carga_orden_compra();
        },
        url: '../aceptar_carga_orden_compra_alumnos'
    });

}


 