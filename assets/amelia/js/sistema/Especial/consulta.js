$(document).ready(function () {
    cargaTodosDatosExtras();
    $('[data-toggle="tooltip"]').tooltip();

    $("#btn_buscar_ficha").click(function () {
        llamaDatosFicha();
    });

    $("#btn_limpiar_form").click(function () {
        location.reload();
    });

    $("#holding").change(function () {
        var id_holding = $('#holding').val();
        getEmpresaByHolding(id_holding);
    });

    $('#data_5 .input-daterange').datepicker({
        format: "dd-mm-yyyy",
        language: 'es',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    llamaDatosFicha();
});


function  cargaTodosDatosExtras() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            cargaComboGenerico(result, 'ejecutivo');
        },
        url: "Consulta/getEjecutivo"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            cargaComboGenerico(result, 'periodo');
        },
        url: "Consulta/getPeriodo"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result);
        },
        url: "Consulta/getEstadoCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo2(result);
        },
        url: "Consulta/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo3(result);
        },
        url: "Consulta/getHoldingCBX"
    });

}
function cargaComboGenerico(midata, id) {

    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

    $("#" + id + " option[value='0']").remove();

}

function llamaDatosFicha() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaFicha(result);
        },
        url: "Consulta/consultarFicha"
    });


}

function getEmpresaByHolding(id_holding) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            cargaCombo2(result);
        },
        url: "Consulta/getEmpresaByHoldingCBX"
    });
}

function cargaTablaFicha(data) {


    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }

    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "ejecutivo"},
            {"mDataProp": "categoria"},
            {"mDataProp": "modalidad"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += o.fecha_inicio + '-' + o.fecha_fin;
                    return html;
                }
            },
            {"mDataProp": "descripcion_producto"},
            {"mDataProp": "otic"},
            {"mDataProp": "empresa"},
            {"mDataProp": "estado"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '<p data-toggle="tooltip" data-placement="top" title data-original-title="Sence: ' + o.valor_sence + ' • Empresa: ' + o.valor_empresa + '" >' + o.valor_total + '</p>';
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = "<a href='#' title='Subir o editar documentos' onclick='DocumentosFicha(" + o.id_ficha + "," + o.id_estado + ");'>" + o.cant_documento + "</a>";
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = " Colo car POP UP";
                    html = "<a href='#' title='Ver Alumnos' onclick='AlumnosFicha(" + o.id_ficha + ");'>" + o.cant_alumno + "</a>";
                    html = o.cant_alumno;
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {

                    var html = "";
                    var boton = "class='btn btn-warning dropdown-toggle'";

                    if (o.id_estado == 30 || o.id_estado == 50) {
                        boton = "class='btn btn-danger dropdown-toggle'";
                    }
                    if (o.id_estado == 20) {
                        boton = "class='btn btn-secondary dropdown-toggle'";
                    }
                    if (o.id_estado == 60) {
                        boton = "class='btn btn-primary dropdown-toggle'";
                    }

                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' " + boton + ">Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";

                    if (o.id_estado == 10 || o.id_estado == 30 || o.id_estado == 50) {
                        html += '<li><a href="#"  onclick="validaPreEnvioFicha(' + o.id_ficha + ')"   >Enviar</a></li>';
                    }

                    var x = "Editar/index/" + o.id_ficha;
                    var y = "OrdenCompra/index/" + o.id_ficha;
                    var z = "EditarArriendo/index/" + o.id_ficha;
                    var w = "Rectificacion/index/" + o.id_ficha;

                    var extencionplazo = "ExtensionPlazo/index/" + o.id_ficha;

                    var a = "Resumen/index/" + o.id_ficha;
                    var b = "ResumenArriendo/index/" + o.id_ficha;

                    if (o.id_categoria == 1) {
                        html += "<li><a href='" + a + "'>Resumen</a></li>";
                    }
                    if (o.id_categoria == 2) {
                        html += "<li><a href='" + b + "'>Resumen</a></li>";
                    }
//                    if (o.id_estado == 10 || o.id_estado == 30 || o.id_estado == 50) {
//                        if (o.id_categoria == 1) {
//                            html += "<li><a href='" + x + "'>Editar</a></li>";
//                        }
//
//                        if (o.id_categoria == 2) {
//                            html += "<li><a href='" + z + "'>Editar</a></li>";
//                        }
//                        html += '<li class="divider"></li>';
//                    }

                   

                    if (o.id_categoria == 1 && (o.id_estado == 10)) {
                        html += "<li><a href='" + y + "'>Cargar Alumnos</a></li>";
                    }

                    if (o.id_categoria == 1 && (o.id_estado >= 60)) {
                        html += "<li><a href='" + w + "'>Rectificación</a></li>";
                    //    html += "<li><a href='" + extencionplazo + "'>Extender Plazo</a></li>";
                    }


                    if (o.id_estado == 10) {
                        html += '<li><a href="#" onclick="validacionEliminar(' + o.id_ficha + ')"   >Eliminar</a></li>';
                    }

                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaCombo(midata) {
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true,
        data: midata
    });

    $("#estado option[value='0']").remove();
}

function cargaCombo2(midata) {
    $('#empresa').html('').select2({data: [{id: '', text: ''}]});
    $("#empresa").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#empresa option[value='0']").remove();
}

function cargaCombo3(midata) {

    $("#holding").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#holding option[value='0']").remove();

}
  