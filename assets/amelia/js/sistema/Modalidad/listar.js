$(document).ready(function () {

    llamaDatosModalidad();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

});

function llamaDatosModalidad() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaModalidad(result);
        },
        url: "Listar/modalidadListar"
    });
}

function EditarModalidadEstado(id, estado) {
    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_modalidad: id, estado_modalidad: estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(  textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result);
            llamaDatosModalidad();
        },
        url: "Listar/EditarModalidadEstado"
    });
}

function cargaTablaModalidad(data) {

    if ($.fn.dataTable.isDataTable('#tbl_modalidad')) {
        $('#tbl_modalidad').DataTable().destroy();
    }

    $('#tbl_modalidad').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_modalidad"},
            {"mDataProp": "nombre_modalidad"},
            {"mDataProp": "descripcion_modalidad"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    if (o.estado_modalidad == 'Activo') {
                        return ' <td><div class="infont col-md-3 col-sm-4"><a><i style="color:green" class="fa fa-check"></i></a></div>Habilitada</td>';
                    } else {
                        return '<td><div class="infont col-md-3 col-sm-4"><a><i style="color:red" class="fa fa-times"></i></a></div>Deshabilitada</td>';
                    }
                }},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {

                    var html = "";

                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu pull-right'>";
                    var x = "Editar/index/" + o.id_modalidad;
                    html += "<li><a href='" + x + "'>Modificar</a></li>";
                    if (o.estado_modalidad == 'Activo') {
                        html += "<li><a    onclick='EditarModalidadEstado(" + o.id_modalidad + ",0);'>Desactivar</a></li>";
                    } else {
                        html += "<li><a    onclick='EditarModalidadEstado(" + o.id_modalidad + ",1);'>Activar</a></li> ";
                    }
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Modalidad'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}
