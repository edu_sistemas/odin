$(document).ready(function () {
    llamaDatosSalas(); 
    getEjecutivos();
    getReservas();
    
    $("#btn_nuevo").click(function () {
        nuevaReserva();
    });

    var $eventcbx_reserva = $("#cbx_reserva");
    $eventcbx_reserva.on("change", function (e) {
        datosReserva($("#cbx_reserva").select2("val"));
    });
    
    var $eventcbx_sala = $("#cbx_sala");
    $eventcbx_sala.on("change", function (e) {
        $('#idSala').val($("#cbx_sala").select2("val"));
        var indexp = $("#cbx_sala").select2("val");
        if (indexp == 23 || indexp == 24 || indexp == 30 || indexp == 31 || indexp == 16) {
            $('#ldireccion').show();
            $('#ddireccion').show();
        } else {
            $('#ldireccion').hide();
            $('#ddireccion').hide();
        }
    });
    
    var $eventcbx_ejecutivo = $("#cbx_ejecutivo");
    $eventcbx_ejecutivo.on("change", function (e) {
        datosHolding($("#cbx_ejecutivo").select2("val"));
    });
    
    var $eventcbx_holding = $("#cbx_holding");
    $eventcbx_holding.on("change", function (e) {
        datosEmpresas($("#cbx_holding").select2("val"));
    });
    
    $(".form_datetime").datetimepicker({
        language: 'es',
        format: "dd-mm-yyyy hh:ii:ss",
        autoclose: true,
        todayBtn: true,
        startDate: "2013-02-14 10:00:00",
        minuteStep: 15
    });
    
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    
    $('#datepicker2').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    
    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });
    
    $('#ldireccion').hide();
    $('#ddireccion').hide();
    
    //Cambia valores de los iCheck de días de la semana Lunes
    $('#lunes').on('ifChecked', function (event) {
        $('#lunes').val("1");
    });
    
    $('#lunes').on('ifUnchecked', function (event) {
        $('#lunes').val("0");
    });
    //Fin lunes
    
    //Cambia valores de los iCheck de días de la semana Martes
    $('#martes').on('ifChecked', function (event) {
        $('#martes').val("2");
    });
    
    $('#martes').on('ifUnchecked', function (event) {
        $('#martes').val("0");
    });
    //Fin martes
    
    //Cambia valores de los iCheck de días de la semana miercoles
    $('#miercoles').on('ifChecked', function (event) {
        $('#miercoles').val("3");
    });
    
    $('#miercoles').on('ifUnchecked', function (event) {
        $('#miercoles').val("0");
    });
    //Fin miercoles
    
    //Cambia valores de los iCheck de días de la semana jueves
    $('#jueves').on('ifChecked', function (event) {
        $('#jueves').val("4");
    });
    
    $('#jueves').on('ifUnchecked', function (event) {
        $('#jueves').val("0");
    });
    //Fin jueves
    
    //Cambia valores de los iCheck de días de la semana viernes
    $('#viernes').on('ifChecked', function (event) {
        $('#viernes').val("5");
    });
    
    $('#viernes').on('ifUnchecked', function (event) {
        $('#viernes').val("0");
    });
    //Fin viernes
    
    //Cambia valores de los iCheck de días de la semana sabado
    $('#sabado').on('ifChecked', function (event) {
        $('#sabado').val("6");
    });
    
    $('#sabado').on('ifUnchecked', function (event) {
        $('#sabado').val("0");
    });
    //Fin sabado
    
    //Cambia valores de los iCheck de días de la semana domingo
    $('#domingo').on('ifChecked', function (event) {
        $('#domingo').val("7");
    });
    
    $('#domingo').on('ifUnchecked', function (event) {
        $('#domingo').val("0");
    });
    //Fin Domingo
    resetForm(0);
});

function resetForm(ciclo) {
    $("#nombre_ejecutivo").css("display", "none").prop('readonly', true);
    $("#nombre_empresa").css("display", "none").prop('readonly', true);
    $("#nombre_holding").css("display", "none").prop('readonly', true);
    $("#cbx_holding").css("display", "block");
    $("#cbx_empresa").css("display", "block");
    $("#from_reserva").css("display", "block");
    $("#btn_reservar").show();
    $("#cbx_ejecutivo").next(".select2-container").show();
    $("#cbx_holding").next(".select2-container").show();
    $("#cbx_empresa").next(".select2-container").show();
    $("#nombre_reserva").val('').prop('readonly', false);
    $("#detalles_reserva").val('').prop('readonly', false);
    $("#direccion").val('').prop('readonly', false);
    $("#breakSala").val('').prop('readonly', false);
    $("#computadores").val('').prop('readonly', false);
    $("#fecha_inicio").datepicker('setDate', null);
    $("#fecha_termino").datepicker('setDate', null);
    $("#txt_hora_inicio").val('09:30');
    $("#txt_hora_termino").val('09:30');

    if (ciclo != 0) {
        if ($('#cbx_sala').select2("val") != '') {
            $("#cbx_sala").val('').trigger('change');
        }
        if ($('#cbx_ejecutivo').select2("val") != '') {
            $("#cbx_ejecutivo").val('').trigger('change');
        }
        if ($('#cbx_reserva').select2("val") != '') {
            $("#cbx_reserva").val('').trigger('change');
        }
        /*     if ($('#cbx_holding').select2("val") != '') {
                 $("#cbx_holding").val('').trigger('change');
             }
             if ($('#cbx_empresa').select2("val") != '') {
                 $("#cbx_empresa").val('').trigger('change');
             }*/

    }
    //estado inicial 
    $('#lunes').iCheck('uncheck', function () {
        $('#lunes').val("0");
    });
    $('#martes').iCheck('uncheck', function () {
        $('#martes').val("0");
    });
    $('#miercoles').iCheck('uncheck', function () {
        $('#miercoles').val("0");
    });
    $('#jueves').iCheck('uncheck', function () {
        $('#jueves').val("0");
    });
    $('#viernes').iCheck('uncheck', function () {
        $('#viernes').val("0");
    });
    $('#sabado').iCheck('uncheck', function () {
        $('#sabado').val("0");
    });

    $('#domingo').iCheck('uncheck', function () {
        $('#domingo').val("0");
    });
}

function llamaDatosSalas() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            getSalaidCBX(result);
            cargaCombo(result, 'sala');
        },
        url: "Agregar/getSalas"
    });
}

function datosReserva(dato) {
    $("#idReserva").val(dato);
    if (dato > 0) {
        $.ajax({
            //url: '../../buscarReservaById/'+dato.value,	
            //   url: '../Reserva/AgregarAdm/buscarReservaById/' + dato,
            //url: '../Reserva/Agregar/getDetalleReserva/',
            url: '../Reserva/Agregar/getReserva/',
            dataType: "json",
            type: "POST",
            data: {
                id_reserva: dato,

            },
            success: function (json) {
                //$.each(json, function (id, value) {
                //alert(value.id);
                $("#nombre_reserva").val(json[0].nombre_reserva).prop('readonly', true);
                $("#ejecutivo").val(json[0].ejecutivo);
                $("#cbx_ejecutivo").next(".select2-container").hide();
                $("#cbx_holding").css("display", "none");
                $("#cbx_holding").next(".select2-container").hide();
                $("#cbx_empresa").css("display", "none");
                $("#cbx_empresa").next(".select2-container").hide();
                $("#nombre_ejecutivo").val(json[0].nombre_ejecutivo).prop('readonly', true).css("display", "block");
                $("#nombre_empresa").val(json[0].nombre_empresa).prop('readonly', true).css("display", "block");
                $("#nombre_holding").val(json[0].nombre_holding).prop('readonly', true).css("display", "block");
                $("#detalles_reserva").val(json[0].detalle_reserva);
                $("#direccion").val(json[0].direccion);
                // $("#detalles_reserva").val(json.detalle_reserva).prop('readonly', true);
                $("#breakSala").val(json[0].break);
                $("#computadores").val(json[0].computadores);
                // });
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    } else if (dato == -1 || dato == '') {
        $("#nombre_ejecutivo").css("display", "none");
        $("#nombre_empresa").css("display", "none");
        $("#nombre_holding").css("display", "none");
        $("#cbx_holding").css("display", "block");
        $("#cbx_empresa").css("display", "block");
        $("#cbx_ejecutivo").next(".select2-container").show();
        $("#cbx_holding").next(".select2-container").show();
        $("#cbx_empresa").next(".select2-container").show();
        $("#cbx_ejecutivo").val('').trigger('change');
        $("#nombre_reserva").val('').prop('readonly', false);
        $("#detalles_reserva").val('').prop('readonly', false);
        $("#direccion").val('').prop('readonly', false);
        $("#breakSala").val('').prop('readonly', false);
        $("#computadores").val('').prop('readonly', false);
    }
}

function datosHolding(dato, select, id_empresa) {
    $("#cbx_holding").html('');
    if (dato != null && dato != '') {
        $.ajax({
            url: '../Reserva/Agregar/buscarHoldingById/' + dato,
            dataType: "json",
            type: "POST",
            success: function (json) {
                cargaCombo(json, 'holding');
                if (select > 0) {
                    $("#cbx_holding").val(select);
                    $("#cbx_holding").trigger('change');
                    //  $("#cbx_holding").trigger('change').select2("enable", false);

                } else {
                    $("#cbx_holding").val('');
                }
                datosEmpresas(select, id_empresa);
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    } else {
        $("#cbx_empresa").html('');
    }
}
function datosEmpresas(dato, select) {
    $("#cbx_empresa").html('');
    if (dato != null && dato != '') {
        $.ajax({
            //url: '../../buscarReservaById/'+dato.value,	
            url: '../Reserva/Agregar/buscarEmpresasById/' + dato,
            dataType: "json",
            type: "POST",
            data: {
                id_ejecutivo: $("#cbx_ejecutivo").select2("val")
            },
            success: function (json) {
                cargaCombo(json, 'empresa');
                if (select >= 0) {
                    $("#cbx_empresa").val(select);
                    //  $("#cbx_empresa").select2("enable", false);
                } else {
                    $("#cbx_empresa").val('');
                }
                $("#cbx_empresa").trigger('change');
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }
}

function nuevaReserva() {
    resetForm();
    $("#saladef").css("display", "none");
    $("#salanodef").css("display", "block");
    $("#bt_editar").css("display", "none");
    $("#myModal4").modal();

}

function getSalaidCBX(midata) {
    cargaCBX("id_sala_v", midata);
}

function desplegarCalendario() {
    $('#calendar').fullCalendar('destroy');
    var g = $('#id_sala_v').find(":selected").attr('value');
    $('#idSala').attr('value', g);
    $('#numSala').val($('#id_sala_v').find(":selected").attr('id'));
    
    var indexp = g;

    if (indexp == 23 || indexp == 24 || indexp == 30 || indexp == 31 || indexp == 16) {
        $('#ldireccion').show();
        $('#ddireccion').show();
    } else {
        $('#ldireccion').hide();
        $('#ddireccion').hide();
    }

    //alert("4454sd");
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('#calendar').fullCalendar({
        editable: false,
        slotDuration: {
            minutes: 15
        },
        weekends: true,
        allDaySlot: false,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        //events: "../Reserva/Agregar/llenarCalendar/" + g,
        events: "../Reserva/Agregar/llenarCalendar/" + g,
        eventColor: '#19aa8d',
        // Convert the allDay from string to boolean
        eventSources: [{
            url: "../Reserva/Agregar/getFeriados"
            //url: "/odin/assets/amelia/js/sistema/Reserva/feriados2016.json" // url to get holiday events
        }
            // any other sources...
        ],
        eventRender: function (event, element, view) {
            // lets test if the event has a property called holiday. 
            // If so and it matches '1', change the background of the correct day
            if (event.holiday == '1') {
                var dateString = event.start.format("YYYY-MM-DD");
                $(view.el[0]).find('.fc-day[data-date=' + dateString + ']')
                    .css('background-color', '#F5DEDE');
            }
            if (event.title == 'Feriado') {
                $("#fc-content").css("background-color", "#F6EBEB");
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            getDetallesReserva(calEvent.id_reserva, calEvent.id_detalle_sala);
        },
        eventConstraint: {
            start: moment().format('YYYY-MM-DD'),
            end: '2100-01-01' // hard coded goodness unfortunately
        },
        selectable: true,
        selectHelper: false,
        select: function (start, end, allDay) {
            var band = false;
            var x = moment(start).format('YYYYMMDD');
            var xy = moment(start).format('YYYY-MM-DD HH:mm:ss');
            //alert(xy);
            $.ajax({
                //  url: "../../assets/amelia/js/sistema/Reserva/feriados2016.json",
                url: "../Reserva/Agregar/getFeriados",
                dataType: 'json',
                async: false,
                success: function (data) {
                    $.each(data, function (key, val) {
                        if (val.start == xy) {
                            band = true;
                            //alert('ola');
                        }
                    });
                }
            });

            var date = new Date();
            var dd = date.getDate();
            var mm = date.getMonth() + 1; //sumar 1
            var yy = date.getFullYear();
            if (mm < 10) {
                mm = 0 + '' + mm;
            }
            if (dd < 10) {
                dd = 0 + '' + dd;
            }
            var concat = yy + '' + mm + '' + dd; // trae la suma del actual
            console.log(band);
            if (band) {
                swal({
                    title: "Reserva",
                    text: "No puede reservar un día Feriado",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                })
            } else {
                resetForm();

                var g = $('#id_sala_v').find(":selected").attr('value');
                $('#idSala').attr('value', g);
                
               // $("#btn_reservar").hide();
                $("#myModal4").modal();
                $("#saladef").css("display", "block");

                $("#salanodef").css("display", "none");
                $("#bt_editar").css("display", "none");
                $("#fecha_inicio").val($.fullCalendar.moment(start).format('DD-MM-YYYY'));

            }
        }
    });
}

function getDetallesReserva(id, id_detalle) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_reserva: id,
            id_detalle_sala: id_detalle
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Reserva', errorThrown, 'error');
        },
        success: function (result) {
            //cierra modal	
            $("#from_reserva").css("display", "none");
            $("#saladef").css("display", "block");
            $("#salanodef").css("display", "none");
            $("#btn_reservar").hide();
            $("#myModal4").modal();
            //vacia todos los campos del modal
            //document.getElementById('usuarioComercial').value = 0;
            document.getElementById('fecha_inicio').value = result.fechainicio;
            document.getElementById('fecha_termino').value = result.fechatermino;
            document.getElementById('nombre_reserva').value = result.nombre_reserva;
            document.getElementById('detalles_reserva').value = result.detalle_reserva;
            document.getElementById('direccion').value = result.direccion;
            document.getElementById('id_reserva').innerText = result.id_reserva;
            // document.getElementById('direccion').value = '';
            document.getElementById('breakSala').value = result.break;
            document.getElementById('computadores').value = result.computadores;

            document.getElementById('idReserva').value = result.id_reserva;

            $("#ejecutivo").val(result.ejecutivo);
            $("#cbx_ejecutivo").next(".select2-container").hide();
            $("#cbx_holding").css("display", "none");
            $("#cbx_holding").next(".select2-container").hide();
            $("#cbx_empresa").css("display", "none");
            $("#cbx_empresa").next(".select2-container").hide();
            $("#nombre_ejecutivo").val(result.nombre_ejecutivo).prop('readonly', true).css("display", "block");
            $("#nombre_empresa").val(result.nombre_empresa).prop('readonly', true).css("display", "block");
            $("#nombre_holding").val(result.nombre_holding).prop('readonly', true).css("display", "block");

            var indexp = result.id_sala;
            
            if (indexp == 23 || indexp == 24 || indexp == 30 || indexp == 31 || indexp == 16) {
                $('#ldireccion').show();
                $('#ddireccion').show();
            } else {
                $('#ldireccion').hide();
                $('#ddireccion').hide();
            }

            $('#txt_hora_inicio').val(result.horainicio);
            $('#txt_hora_termino').val(result.horatermino);

            if (result.lunes == 1) {
                $('#lunes').iCheck('check');
                $('#lunes').val("1");
            } else {
                $('#lunes').iCheck('uncheck'); 
                $('#lunes').val("0");
            }

            if (result.martes == 1) {
                $('#martes').iCheck('check');
                $('#martes').val("2");
            } else {
                $('#martes').iCheck('uncheck');
                $('#martes').val("0");
            }
            if (result.miercoles == 1) {
                $('#miercoles').iCheck('check');
                $('#miercoles').val("3");
            } else {
                $('#miercoles').iCheck('uncheck');
                $('#miercoles').val("0");
            }

            if (result.jueves == 1) {
                $('#jueves').iCheck('check')
                $('#jueves').val("4");
            } else {
                $('#jueves').iCheck('uncheck')
                $('#jueves').val("0");
            }

            if (result.viernes == 1) {
                $('#viernes').iCheck('check')
                $('#viernes').val("5");
            } else {
                $('#viernes').iCheck('uncheck')
                $('#viernes').val("0");
            }

            if (result.sabado == 1) {
                $('#sabado').iCheck('check')
                $('#sabado').val("6");
            } else {
                $('#sabado').iCheck('uncheck')
                $('#sabado').val("0");
            }

            if (result.domingo == 1) {
                $('#domingo').iCheck('check')
                $('#domingo').val("7");
            } else {
                $('#domingo').iCheck('uncheck')
                $('#domingo').val("0");
            }

            desplegarCalendario();
        },
        url: "Agregar/getDetalleReserva"
    });
}

function setFecha(input) {
    var fecha = input.split("-");
    return fecha[2] + "-" + fecha[1] + "-" + fecha[0];
}

function validaFormModal(metodo) {
    var finicio = document.getElementById('fecha_inicio').value;
    var ftermino = document.getElementById('fecha_termino').value;

    var hinicio = document.getElementById('txt_hora_inicio').value;
    var htermino = document.getElementById('txt_hora_termino').value;

    var lunes = $('#lunes').val();
    var martes = $('#martes').val();
    var miercoles = $('#miercoles').val();
    var jueves = $('#jueves').val();
    var viernes = $('#viernes').val();
    var sabado = $('#sabado').val();
    var domingo = $('#domingo').val();

    var fRegistro = setFecha(document.getElementById('fRegistro').value);
    var idSala = document.getElementById('idSala').value;
    var idReserva = $('#cbx_reserva').select2("val");
    if (idReserva == -1) {
        var idEjecutivo = $('#cbx_ejecutivo').select2("val");
        var idHolding = $('#cbx_holding').select2("val");
        var idEmpresa = $('#cbx_empresa').select2("val");
    }
    var nombreReserva = document.getElementById('nombre_reserva').value;
    var detalleReserva = document.getElementById('detalles_reserva').value;
    var direccion = document.getElementById('direccion').value;
    var breakSala = document.getElementById('breakSala').value;
    var computadores = document.getElementById('computadores').value;

    var res = finicio.split("-");
    finicio = res[2] + '-' + res[1] + '-' + res[0];
    res = ftermino.split("-");
    ftermino = res[2] + '-' + res[1] + '-' + res[0];

    var f = new Date();

    var mes = (f.getMonth() + 1);

    if (mes < 10) {
        mes = "0" + mes;
    }
    var dia = f.getDate();
    if (dia < 10) {
        dia = "0" + dia;
    }

    var f_actual = f.getFullYear() + "-" + mes + "-" + dia;

    if (finicio < f_actual) {
        swal({
            title: "Fecha",
            text: "Fecha Inicio no puede ser anterior a la fecha Actual",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
        function () {
            swal.close();
            document.getElementById('fecha_inicio').focus();
        })
        return false;
    }

    if (finicio > ftermino) {
        swal({
            title: "Fecha",
            text: "Fecha Inicio no puede ser mayor a la fecha de  fecha Termino",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
        function () {
            swal.close();
            document.getElementById('fecha_inicio').focus();
        })
        return false;
    }

/** validacion de horas */
    if (hinicio >= htermino) {
        swal({
            title: "Reserva",
            text: "La hora de inicio no puede ser igual o mayor ha la hora de termino",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('txt_hora_termino').focus();
            })
        return false;
    }

    if (hinicio == '') {
        swal({
            title: "Reserva",
            text: "Debe ingresar hora de inicio",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('txt_hora_inicio').focus();
            })
        return false;
    }

    if (htermino == '') {
        swal({
            title: "Reserva",
            text: "Debe ingresar hora de termino",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('txt_hora_termino').focus();
            })
        return false;
    }

    if (idReserva == 0 || idReserva == '') {
        swal({
            title: "Reserva",
            text: "Debe Seleccionar una Nueva Reserva o una ya existente",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('cbx_reserva').focus();
            })
        return false;
    }

    if (idEjecutivo == 0 || idEjecutivo == '') {
        swal({
            title: "Reserva",
            text: "Debe Seleccionar un Ejecutivo",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('cbx_ejecutivo').focus();
            })
        return false;
    }
    if (idHolding == '') {
        swal({
            title: "Reserva",
            text: "Debe Seleccionar un Holding",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('cbx_holding').focus();
            })
        return false;
    }

    if (idEmpresa == '') {
        swal({
            title: "Reserva",
            text: "Debe Seleccionar una empresa",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('cbx_empresa').focus();
            })
        return false;
    }
    if (nombreReserva == '') {
        swal({
            title: "Reserva",
            text: "Debe ingresar un nombre para la Reserva",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('nombre_reserva').focus();
            })
        return false;
    } 
    if (idSala == '') {
        swal({
            title: "Reserva",
            text: "Debe Ingresar una sala para la reserva",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('cbx_sala').focus();
            })
        return false;
    }
    if (detalleReserva == '') {
        swal({
            title: "Reserva",
            text: "Debe Ingresar un detalle para la reserva",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('detalles_reserva').focus();
            })
        return false;
    }
    if (breakSala == '') {
        swal({
            title: "Reserva",
            text: "Debe ingresar el detalle del break de la sala",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('breakSala').focus();
            })
        return false;
    }
    if (computadores == '') {
        swal({
            title: "Reserva",
            text: "Debe ingresar el detalle de material de apoyo",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('computadores').focus();
            })
        return false;
    }

    if ((idSala == 23 || idSala == 24 || idSala == 30 || idSala == 31 || idSala == 16) && direccion == '') {
        swal({
            title: "Reserva",
            text: "Debe ingresar la dirección de la reserva",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('direccion').focus();
            })
        return false;
    }

    //insertar capsula
    //insertar disponibilidad detalle
    //validar si ya existe curso a esa hora
    if(idSala == 23 || idSala == 24 || idSala == 30 || idSala == 31 || idSala == 16){
        insertCapsula();

    } else {
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: $('#formModal').serialize(),
            url: '../Reserva/Agregar/existeDisponibilidad/',
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                // console.log(  textStatus + errorThrown);
                alerta('Reserva', errorThrown, 'error');
            },
            success: function (returnedData) {
                if (returnedData[0].v_id_capsula > 0) {
                    swal({
                        title: "Reserva",
                        text: "Exite una reserva en el horario que intenta Reservar",
                        type: "warning",
                        confirmButtonColor: "#DD6B55",
                        closeOnConfirm: false
                    })
                    return false;
                }
                insertCapsula();
            }
        });
    }
}

function insertCapsula() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#formModal').serialize(),
        url: "Agregar/insertReservaCpasula",
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Reserva', errorThrown, 'error');
        },
        success: function (result) {
            swal({
                title: "Reserva",
                text: "Capsula Insertada correctamente",
                type: "success",
                confirmButtonColor: "#69dd54",
                closeOnConfirm: false
            })
            enviaCorreo(result[0].v_id_reserva, result[0].v_id_detalle_sala);
            //cierra modal	
            $("#myModal4").modal('hide');
            //vacia todos los campos del modal
            resetForm();
            getReservas();
            desplegarCalendario();
        }
    });
}

function getEjecutivos() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "Agregar/getUsuarioTipoEcutivoComercial"
    });
}

function getReservas() {
    $.ajax({
        url: '../Reserva/Agregar/listaReservasbyUser/',
        dataType: "json",
        type: "POST",
        success: function (json) {
            cargaCombo(json, "reserva");
        },
        error: function (msg) {
            console.log(msg);
        }
    });
}

function cargaCombo(midata, id) {
    $("#cbx_" + id).html('');
    $("#cbx_" + id).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
}

function enviaCorreo(reserva, detalle_sala) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_reserva: reserva,
            id_detalle_sala: detalle_sala
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //   history.back();
            // cargaTablaFicha(result);
        },
        url: "Agregar/enviaEmail"
    });
}
