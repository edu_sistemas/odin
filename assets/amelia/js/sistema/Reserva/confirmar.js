$(document).ready(function () {
    $('#id_empresa span').css('z-index: 1 !important;');

    llamaDatosReservaSala();
    llamaDatosSalas();
    llamaDatosEmpresa();

    $("#btn_buscar_empresa").click(function () {
        llamaDatosReservaSala();
    });

    $("#btn_confirmar_bloque").click(function () {
        ConfirmarBloqueModal($("#id_reserva").val(), $("#id_detalle_sala").val());
    });
    $("#btn_confirmar_reserva").click(function () {
        ConfirmarReservaModal($("#id_reserva").val());
    });
    $("#btn_confirmar_reserva").click(function () {
        ConfirmarReservaModal($("#id_reserva").val());
    });

    $("#btn_rechazar").click(function () {
        modalRechazarReserva($("#id_reserva").val());
    });

    $("#btn_rechazar_reserva").click(function () {
        if($("#motivo_rechazo").val().length =="") {
            swal("Atencion", "Debe ingresar motivo del rechazo de la reserva", "info");
            return false;
        }else{
            RechazarReserva($("#id_reserva").val(), $("#motivo_rechazo").val());
        }
    });

    $("#btn_rechazar_bloque_reserva").click(function () {
        if($("#motivo_rechazo_bloque").val().length =="") {
            swal("Atencion", "Debe ingresar motivo del rechazo del bloque", "info");
            return false;
        }else{
            //console.log($("#id_reserva").val() + 'detallesal' + $("#id_detalle_sala").val() + ' ' +$("#motivo_rechazo_bloque").val());
            RechazarBloqueReserva($("#id_reserva").val(), $("#id_detalle_sala").val(), $("#motivo_rechazo_bloque").val());
        }
    });

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    recargaLaPagina();
    $('#fechas_reserva').hide();
    $('#fechas_reserva2').hide();
});

// carga el cbx para la busqueda de empresas
function llamaDatosEmpresa() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, 'id_empresa');
        },
        url: "Confirmar/getEmpresas"
    });
}

function cargaCombo(midata, item) {
    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
}

function llamaDatosReservaSala() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_fichas').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            TablaReservas(result);
        },
        url: "Confirmar/ListarReservas"
    });
}

function TablaReservas(data) {
    if ($.fn.dataTable.isDataTable('#tbl_reservas')) {
        $('#tbl_reservas').DataTable().destroy();
    }
    $('#tbl_reservas').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "id_reserva" },
            { "mDataProp": "id_detalle_sala" },
            { "mDataProp": "nombre_reserva" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "fecha_termino" },
            { "mDataProp": "hora_inicio" },
            { "mDataProp": "hora_termino" },
            { "mDataProp": "dia" },
            { "mDataProp": "sala" },
            { "mDataProp": "holding" },
            { "mDataProp": "empresa" },
            { "mDataProp": "ejecutivo" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu pull-right'>";
                    var x = o.id_reserva;
                    var y = o.id_detalle_sala;
                    html += "<li><a onclick='modalDetalleReserva(" + x + ',' + y + ")'>Ver Detalle</a></li>";
                    html += "<li><a onclick='ConfirmarBloque(" + x + ',' + y + ")'>Confirmar Bloque</a></li>";
                    html += "<li><a onclick='ConfirmarReserva(" + x + ")'>Confirmar Reserva</a></li>";
                    html += "<li><a onclick='modalRechazarBloqueReserva(" + x + ',' + y + ")'>Rechazar Bloque de Reserva</a></li>";
                    html += "<li><a onclick='modalRechazarReserva(" + x + ")'>Rechazar Reserva</a></li>";
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        "order": [
            [0, 'desc']
        ],
        pageLength: 20,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Fichas'
        }, {
            extend: 'pdf',
            title: 'Fichas'
        }, {
            extend: 'print',
            customize: function (win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }
        }]
    });
}

//Carga ek cbx de salas para cargar el calendar
function llamaDatosSalas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            getSalaidCBX(result);
        },
        url: "Confirmar/getSalas"
    });
}

function modalDetalleReserva(id_reserva, id_detalle_sala) {
    $("#modalConfirmacion").modal();
    llamaDatosControlesReserva(id_reserva, id_detalle_sala);
    $('#id_reserva').val(id_reserva);
    $('#id_detalle_sala').val(id_detalle_sala);
}

function llamaDatosControlesReserva(id_reserva, id_detalle_sala) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        data: {
            id_reserva: id_reserva,
            id_detalle_sala: id_detalle_sala
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            $("#nombre_reserva").val(result.nombre_reserva);
            $("#estado").text("Estado Reserva: "+result.estado);
            $("#fecha_registro").val(result.fecha_registro);
            $("#detalle_reserva").val(result.detalle_reserva);
            $("#direccion").val(result.direccion);
            $("#break").val(result.break);
            $("#computadores").val(result.computadores);
            $("#fecha_inicio").val(result.fecha_inicio);
            $("#fecha_termino").val(result.fecha_termino);
            $('#hora_inicio').val(result.hora_inicio);
            $('#hora_termino').val(result.hora_termino);
            $("#sala").val(result.sala);
            $("#holding").val(result.holding);
            $("#empresa").val(result.empresa);
            $("#ejecutivo").val(result.ejecutivo);
            
            if (result.dia == 1) {
                $('#lunesS').iCheck('check');
                $('#lunesS').val("1");
            } else {
                $('#lunesS').iCheck('uncheck');
                $('#lunesS').val("0");
            }

            if (result.martes == 1) {
                $('#martesS').iCheck('check');
                $('#martesS').val("2");
            } else {
                $('#martesS').iCheck('uncheck');
                $('#martesS').val("0");
            }

            if (result.miercoles == 1) {
                $('#miercolesS').iCheck('check');
                $('#miercolesS').val("3");
            } else {
                $('#miercolesS').iCheck('uncheck');
                $('#miercolesS').val("0");
            }

            if (result.jueves == 1) {
                $('#juevesS').iCheck('check')
                $('#juevesS').val("4");
            } else {
                $('#juevesS').iCheck('uncheck')
                $('#juevesS').val("0");
            }

            if (result.viernes == 1) {
                $('#viernesS').iCheck('check')
                $('#viernesS').val("5");
            } else {
                $('#viernesS').iCheck('uncheck')
                $('#viernesS').val("0");
            }

            if (result.sabado == 1) {
                $('#sabadoS').iCheck('check')
                $('#sabadoS').val("6");
            } else {
                $('#sabadoS').iCheck('uncheck')
                $('#sabadoS').val("0");
            }

            if (result.domingo == 1) {
                $('#domingoS').iCheck('check')
                $('#domingoS').val("7");
            } else {
                $('#domingoS').iCheck('uncheck')
                $('#domingoS').val("0");
            }
            var estado = $('#estado').text();

            if(estado == "Estado Reserva: Sin Confirmar" ){
                $("#btn_confirmar_bloque").show();
                $("#btn_confirmar_reserva").show();
                $("#btn_rechazar").show();
            }
            if (estado == "Estado Reserva: Confirmada" ) {
                $("#btn_confirmar_bloque").hide();
                $("#btn_confirmar_reserva").hide();
                $("#btn_rechazar").hide();
            } 
            
            if (estado == "Estado Reserva: Rechazada") {
                $("#btn_confirmar_bloque").hide();
                $("#btn_confirmar_reserva").hide();
                $("#btn_rechazar").hide();
            }
        },
        url: "Confirmar/getReservaPorConf"
    });
}

function ConfirmarBloque(id_reserva, id_detalle_sala) {
    confirmarBloqueReserva(id_reserva, id_detalle_sala);
    $('#id_reserva').val(id_reserva);
    $('#id_detalle_sala').val(id_detalle_sala);

}

function confirmarBloqueReserva(id_reserva, id_detalle_sala) {
    swal({
        title: "Bloque de la Reserva",
        text: "¿Desea Confirmar bloque?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirmar Bloque",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'post',
                data: {
                    id_reserva: id_reserva,
                    id_detalle_sala: id_detalle_sala
                },
                error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    swal({
                        title: result[0].titulo,
                        text: result[0].mensaje,
                        type: result[0].tipo_alerta,
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        closeOnConfirm: true
                    },
                    function () {
                        llamaDatosReservaSala();
                        sendMailBloqueConfirmado(id_reserva, id_detalle_sala);
                            if($("#calendar").children().length>0){
                                desplegarCalendario();
                            }
                    });
                },
                url: "Confirmar/getConfirmarBloque"
            });
        } else {
            swal("Cancelado", "La confirmación del bloque fue cacelada", "error");
        }
    });
}

function ConfirmarBloqueModal(id_reserva, id_detalle_sala) {
    confirmarBloqueReservaModal(id_reserva, id_detalle_sala);
    $('#id_reserva').val(id_reserva);
    $('#id_detalle_sala').val(id_detalle_sala);

}
function confirmarBloqueReservaModal(id_reserva, id_detalle_sala) {
    swal({
        title: "Bloque de la Reserva",
        text: "¿Desea Confirmar bloque?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirmar Bloque",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'post',
                data: {
                    id_reserva: id_reserva,
                    id_detalle_sala: id_detalle_sala
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    swal({
                        title: result[0].titulo,
                        text: result[0].mensaje,
                        type: result[0].tipo_alerta,
                        showCancelButton: false,
                        confirmButtonText: "Aceptar",
                        closeOnConfirm: true
                        },
                    function () {
                        llamaDatosReservaSala();
                        sendMailBloqueConfirmado(id_reserva, id_detalle_sala);
                        if($("#calendar").children().length>0){
                            desplegarCalendario();
                        }
                        $('#modalConfirmacion').modal('toggle');
                        });
                },
                url: "Confirmar/getConfirmarBloque"
            });
        } else {
            swal("Cancelado", "La confirmación del bloque fue cacelada", "error");
        }
    });
}

function  sendMailBloqueConfirmado(id_reserva, id_detalle_sala) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "text",
        type: 'POST',
        data:  {
            id_reserva: id_reserva,
            id_detalle_sala: id_detalle_sala 
        },
        error: function (jqXHR, textStatus, errorThrown) {
           alerta("ERROR", jqXHR.responseText, "error");
        },
        success: function (result) {
            alerta('Validación', 'Se ha enviado una notifiación con su solicitud', 'success');          
        },
        url:  "Confirmar/enviaEmailBloqueConfirmado"
    });
}

function ConfirmarReserva(id_reserva) {
    confirmarReservaCompleta(id_reserva);
    $('#id_reserva').val(id_reserva);
}

function confirmarReservaCompleta(id_reserva) {
    swal({
        title: "Reserva",
        text: "¿Desea Confirmar Reserva?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: 'post',
                    data: {
                        id_reserva: id_reserva,
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // code en caso de error de la ejecucion del ajax
                        console.log(textStatus + errorThrown);
                    },
                    success: function (result) {
                        swal({
                            title: result[0].titulo,
                            text: result[0].mensaje,
                            type: result[0].tipo_alerta,
                            showCancelButton: false,
                            confirmButtonText: "Aceptar",
                            closeOnConfirm: true
                        },
                        function () {
                            llamaDatosReservaSala();
                            sendMailConfirmada(id_reserva);
                        });
                    },
                    url: "Confirmar/getConfirmarReserva"
                    });
                } else {
                    swal("Cancelado", "La confirmación de la reserva fue cacelada", "error");
                }
    });
}

function ConfirmarReservaModal(id_reserva) {
    confirmarReservaCompletaModal(id_reserva);
    $('#id_reserva').val(id_reserva);
}

function confirmarReservaCompletaModal(id_reserva) {
    swal({
        title: "Reserva",
        text: "¿Desea Confirmar Esta reserva?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
    },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: 'post',
                    data: {
                        id_reserva: id_reserva,
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // code en caso de error de la ejecucion del ajax
                        console.log(textStatus + errorThrown);
                    },
                    success: function (result) {
                        swal({
                            title: result[0].titulo,
                            text: result[0].mensaje,
                            type: result[0].tipo_alerta,
                            showCancelButton: false,
                            confirmButtonText: "Aceptar",
                            closeOnConfirm: true
                        },
                        function () {
                            llamaDatosReservaSala();
                            $("#modalConfirmacion").modal("toggle");
                        });
                        sendMailConfirmada(id_reserva);
                    },
                    url: "Confirmar/getConfirmarReserva"
                });
            } else {
                swal("Cancelado", "La confirmación de la reserva fue cacelada", "error");
            }
        });
}
function  sendMailConfirmada(id_reserva) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "text",
        type: 'POST',
        data:  {
            id_reserva: id_reserva
        },
        error: function (jqXHR, textStatus, errorThrown) {
           alerta("ERROR", jqXHR.responseText, "error");
        },
        success: function (result) {
            alerta('Validación', 'Se ha enviado una notifiación con su solicitud', 'success');          
        },
        url:  "Confirmar/enviaEmailReservaConfirmada"
    });
}

function modalRechazarReserva(id_reserva) {
    $("#motivo_rechazo").val("");
    $("#modalRechazarReserva").modal();
    $('#id_reserva').val(id_reserva);
}

function RechazarReserva(id_reserva, motivo_rechazo ) {
   $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        data:  {
            id_reserva: id_reserva,
            motivo_rechazo: motivo_rechazo
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
            function () {
                llamaDatosReservaSala();
                if($("#calendar").children().length>0){
                    desplegarCalendario();
                }
                $('#modalRechazarReserva').modal('toggle');
                if($('#modalConfirmacion').is(':visible')){
                    $('#modalConfirmacion').modal('toggle');
                }
                sendMailRechazo(id_reserva);
            });
        },
        url: "Confirmar/RechazarReserva"
    });
} 

function  sendMailRechazo(id_reserva) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "text",
        type: 'POST',
        data:  {
            id_reserva: id_reserva
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alerta("ERROR", jqXHR.responseText, "error");
        },
        success: function (result) {
            alerta('Validación', 'Se ha enviado una notifiación con su solicitud', 'success');          
        },
        url:  "Confirmar/enviaEmailRechazo"
    });
}

function modalRechazarBloqueReserva(id_reserva, id_detalle_sala) {
    $("#motivo_rechazo_bloque").val("");
    $("#modalRechazarBloqueReserva").modal();
    $('#id_reserva').val(id_reserva);
    $('#id_detalle_sala').val(id_detalle_sala);
}

function RechazarBloqueReserva(id_reserva, id_detalle_sala, motivo_rechazo_bloque ) {
   $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        data:  {
            id_reserva: id_reserva,
            id_detalle_sala: id_detalle_sala,
            motivo_rechazo_bloque: motivo_rechazo_bloque
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
            function () {
                llamaDatosReservaSala();
                if($("#calendar").children().length>0){
                    desplegarCalendario();
                }
                $('#modalRechazarBloqueReserva').modal('toggle');
                if($('#modalConfirmacion').is(':visible')){
                    $('#modalConfirmacion').modal('toggle');
                }
                //sendMailRechazo(id_reserva);
            });
        },
        url: "Confirmar/RechazarBloqueReserva"
    });
} 

//----------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------
function desplegarCalendario() {
    $('#calendar').fullCalendar('destroy');
    var g = $('#id_sala_v').find(":selected").attr('value');
    $('#idSala').attr('value', g);
    $('#numSala').val($('#id_sala_v').find(":selected").attr('id'));
    $('#numSala2').val($('#id_sala_v').find(":selected").attr('value')); //Rescata el id de la sala para ser usado en el form de confirmación
    $('#numSala22').val($('#id_sala_v').find(":selected").attr('value')); //Rescata el id de la sala para ser usado en el form de confirmación
    $.ajax({
        url: '../Reserva/Confirmar/listaReservasbyUser/',
        dataType: "json",
        type: "POST"
    });
    //alert("4454sd");
   
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('#calendar').fullCalendar({
        editable: true,
        slotDuration: {
            minutes: 15
        },
        weekends: true,
        allDaySlot: false,
        eventStartEditable: false,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: "../Reserva/Confirmar/llenarCalendar/" + g,
        eventColor: '#1674E9',
        // Convert the allDay from string to boolean
        eventSources: [{
            url: "../Reserva/Agregar/getFeriados"
            //  url: "../../assets/amelia/js/sistema/Reserva/feriados2016.json" // url to get holiday events
        }
            // any other sources...
        ],
        eventRender: function (event, element, view) {
            // lets test if the event has a property called holiday. 
            // If so and it matches '1', change the background of the correct day
            if (event.holiday == '1') {
                var dateString = event.start.format("YYYY-MM-DD");
                $(view.el[0]).find('.fc-day[data-date=' + dateString + ']')
                    .css('background-color', '#F5DEDE');
            }
        },
        eventConstraint: {
            start: moment().format('YYYY-MM-DD'),
            end: '2100-01-01' // hard coded goodness unfortunately
        },
        eventClick: function (calEvent, jsEvent, view) {
            var x = moment(event.start).format('YYYYMMDD');
            var y = moment(calEvent.start).format('YYYYMMDD');
            var ttl = (calEvent.title);
            if (x > y || ttl == 'Feriado') {
                if (ttl == 'Feriado') {
                    var text = "!No puede confirmar un día feriado!"
                } else {
                    var text = "!No puede confirmar un día anterior al día actual!"
                }
                swal({
                    title: "Reserva",
                    text: text,
                    type: "warning",
                    confirmButtonColor: "#DD6B55"
                })
            } else {
                llamaDatosControlesReserva(calEvent.id_reserva, calEvent.id_detalle_sala);
                $('#id_reserva').val(calEvent.id_reserva);
                $('#id_detalle_sala').val(calEvent.id_detalle_sala); 
                $("#modalConfirmacion").modal();

                
            $('#dia_resrv').val(moment(calEvent.start).format('DD')); // Recupera el numero del dia que se quiere coonfirmar la reserva
            }
        }
    });
}
//-----------------------------------------------------------------------------------------------------------------------------------------------
function cargaCombo3(midata) {
    $('#cbx_reservas_sala').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{
            id: '',
            text: ''
        }]
    });
    $("#cbx_reservas_sala").select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata,
        hideSearch: false
    });
}

function getSalaidCBX(midata) {
    cargaCBX("id_sala_v", midata);
}




