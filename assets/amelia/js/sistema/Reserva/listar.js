
$(document).ready(function () {
    recargaLaPagina();
    llamaDatosSalas();

});


function llamaDatosSalas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            getSalaidCBX(result);
        },
        url: "Listar/getSalas"
    });
}

function getSalaidCBX(midata) {


    cargaCBX("id_sala_v", midata);

}

function desplegarCalendario() {
    $('#calendar').fullCalendar('destroy');
    var g = $('#id_sala_v').find(":selected").attr('value');

    //alert("4454sd");
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    var calendar = $('#calendar').fullCalendar({
        editable: false,
        weekends: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },

        events: "../Reserva/Listar/llenarCalendar/" + g,
        eventColor: '#19aa8d',

        eventSources: [
            {
                url: "../Reserva/Listar/getFeriados" // url to get holiday events
            }
            // any other sources...

        ],
        eventRender: function (event, element, view) {
            // lets test if the event has a property called holiday. 
            // If so and it matches '1', change the background of the correct day
            if (event.holiday == '1') {
                var dateString = event.start.format("YYYY-MM-DD");

                $(view.el[0]).find('.fc-day[data-date=' + dateString + ']')
                        .css('background-color', '#F9E6E6');

            }

            if (event.title == 'Feriado') {
                $("#fc-content").css("background-color", "#D21A1A");

            }

        },
        eventClick: function (calEvent, jsEvent, view) {

            llamadatoFecha(calEvent.id);
            llamadoDetallesReserva(calEvent.id);
            cuentaalumnos(calEvent.id_ficha);
            $('#numSala').val(calEvent.nsala);
            $('#infoModal').modal();


        },
        eventConstraint: {
            start: moment().format('YYYY-MM-DD'),
            end: '2100-01-01' // hard coded goodness unfortunately
        }

    });
}

function llamadatoFecha(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result[0].horario_inicio);
            $("#horario_inicio").val(result[0].horario_inicio);
            $("#horario_termino").val(result[0].horario_termino);

            // console.log(result.horario_inicio);
        },
        url: "../Reserva/Visualizacion/getFechas/" + id
    });
}

function llamadoDetallesReserva(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result[0].horario_inicio);
            $("#nombre_reserva").val(result[0].nombre_reserva);
            $("#detalle_reserva").val(result[0].detalle_reserva);
            $("#direccion").val(result[0].direccion);
            $("#break").val(result[0].break);
            $("#computadores").val(result[0].computadores);
            $("#usuario_registro").val(result[0].usuario_registro);
            $("#ejecutivo").val(result[0].ejecutivo);
            // console.log(result);

            // llamaDatosControlesFicha(result[0].id_ficha);
        },
        url: "../Reserva/Visualizacion/getDetallesReserva/" + id
    });
}

function cuentaalumnos(nsala) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result[0].horario_inicio);
            $("#cantalm").val(result[0].cantidad);


            // console.log(result.horario_inicio);
        },
        url: "../Reserva/Visualizacion/getalmumnos/" + nsala
    });
}
