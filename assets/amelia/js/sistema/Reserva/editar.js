$(document).ready(function () {
    llamaDatosSalas();
    getEjecutivos();
    getReservas();

    $("#btn_editar").click(function () {
        editarReserva();
    });

    //$('#fRegistro').click(false);
    
    var $eventcbx_reserva = $("#cbx_reserva");
    $eventcbx_reserva.on("change", function (e) {
        datosReserva($("#cbx_reserva").select2("val"));
    });
    
    var $eventcbx_sala = $("#cbx_sala");
    $eventcbx_sala.on("change", function (e) {
        $('#idSala').val($("#cbx_sala").select2("val"));
    });
    
 /*   var $eventcbx_ejecutivo = $("#cbx_ejecutivo");
    $eventcbx_ejecutivo.on("change", function (e) {
        datosHolding($("#cbx_ejecutivo").select2("val"));
    });*/
    
    var $eventcbx_holding = $("#cbx_holding");
    $eventcbx_holding.on("change", function (e) {
        datosEmpresas($("#cbx_holding").select2("val"), '', $("#idEjecutivo").val());
    }); 
    
    var $eventcbx_bloque = $("#cbx_bloque");
    $eventcbx_bloque.on("change", function (e) {
        datosDetalleSala($("#cbx_bloque").select2("val"));
    });
    
    var $eventradio_edicion = $('input[name="edicion"]');

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    
    $eventradio_edicion.on('ifChanged', function (event) {
        if ($("input[name='edicion']:checked").val() == 1) {
            $('#datos_reserva').show();
            $('#detalles_bloque').hide();
            $('#detalles_horario').hide();
        } 
        if ($("input[name='edicion']:checked").val() == 2) {
            $('#datos_reserva').hide();
            $('#detalles_bloque').show();
            $('#detalles_horario').hide();
        }
        if ($("input[name='edicion']:checked").val() == 3) {
            $('#datos_reserva').hide();
            $('#detalles_bloque').hide();
            $('#detalles_horario').show();
        }
    });

    $(".form_datetime").datetimepicker({
        language: 'es',
        format: "dd-mm-yyyy hh:ii:ss",
        autoclose: true,
        todayBtn: true,
        startDate: "2013-02-14 10:00:00",
        minuteStep: 15
    });

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#datepicker2').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    $('#datepicker2S').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });

    $('#ldireccion').hide();
    $('#ddireccion').hide();
    $('#ldireccionS').hide();
    $('#ddireccionS').hide();

    //Cambia valores de los iCheck de días de la semana Lunes
    $('#lunes').on('ifChecked', function (event) {
        $('#lunes').val("1");
    });

    $('#lunes').on('ifUnchecked', function (event) {
        $('#lunes').val("0");
    });
    //Fin lunes

    //Cambia valores de los iCheck de días de la semana Martes
    $('#martes').on('ifChecked', function (event) {
        $('#martes').val("2");
    });

    $('#martes').on('ifUnchecked', function (event) {
        $('#martes').val("0");
    });
    //Fin martes

    //Cambia valores de los iCheck de días de la semana miercoles
    $('#miercoles').on('ifChecked', function (event) {
        $('#miercoles').val("3");
    });

    $('#miercoles').on('ifUnchecked', function (event) {
        $('#miercoles').val("0");
    });
    //Fin miercoles

    //Cambia valores de los iCheck de días de la semana jueves
    $('#jueves').on('ifChecked', function (event) {
        $('#jueves').val("4");
    });

    $('#jueves').on('ifUnchecked', function (event) {
        $('#jueves').val("0");
    });
    //Fin jueves

    //Cambia valores de los iCheck de días de la semana viernes
    $('#viernes').on('ifChecked', function (event) {
        $('#viernes').val("5");
    });

    $('#viernes').on('ifUnchecked', function (event) {
        $('#viernes').val("0");
    });
    //Fin viernes

    //Cambia valores de los iCheck de días de la semana sabado
    $('#sabado').on('ifChecked', function (event) {
        $('#sabado').val("6");
    });

    $('#sabado').on('ifUnchecked', function (event) {
        $('#sabado').val("0");
    });
    //Fin sabado

    //Cambia valores de los iCheck de días de la semana domingo
    $('#domingo').on('ifChecked', function (event) {
        $('#domingo').val("7");
    });

    $('#domingo').on('ifUnchecked', function (event) {
        $('#domingo').val("0");
    });
    //Fin Domingo
    resetForm(0);
    resetFormS(0);
});

function resetForm(ciclo) {
    $('#datos_reserva').hide();
    $('#detalles_bloque').hide();
    $('#detalles_horario').hide();
    $("#nombre_ejecutivo").val('');
   // $("#nombre_empresa").css("display", "none").prop('readonly', true);
   // $("#nombre_holding").css("display", "none").prop('readonly', true);
    $("#cbx_holding").css("display", "block");
    $("#cbx_empresa").css("display", "block");
    $("#from_reserva").css("display", "block");
   // $("#btn_reservar").show();
    $("#cbx_ejecutivo").next(".select2-container").show();
    $("#cbx_holding").next(".select2-container").show();
    $("#cbx_empresa").next(".select2-container").show();
    $("#nombre_reserva").val('').prop('readonly', false);
    $("#detalles_reserva").val('').prop('readonly', false);
    $("#direccion").val('').prop('readonly', false);
    $("#breakSala").val('').prop('readonly', false);
    $("#direccion").val('').prop('readonly', false);
    $("#computadores").val('').prop('readonly', false);
    $("#fecha_inicio").datepicker('setDate', null);
    $("#fecha_termino").datepicker('setDate', null);
    $("#txt_hora_inicio").val('09:30');
    $("#txt_hora_termino").val('09:30');
    $("#cbx_bloque").html('').prop('disabled', true);
    $("#cbx_holding").html('').prop('disabled', true);
    $("#cbx_empresa").html('').prop('disabled', true);
    $('input[name="edicion"]').iCheck('uncheck');
    if(ciclo != 0){
        console.log($('#cbx_sala').val());
        if ($('#cbx_sala').select2("val") != null) {
           $("#cbx_sala").val('').trigger('change');
        }
        console.log($('#cbx_reserva').val());
        if ($('#cbx_reserva').select2("val") != null) {
            $("#cbx_reserva").val('').trigger('change');
        }
    }
    //estado inicial 
    $('#lunes').iCheck('uncheck', function () {
        $('#lunes').val("0");
    });
    $('#martes').iCheck('uncheck', function () {
        $('#martes').val("0");
    });
    $('#miercoles').iCheck('uncheck', function () {
        $('#miercoles').val("0");
    });
    $('#jueves').iCheck('uncheck', function () {
        $('#jueves').val("0");
    });
    $('#viernes').iCheck('uncheck', function () {
        $('#viernes').val("0");
    });
    $('#sabado').iCheck('uncheck', function () {
        $('#sabado').val("0");
    });
    $('#domingo').iCheck('uncheck', function () {
        $('#domingo').val("0");
    });
}


function resetFormS(ciclo) {
    $("#nombre_ejecutivoS").css("display", "none").prop('readonly', true);
    $("#nombre_empresaS").css("display", "none").prop('readonly', true);
    $("#nombre_holdingS").css("display", "none").prop('readonly', true);
    $("#from_reservaS").css("display", "block");
   // $("#btn_reservarS").show();
    $("#nombre_reservaS").val('').prop('readonly', false);
    $("#detalles_reservaS").val('').prop('readonly', false);
    $("#direccionS").val('').prop('readonly', false);
    $("#breakSalaS").val('').prop('readonly', false);
    $("#computadoresS").val('').prop('readonly', false);
    $("#fecha_inicioS").datepicker('setDate', null);
    $("#fecha_terminoS").datepicker('setDate', null);
    $("#txt_hora_inicioS").val('09:30');
    $("#txt_hora_terminoS").val('09:30');

    //estado inicial 
    $('#lunesS').iCheck('uncheck', function () {
        $('#lunesS').val("0");
    });
    $('#martesS').iCheck('uncheck', function () {
        $('#martesS').val("0");
    });
    $('#miercolesS').iCheck('uncheck', function () {
        $('#miercolesS').val("0");
    });
    $('#juevesS').iCheck('uncheck', function () {
        $('#juevesS').val("0");
    });
    $('#viernesS').iCheck('uncheck', function () {
        $('#viernesS').val("0");
    });
    $('#sabadoS').iCheck('uncheck', function () {
        $('#sabadoS').val("0");
    });

    $('#domingoS').iCheck('uncheck', function () {
        $('#domingoS').val("0");
    });
}

function llamaDatosSalas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            getSalaidCBX(result);
            cargaCombo(result, 'sala');
        },
        url: "Editar/getSalas"
    });
}

function datosReserva(dato) {
    $("#idReserva").val(dato);
    if (dato > 0) {
        $.ajax({
            url: '../Reserva/Editar/getReserva/',
            dataType: "json",
            type: "POST",
            data: {
                id_reserva: dato,
            },
            success: function (json) {
                console.log(json);
                //$.each(json, function (id, value) {
                //alert(value.id);
                $("#nombre_reserva").val(json[0].nombre_reserva);
                $("#ejecutivo").val(json[0].ejecutivo);
                $("#idEjecutivo").val(json[0].id_ejecutivo);
                $("#nombre_ejecutivo").val(json[0].nombre_ejecutivo).prop('readonly', true).css("display", "block");
                datosHolding(json[0].id_ejecutivo, json[0].id_holding, json[0].id_empresa);
                $("#cbx_holding").prop('disabled', false);
                $("#cbx_empresa").prop('disabled', false);
                /*$("#cbx_holding").css("display", "none");
                $("#cbx_holding").next(".select2-container").hide();
                $("#cbx_empresa").css("display", "none");
                $("#cbx_empresa").next(".select2-container").hide();*/
                //$("#nombre_empresa").val(json[0].nombre_empresa).prop('readonly', true).css("display", "block");
                //$("#nombre_holding").val(json[0].nombre_holding).prop('readonly', true).css("display", "block");
                $("#cbx_bloque").html('').prop('disabled', false);
                cargaCombo(json, 'bloque');
                console.log(json.length);
                if (json.length == 1) {
                    $("#cbx_bloque").prop('disabled', true);
                    datosDetalleSala(json[0].id);
                    /* $("#cbx_sala").val(json[0].id_sala);
                    $("#cbx_sala").trigger('change');
                    $("#detalles_reserva").val(json[0].detalle_reserva);
                    $("#direccion").val(json[0].direccion);
                    $("#breakSala").val(json[0].break);
                    $("#computadores").val(json[0].computadores);*/
                } else {
                    datosDetalleSala(json[0].id);
                   /* $("#cbx_sala").val('').trigger('change');
                    $("#detalles_reserva").val('');
                    $("#direccion").val('');
                    $("#breakSala").val('');
                    $("#computadores").val(''); */
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    } else {
        resetForm();
    }
}

function datosDetalleSala(dato) {
    if (dato > 0) {
        $.ajax({
            url: '../Reserva/Editar/getDetalleReserva/',
            dataType: "json",
            type: "POST",
            data: {
                id_reserva: $("#cbx_reserva").select2("val"),
                id_detalle_sala: dato,
            },
            success: function (result) {
                $("#cbx_sala").val(result.id_sala).trigger('change');
                $("#detalles_reserva").val(result.detalle_reserva);
                $("#direccion").val(result.direccion);
                $("#breakSala").val(result.break);
                $("#computadores").val(result.computadores);
                document.getElementById('fecha_inicio').value = result.fechainicio;
                document.getElementById('fecha_termino').value = result.fechatermino;
                document.getElementById('id_reserva').innerText = result.id_reserva;

                var indexp = result.id_sala;
                console.log(indexp);
                if (indexp == 23 || indexp == 24 || indexp == 30 || indexp == 31 || indexp == 16) {
                    $('#ldireccion').show();
                    $('#ddireccion').show();
                } else {
                    $('#ldireccion').hide();
                    $('#ddireccion').hide();
                }

                $('#txt_hora_inicio').val(result.horainicio);
                $('#txt_hora_termino').val(result.horatermino);

                if (result.lunes == 1) {
                    $('#lunes').iCheck('check');
                    $('#lunes').val("1");
                } else {
                    $('#lunes').iCheck('uncheck');
                    $('#lunes').val("0");
                }

                if (result.martes == 1) {
                    $('#martes').iCheck('check');
                    $('#martes').val("2");
                } else {
                    $('#martes').iCheck('uncheck');
                    $('#martes').val("0");
                }
                if (result.miercoles == 1) {
                    $('#miercoles').iCheck('check');
                    $('#miercoles').val("3");
                } else {
                    $('#miercoles').iCheck('uncheck');
                    $('#miercoles').val("0");
                }

                if (result.jueves == 1) {
                    $('#jueves').iCheck('check')
                    $('#jueves').val("4");
                } else {
                    $('#jueves').iCheck('uncheck')
                    $('#jueves').val("0");
                }

                if (result.viernes == 1) {
                    $('#viernes').iCheck('check')
                    $('#viernes').val("5");
                } else {
                    $('#viernes').iCheck('uncheck')
                    $('#viernes').val("0");
                }

                if (result.sabado == 1) {
                    $('#sabado').iCheck('check')
                    $('#sabado').val("6");
                } else {
                    $('#sabado').iCheck('uncheck')
                    $('#sabado').val("0");
                }

                if (result.domingo == 1) {
                    $('#domingo').iCheck('check')
                    $('#domingo').val("7");
                } else {
                    $('#domingo').iCheck('uncheck')
                    $('#domingo').val("0");
                }

            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }
}

function datosHolding(idEjecutivo, idHolding, idEmpresa) {
    console.log(idEjecutivo + 'holding' + idHolding);
    $("#cbx_holding").html('');
    if (idEjecutivo != null && idEjecutivo != '') {
        $.ajax({
            url: '../Reserva/Editar/buscarHoldingById/' + idEjecutivo,
            dataType: "json",
            type: "POST",
            success: function (json) {
                cargaCombo(json, 'holding');
                if (idHolding > 0) {
                    $("#cbx_holding").val(idHolding);
                    $("#cbx_holding").trigger('change');
                    //  $("#cbx_holding").trigger('change').select2("enable", false);
                    
                } else {
                    $("#cbx_holding").val('');
                    $("#cbx_holding").trigger('change');
                }
                datosEmpresas(idHolding, idEmpresa, idEjecutivo);
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    } else {
        $("#cbx_empresa").html('');
    }
}

function datosEmpresas(idHolding, idEmpresa, idEjecutivo) {
    console.log(idHolding + 'empresa' + idEmpresa + 'ejecutivo ' + idEjecutivo);
    $("#cbx_empresa").html('');
    if (idHolding != null && idHolding != '') {
        $.ajax({
            url: '../Reserva/Editar/buscarEmpresasById/' + idHolding,
            dataType: "json",
            type: "POST",
            data: {
                id_ejecutivo: idEjecutivo
            },
            success: function (json) {
                cargaCombo(json, 'empresa');
                if (idEmpresa >= 0) {
                    $("#cbx_empresa").val(idEmpresa);
                    $("#cbx_empresa").trigger('change');
                    //  $("#cbx_empresa").select2("enable", false);
                } else {
                    $("#cbx_empresa").val('');
                    $("#cbx_empresa").trigger('change');
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }
}

function editarReserva() {

    console.log($('#cbx_reserva').val());
    resetForm();
    $("#saladef").css("display", "none");
    $("#salanodef").css("display", "block");
    $("#myModalEditar").modal();

}

function getSalaidCBX(midata) {
    cargaCBX("id_sala_v", midata);
}

function desplegarCalendario() {
    var indexp = $('#id_sala_v').find(":selected").val();
    if (indexp == 23 || indexp == 24 || indexp == 30 || indexp == 31 || indexp == 16) {
        $('#ldireccion').show();
        $('#ddireccion').show();
    }
    $('#calendar').fullCalendar('destroy');
    var g = $('#id_sala_v').find(":selected").attr('value');
    $('#idSala').attr('value', g);
    $('#numSala').val($('#id_sala_v').find(":selected").attr('id'));
    var indexp = g;

    console.log(indexp);
    if (indexp == 23 || indexp == 24 || indexp == 30 || indexp == 31 || indexp == 16) {
        $('#ldireccion').show();
        $('#ddireccion').show();
    } else {
        $('#ldireccion').hide();
        $('#ddireccion').hide();
    }
    //alert("4454sd");
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('#calendar').fullCalendar({
        editable: false,
        slotDuration: {
            minutes: 15
        },
        weekends: true,
        allDaySlot: false,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        //events: "../Reserva/Agregar/llenarCalendar/" + g,
        events: "../Reserva/Agregar/llenarCalendar/" + g,
        eventColor: '#19aa8d',
        // Convert the allDay from string to boolean
        eventSources: [{
            url: "../Reserva/Agregar/getFeriados"
            //url: "/odin/assets/amelia/js/sistema/Reserva/feriados2016.json" // url to get holiday events
        }
            // any other sources...
        ],
        eventRender: function (event, element, view) {
            // lets test if the event has a property called holiday. 
            // If so and it matches '1', change the background of the correct day
            if (event.holiday == '1') {
                var dateString = event.start.format("YYYY-MM-DD");
                $(view.el[0]).find('.fc-day[data-date=' + dateString + ']')
                    .css('background-color', '#F5DEDE');
            }
            if (event.title == 'Feriado') {
                $("#fc-content").css("background-color", "#F6EBEB");
            }
        },
        eventClick: function (calEvent, jsEvent, view) {
            getDetallesReserva(calEvent.id_reserva, calEvent.id_detalle_sala);
        },
        eventConstraint: {
            start: moment().format('YYYY-MM-DD'),
            end: '2100-01-01' // hard coded goodness unfortunately
        },
        selectable: false,
        selectHelper: false
        
    });
}

function getDetallesReserva(id, id_detalle) {
    resetFormS();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_reserva: id,
            id_detalle_sala: id_detalle
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Reserva', errorThrown, 'error');
        },
        success: function (result) {
            console.log(result);
            //cierra modal	
            $("#from_reserva").css("display", "none");
            $("#saladef").css("display", "none");
            $("#salanodef").css("display", "block");
            //$("#btn_reservar").hide();
            $("#myModal4").modal();
            //vacia todos los campos del modal
            //document.getElementById('usuarioComercial').value = 0;
            document.getElementById('fecha_inicioS').value = result.fechainicio;
            document.getElementById('numSalaS').value = result.nombre_sala;
            document.getElementById('fecha_terminoS').value = result.fechatermino;
            document.getElementById('nombre_reservaS').value = result.nombre_reserva;
            document.getElementById('detalles_reservaS').value = result.detalle_reserva;
            document.getElementById('direccionS').value = result.direccion;
            document.getElementById('id_reservam').innerText = result.id_reserva;
            // document.getElementById('direccion').value = '';
            document.getElementById('breakSalaS').value = result.break;
            document.getElementById('computadoresS').value = result.computadores;

            $("#nombre_ejecutivoS").val(result.nombre_ejecutivo).prop('readonly', true).css("display", "block");
            $("#nombre_empresaS").val(result.nombre_empresa).prop('readonly', true).css("display", "block");
            $("#nombre_holdingS").val(result.nombre_holding).prop('readonly', true).css("display", "block");

            var indexp = result.id_sala;
            console.log(indexp);
            if (indexp == 23 || indexp == 24 || indexp == 30 || indexp == 31 || indexp == 16) {
                $('#ldireccionS').show();
                $('#ddireccionS').show();
            } else {
                $('#ldireccionS').hide();
                $('#ddireccionS').hide();
            }

            $('#txt_hora_inicioS').val(result.horainicio);
            $('#txt_hora_terminoS').val(result.horatermino);

            if (result.lunes == 1) {
                $('#lunesS').iCheck('check');
                $('#lunesS').val("1");
            } else {
                $('#lunesS').iCheck('uncheck');
                $('#lunesS').val("0");
            }

            if (result.martes == 1) {
                $('#martesS').iCheck('check');
                $('#martesS').val("2");
            } else {
                $('#martesS').iCheck('uncheck');
                $('#martesS').val("0");
            }
            if (result.miercoles == 1) {
                $('#miercolesS').iCheck('check');
                $('#miercolesS').val("3");
            } else {
                $('#miercolesS').iCheck('uncheck');
                $('#miercolesS').val("0");
            }

            if (result.jueves == 1) {
                $('#juevesS').iCheck('check')
                $('#juevesS').val("4");
            } else {
                $('#juevesS').iCheck('uncheck')
                $('#juevesS').val("0");
            }

            if (result.viernes == 1) {
                $('#viernesS').iCheck('check')
                $('#viernesS').val("5");
            } else {
                $('#viernesS').iCheck('uncheck')
                $('#viernesS').val("0");
            }

            if (result.sabado == 1) {
                $('#sabadoS').iCheck('check')
                $('#sabadoS').val("6");
            } else {
                $('#sabadoS').iCheck('uncheck')
                $('#sabadoS').val("0");
            }

            if (result.domingo == 1) {
                $('#domingoS').iCheck('check')
                $('#domingoS').val("7");
            } else {
                $('#domingoS').iCheck('uncheck')
                $('#domingoS').val("0");
            }
            desplegarCalendario();
        },
        url: "Editar/getDetalleReserva"
    });
}

function setFecha(input) {
    var fechaHoraArr = input.split(" ");
    var fecha = fechaHoraArr[0];
    var hora = fechaHoraArr[1];
    var fechaArr = fecha.split("-");
    return fechaArr[2] + "-" + fechaArr[1] + "-" + fechaArr[0] + " " + fechaHoraArr[1];
}

function validaFormModal(metodo) {
    var edicion = $("input[name='edicion']:checked").val();
    console.log(edicion);
    var finicio = document.getElementById('fecha_inicio').value;
    var ftermino = document.getElementById('fecha_termino').value;

    var hinicio = document.getElementById('txt_hora_inicio').value;
    var htermino = document.getElementById('txt_hora_termino').value;

    var lunes = $('#lunes').val();
    var martes = $('#martes').val();
    var miercoles = $('#miercoles').val();
    var jueves = $('#jueves').val();
    var viernes = $('#viernes').val();
    var sabado = $('#sabado').val();
    var domingo = $('#domingo').val();

    // var fRegistro = setFecha(document.getElementById('fRegistro').value);
    var idSala = document.getElementById('idSala').value;
    var idReserva = $('#cbx_reserva').select2("val");
    var idDetalleReserva = $('#cbx_bloque').select2("val");
    if (idReserva == -1) {
       // var idEjecutivo = $('#cbx_ejecutivo').select2("val");
        var idHolding = $('#cbx_holding').select2("val");
        var idEmpresa = $('#cbx_empresa').select2("val");
    }
    var nombreReserva = document.getElementById('nombre_reserva').value;
    var detalleReserva = document.getElementById('detalles_reserva').value;
    var direccion = document.getElementById('direccion').value;
    var breakSala = document.getElementById('breakSala').value;
    var computadores = document.getElementById('computadores').value;

    if (idReserva == 0 || idReserva == '') {
        swal({
            title: "Reserva",
            text: "Debe Seleccionar una reserva a editar",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
        function () {
            swal.close();
            document.getElementById('cbx_reserva').focus();
        })
        return false;
    }
    
    switch (edicion) {
        case '1':
            if (idHolding == '') {
                swal({
                    title: "Reserva",
                    text: "Debe Seleccionar un Holding",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('cbx_holding').focus();
                    })
                return false;
            }

            if (idEmpresa == '') {
                swal({
                    title: "Reserva",
                    text: "Debe Seleccionar una empresa",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('cbx_empresa').focus();
                    })
                return false;
            }
            if (nombreReserva == '') {
                swal({
                    title: "Reserva",
                    text: "Debe ingresar un nombre para la Reserva",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('nombre_reserva').focus();
                    })
                return false;
            }
            if (detalleReserva == '') {
                swal({
                    title: "Reserva",
                    text: "Debe Ingresar un detalle para la reserva",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('detalles_reserva').focus();
                    })
                return false;
            }
            //
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#formModal').serialize(),
                url: '../Reserva/Agregar/existeDisponibilidad/',
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    // console.log(  textStatus + errorThrown);
                    alerta('Reserva', errorThrown, 'error');
                },
                success: function (returnedData) {
                    if (returnedData[0].v_id_capsula > 0) {
                        swal({
                            title: "Reserva",
                            text: "Exite una reserva en el horario que intenta Reservar",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            closeOnConfirm: false
                        })
                        return false;
                    }else{
                        $.ajax({
                            async: false,
                            cache: false,
                            dataType: "json",
                            type: 'POST',
                            data: $('#formModal').serialize(),
                            error: function (jqXHR, textStatus, errorThrown) {
                                // code en caso de error de la ejecucion del ajax
                                //  console.log(textStatus + errorThrown);
                                alerta('reserva', jqXHR.responseText, 'error');
                            },
                            success: function (result) {
                                swal({
                                    title: "Reserva",
                                    text: "Reserva Modificada Correctamente",
                                    type: "success",
                                    confirmButtonColor: "#DD6B55"
                                })
                                $("#myModalEditar").modal('hide');
                                desplegarCalendario();
                                getReservas();
                            },
                            url: "Editar/EditarReserva/"
                        });
                        
                    }
                }
                    
            });

           
        break; 
        case '2':
            if (idDetalleReserva == '') {
                swal({
                    title: "Reserva",
                    text: "Debe Ingresar un bloque de la reserva",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('cbx_bloque').focus();
                    })
                return false;
            }
            if (idSala == '') {
                swal({
                    title: "Reserva",
                    text: "Debe Ingresar una sala para la reserva",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('cbx_sala').focus();
                    })
                return false;
            }
            if (breakSala == '') {
                swal({
                    title: "Reserva",
                    text: "Debe ingresar el detalle del break de la sala",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('breakSala').focus();
                    })
                return false;
            }
            if (computadores == '') {
                swal({
                    title: "Reserva",
                    text: "Debe ingresar el detalle de material de apoyo",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('computadores').focus();
                    })
                return false;
            }
            if ((idSala == 23 || idSala == 24 || idSala == 30 || idSala == 31 || idSala == 16) && direccion == '') {
                swal({
                    title: "Reserva",
                    text: "Debe ingresar la dirección de la reserva",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('direccion').focus();
                    })
                return false;
            }
            $("#cbx_bloque").prop('disabled', false);

            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#formModal').serialize(),
                url: '../Reserva/Agregar/existeDisponibilidad/',
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    // console.log(  textStatus + errorThrown);
                    alerta('Reserva', errorThrown, 'error');
                },
                success: function (returnedData) {
                    if (returnedData[0].v_id_capsula > 0) {
                        swal({
                            title: "Reserva",
                            text: "Exite una reserva en el horario que intenta Reservar",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            closeOnConfirm: false
                        })
                        return false;
                    }else{
                        $.ajax({
                            async: false,
                            cache: false,
                            dataType: "json",
                            type: 'POST',
                            data: $('#formModal').serialize(),
                            error: function (jqXHR, textStatus, errorThrown) {
                                // code en caso de error de la ejecucion del ajax
                                //  console.log(textStatus + errorThrown);
                                alerta('reserva', jqXHR.responseText, 'error');
                            },
                            success: function (result) {
                                swal({
                                    title: "Reserva",
                                    text: " Detalles de Reserva Modificada Correctamente",
                                    type: "success",
                                    confirmButtonColor: "#DD6B55"
                                })
                                $("#myModalEditar").modal('hide');
                                desplegarCalendario();
                                getReservas();
                            },
                            url: "Editar/EditarDetalleReserva/"
                        });
                    }
                }
            });
        break; 

        case '3':
            // Variables para la comparacion de la fechas
            /**validacion de fechas */
            if (idDetalleReserva == '') {
                swal({
                    title: "Reserva",
                    text: "Debe Ingresar un bloque de la reserva",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('cbx_bloque').focus();
                    })
                return false;
            }
            var res = finicio.split("-");
            finicio = res[2] + '-' + res[1] + '-' + res[0];
            res = ftermino.split("-");
            ftermino = res[2] + '-' + res[1] + '-' + res[0];

            if (finicio > ftermino) {
                swal({
                    title: "Fecha",
                    text: "Fecha Inicio no puede ser mayor a la fecha de  fecha Termino",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('fecha_inicio').focus();
                    })
                return false;
            }

            if (hinicio >= htermino) {
                swal({
                    title: "Reserva",
                    text: "La hora de inicio no puede ser igual o mayor ha la hora de termino",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('txt_hora_termino').focus();
                    })
                return false;
            }

            if (hinicio == '') {
                swal({
                    title: "Reserva",
                    text: "Debe ingresar hora de inicio",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('txt_hora_inicio').focus();
                    })
                return false;
            }
            if (htermino == '') {
                swal({
                    title: "Reserva",
                    text: "Debe ingresar hora de termino",
                    type: "warning",
                    confirmButtonColor: "#DD6B55",
                    closeOnConfirm: false
                },
                    function () {
                        swal.close();
                        document.getElementById('txt_hora_termino').focus();
                    })
                return false;
            }

            $("#cbx_bloque").prop('disabled', false);

            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#formModal').serialize(),
                url: '../Reserva/Agregar/existeDisponibilidad/',
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    // console.log(  textStatus + errorThrown);
                    alerta('Reserva', errorThrown, 'error');
                },
                success: function (returnedData) {
                    if (returnedData[0].v_id_capsula > 0) {
                        swal({
                            title: "Reserva",
                            text: "Exite una reserva en el horario que intenta Reservar",
                            type: "warning",
                            confirmButtonColor: "#DD6B55",
                            closeOnConfirm: false
                        })
                        return false;
                    }else{
                        $.ajax({
                            async: false,
                            cache: false,
                            dataType: "json",
                            type: 'POST',
                            data: $('#formModal').serialize(),
                            error: function (jqXHR, textStatus, errorThrown) {
                                // code en caso de error de la ejecucion del ajax
                                // console.log(  textStatus + errorThrown);
                                alerta('Reserva', errorThrown, 'error');
                            },
                            success: function (result) {
                                swal({
                                    title: "Reserva",
                                    text: "Horarios de Reserva Modificada Correctamente",
                                    type: "success",
                                    confirmButtonColor: "#DD6B55"
                                })
                                $("#myModalEditar").modal('hide');
                                desplegarCalendario();
                                getReservas();
                            },
                            url: "Editar/EditarCapsula"
                        });
                    }
                    
                }
            });
        break;
    }
 /*   if (idEjecutivo == 0 || idEjecutivo == '') {
        swal({
            title: "Reserva",
            text: "Debe Seleccionar un Ejecutivo",
            type: "warning",
            confirmButtonColor: "#DD6B55",
            closeOnConfirm: false
        },
            function () {
                swal.close();
                document.getElementById('cbx_ejecutivo').focus();
            })
        return false;
    }*/
}

function getEjecutivos() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, "ejecutivo");
        },
        url: "Editar/getUsuarioTipoEcutivoComercial"
    });

}

function getReservas() {
    $.ajax({
        url: '../Reserva/Editar/listaReservasbyUserEditar/',
        dataType: "json",
        type: "POST",
        success: function (json) {
            console.log(json);
            cargaCombo(json, "reserva");
        },
        error: function (msg) {
            console.log(msg);
        }
    });
}

function cargaCombo(midata, id) {
    $("#cbx_" + id).html('').select2();
    $("#cbx_" + id).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
}
