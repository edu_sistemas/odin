 /* Ultima edicion: 2018 - 03 - 05[Jessica Roa]<jroa@edutecno.com>*/ 

$(document).ready(function () {
    recargaLaPagina();
    desplegarCalendario();

    $('#id_sede').change(function () {
        desplegarCalendario($('#id_sede').val());
    });

});

function desplegarCalendario(sede) {
    $('#calendar').fullCalendar('destroy');
    //alert("4454sd");
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('#calendar').fullCalendar({
        defaultView: 'agendaDay',
        minTime: '08:00:00',
        axisFormat: 'H:mm',
        height: 770,
        editable: false,
        slotEventOverlap: false,
        weekends: true,
        navLinks: true,

       // editable: true,
       // selectable: true,
       // eventLimit: true,

        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'agendaDay,agendaWeek,month'
        },
        resourceRender: function (resourceObj, $th) {
            $th.append(
                $('<strong>(?)</strong>').popover({
                    title: resourceObj.title,
                    content: resourceObj.detalles,
                    trigger: 'hover',
                    placement: 'bottom',
                    container: 'body'
                })
            );
        },
        resources: "../Reserva/visualizacion/llenarSalas/" + sede,

        events: "../Reserva/visualizacion/llenarCalendar",


       // eventColor: '#19aa8d',
        
        eventSources: [{
                url: "../Reserva/visualizacion/getFeriados"
                        //url: "../../assets/amelia/js/sistema/Reserva/feriados2016.json" // url to get holiday events
            },
                    // any other sources...
        ],
        eventRender: function (event, element, view) {
            // lets test if the event has a property called holiday. 
            // If so and it matches '1', change the background of the correct day

       //     $(element).tooltip({title: event.title + ' / ' + event.computadores + ' / ' + event.break + ' / ' + event.detalle_reserva});
            $(element).tooltip({ title: 'Hacer click para más información ' + (event.title)});
//            if (event.holiday == '1') {
//                var dateString = event.start.format("YYYY-MM-DD");
//                $(view.el[0]).find('.fc-day[data-date=' + dateString + ']')
//                        .css('background-color', '#F9E6E6');
//            }
//            if (event.title == 'Feriado') {
//                $("#fc-content").css("background-color", "#D21A1A");
//            }
        },
        eventClick: function (calEvent, jsEvent, view) {

            //llamadatoFecha(calEvent.id);
            if(calEvent.estado<=1){
                llamadatoFechaBloque(calEvent.id_detalle_sala);
                llamadoDetallesReserva(calEvent.id);
                cuentaalumnos(calEvent.id_ficha);
                $('#numSala').val(calEvent.nsala);
                $('#infoModal').modal();
            }else{
                estadoText="";
                if(calEvent.estado==2){
                    estadoText="Reserva Rechazada";
                    swal(estadoText, "Esta reserva no posee datos para visualizar", "info")
                }
                estadoText2="";
                 if(calEvent.estado==3){
                    estadoText2="Reserva Eliminada";
                    swal(estadoText2, "Esta reserva no posee datos para visualizar", "info")
                }
                
            }

           


        },
        eventConstraint: {
            start: moment().format('YYYY-MM-DD'),
            end: '2100-01-01' // hard coded goodness unfortunately
        }

    });
}

function cuentaalumnos(nsala) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result[0].horario_inicio);
            $("#cantalm").val(result[0].cantidad);


            // console.log(result.horario_inicio);
        },
        url: "../Reserva/Visualizacion/getalmumnos/" + nsala
    });
}

function llamadatoFecha(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result[0].horario_inicio);
            $("#horario_inicio").val(result[0].horario_inicio);
            $("#horario_termino").val(result[0].horario_termino);

            // console.log(result.horario_inicio);
        },
        url: "../Reserva/Visualizacion/getFechas/" + id
    });
}
function llamadatoFechaBloque(id_detalle_sala) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result[0].horario_inicio);
            $("#horario_inicio").val(result[0].horario_inicio);
            $("#horario_termino").val(result[0].horario_termino);

            // console.log(result.horario_inicio);
        },
        url: "../Reserva/Visualizacion/getFechasBloque/" + id_detalle_sala
    });
}

function llamadoDetallesReserva(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result[0].horario_inicio);
            $("#id_reserva").text(result[0].id_reserva);
            $("#id_detalle_sala").text(result[0].id_detalle_sala);
            $("#estado").text(result[0].estado_reserva);
            $("#bloques").text(result[0].bloques);
            $("#nombre_reserva").val(result[0].nombre_reserva);
            $("#detalle_reserva").val(result[0].detalle_reserva);
            $("#direccion").val(result[0].direccion);
            $("#break").val(result[0].break);
            $("#computadores").val(result[0].computadores);
            $("#usuario_registro").val(result[0].usuario_registro);
            $("#ejecutivo").val(result[0].ejecutivo);
            // console.log(result);

            // llamaDatosControlesFicha(result[0].id_ficha);
        },
        url: "../Reserva/Visualizacion/getDetallesReserva/" + id
    });
}