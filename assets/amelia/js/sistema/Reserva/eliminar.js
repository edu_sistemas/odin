var pepito;
$(document).ready(function () {
       recargaLaPagina();
    // llamaDatosControlesFicha();
    llamaDatosSalas();
    $("#capsula").click(function () {
        sendMailCapsulaEliminada($("#id_capsula").val());
        eliminaCapsula( $("#id_capsula").val(), $("#id_detalle_sala").val(), $("#id_reserva").val());
    });

    $("#bloque").click(function () {
        sendMailBloqueEliminado( $("#id_detalle_sala").val());
        eliminaBloque( $("#id_detalle_sala").val(), $("#id_reserva").val());
    });

    $("#reserva").click(function () {
        sendMailReservaEliminada($("#id_reserva").val());
        eliminaReserva($("#id_reserva").val());
    });
});

function llamaDatosSalas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            getSalaidCBX(result);
        },
        url: "Eliminar/getSalas"
    });
}

function getSalaidCBX(midata) {
    cargaCBX("id_sala_v", midata);
}

function desplegarCalendario() {
    $('#calendar').fullCalendar('destroy');
    var g = $('#id_sala_v').find(":selected").attr('value');
    $('#idSala').attr('value', g);
    $('#numSala').val($('#id_sala_v').find(":selected").attr('id'));
    $.ajax({
        url: '../Reserva/Eliminar/listaReservasbyUser/',
        dataType: "json",
        type: "POST"
    });
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('#calendar').fullCalendar({
        editable: true,
        slotDuration: {
            minutes: 15
        },
        weekends: true,
        allDaySlot: false,
        eventStartEditable: false,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        events: "../Reserva/Eliminar/llenarCalendar/" + g,
        eventColor: '#1674E9',
        // Convert the allDay from string to boolean
        eventSources: [{
                url: "../Reserva/Eliminar/getFeriados"
                        // url: "../../assets/amelia/js/sistema/Reserva/feriados2016.json" // url to get holiday events
            }
            // any other sources...
        ],
        eventRender: function (event, element, view) {
            // lets test if the event has a property called holiday. 
            // If so and it matches '1', change the background of the correct day
            if (event.holiday == '1') {
                var dateString = event.start.format("YYYY-MM-DD");
                $(view.el[0]).find('.fc-day[data-date=' + dateString + ']')
                        .css('background-color', '#F5DEDE');
            }
        },
        eventConstraint: {
            start: moment().format('YYYY-MM-DD'),
            end: '2100-01-01' // hard coded goodness unfortunately
        },
        eventClick: function (calEvent, jsEvent, view) {
            var x = moment(event.start).format('YYYYMMDD');
            var y = moment(calEvent.start).format('YYYYMMDD');
            var ttl = (calEvent.title);
            console.log(y + 'ddd' +x);
            $("#myModal4").hide();
            if (calEvent.backgroundColor == "#DA2155") {
                var text = "!No puede Eliminar un día feriado!"
               
                swal({
                    title: "Reserva",
                    text: text,
                    type: "warning",
                    confirmButtonColor: "#DD6B55"
                });
            }else if(y<x){
                var text = "!No puede Eliminar reserva de un día anterior al día actual!"
                
                swal({
                    title: "Reserva",
                    text: text,
                    type: "warning",
                    confirmButtonColor: "#DD6B55"
                });


            }
               /* pepito = "1";
                $('#visible_div').val(pepito);
            } else {
                pepito = "0";
                $('#visible_div').val(pepito);
            }
            if ($('#visible_div').val() == '1' || $('#visible_div').val() == 1) {
                //$('#eliminacion_r').hide();
            } else {
                $('#eliminacion_r').show();
            }
            if (/*x > y ||*/ /*ttl == 'Feriado' && pepito != "1") {
                if (ttl == 'Feriado') {
                    var text = "!No puede Eliminar un día feriado!"
                } else {
                    var text = "!No puede Eliminar un día anterior al día actual!"
                }
               */
             else {
                //               
                llamadatoFecha(calEvent.id);
                llamadoDetallesReserva(calEvent.id);
                $('#id_reserva').val(calEvent.id_reserva);
                //alert('id capsula: ' + calEvent.id + ' Id Reserva: ' + calEvent.id_reserva);
                $('#id_capsula').val(calEvent.id);
                $('#id_reserva').val(calEvent.id_reserva);
                $('#id_detalle_sala').val(calEvent.id_detalle_sala);
                $("#myModal4").modal();
            }
        }
    });
}

function llamadatoFecha(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result[0].horario_inicio);
            $("#horario_inicio").val(result[0].horario_inicio);
            $("#horario_termino").val(result[0].horario_termino);
            // console.log(result.horario_inicio);
        },
        url: "../Reserva/Eliminar/getFechas/" + id
    });
}

function llamadoDetallesReserva(id) {
    //console.log(id);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        //data:
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result);
            $("#nombre_reserva").val(result[0].nombre_reserva);
            $("#detalle_reserva").val(result[0].detalle_reserva);
            $("#id_reserva_e").text(result[0].id_reserva);
            console.log(result[0].id_reserva, result[0].id_detalle_sala);
            $("#id_detalle_sala").val(result[0].id_detalle_sala);
            $("#id_detalle_sala_e").text(result[0].id_detalle_sala);
            $("#direccion").val(result[0].direccion);
            $("#break").val(result[0].break);
            $("#computadores").val(result[0].computadores);
            $("#usuario_registro").val(result[0].usuario_registro);
            $("#ejecutivo").val(result[0].ejecutivo);
            if (result[0].estado_reserva == '1') {
                $('.icheckbox_square-green').addClass('checked');
                $("#chk").attr("disabled", true);
                document.getElementById("confirmarR").disabled = true;
                $('#cbx_ficha').prop('disabled', true);
                $('#msg2').html('Estado reserva: CONFIRMADA');
            } else {
                //alert(result[0].estado_reserva);
                $('.icheckbox_square-green').removeClass('checked');
                $("#chk").attr("disabled", false);
                $('#cbx_ficha').prop('disabled', false);
            }
            pepito = result[0].estado_reserva;
           // llamaDatosControlesFicha(result[0].id_ficha);
        },
        url: "../Reserva/Eliminar/getDetallesReserva/" + id
    });
}

/*function llamaDatosControlesFicha(id_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, id_ficha);
        },
        url: "Confirmar/getFicha"
    });
}*/

function cargaCombo(midata, id_ficha) {
    cargaCBXSelected('cbx_ficha', midata, id_ficha);
    if (id_ficha == null) {
        $("#cbx_ficha").prepend('<option value="0" selected>Seleccione...</option>');
    }
}

function eliminaCapsula(id_capsula, id_detalle_sala, id_reserva) {
    // alert('Se elimina capsula');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_capsula: id_capsula,
            id_detalle_sala: id_detalle_sala,
            id_reserva: id_reserva
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta('Capsula', jqXHR.responseText, 'error');
        },
        success: function (result) {
            
            swal({
                title: "Eliminación",
                text: "Horario Eliminado Correctamente",
                type: "success",
                confirmButtonColor: "#DD6B55"
            });
            
            $("#myModal4").modal('hide');
            desplegarCalendario();
        },
        url: "../Reserva/Eliminar/EliminarCapsula"
    });
}

function  sendMailCapsulaEliminada(id_capsula) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "text",
        type: 'POST',
        data:  {
            id_capsula: id_capsula
        },
        error: function (jqXHR, textStatus, errorThrown) {
           alerta("ERROR", jqXHR.responseText, "error");
        },
        success: function (result) {
            alerta('Validación', 'Se ha enviado una notifiación con su solicitud', 'success');          
        },
        url: "../Reserva/Eliminar/enviaEmailCapsulaEliminada"
    });
}

function  sendMailBloqueEliminado(id_detalle_sala) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "text",
        type: 'POST',
        data:  {
            id_detalle_sala: id_detalle_sala
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
           alerta("ERROR", jqXHR.responseText, "error");
        },
        success: function (result) {
            alerta('Validación', 'Se ha enviado una notifiación con su solicitud', 'success');          
        },
        url: "../Reserva/Eliminar/enviaEmailBloqueEliminado"
    });
}



function eliminaBloque(id_detalle_sala, id_reserva) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_detalle_sala: id_detalle_sala,
            id_reserva: id_reserva
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta('bloque', jqXHR.responseText, 'error');
        },
        success: function (result) {
            swal({
                title: "Eliminación",
                text: "Bloque Eliminado Correctamente",
                type: "success",
                confirmButtonColor: "#DD6B55"
            });
            $("#myModal4").modal('hide');
            desplegarCalendario();
        },
        url: "../Reserva/Eliminar/EliminarBloque"
    });
}

function eliminaReserva(id_reserva) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_reserva: id_reserva
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta('reserva', jqXHR.responseText, 'error');
        },
        success: function (result) {
            swal({
                title: "Eliminación",
                text: "Reserva Eliminada Correctamente",
                type: "success",
                confirmButtonColor: "#DD6B55"
            })
            $("#myModal4").modal('hide');
            desplegarCalendario();
        },
        url: "../Reserva/Eliminar/EliminarReserva"
    });
}

function  sendMailReservaEliminada(id_reserva) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "text",
        type: 'POST',
        data:  {
            id_reserva: id_reserva
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
           alerta("ERROR", jqXHR.responseText, "error");
        },
        success: function (result) {
            alerta('Validación', 'Se ha enviado una notifiación con su solicitud', 'success');          
        },
        url: "../Reserva/Eliminar/enviaEmailReservaEliminada"
    });
}