$(document).ready(function () {

    $("#btn_registrar").click(function () {

        if (validarRequeridoForm($("#frm_docente_registro"))) {
            RegistraDocente();
        }
    });

    $('#dv_docente').keyup(function (e) {
        comprobarRut();
    });
    $('#rut_docente').keyup(function (e) {
        comprobarRut();
    });

    var $inputImage = $("#inputImage");
    if (window.FileReader) {
        $inputImage.change(function () {
            var arrayForExtension = document.getElementById('inputImage').value.split('.');
            arrayForExtension.reverse();
            var extension = arrayForExtension[0];
            // console.log(extension);

            // Read in file
            var file = event.target.files[0];

            // Ensure it's an image
            if (file.type.match(/image.*/)) {
                // console.log('An image has been loaded');

                // Load the image
                var reader = new FileReader();
                reader.onload = function (readerEvent) {
                    var image = new Image();
                    image.src = readerEvent.target.result;
                    // Resize the image
                    var canvas = document.createElement('canvas'),
                            max_size = 544, // TODO : pull max size from a site config
                            width = image.width,
                            height = image.height;
                    if (width > height) {
                        if (width > max_size) {
                            height *= max_size / width;
                            width = max_size;
                        }
                    } else {
                        if (height > max_size) {
                            width *= max_size / height;
                            height = max_size;
                        }
                    }
                    canvas.width = width;
                    canvas.height = height;
                    canvas.getContext('2d').drawImage(image, 0, 0, width, height);
                    var dataUrl = canvas.toDataURL('image/' + extension);
                    var resizedImage = dataURLToBlob(dataUrl);
                    // $.event.trigger({
                    //     type: "imageResized",
                    //     blob: resizedImage,
                    //     url: dataUrl
                    // });


                    document.getElementById("preimagen").src = dataUrl;
                    // console.log(dataUrl);
                    document.getElementById("imagen").value = document.getElementById("preimagen").src;

                }
                reader.readAsDataURL(file);
            }
        });
    }

    /* Utility function to convert a canvas to a BLOB */
    var dataURLToBlob = function (dataURL) {
        var BASE64_MARKER = ';base64,';
        if (dataURL.indexOf(BASE64_MARKER) == -1) {
            var parts = dataURL.split(',');
            var contentType = parts[0].split(':')[1];
            var raw = parts[1];

            return new Blob([raw], {type: contentType});
        }

        var parts = dataURL.split(BASE64_MARKER);
        var contentType = parts[0].split(':')[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;

        var uInt8Array = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }

        return new Blob([uInt8Array], {type: contentType});
    }
    /* End Utility function to convert a canvas to a BLOB      */


    llamaDatosControles();
});


function RegistraDocente() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_docente_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            alerta('Docente', jqXHR.responseText, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }

        },
        url: "Agregar/Registra_docente"
    });
}

 

 

function comprobarRut() {

//     ALGORITMO 3, propiedades de la división por 11

// 1. Multiplicar cada dígito del RUT se por 9, 8, ..., 4, 9, 8, ... de atrás hacia adelante.
// 2. Sumar las multiplicaciones parciales.
// 3. Suma alternada de la lista reversa de los dígitos del resultado anterior.
// 4. El Dígito Verificador es el resultado anterior. Si es 10, se cambia por 'k'.


// EJEMPLO.  RUT: 11.222.333

// 1.   1   1   2   2   2   3   3   3  <--  RUT
//    * 8   9   4   5   6   7   8   9  <--  9, 8, 7, 6, 5, 4, 9, 8, ...
//    --------------------------------------
//      8   9   8  10  12  21  24  27

// 2. SUMA: 8 + 9 + 8 + 10 + 12 + 21 + 24 + 27 = 119

// 3. SUMA ALTERNADA:  119 -> 9 - 1 + 1 = 9

// 4. 9 <-- DÍGITO VERIFICADOR

    var rut = $("#rut_docente").val();
    var dv_form = $("#dv_docente").val();

    var out_print = "";



    if (rut.trim() == "" || dv_form.trim() == "") {
        out_print = "Debe ingresar RUT y DV";
    } else {
        if (rut != "" && dv_form != "") {
            out_print = "";

            if (dv_form == "k") {
                dv_form = "10";
            }

            var rutArr = [];
            var secuencia = [9, 8, 7, 6, 5, 4, 9, 8];

            for (var i = 0; i < rut.length; i++) {
                rutArr.push(parseInt(rut.charAt(i)));
            }

            rutArr.reverse();

            var xParciales = 0;

            for (var i = 0; i < rutArr.length; i++) {
                xParciales = xParciales + (secuencia[i] * rutArr[i]);
            }

            var str_xParciales = xParciales.toString();
            var xParcialesArr = [];

            for (var i = 0; i < str_xParciales.length; i++) {
                xParcialesArr.push(parseInt(str_xParciales.charAt(i)));
            }

            xParcialesArr.reverse();

            var dvSumas = 0;
            var dvRestas = 0;

            for (var i = 0; i < xParcialesArr.length; i++) {
                if ((i + 1) % 2 == 0) {
                    dvRestas = dvRestas - xParcialesArr[i];
                } else {
                    dvSumas = dvSumas + xParcialesArr[i];
                }
            }

            var dv = dvSumas + dvRestas;

            if (dv < 0 || dv > 9) {
                dv = 10;
            }

            console.log(dv);

            if (dv == parseInt(dv_form)) {
                out_print = "";
            } else {
                out_print = "No se ha ingresado un rut válido";
            }
        } else {
            out_print = "No se ha ingresado un rut válido";
        }
    }


    document.getElementById("out_print").innerHTML = out_print;




}