$(document).ready(function () {
       recargaLaPagina();
    cargaCbxAll();
    $('#btn_nuevo').click(function () {
        location.href = 'Agregar';
    });

    $('#btn_filtrar').click(function () {
        listarDocentes();
    });

    $('#btn_left_edit').click(function () {
        setProgressBar("left");
    });
    $('#btn_right_edit').click(function () {
        setProgressBar("right");
    });

    $('#btn_left_ingresar').click(function () {
        setProgressBarIngresar("left");
    });
    $('#btn_right_ingresar').click(function () {
        setProgressBarIngresar("right");
    });

    $('#btn_actualizar_skill').click(function () {
        actualizarSkill();
    });

    $('#btn_ingresar_skill').click(function () {
        ingresarSkill();
    });

    $("#btn_cancelar_ingresar_skill").click(function () {
        $("#ingresar_skill").animate({
            height: "toggle"
        }, 500, function () {
            ShowSkills($('#id_usuario_edit').val(), $("#nom_relator").html());
        });

    });

    $("#btn_guardar_ingresar_skill").click(function () {
        guardarSkill();
    });

    listarDocentes();

});
function cargaTablaUsuario(data) {

    if ($.fn.dataTable.isDataTable('#tbl_docentes')) {
        $('#tbl_docentes').DataTable().destroy();
    }

    $('#tbl_docentes')
            .DataTable(
                    {
                        "aaData": data,
                        "aoColumns": [
                            {
                                "mDataProp": "id"
                            },
                            {
                                "mDataProp": "usuario"
                            },
                            {
                                "mDataProp": "nombre"
                            },
                            {
                                "mDataProp": "apellidos"
                            },
                            {
                                "mDataProp": "rut"
                            },
                            {
                                "mDataProp": "correo"
                            },
                            {
                                "mDataProp": "direccion"
                            },
                            {
                                "mDataProp": "telefono"
                            },
                            {
                                "mDataProp": null,
                                "bSortable": false,
                                "mRender": function (o) {
                                    var html = "";
                                    html = '<i title="Inactivo" class="fa fa-circle text-danger"></i>';
                                    if (o.estado == 1) {
                                        html = '  <i title="Activo"  class="fa fa-circle text-navy"></i>';
                                    }
                                    return html;
                                }
                            },
                            {
                                "mDataProp": null,
                                "bSortable": false,
                                "mRender": function (o) {

                                    var nombre_relator = "'" + o.nombre
                                            + " " + o.apellidos + "'";

                                    var y = "Editar/index/" + o.id + "";
                                    var html = "";
                                    html += '  <div class="btn-group">';
                                    html += '  <button data-toggle="dropdown" class="btn btn-default dropdown-toggle ">Accion<span class="caret"></span></button>';
                                    html += '  <ul class="dropdown-menu pull-right">';

                                    html += '  <li><a href="' + y
                                            + '"  >Editar</a></li>';
                                    if (o.estado == 1) {
                                        html += "<li><a onclick='EditarUsuarioEstado("
                                                + o.id
                                                + ",0);'>Desactivar</a></li>";
                                    } else {
                                        html += "<li><a onclick='EditarUsuarioEstado("
                                                + o.id
                                                + ",1);'>Activar</a></li> ";
                                    }
                                    html += '<li><a onclick="ShowSkills('
                                            + o.id + ' , ' + nombre_relator
                                            + ');">Skills</a></li> ';
                                    html += ' </ul>';
                                    html += ' </div>';
                                    return html;
                                }
                            }

                        ],
                        pageLength: 25,
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [
                            {
                                extend: 'copy'
                            },
                            {
                                extend: 'csv'
                            },
                            {
                                extend: 'excel',
                                title: 'Empresa'
                            },
                            {
                                extend: 'pdf',
                                title: 'Empresa'
                            },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass(
                                            'white-bg');
                                    $(win.document.body).css('font-size',
                                            '10px');
                                    $(win.document.body).find('table')
                                            .addClass('compact').css(
                                            'font-size', 'inherit');
                                }
                            }]
                    });
}

function ShowSkills(id_usuario, nombre_relator) {


    $("#edit_skill").css({
        'display': 'none'
    });
    $("#ingresar_skill").css({
        'display': 'none'
    });
    $("#lista_skills").css({
        'display': 'block'
    });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_usuario: id_usuario
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);

            $('#ModalSkill').modal('show');
            $('#ModalSkill .modal-body').css({"opacity": '0'});
            $("#nom_relator").html(nombre_relator);

            $("#id_usuario_edit").val(id_usuario);

            LoadSkills(result);

            $('#ModalSkill .modal-body').animate({
                'opacity': '1'
            }, 1000, function () {

            })

        },
        url: "Listar/getSkillsByUsuario"
    });

}

function guardarSkill() {
    var id_conocimiento = $('#conocimiento_cbx').val();
    var id_nivel_conocimiento = 0;
    var id_usuario = $('#id_usuario_edit').val();

    var bar_level = Math.round($("#progress_bar_ingresar").width() / $('#progress_bar_ingresar').parent().width() * 100);
    if (bar_level == 25) {
        id_nivel_conocimiento = 1;
    }
    if (bar_level == 50) {
        id_nivel_conocimiento = 2;
    }
    if (bar_level == 75) {
        id_nivel_conocimiento = 3;
    }
    if (bar_level == 100) {
        id_nivel_conocimiento = 4;
    }


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_conocimiento: id_conocimiento, id_nivel_conocimiento: id_nivel_conocimiento, id_usuario: id_usuario
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje,
                    result[0].tipo_alerta);
            ShowSkills($('#id_usuario_edit').val(), $("#nom_relator").html());

        },
        url: "Listar/guardarSkill"
    });
}

function ingresarSkill() {
    $("#lista_skills").animate({
        height: "toggle"
    }, 500, function () {

        $("#ingresar_skill").animate({
            height: "toggle"
        }, 500, function () {

        });
    });


}

function actualizarSkill() {

    var id_skill = $("#id_skill_edit").val();
    var id_nivel_conocimiento = 0;

    var bar_level = Math.round($("#progress_bar_edit").width() / $('#progress_bar_edit').parent().width() * 100);
    if (bar_level == 25) {
        id_nivel_conocimiento = 1;
    }
    if (bar_level == 50) {
        id_nivel_conocimiento = 2;
    }
    if (bar_level == 75) {
        id_nivel_conocimiento = 3;
    }
    if (bar_level == 100) {
        id_nivel_conocimiento = 4;
    }


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_skill: id_skill, id_nivel_conocimiento: id_nivel_conocimiento
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje,
                    result[0].tipo_alerta);
            ShowSkills($('#id_usuario_edit').val(), $("#nom_relator").html());
            //LoadSkills(result);
        },
        url: "Listar/updateSkill"
    });
}

function setProgressBar(caso) {
    $("#progress_bar_edit").removeClass("progress-bar-danger");
    $("#progress_bar_edit").removeClass("progress-bar-warning");
    $("#progress_bar_edit").removeClass("progress-bar-primary");
    $("#progress_bar_edit").removeClass("progress-bar-success");

    var bar_level = Math.round($("#progress_bar_edit").width() / $('#progress_bar_edit').parent().width() * 100);


    if (caso == "left") {

        if (bar_level == 50 || bar_level == 25) {

            $("#progress_bar_edit").css({"width": "25%"})
            $("#progress_bar_edit").addClass("progress-bar-danger");
            $("#progress_bar_edit").text("Básico");
        }
        if (bar_level == 75) {

            $("#progress_bar_edit").css({"width": "50%"})
            $("#progress_bar_edit").addClass("progress-bar-warning");
            $("#progress_bar_edit").text("Intermedio");

        }
        if (bar_level == 100) {

            $("#progress_bar_edit").css({"width": "75%"})
            $("#progress_bar_edit").removeClass("progress-bar-primary");
            $("#progress_bar_edit").text("Avanzado");
        }
    }

    if (caso == "right") {
        if (bar_level == 25) {

            $("#progress_bar_edit").css({"width": "50%"})
            $("#progress_bar_edit").addClass("progress-bar-warning");
            $("#progress_bar_edit").text("Intermedio");
        }
        if (bar_level == 50) {

            $("#progress_bar_edit").css({"width": "75%"})
            $("#progress_bar_edit").addClass("progress-bar-primary");
            $("#progress_bar_edit").text("Avanzado");
        }
        if (bar_level == 75 || bar_level == 100) {

            $("#progress_bar_edit").css({"width": "100%"})
            $("#progress_bar_edit").addClass("progress-bar-success");
            $("#progress_bar_edit").text("Macro");

        }
    }


}

function setProgressBarIngresar(caso) {
    $("#progress_bar_ingresar").removeClass("progress-bar-danger");
    $("#progress_bar_ingresar").removeClass("progress-bar-warning");
    $("#progress_bar_ingresar").removeClass("progress-bar-primary");
    $("#progress_bar_ingresar").removeClass("progress-bar-success");

    var bar_level = Math.round($("#progress_bar_ingresar").width() / $('#progress_bar_ingresar').parent().width() * 100);


    if (caso == "left") {

        if (bar_level == 50 || bar_level == 25) {

            $("#progress_bar_ingresar").css({"width": "25%"})
            $("#progress_bar_ingresar").addClass("progress-bar-danger");
            $("#progress_bar_ingresar").text("Básico");
        }
        if (bar_level == 75) {

            $("#progress_bar_ingresar").css({"width": "50%"})
            $("#progress_bar_ingresar").addClass("progress-bar-warning");
            $("#progress_bar_ingresar").text("Intermedio");

        }
        if (bar_level == 100) {

            $("#progress_bar_ingresar").css({"width": "75%"})
            $("#progress_bar_ingresar").removeClass("progress-bar-primary");
            $("#progress_bar_ingresar").text("Avanzado");
        }
    }

    if (caso == "right") {
        if (bar_level == 25) {

            $("#progress_bar_ingresar").css({"width": "50%"})
            $("#progress_bar_ingresar").addClass("progress-bar-warning");
            $("#progress_bar_ingresar").text("Intermedio");
        }
        if (bar_level == 50) {

            $("#progress_bar_ingresar").css({"width": "75%"})
            $("#progress_bar_ingresar").addClass("progress-bar-primary");
            $("#progress_bar_ingresar").text("Avanzado");
        }
        if (bar_level == 75 || bar_level == 100) {

            $("#progress_bar_ingresar").css({"width": "100%"})
            $("#progress_bar_ingresar").addClass("progress-bar-success");
            $("#progress_bar_ingresar").text("Macro");

        }
    }


}

function LoadSkills(data) {

    var basico = '<div class="progress"><div class="progress-bar progress-bar-danger" style="width: 25%">Básico</div></div>', intermedio = '<div class="progress"><div class="progress-bar progress-bar-warning" style="width: 50%">Intermedio</div></div>', avanzado = '<div class="progress"><div class="progress-bar progress-bar-primary" style="width: 70%">Avanzado</div></div>', macro = '<div class="progress"><div class="progress-bar progress-bar-success" style="width: 100%">Macro</div></div>';

    var html = "";
    $('#tbl_skills tbody').html(html);
    $
            .each(
                    data,
                    function (i, item) {
                        html += '<tr>';
                        html += '<td style="width: 1px; white-space: nowrap;"><strong style="color: #176FA8;">'
                                + item.conocimiento + '</strong></td>';
                        if (item.id_nivel == 1) {
                            html += '<td>' + basico + '</td>';
                        }
                        if (item.id_nivel == 2) {
                            html += '<td>' + intermedio + '</td>';
                        }
                        if (item.id_nivel == 3) {
                            html += '<td>' + avanzado + '</td>';
                        }
                        if (item.id_nivel == 4) {
                            html += '<td>' + macro + '</td>';
                        }

                        html += '<td style="width: 1px; white-space: nowrap;">';
                        html += '<button onclick="editarSkill('
                                + item.id_skill
                                + ')" class="btn btn-info  dim" type="button"><i class="fa fa-edit" style="font-size:120%;"></i><br><small style="font-size:60%; padding:none;">Editar</small></button>';
                        html += '<button onclick="eliminarSkill('
                                + item.id_skill
                                + ', '
                                + item.id_usuario
                                + ')"; class="btn btn-danger  dim" type="button"><i class="fa fa-trash"  style="font-size:120%;"></i><br><small style="font-size:60%; padding:none;">Eliminar</small></button>'
                                + '</td>';
                    });
    $('#tbl_skills tbody').html(html);
}

function editarSkill(id_skill) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_skill: id_skill
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $("#progress_bar_edit").removeClass("progress-bar-danger");
            $("#progress_bar_edit").removeClass("progress-bar-warning");
            $("#progress_bar_edit").removeClass("progress-bar-primary");
            $("#progress_bar_edit").removeClass("progress-bar-success");

            $('#lista_skills').animate({
                height: "toggle"
            }, 300, function () {
                var nivel = '';
                if (result[0].id_nivel == 1) {
                    nivel = '25%';
                    $("#progress_bar_edit").addClass("progress-bar-danger");

                }
                if (result[0].id_nivel == 2) {
                    nivel = '50%';
                    $("#progress_bar_edit").addClass("progress-bar-warning");
                }
                if (result[0].id_nivel == 3) {
                    nivel = '75%';
                    $("#progress_bar_edit").addClass("progress-bar-primary");
                }
                if (result[0].id_nivel == 4) {
                    nivel = '100%';
                    $("#progress_bar_edit").addClass("progress-bar-success");
                }
                $("#nombre_conocimiento").text(result[0].conocimiento);
                $("#progress_bar_edit").css({
                    'width': nivel
                });
                $("#progress_bar_edit").text(result[0].nivel);
                $("#id_skill_edit").val(id_skill);
                $('#edit_skill').animate({
                    height: "toggle"
                }, 300, function () {

                });
            });

        },
        url: "Listar/getSkillById"
    });
}

function eliminarSkill(id_skill, id_usuario) {
    swal({
        title: "¿Está Seguro?",
        text: "Si elimina este skill, no podrá recuperarlo posteriormente",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Sí, deseo eliminarlo!",
        closeOnConfirm: false
    }, function () {

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {
                id_skill: id_skill
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(textStatus + errorThrown);
            },
            success: function (result) {
                ShowSkills(id_usuario, $('#nom_relator').html());
                alerta(result[0].titulo, result[0].mensaje,
                        result[0].tipo_alerta);
            },
            url: "Listar/deleteSkillRelator"
        });
        // swal("Borrado!", "Nah, todavía no está listo el proceso .",
        // "success");
    });
}

function cargaCbxAll() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "conocimiento_filtro_cbx");
            cargaCBX("conocimiento_cbx", result);
        },
        url: "Listar/getConocimientosCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "nivel_filtro_cbx");
        },
        url: "Listar/getNivelConocimientosCBX"
    });
}

function listarDocentes() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_filtrarxxxx').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            cargaTablaUsuario(result);
        },
        url: "Listar/listarDocentes"
    });
}

function EditarUsuarioEstado(id_docente, tipo_estado) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id: id_docente,
            estado: tipo_estado
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Usuario", errorThrown, "error");
        },
        success: function (result) {

            listarDocentes();
        },
        url: "Listar/CambiarEstadoUsuario"
    });

}

function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    // $("#" + item + " option[value='0']").remove();
    $("#" + item).val(0).trigger("change");
}
