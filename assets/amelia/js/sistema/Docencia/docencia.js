$(document).ready(function () {

	//recargaLaPagina();

	var d = new Date();
	var mes = d.getMonth() + 1;
	var llear = d.getFullYear();

	cargaDatosFicha();

	for (var x = 1; x <= mes; x++) {

		var texto = '';
		switch (x) {
			case 1:
				texto = 'Enero';
				x = '01';
				break;
			case 2:
				texto = 'Febrero';
				x = '02';
				break;
			case 3:
				texto = 'Marzo';
				x = '03';
				break;
			case 4:
				texto = 'Abril';
				x = '04';
				break;
			case 5:
				texto = 'Mayo';
				x = '05';
				break;
			case 6:
				texto = 'Junio';
				x = '06';
				break;
			case 7:
				texto = 'Julio';
				x = '07';
				break;
			case 8:
				texto = 'Agosto';
				x = '08';
				break;
			case 9:
				texto = 'Septiembre';
				x = '09';
				break;
			case 10:
				texto = 'Octubre';
				break;
			case 11:
				texto = 'Noviembre';
				break;
			case 12:
				texto = 'Diciembre';
				break;
		}

		$('#mes').append('<option value="' + llear + '-' + x + '" selected="selected">' + texto + ' ' + (llear) + '</option>');
	}


	for (var x = (mes + 1); x <= 12; x++) {

		var texto = '';
		switch (x) {
			case 1:
				texto = 'Enero';
				x = '01';
				break;
			case 2:
				texto = 'Febrero';
				x = '02';
				break;
			case 3:
				texto = 'Marzo';
				x = '03';
				break;
			case 4:
				texto = 'Abril';
				x = '04';
				break;
			case 5:
				texto = 'Mayo';
				x = '05';
				break;
			case 6:
				texto = 'Junio';
				x = '06';
				break;
			case 7:
				texto = 'Julio';
				x = '07';
				break;
			case 8:
				texto = 'Agosto';
				x = '08';
				break;
			case 9:
				texto = 'Septiembre';
				x = '09';
				break;
			case 10:
				texto = 'Octubre';
				break;
			case 11:
				texto = 'Noviembre';
				break;
			case 12:
				texto = 'Diciembre';
				break;
		}

		$('#mes').append('<option value="' + (llear - 1) + '-' + x + '">' + texto + ' ' + (llear - 1) +'</option>');
	}




	$('#generar').click(function () {
		var inicio = $('#mes').find(":selected").val();
		console.log(inicio);
		var url = "../Docencia/Listar/generarExcel/" + inicio;
		window.open(url);

	});

	$('#mes').on('change', function () {

	 
		$.ajax({
			async: false,
			cache: false,
			dataType: "json",
			type: 'POST',
		data: {fecha: this.value},
//			llear: llear},
			error: function (jqXHR, textStatus, errorThrown) {
				// code en caso de error de la ejecucion del ajax
				console.log(textStatus + errorThrown);
			},
			success: function (result) {
				cargaTabla(result);
			},
			url: "Listar/ListarChange"
		});

	});

});


function cargaDatosFicha() {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
//		data: {mes: x,
//			llear: llear},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTabla(result);
		},
		url: "Listar/Listar"
	});
}


function cargaTabla(data) {
	if ($.fn.dataTable.isDataTable('#tbl_fichas')) {
		$('#tbl_fichas').DataTable().destroy();
	}

	$('#tbl_fichas').DataTable({
		"aaData": data,
		"aoColumns": [
			{"mDataProp": "num_ficha"},
			{"mDataProp": "estado"},
			{"mDataProp": "fecha_inicio"},
			{"mDataProp": "fecha_fin"},
			{"mDataProp": "fecha_rectificacion"},
			{"mDataProp": "nombre_curso"},
			{"mDataProp": "razon_social_empresa"}

		],
		pageLength: 100,
		responsive: true,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [
			{
				extend: 'copy'
			}, {
				extend: 'csv'
			}, {
				extend: 'excel',
				title: 'Empresa'
			}, {
				extend: 'pdf',
				title: 'Empresa'
			}, {
				extend: 'print',
				customize: function (win) {
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					$(win.document.body).find('table')
							.addClass('compact')
							.css('font-size', 'inherit');
				}
			}
		]
	});
}




