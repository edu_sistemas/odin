$(document).ready(function () {

    llamaDatosEdutecno();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_buscar_edutecno").click(function () {
        llamaDatosEdutecno();
    });

    $("#cbx_holding").select2({
        placeholder: "Todos",
        allowClear: true
    });

});



function llamaDatosEdutecno() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaEdutecno(result);
        },
        url: "Listar/buscarEdutecno"
    });
}


function cargaTablaEdutecno(data) {

    if ($.fn.dataTable.isDataTable('#tbl_edutecno')) {
        $('#tbl_edutecno').DataTable().destroy();
    }

    $('#tbl_edutecno').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_edutecno"},
            {"mDataProp": "rut_edutecno"},
            {"mDataProp": "razon_social_edutecno"},
            {"mDataProp": "giro_edutecno"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {

                    var html = "";
                    html = '<i title="Inactivo" class="fa fa-circle text-danger"></i>';
                    if (o.estado_edutecno == 1) {
                        html = '  <i title="Activo"  class="fa fa-circle text-navy"></i>';
                    }
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var x = "'Editar/index/" + o.id_edutecno + "'";


                    return '<button class="btn-primary"  onclick="location.href=' + x + '">Editar</button>';
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

