$(document).ready(function () {

    recargaLaPagina();
    $("#btn_nueva_comision").click(function () {
        location.href = 'AgregarComision';
    });

    llamaDatosComisiones();

});



function carga_tipo_usuario() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo('cbx_tipo_usuario', result);
        },
        url: "Listar/getTipoUsuarioComisionCBX"
    });
}
function llamaDatosComisiones() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        // data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            cargaTablaComisionDetalle(result);
        },
        url: "ListarComisiones/buscarComisionUsuarios"
    });


}
function cargaTablaComisionDetalle(data) {
    if ($.fn.dataTable.isDataTable('#tbl_detalle_comision_usuarios')) {
        $('#tbl_detalle_comision_usuarios').DataTable().destroy();
    }
    /*<th>Usuario</th>
     <th>Tipo Usuario</th>                                 
     <th>% Comision Cursos</th>
     <th>% Comision Arriendos</th>
     <th>% Comision Jefatura</th>
     <th>% Comision  Especial Nicole</th>
     <th>Validad Apartir de:</th>
     <th>Fecha Termino</th>   */

    $('#tbl_detalle_comision_usuarios').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "usuario"},
            {"mDataProp": "tipo"},
            {"mDataProp": "porcent_comision_curso"},
            {"mDataProp": "porcent_comision_arriendo"},
            {"mDataProp": "porcent_comision_jefatura"},
            {"mDataProp": "porcent_comision_otro"},
            {"mDataProp": "valida_apartir_de"},
            {"mDataProp": "fecha_termino"}

        ],
        pageLength: 50,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Comisiones Usuarios'
            }, {
                extend: 'pdf',
                title: 'Comisiones Usuarios'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}


 