$(document).ready(function () {

    recargaLaPagina();
    cargaTodosDatosExtras();

    $('#comision_valida_apartir_de').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    $('#btn_registrar').click(function(){
        validaComision();
    });
    $('#btn_reset').click(function(){
       $('#usuario').val('').trigger("change");
    });
});

function  cargaTodosDatosExtras() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {

            cargaComboGenerico(result, 'usuario');
        },
        url: "AgregarComision/ListarUsuarioSistema"
    });


}
function cargaComboGenerico(midata, id) {

    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

    $("#" + id + " option[value='0']").remove();

}


function validaComision(){
    if(validarRequeridoForm($('#frm_ingreso_comision'))) {        
        inserta_comision();        
    }
    
}

function inserta_comision(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_ingreso_comision').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            swal(result[0].titulo,result[0].mensaje,result[0].tipo_alerta);
            $('#btn_reset').click();
        },
        url: "AgregarComision/insert_comision"
    });
}




 