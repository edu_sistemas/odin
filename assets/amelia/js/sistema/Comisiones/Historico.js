/* Ultima edicion: 2018 - 01 - 04[Jessica Roa]<jroa@edutecno.com>*/

$(document).ready(function () {
    $("#calendario_mes").datepicker("setDate", '-2m');

    $("#btn_get_informe").click(function () {
        id_tipo = $("#cbx_tipo_usuario").val();
        id_ejecutivo = $("#cbx_ejecutivo").val();
        fecha = $("#calendario_mes").val();
        if (id_tipo != '' && id_ejecutivo != '' && fecha != '') {
            window.open('Historico/generar/?id_tipo=' + id_tipo + '&id_ejecutivo=' + id_ejecutivo + '&fecha=' + fecha, '_blank');
        } else {
            swal({
                title: "Faltan datos",
                text: "Debe seleccionar un tipo, ejecutivo y fecha",
                type: "info",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok!",
                closeOnConfirm: true
            });
        }
    });

    $("#btn_buscar_comision").click(function () {
        llamaDatosComisiones();
        listarRectificaciones();
    });

    $("#cbx_tipo_usuario").change(function () {

        if ($("#cbx_tipo_usuario").val() != "") {

            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: { id_tipo_usuario: $("#cbx_tipo_usuario").val() },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {

                    cargaCombo('cbx_ejecutivo', result);
                },
                url: "Historico/getUsuarioComisionCBX"
            });
        }
    });
    carga_tipo_usuario();
    // listarRectificaciones();
});


//DateIcker para fechas con solo mes y año
$('#calendario_mes').datepicker({
    format: "mm-yyyy",
    endDate: '-2m',
    defaultViewDate: '-2m',
    minViewMode: 1,
    maxViewMode: 1,
    todayBtn: false,
    language: "es"
});

// evento keypress bloqueado para que no ingresen valores por teclado
$('#calendario_mes').keypress(function (e) {
    e.preventDefault();
});


function cargaCombo(id, midata) {
    $("#" + id).html('').select2({ data: [{ id: '', text: '' }] });
    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}

function carga_tipo_usuario() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo('cbx_tipo_usuario', result);
        },
        url: "Historico/getTipoUsuarioComisionCBX"
    });
}
function llamaDatosComisiones() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        beforeSend: function () {
            $("#cargando").html('<img src="../../assets/img/timer.gif">');
            $("#btn_buscar_comision").attr('disabled', true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            cargaTablaComision(result);
        },
        complete: function () {
            $("#cargando").html("");
            $("#btn_buscar_comision").attr('disabled', false);
        },
        url: "Historico/buscarComisionGlobal"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        beforeSend: function () {
            $("#cargando").html('<img src="../../assets/img/timer.gif" style="width: 30%;">');
            $("#btn_buscar_comision").attr('disabled', true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            cargaTablaComisionDetalle(result);
        },
        complete: function () {
            $("#cargando").html("");
            $("#btn_buscar_comision").attr('disabled', false);
        },
        url: "Historico/getHistoricoComisiones"
    });
}


function listarRectificaciones() {
    var mesrg = $('#calendario_mes').val().split('-');
    console.log(mesrg);
    console.log($('#cbx_tipo_usuario').val() + 'eje ' + $('#cbx_ejecutivo').val());
    var rgfinal = [];
    if (mesrg[0] < 11) {
        rgfinal[0] = (parseInt(mesrg[0]) + 2);
        mesrg[0] = (parseInt(mesrg[0]) + 1);
        rgfinal[1] = mesrg[1];
    } else {
        if (mesrg[0] == 12) {
            mesrg[0] = '01';
            mesrg[1] = (parseInt(mesrg[1]) + 1);
            rgfinal[0] = '02';
            rgfinal[1] = (mesrg[1]);
        } else {
            if (mesrg[0] == 11) {
                mesrg[0] = (parseInt(mesrg[0]) + 1);
                rgfinal[0] = '01';
                rgfinal[1] = (parseInt(mesrg[1]) + 1);
            }
        }
    }
    var res = [];
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            console.log(result);
            console.log(mesrg[1].toString().substr(2));
            $('.rango').html('Del: 21-' + mesrg[0] + '-' + mesrg[1].toString().substr(2) + '<br>Al: 20-' + rgfinal[0] + '-' + rgfinal[1].toString().substr(2));
            cargaTablaDescuentos(result);
        },
        url: "Historico/getHistoricoRectificaciones"
    });
}
    
function cargaTablaComision(data) {
    if ($.fn.dataTable.isDataTable('#tbl_comision')) {
        $('#tbl_comision').DataTable().destroy();
    }
    $('#tbl_comision').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "ejecutivo" },
            { "mDataProp": "valor_venta_activa" },
            { "mDataProp": "valor_total_eliminados" },
            { "mDataProp": "total_participantes" },
            { "mDataProp": "total_descuentos_por_productos" },
            { "mDataProp": "%comision" },
            { "mDataProp": "comision_neta" }

        ],
        pageLength: 50,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

function cargaTablaComisionDetalle(data) {

    if ($.fn.dataTable.isDataTable('#tbl_detalle_comision')) {
        $('#tbl_detalle_comision').DataTable().destroy();
    }
    $('#tbl_detalle_comision').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "num_ficha" },
            { "mDataProp": "ejecutivo" },
            { "mDataProp": "tipo" },
            { "mDataProp": "empresa" },
            { "mDataProp": "fecha_venta" },
            { "mDataProp": "valor_total_eliminados" },
            { "mDataProp": "valor" },
            { "mDataProp": "total_participantes" },
            { "mDataProp": "valor_venta_activa" },
            { "mDataProp": "tipo_producto" },
            { "mDataProp": "descuento" },
            { "mDataProp": "total_descuentos" },
            { "mDataProp": "venta_neta" },
            { "mDataProp": "%comision" },
            { "mDataProp": "comision_neta" }

        ],
        pageLength: 50,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

function cargaTablaDescuentos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_descuentos_productos')) {
        $('#tbl_descuentos_productos').DataTable().destroy();
    }
    $('#tbl_descuentos_productos')
        .DataTable(
        {
            "aaData": data,
            "aoColumns": [
                { "mDataProp": "nom_ejecutivo" },
                { "mDataProp": "num_ficha" },
                { "mDataProp": "fecha_inicio" },
                { "mDataProp": "monto_rectificacion" },
                // { "mDataProp": "tipo_producto" },
                { "mDataProp": "val_final" },
                { "mDataProp": "descuento" },
                { "mDataProp": "total_descuentos" },
                { "mDataProp": "venta_neta" },
                //  { "mDataProp": "%comision" },
                { "mDataProp": "comision_neta" },
                { "mDataProp": "comision_pagada" },
                { "mDataProp": "diferencia_comision" }
            ],
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'Empresa'
                },
                {
                    extend: 'pdf',
                    title: 'Empresa'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass(
                            'white-bg');
                        $(win.document.body).css('font-size',
                            '10px');
                        $(win.document.body).find('table')
                            .addClass('compact').css(
                            'font-size', 'inherit');
                    }
                }]
        });
}

