 /* Ultima edicion: 2018 - 01 - 04[Jessica Roa]<jroa@edutecno.com>*/

$(document).ready(function () {

    recargaLaPagina();

    $('#calendario_mes').datepicker({
        format: "mm-yyyy",
        minViewMode: 1,
        maxViewMode: 1,
        todayBtn: false,
        language: "es"
    });
    $("#btn_get_informe").click(function(){
        id_tipo = $("#cbx_tipo_usuario").val();
        id_ejecutivo = $("#cbx_ejecutivo").val();
        fecha = $("#calendario_mes").val();
        if (id_tipo != '' && id_ejecutivo != '' && fecha != '') {
            window.open('Listar/generar/?id_tipo=' + id_tipo + '&id_ejecutivo=' + id_ejecutivo + '&fecha=' + fecha, '_blank');
        } else {
            swal({
                title: "Faltan datos",
                text: "Debe seleccionar un tipo, ejecutivo y fecha",
                type: "info",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok!"
            });
        }
    });

    $("#btn_set_descuento").click(function () {
        id_tipo = $("#cbx_tipo_usuario").val();
        id_ejecutivo = $("#cbx_ejecutivo").val();
        fecha = $("#calendario_mes").val();
        $("#btn_set_descuento").attr("disabled", true);
        if (id_tipo != '' && id_ejecutivo != '' && fecha != '') {
            swal({
                title: "Estás seguro?",
                text: $('#tit_historico_rectificaciones').text() == "No se ha generado histórico" ? 
                    "Registrará en Histórico los datos reflejados en la pantalla" : 
                    "Reemplazará el histórico anterior generado!" ,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Guardar!",
                cancelButtonText: "No, Cancelar!"
            }).then(function () {
                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: $('#frm').serialize(),
                        error: function (jqXHR, textStatus, errorThrown) {
                            console.log(textStatus + errorThrown);
                        },
                        success: function (result) {
                            console.log(result);
                            swal({
                                title: result[0].titulo,
                                text: result[0].mensaje,
                                type: result[0].tipo_alerta,
                                showCancelButton: false,
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "Ok!"
                            });
                        },
                        url: "Listar/setHistorico"
                    }); 
            });
        } else {
            swal({
                title: "Faltan datos",
                text: "Debe seleccionar un tipo, ejecutivo y fecha",
                type: "info",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Ok!"
            });
        }
        $("#btn_set_descuento").attr("disabled", false);
    });
    
    
    $("#btn_buscar_comision").click(function () {
        $('#tit_historico_comisiones').html("");
        $('#tit_historico_rectificaciones').html("");
        llamaDatosComisiones();
        listarRectificaciones();
    });
    
    $("#cbx_tipo_usuario").change(function () {
        
        if ($("#cbx_tipo_usuario").val() != "") {
            
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {id_tipo_usuario: $("#cbx_tipo_usuario").val()},
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    
                    cargaCombo('cbx_ejecutivo', result);
                },
                url: "Listar/getUsuarioComisionCBX"
            });
        }
    });
    carga_tipo_usuario();
   // listarRectificaciones();
});



function carga_tipo_usuario() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo('cbx_tipo_usuario', result);
        },
        url: "Listar/getTipoUsuarioComisionCBX"
    });
}

function llamaDatosComisiones() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        beforeSend: function () {
            $("#cargando").html('<img src="../../assets/img/timer.gif">');
            $("#btn_buscar_comision").attr('disabled', true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            cargaTablaComision(result);
        },
        complete: function () {
            $("#cargando").html("");
            $("#btn_buscar_comision").attr('disabled', false);
        },
        url: "Listar/buscarComisionGlobal"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        beforeSend: function () {
            $("#cargando").html('<img src="../../assets/img/timer.gif" style="width: 30%;">');
            $("#btn_buscar_comision").attr('disabled', true);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            cargaTablaComisionDetalle(result);
        },
        complete: function () {
            $("#cargando").html("");
            $("#btn_buscar_comision").attr('disabled', false);
        },
        url: "Listar/buscarComisionDetalle"
    });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            console.log(result);
          //  $('#tit_historico_comisiones').html("Histórico generado el:");
        if (result.length > 0) {
            $('#tit_historico_comisiones').html("Histórico generado el: " + result[0].fecha_registro_historico);
        } else {
            $('#tit_historico_comisiones').html("No se ha generado histórico");
        }
        },
        url: "Listar/getFechaUltimoHistoricoComision"
    });

}


function cargaTablaComision(data) {

    if ($.fn.dataTable.isDataTable('#tbl_comision')) {
        $('#tbl_comision').DataTable().destroy();
    }
    /*
     id_usuario
     ,ejecutivo
     ,SUM(valor_venta_activa) AS valor_venta_activa 
     ,SUM(valor_total_eliminados) AS valor_total_eliminados 
     ,SUM(total_participantes) AS total_participantes
     ,SUM(descuento) AS total_descuentos_por_productos
     ,porcent_comision AS  '%comision' 
     ,SUM(comision_neta) AS comision_neta
     ,mes
     ,anio
     *//*
      <th>Ejecutivo</th>
      <th>Total Ventas</th>
      <th>Total Eliminados</th>
      <th>Total Participantes</th>
      <th>Total Descuentos</th>
      <th>% Comision</th>
      <th>Comision Neta</th>*/

    $('#tbl_comision').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "ejecutivo"},
            {"mDataProp": "valor_venta_activa"},
            {"mDataProp": "valor_total_eliminados"},
            {"mDataProp": "total_participantes"},
            {"mDataProp": "total_descuentos_por_productos"},
            { "mDataProp": "tot_rectificaciones"},
            {"mDataProp": "%comision"},
            {"mDataProp": "comision_neta"}

        ],
        pageLength: 50,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

function cargaTablaComisionDetalle(data) {

    if ($.fn.dataTable.isDataTable('#tbl_detalle_comision')) {
        $('#tbl_detalle_comision').DataTable().destroy();
    }
    /*
     usu.id_usuario
     , CONCAT(usu.nombre_usuario, ' ',usu.apellidos_usuario) AS ejecutivo 
     , f.num_ficha
     , IF(f.id_categoria = 1 , 'Curso' ,'Arriendo') AS tipo
     , empresa.razon_social_empresa AS empresa
     , DATE_FORMAT(oc.fecha_inicio , '%d-%m-%Y')  AS fecha_venta 
     , f_get_valor_total_eliminado_by_ficha(f.id_ficha) AS valor_total_eliminados
     , dtl.valor_cobrado AS valor
     , f_get_cantidad_alumnos_ficha(f.id_ficha) AS total_participantes
     , f_get_valor_total_by_ficha(f.id_ficha)  AS valor_venta_activa
     , descuento.nombre_sub_linea_negocio AS tipo_producto
     , descuento.valor_descuento AS descuento
     , (descuento.valor_descuento * f_get_cantidad_alumnos_ficha(f.id_ficha)) AS total_descuentos
     , ( f_get_valor_total_by_ficha(f.id_ficha) -
     (descuento.valor_descuento*f_get_cantidad_alumnos_ficha(f.id_ficha))
     ) AS venta_neta
     ,f_get_comision_usuario(usu.id_usuario,f.id_categoria,MONTH(oc.fecha_inicio),YEAR(oc.fecha_inicio)) AS '%comision'
     , ROUND( 
     ( f_get_valor_total_by_ficha(f.id_ficha) -
     (descuento.valor_descuento*f_get_cantidad_alumnos_ficha(f.id_ficha))
     )  * f_get_comision_usuario(usu.id_usuario,f.id_categoria,MONTH(oc.fecha_inicio),YEAR(oc.fecha_inicio)) 
     ) AS comision_neta
     
     , MONTHNAME(oc.fecha_inicio) AS mes
     , YEAR(oc.fecha_inicio) AS anio
     
     
     <th>Ficha</th>
     <th>Tipo</th>
     <th>Empresa</th>
     <th>Fecha Venta</th>
     <th>Valor Eliminados</th>
     <th>Valor Part.</th>
     <th>Cant. Participantes</th>
     <th>Total Fichas Vigente</th>
     <th>Tipo Producto</th>
     <th>Descuento Unitario</th>
     <th>Total Descuentos</th>
     <th>Venta Neta</th>
     <th>%Comision</th>
     <th>Comision Neta</th>
     */
    $('#tbl_detalle_comision').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "ejecutivo"},
            {"mDataProp": "tipo"},
            {"mDataProp": "empresa"},
            {"mDataProp": "fecha_venta"},
            {"mDataProp": "valor_total_eliminados"},
            {"mDataProp": "valor"},
            {"mDataProp": "total_participantes"},
            {"mDataProp": "valor_venta_activa"},
            {"mDataProp": "tipo_producto"},
            {"mDataProp": "descuento"},
            {"mDataProp": "total_descuentos"},
            {"mDataProp": "venta_neta"},
            {"mDataProp": "%comision"},
            {"mDataProp": "comision_neta"}

        ],
        pageLength: 50,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}


function cargaCombo(id, midata) {
    $("#" + id).html('').select2({data: [{id: '', text: ''}]});
    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}


function cargaTablaDescuentos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_descuentos_productos')) {
        $('#tbl_descuentos_productos').DataTable().destroy();
    }
    $('#tbl_descuentos_productos')
        .DataTable(
        {
            "aaData": data,
            "aoColumns": [
                { "mDataProp": "nom_ejecutivo" },
                { "mDataProp": "num_ficha" },
                { "mDataProp": "fecha_inicio" },
                { "mDataProp": "monto_rectificacion" },
               // { "mDataProp": "tipo_producto" },
               { "mDataProp": "val_final" },
                { "mDataProp": "descuento" },
                { "mDataProp": "total_descuentos" },
                { "mDataProp": "venta_neta" },
              //  { "mDataProp": "%comision" },
                { "mDataProp": "comision_neta" },
                { "mDataProp": "comision_pagada" },
                { "mDataProp": "diferencia_comision" }
            ],
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'Empresa'
                },
                {
                    extend: 'pdf',
                    title: 'Empresa'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass(
                            'white-bg');
                        $(win.document.body).css('font-size',
                            '10px');
                        $(win.document.body).find('table')
                            .addClass('compact').css(
                            'font-size', 'inherit');
                    }
                }]
        });
}


function listarRectificaciones() {
    var mesrg = $('#calendario_mes').val().split('-');
    console.log(mesrg);
    console.log($('#cbx_tipo_usuario').val() + 'eje ' + $('#cbx_ejecutivo').val());
    var rgfinal = [];
    if (mesrg[0] < 11) {
        rgfinal[0] = (parseInt(mesrg[0]) + 2);
        mesrg[0] = (parseInt(mesrg[0]) + 1);
        rgfinal[1] = mesrg[1];
    } else {
        if (mesrg[0] == 12) {
            mesrg[0] = '01';
            mesrg[1] = (parseInt(mesrg[1]) + 1);
            rgfinal[0] = '02';
            rgfinal[1] = (mesrg[1]);
        } else {
            if (mesrg[0] == 11) {
                mesrg[0] = (parseInt(mesrg[0]) + 1);
                rgfinal[0] = '01';
                rgfinal[1] = (parseInt(mesrg[1]) + 1);
            }
        }
    }
    var res = [];
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            console.log(result);
            console.log(mesrg[1].toString().substr(2));
            $('.rango').html('Del: 21-' + mesrg[0] + '-' + mesrg[1].toString().substr(2) + '<br>Al: 20-' + rgfinal[0] + '-' + rgfinal[1].toString().substr(2));
            cargaTablaDescuentos(result);
        },
        url: "Listar/listarRectificaciones"
    });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            console.log(result);
            if (result.length > 0){
                $('#tit_historico_rectificaciones').html("Histórico generado el: " + result[0].fecha_registro_historico);
            }else{
                $('#tit_historico_rectificaciones').html("No se ha generado histórico");
            }
        },
        url: "Listar/getFechaUltimoHistoricoRectificaiones"
    });
}
