$(document).ready(function () {
    //ListarFichas();
    getCBX('select_otic', 'sp_select_tipo_entrega_cbx');


    
    $("#formulario").submit(function(){
        enviarFormulario('formulario', 'sp_test_insert');
        event.preventDefault();
    });
    
});


function cargaDataTable(Data, opciones){

}


function ListarFichas() {
    $.ajax({
        url: "Listar/getFichas",
        type: 'POST',
        dataType: "json",
        data: $('#frm_busqueda_personalizada').serialize(),
        beforeSend: function () {
            $("#cargando").html('<img src="../../assets/img/loading.gif">');
        },
        success: function (result) {
            cargaTablaFichas(result);
            $("#contenido").css("display", "block");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar las fichas: "+errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        complete: function () {
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });
}
function cargaTablaFichas(data) {
    if ($.fn.dataTable.isDataTable('#tbl_fichas')) {
        $('#tbl_fichas').DataTable().destroy();
    }

    $('#tbl_fichas').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "num_ficha" },
            { "mDataProp": "nombre_curso" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "fecha_cierre" },
            { "mDataProp": "estado" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var x = "'Ver_Curso/index/" + o.id_ficha + "'";
                    html += '<div class="btn-group">';
                    html += '<button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">Opciones <span class="caret"></span></button>';
                    html += '<ul class="dropdown-menu">';
                    html += '<li><a onclick="location.href=' + x + '">Ver_Curso</a></li>';
                    html += '</ul>';
                    html += '</div>';
                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Categoria'
            }, {
                extend: 'pdf',
                title: 'Categoria'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        order: [[0, "desc"]]
    });
}