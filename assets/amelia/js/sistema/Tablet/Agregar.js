/**
 * Tablet
 *
 * Description...
 *
 * @version 0.0.1
 * Fecha creacion:	2018-03-06 [Jessica Roa] <jroa@edutecno.com>
 */

$(document).ready(function () {
    iniciopag();
    $("#proveedor").attr('autocomplete','on');
    $("#proveedor").autocomplete({
        source: "Agregar/buscarEmpresa",
        select: function( event, ui ) {
            event.preventDefault();
            $("#proveedor").val(ui.item.value);
            $("#cbx_proveedor").val(ui.item.id);
        }
    });
});

function iniciopag() {
    cargaInventario();
    cargaIngreso();
    cargacCBXAll();

    $("#btn_nuevo").click(function () {
        $('#NuevoModal').modal();
    });

    $("#btn_ingreso").click(function () {
        $('#IngresoModal').modal();
        IngresoTablet();
    });

    $("#frm_nuevo_modelo").submit(function () {
        AgregarModelo();
        event.preventDefault();
    });
    $("#form_insert_cantidad").submit(function () {
        AgregarIngreso();
        event.preventDefault();
    });

    $("#btn_eliminar_ingreso").click(function () {
        BorrarIngreso();
    });

    $('#datepicker_ingreso_tablet').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
    $('#datetimepickerTermino').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
    
}

function cargaInventario() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosInventario(result);
        },
        url: "Agregar/getInventario"
    });
}

function cargacCBXAll(){
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, 'gamma');
        },
        url: "Agregar/getGammaCBX"
    });

    // $.ajax({
    //     async: true,
    //     cache: false,
    //     dataType: "json",
    //     type: 'GET',
    //     error: function (jqXHR, textStatus, errorThrown) {
    //         // code en caso de error de la ejecucion del ajax
    //         console.log(textStatus + errorThrown + jqXHR);
    //     },
    //     success: function (result) {
    //         console.log(result);
    //         cargaCombo(result, 'proveedor');
    //     },
    //     url: "Agregar/getProveedorCBX"
    // });
}

function cargaDatosInventario(data) {
    if ($.fn.dataTable.isDataTable('#tbl_Inventario_tablet')) {
        $('#tbl_Inventario_tablet').DataTable().destroy();
    }

    $('#tbl_Inventario_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "marca" },
            { "mDataProp": "modelo" },
            { "mDataProp": "gamma" },
            { "mDataProp": "descripcion" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "cantidad_solicitada" },
            { "mDataProp": "cantidad_necesaria" },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                var html = "";
                // html += '<button class="btn btn-primary btn-xs" onclick="getDescripcionTecnicaTablet('+o.id_modelo+')">Descripción Técnica</button></a></div>';
                // html += '<button class="btn btn-danger btn-xs" onclick="EliminarTablet(' + o.id_modelo +')">Eliminar</button></a></div>';

                html += '<div class="dropdown">';
                html += '<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></button>';
                html += '<ul class="dropdown-menu pull-right">';

                html += '<li><a href="#" onclick="getDescripcionTecnicaTablet(' + o.id_modelo +')">Descripción Técnica</a></li>';
                html += '<li><a href="#" onclick="EliminarTablet(' + o.id_modelo +')">Eliminar</a></li></ul></div>';
                return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[5, "asc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function getDescripcionTecnicaTablet(id_modelo) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_modelo: id_modelo},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            $('#titulo_descripcion_tecnica').text('');
            $('#texto_descripcion_tecnica').text('');
            $('#titulo_descripcion_tecnica').text('Descripción técnica del equipo ' + result[0].marca + ' ' + result[0].modelo);
            $('#texto_descripcion_tecnica').text(result[0].descripcion_tecnica);

            $("#modal_descripcion_tecnica").modal('show');
        },
        url: "Agregar/getModeloByID"
    });
}

// function cargaSolicitud() {
//     $.ajax({
//         async: true,
//         cache: false,
//         dataType: "json",
//         type: 'GET',
//         error: function (jqXHR, textStatus, errorThrown) {
//             // code en caso de error de la ejecucion del ajax
//             console.log(textStatus + errorThrown + jqXHR);
//         },
//         success: function (result) {
//             console.log(result);
//             cargaDatosSolicitudes(result);
//         },
//         url: "Agregar/getSolicitudes"
//     });
// }

// function cargaDatosSolicitudes(data) {
//     if ($.fn.dataTable.isDataTable('#tbl_Solicitud_tablet')) {
//         $('#tbl_Solicitud_tablet').DataTable().destroy();
//     }

//     $('#tbl_Solicitud_tablet').DataTable({
//         "language": {
//             "decimal": ",",
//             "thousands": "."
//         },
//         "aaData": data,
//         "aoColumns": [
//             { "mDataProp": "id_solicitud" },
//             { "mDataProp": "nombre_solicitante" },
//             { "mDataProp": "fecha_solicitud" },
//             { "mDataProp": "cantidad" },
//             { "mDataProp": "modelo_tablet" },
//             { "mDataProp": "aplicacion" },
//             { "mDataProp": "num_ficha" },
//             { "mDataProp": "fecha_inicio" },
//             { "mDataProp": "direccion_entrega" },
//             { "mDataProp": "estado_solicitud" },
//             { "mDataProp": "observacion_solicitud" },
//             {
//                 "mDataProp": null, "bSortable": false, "mRender": function (o) {
//                     var html = "";
//                     // html += '<button class="btn btn-primary btn-xs" onclick="getDescripcionTecnicaTablet('+o.id_modelo+')">Descripción Técnica</button></a></div>';
//                     // html += '<button class="btn btn-danger btn-xs" onclick="EliminarTablet(' + o.id_modelo +')">Eliminar</button></a></div>';

//                     html += '<div class="dropdown">';
//                     html += '<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></button>';
//                     html += '<ul class="dropdown-menu pull-right">';

//                     html += '<li><a href="#" onclick="x(' + o.id_modelo + ')">Generar Reporte</a></li></ul></div>';
//                     return html;
//                 }
//             }
//         ],
//         pageLength: 25,
//         responsive: false,
//         dom: '<"html5buttons"B>lTfgitp',
//         buttons: [
//             {
//                 extend: 'copy'
//             }, {
//                 extend: 'csv'
//             }, {
//                 extend: 'excel',
//                 title: 'Edutecno'
//             }, {
//                 extend: 'pdf',
//                 title: 'Edutecno'
//             }, {
//                 extend: 'print',
//                 customize: function (win) {
//                     $(win.document.body).addClass('white-bg');
//                     $(win.document.body).css('font-size', '10px');
//                     $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
//                 }
//             }
//         ],
//         order: [[0, "desc"]]
//     });

//     $('[data-toggle="tooltip"]').tooltip();
// }


function cargaIngreso() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosIngresos(result);
        },
        url: "Agregar/getIngresos"
    });
}

function cargaDatosIngresos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_Ingresos_tablet')) {
        $('#tbl_Ingresos_tablet').DataTable().destroy();
    }

    $('#tbl_Ingresos_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "marca" },
            { "mDataProp": "modelo" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "fecha_ingreso" },
            { "mDataProp": "descripcion" },
            { "mDataProp": null, "bSortable": false, "mRender": function (o) {
                var html = "";
                    html += '<div class="dropdown">';
                    html += '<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></button>';
                    html += '<ul class="dropdown-menu pull-right">';
                    html += '<li><a href="#" onclick="delIngreso(' + o.id_cantidad + ');">Eliminar</a></li></ul></div>';
                return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[3, "desc"]]
    });
    $('[data-toggle="tooltip"]').tooltip();
}

function delIngreso(id_cantidad) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_cantidad: id_cantidad },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            $('#ref_mod').text(result[0].marca + ' Modelo: ' + result[0].modelo);
            $('#ref_mod_dat').text('Cantidad: ' + result[0].cantidad + ' de fecha: ' + result[0].fecha_ingreso);
            $('#id_ingreso_eliminar').val(id_cantidad);
        },
        url: "Agregar/getTabletCantidadByID"
    });

    $('#delModal').modal();
}

function IngresoTablet() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, 'modelo');
        },
        url: "Agregar/getModelos"
    });
}

function cargaCombo(midata, id) {
    $("#cbx_" + id).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
}

function AgregarModelo() {
    /*  
     
     */
    console.log('agregar' + $("#marca_tablet").val() + $("#modelo_tablet").val() + $("#detalles_tablet").val());
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#frm_nuevo_modelo").serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(  textStatus + errorThrown);
           // alerta(textStatus, errorThrown, 'error');
        },
        success: function (result) {

            // alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (result[0].reset == 1) {

            //     setTimeout(function () {
            //         location.reload();
            //     }, 2000);

            // }

            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: false
            },
                function () {
                    if (result[0].reset == 1) {
                            location.reload();
                    }
                });
 
        },
        url: "Agregar/AgregarModelo"
    });

}

function EliminarTablet(id_modelo){
    swal({
        title: "Está seguro?",
        text: "Se procederá a eliminar el modelo seleccionado",
        type: "warning",
        showCancelButton: true,
        //confirmButtonClass: "btn-danger",
        confirmButtonText: "Eliminar",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                async: true,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {id_modelo: id_modelo},
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown + jqXHR);
                },
                success: function (result) {
                    swal({
                        title: result[0].titulo,
                        text: result[0].mensaje,
                        type: result[0].tipo_alerta,
                        showCancelButton: false,
                        //confirmButtonClass: "btn-danger",
                        confirmButtonText: "Aceptar",
                        closeOnConfirm: true
                    },
                        function () {
                            if (result[0].reset == 1) {
                                cargaInventario();
                            }
                        });
                },
                url: "Agregar/EliminarModelo"
            });
        });
}

function AgregarIngreso() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#form_insert_cantidad").serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Ingreso', errorThrown, 'error');
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        cargaIngreso();
                    }
                });
        },
        url: "Agregar/AgregarIngreso"
    });

}

function BorrarIngreso() {
    /*  
     
     */
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ingreso_eliminar: $("#id_ingreso_eliminar").val(),
            descripcion_eliminar: $("#descripcion_eliminar").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Eliminar', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {

                setTimeout(function () {
                    location.reload();
                }, 2000);

            }
        },
        url: "Agregar/EliminarIngreso"
    });

}