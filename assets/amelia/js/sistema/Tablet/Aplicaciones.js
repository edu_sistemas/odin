/**
 * Tablet
 *
 * Description...
 *
 * @version 0.0.1
 * Fecha creacion:	2018-03-09 [Jessica Roa] <jroa@edutecno.com>
 */
var contador = 0;
var index = 0;
$(document).ready(function () {
    iniciopag();
});

function iniciopag() {
    cargaInventario();
    cargaSolicitud();
    cargaSolicitudHistorico();

    $("#frm_despacho_tablet").submit(function () {
        cambiarEstado();
        event.preventDefault();
    });

    $('#datepicker_ingreso_tablet').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
    $('#datetimepickerTermino').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
    $('.hasTooltip').each(function () {
        $(this).qtip({
            content: {
                text: $(this).next('div')
            }
        });
    });
}

function cargaInventario() {
    /* funcion que hace la consulta del inventario de las tablets en la bd*/
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosInventario(result);
        },
        url: "Aplicaciones/getInventario"
    });
}

function cargaDatosInventario(data) {
    /* carga los datos traidos en la tabla de inventario de tablets */
    if ($.fn.dataTable.isDataTable('#tbl_Inventario_tablet')) {
        $('#tbl_Inventario_tablet').DataTable().destroy();
    }

    $('#tbl_Inventario_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "marca" },
            { "mDataProp": "modelo" },
            { "mDataProp": "descripcion" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "cantidad_solicitada" },
            { "mDataProp": "cantidad_necesaria" }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[5, "asc"]]
    });
}

function cargaSolicitud() {
    /* funcion que hace la consulta de solicitudes de tablets en la bd*/
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosSolicitudes(result);
        },
        url: "Aplicaciones/getSolicitudes"
    });
}

function cargaDatosSolicitudes(data) {
    console.log(data);
    /* carga los datos traidos en la tabla de solicitudes de tablets */
    if ($.fn.dataTable.isDataTable('#tbl_Solicitud_tablet')) {
        $('#tbl_Solicitud_tablet').DataTable().destroy();
    }

    $('#tbl_Solicitud_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "id_solicitud" },
            { "mDataProp": "nombre_solicitante" },
            { "mDataProp": "fecha_solicitud" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "modelo_tablet" },
            { "mDataProp": "aplicacion" },
            { "mDataProp": "num_ficha" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "direccion_entrega" },
            { "mDataProp": "estado_solicitud" },
            { "mDataProp": null, "bSortable": false, "mRender": function(o){
                var html = '';
                    html += '<a>Observación</a>';
                return html;
                } 
            },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-danger dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    html += '<li><a href="#"  onclick="cambioEstado(' + o.id_solicitud + ')">Despachar Solicitud</a></li>';
                    html += '<li><a href="#"  onclick="generarFormulario(' + o.id_solicitud + ')">Generar Formulario Control Tablet</a></li>';
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[0, "desc"]]
    }).on('mouseenter', 'tr', function (event) {
        var obs = null;
        if(data.length > 0){
             obs = data[$(this).closest("tr").prevAll("tr").length].observacion_solicitud;
        }
        
        
        $(this).qtip({
            overwrite: false,
            content: '<div style="font-size: 13px;">'+obs+'</div>',
            position: {
                my: 'right center',
                at: 'left center',
                target: $('td:eq(10)', this),
                viewport: $('#datatables')
            },
            show: {
                event: event.type,
                ready: true
            },
            hide: {
                fixed: true
            }
        }, event); // Note we passed the event as the second argument. Always do this when binding within an event handler!

    });

    //$('[data-toggle="tooltip"]').tooltip();
}

function cargaSolicitudHistorico() {
    /* funcion que hace la consulta de solicitudes de tablets en la bd*/
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosSolicitudesHistorico(result);
        },
        url: "Aplicaciones/getSolicitudesHistorico"
    });
}

function cargaDatosSolicitudesHistorico(data) {
    /* carga los datos traidos en la tabla de solicitudes de tablets */
    if ($.fn.dataTable.isDataTable('#tbl_Solicitud_historico')) {
        $('#tbl_Solicitud_historico').DataTable().destroy();
    }

    $('#tbl_Solicitud_historico').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "id_solicitud" },
            { "mDataProp": "nombre_solicitante" },
            { "mDataProp": "fecha_solicitud" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "modelo_tablet" },
            { "mDataProp": "aplicacion" },
            { "mDataProp": "num_ficha" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "direccion_entrega" },
            { "mDataProp": "estado_solicitud" },
            { "mDataProp": "observacion_solicitud" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-danger dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    html += '<li><a href="#"  onclick="generarFormulario(' + o.id_solicitud + ')">Generar Formulario Control Tablet</a></li>';
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[0, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}


function EstadosTablet(id_solicitud) {
    /* funcion que hace la consulta de los estados que puede tener de una solicitud de tablets en la bd*/
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_solicitud
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            //cargaCombo(result, 'estado');
        },
        url: "Aplicaciones/getEstados"
    });
}

function cargaCombo(midata, id) {
    /** funcion que carga el el select */
    $("#cbx_" + id).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
}

function cambioEstado(id_solicitud) {
    /* activa el modal para poder realizar los cambios de los estados en una solicitud */
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_solicitud: id_solicitud },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            $("#observacion").val('');
            $('#cambioSolicitudModal').modal();
            $('#ref_mod').text('Solicitud: ' + id_solicitud);
            $('#ref_mod_dat').text('Modelo: ' + result[0].modelo + ' Cantidad: ' + result[0].cantidad + ' De fecha: ' + result[0].fecha);
            $('#id_solicitud').val(id_solicitud); 
        },
        url: "Aplicaciones/getSolicitudByID"
    });
}

function cambiarEstado() {
    /* envia a la bd el cambio de estado con su observacion a una solicitud  */
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#frm_despacho_tablet").serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Eliminar', errorThrown, 'error');
        },
        success: function (result) {
            console.log(result);
            descontarStockTablet(result[0].solicitud_actualizada);
            aceptarCambio(result[0].cambio_estado, 4);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        $('#cambioSolicitudModal').modal('hide');
                        cargaSolicitud();
                    }
                });
        },
        url: "Aplicaciones/cambiarEstado"
    });
}

function descontarStockTablet(id_solicitud){
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_solicitud: id_solicitud },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
        },
        url: "Aplicaciones/descontarStockTablet"
    });
}

function generarFormulario(id_solicitud) {

    var url = "Aplicaciones/generarFormulario/" + id_solicitud;
    window.open(url, '_blank');
}

function aceptarCambio(id_cambio, tipo) {
    console.log(id_cambio);
    $.ajax({
        cache: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        data: {id_cambio_solicitud: id_cambio},
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Rectificación', jqXHR.responseText, 'error');
        },
        success: function (result) {
            if (contador == 0) {
                enviaCorreo(result, tipo);
                contador = 1;
            }
        },
        url: 'Aplicaciones/aceptarCambio'
    });
}

function enviaCorreo(data) {
    console.log(data);
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { datos: data },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
         //   history.back();
            // cargaTablaFicha(result);
        },
        url: "Aplicaciones/enviaEmail"
    });
}
