/**
 * Tablet
 *
 * Description...
 *
 * @version 0.0.1
 * Fecha creacion:	2018-03-09 [Jessica Roa] <jroa@edutecno.com>
 */
var contador = 0;
$(document).ready(function () {
    iniciopag();
});

function iniciopag() {
    cargaSolicitud();
    cargaInventario();
    cargaSolicitudHistorico();
    getGamas();

    $("#btn_ingreso").click(function () {
        $('#IngresoModal').modal();
        IngresoTablet();
        getCursosTablet();
        getEjecutivos();
    });

    $("#frm_ingreso_solicitud").submit(function () {
        AgregarSolicitud();
        event.preventDefault();
    });

    $("#frm_gama_tablet").submit(function () {
        AgregarGama();
        event.preventDefault();
    });

    $("#btn_editar_direccion").click(function () {
        editaDireccion();
    });
    $("#btn_anula_solicitud").click(function () {
        anulaSolicitud();
    });

    $('#datepicker_ingreso_tablet').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
    $('#datetimepickerTermino').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });

}

function cargaInventario() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosInventario(result);
        },
        url: "Solicitud_Tablet/getInventario"
    });
}

function cargaDatosInventario(data) {
    if ($.fn.dataTable.isDataTable('#tbl_Inventario_tablet')) {
        $('#tbl_Inventario_tablet').DataTable().destroy();
    }

    $('#tbl_Inventario_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "marca" },
            { "mDataProp": "modelo" },
            { "mDataProp": "descripcion" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "cantidad_solicitada" },
            { "mDataProp": "cantidad_necesaria" }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[5, "asc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function cargaSolicitud() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosSolicitudes(result);
        },
        url: "Control_Solicitudes/getSolicitudes"
    });
}

function cargaDatosSolicitudes(data) {
    if ($.fn.dataTable.isDataTable('#tbl_Solicitud_tablet')) {
        $('#tbl_Solicitud_tablet').DataTable().destroy();
    }

    $('#tbl_Solicitud_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "id_solicitud" },
            { "mDataProp": "nombre_solicitante" },
            { "mDataProp": "fecha_solicitud" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "modelo_tablet" },
            { "mDataProp": "aplicacion" },
            { "mDataProp": "num_ficha" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "direccion_entrega" },
            { "mDataProp": "estado_solicitud" },
            { "mDataProp": "observacion_solicitud" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-danger dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    html += '<li><a href="#"  onclick="aprobarSolicitud(' + o.id_solicitud + ')">Aprobar</a></li>';
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[0, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function getGamas() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaTablaGama(result);
        },
        url: "Control_Solicitudes/getGamas"
    });
}

function cargaTablaGama(data) {
    if ($.fn.dataTable.isDataTable('#tbl_gama_tablet')) {
        $('#tbl_gama_tablet').DataTable().destroy();
    }

    $('#tbl_gama_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "id_gama_tablet" },
            { "mDataProp": "nombre" },
            { "mDataProp": "descuento" },
            { "mDataProp": "fecha_creacion" },
            { "mDataProp": "txt_estado" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-danger dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    if(o.estado == 1){
                        html += '<li><a href="#" onclick="desactivarGama('+o.id_gama_tablet+')">Desactivar</a></li>';
                    }else{
                        html += '<li><a href="#" onclick="desactivarGama('+o.id_gama_tablet+')">Activar</a></li>';
                    }
                    
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[2, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function desactivarGama(id_gama_tablet){
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data:{id_gama_tablet: id_gama_tablet},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            getGamas();
        },
        url: "Control_Solicitudes/desactivarGama"
    });
}

function cargaSolicitudHistorico() {
    /* funcion que hace la consulta de solicitudes de tablets en la bd*/
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosSolicitudesHistorico(result);
        },
        url: "Control_Solicitudes/getSolicitudesHistorico"
    });
}

function cargaDatosSolicitudesHistorico(data) {
    /* carga los datos traidos en la tabla de solicitudes de tablets */
    if ($.fn.dataTable.isDataTable('#tbl_Solicitud_historico')) {
        $('#tbl_Solicitud_historico').DataTable().destroy();
    }

    $('#tbl_Solicitud_historico').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "id_solicitud" },
            { "mDataProp": "nombre_solicitante" },
            { "mDataProp": "fecha_solicitud" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "modelo_tablet" },
            { "mDataProp": "aplicacion" },
            { "mDataProp": "num_ficha" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "direccion_entrega" },
            { "mDataProp": "estado_solicitud" },
            { "mDataProp": "observacion_solicitud" }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[0, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function editarDireccion(id_solicitud, fecha, cantidad, modelo, direccion, estado) {
    $('#editardirModal').modal();
    $('#ref_mod').text('Solicitud: ' + id_solicitud);
    $('#ref_mod_dat').text('Modelo: ' + modelo + ' Cantidad: ' + cantidad + ' De fecha: ' + fecha);
    $('#id_solicitud').val(id_solicitud);
    $('#direccion_editar').val(direccion);
}

function cambioEstado(id_solicitud, fecha, cantidad, modelo, estado) {
    $('#anularModal').modal();
    $('#ref_mod_a').text(id_solicitud);
    $('#ref_mod_dat_a').text('Modelo: ' + modelo + ' Cantidad: ' + cantidad + ' De fecha: ' + fecha);
    $('#id_solicitud_a').val(id_solicitud);
}

function IngresoTablet() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, 'modelo');
        },
        url: "Solicitud_Tablet/getModelos"
    });
}

function getCursosTablet() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, 'aplicacion');
        },
        url: "Solicitud_Tablet/getCursos"
    });
}

function getEjecutivos() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, "ejecutivo");
        },
        url: "Solicitud_Tablet/getUsuarioTipoEcutivoComercial"
    });

}

function cargaCombo(midata, id) {
    $("#cbx_" + id).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
}

function AgregarSolicitud() {
    /*  
     
     */
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#frm_ingreso_solicitud").serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Ingreso', errorThrown, 'error');
        },
        success: function (result) {
            console.log(result[0].cambio_estado + ' 1');
            aceptarCambio(result[0].cambio_estado, 1);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        location.reload();
                    }
                });
        },
        url: "Solicitud_Tablet/AgregarSolicitud"
    });
}

function AgregarGama() {
    /*  
     
     */
    swal({
        title: "¿Está seguro?",
        text: "Se ingresará una nueva gama de tablet, la cual afectarán las comisiones de los ejecutivos comerciales.",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: true
    },
        function () {
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $("#frm_gama_tablet").serialize(),
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    // console.log(  textStatus + errorThrown);
                    alerta('Ingreso', errorThrown, 'error');
                },
                success: function (result) {
                    getGamas();
                    swal({
                        title: result[0].titulo,
                        text: result[0].mensaje,
                        type: result[0].tipo_alerta,
                        showCancelButton: false,
                        //confirmButtonClass: "btn-danger",
                        confirmButtonText: "Aceptar",
                        closeOnConfirm: true
                    },
                        function () {
                            //
                        });
                },
                url: "Control_Solicitudes/setGamas"
            });
        });
}

function editaDireccion() {
    /*  
     
     */
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_solicitud: $("#id_solicitud").val(),
            direccion_editar: $("#direccion_editar").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Eliminar', errorThrown, 'error');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            aceptarCambio(result[0].cambio_estado, 0);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        location.reload();
                    }
                });
        },
        url: "Solicitud_Tablet/EditarDireccion"
    });
}

function anulaSolicitud() {
    /*  
     
     */
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_solicitud: $("#id_solicitud_a").val(),
            observacion: $("#observacion_solicitud").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Eliminar', errorThrown, 'error');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            aceptarCambio(result[0].cambio_estado, 4);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        location.reload();
                    }
                });
        },
        url: "Solicitud_Tablet/anulaSolicitud"
    });
}

function aceptarCambio(id_cambio, tipo) {
    console.log(id_cambio + tipo);
    $.ajax({
        cache: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        data: { id_cambio_solicitud: id_cambio },
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Rectificación', jqXHR.responseText, 'error');
        },
        success: function (result) {
            if (contador == 0) {
                enviaCorreo(result, tipo);
                contador = 1;
            }
        },
        url: 'Solicitud_Tablet/aceptarCambio'
    });
}
function cargaInventario() {
    /* funcion que hace la consulta del inventario de las tablets en la bd*/
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosInventario(result);
        },
        url: "Aplicaciones/getInventario"
    });
}

function enviaCorreo(data, tip) {
    console.log(data);
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            datos: data,
            tipo: tip
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            //   history.back();
            // cargaTablaFicha(result);
        },
        url: "Solicitud_Tablet/enviaEmail"
    });
}

function aprobarSolicitud(id_solicitud) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_solicitud: id_solicitud
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Eliminar', errorThrown, 'error');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            aceptarCambio(result[0].cambio_estado, 2);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        location.reload();
                    }
                });
        },
        url: "Control_Solicitudes/aprobarSolicitud"
    });
}
