/**
 * Tablet
 *
 * Description...
 *
 * @version 0.0.1
 * Fecha creacion:	2018-03-09 [Jessica Roa] <jroa@edutecno.com>
 */
var contador = 0;
var appcount = 0;
var aplicaciones;
$(document).ready(function () {
    cargaInventario();
    cargaSolicitud();

    $("#btn_agregar_aplicacion").click(function () {
        var elemento = "";
        var opciones = $("#cbx_aplicacion").html();
        elemento += '<div class="form-group">';
        elemento += '<label class="col-sm-2 control-label">Aplicación:</label>';
        elemento += '<div class="col-md-6">';
        elemento += '<select id="cbx_aplicacion' + appcount + '" name="cbx_aplicacion' + appcount + '" class="select2_demo_3 form-control" required>';
        elemento += '<option></option>';
        elemento += '</select>';
        elemento += '</div>';
        elemento += '<div class="col-md-1">';
        elemento += '<button type="button" id="btn_quitar_aplicacion' + appcount + '" class="btn_quitar_aplicacion btn btn-default dim"><i style="color: red;" class="fa fa-minus"></i></button>';
        elemento += '</div>';
        elemento += '</div>';
        $(this).parent().parent().after(elemento);
        getCBX("cbx_aplicacion" + appcount, 'sp_curso_tablet_select_cbx');
        $(".btn_quitar_aplicacion").click(function () {
            $(this).parent().parent().remove();
        });


        appcount++;
    });
    $("input[name='motivo_solicitud']").click(function () {
        if ($(this).val() == "otro") {
            $(this).parent().append('<input type="text" name="texto_motivo" id="texto_motivo" required>');
        } else {
            $("#texto_motivo").remove();
        }
    });

    $("#btn_ingreso").click(function () {
        $("#frm_ingreso_solicitud")[0].reset();
        $('#IngresoModal').modal();
        IngresoTablet();
        getCBX("cbx_aplicacion", 'sp_curso_tablet_select_cbx');
        getEjecutivos();
    });

    $("#frm_ingreso_solicitud").submit(function () {
        AgregarSolicitud();
        event.preventDefault();
    });

    $("#btn_editar_direccion").click(function () {
        editaDireccion();
    });
    $("#btn_anula_solicitud").click(function () {
        anulaSolicitud();
    });

    $('#datepicker_ingreso_tablet').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
    $('#datetimepickerTermino').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
});

function quitarAplicacion() {
    console.log($(this).parent().parent());
}

function cargaInventario() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosInventario(result);
        },
        url: "Solicitud_Tablet/getInventario"
    });
}

function cargaDatosInventario(data) {
    if ($.fn.dataTable.isDataTable('#tbl_Inventario_tablet')) {
        $('#tbl_Inventario_tablet').DataTable().destroy();
    }

    $('#tbl_Inventario_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "marca" },
            { "mDataProp": "modelo" },
            { "mDataProp": "descripcion" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "cantidad_solicitada" },
            { "mDataProp": "cantidad_necesaria" }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[5, "asc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function cargaSolicitud() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaDatosSolicitudes(result);
        },
        url: "Solicitud_Tablet/getSolicitudes"
    });
}

function cargaDatosSolicitudes(data) {
    if ($.fn.dataTable.isDataTable('#tbl_Solicitud_tablet')) {
        $('#tbl_Solicitud_tablet').DataTable().destroy();
    }

    $('#tbl_Solicitud_tablet').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "id_solicitud" },
            { "mDataProp": "nombre_solicitante" },
            { "mDataProp": "fecha_solicitud" },
            { "mDataProp": "cantidad" },
            { "mDataProp": "modelo_tablet" },
            { "mDataProp": "aplicacion" },
            { "mDataProp": "num_ficha" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "direccion_entrega" },
            { "mDataProp": "estado_solicitud" },
            { "mDataProp": "observacion_solicitud" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var enabler = o.id_estado < 3 ? "" : "disabled";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-danger dropdown-toggle' " + enabler + ">Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";

                    if (o.id_estado > 1 && o.id_estado < 4) {
                        html += '<li><a href="#"  onclick="generarFormulario(' + o.id_solicitud + ')">Generar Formulario Control Tablet</a></li>';
                    }
                    if (o.id_estado < 3) {
                        html += '<li><a href="#"  onclick="editarDireccion(' + o.id_solicitud + ')">Editar Dirección</a></li>';
                        html += '<li><a href="#"  onclick="cambioEstado(' + o.id_solicitud + ')">Anular</a></li>';
                    }
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[0, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function generarFormulario(id_solicitud) {

    var url = "Aplicaciones/generarFormulario/" + id_solicitud;
    window.open(url, '_blank');
}

function editarDireccion(id_solicitud_tablet) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_solicitud_tablet: id_solicitud_tablet},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Ingreso', errorThrown, 'error');
        },
        success: function (result) {
            console.log(result);
            $('#editardirModal').modal();
            $('#ref_mod').text('Solicitud: ' + result[0].id_solicitud);
            $('#ref_mod_dat').text('Modelo: ' + result[0].modelo + ' Cantidad: ' + result[0].cantidad + ' De fecha: ' + result[0].fecha_solicitud);
            $('#id_solicitud').val(result[0].id_solicitud);
            $('#direccion_editar').val(result[0].direccion_entrega);
        },
        url: "Solicitud_Tablet/getSolicitud"
    });
}

function cambioEstado(id_solicitud) {
    $('#anularModal').modal();
    $('#ref_mod_a').text(id_solicitud);
    $('#id_solicitud_a').val(id_solicitud);
}

function IngresoTablet() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, 'modelo');
        },
        url: "Solicitud_Tablet/getModelos"
    });
}

function getCursosTablet() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, 'aplicacion');
            aplicaciones = result;
        },
        url: "Solicitud_Tablet/getCursos"
    });
}

function getEjecutivos() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, "ejecutivo");
        },
        url: "Solicitud_Tablet/getUsuarioTipoEcutivoComercial"
    });

}

function cargaCombo(midata, id) {
    $("#cbx_" + id).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
}

function AgregarSolicitud() {
    /*  
     */
    if ($("input[name='motivo_solicitud']:checked").val() == "otro") {
        $("input[name='motivo_solicitud']:checked").val($("#texto_motivo").val());
    }
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#frm_ingreso_solicitud").serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Ingreso', errorThrown, 'error');
        },
        success: function (result) {
            console.log(result[0].cambio_estado + ' 1');
            aceptarCambio(result[0].cambio_estado, 1);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        location.reload();
                    }
                });
        },
        url: "Solicitud_Tablet/AgregarSolicitud"
    });
}

function editaDireccion() {
    /*  
     
     */
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_solicitud: $("#id_solicitud").val(),
            direccion_editar: $("#direccion_editar").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Eliminar', errorThrown, 'error');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            if (result[0].id_estado == 2) {
                enviaCorreo(result, 0);
            }
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        location.reload();
                    }
                });
        },
        url: "Solicitud_Tablet/EditarDireccion"
    });
}

function anulaSolicitud() {
    /*  
     
     */
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_solicitud: $("#id_solicitud_a").val(),
            observacion: $("#observacion_solicitud").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Eliminar', errorThrown, 'error');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            aceptarCambio(result[0].cambio_estado, 4);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                //confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function () {
                    if (result[0].reset == 1) {
                        location.reload();
                    }
                });
        },
        url: "Solicitud_Tablet/anulaSolicitud"
    });
}

function aceptarCambio(id_cambio, tipo) {
    console.log(id_cambio + tipo);
    $.ajax({
        cache: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        data: { id_cambio_solicitud: id_cambio },
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Rectificación', jqXHR.responseText, 'error');
        },
        success: function (result) {
            console.log(result);
            if (contador == 0) {
                enviaCorreo(result, tipo);
                contador = 1;
            }
        },
        url: 'Solicitud_Tablet/aceptarCambio'
    });
}

function enviaCorreo(data, tip) {
    console.log(data);
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            datos: data,
            tipo: tip
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            //   history.back();
            // cargaTablaFicha(result);
        },
        url: "Solicitud_Tablet/enviaEmail"
    });
}
