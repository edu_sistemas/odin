$( document ).ready(function() {
    //listarEncuestas();

    $('#btn_nueva_encuesta').click(function(){
        $("#tabla_encuestas").animate({
            height: "toggle"
        }, 200, function () {
            $(this).stop(true, true);
            $("#frm_encuesta_satisfaccion").animate({
                height: "toggle"
            }, 200, function () {
                $(this).stop(true, true);
            });
            cargaCBXRelator();
            getEncuesta();
            $('#btn_nueva_encuesta').hide();
        });
    });

    $('#btn_descartar_encuesta').click(function(){
        $("#frm_encuesta_satisfaccion").animate({
            height: "toggle"
        }, 200, function () {
            $(this).stop(true, true);
            $("#tabla_encuestas").animate({
                height: "toggle"
            }, 200, function () {
                $(this).stop(true, true);
            });
            $('#btn_nueva_encuesta').show();
        });
    });

    $('#frm_encuesta_satisfaccion').submit(function(){
        guardarEncuesta();
        event.preventDefault();
    });

});

function listarEncuestas(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: $('#id_ficha').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('#num_ficha_hidden').val(result[0].num_ficha);
            $('#main_title').append(result[0].num_ficha);
            cargaTablaFicha(result);
        },
        url: "../listarEncuestas"
    });
}

function cargaTablaFicha(data) {
    
        if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
            $('#tbl_ficha').DataTable().destroy();
        }
    
        $('#tbl_ficha').DataTable({
            "aaData": data,
            "aoColumns": [
                {"mDataProp": "id_encuesta"},
                {"mDataProp": "fecha_ingreso"},
                {"mDataProp": "relator"},
                {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                        var html = "";
                        html = '<button class="btn btn-primary" onclick="verEncuesta('+o.id_encuesta+')">Ver</button>';
                        return html;
                    }
                }    
            ],
            pageLength: 25,
            responsive: false,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {
                    extend: 'copy'
                }, {
                    extend: 'csv'
                }, {
                    extend: 'excel',
                    title: 'Edutecno'
                }, {
                    extend: 'pdf',
                    title: 'Edutecno'
                }, {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ],
            order: [[1, "asc"]]
        });
    
        $('[data-toggle="tooltip"]').tooltip();
    }

    function verEncuesta(id_encuesta){
        $(".modal-title").text('Respuesta de la Encuesta ID: '+id_encuesta);
        $(".modal-title2").text('Ficha: '+$('#num_ficha_hidden').val());
        $('#modal_encuesta').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#modal_encuesta').modal('show');

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_encuesta: id_encuesta},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                setRespuestas(result);
            },
            url: "../getEncuestaById"
        });
    }

    function setRespuestas(data){
        $('#tbl_respuestas :radio').attr('disabled', false);
        $.each( data, function( i, item ) {
            if(item.id_pregunta == 20){
                $('#respuesta'+item.id_pregunta).text(item.respuesta);
            }else{
                $('#tbl_respuestas input[name=respuesta'+item.id_pregunta+'][value="'+item.respuesta+'"]').prop('checked', true);
            }
        });
        $('#tbl_respuestas :radio:not(:checked)').attr('disabled', true);
        $('#tbl_respuestas textarea').attr('disabled', true);
        //$('').attr('disabled', true);
    }

    function getEncuesta(){
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                showEncuesta(result);
            },
            url: "../getEncuestaSatisfaccion"
        });
    }

function showEncuesta(data){
    var categorias = [];
    var preguntas = [];
    var html = "";
    $('#frm_encuesta_satisfaccion table').html('');

    //AQUI SE METEN LAS CATEGORIAS EN UN ARRAY, PARA PODER CONSTRUIR LA TABLA POSTERIORMENTE
    for (var index = 0; index < (data[0].id_categorias.split("&")).length; index++) {
        var categoria= {};
        categoria.id_categoria = data[0].id_categorias.split("&")[index];
        categoria.nombre_categoria = data[0].categorias.split("&")[index];
        categorias.push(categoria);
    }

    //LO MISMO SE HACE CON LAS PREGUNTAS
    $.each( data, function( i, item ) {
        var pregunta ={};
        pregunta.id_pregunta = item.id_pregunta;
        pregunta.num_pregunta = item.num_pregunta;
        pregunta.id_categoria_encuesta = item.id_categoria_encuesta;
        pregunta.pregunta = item.pregunta;
        preguntas.push(pregunta);
    });

    //AQUI PRIMERO SE INSERTAN LAS CATEGORIAS EN LA TABLA CON SU RESPECTIVO ID Y CSS
    $.each( categorias, function( i, item ) {
        if(item.id_categoria == 6){
            html += '<tr id="categoria'+item.id_categoria+'" style="color:#fff;background-color:#2e74b4;"><td style="width:72%; text-align:left">';
            html += item.nombre_categoria;
            html += '</td><td align="center" style="width: 4%;"></td><td align="center" style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center" style="width: 4%;"></td></tr>';
        }else{
            html += '<tr id="categoria'+item.id_categoria+'" style="color:#fff;background-color:#2e74b4;"><td style="width:72%; text-align:left">';
            html += item.nombre_categoria;
            html += '</td><td align="center" style="width: 4%;">1</td><td align="center" style="width: 4%;">2</td><td align="center"  style="width: 4%;">3</td><td align="center"  style="width: 4%;">4</td><td align="center"  style="width: 4%;">5</td><td align="center"  style="width: 4%;">6</td><td align="center" style="width: 4%;">7</td></tr>';
        }
    });
    $('#frm_encuesta_satisfaccion table').html(html);
        
    // Y AQUÍ SE INSERTA CADA PREGUNTA EN SU RESPECTIVA CATEGORIA, PREGUNTANDO POR EL ID DE CATEGORIA QUE INSERTAMOS EN EL $.each ANTERIOR
    $.each( preguntas.reverse(), function( i, item ) {
        var innerhtml = "";
        if(item.id_categoria_encuesta == 6){
            innerhtml += '<tr><td><br></td><tr>';
            innerhtml += '<tr id="pregunta'+item.id_pregunta+'"><td>';
            innerhtml += '<textarea id="respuesta'+item.id_pregunta+'" name="respuesta'+item.id_pregunta+'" rows="4" cols="50"></textarea>';
            innerhtml += '</td></tr>';
            $("tr#categoria"+item.id_categoria_encuesta).after(innerhtml);
        }else{
            innerhtml += '<tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta'+item.id_pregunta+'"><td style="width:72%;color:#000;" align="left">';
            if(item.num_pregunta == 0){
                innerhtml += item.pregunta;
            }else{
                innerhtml += item.num_pregunta +'.\t'+ item.pregunta;
            }
            innerhtml += '</td>';
            for (var index = 0; index < 7; index++) {
                innerhtml += '<td style="width: 4%;text-align:center;" ><input type="radio" name="respuesta'+item.id_pregunta+'" value="'+(index+1)+'"></td>';
            }
            innerhtml += '</tr>';
            $("tr#categoria"+item.id_categoria_encuesta).after(innerhtml);
        }
    });
}

function guardarEncuesta(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_encuesta_satisfaccion').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            location.reload();
        },
        url: "../setEncuestaSatisfaccion"
    });
}

function cargaCBXRelator(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data:{id_ficha: $('#id_ficha').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX('id_relator', result);
        },
        url: "../getRelatoresCBX"
    });
}