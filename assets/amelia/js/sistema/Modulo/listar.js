$(document).ready(function () {

    llamaDatosModulo();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

});


function llamaDatosModulo() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaModulo(result);
        },
        url: "Listar/modulo"
    });
}

function cargaTablaModulo(data) {

    if ($.fn.dataTable.isDataTable('#tbl_modulo')) {
        $('#tbl_modulo').DataTable().destroy();
    }

    $('#tbl_modulo').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id"},
            {"mDataProp": "nombre"},
            {"mDataProp": "titulo"},
            {"mDataProp": "descripcion"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {

                    if (o.estado == 1) {
                        return ' <td><div class="infont col-md-3 col-sm-4"><a><i style="color:green" class="fa fa-check"></i></a></div>Activo</td>';
                    } else {
                        return '<td><div class="infont col-md-3 col-sm-4"><a><i style="color:red" class="fa fa-times"></i></a></div>Inactivo</td>';
                    }
                }},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {


                    var html = "";

                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu'>";
                    var x = "Editar/index/" + o.id;
                    html += "<li><a href='" + x + "'>Modificar</a></li>";
                    if (o.estado == 1) {
                        html += "<li><a    onclick='EditarModuloEstado(" + o.id + ",0);'>Desactivar</a></li>";
                    } else {
                        html += "<li><a    onclick='EditarModuloEstado(" + o.id + ",1);'>Activar</a></li> ";
                    }

                    html += "</ul>";
                    html += "</div>";


                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });

}


function EditarModuloEstado(id, estado) {
    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_modulo: id, estado_modulo: estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(  textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result);
        },
        url: "Listar/EditarEstado"
    });
    llamaDatosModulo();
}
