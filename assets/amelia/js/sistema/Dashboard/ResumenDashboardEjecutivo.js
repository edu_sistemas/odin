var array_venta_anio_actual = [];
var array_venta_anio_anterior = [];
$(document).ready(function () {
    //MetasGlobales(0);
    iniciopag();
    $("#btn-fecha-sdashboard").click(function () {
        var fecha = $("#fecha_sdashboard").val();
        SelectGruposEjecutivosFiltrados(fecha);
        SelectMejoresEjecutivos(fecha);
    });
});

function iniciopag() {
    var fechaini = $("#fecha_sdashboard").val();
    SelectGruposEjecutivosFiltrados(fechaini);
    SelectMejoresEjecutivos(fechaini);
}

function fecha() {

    $('#data1 .input-group.date').datepicker({
        minViewMode: 1,
        keyboardNavigation: false,
        forceParse: false,
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        format: "yyyy-mm"
    });
}

function iniciarcoloresindicadores(result) {
    var icon = '';
    switch (result) {
        case '1':
            icon = 'fa fa-circle text-info fa-x4 pull-right';
            break;
        case '2':
            icon = 'fa fa-circle text-warning fa-x4 pull-right';
            break;
        case '3':
            icon = 'fa fa-circle text-danger fa-x4 pull-right';
            break;
        case '0':
            icon = 'fa fa-circle text-primary fa-x4 pull-right';
            break;
    }
    return icon;
}

function SelectGruposEjecutivosFiltrados(fecha) {
    $("#tblSelectGruposEjecutivos tbody tr").each(function (index) {
        $(this).remove();
    });

    $('#tblSelectGruposEjecutivos tbody').append('<tr id="trwaitSelectGruposEjecutivos"><td colspan="5" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>');
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { fecha: fecha },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            $('#trwaitSelectGruposEjecutivos').remove();
            iconmes = '';
            iconacum = '';
            $(result).each(function (index) {
                iconmes = iniciarcoloresindicadores(result[index].iconmes);
                iconacum = iniciarcoloresindicadores(result[index].iconacum);
                $('#tblSelectGruposEjecutivos tbody').append('<tr id="tr_' + result[index].id_usuario + '" ><td style="text-align:right;">' + result[index].ejecutivo + '</td><td><i class="' + iconmes + '"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">' + result[index].porcenmes + '%</p></td><td><i class="' + iconacum + '"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">' + result[index].porcenacum + '%</p></td></tr>');
            });
        },
        url: "ResumenDashboardEjecutivo/getDatosSelectGruposEjecutivos"
    });
}

function SelectMejoresEjecutivos(fecha) {
    $("#tblSelectMejoresEjecutivos tbody tr").each(function (index) {
        $(this).remove();
    });

    $('#tblSelectMejoresEjecutivos tbody').append('<tr id="trwaitSelectMejoresEjecutivos"><td colspan="5" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>');
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { fecha: fecha },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            $('#trwaitSelectMejoresEjecutivos').remove();
            iconmes = '';
            iconacum = '';
            $(result).each(function (index) {
                iconmes = iniciarcoloresindicadores(result[index].iconmes);
                iconacum = iniciarcoloresindicadores(result[index].iconacum);
                $('#tblSelectMejoresEjecutivos tbody').append('<tr id="tr_' + result[index].id_usuario + '" ><td style="text-align:right;">' + (index + 1) + '</td><td style="text-align:right;">' + result[index].ejecutivo + '</td><td><i class="' + iconmes + '"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">' + result[index].porcenmes + '%</p></td><td><i class="' + iconacum + '"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">' + result[index].porcenacum + '%</p></td></tr>');
            });
        },
        url: "ResumenDashboardEjecutivo/getDatosSelectMejoresEjecutivos"
    });
}
