var ejecutivos = [];
var ventas_ejecutivo = [];
var periodo_anterior = "";
var periodo_actual = "";

var array_venta_mes_anio_anterior = [];
var array_venta_mes_anio_actual = [];


var array_venta_anio_anterior = [];
var array_venta_anio_actual = [];

$(document).ready(function () {
    recargaLaPagina();
    InitHome();

    $("#btn").click(function () {

        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            // data: {id_holding: id_holding},
            error: function (jqXHR, textStatus, errorThrown) {
            },
            success: function (result) {
                console.log(result);
            },
            url: "Director/senMailtest"
        });

        location.href = "../Alumno/Listar";
    });

});


function  InitHome() {

    ventaMensual();

    ventaAcomulada();
}


function ventaMensual() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            for (var i = 0; i < result.length; i++) {
                if (result[i].anio == 2016) {
                    periodo_anterior = result[i].periodo;
                    array_venta_mes_anio_anterior.push(result[i].total_suma_venta);
                } else {
                    array_venta_mes_anio_actual.push(result[i].total_suma_venta);
                    periodo_actual = result[i].periodo;
                    ejecutivos.push(result[i].nombre_ejecutivo);
                }
            }
            graficoVentasEjecutivo();

        },
        url: "Director/getVentaMensual"
    });

}

function graficoVentasEjecutivo() {
    /*Grafico Ejecutivo*/


    var lineData_ejecutivo = {
        labels: ejecutivos,
        datasets: [
            {
                label: periodo_actual,
                backgroundColor: 'rgba(26,179,148,0.5)',
                borderColor: "rgba(26,179,148,0.7)",
                pointBackgroundColor: "rgba(26,179,148,1)",
                pointBorderColor: "#fff",
                data: array_venta_mes_anio_actual
            }, {
                label: periodo_anterior,
                backgroundColor: 'rgba(220, 220, 220, 0.5)',
                pointBorderColor: "#fff",
                data: array_venta_mes_anio_anterior
            }

        ]
    };

    var lineOptions_ejecutivo = {
        scales: {
            yAxes: [{
                    barThickness: 80,
                    stacked: true,

                    ticks: {
                        beginAtZero: true
                    }
                }],
            xAxes: [{

                    ticks: {
                        callback: function (value) {
                            return "$ " + parseInt(value).toLocaleString();
                        }
                    }
                }]},
        tooltips: {
            mode: 'label',
            callbacks: {
                label: function (tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex].label + ": " + "$ " + parseInt(tooltipItem.xLabel).toLocaleString(); //Barra
                }
            }
        }

    };
    var ctx = document.getElementById("lineChart_ejecutivo").getContext("2d");
    new Chart(ctx, {type: 'horizontalBar', data: lineData_ejecutivo, options: lineOptions_ejecutivo});


    /*End  Grafico Ejecutivo*/

}



function ventaAcomulada() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            for (var i = 0; i < result.length; i++) {
                if (result[i].anio == 2016) {
                    anio2 = 2016;
                    array_venta_anio_anterior.push(result[i].total_suma_venta);
                } else {
                    array_venta_anio_actual.push(result[i].total_suma_venta);
                    anio1 = 2017;
                }

            }

            graficoComparativaVentas();

        },
        url: "Director/getVentaAcumulada"
    });

}

function graficoComparativaVentas() {


    /*Grafico Ventas*/

    var lineData_venta = {
        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        datasets: [
            {
                label: anio1,
                backgroundColor: 'rgba(26,179,148,0.5)',
                borderColor: "rgba(26,179,148,0.7)",
                pointBackgroundColor: "rgba(26,179,148,1)",
                pointBorderColor: "#fff",
                data: array_venta_anio_actual
            }, {
                label: anio2,
                backgroundColor: 'rgba(220, 220, 220, 0.5)',
                pointBorderColor: "black",
                data: array_venta_anio_anterior
            }
        ]
    };

    var lineOptions_venta = {
        responsive: true,
        scales: {
            yAxes: [{
                    ticks: {
                        callback: function (value) {
                            return "$ " + parseInt(value).toLocaleString();
                        }
                    }
                }]},
        tooltips: {
            mode: 'label',
            callbacks: {
                label: function (tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex].label + ": " + "$ " + parseInt(tooltipItem.yLabel).toLocaleString();
                }
            }
        }
    };



    var ctx = document.getElementById("lineChart_ventas").getContext("2d");
    new Chart(ctx, {type: 'line', data: lineData_venta, options: lineOptions_venta});

    /*End  Grafico Ventas*/

}
