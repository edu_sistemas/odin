var array_venta_anio_actual = [];
var array_venta_anio_anterior = [];
$(document).ready(function () {
    //MetasGlobales(0);
    ventaAcomulada();
    iniciopag();

    $("#btn-fecha-sdashboard").click(function () {

        var fecha = $("#fecha_sdashboard").val();

        ListVentasEmpresas(fecha);
        ListRubros(fecha);
        ListLinNegocio(fecha);
        SelectEjecutivosFiltrados(fecha);
    });

    const min = document.querySelector('#min');
    const max = document.querySelector('#max');
    const filtro = $('input[name=filtro]:checked').val();
/*    min.addEventListener('keyup', (e) => {
        const element = e.target;
        const value = element.value;
        element.value = formatNumber(value);
    });
    max.addEventListener('keyup', (e) => {
        const element = e.target;
        const value = element.value;
        element.value = formatNumber(value);
    });*/
    $('input[name=filtro]').click(function () {
        $('#min, #max').val('');
        $('#tblListVentasEmpresas').DataTable().draw();
    });
    $('#min, #max').keyup(function (event) {
        const filtro = $('input[name=filtro]:checked').val();
        console.log(filtro + 'filtro');
        if (filtro == 4 || filtro == 5) {
            // skip for arrow keys
            if (event.which >= 37 && event.which <= 40) {
                event.preventDefault();
            }
            $(this).val(function (index, value) {
                return value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
            });
        } else {
            // skip for arrow keys
            if (event.which >= 37 && event.which <= 40) {
                event.preventDefault();
            }
            
            $(this).val(function (index, value) {
                return value
                .replace(/\D/g, "")
                .replace(/([0-9])([0-9]{2})$/, '$1,$2')
                .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ".");
            });
        }
        $('#tblListVentasEmpresas').DataTable().draw();
    });

    $("#btn_generar_excel_ventas_empresas").click(function () {
        var fecha = $("#fecha_sdashboard").val();
        if (fecha == "") {
            fecha = "0";
        }
        var url_final = "";    
        url_final = "GeneralDashboard/generarExcelVentasEmpresas/" + fecha;
        
        window.open(url_final, '_blank');
    });

    $("#btn_generar_excel_ranking_ejecutivos").click(function () {
        var fecha = $("#fecha_sdashboard").val();
        if (fecha == "") {
            fecha = "0";
        }
        var url_final = "";
        url_final = "GeneralDashboard/generarExcelRankingEjecutivos/" + fecha;

        window.open(url_final, '_blank');
    });


});



$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
        if ([$('input[name=filtro]:checked').val()] == 4 || [$('input[name=filtro]:checked').val()] == 5){
            var min = parseFloat(replaceAll($('#min').val(), '.', ""), 10);
            var max = parseFloat(replaceAll($('#max').val(), '.', ""), 10);
            var age = parseFloat(replaceAll(data[$('input[name=filtro]:checked').val()],'.', "")) || 0;
        } else {
            var min = parseFloat(replaceAll(replaceAll($('#min').val(), '.', ""), ',', "."), 10);
            var max = parseFloat(replaceAll(replaceAll($('#max').val(), '.', ""), ',', "."), 10);
            var age = parseFloat(data[$('input[name=filtro]:checked').val()]) || 0;
        }
       // console.log(min + 'max' + max + 'age' + age + 'input' + $('input[name=filtro]:checked').val() );

        if ((isNaN(min) && isNaN(max)) || (isNaN(min) && age <= max) || (min <= age && isNaN(max)) || (min <= age && age <= max)) {
            return true;
        }
        return false;
    }
);

function replaceAll(text, busca, reemplaza) {
    while (text.toString().indexOf(busca) != -1)
        text = text.toString().replace(busca, reemplaza);
    return text;
}

function formatNumber(n) {
    n = String(n).replace(/\D/g, "");
    return n === '' ? n : Number(n).toLocaleString('de-DE');
}

function iniciopag() {
    var fechaini = $("#fecha_sdashboard").val();
    
    ListVentasEmpresas(fechaini);
    ListRubros(fechaini);
    ListLinNegocio(fechaini);
    SelectEjecutivosFiltrados(fechaini);
    
    
}

function fecha() {

    $('#data1 .input-group.date').datepicker({
        minViewMode: 1,
        keyboardNavigation: false,
        forceParse: false,
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        format: "yyyy-mm"
    });
}

function iniciarcoloresindicadores(result) {
    var icon = '';
    switch (result) {
        case '1':
            icon = 'fa fa-circle text-info fa-x4 pull-right';
            break;
        case '2':
            icon = 'fa fa-circle text-warning fa-x4 pull-right';
            break;
        case '3':
            icon = 'fa fa-circle text-danger fa-x4 pull-right';
            break;
        case '0':
            icon = 'fa fa-circle text-primary fa-x4 pull-right';
            break;
    }
    return icon;
}

function SelectEjecutivosFiltrados(fecha) {
    $("#tblSelectEjecutivos tbody tr").each(function (index) {
        $(this).remove();
    });

    $('#tblSelectEjecutivos tbody').append('<tr id="trwaitSelectEjecutivos"><td colspan="5" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>');
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { fecha: fecha },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            $('#trwaitSelectEjecutivos').remove();
            iconmes = '';
            iconacum = '';
            $(result).each(function (index) {
                iconmes = iniciarcoloresindicadores(result[index].iconmes);
                iconacum = iniciarcoloresindicadores(result[index].iconacum);
                $('#tblSelectEjecutivos tbody').append('<tr id="tr_' + result[index].id_usuario + '" ><td style="text-align:right;">' + result[index].ejecutivo + '</td><td><i class="' + iconmes + '"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">' + result[index].porcenmes + '%</p></td><td><i class="' + iconacum + '"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">' + result[index].porcenacum + '%</p></td><td style="text-align:right;">$' + result[index].venta_mes + '</td><td style="text-align:right;">$' + result[index].meta_mes + '</td><td style="text-align:right;">$' + result[index].venta_acum + '</td><td style="text-align:right;">$' + result[index].meta_acum + '</td><td style="text-align:right;">$' + result[index].meta_anual + '</td></tr>');
            });
        },
        url: "GeneralDashboard/getDatosSelectEjecutivos"
    });
}


function ListVentasEmpresas(fecha) {


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { fecha: fecha },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);
            $('#trwaitEmpresas').remove();
            iconmes = '';
            iconacum = '';
            res = [];
            $(result).each(function (index) {
                iconmes = iniciarcoloresindicadores(result[index].iconmes);
                iconacum = iniciarcoloresindicadores(result[index].iconacum);
                result[index].iconmes = iconmes;
                result[index].iconacum = iconacum;
            });
            cargaDatosEmpresa(result);

        },
        url: "GeneralDashboard/getEmpresas"
    });
 
}

function cargaDatosEmpresa(data) {
    if ($.fn.dataTable.isDataTable('#tblListVentasEmpresas')) {
        $('#tblListVentasEmpresas').DataTable().destroy();
    }

    $('#tblListVentasEmpresas').DataTable({
        "language": {
            "decimal": ",",
            "thousands": "."
        },
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "razon_social" },
            { "mDataProp": "ejecutivo" },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<i class="' + o.iconmes + '"></i>&nbsp;&nbsp;&nbsp; <p class="pull-right">' + o.porcenmes + '%</p>';
                    return html;
                } 
            },
            { "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<i class="' + o.iconacum + '"></i>&nbsp;&nbsp;&nbsp; <p class="pull-right">' + o.porcenacum + '%</p>';
                    return html;
                } 
            },
            { "mDataProp": "venta_mes" },
            { "mDataProp": "venta_acum" },
            { "mDataProp": "rubro" }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [[4, "desc"], [2, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}
/*
function funchrefEjecutivo(id_user) {
    //alert('edashboard/index/'+id_user+'');
    window.location.href = '../Crm/edashboard/index/' + id_user + '';
}
*/

function ListRubros(fecha) {
    //var ejecutivo=$("#ejecutivos").val();

    $("#tblRubro tbody tr").each(function (index) {
        $(this).remove();

    });

    $('#tblRubro tbody').append('<tr id="trwaitRubro"><td colspan="3" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>');

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#form_fecha_sdashboard').serialize(),
        data: { fecha: fecha },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            console.log(result);

            $('#trwaitRubro').remove();

            $(result).each(function (index) {

                $('#tblRubro tbody').append('<tr><td style="width: 15%">' + result[index].rubro + '</td><td style="text-align:right;">$' + result[index].venta_mes + '</td><td style="text-align:right;">$' + result[index].venta_acum + '</td>' + '<td style="text-align:right;">$' + result[index].rubro + '</td></tr>');
            });
        },
        url: "GeneralDashboard/getRubros"
    });
}


function ListLinNegocio(fecha) {
    $("#tbllinNegocio tbody tr").each(function (index) {
        $(this).remove();

    });

    $('#tbllinNegocio tbody').append('<tr id="trwaitLinNeg"><td colspan="4" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>');

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#form_fecha_sdashboard').serialize(),
        data: { fecha: fecha },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            $('#trwaitLinNeg').remove();

            $(result).each(function (index) {

                $('#tbllinNegocio tbody').append('<tr><td style="width: 15%">' + result[index].linea + '</td><td>' + result[index].sub_linea + '</td><td>$' + result[index].venta_mes + '</td><td>$' + result[index].venta_acum + '</td></tr>');
            });
        },
        url: "GeneralDashboard/getLineaNegocio"
    });
}


function ventaAcomulada() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            for (var i = 0; i < result.length; i++) {
                if (result[i].anio == (new Date().getFullYear()- 1)) {
                    anio2 = new Date().getFullYear() - 1;
                    array_venta_anio_anterior.push(result[i].total_suma_venta);
                } else {
                    array_venta_anio_actual.push(result[i].total_suma_venta);
                    anio1 = new Date().getFullYear();

                  //  var anyo = new Date().getFullYear();
                }

            }

            graficoComparativaVentas();

        },
        url: "GeneralDashboard/getVentaAcumulada"
    });

}

function graficoComparativaVentas() {


    /*Grafico Ventas*/

    var lineData_venta = {
        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        datasets: [
            {
                label: anio1,
                backgroundColor: 'rgba(26,179,148,0.5)',
                borderColor: "rgba(26,179,148,0.7)",
                pointBackgroundColor: "rgba(26,179,148,1)",
                pointBorderColor: "#fff",
                data: array_venta_anio_actual
            }, {
                label: anio2,
                backgroundColor: 'rgba(220, 220, 220, 0.5)',
                pointBorderColor: "black",
                data: array_venta_anio_anterior
            }
        ]
    };

    var lineOptions_venta = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    callback: function (value) {
                        return "$ " + parseInt(value).toLocaleString();
                    }
                }
            }]
        },
        tooltips: {
            mode: 'label',
            callbacks: {
                label: function (tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex].label + ": " + "$ " + parseInt(tooltipItem.yLabel).toLocaleString();
                }
            }
        }
    };
    var ctx = document.getElementById("lineChart_ventas").getContext("2d");
    new Chart(ctx, { type: 'line', data: lineData_venta, options: lineOptions_venta });

    /*End  Grafico Ventas*/

}