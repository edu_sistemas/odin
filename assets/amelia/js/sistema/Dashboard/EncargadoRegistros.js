
$(document).ready(function () {
    cargaDatos();
});
function cargaDatos(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            console.log(result);
            $("#cantidad_gestiones").html(result[0].total_gestiones);
            $("#fichas_pendientes").html(result[0].fichas_pendientes);
            cargaTablaFichasDachboard(result);
        },
        url: "EncargadoRegistros/Listar"
    });
}

function cargaTablaFichasDachboard(datos){
    var contenido = "";
    $("#tabla_pendientes tbody").html('');
    $.each(datos, function (i, item) {
        contenido += '<tr><td>';
        contenido += item.dias_restantes;
        contenido += '</td><td>';
        contenido += item.num_ficha;
        contenido += '</td><td>';
        contenido += item.empresa;
        contenido += '</td><td>';
        contenido += item.ejecutivo;
        contenido += '</td><td>';
        contenido += item.estado_facturacion;
        contenido += '</td><td>';
        contenido += item.gestiones_realizadas;
        contenido += '</td><tr>';
    });
    $("#tabla_pendientes tbody").html(contenido);
}