var idalm = 0;
var idast = 0;
var idast2 = 0;

$(document).ready(function () {
    $('#reset_superior').hide();
    $('#fichas').hide();
    $('#listaAlm').hide();
    $("#confirmarR").click(function () {

        if (validarRequeridoForm($("#frmLista"))) {
            registrarAsistencia();
        }
    });
    llamaDatosControles();
    $("#id_ficha_v").select2({
        placeholder: "Todos",
        allowClear: true
    });

    $('#reset').click(function () {
        location.reload();
    });

    $('#reset_superior').click(function () {
        location.reload();
        $("#id_ficha_v").prop("disabled", false);
    });

    $("#id_ficha_v").change(function () {
        llamaDatosFichas($("#id_ficha_v").val());
        $('#largo').val('');
        var html3 = "";
        html3 += '<div class="alert alert-info" role="alert"> ';
        html3 += '<strong> Para consultar otra ficha, presione el botón Volver.</strong>';
        html3 += '<p></p>';
        html3 += '</div>';
        $('#mensaje2').html(html3);
        $('#mensaje2').show();
        $("#id_ficha_v").prop("disabled", true);
        $('#reset_superior').show();
    });

});

function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, 'id_ficha_v');
        },
        url: "Listar_Sence/getFichas"
    });
}

function cargaCombo(midata) {
    $('#id_ficha_v').select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}

function llamaDatosFichas(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {
            mostrartablaFichas(result);

        },
        url: "Listar_Sence/fichasListar"
    });
}

function llamaDatosAlumnos(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // mostrartablaAlumnos(result);
            //console.log(result.length)
            console.log(result);
            $('#numRows').val(result.length);
            if (result[0].asistencia != null) {
                $('#largo').val(result[0].asistencia.split(',').length);
            } else {

                $('#largo').val('0');
            }
            mostrartablaAlumnos(result);

        },
        url: "Listar_Sence/alumnosListar"
    });
}

function mostrartablaFichas(result) {
    //  llamaDatosCapsula(id); 
    $('#fichas').show();
    if ($.fn.dataTable.isDataTable('#fichas')) {
        $('#fichas').DataTable().destroy();
    }
    var cnt = 0;
    var cnt2 = 0;
    var ta = $('#fichas').DataTable({
        data: result,
        "ordering": false,
        "aoColumns": [{
                "mDataProp": "num_ficha"
            },
            {
                "mDataProp": "razon_social_empresa"
            },
            {
                "mDataProp": "codigo_sence"
            },
            {
                "mDataProp": "nombre_curso"
            },
            {
                "mDataProp": "fecha_inicio"
            },
            {
                "mDataProp": "fecha_fin"
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    var x = o.id_ficha;
                    html += "<li><a onclick='modal(" + x + ")'>Ingresar Asistencia</a></li>";
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
        "order": [
            [1, 'asc']
        ],
        pageLength: 20000,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        "bLengthChange": false,
        buttons: [{
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Capsulas'
            }, {
                extend: 'pdf',
                title: 'Capsulas'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }]
    });

}

function modal(id) {
    $("#asistencia").modal();
    llamaDatosAlumnos(id);
}

function mostrartablaAlumnos(result) {

    var html = "";
    $('#listaAlm').show();
    $('#listaAlm tbody').html(html);
    casistrue = $('#largo').val();
    casisfalse = 1;

    $.each(result, function (i, item) {
        html += '<tr id="">';
        html += '<td>' + item.rut + '</td>';
        html += '<td>' + item.nombre + '<input type="hidden" id="dtl' + idalm + '" name="dtl' + idalm + '" value="' + item.id_detalle_alumno + '"/></td>';

        if (casistrue > 0) {
            for (h = 0; h < casistrue; h++) {
                html += '<td><input class="number" requerido="true" mensaje=" Ingrese porcentaje de asistencia" value="' + item.asistencia.split(',')[h] + '"type="text" id="por' + idast + '" name="por' + idast + '" maxlength="4" size="4" onkeypress="return solonumerom(event), unpunto(event)" > %';
                html += '<input class="number" value="' + item.asistencia_id.split(',')[h] + '" type="hidden" size="2" id="id_asis' + idast2 + '" name="id_asis' + idast2 + '"/>';
                idast++;
                idast2++;
            }

        } else {

            for (h = 0; h < casisfalse; h++) {
                html += '<td><input value="" requerido="true" mensaje=" Ingrese porcentaje de asistencia" type="text" class="number" id="por' + idast + '" name="por' + idast + '" maxlength="4" size="4" onkeypress="return solonumerom(event), unpunto(event)"> %';

                idast++;
                idast2++;
            }
        }
        idalm++;
    });

    $('#listaAlm tbody').html(html);
}

function solonumerom(event) {

    if (event.keyCode < 48 || event.keyCode > 57) { //valida que no pueda apretar letras

        if (event.keyCode != 46) { // valida que pueda apretar el púnto
            event.returnValue = false;
        }
    }
}

function unpunto(event) {
    $('.number').keypress(function (event) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
}

function registrarAsistencia() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frmLista').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // alerta('Asistencia', errorThrown, 'Debe seleccionar un relator !');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            if (result[0].reset == 1) {
                $('#reset_superior').trigger("click");
            }
        },
        url: "Listar_Sence/alumnosRegistrarLista/"
    });
}