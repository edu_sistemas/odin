$(document).ready(function () {
	recargaLaPagina();
	llamaDatosFicha();

	$("#btn_buscar_ficha").click(function () {
		llamaDatosFicha();
	});

	$("#btn_guardar_edutecno").click(function () {
		guardarIDEdutecno();
	});


});

function llamaDatosFicha() {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#frm').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTablaFicha(result);
		},
		url: "Listar/buscarFicha"
	});
}

function cargaTablaFicha(data) {

	if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
		$('#tbl_ficha').DataTable().destroy();
	}
	$('#tbl_ficha').DataTable({
		"aaData": data,
		"aoColumns": [
			{"mDataProp": "num_ficha"},
			{"mDataProp": "ejecutivo"},
			{"mDataProp": "categoria"},
			{"mDataProp": "modalidad"},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {
					var html = "";
					html += o.fecha_inicio + '-' + o.fecha_fin;
					return html;
				}
			},
			{"mDataProp": "descripcion_producto"},
			{"mDataProp": "otic"},
			{"mDataProp": "empresa"},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {
					var html = "";
					html += '  <p data-toggle="tooltip" data-placement="top" title data-original-title="Sence: ' + o.valor_sence + ' • Empresa: ' + o.valor_empresa + '" >' + o.valor_total + '</p>';
					return html;
				}
			},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {
					var html = " Colo car POP UP";
					html = "<a href='#' title='Ver Alumnos' onclick='AlumnosFicha(" + o.id_ficha + ");'>" + o.cant_alumno + "</a>";
					html = o.cant_alumno;
					return html;
				}
			},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {

					var html = "";
					html += '  <div class="btn-group">';

					var a = "Resumen/index/" + o.id_ficha;
					var b = "ResumenArriendo/index/" + o.id_ficha;
					var AnularA = "ResumenAnulacionFC/index/" + o.id_ficha;

					if (o.id_estado == 64) {
						var con = o.num_ficha;
						$('tr td:contains(' + con + ')').each(function () {
							$(this).addClass('alert alert-danger');
						});
					}
					if (o.id_rectificacion == '') {

						if (o.id_categoria == 1) {
							if (o.id_estado == 64) {
								html += " <a href='" + AnularA + "'> <button   class='btn btn-danger dropdown-toggle'>Anular </button> </a> ";
							} else {
								html += " <a href='" + a + "'> <button   class='btn btn-primary dropdown-toggle'>VER </button> </a> ";
							}
						}
						if (o.id_categoria == 2) {
							if (o.id_estado == 64) {
								html += " <a href='" + AnularA + "'> <button   class='btn btn-danger dropdown-toggle'>Anular </button> </a> ";
							} else {
								html += " <a href='" + b + "'> <button   class='btn btn-primary dropdown-toggle'>VER </button> </a> ";
							}
						}
					} else {
						var resumen = "";
						if (o.categoria == "Extensión de Plazo") {
							resumen = "ResumenRectificacionExtension/index/" + o.id_rectificacion;

						} else {
							resumen = "ResumenRectificacion/index/" + o.id_rectificacion;
						}

						html += " <a href='" + resumen + "'> <button   class='btn btn-primary dropdown-toggle'>VER </button> </a> ";
					}

					html += ' </div>';
					return html;

				}
			}
		],
		pageLength: 10,
		responsive: false,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [
			{
				extend: 'copy'
			}, {
				extend: 'csv'
			}, {
				extend: 'excel',
				title: 'Edutecno'
			}, {
				extend: 'pdf',
				title: 'Edutecno'
			}, {
				extend: 'print',
				customize: function (win) {
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					$(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
				}
			}
		]
	});

	$('[data-toggle="tooltip"]').tooltip();

	/*if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
	 $('#tbl_ficha').DataTable().destroy();
	 }
	 
	 $('#tbl_ficha').DataTable({
	 "aaData": data,
	 "aoColumns": [
	 {"mDataProp": "id_ficha"},
	 {"mDataProp": "categoria"},
	 {"mDataProp": "curso"},
	 {"mDataProp": "fecha_inicio"},
	 {"mDataProp": "fecha_fin"},
	 {"mDataProp": "fecha_cierre"},
	 {"mDataProp": "empresa"},
	 //{"mDataProp": "nombre_corto_otic"},
	 {"mDataProp": null, "bSortable": false, "mRender": function (o) {
	 var html = "";
	 html += "<p title=" + o.nombre_largo_otic + ">" + o.nombre_corto_otic + "</p>";
	 return html;
	 }
	 },
	 {"mDataProp": "horas_curso"},
	 //{"mDataProp": "ordenes_de_compra"},
	 {"mDataProp": null, "bSortable": false, "mRender": function (o) {
	 var html = "";
	 html += "<a onclick='getOrdenesDeCompra(" + o.id_ficha + ");'>" + o.ordenes_de_compra + "</a>";
	 return html;
	 }
	 },
	 {"mDataProp": "cantidad_alumnos"},
	 {"mDataProp": null, "bSortable": false, "mRender": function (o) {
	 var html = "";
	 html += '  <div class="btn-group">';
	 html += '  <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle ">Accion<span class="caret"></span></button>';
	 html += '  <ul class="dropdown-menu pull-right">';
	 
	 var a = "Resumen/index/" + o.id_ficha;
	 html += "<li><a href='" + a + "'>Resumen</a></li>";
	 html += "<li><a    onclick='aprobarFicha(" + o.id_ficha + ");'>Aprobar</a></li>";
	 
	 html += "<li><a    onclick='abrirRechazo(" + o.id_ficha + ");'>Rechazar</a></li> ";
	 
	 html += ' </ul>';
	 html += ' </div>';
	 return html;
	 }
	 }
	 ],
	 pageLength: 10,
	 responsive: true,
	 dom: '<"html5buttons"B>lTfgitp',
	 buttons: [
	 {
	 extend: 'copy'
	 }, {
	 extend: 'csv'
	 }, {
	 extend: 'excel',
	 title: 'Edutecno'
	 }, {
	 extend: 'pdf',
	 title: 'Edutecno'
	 }, {
	 extend: 'print',
	 customize: function (win) {
	 $(win.document.body).addClass('white-bg');
	 $(win.document.body).css('font-size', '10px');
	 $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
	 }
	 }
	 ]
	 });*/
}

function selectIdEdutecno(id_ficha, id_edutecno) {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {id_ficha: id_ficha},
		error: function (jqXHR, textStatus, errorThrown) {

			alerta("Ficha", errorThrown, "error");
		},
		success: function (result) {
			$("#modal_edutecno").modal("show");
			$("#modal_id_ficha2").html(id_ficha);
			$("#id_ficha_edutecno").val(id_ficha);
			cargaCBXSelected('edutecno_cbx', result, id_edutecno);


		},
		url: "Listar/getidEdutecnoCBX"
	});
}
function abrirRechazo(id) {
	$("#modal_id_ficha_rechazo").text(id);

	$("#id_ficha").val(id);
	$("#modal_rechazo").modal("show");
}

function guardarIDEdutecno() {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#frm_edutecno').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//console.log(textStatus + errorThrown);
			alerta('Ficha', errorThrown, 'error');
		},
		success: function (result) {

			swal({
				title: result[0].titulo,
				text: result[0].mensaje,
				type: result[0].tipo_alerta
			},
					function (isConfirm) {
						if (isConfirm) {
							llamaDatosFicha();
						}
					});

		},
		url: "Listar/setIDEdutecno"
	});
}

function rechazarFicha() {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#frm_rechazo').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//console.log(textStatus + errorThrown);
			alerta('Ficha', errorThrown, 'error');
		},
		success: function (result) {

			swal({
				title: result[0].titulo,
				text: result[0].mensaje,
				type: result[0].tipo_alerta
			},
					function (isConfirm) {
						if (isConfirm) {
							llamaDatosFicha();
						}
					});


		},
		url: "Listar/rechazarFicha"
	});


}

function aprobarFicha(id) {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {id_ficha: id},
		error: function (jqXHR, textStatus, errorThrown) {

			alerta("Ficha", errorThrown, "error");
		},
		success: function (result) {
			swal({
				title: result[0].titulo,
				text: result[0].mensaje,
				type: result[0].tipo_alerta
			},
					function (isConfirm) {
						if (isConfirm) {
							llamaDatosFicha();
						}
					});
		},
		url: "Listar/aprobarFicha"
	});
}

function DocumentosFicha(id, id_estado) {
	$("#modal_id_ficha").text(id);

	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {id_ficha: id},
		error: function (jqXHR, textStatus, errorThrown) {

			alerta("Ficha", errorThrown, "error");
		},
		success: function (result) {
			cargaDatosDocumentoFicha(result);
			$("#modal_documentos_ficha").modal("show");
			$("#div_frm_add").css("display", "none");
			$("#div_tbl_contacto_empresa").css("display", "block");
			$("#btn_nuevo_contacto").css("display", "block");
		},
		url: "Listar/verDocumentosFicha"
	});
}

function cargaDatosDocumentoFicha(data) {


	var html = "";
	$('#tbl_documentos_ficha tbody').html(html);
	$.each(data, function (i, item) {
		html += '        <tr>';
		if (item.nombre_documento % 2 == 0) {
			html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a></td> ';
		} else {
			html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a></td> ';
		}

		html += '   <td>' + item.fecha_registro + '</td> ';

		html += '</tr>';
	});
	$('#tbl_documentos_ficha tbody').html(html);
}
// MODAL 2

function getOrdenesDeCompra(id) {
	mostrarOC(id);
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {id_ficha: id},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTablaOC(result);
		},
		url: "Listar/buscarOrdenes"
	});
}

function mostrarOC(id) {
	$("#modal_id_ficha2").text(id);
	$("#txt_id_ficha2").val(id);
	$("#id_ficha2").val(id);
	$("#modal_documentos_ficha2").modal("show");
}

function cargaTablaOCxxx(data) {

	if ($.fn.dataTable.isDataTable('#tbl_oc')) {
		$('#tbl_oc').DataTable().destroy();
	}

	$('#tbl_oc').DataTable({
		"aaData": data,
		"aoColumns": [
			{"mDataProp": "num_orden_compra"},
			{"mDataProp": "n_rectificacion"},
			{"mDataProp": "fecha_registro"},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {
					var html = "";
					html += ' <p>---</p>';
					return html;
				}
			}
		],
		pageLength: 10,
		responsive: true,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [
			// {
			//     extend: 'copy'
			// }, {
			//     extend: 'csv'
			// }, {
			//     extend: 'excel',
			//     title: 'Edutecno'
			// }, {
			//     extend: 'pdf',
			//     title: 'Edutecno'
			// }, {
			//     extend: 'print',
			//     customize: function (win) {
			//         $(win.document.body).addClass('white-bg');
			//         $(win.document.body).css('font-size', '10px');
			//         $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
			//     }
			// }
		]
	});
}

function cargaTablaOC(data) {
	var html = "";
	$('#tbl_oc tbody').html(html);
	$.each(data, function (i, item) {
		html += '<tr>';
		html += '<td>' + item.num_orden_compra + '</td>';
		if (item.n_rectificacion == null) {
			html += '<td></td>';
		} else {
			html += '<td>' + item.n_rectificacion + '</td>';
		}
		html += '<td>' + item.fecha_registro + '</td>';
		html += '<td></td>';
		html += '</tr>';
	});
	$('#tbl_oc tbody').html(html);
}