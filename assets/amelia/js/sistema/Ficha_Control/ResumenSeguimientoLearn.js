$(document).ready(function () {
	recargaLaPagina();
	carga_rectificaciones();
	$("#btn_back").click(function () {
		// retrocede una pagina
		limpiarFormulario();
		// history.back();
	});
	//$('#mensaje_text_area').hide();
	$('#aprobar_extension').click(function () {

		if (!$('#comentario_extension').val() == '' || !$('#comentario_extension').val() == null) {
			$('#mensaje_text_area').text('');
			confirmaRectificacionExtensioLOG();
		} else {
			//$('#mensaje_text_area').show();
			$('#mensaje_text_area').text($('#comentario_extension').attr('mensaje'));
		}
	});

	$('#aprobar_alumnos').click(function () {

		if (!$('#comentario_alumno').val() == '' || !$('#comentario_alumno').val() == null) {
			$('#mensaje_text_area_alumno').text('');
			confirmaRectificacionAlumnosLOG();
		} else {
			$('#mensaje_text_area_alumno').text($('#comentario_alumno').attr('mensaje'));
		}
	});



	$("#btn_descarte").click(function () {
		// retrocede una pagina

		if (validarRequeridoForm($("#frm_descarte"))) {
			descartar();
		}

	});

	$("#btn_envio_correo").click(function () {
		// retrocede una pagina

		enviar();

	});

	contarCaracteres("comentario", "text-out", 200);
	$('#datepicker').datepicker({
		keyboardNavigation: false,
		forceParse: false,
		autoclose: true
	});
	inicializaControles();

	$("#cbx_orden_compra").change(function () {
		var id_ficha = "";
		var id_oc = "";
		id_ficha = $("#id_ficha").val();
		id_oc = $("#cbx_orden_compra").val();

		$.ajax({
			async: true,
			cache: false,
			dataType: "json",
			type: 'POST',
			data: {
				ficha: id_ficha,
				oc: id_oc
			},
			error: function (jqXHR, textStatus, errorThrown) {
				// code en caso de error de la ejecucion del ajax
				//  console.log(textStatus + errorThrown);
			},
			success: function (result) {
				cargaTablaDatosAlumnos(result);
			},
			url: "../cargaDatosOC"
		});

	});


});

/**
 * Comment
 */
function carga_rectificaciones() {

	var id_ficha = "";
	var id_oc = "";
	id_ficha = $("#id_ficha").val();
	id_oc = $("#cbx_orden_compra").val();


	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {
			ficha: id_ficha,
			oc: id_oc
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTablaRectficacionesAlumnos(result);
		},
		url: "../getRectificacionesAlumnosbyFicha"
	});


	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {
			ficha: id_ficha,
			oc: id_oc
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTablaDatoDocumentosRectificacione(result);
		},
		url: "../getRectificacionesAlumnosDocbyFicha"
	});

	//Extensiones

	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {
			ficha: id_ficha,
			oc: id_oc
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTablaRectficacionesExtensiones(result);
		},
		url: "../getRectificacionesExtensionbyFicha"
	});


	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {
			ficha: id_ficha,
			oc: id_oc
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTablaDatoDocumentosExtensiones(result);
		},
		url: "../getRectificacionesExtensionDocbyFicha"
	});

}

function inicializaControles() {
	var id_ficha = "";
	var id_oc = "";
	id_ficha = $("#id_ficha").val();
	id_oc = $("#cbx_orden_compra").val();

	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {
			ficha: id_ficha,
			oc: id_oc
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTablaDatosAlumnos(result);
			$('#estado_edu').val(result[0].id_estado_edutecno);
			$('#cantidad').val(result.length);
			$('#id_fichaff').val(result[0].id_ficha);

			if (!result[0].comentario_log == '') {
				$('#aprobar_extension').removeClass('btn-danger');
				$('#aprobar_extension').addClass('btn-primary');
				$('#comentario_extension').val(result[0].comentario_log);
			}

			if (!result[0].comentario_log_alumno == '') {
				$('#aprobar_alumnos').removeClass('btn-danger');
				$('#aprobar_alumnos').addClass('btn-primary');
				$('#comentario_alumno').val(result[0].comentario_log_alumno);
			}


		},
		url: "../cargaDatosOC"
	});
}

function cargaTablaDatosAlumnos(data) {
	$("#total_alumnos").html(data[0].total_alumnos);
	$("#valor_final").html(data[0].valor_final);
	$("#total_sence").html(data[0].total_sence);
	$("#total_empresa").html(data[0].total_empresa);
	$("#total_sence_empresa").html(data[0].total_sence_empresa);
	$("#total_agregado").html(data[0].total_agregado);
	$("#total_becados").html(data[0].total_becados);
	$("#total_oc").html(data[0].total_oc);


	if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
		$('#tbl_orden_compra_carga_alumno').DataTable().destroy();
	}

	var ctr = 0;

	$('#tbl_orden_compra_carga_alumno').DataTable({
		"aaData": data,
		"aoColumns": [{
				"mDataProp": "rut"
			},
			{
				"mDataProp": "nombre"
			},
			{
				"mDataProp": "estado"
			},
			//{"mDataProp": "eliminado"},
			{
				"mDataProp": null,
				"bSortable": false,
				"mRender": function (o) {
					var html = "";

					if (o.id_estado === '4') {
						html += "<div class='btn-'>";
						html += "<label>" + o.causa_eliminacion + "</label>";
						html += "</div>";
					} else {
						html += "<div class='btn-'>";
						html += "<label>" + o.eliminado + "</label>";
						html += "</div>";
					}
					return html;
				}
			},
			{
				"mDataProp": "centro_costo"
			},
			{
				"mDataProp": "fq"
			},
			{
				"mDataProp": "valor_otic"
			},
			{
				"mDataProp": "valor_empresa"
			},
			{
				"mDataProp": "valor_total"
			},
			{
				"mDataProp": null,
				"bSortable": false,
				"mRender": function (o) {
					var html = "";
					if (o.id_estado === '5') {

						html += "<input id='chkAst" + o.rut + "' type='checkbox' checked onclick='prueba(`" + o.rut + "`);' value='5' name='chkAst" + ctr + "'><p id='mensaje" + o.rut + "'>Descartado</p>";

					} else {

						html += "<input id='chkAst" + o.rut + "' type='checkbox' onclick='prueba(`" + o.rut + "`);' value='1' name='chkAst" + ctr + "'><p id='mensaje" + o.rut + "'></p>";

					}

					html += "<div class='btn-group'>";
					html += "<label>";
					html += "<input id='id_alm" + o.rut + "' type='hidden' value='" + o.id_detalle_alumno + "' name='a" + ctr + "'>";
					html += "<i></i>";
					html += "  </label>";
					html += "</div>";

					return html;
				}
			},
			{
				"mDataProp": null,
				"bSortable": false,
				"mRender": function (o) {

					if (o.id_estado === '5') {
						var html = "";
						html += "<div>";
						html += "<textarea  requerido='true' mensaje ='Debe Ingresar el motivo'  id='area" + o.rut + "' name='area" + ctr + "'>" + o.eliminado + "</textarea>";

					} else {

						var html = "";
						html += "<div>";
						html += "<textarea readonly id='area" + o.rut + "' name='area" + ctr + "'></textarea>";

					}
					ctr++;
					return html;
				}
			}
		],
		pageLength: 100,
		responsive: false,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [{
				extend: 'copy'
			}, {
				extend: 'csv'
			}, {
				extend: 'excel',
				title: 'Edutecno'
			}, {
				extend: 'pdf',
				title: 'Edutecno'
			}, {
				extend: 'print',
				customize: function (win) {
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					$(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
				}
			}]
	});

}

function cargaTablaRectficacionesAlumnos(data) {

	if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno_rectificacion')) {
		$('#tbl_orden_compra_carga_alumno_rectificacion').DataTable().destroy();
	}

	$('#tbl_orden_compra_carga_alumno_rectificacion').DataTable({
		"aaData": data,
		"aoColumns": [{
				"mDataProp": "tipo_rectificacion"
			},
			{
				"mDataProp": "rut"
			},
			{
				"mDataProp": "nombre_alumno"
			},
			{
				"mDataProp": "apellido_paterno"
			},
			{
				"mDataProp": "centro_costo"
			},
			{
				"mDataProp": "porcentaje_franquicia"
			},
			//            {"mDataProp": "costo_otic"},
			//            {"mDataProp": "costo_empresa"},
			//            {"mDataProp": "valor_agregado"},
			{
				"mDataProp": "valor_total"
			},
			{
				"mDataProp": "causa"
			},
			{
				"mDataProp": "fecha_rectificacion"
			},
			{
				"mDataProp": "estado"
			}
		],
		pageLength: 10,
		responsive: true,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [{
				extend: 'copy'
			}, {
				extend: 'csv'
			}, {
				extend: 'excel',
				title: 'Edutecno'
			}, {
				extend: 'pdf',
				title: 'Edutecno'
			}, {
				extend: 'print',
				customize: function (win) {
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					$(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
				}
			}]
	});

	$('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentosRectificacione(data) {

	if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documentos_rectificaciones')) {
		$('#tbl_orden_compra_documentos_rectificaciones').DataTable().destroy();
	}

	$('#tbl_orden_compra_documentos_rectificaciones').DataTable({
		"aaData": data,
		"aoColumns": [{
				"mDataProp": "tipo_documento"
			},
			{
				"mDataProp": null,
				"bSortable": false,
				"mRender": function (o) {
					var html = "";

					html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

					return html;
				}
			},
			{
				"mDataProp": "fecha_registro"
			}
		],
		pageLength: 10,
		responsive: true,
		dom: '<"clear">',
		order: [2, "desc"]
	});

	$('[data-toggle="tooltip"]').tooltip();

}

function prueba(id) {

	if ($('#chkAst' + id).is(":checked")) {
		$('#area' + id).prop('readonly', false);
		$('#chkAst' + id).val('5');
		$('#mensaje' + id).text('');
		$('#mensaje' + id).html('Descartado');

	} else {
		$('#area' + id).prop('readonly', true);
		$('#chkAst' + id).val('1');
		$('#mensaje' + id).text('');
		$('#area' + id).val('Standby');
	}
}

function descartar() {

	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#frm_descarte').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
			inicializaControles();
		},
		url: "../DescartarAlms"
	});
}

function enviar() {

	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#frm_descarte').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
			$('#comentarios_email').val('');
			//			inicializaControles();
		},
		url: "../enviaEmail"
	});
}

// extensiones
function cargaTablaRectficacionesExtensiones(data) {

	if ($.fn.dataTable.isDataTable('#tbl_rectificacion_extension_plazo')) {
		$('#tbl_rectificacion_extension_plazo').DataTable().destroy();
	}

	$('#tbl_rectificacion_extension_plazo').DataTable({
		"aaData": data,
		"aoColumns": [{
				"mDataProp": "tipo_rectificacion"
			},
			{
				"mDataProp": "fecha_inicio"
			},
			{
				"mDataProp": "fecha_cierre"
			},
			{
				"mDataProp": "fecha_extension"
			},
			{
				"mDataProp": "num_orden_compra"
			},
			{
				"mDataProp": "fecha_rectificacion"
			},
			{
				"mDataProp": "estado"
			}

		],
		pageLength: 10,
		responsive: false,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [{
				extend: 'copy'
			}, {
				extend: 'csv'
			}, {
				extend: 'excel',
				title: 'Edutecno'
			}, {
				extend: 'pdf',
				title: 'Edutecno'
			}, {
				extend: 'print',
				customize: function (win) {
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					$(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
				}
			}],
		order: [5, "desc"]
	});

	$('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentosExtensiones(data) {

	if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documentos_extensiones')) {
		$('#tbl_orden_compra_documentos_extensiones').DataTable().destroy();
	}

	$('#tbl_orden_compra_documentos_extensiones').DataTable({
		"aaData": data,
		"aoColumns": [{
				"mDataProp": "tipo_documento"
			},
			{
				"mDataProp": null,
				"bSortable": false,
				"mRender": function (o) {
					var html = "";

					html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

					return html;
				}
			},
			{
				"mDataProp": "fecha_registro"
			}
		],
		pageLength: 10,
		responsive: true,
		dom: '<"clear">',
		order: [2, "desc"]

	});

	$('[data-toggle="tooltip"]').tooltip();

}

/**
 * Comment
 */
function confirmaRectificacionExtensioLOG() {

	var id_ficha = $('#id_ficha').val();
	var hito_c = $('#txt_aprobar_extension').val();
	var est = $('#estado_edu').val();
	var com = $('#comentario_extension').val();



	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {
			ficha: id_ficha,
			hito: hito_c,
			id_estado: est,
			comentario: com

		},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			location.reload();
		},
		url: "../ConfirmaExtension"
	});

}

function confirmaRectificacionAlumnosLOG() {

	var id_ficha = $('#id_ficha').val();
	var hito_a = $('#txt_aprobar_alumnos').val();
	var esta = $('#estado_edu').val();
	var come = $('#comentario_alumno').val();



	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {
			ficha: id_ficha,
			hitoa: hito_a,
			id_estadoa: esta,
			comentarioa: come

		},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			location.reload();
		},
		url: "../ConfirmaAlumnos"
	});

}

// saludos