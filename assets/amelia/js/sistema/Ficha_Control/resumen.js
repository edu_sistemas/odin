$(document).ready(function () {
    recargaLaPagina();
    cargaCbxAll();
    $("#btn_back").click(function () {
        // retrocede una pagina
        limpiarFormulario();
        // history.back();
    });


    $("#btn_aceptar").click(function () {
        if (validarRequeridoForm($("#frm_rechazo"))) {
            rechazarFicha();
            $('#modal_rechazo').modal('hide');
            history.back();
        } else {
            event.stopPropagation();
        }
    });

    $("#btn_aceptar_anulacion").click(function () {
        if (validarRequeridoForm($("#frm_anulacion"))) {
            anularFicha();
            $('#modal_anulacion').modal('hide');
            history.back();
        } else {
            event.stopPropagation();
        }
    });

    $("#btn_anular").click(function () {
        swal({
            title: 'ATENCIÓN',
            text: "Haga clic en continuar sólo si está seguro(a) que quiere anular la ficha!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: 'red',
            cancelButtonColor: 'gray',
            confirmButtonText: 'Continuar'
        }).then(function () {
            abrirAnulacion($("#id_ficha").val());
        });

    });


    $("#btn_rechazar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        abrirRechazo(ficha);
    });

    $("#btn_aprobar_ficha").click(function () {
        $("#btn_aprobar_ficha").attr('disabled', 'disabled');
        $("#btn_rechazar_ficha").attr('disabled', 'disabled');
        var ficha = $("#id_ficha").val();
        aprobarFicha(ficha);


    });


    contarCaracteres("comentario", "text-out", 1000);

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    inicializaControles();

    $("#btn_enviar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        validaPreEnvioFicha(ficha);
    });

    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });

    selectIdEdutecno();
    

    $("#edutecno_cbx").change(function () {

        if ($("#edutecno_cbx").val() == "0") {
            location.reload();
        } else {
            if ($("#edutecno_cbx").val() != "") {
                guardarIDEdutecno();
            }
        }

    });

    guardarIDEdutecno();

    $("#cbx_orden_compra").change(function () {

        var id_ficha = "";
        var id_oc = "";
        id_ficha = $("#id_ficha").val();
        id_oc = $("#cbx_orden_compra").val();

        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {ficha: id_ficha, oc: id_oc},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                cargaTablaDatosAlumnos(result);
            },
            url: "../cargaDatosOC"
        });
    });

    cargaAdicionales();

});

function selectIdEdutecno() {

    var id_ficha = $('#id_ficha').val();
    var id_edutecno = $('#id_edutecno').val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
//        	$("#modal_edutecno").modal("show");
//        	$("#modal_id_ficha2").html(id_ficha);
//        	$("#id_ficha_edutecno").val(id_ficha);
            cargaCBXSelected('edutecno_cbx', result, $("#id_edutecno_original").val());


        },
        url: "../getidEdutecnoCBX"
    });
}

function guardarIDEdutecno() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha_edutecno: $('#id_ficha').val(), edutecno_cbx: $('#edutecno_cbx').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            /*  swal({
             title: result[0].titulo,
             text: result[0].mensaje,
             type: result[0].tipo_alerta
             },
             function (isConfirm) {
             if (isConfirm) {
             //llamaDatosFicha();
             }
             });*/

        },
        url: "../setIDEdutecno"
    });
}

function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() {

    var id_ficha = "";
    var id_oc = "";
    id_ficha = $("#id_ficha").val();
    id_oc = $("#cbx_orden_compra").val();

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosAlumnos(result);
        },
        url: "../cargaDatosOC"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../cargaDatosOCDocumentoResumen"
    });
    selectIdEdutecno();

}

function cargaTablaDatosAlumnos(data) {
    console.log(data);

    if (data.length > 0) {
        $("#total_alumnos").html(data[0].total_alumnos);
        $("#valor_final").html(data[0].valor_final);
        $("#total_sence").html(data[0].total_sence);
        $("#total_empresa").html(data[0].total_empresa);
        $("#total_sence_empresa").html(data[0].total_sence_empresa);

        $("#total_adicional").html(data[0].total_adicional);

        $("#total_becados").html(data[0].total_becados);
        $("#total_cobrado").html(data[0].total_cobrado);
        $("#total_oc").html(data[0].total_oc);
    }

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }


    $('#tbl_orden_compra_carga_alumno').DataTable({
        initComplete: function () {
            this.api().columns([7]).every(function () {
                var column = this;
                var select = $('<select><option value="">Filtrar Por OC</option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.cells('', column[0]).render('display').sort().unique().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        },
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut"},
            {"mDataProp": "nombre"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "fq"},
            { "mDataProp": "valor_otic"},
           {"mDataProp": "valor_adicional"},
            { "mDataProp": "valor_total" },
            {"mDataProp": "num_orden_compra"}
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
    $('[data-toggle="tooltip"]').tooltip();
}

function cargaTablaDatoDocumentos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';
                    return html;
                }
            }
        ],
        responsive: false,
        dom: '<"clear">'
    });
    $('[data-toggle="tooltip"]').tooltip();
}


function abrirRechazo(id) {
    $("#modal_id_ficha_rechazo").text($("#num_ficha").val());
    $("#id_ficha_rechazo").val(id);
    $("#modal_rechazo").modal("show");
}

function rechazarFicha() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_rechazo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {
            enviaCorreoRechazo(result);

            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            }, function (isConfirm) {
                if (isConfirm) {
                    //  history.back();
                }
            });


        },
        url: "../rechazarFicha"
    });

}

function enviaCorreoRechazo(data) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {datos: data},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "../enviaEmailCorreccion"
    });
}

function aprobarFicha(id) {


    var final = false;

    if ($("#edutecno_cbx").val() == "0") {

        $("#btn_aprobar_ficha").attr('disabled', false);
        $("#btn_rechazar_ficha").attr('disabled', false);

        swal({
            title: "Error",
            text: "Debe seleccionar que empresa Edutecno va a facturar la ficha",
            type: "error"
        },
                function (isConfirm) {
                    if (isConfirm) {
                        //history.back();
                    }
                });
    } else {


        swal({
            title: "Ficha",
            text: "Estas Seguro que deseas aprobar la venta?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Si, Estoy Seguro!',
            cancelButtonText: "Cancelar!",
            showLoaderOnConfirm: true,
            preConfirm: function (email) {
                $("#btn_aprobar_ficha").attr('disabled', true);
                $("#btn_rechazar_ficha").attr('disabled', true);
                return new Promise(function (resolve, reject) {

                    $.ajax({
                        async: true,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {
                            id_ficha: id
                        },
                        error: function (jqXHR, textStatus, errorThrown) {

                            alerta("Ficha", errorThrown, "error");
                            final = false;
                        },
                        success: function (result) {

                            // alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

                            final = true;


                        },
                        url: "../aprobarFicha"
                    });


                    setTimeout(function () {

                        resolve();

                    }, 4000);
                });
            },
            allowOutsideClick: false
        }).then(function (result) {
            //  resetForm();
            // history.back();
            //   location.href = "../../Listar";
//            alert(final);


            final = true;
//            alert(final);
            if (final) {


                swal({
                    title: "Ficha",
                    text: "Ha aprobado la Ficha",
                    type: "success",
                    showCancelButton: false,
                    //  confirmButtonColor: "#DD6B55",
                    confirmButtonText: "OK",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: true,
                    closeOnCancel: false,
                    timer: 15000
                }).then(function () {
                    if (final) {
                        location.href = "../../Listar";
                    }
                }, function (final) {
                    if (final) {
                        //     alert(" if (function) {" + final);
                        //  history.back();
                        location.href = "../../Listar";
                    }
                }
                );
            } else {
            }
        }).catch(swal.noop);

        $("#btn_aprobar_ficha").attr('disabled', false);
        $("#btn_rechazar_ficha").attr('disabled', false);

    }

}

function abrirAnulacion(id) {
    $("#modal_id_ficha_anulacion").text($("#num_ficha").val());
    $("#id_ficha_anulacion").val(id);
    $("#modal_anulacion").modal("show");
}

function anularFicha() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_anulacion').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            //  history.back();
                        }
                    });


        },
        url: "../anularFicha"
    });

}

function cargaAdicionales(){
    var id_ficha = $('#id_ficha').val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            var contenido = '';
            $('#tbl_adicionales tbody').html('');
            $.each(result , function (i, item){
                if(item.nombre == null){
                    contenido += '';
                }else{
                    contenido += '<tr><td>';
                    contenido += item.nombre;
                    contenido += '</td><td>';
                    contenido += item.valor_formateado;
                    contenido += '</td></tr>';
                }
              });
              $('#tbl_adicionales tbody').html(contenido);
              contenido = "";
              $('#total_adicionales').text(result[0].valor_total);
        },
        url: "../getAdicionalesFicha"
    });
}

 