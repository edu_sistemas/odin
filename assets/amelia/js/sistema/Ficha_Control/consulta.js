$(document).ready(function() {
    recargaLaPagina();
    cargaTodosDatosExtras();
    $('[data-toggle="tooltip"]').tooltip();

    $("#btn_buscar_ficha").click(function() {
        llamaDatosFicha();
    });

    $("#btn_limpiar_form").click(function() {
        location.reload();
    });

    $("#holding").change(function() {
        var id_holding = $('#holding').val();
        getEmpresaByHolding(id_holding);
    });

    $('#data_5 .input-daterange').datepicker({
        format: "dd-mm-yyyy",
        language: 'es',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    llamaDatosFicha();
});


function cargaTodosDatosExtras() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function(result) {

            cargaComboGenerico(result, 'ejecutivo');
        },
        url: "Consulta/getEjecutivo"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function(result) {

            cargaComboGenerico(result, 'periodo');
        },
        url: "Consulta/getPeriodo"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaComboGenerico(result, 'estado');


        },
        url: "Consulta/getEstadoCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaComboGenerico(result, 'empresa');

        },
        url: "Consulta/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaComboGenerico(result, 'holding');

        },
        url: "Consulta/getHoldingCBX"
    });

}

function cargaComboGenerico(midata, id) {

    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

    $("#" + id + " option[value='0']").remove();

}

function llamaDatosFicha() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $("#loader").html("");
        },
        beforeSend: function() {
            $("#loader").html("<h2>Cargando...</h2>");
        },
        success: function(result) {
            $("#loader").html("");
            cargaTablaFicha(result);
        },
        url: "Consulta/consultarFicha"
    });


}

function getEmpresaByHolding(id_holding) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_holding: id_holding },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown + jqXHR);
        },
        success: function(result) {
            cargaComboGenerico(result, 'holding');

        },
        url: "Consulta/getEmpresaByHoldingCBX"
    });
}

function cargaTablaFicha(data) {


    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }

    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "num_ficha" },
            { "mDataProp": "ejecutivo" },
            { "mDataProp": "categoria" },
            { "mDataProp": "modalidad" },
            { "mDataProp": "localidad" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    html += o.fecha_inicio + '-' + o.fecha_fin;
                    return html;
                }
            },
            { "mDataProp": "descripcion_producto" },
            { "mDataProp": "cod_sence" },
            { "mDataProp": "linea" },
            { "mDataProp": "sublinea" },
            { "mDataProp": "otic" },
            { "mDataProp": "empresa" },
            { "mDataProp": "rubro" },
            { "mDataProp": "holding" },
            { "mDataProp": "estado" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    html += '<p data-toggle="tooltip" data-placement="top" title data-original-title="Sence: ' + o.valor_sence + ' • Empresa: ' + o.valor_empresa + '" >' + o.valor_total + '</p>';
                    return html;
                }
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    html = "<a href='#' title='Subir o editar documentos' onclick='DocumentosFicha(" + o.id_ficha + "," + o.id_estado + ");'>" + o.cant_documento + "</a>";
                    return html;
                }
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = " Colo car POP UP";
                    html = "<a href='#' title='Ver Alumnos' onclick='AlumnosFicha(" + o.id_ficha + ");'>" + o.cant_alumno + "</a>";
                    html = o.cant_alumno;
                    return html;
                }
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {

                    var html = "";
                    html += '  <div class="btn-group">';

                    var a = "";
                    var b = "";

                    //                    if (o.perfil == "10") { // señora tatiana
                    //                        a = "Resumen/index/" + o.id_ficha;
                    //                        b = "ResumenArriendo/index/" + o.id_ficha;
                    //
                    //                    } else { // cualquier otro 
                    a = "ResumenSeguimiento/index/" + o.id_ficha;
                    b = "ResumenArriendoSeguimiento/index/" + o.id_ficha;

                    //                    }


                    if (o.id_categoria == 1) {
                        html += ' <a href="' + a + '"> <button class="btn btn-primary dropdown-toggle ">Resumen </button></a>';
                    }
                    if (o.id_categoria == 2) {

                        html += ' <a href="' + b + '"> <button class="btn btn-primary dropdown-toggle ">Resumen </button></a>';
                    }

                    html += ' </div>';
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Edutecno'
        }, {
            extend: 'pdf',
            title: 'Edutecno'
        }, {
            extend: 'print',
            customize: function(win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }
        }],
        "order": [
            [0, "desc"]
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function DocumentosFicha(id, id_estado) {
    $("#modal_id_ficha").text(id);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            cargaDatosDocumentoFicha(result);
            $("#modal_documentos_ficha").modal("show");
            $("#div_frm_add").css("display", "none");
            $("#div_tbl_contacto_empresa").css("display", "block");
            $("#btn_nuevo_contacto").css("display", "block");
        },
        url: "Listar/verDocumentosFicha"
    });
}

function cargaDatosDocumentoFicha(data) {


    var html = "";
    $('#tbl_documentos_ficha tbody').html(html);
    $.each(data, function(i, item) {
        html += '        <tr>';
        if (item.nombre_documento % 2 == 0) {
            html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a></td> ';
        } else {
            html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a></td> ';
        }

        html += '   <td>' + item.fecha_registro + '</td> ';

        html += '</tr>';
    });
    $('#tbl_documentos_ficha tbody').html(html);
}