$(document).ready(function () {
	recargaLaPagina();
	inicializaControles();

	$("#btn_enviar").click(function () {
		var ficha = $('#id_ficha').val();
		var num_ficha = $('#num_ficha').val();
		swal({
			title: "¿Estas Seguro que quieres Anular la ficha: " + num_ficha + " ?",
			text: "",
			type: "warning",
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Si, Anular",
			cancelButtonText: "Cancelar",
			closeOnConfirm: false,
			closeOnCancel: true
		},
				function () {
					$.ajax({
						async: false,
						cache: false,
						dataType: "json",
						type: 'POST',
						data: {id_ficha: ficha},
						error: function (jqXHR, textStatus, errorThrown) {

							alerta("Ficha", errorThrown, "error");
						},
						success: function (result) {
						},
						url: "../AnularEnviarFicha" // Se anula la ficha pero en estado 64 para que Ficha Control haga la anulación a 70
					});

					swal({
						title: "Ficha",
						text: "La ficha fué anulada correctamente",
						type: "success",
						confirmButtonClass: "btn-danger",
						confirmButtonText: "Ok",
						closeOnConfirm: true
					},
							function (isConfirm) {
								if (isConfirm) {
									location.href = '../../Listar';
								}
							});
				});
	});

	DocumentosFicha($('#id_ficha').val());
});

function DocumentosFicha(id) {

	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {id_ficha: id},
		error: function (jqXHR, textStatus, errorThrown) {

			alerta("Ficha", errorThrown, "error");
		},
		success: function (result) {
			
			cargaDatosDocumentoFicha(result);
			
		},
		url: "../verDocumentosFicha"
	});
}

function cargaDatosDocumentoFicha(data, id) {

	var html = "";
	$('#tbl_documentos_ficha tbody').html(html);
	$.each(data, function (i, item) {
		html += '        <tr>';
		if (item.nombre_documento % 2 == 0) {
			html += ' <td><a href="../../../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a></td> ';
		} else {
			html += ' <td><a href="../../../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a></td> ';
		}
		html += '   <td>' + item.fecha_registro + '</td> ';

		html += '</tr>';
	});
	$('#tbl_documentos_ficha tbody').html(html);
}
//
function EnviarCorreo(ficha) {

	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {id_ficha: ficha},
		error: function (jqXHR, textStatus, errorThrown) {

		},
		success: function (result) {
		},
		url: "../enviarCorreo"
	});
}

function inicializaControles() {
	var id_dias = $("#id_dias").val();
	var res = id_dias.split(",");
	for (var i = 0; i < res.length; i++) {
		switch (res[i]) {
			case "1":
				$("#lunes").prop("checked", true);
				break;
			case "2":
				$("#martes").prop("checked", true);
				break;
			case "3":
				$("#miercoles").prop("checked", true);
				break;
			case "4":
				$("#jueves").prop("checked", true);
				break;
			case "5":
				$("#viernes").prop("checked", true);
				break;
			case "6":
				$("#sabado").prop("checked", true);
				break;
			case "7":
				$("#domingo").prop("checked", true);
				break;
		}
	}
}
