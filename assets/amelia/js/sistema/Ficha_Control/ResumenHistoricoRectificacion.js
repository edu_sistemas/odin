$(document).ready(function () {
       recargaLaPagina();
    cargaCbxAll();
    $("#btn_back").click(function () {
        // retrocede una pagina
        limpiarFormulario();
        // history.back();
    });

    contarCaracteres("comentario", "text-out", 200);
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    inicializaControles();

    $("#btn_aprobar_rectificacion").click(function () {
        aprobarRectificacion();
    });


    $("#btn_rechazar_rectificacion").click(function () {
        var id_rectificacion = $("#id_rectificacion").val();
        abrirRechazo(id_rectificacion);
    });


    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });

    $("#btn_aceptar").click(function () {
        if (validarRequeridoForm($("#frm_rechazo"))) {
            rechazarRectificacion();
            $('#modal_rechazo').modal('hide');
            history.back();
        } else {
            event.stopPropagation();
        }
    });


});

function rechazarRectificacion() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_rechazo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            history.back();
                        }
                    });


        },
        url: "../rechazarRectificacion"
    });

}


function abrirRechazo(id) {
    $("#modal_id_rectificacion_rechazo").text(id);
    $("#id_rectificacion_rechazo").val(id);
    $("#modal_rechazo").modal("show");
}

function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() {


    var id_rectificacion = "";

    id_rectificacion = $("#id_rectificacion").val();

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosAlumnos(result);
        },
        url: "../cargaDatosOC"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../cargaDatosOCDocumentoResumen"
    });



    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosAlumnosPorRectificar(result);
        },
        url: "../cargaDatosOCRectificacion"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentosRectificacion(result);
        },
        url: "../cargaDatosOCDocumentoResumenRectificaciones"
    });


}





function cargaTablaDatosAlumnosPorRectificar(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno_rectificacion')) {
        $('#tbl_orden_compra_carga_alumno_rectificacion').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno_rectificacion').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_rectificacion"},
            {"mDataProp": "rut"},
            {"mDataProp": "nombre_alumno"},
            {"mDataProp": "apellido_paterno"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "porcentaje_franquicia"},
            {"mDataProp": "costo_otic"},
            {"mDataProp": "costo_empresa"},
            {"mDataProp": "valor_agregado"},
            {"mDataProp": "valor_total"},
            {"mDataProp": "causa"}
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}


function cargaTablaDatosAlumnos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut"},
            {"mDataProp": "nombre"},
            {"mDataProp": "fq"},
//            {"mDataProp": "valor_otic"},
//            {"mDataProp": "valor_empresa"},
//            {"mDataProp": "valor_agregado"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "valor_total"},
            {"mDataProp": "num_orden_compra"}
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentosRectificacion(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetos_rectificacion')) {
        $('#tbl_orden_compra_documetos_rectificacion').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetos_rectificacion').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            }

        ],
        pageLength: 5,
        responsive: false,
        dom: '<"clear">'

    });

    $('[data-toggle="tooltip"]').tooltip();

}



function cargaTablaDatoDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            }

        ],
        pageLength: 5,
        responsive: false,
        dom: '<"clear">'

    });

    $('[data-toggle="tooltip"]').tooltip();

}

function aprobarRectificacion() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: $("#id_rectificacion").val()},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Rectificación", errorThrown, "error");
        },
        success: function (result) {

            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function () {


                        location.href = "../../Rectificaciones";


                    });
        },
        url: "../aprobarRectificacion"
    });

}