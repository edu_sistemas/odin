$(document).ready(function () {

    llamaDatosControles();
});

function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaFichas(result);
        },
        url: "Informe/CargaTaba"
    });
}

function cargaTablaFichas(data) {

    if ($.fn.dataTable.isDataTable('#fichas')) {
        $('#fichas').DataTable().destroy();
    }

    $('#fichas').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "duracion"},
            {"mDataProp": "razon_social_empresa"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var x = o.id_ficha;
                    html = '<a   class="btn btn-primary btn-rounded btn-block" href="../Nota/Informe/GenerarExcel/' + x + '"><i class="fa fa-archive"></i> Generar Informe Notas</a>';
                    return html;
                }
            }
        ], 
        pageLength: 100,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Notas'},
            {extend: 'pdf', title: 'Notas'},
            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

//function calcularPromedios(id) { //Calcula los promedios de las notas de cada alumno
//	$.ajax({
//		async: false,
//		cache: false,
//		dataType: "json",
//		type: 'POST',
//		data: {
//			id_ficha: id
//		},
//		error: function (jqXHR, textStatus, errorThrown) {
//			// code en caso de error de la ejecucion del ajax
//			alerta('Notas', errorThrown, 'error');
//		},
//		success: function (result) {
//			for (t = 0; t < result.length; t++) {
//				$('#promedio' + t).val(result[t].promedio)
//			}
//		},
//		url: "Agregar/calcularPomedio"
//	});
//}