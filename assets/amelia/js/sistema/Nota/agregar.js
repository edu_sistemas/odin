var idalm = 0; // variable contador para los id de alumnos
var idnota = 0; // variable contador para los id de las notas
var idnota2 = 0; // variable contador para los id de los inputs id_nota desde la bd
var cvar = 3; // variable contador para insertar la columna en la posición correspondiente. (agregar evaluación)
var header = 1;
var header2 = 0; // variable contador para asignar el nombre del th nuevo ejemplo " <th>Nota " + header + " </th> ";
var trCount = 0; // variable contador para los tr de la tabla
var cnotas = 0;
var fila = 0;
var promedio = 0;
$(document).ready(function () {
    $('#reset_superior').hide();
    $('#mensaje2').hide();
    llamaDatosControles();
    $("#id_ficha_v").select2({
        placeholder: "Todos",
        allowClear: true
    });
    $('#guardarN').click(function () {
        notaMinima();
        CompletarNota();
        registrarNota();
    });
    $('#reset').click(function () {
        location.reload();
        $("#id_ficha_v").prop("disabled", false);
    });
    $('#reset_superior').click(function () {
        location.reload();
        $("#id_ficha_v").prop("disabled", false);
    });
    $("#id_ficha_v").change(function () {
        $('#largo').val('');
        $("#id_ficha_v").prop("disabled", true);
        var html3 = "";
        html3 += '<div class="alert alert-info" role="alert"> ';
        html3 += '<strong> Para consultar otra ficha, presione el botón Volver.</strong>';
        html3 += '<p></p>';
        html3 += '</div>';
        $('#mensaje2').html(html3);
        $('#mensaje2').show();
        $("#id_ficha_v").prop("disabled", true);
        if ($('#fila0 td:last input.input').val() == "") {
            QuitarNotaN();
            var $span = $('#nota thead');
            $span.find('td').wrapInner('th').contents().unwrap();
        }
        if ($("#id_ficha_v").val() != "") {
            llamaDatosFicha($("#id_ficha_v").val());
            $('#nota').show();
        }
        fila = 0;
        thCount = ($('#nota thead').find('th').length);
        tdCount = ($('#fila0').find('td').length);
        men = thCount - tdCount;
        if (thCount != tdCount) {
            for (var p = 0; p < men; p++) {
                $('#nota thead').find('th').last().remove();
                $('#row td:contains("Promedio")').last().remove();
                header--;
                cvar--;
            }
        }
        if ($('#cierre').val() == 1) {
            calcularPromedios($("#id_ficha_v").val());
            $('#row td').last().text("Promedio").css('font-weight', 'bold');
        }
        $('#reset_superior').show();
    });
    $('#cierresend').click(function () {
        cerrarCurso($("#id_ficha_v").val());
        calcularPromedios($("#id_ficha_v").val());
        setTimeout(function () {
            location.reload()
        }, 2000);
    });
    //    cerrarCurso(
    $('#guardarN').prop('disabled', true);
    $('#cierresend').prop('disabled', true);
    $('#n3').prop('disabled', true);
    $('#n2').prop('disabled', true);
    $('#nota').hide();
    $('#promedio').hide();
});

function llamaDatosFicha(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('#total').val(result.length);
            if (result[0].notas != null) {
                $('#largo').val(result[0].notas.split(',').length);
            }
            if (result[0].estado == 1) {
                $('#cierre').val(result[0].estado);
            } else {
                $('#cierre').val(result[0].estado);
            }
            generaTabla(result);
        },
        url: "Agregar/getfichaData"
    });
}

function generaTabla(data) {
    var html = "";
    promedio = 0;
    $('#n2').prop('disabled', false);
    $('#nota tbody').html(html);
    cnotas = $('#largo').val();
    $.each(data, function (i, item) {
        html += '<tr id="fila' + fila + '">';
        html += '<td>' + item.rut + '</td>';
        html += '<td>' + item.nombre + '<input type="hidden" id="dtl' + idalm + '" name="dtl' + idalm + '" value="' + item.id_detalle_alumno + '"/></td>';
        html += '<td>' + item.curso + '</td>';
        if (cnotas > 0) {

            $('#guardarN').prop('disabled', false);

            for (h = 0; h < cnotas; h++) {
                html += '<td><input value="' + item.notas.split(',')[h] + '"  type="text" size="3" class="input" id="' + idnota + '" name="' + idnota + '" maxlength="2" onkeypress="return solonumero(event)" onkeydown="return keypress(this)">';
                html += '<input value="' + item.notas_id.split(',')[h] + '" type="hidden" size="2" class="input" id="id_' + idnota2 + '" name="id_' + idnota2 + '"/>';
                $('#cierresend').prop('disabled', false);
                idnota++;
                idnota2++;
            }
            if ($('#cierre').val() == 1) {
                //curso cerrado //ademas agrega los inpu de los promedios
                $('#mensaje').show();
                var html2 = "";
                html2 += '<div class="alert alert-danger" role="alert"> ';
                html2 += '<strong> El curso está cerrado !</strong>';
                html2 += '<p></p>';
                html2 += '</div>';
                // agrega los encabezados de las columnas    
                html += '<td><input value="" id="promedio' + promedio + '" readonly size="2" ></td>';
                $('#mensaje').html(html2);
                $('#n2').prop('disabled', true);
                $('#n3').prop('disabled', true);
                $('#guardarN').remove();
                $('#cierresend').remove();
                $('#promedio').show();
                calcularPromedios($("#id_ficha_v").val());
            } else {
                var html2 = "";
                $('#mensaje').html(html2);
                $('#n2').prop('disabled', false);
                $('#n3').prop('disabled', false);
                $('#guardarN').show();
                $('#cierresend').show();
                $('#promedio').remove();
                $('#row td:contains("Promedio")').last().remove();
            }
        } else {
            $('#mensaje').hide();
        }
        idalm++;
        promedio++;
    });
    if (cnotas > 0) { // cuaando ya hay notas ingresadas
        for (var l = 0; l < cnotas; l++) {
            $("#row").append("<th><b>Nota " + header + " </b></th>");
            $('#columnas').val(header); //Es un contador de columnas, y asigna el valor al head llamado columnas en la vista
            header++;
            cvar++;
        }
    }
    if ($('#cierre').val() == 1) { //Imprime el th de la tabal que dice promedio
        var row = document.getElementById("row");
        var f = row.insertCell(cvar);
        f.innerHTML = "<th><b>Promedio</b></th>";
    }
    $('#nota tbody').html(html);
    trCount = ($('#nota').find('tr').length) - 1;
}

function AgregarNotaN() {
    $('#guardarN').prop('disabled', false);
    //Agrega una columna para agregar mós notas // Ademas
    if (cvar > 2) { //este if habilita o deshabilita el boto (-) Para agregar o quitar columnas
        $('#n3').prop('disabled', false);
    }
    var row = document.getElementById("row");
    var x = row.insertCell(cvar); //Inserta la columna despues del ultimo y cvar le indica donde debe hacerlo
    x.innerHTML = "<th><b>Nota " + header + " </b></th>"; // agrega los encabezados de las columnas    
    $('#columnas').val(header); //Es un contador de columnas, y asigna el valor alhead llamado columnas en la vista
    var inpt = "<td><input class='input' columna='" + trCount + "'  type='text' size='2' maxlength='3' onkeypress='return solonumero(event) , flotante(this)' onkeydown='return keypress(this)'/>";
    inpt += '<input class="perro" columna="' + trCount + '" type="hidden" size="2"/> </td>';
    $('#nota tbody tr').append(inpt).last();
    $('tbody input.input').each(function (i) {
        if ($(this).attr("columna") == trCount) { //agrega los id correlativo al input de ingreso de 
            this.name = idnota;
            this.id = idnota;
            idnota++;
        }
    });
    $('tbody input.perro').each(function (i) {
        if ($(this).attr("columna") == trCount) { //agrega los id correlativo al input de ingreso de 
            this.name = 'id_' + idnota2;
            this.id = 'id_' + idnota2;
            idnota2++;
        }
    });
    trCount++;
    header++;
    header2++;
    cvar++;
}

function QuitarNotaN() {
    var total1 = parseInt($('#total').val()); // esta variable define el total de alumnos y sirve para restar los id al quitar una columna entera y
    //al momento de volver ha agregar una columan el id no tenga problemas
    $('#nota').find('tr').each(function () {
        $(this).find('td,th').last().remove();
        if (cvar == 4) {
            $('#n3').prop('disabled', true);
            $('#guardarN').prop('disabled', true);
        }
    });
    cvar--;
    header--;
    header2--;
    idnota = idnota - total1;
    idnota2 = idnota;
    $('#columnas').val('');
    $('#columnas').val(header2);
}

function notaMinima() { //Agrega nota minima para que se llene cuando el input de nota queda vacio
    var total = parseInt($('#total').val());
    var aument = ($('#columnas').val()) * 2;
    var multi = total * aument;
    for (x = 0; x < multi; x++) {
        if ($('#' + x).val() == null || $('#' + x).val() == "") {
            $('#' + x).val("1.0").css('color', 'red');
        }
    }
}

function keypress(element) {
    var code = event.which || event.keyCode; // captura los eventos de las teclas
    if ($('#' + element.id).val().length == 0) { //sobre el input que va ha actuar
        if (code == 96 || code == 104 || code == 105 || code == 48 || code == 56 || code == 57) {
            //bloque si quiere poner nota mayor a 7 ó 0
            return false;
        }
        //  alert('here');
        // $('#' + element.id).after('.');
    } else {
        //alert('here2');
        if (code != 8 || code == 46) { //Permite borrar cuando la nota es 0
            if ($('#' + element.id).val() == "7" || $('#' + element.id).val() == "7.") { //Cuando la primera nota es 0, bloquea los demas números para no permitir poner notas mayores ha 7.0
                $('#' + element.id).val("7.0");
                code == 97; // 1
                code == 98; // 2
                code == 99; // 3
                code == 100; // 4
                code == 101; // 5
                code == 102; // 6
                code == 103; // 7
                code == 104; // 8
                code == 105; // 9
                code == 57; // 9 arriba
                code == 56; // 8 arriba
                code == 55; // 7 arriba
                code == 54; // 6 arriba
                code == 53; // 5 arriba
                code == 52; // 4 arriba
                code == 51; // 3 arriba
                code == 50; // 2 arriba
                code == 49; // 1 arriba 
                return false;
            } else {
                code == 96; // 0 
                code == 104; // 8
                code == 105; // 9
                code == 48; // 0 arriba
                code == 56; // 8 arriba
                code == 57; // 9 arriba
                return true;
            }
        } else { // Si no es nota 7 habilita los numero para poner notas como: 6.9
            code == 96; // 0 
            code == 104; // 8
            code == 105; // 9
            code == 48; // 0 arriba
            code == 56; // 8 arriba
            code == 57; // 9 arriba
            return true;
        }
    }

}

function flotante(a) {
    console.log(a);
    var h56 = $('#total').val();
    if ($('#' + a.id).val().length === 1) {
        return   $('#' + a.id).val($('#' + a.id).val() + ".");
    } else {
    }


    //  var h56 = $('#total').val();
//  var h56 = 10000;
//    for (b = 0; b < h56; b++) {
//		console.log($('#' + b));
//        if ($('#' + b).val().length() === 1) {
//            return $('#' + b).val($('#' + b).val() + ".");
//        } else {
//        }
//    }
}

function CompletarNota() { //completa las notas cuando se le inserta un puro numero
    var total = parseInt($('#total').val());
    var ttl = ($('#total').val()) - 1;
    var aument = $('#columnas').val();
    var multi = total * aument;
    for (x = 0; x < multi; x++) {
        var cnv = $('#' + x).val();
        switch (cnv) {
            case "1":
                cnv = $('#' + x).val("1.0");
                break;
            case "2":
                cnv = $('#' + x).val("2.0");
                break;
            case "3":
                cnv = $('#' + x).val("3.0");
                break;
            case "4":
                cnv = $('#' + x).val("4.0");
                break;
            case "5":
                cnv = $('#' + x).val("5.0");
                break;
            case "6":
                cnv = $('#' + x).val("6.0");
                break;
            case "7":
                cnv = $('#' + x).val("7.0");
                break;

            case "1.":
                cnv = $('#' + x).val("1.0");
                break;
            case "2.":
                cnv = $('#' + x).val("2.0");
                break;
            case "3.":
                cnv = $('#' + x).val("3.0");
                break;
            case "4.":
                cnv = $('#' + x).val("4.0");
                break;
            case "5.":
                cnv = $('#' + x).val("5.0");
                break;
            case "6.":
                cnv = $('#' + x).val("6.0");
                break;
            case "7.":
                cnv = $('#' + x).val("7.0");
                break;
        }
    }
}

function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result);
        },
        url: "Agregar/getFichas"
    });
}

function cargaCombo(midata) {
    $("#id_ficha_v").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}

function registrarNota() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_notas_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta('Notas', errorThrown, 'error');
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok"
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });
        },
        url: "Agregar/setRegistrarNotas"
    });
}

function cerrarCurso(id) { //Basicamente actualiza el estado de las nota ha 1
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta('Notas', errorThrown, 'error');
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok"
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });
        },
        url: "Agregar/cerrarCurso"
    });
}

function calcularPromedios(id) { //Calcula los promedios de las notas de cada alumno
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta('Notas', errorThrown, 'error');
        },
        success: function (result) {
            for (t = 0; t < result.length; t++) {
                $('#promedio' + t).val(result[t].promedio)
            }
        },
        url: "Agregar/calcularPomedio"
    });
}