var url = location.hostname == "localhost" ? "http://" + location.hostname + "/odin/back_office/Home/" : "http://" + location.hostname + "/back_office/Home/";
function getCBX(name, sp_name) {
    $.ajax({
        url: url + "getCBX/" + sp_name,
        type: 'POST',
        dataType: "json",
        success: function (result) {
            cargaCBX(name, result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar los datos del select: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
}
function getCBX_select2(name, sp_name) {
    $.ajax({
        url: url + "getCBX/" + sp_name,
        type: 'POST',
        dataType: "json",
        success: function (result) {
            cargaCombo(name, result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar los datos del select: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
}
function getCBXByID_select2(name, sp_name, id) {
    var resultado;
    $.ajax({
        url: url + "getCBXByID/" + sp_name,
        type: 'POST',
        dataType: "json",
        data: {id: id},
        success: function (result) {
            resultado = result;
            if(name != ""){
                cargaCombo(name, result);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar los datos del select: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
    return resultado;
}
function enviarFormulario(name, sp_name) {
    $.ajax({
        url: url + "enviarFormulario/" + sp_name,
        type: 'POST',
        dataType: "json",
        data: $("#" + name).serialize(),
        success: function (result) {
            swal({
                title: "Enviado",
                text: "La información ha sido enviada exitosamente",
                type: "success",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    //location.reload();
                });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "Problema al enviar el formulario: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
}

function cargaCombo(item, midata) {
    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
    $("#" + item + " option[value='0']").remove();
    if ("otic" == item) {
        $("#otic option[value='0']").remove();
    }
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");
}

function alerta(titulo, contenido, tipo) {

    /*
     tipos
     type: "error",
     type: "warning",
     type: "success",
     */

    swal({
        title: titulo,
        text: contenido,
        type: tipo
    });
}


function validarRequeridoForm($form) {
    var salida = true;
    $form.find('[requerido="true"]').each(function () {
        var $item = $(this);
        //debugger;

        switch ($item[0].tagName.toLowerCase()) {
            case 'input':
                switch ($item[0].type.toLowerCase()) {
                    case 'text':
                        if ($item.val().trim() == "") {
                            if ($("#" + $item.attr("name") + "_error").length == 0) {
                                $($item).focus().after("<span id ='" + $item.attr("name") + "_error' style='font-weight:bold;color:#FF0000;' class='error'>" +
                                    $item.attr('mensaje') + "</span>");
                                $($item).css('border-color', '#ed5565');
                            }

                            salida = false;
                        } else {

                            $("#" + $item.attr("name") + "_error").remove();
                            $($item).css('border-color', '#aaaaaa').addClass();


                        }

                    case 'number':
                        if ($item.val().trim() == "") {
                            if ($("#" + $item.attr("name") + "_error").length == 0) {
                                $($item).focus().after("<span id ='" + $item.attr("name") + "_error' style='font-weight:bold;color:#FF0000;' class='error'>" +
                                    $item.attr('mensaje') + "</span>");
                                $($item).css('border-color', '#ed5565');
                            }

                            salida = false;
                        } else {

                            $("#" + $item.attr("name") + "_error").remove();
                            $($item).css('border-color', '#aaaaaa').addClass();



                        }

                    case 'email':
                        if ($item.val().trim() == "") {
                            if ($("#" + $item.attr("name") + "_error").length == 0) {
                                $($item).focus().after("<span id ='" + $item.attr("name") + "_error' style='font-weight:bold;color:#FF0000;' class='error'>" +
                                    $item.attr('mensaje') + "</span>");
                                $($item).css('border-color', '#ed5565');
                            }

                            salida = false;
                        } else {

                            $("#" + $item.attr("name") + "_error").remove();
                            $($item).css('border-color', '#aaaaaa').addClass();




                        }


                        break;
                    case 'checkbox':
                        /*val = val.toLowerCase()
                         if((val == true)||(val == 1)||(val == 'si')){
                         $item.prop('checked', true);
                         }else{
                         $item.prop('checked', false);
                         }*/
                        //
                        break;
                    case 'radio':
                        //debugger;
                        //console.log($item);
                        /*$item.each(function () {
                         var valor = $(this).val();
                         //console.log(valor);
                         if (String(valor) == String(val)) {
                         $(this).prop('checked', true);
                         }
                         });*/
                        var name = $item.attr('name');
                        if ($item.parent().parent().find('[name="' + name + '"]:checked').length == 0) {

                            if ($("#" + name + "_error").length == 0) {
                                $($item).focus().after("<span id ='" + name + "_error' style='font-weight:bold;color:#FF0000;' class='error'>" +
                                    $item.attr('mensaje')
                                    + "</span>");
                                $($item).css('border-color', '#ed5565');
                            }
                            salida = false;
                        }
                        ;
                        break;
                    default:
                        if ($item.val() == "") {



                            if ($("#" + $item.attr("name") + "_error").length == 0) {
                                $($item).focus().after("<span id ='" + $item.attr("name") + "_error' style='font-weight:bold;color:#FF0000;' class='error'>" +
                                    $item.attr('mensaje')
                                    + "</span>");
                                $($item).css('border-color', '#ed5565');
                            }
                            salida = false;

                        }
                        ;
                }
                break;
            case 'select':
                //alert($item.val() +' ' + $item.attr('name'));
                if ($item.val() == null || $item.val() == "") {


                    if ($("#" + $item.attr("name") + "_error").length == 0) {
                        $($item).focus().after("<span id ='" + $item.attr("name") + "_error' style='font-weight:bold;color:#FF0000;' class='error'>" +
                            $item.attr('mensaje')
                            + "</span>");
                        $($item).css('border-color', '#ed5565');
                    }
                    salida = false;


                } else {
                    $("#" + $item.attr("name") + "_error").remove();
                    $($item).css('border-color', '#aaaaaa').addClass();

                }
                ;
                break;
            case 'textarea':
                if ($item.val().trim() == "") {

                    if ($("#" + $item.attr("name") + "_error").length == 0) {
                        $($item).focus().after("<span id ='" + $item.attr("name") + "_error' style='font-weight:bold;color:#FF0000;' class='error'>" +
                            $item.attr('mensaje')
                            + "</span>");
                        $($item).css('border-color', '#ed5565');
                    }

                    salida = false;


                } else {
                    $("#" + $item.attr("name") + "_error").remove();
                    $($item).css('border-color', '#aaaaaa').addClass();

                }
                ;
                break;
            default:
                if ($item.val().trim() == "") {

                    if ($("#" + $item.attr("name") + "_error").length == 0) {
                        $($item).focus().after("<span id ='" + $item.attr("name") + "_error' style='font-weight:bold;color:#FF0000;' class='error'>" +
                            $item.attr('mensaje') + "</span>");
                        $($item).css('border-color', '#ed5565');
                    }

                    salida = false;
                }
                ;
        }
    });
    return salida;
}


function cargaCBX(idcbx, data) {

    var html = "";


    html += '<option value="">Seleccione</option>';



    $.each(data, function (i, item) {
        html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
    });

    $("#" + idcbx).html(html);

}

function cargaCBXSelected(idcbx, data, seleccionado) {

    var html = "";

    // html += '<option value="0" >Seleccione</option>';

    $.each(data, function (i, item) {

        if (item.id == seleccionado) {
            html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '" selected>' + item.text + '</option>';

        } else {
            html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';

        }



    });

    $("#" + idcbx).html(html);

}


function contarCaracteres(idtextarea, idcontador, max) {
    $("#" + idtextarea).keyup(function () {
        updateContadorTa(idtextarea, idcontador, max);
    });

    $("#" + idtextarea).change(function () {
        updateContadorTa(idtextarea, idcontador, max);
    });

}

function updateContadorTa(idtextarea, idcontador, max) {
    var contador = $("#" + idcontador);
    var ta = $("#" + idtextarea);

    contador.text(" 0/" + max + " caracteres restantes");

    contador.text(ta.val().length + "/" + max + " caracteres restantes");
    if (parseInt(ta.val().length) > max) {
        ta.val(ta.val().substring(0, max - 1));
        contador.text(max + "/" + max + " caracteres restantes");
    }

}


function solonumero(event) {

    if (event.keyCode < 48 || event.keyCode > 57) {
        event.returnValue = false;
    }
}

function simpleLoad(btn, state) {
    if (state) {
        btn.children().addClass('fa-spin');
        btn.contents().last().replaceWith(" Cargando");
    } else {
        setTimeout(function () {
            btn.children().removeClass('fa-spin');
            btn.contents().last().replaceWith(" Actualizar");
        }, 100);
    }
}

function arrayObjectIndexOf(myArray, searchTerm, property) {
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm)
            return i;
    }
    return -1;
}


function recargaLaPagina() {

    var pathname = window.location.pathname; // Returns path only
    //var url = window.location.href;     // Returns full URL

    var urls_sitio = [];
    var urls_sitio_json;
    var d = new Date();
    var dia = d.getDate();


    if (localStorage.getItem("urls_sitio")) {
        console.log(localStorage.getItem("urls_sitio"));
        urls_sitio_json = localStorage.getItem("urls_sitio");
        urls_sitio = JSON.parse(urls_sitio_json);

        var existe_posicion = arrayObjectIndexOf(urls_sitio, pathname, "ruta");

        if (existe_posicion >= 0) {


            if (urls_sitio[existe_posicion].dia != dia) {

                urls_sitio.splice(urls_sitio[existe_posicion]);
                urls_sitio.push({ ruta: pathname, dia: dia });
                urls_sitio_json = JSON.stringify(urls_sitio);
                localStorage.setItem("urls_sitio", urls_sitio_json);
                location.reload();

            } else {

                //  alert('NO Reload Dia  ' + dia);
            }

        } else {
            // alert('New URL  ' + dia);

            urls_sitio.push({ ruta: pathname, dia: dia });
            urls_sitio_json = JSON.stringify(urls_sitio);
            localStorage.setItem("urls_sitio", urls_sitio_json);
            location.reload();

        }

    } else {

        urls_sitio.push({ ruta: pathname, dia: dia });
        urls_sitio_json = JSON.stringify(urls_sitio);
        localStorage.setItem("urls_sitio", urls_sitio_json);
        location.reload();

    }

}

