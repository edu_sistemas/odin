// JavaScript Document
$(document).ready(function () {
	recargaLaPagina();
	$("#btn_buscar_ficha").click(function () {
		llamaFichas();
	});
	cargaCBXall();

	llamaFichas();

	$('button#btnGenerarPDF').on('click', function () {


	});


});

function llamaFichas() {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#frm').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			// console.log(textStatus + errorThrown);
			console.log(jqXHR, textStatus, errorThrown);
		},
		success: function (result) {
			console.log(result);
			TablaFichas(result);
		},
		url: "Listar/fichas"
	});
}

function cargaCBXall() {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'GET',
		//data: $('#frm').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			// console.log(textStatus + errorThrown);
			console.log(jqXHR, textStatus, errorThrown);
		},
		success: function (result) {
			console.log(result);
			cargaCornete(result, "empresa");
		},
		url: "Listar/getEmpresasCBX"
	});
}

function cargaCornete(midata, item) {

	$("#" + item).select2({
		placeholder: "Seleccione",
		allowClear: true,
		data: midata
	});

	$("#" + item + " option[value='0']").remove();

	if ("otic" == item) {
		$("#otic option[value='1']").remove();
	}

}

function TablaFichas(data) {
	if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
		$('#tbl_ficha').DataTable().destroy();
	}
	$('#tbl_ficha').DataTable({
		"aaData": data,
		"aoColumns": [
			{"mDataProp": "num_ficha"},
			{"mDataProp": "curso"},
			{"mDataProp": "nombre_modalidad"},
			{"mDataProp": "razon_social_empresa"},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {
 
					var html = "";
					html += '  <div class="btn-group">';
					html += '<a target="_blank" href="../Sticker/Listar/generarSticker/' + o.id_ficha + '" target="_blank" ><button class="btn btn-info btn-rounded">Generar Autoadhesivos</button></a>';
 					html += '<button class="btn btn-info btn-rounded" onclick="jsPdf(' + o.id_ficha + ');">Generar js</button>';
 					html += ' </div>';
					return html;

				}
			}
		],
		pageLength: 25,
		responsive: false,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [{
				extend: 'copy'
			}, {
				extend: 'csv'
			}, {
				extend: 'excel',
				title: 'Empresa'
			}, {
				extend: 'pdf',
				title: 'Sence'
			}, {
				extend: 'print',
				customize: function (win) {
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					$(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
				}
			}], order: [0, 'desc']
	});
}
function MaysPrimera(string){
  	return string.toLowerCase().replace(/\b[a-z]/g, function(letter) {
    	return letter.toUpperCase();
	});
}
function jsPdf(id_ficha){
	var resultado;
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: { idFicha : id_ficha },
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			// console.log(textStatus + errorThrown);
			console.log(jqXHR, textStatus, errorThrown);
		},
		success: function (result) {
			console.log(result);
			resultado = result ;
		},
		url: "Listar/generarStickerJS"
	});

	var doc = new jsPDF("p", "pt", "letter");
	doc.setFontSize(14);
	for(i=0;i < resultado.length; i = i+2){

		doc.text(60, 90, 'Linares y Compañia');
		doc.text(60, 105, MaysPrimera(resultado[i].nombre));
		doc.text(60, 120, 'Santiago fono: 24398800');

		doc.text(320, 90, 'Linares y Compañia');
		doc.text(320, 105, MaysPrimera(resultado[i+1].nombre));
		doc.text(320, 120, 'Santiago fono: 24398800');

	}
	

	
	doc.addPage();
	doc.text(20, 20, 'Do you like that?');

	doc.save('Test.pdf');
}