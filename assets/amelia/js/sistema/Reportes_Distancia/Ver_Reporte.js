var d = new Date();
var ficha_alumno, rut, nombre_alumno, email_alumno, telefono_alumno, codigo_tablet, nombre_curso;
$(document).ready(function () {
    ListarAlumnos();
});

function ListarAlumnos() {
    var id_ficha, rut_alumno;
    id_ficha = $("#id_ficha").val();
    rut_alumno = $("#rut").val();

    $.ajax({
        url: "../../getReporteAlumno",
        type: 'POST',
        dataType: "json",
        data: {id_ficha: id_ficha, rut_alumno: rut_alumno},
        beforeSend: function () {
            $("#cargando").html('<img src="../../../../../assets/img/loading.gif">');
        },
        success: function (result) {
            console.log(result);

            ficha_alumno = result[0].num_ficha;
            rut= result[0].rut;
            nombre_alumno = result[0].nombre_completo;
            email_alumno = result[0].email;
            telefono_alumno = result[0].telefono_alumno;
            codigo_tablet = result[0].codigo_tablet;
            nombre_curso = result[0].nombre_curso;
            

            $("#ficha_alumno").text(ficha_alumno);
            $("#rut_alumno").text(rut);
            $("#nombre_alumno").text(nombre_alumno);
            $("#email_alumno").text(email_alumno);
            $("#email_alumno").attr("href", "mailto:" + email_alumno);
            $("#telefono_alumno").text(telefono_alumno);
            $("#codigo_tablet").text(codigo_tablet);
            $("#nombre_curso").text(nombre_curso);

            var id_ficha = result[0].id_ficha;
            var promedio_diagnostico = result[0].promedio_diagnostico;
            var promedio_evaluacion_final = result[0].promedio_evaluacion_final;
            var fecha_realizacion_evaluacion_diagnostica = result[0].fecha_realizacion_evaluacion_diagnostica;
            var fecha_realizacion_evaluacion_final = result[0].fecha_realizacion_evaluacion_final;

            var html ="";
            $("#tbl_global tbody").html("");
            
            var btnPdf = '<img align="middle" id="pdf-icon"src="../../../../../assets/img/icon-pdf.ico" style="width:40px; cursor:pointer;" class="pull-left" onclick="generatePDF(\'tbl_global\',0);" /><br><br><h4> Exportar reporte a PDF </h4>';
            $("#tbl_global").before(btnPdf);

            html += '<tr><th style="border-bottom: 1px solid #858585; text-align: left; width:35%;" >Reporte Progreso Alumno Tablet Distacia</th><th style="border-bottom: 1px solid #858585; text-align: center; width:65%;"></th></tr>';
            html += '<tr>';
            html += '<th style="border-bottom: 1px solid #858585;">Nota E. Diagnóstica: </th>';
            html += '<td style="width: 80%; border-bottom: 1px solid #858585;">';
            html += '<div class="progress">';
            html += '<div id="promedio_diagnostico" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + ((promedio_diagnostico * 100) / 7) + '%;">';
            html += promedio_diagnostico;
            html += '</div>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';

            html += '<tr>';
            html += '<th>Fecha evaluación: </th>';
            html += '<td style="width: 80%">';
            html += fecha_realizacion_evaluacion_diagnostica;
            html += '</td>';
            html += '</tr>';


            var unidades = getUnidadesReporteAlumno(result[0].id_reporte);

            console.log(unidades);

            for (let i = 0; i < unidades.length; i++) {
                html += '<tr><td colspan="2" style="background-color: #dafaf3;"><b>UNIDAD' + (i + 1) + ' (' + unidades[i].id_unidad + ')</b></tr>';

                html += '<tr>';
                html += '<td style="border-top: 1px solid #858585;">Progreso: </td>';
                html += '<td style="width: 80%;border-top: 1px solid #858585;">';
                html += '<div class="progress">';
                html += '<div id="promedio_diagnostico" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + unidades[i].prom_porcentaje_unidad + '%;">';
                html += unidades[i].prom_porcentaje_unidad+'%';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                html += '</tr>';

                

                var modulos = getModulosReporteAlumno(result[0].id_reporte, unidades[i].id_unidad);
                
                for (let j = 0; j < modulos.length; j++) {
                    html += '<tr><td colspan="2"><b>Modulo ' + (j + 1) + ' (' + modulos[j].id_modulo + ')</b></tr>';

                    html += '<tr>';
                    html += '<td style="border-top: 1px solid #858585;">Progreso: </td>';
                    html += '<td style="width: 80%;border-top: 1px solid #858585;">';
                    html += '<div class="progress">';
                    html += '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + modulos[j].prom_porcentaje_modulo + '%;">';
                    html += modulos[j].prom_porcentaje_modulo + '%';
                    html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '</tr>';

                    html += '<tr>';
                    html += '<td>Nota Evaluación Módulo : </td>';
                    html += '<td style="width: 80%">';
                    html += '<div class="progress">';
                    html += '<div id="promedio_diagnostico" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + ((modulos[j].prom_nota_modulo * 100) / 7) + '%;">';
                    html += modulos[j].prom_nota_modulo;
                    html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '</tr>';

                    html += '<tr>';
                    html += '<td>Fecha evaluación: </td>';
                    html += '<td style="width: 80%">';
                    html += modulos[j].fecha_realizacion_evaluacion;
                    html += '</td>';
                    html += '</tr>';
                    
                }
            }

            html += '<tr>';
            html += '<th>Nota E. Final: </th>';
            html += '<td style="width: 80%">';
            html += '<div class="progress">';
            html += '<div id="promedio_evaluacion_final" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"';
            html += 'style="width: ' + ((promedio_evaluacion_final * 100) / 7) +'%;">';
            html += promedio_evaluacion_final;
            html += '</div>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';
            
            html += '<tr>';
            html += '<th>Fecha evaluación: </th>';
            html += '<td style="width: 80%">';
            html += fecha_realizacion_evaluacion_final;
            html += '</td>';
            html += '</tr>';

            $("#tbl_global tbody").append(html);


            if(result.length > 0){
                $('.ibox-title h5').append(result[0].num_ficha);
                $("#contenido").css("display", "block");
            }else{
                swal({
                    title: "Error",
                    text: "No existen códigos en esta ficha",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                        window.history.back();
                    });
            }
            
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar los codigos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        complete: function () {
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });
}
function cargaTablaAlumnos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_alumnos')) {
        $('#tbl_alumnos').DataTable().destroy();
    }

    $('#tbl_alumnos').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "rut" },
            { "mDataProp": "nombre_completo" },
            { "mDataProp": "num_orden_compra" },
            { "mDataProp": "codigo_tablet" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    
                    html += '<div class="btn-group">';
                    html += '<button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">Opciones <span class="caret"></span></button>';
                    html += '<ul class="dropdown-menu">';
                    if (o.id_reporte != null) {
                        var x = "'editar/index/" + o.id_reporte + "'";
                        html += '<li><a onclick="location.href=' + x + '">Ver Reporte Individual</a></li>';
                    }else{
                        html += '<li><a><strike>Ver Reporte Individual</strike></a></li>';
                    }
                    html += '</ul>';
                    html += '</div>';
                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Categoria'
            }, {
                extend: 'pdf',
                title: 'Categoria'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        order: [[1, "asc"]]
    });
}

function getCodigoTabletByidAlumno(id_alumno) {
    return '<img src="../../../../assets/img/loading.gif" style="width:25px">';
}

function getUnidadesReporteAlumno(codigo_tablet){
    var resultado = new Array();
        $.ajax({
            url: "../../getUnidadesReporteAlumno",
            type: 'POST',
            dataType: "json",
            data: {codigo_tablet: codigo_tablet},
            success: function (result) {
                resultado = result;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                swal({
                    title: "Error",
                    text: "No se ha podido cargar los datos: " + errorThrown,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Recargar",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                        location.reload();
                    });
            },
            async: false,
            cache: false
        });

    return resultado;
}

function getModulosReporteAlumno(codigo_tablet, id_unidad) {
    var resultado = new Array();
    $.ajax({
        url: "../../getModulosReporteAlumno",
        type: 'POST',
        dataType: "json",
        data: { codigo_tablet: codigo_tablet, id_unidad: id_unidad},
        success: function (result) {
            resultado = result;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se ha podido cargar los datos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });

    return resultado;
}

function generatePDF(id_table) {

    var doc = new jsPDF();
    doc.setFontSize(8);
    doc.text("EDUTECNO CAPACITACIÓN", 14, 10);
    doc.setFontSize(16);
    doc.text("Reporte Progreso Alumno Tablet Distacia", 14, 15);
    doc.setFontSize(10);
    doc.text("Fecha de emisión del informe: " + d.getDate().toString() + '-' + (d.getMonth() + 1).toString() + '-' + d.getFullYear().toString(), 14, 20);
    doc.text("Nombre Alumno: "+nombre_alumno, 14, 25);
    doc.text("RUT: "+rut, 14, 30);
    doc.text("Ficha N°: "+ficha_alumno, 14, 35);
    doc.text("Curso: "+nombre_curso, 14, 40);

    var elem = document.getElementById(id_table);

    var res = doc.autoTableHtmlToJson(elem);

    doc.autoTable(res.columns, res.data, {
    startY: 45,
    headerStyles: {
    overflow: 'linebreak',
    fontSize: 9
    },
    columnStyles: {
            0: { columnWidth: 100, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            1: { columnWidth: 80, overflow: 'linebreak', halign: 'left', fontSize: 8 }
        }
    });

    doc.save('informe_' + d.getDate().toString() + '_' + (d.getMonth() + 1).toString() + '_' + d.getFullYear().toString() + '_' + d.getTime().toString() + '.pdf');
}