var base64_characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
var base64_charactersArray = base64_characters.split("");

var base64_charactersRandom = "uxlD6KUVWd=be3wHvCcPZTaIM+rqzOhm8A/BSXLpFi7NoJs2fG5tQnjkYgE01R49y";
var base64_charactersRandomArray = base64_charactersRandom.split("");
var d = new Date();
var id_ficha = $(location).attr('href').split("/").reverse().splice(0, 1).toString();
var num_ficha, nombre_curso;
$(document).ready(function () {
    ListarAlumnos();
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
});

function ListarAlumnos() {
    $.ajax({
        url: "../getAlumnosbyIDFicha",
        type: 'POST',
        dataType: "json",
        data: { id_ficha: id_ficha },
        beforeSend: function () {
            $("#cargando").html('<img src="../../../../assets/img/loading.gif">');
        },
        success: function (result) {
            num_ficha = result[0].num_ficha;
            nombre_curso = result[0].nombre_curso;
            var id_ficha = result[0].id_ficha;
            var total_alumnos = result[0].total_alumnos;
            var cantidad_reportes = result[0].cantidad_reportes;
            var promedio_diagnostico = result[0].promedio_diagnostico;
            var promedio_evaluacion_final = result[0].promedio_evaluacion_final;

            var html = "";
            $("#tbl_global tbody").html("");

            var btnPdf = '<br><button class="btn btn-danger btn-sm" onclick="generatePDF(\'tbl_global\',0);">Exportar reporte global a PDF</button>';
            $("#tbl_global").before(btnPdf);

            html += '<tr><th style="border-bottom: 1px solid #858585; text-align: left; width:35%;" >Reporte Progreso Global Curso Tablet Distacia</th><th style="border-bottom: 1px solid #858585; text-align: center; width:65%;"></th></tr>';
            html += '<tr><th>Total Alumnos: </th><td id="total_alumnos"><b>' + total_alumnos + '</b></td></tr>';
            html += '<tr><th>Alumnos con Reporte: </th><td style="width: 80%">';
            html += '<div class="progress">';
            html += '<div id="cantidad_reportes" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + ((cantidad_reportes * 100) / total_alumnos) + '%;">';
            html += cantidad_reportes;
            html += '</div>';
            html += '</div>';
            html += '</td></tr>';

            html += '<tr>';
            html += '<th style="border-bottom: 1px solid #858585;">Promedio E. Diagnóstica: </th>';
            html += '<td style="width: 80%; border-bottom: 1px solid #858585;">';
            html += '<div class="progress">';
            html += '<div id="promedio_diagnostico" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + ((promedio_diagnostico * 100) / 7) + '%;">';
            html += promedio_diagnostico;
            html += '</div>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';
            var unidades = getUnidadesByIDFicha(id_ficha);

            console.log(unidades);

            for (let i = 0; i < unidades.length; i++) {
                html += '<tr><td colspan="2" style="background-color: #dafaf3;"><b>UNIDAD' + (i + 1) + ' (' + unidades[i].id_unidad + ')</b></tr>';

                html += '<tr>';
                html += '<td style="border-top: 1px solid #858585;">Progreso General: </td>';
                html += '<td style="width: 80%;border-top: 1px solid #858585;">';
                html += '<div class="progress">';
                html += '<div id="promedio_diagnostico" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + unidades[i].prom_porcentaje_unidad + '%;">';
                html += unidades[i].prom_porcentaje_unidad + '%';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                html += '</tr>';



                var modulos = getModulosByFichaYUnidad(id_ficha, unidades[i].id_unidad);

                for (let j = 0; j < modulos.length; j++) {
                    html += '<tr><td colspan="2"><b>Modulo ' + (j + 1) + ' (' + modulos[j].id_modulo + ')</b></tr>';

                    html += '<tr>';
                    html += '<td style="border-top: 1px solid #858585;">Progreso General: </td>';
                    html += '<td style="width: 80%;border-top: 1px solid #858585;">';
                    html += '<div class="progress">';
                    html += '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + modulos[j].prom_porcentaje_modulo + '%;">';
                    html += modulos[j].prom_porcentaje_modulo + '%';
                    html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '</tr>';

                    html += '<tr>';
                    html += '<td>Promedio Nota Módulo : </td>';
                    html += '<td style="width: 80%">';
                    html += '<div class="progress">';
                    html += '<div id="promedio_diagnostico" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: ' + ((modulos[j].prom_nota_modulo * 100) / 7) + '%;">';
                    html += modulos[j].prom_nota_modulo;
                    html += '</div>';
                    html += '</div>';
                    html += '</td>';
                    html += '</tr>';

                }
            }

            html += '<tr>';
            html += '<th>Promedio E. Final: </th>';
            html += '<td style="width: 80%">';
            html += '<div class="progress">';
            html += '<div id="promedio_evaluacion_final" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"';
            html += 'style="width: ' + ((promedio_evaluacion_final * 100) / 7) + '%;">';
            html += promedio_evaluacion_final;
            html += '</div>';
            html += '</div>';
            html += '</td>';
            html += '</tr>';

            $("#tbl_global tbody").append(html);

            cargaTablaAlumnos(result);



            if (result.length > 0) {
                $('.ibox-title h5').append(result[0].num_ficha);
                $("#contenido").css("display", "block");
            } else {
                swal({
                    title: "Error",
                    text: "No existen códigos en esta ficha",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                        window.history.back();
                    });
            }

        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar los codigos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        complete: function () {
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });
}
function cargaTablaAlumnos(data) {
    console.log('cargaTablaAlumnos:');
    console.log(data);
    if ($.fn.dataTable.isDataTable('#tbl_alumnos')) {
        $('#tbl_alumnos').DataTable().destroy();
    }

    $('#tbl_alumnos').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "rut" },
            { "mDataProp": "nombre_completo" },
            { "mDataProp": "num_orden_compra" },
            { "mDataProp": "codigo_tablet" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '<button class="btn btn-warning" onclick="getXMLbyRUT(' + o.rut_alumno + ', \'' + o.rut + '\', \'' + o.nombre_completo + '\')"><i class="fa fa-database"></i>Buscar</button>'
                    return html;
                }
            },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html += '<div class="btn-group">';
                    html += '<button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">Opciones <span class="caret"></span></button>';
                    html += '<ul class="dropdown-menu">';
                    if (o.id_reporte != null) {
                        var x = "'../../Ver_Reporte/index/" + o.rut_alumno + "/" + o.id_ficha + "'";
                        html += '<li><a onclick="location.href=' + x + '">Ver Reporte Individual</a></li>';
                    } else {
                        html += '<li><a><strike>Ver Reporte Individual</strike></a></li>';
                    }
                    html += '</ul>';
                    html += '</div>';
                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Categoria'
            }, {
                extend: 'pdf',
                title: 'Categoria'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        order: [[1, "asc"]]
    });
}

function getXMLbyRUT(rut, rut_completo, nombre) {
    var str = rut.toString();
    var rut_con_puntos = str.slice(0, -6) + '.' + str.slice(-6, -3) + '.' + str.slice(-3);
    $.ajax({
        url: "../getXMLbyRUT",
        type: 'POST',
        dataType: "json",
        data: { rut: rut, rut_con_puntos: rut_con_puntos },
        success: function (result) {
            console.log(result);
            var xmls = [];
            if (result.length > 0) {
                // Create x2js instance with default config
                var select_options = {};
                $.each(result, function (index, value) {
                    var x2js = new X2JS();
                    xmls.push(x2js.xml_str2json(value.json));
                });
                $.each(xmls, function (index, value) {
                    select_options[index] = value.registro.curso[0] + ' - [actualiz.: ' + result[index].fecha + ']';
                });

                swal({
                    title: 'Seleccione Curso',
                    input: 'select',
                    inputOptions: select_options,
                    inputPlaceholder: 'Seleccione Curso',
                    showCancelButton: true,
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            console.log(value);

                            if (value == '') {
                                resolve('Debe Seleccionar un curso')
                            } else {
                                var xml = xmls[value];
                                console.log(xml);
                                $('.modal-title').text(nombre + ' ' + rut_completo);
                                var html = '';
                                html += '<form id="frm-xml-' + rut + '" class="form-horizontal">';
                                html += '<div class="form-group"><label class="col-lg-2 control-label">Última Sincronización: </label>';
                                html += '<div class="col-lg-10"><input type="text" id="lastSync" name="lastSync" class="form-control" value="' + xml.registro._lastSync + '" disabled>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group"><label class="col-sm-2 control-label">Código: </label>';
                                html += '<div class="col-lg-10">';
                                html += '<div class="row" id="codigo_encriptado">';
                                html += '<div class="col-md-3"><input id="cod1" type="text" maxlength="4" class="form-control"></div>';
                                html += '<div class="col-md-3"><input id="cod2" type="text" maxlength="4" class="form-control"></div>';
                                html += '<div class="col-md-3"><input id="cod3" type="text" maxlength="4" class="form-control"></div>';
                                html += '<div class="col-md-3"><input id="cod4" type="text" maxlength="4" class="form-control"></div>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group">';
                                html += '<div class="col-lg-offset-2 col-lg-10">';
                                html += '<button onclick="generarCodigoNumerico();" class="btn btn-sm btn-warning" type="button">Generar código numérico</button>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group"><label class="col-lg-2 control-label">Código (Sólo numeros): </label>';
                                html += '<div class="col-lg-10"><input type="text" id="codigo" name="codigo" class="form-control" value="' + xml.registro.codigo + '" required>';
                                html += '<i>Los ultimos 5 numeros corresponden al numero de ficha. Cambielos sólo cuando sea extrictamente necesario.</i>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group"><label class="col-lg-2 control-label">RUT: </label>';
                                html += '<div class="col-lg-10"><input type="text" id="rut" name="rut" class="form-control" value="' + xml.registro.rut + '" required>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group"><label class="col-lg-2 control-label">Nombre: </label>';
                                html += '<div class="col-lg-10"><input type="text" id="nombre" name="nombre" class="form-control" value="' + xml.registro.nombre + '" disabled>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group"><label class="col-lg-2 control-label">Apellido: </label>';
                                html += '<div class="col-lg-10"><input type="text" id="apellido" name="apellido" class="form-control" value="' + xml.registro.apellido + '" disabled>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group"><label class="col-lg-2 control-label">Curso: </label>';
                                html += '<div class="col-lg-10"><input type="text" id="curso" name="curso" class="form-control" value="' + xml.registro.curso[0] + '" disabled>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group"><label class="col-lg-2 control-label">Email: </label>';
                                html += '<div class="col-lg-10"><input type="text" id="email" name="email" class="form-control" value="' + xml.registro.email + '" disabled>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group"><label class="col-lg-2 control-label">Teléfono: </label>';
                                html += '<div class="col-lg-10"><input type="text" id="telefono" name="telefono" class="form-control" value="' + xml.registro.telefono + '" disabled>';
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                                html += '<div class="form-group">';
                                html += '<div class="col-lg-offset-2 col-lg-10">';
                                html += '<button id="btn-guardar-xml" class="btn btn-sm btn-success" type="button">Guardar Cambios</button>';
                                html += '</div>';
                                html += '</div>';
                                html += '</form>';
                                $('#modalXML .modal-body').html('');
                                $('#modalXML .modal-body').html(html);

                                $("#btn-guardar-xml").click(function () {

                                    xml.registro.codigo = $("#frm-xml-" + rut + " #codigo").val();
                                    xml.registro.rut = $("#frm-xml-" + rut + " #rut").val();
                                    xml.registro.email = $("#frm-xml-" + rut + " #email").val();

                                    setXML(xml);

                                });

                                $('#modalXML').modal('show');
                                resolve()
                            }
                        })
                    }
                });

            } else {
                swal({
                    title: "Error",
                    text: "Alumno no tiene ningún informe registrado",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: true,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                    });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        async: false,
        cache: false
    });
}

function setXML(data) {
    // Create x2js instance with default config
    var x2js = new X2JS();
    var xmlAsStr = x2js.json2xml_str(data);
    xmlAsStr = xmlAsStr.replace(/'/g, '"');
    console.log(xmlAsStr);

    sendXMLtoApitablet(xmlAsStr);
}

function sendXMLtoApitablet(xml) {
    $.ajax({
        url: "../sendXMLtoApitablet",
        type: 'POST',
        dataType: "text",
        data: { xml: xml },
        beforeSend: function () {
            $("#cargando").html('<img src="../../../../assets/img/loading.gif">');
        },
        complete: function () {
            $("#cargando").html("");
        },
        success: function (resultado) {
            swal({
                title: "Atención",
                text: resultado,
                type: 'success',
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            }).then((result) => {
                if (result.value) {
                    location.reload();
                }
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
            swal({
                title: "Atención",
                text: errorThrown,
                type: 'error',
                showCancelButton: false,
                confirmButtonText: 'Aceptar'
            }).then((result) => {
                if (result.value) {
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        },
        async: false,
        cache: false
    });
}

function generarCodigoNumerico() {

    if (
        $('#cod1').val() != '' &&
        $('#cod2').val() != '' &&
        $('#cod3').val() != '' &&
        $('#cod4').val() != ''
    ) {
        var cod = $('#cod1').val() + "-" + $('#cod2').val() + "-" + $('#cod3').val() + "-" + $('#cod4').val();
        dCod = decodeWord(cod);
        console.log("cod: " + cod);

        codigoFinal = dCod.charAt(1) +
            dCod.charAt(4) +
            dCod.charAt(6) +
            dCod.charAt(8) +
            dCod.charAt(2) +
            dCod.charAt(3) +
            dCod.charAt(5) +
            dCod.charAt(9) +
            dCod.charAt(11) +
            dCod.charAt(0) +
            dCod.charAt(7) +
            dCod.charAt(10);
        codigoFinal = codigoFinal.slice(0, -3);
        $("#codigo").val(codigoFinal);
    }
}

function decodeWord(x) {
    var palabraArray = x.split("-");
    var palabrajuntada = "";
    for (let i = 0; i < palabraArray.length; i++) {
        palabrajuntada += palabraArray[i];
    }
    palabraseparada = palabrajuntada.split("");
    var salida = "";
    for (let i = 0; i < palabraseparada.length; i++) {
        for (let j = 0; j < base64_charactersArray.length; j++) {
            if (palabraseparada[i] == base64_charactersRandomArray[j]) {
                salida += base64_charactersArray[j];
            }
        }
    }
    salida = atob(salida);
    return salida;
}

function getCodigoTabletByidAlumno(id_alumno) {
    return '<img src="../../../../../assets/img/loading.gif" style="width:25px">';
}

function getUnidadesByIDFicha(id_ficha) {
    var resultado = new Array();
    $.ajax({
        url: "../getUnidadesByIDFicha",
        type: 'POST',
        dataType: "json",
        data: { id_ficha: id_ficha },
        success: function (result) {
            resultado = result;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se ha podido cargar los datos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });

    return resultado;
}

function getModulosByFichaYUnidad(id_ficha, id_unidad) {
    var resultado = new Array();
    $.ajax({
        url: "../getModulosByFichaYUnidad",
        type: 'POST',
        dataType: "json",
        data: { id_ficha: id_ficha, id_unidad: id_unidad },
        success: function (result) {
            resultado = result;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se ha podido cargar los datos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });

    return resultado;
}

function getReporteDetalladoByIDFicha() {
    $.ajax({
        url: "../getReporteDetalladoByIDFicha",
        type: 'POST',
        dataType: "json",
        data: { id_ficha: id_ficha },
        success: function (result) {
            console.log(result);
            var html = "";
            var rut = "";
            var nombre = "";
            var unidad = "";
            $("#tbl_reporte_detallado").html("");
            html += "<tr>";
            html += "<th>RUT</th>";
            html += "<th>Nombre</th>";
            html += "<th>Unidad</th>";
            html += "<th>Nota Unidad</th>";
            html += "<th>Módulo</th>";
            html += "<th>Avance Módulo</th>";
            html += "<th>Nota Módulo</th>";
            html += "</tr>";

            for (let i = 0; i < result.length; i++) {

                if ((i + 1) < result.length) {
                    if (result[i].modulo == result[i + 1].modulo && result[i].modulo.includes(" m")) {
                        if (result[i + 1].nota_modulo <= result[i + 1].nota_modulo) {
                            html += '<tr><td><b>';
                            html += result[i + 1].rut == rut ? "" : result[i + 1].rut;
                            rut = result[i + 1].rut;
                            html += '</b></td><td><b>';
                            html += result[i + 1].nombre == nombre ? "" : result[i + 1].nombre;
                            nombre = result[i + 1].nombre;
                            html += '</b></td><td>';
                            html += result[i + 1].unidad == unidad ? "" : result[i + 1].unidad;
                            unidad = result[i + 1].unidad;
                            html += '</td><td>';
                            html += '<b>' + result[i + 1].nota_unidad + '</b>';
                            html += '</td><td>';
                            html += result[i + 1].modulo;
                            html += '</td><td>';
                            html += result[i + 1].avance_modulo;
                            html += '</td><td>';
                            html += '<b>' + result[i + 1].nota_modulo + '</br>';
                            html += '</td>';
                            html += '</tr>';
                        } else {
                            html += '<tr><td><b>';
                            html += result[i].rut == rut ? "" : result[i].rut;
                            rut = result[i].rut;
                            html += '</b></td><td><b>';
                            html += result[i].nombre == nombre ? "" : result[i].nombre;
                            nombre = result[i].nombre;
                            html += '</b></td><td>';
                            html += result[i].unidad == unidad ? "" : result[i].unidad;
                            unidad = result[i].unidad;
                            html += '</td><td>';
                            html += '<b>' + result[i].nota_unidad + '</b>';
                            html += '</td><td>';
                            html += result[i].modulo;
                            html += '</td><td>';
                            html += result[i].avance_modulo;
                            html += '</td><td>';
                            html += '<b>' + result[i].nota_modulo + '</br>';
                            html += '</td>';
                            html += '</tr>';
                        }
                        i++;
                    } else {
                        html += '<tr><td><b>';
                        html += result[i].rut == rut ? "" : result[i].rut;
                        rut = result[i].rut;
                        html += '</b></td><td><b>';
                        html += result[i].nombre == nombre ? "" : result[i].nombre;
                        nombre = result[i].nombre;
                        html += '</b></td><td>';
                        html += result[i].unidad == unidad ? "" : result[i].unidad;
                        unidad = result[i].unidad;
                        html += '</td><td>';
                        html += '<b>' + result[i].nota_unidad + '</b>';
                        html += '</td><td>';
                        html += result[i].modulo;
                        html += '</td><td>';
                        html += result[i].avance_modulo;
                        html += '</td><td>';
                        html += '<b>' + result[i].nota_modulo + '</br>';
                        html += '</td>';
                        html += '</tr>';
                    }
                } else {
                    html += '<tr><td><b>';
                    html += result[result.length - 1].rut == rut ? "" : result[result.length - 1].rut;
                    rut = result[result.length - 1].rut;
                    html += '</b></td><td><b>';
                    html += result[result.length - 1].nombre == nombre ? "" : result[result.length - 1].nombre;
                    nombre = result[result.length - 1].nombre;
                    html += '</b></td><td>';
                    html += result[result.length - 1].unidad == unidad ? "" : result[result.length - 1].unidad;
                    unidad = result[result.length - 1].unidad;
                    html += '</td><td>';
                    html += '<b>' + result[result.length - 1].nota_unidad + '</b>';
                    html += '</td><td>';
                    html += result[result.length - 1].modulo;
                    html += '</td><td>';
                    html += result[result.length - 1].avance_modulo;
                    html += '</td><td>';
                    html += '<b>' + result[result.length - 1].nota_modulo + '</br>';
                    html += '</td>';
                    html += '</tr>';
                }
            }
            $("#tbl_reporte_detallado").html(html);
            generatePDFReporteDetallado('tbl_reporte_detallado');

        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se ha podido cargar los datos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
}

function generatePDF(id_table) {

    var doc = new jsPDF();
    doc.setFontSize(8);
    doc.text("EDUTECNO CAPACITACIÓN", 14, 10);
    doc.setFontSize(16);
    doc.text("Reporte Progreso Global Curso Tablet Distacia", 14, 15);
    doc.setFontSize(10);
    doc.text("Fecha de emisión del informe: " + d.getDate().toString() + '-' + (d.getMonth() + 1).toString() + '-' + d.getFullYear().toString(), 14, 20);
    doc.text("Ficha N°: " + num_ficha, 14, 25);
    doc.text("Curso: " + nombre_curso, 14, 30);

    var elem = document.getElementById(id_table);

    var res = doc.autoTableHtmlToJson(elem);

    doc.autoTable(res.columns, res.data, {
        startY: 35,
        headerStyles: {
            overflow: 'linebreak',
            fontSize: 9
        },
        columnStyles: {
            0: { columnWidth: 100, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            1: { columnWidth: 80, overflow: 'linebreak', halign: 'left', fontSize: 8 }
        }
    });

    doc.save('informe_' + d.getDate().toString() + '_' + (d.getMonth() + 1).toString() + '_' + d.getFullYear().toString() + '_' + d.getTime().toString() + '.pdf');
}

function generatePDFReporteDetallado(id_table) {

    var doc = new jsPDF();
    doc.setFontSize(8);
    doc.text("EDUTECNO CAPACITACIÓN", 14, 10);
    doc.setFontSize(16);
    doc.text("Reporte Detallado Curso Tablet Distacia", 14, 15);
    doc.setFontSize(10);
    doc.text("Fecha de emisión del informe: " + d.getDate().toString() + '-' + (d.getMonth() + 1).toString() + '-' + d.getFullYear().toString(), 14, 20);
    doc.text("Ficha N°: " + num_ficha, 14, 25);
    doc.text("Curso: " + nombre_curso, 14, 30);

    var elem = document.getElementById(id_table);

    var res = doc.autoTableHtmlToJson(elem);

    doc.autoTable(res.columns, res.data, {
        startY: 35,
        headerStyles: {
            overflow: 'linebreak',
            fontSize: 8
        },
        columnStyles: {
            0: { columnWidth: 18, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            1: { columnWidth: 25, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            2: { columnWidth: 45, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            3: { columnWidth: 23, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            4: { columnWidth: 25, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            5: { columnWidth: 25, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            6: { columnWidth: 23, overflow: 'linebreak', halign: 'left', fontSize: 8 }
        }
    });

    doc.save('informe_' + d.getDate().toString() + '_' + (d.getMonth() + 1).toString() + '_' + d.getFullYear().toString() + '_' + d.getTime().toString() + '.pdf');
}