var d = new Date();
var id_ficha = $(location).attr('href').split("/").reverse().splice(0, 1).toString();
var num_ficha, nombre_curso;
$(document).ready(function () {
    ListarCodigos();
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
});

function ListarCodigos() {
    $.ajax({
        url: "../getCodigosbyIDFicha",
        type: 'POST',
        dataType: "json",
        data: {id_ficha: id_ficha},
        beforeSend: function () {
            $("#cargando").html('<img src="../../../../assets/img/loading.gif">');
        },
        success: function (result) {

            cargaTablaCodigos(result);

            if(result.length > 0){
                $('.ibox-title h5').append(result[0].num_ficha);
                $("#contenido").css("display", "block");
            }else{
                swal({
                    title: "Error",
                    text: "No existen códigos en esta ficha",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                        window.history.back();
                    });
            }
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar los codigos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        complete: function () {
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });
}

function cargaTablaCodigos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_codigos')) {
        $('#tbl_codigos').DataTable().destroy();
    }

    $('#tbl_codigos').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "codigo" },
            { "mDataProp": "nombre_alumno" },
            { "mDataProp": "rut" }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Categoria'
            }, {
                extend: 'pdf',
                title: 'Categoria'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        order: [[1, "asc"]]
    });
}

function getCodigoTabletByidAlumno(id_alumno) {
    return '<img src="../../../../../assets/img/loading.gif" style="width:25px">';
}

function getUnidadesByIDFicha(id_ficha){
    var resultado = new Array();
        $.ajax({
            url: "../getUnidadesByIDFicha",
            type: 'POST',
            dataType: "json",
            data: {id_ficha: id_ficha},
            success: function (result) {
                resultado = result;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                swal({
                    title: "Error",
                    text: "No se ha podido cargar los datos: " + errorThrown,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Recargar",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                        location.reload();
                    });
            },
            async: false,
            cache: false
        });

    return resultado;
}

function getModulosByFichaYUnidad(id_ficha, id_unidad) {
    var resultado = new Array();
    $.ajax({
        url: "../getModulosByFichaYUnidad",
        type: 'POST',
        dataType: "json",
        data: { id_ficha: id_ficha, id_unidad: id_unidad},
        success: function (result) {
            resultado = result;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se ha podido cargar los datos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });

    return resultado;
}

function getReporteDetalladoByIDFicha() {
    $.ajax({
        url: "../getReporteDetalladoByIDFicha",
        type: 'POST',
        dataType: "json",
        data: { id_ficha: id_ficha},
        success: function (result) {
            console.log(result);
            var html = "";
            var rut="";
            var nombre="";
            var unidad ="";
            $("#tbl_reporte_detallado").html("");
            html += "<tr>";
            html += "<th>RUT</th>";
            html += "<th>Nombre</th>";
            html += "<th>Unidad</th>";
            html += "<th>Nota Unidad</th>";
            html += "<th>Módulo</th>";
            html += "<th>Avance Módulo</th>";
            html += "<th>Nota Módulo</th>";
            html+="</tr>";
            $.each(result, function (i, item) {
                html += '<tr><td><b>';
                html += item.rut == rut ? "" : item.rut;
                rut = item.rut;
                html += '</b></td><td><b>';
                html += item.nombre == nombre ? "" : item.nombre;
                nombre = item.nombre;
                html += '</b></td><td>';
                html += item.unidad == unidad ? "": item.unidad;
                unidad = item.unidad;
                html += '</td><td>';
                html += '<b>'+item.nota_unidad+'</b>';
                html += '</td><td>';
                html += item.modulo;
                html += '</td><td>';
                html += item.avance_modulo;
                html += '</td><td>';
                html += '<b>'+item.nota_modulo+'</br>';
                html += '</td>';
                html += '</tr>';
            });
            $("#tbl_reporte_detallado").html(html);
            generatePDFReporteDetallado('tbl_reporte_detallado');

        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se ha podido cargar los datos: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
}

function generatePDF(id_table) {

    var doc = new jsPDF();
    doc.setFontSize(8);
    doc.text("EDUTECNO CAPACITACIÓN", 14, 10);
    doc.setFontSize(16);
    doc.text("Reporte Progreso Global Curso Tablet Distacia", 14, 15);
    doc.setFontSize(10);
    doc.text("Fecha de emisión del informe: " + d.getDate().toString() + '-' + (d.getMonth() + 1).toString() + '-' + d.getFullYear().toString(), 14, 20);
    doc.text("Ficha N°: " + num_ficha, 14, 25);
    doc.text("Curso: " + nombre_curso, 14, 30);

    var elem = document.getElementById(id_table);

    var res = doc.autoTableHtmlToJson(elem);

    doc.autoTable(res.columns, res.data, {
        startY: 35,
        headerStyles: {
            overflow: 'linebreak',
            fontSize: 9
        },
        columnStyles: {
            0: { columnWidth: 100, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            1: { columnWidth: 80, overflow: 'linebreak', halign: 'left', fontSize: 8 }
        }
    });

    doc.save('informe_' + d.getDate().toString() + '_' + (d.getMonth() + 1).toString() + '_' + d.getFullYear().toString() + '_' + d.getTime().toString() + '.pdf');
}

function generatePDFReporteDetallado(id_table) {

    var doc = new jsPDF();
    doc.setFontSize(8);
    doc.text("EDUTECNO CAPACITACIÓN", 14, 10);
    doc.setFontSize(16);
    doc.text("Reporte Detallado Curso Tablet Distacia", 14, 15);
    doc.setFontSize(10);
    doc.text("Fecha de emisión del informe: " + d.getDate().toString() + '-' + (d.getMonth() + 1).toString() + '-' + d.getFullYear().toString(), 14, 20);
    doc.text("Ficha N°: " + num_ficha, 14, 25);
    doc.text("Curso: " + nombre_curso, 14, 30);

    var elem = document.getElementById(id_table);

    var res = doc.autoTableHtmlToJson(elem);

    doc.autoTable(res.columns, res.data, {
        startY: 35,
        headerStyles: {
            overflow: 'linebreak',
            fontSize: 8
        },
        columnStyles: {
            0: { columnWidth: 18, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            1: { columnWidth: 25, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            2: { columnWidth: 45, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            3: { columnWidth: 23, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            4: { columnWidth: 25, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            5: { columnWidth: 25, overflow: 'linebreak', halign: 'left', fontSize: 8 },
            6: { columnWidth: 23, overflow: 'linebreak', halign: 'left', fontSize: 8 }
        }
    });

    doc.save('informe_' + d.getDate().toString() + '_' + (d.getMonth() + 1).toString() + '_' + d.getFullYear().toString() + '_' + d.getTime().toString() + '.pdf');
}