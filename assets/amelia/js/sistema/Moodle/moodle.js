$(document).ready(function () {
    $("#detalle_ficha").hide();
    $('#chkTodos').on('change', function () {
        var chks = $("input[name=chkidficha]");
        for (var i = 0; i < chks.length; i++) {
            chks[i].checked = $('#chkTodos').is(':checked');
        }
    });
    $("#btnAno").on("click", function () {
            $.ajax({
                async: true,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {txtano: $("#txtano").val()},
                beforeSend: function(){
                    $("#tblFichas tbody").html("");
                    $("#tblFichas tbody").html('<tr><td colspan="3" style="text-align: center;"><img src="../../assets/img/loading.gif" /></td></tr>');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    if (result.fichas.length > 0) {
                        $("#tblFichas tbody").html("");
                        $.each(result.fichas, function (i, v) {
                            $("#tblFichas tbody").append("<tr bgcolor='" + v.color + "'>"
                                    + "<td>"
                                    + "<input type=\"checkbox\" name=\"chkidficha\" value=\"" + v.id_ficha + "\" />"
                                    + "</td>"
                                    + "<td>"
                                    + v.nombre_curso + " (" + v.num_ficha + ")"
                                    + "</td>"
                                    + "<td>"
                                    + v.rectificada
                                    + "</td>"
                                    + "<td>"
                                    + "<button type=\"button\" onclick=\"seleccionarFicha(" + v.id_ficha + ")\" class=\"btn btn-info btn-xs\"><i class=\"fa fa-eye\"></i></button>"
                                    + "</td>"
                                    + "</tr>");
                        });
                    } else {
                        $("#tblFichas tbody").html("");
                        $("#tblFichas tbody").append("<tr>"
                                    + '<td colspan="3">'
                                    + "No se encontraron fichas en el año ingresado"
                                    + "</td>"
                                    + "</tr>");
                    }
                },
                complete: function () {
                    $("tblFichas tbody").html("");
                },
                url: "Moodle/get_fichas_aprobadas/"
            });
    });
    
    $('#tblFichas').DataTable({
//language: { "url": '../Scripts/plugins/dataTables/Spanish.json'},
        paging: false,
        ordering: false,
        info: false,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: []

    });
    $('#tblErrores').DataTable({
//language: { "url": '../Scripts/plugins/dataTables/Spanish.json'},
        paging: false,
        ordering: false,
        info: false,
        searching: false,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: []


    });
    $('#btnMdlEnviarMoodle').on('click', function () {
        swal({
            title: 'Confirmación',
            text: "¿Está seguro que desea enviar los cursos seleccionados a Moodle?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve) {
                    swal({
                        title: 'Espere un momento...',
                        text: 'Se estan enviando los cursos Moodle',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        onOpen: () => {
                            swal.showLoading()
                        }
                    });
                    setTimeout(function () {
                        resolve()
                    }, 2000)
                })
            }
        }).then((confirm) => {
            if (confirm) {

                var listaFichas = [];
                var chks = $("input[name=chkidficha]");
                for (var i = 0; i < chks.length; i++) {
                    if (chks[i].checked) {
                        listaFichas.push(chks[i].value);
                    }
                }
                if (listaFichas.length > 0) {
                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {
                            fichas: listaFichas
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal.hideLoading()
                            console.log(textStatus + errorThrown + jqXHR.responseText);
                            swal({
                                title: "Error",
                                text: errorThrown,
                                type: "error"

                            }).then((confirm) => {
                                location.reload();
                            });
                        },
                        success: function (result) {
                            swal.hideLoading()
                            console.log(result.Enrols);
                            swal({
                                title: "Aviso",
                                text: result.Msg,
                                type: "info"

                            }).then((confirm) => {
                                location.reload();
                            });
                            console.log(result.Cod, result.Msg);
                        },
                        url: "Moodle/set_cursos_moodle"
                    });
                } else {
                    swal("Error", "No ha seleccionado ningun curso", "error");
                }
                //
            }
        });
        /*
         swal({
         title: "Confirmación",
         text: "¿Está seguro que desea enviar los cursos seleccionados a Moodle?",
         type: "warning",
         showCancelButton: true,
         confirmButtonColor: '#DD6B55',
         confirmButtonText: 'Aceptar',
         cancelButtonText: "Cancelar",
         closeOnConfirm: false,
         closeOnCancel: true
         },
         function (isConfirm) {
         if (isConfirm) {
         var listaFichas = [];
         var chks = $("input[name=chkidficha]");
         for (var i = 0; i < chks.length; i++) {
         if (chks[i].checked) {
         listaFichas.push(chks[i].value);
         }
         }
         if (listaFichas.length > 0) {
         var swalLoad = swal('Please wait');
         swalLoad.showLoading();
         $.ajax({
         async: false,
         cache: false,
         dataType: "json",
         type: 'POST',
         data: {
         fichas: listaFichas
         },
         error: function (jqXHR, textStatus, errorThrown) {
         swalLoad.close();
         console.log(textStatus + errorThrown + jqXHR.responseText);
         //alert(textStatus + errorThrown + jqXHR.responseText);
         swal({
         title: "Error",
         text: errorThrown,
         type: "error"
         
         }, function () {
         location.reload();
         });
         },
         success: function (result) {
         swalLoad.close();
         console.log(result.Enrols);
         swal({
         title: "Aviso",
         text: result.Msg,
         type: "info"
         
         }, function () {
         location.reload();
         });
         console.log(result.Cod, result.Msg);
         },
         url: "Moodle/set_cursos_moodle"
         });
         } else {
         swal("Error", "No ha seleccionado ningun curso", "error");
         }
         }
         });
         */
    });
});
function seleccionarFicha(id_ficha) {
    $("#detalle_ficha").show();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
            alert(textStatus + errorThrown + jqXHR.responseText);
        },
        success: function (result) {
            console.log(result);
            $("#lblFicha").text(result.alumnos[0].num_ficha);
            $("#lblCurso").text(result.alumnos[0].nombre_curso);
            $("#lblFechaAprobacion").text(result.alumnos[0].fecha_apb_comercial);
            if (result.errores.length > 0) {
                $("#info_excepcion").show();
                $("#icon_sombrero").hide();
                $("#tblErrores tbody").html("");
                $.each(result.errores, function (i, v) {
                    $("#tblErrores tbody").append("<tr><td>" + v.fecha_excepcion + "</td><td>" + v.nombre_metodo + "</td><td>" + v.msg_excepcion + "</td></tr>");
                });
            } else {
                $("#info_excepcion").hide();
                $("#icon_sombrero").show();
            }
            $("#tblAlumnos tbody").html("");
            $.each(result.alumnos, function (i, v) {
                $("#tblAlumnos tbody").append("<tr><td>" + v.id_alumno + "</td><td>" + v.rut_alumno + "</td><td>" + v.alumno + "</td></tr>");
            });
        },
        url: "Moodle/get_ficha/" + id_ficha
    });
}