$(document).ready(function () {

    llamaDatosLog();


    $("#btn_buscar_log").click(function () {
        llamaDatosLog();
    });

    /*$('.input-daterange').datepicker({
     format: "yyyy-mm-dd",
     weekStart: 1,
     startView: 1,
     maxViewMode: 2,
     todayBtn: true,
     clearBtn: true,
     language: "es"
     });
     */
    $('#data_5 .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $("#btn_limpiar_form").click(function () {
        location.reload();
    });


    llamaDatosControles();
    llamaDatosControles2();
});


function llamaDatosLog() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaLog(result);
        },
        url: "Listar/buscarLog"
    });
}


function cargaTablaLog(data) {

    if ($.fn.dataTable.isDataTable('#tbl_log')) {
        $('#tbl_log').DataTable().destroy();
    }

    $('#tbl_log').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_log"},
            {"mDataProp": "fecha_corta"},
            {"mDataProp": "hora_minuto"},
            {"mDataProp": "actividad"},
            {"mDataProp": "observacion"},
            {"mDataProp": "usuario"},
            {"mDataProp": "perfil"}
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
         , "order": [[0, "desc"]]
    });
}


function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result);
        },
        url: "Listar/getUsuarioCBX"
    });


}

function cargaCombo(midata) {
    $("#usuario").select2({
        placeholder: "Seleccione usuario",
        allowClear: true,
        data: midata
    });

    $("#usuario option[value='0']").remove();

}

function llamaDatosControles2() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo2(result);
        },
        url: "Listar/getPerfilCBX"
    });


}

function cargaCombo2(midata) {
    $("#perfil").select2({
        placeholder: "Seleccione perfil",
        allowClear: true,
        data: midata
    });

    $("#perfil option[value='0']").remove();

}
