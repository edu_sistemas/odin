$(document).ready(function () {
    recargaLaPagina();
    $("#btn_forgort_mi_passs").click(function () {
        if (validarRequeridoForm($("#forgot_pass"))) {
            var username = $("#user_name").val();
            if (username.includes("-")) {
                if (username.includes(".")) {
                    swal({
                        title: "error",
                        text: "rut sólo debe contener numeros, guión y digito verificador EJ: 9876543-k",
                        type: "error"
                    });
                } else {
                    recuperarPassword();
                }
            } else {
                swal({
                    title: "error",
                    text: "rut sólo debe contener numeros, guión y digito verificador EJ: 9876543-k",
                    type: "error"
                });
            }
        }
    });
});


function recuperarPassword() {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#forgot_pass').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (result) {


            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            });



        },
        url: "RecuperarPassword/forgotPassword"
    });
}