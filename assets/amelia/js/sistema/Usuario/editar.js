$(document).ready(function () {
   recargaLaPagina();
    $("#btn_editar_usuario").click(function () {

        if (validarRequeridoForm($("#btn_editar_usuario"))) {
            EditarUsuario();
        }

    });
    $("#btn_reset_password").click(function () {
        upPassword();
    });

    llamaDatosControles();

    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });
});


function EditarUsuario() {

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_usuario_editar').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta('Usuario', textStatus, 'error');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }

        },
        url: "../EditarUsuario"
    });
}

function upPassword() {
    /* funcion para reestablecer la contraseña del usuario */
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
           id: $('#id').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            console.log(jqXHR + ' ' + textStatus + ' ' + errorThrown);
            alerta('Usuario', textStatus, 'error');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }
        },
        url: "../upPassword"
    });
}


function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "perfil", "id_perfil_seleccionado");
        },
        url: "../getPerfiles"
    });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "departamento", "id_departamento_seleccionado");
        },
        url: "../getDepartamentos"
    });
}

function cargaCombo(midata, idcbx, id_seleccionado) {

    $("#"+idcbx).select2({
        allowClear: true,
        data: midata
    });

    //$("#perfil option[value='0']").remove();
    $("#" + idcbx).val($("#" + id_seleccionado).val()).trigger("change");
}
