$(document).ready(function () {
    recargaLaPagina();
    $('#btn_nuevo').click(function () {
        location.href = 'Agregar';
    });
    listarUsuarios();
});
function cargaTablaUsuario(data) {

    if ($.fn.dataTable.isDataTable('#tbl_usuarios')) {
        $('#tbl_usuarios').DataTable().destroy();
    }

    $('#tbl_usuarios').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "usuario"},
            {"mDataProp": "nombre"},
            {"mDataProp": "apellidos"},
            {"mDataProp": "telefono"},
            {"mDataProp": "perfil"},
            {"mDataProp": "tipo_usuario"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<i title="Inactivo" class="fa fa-circle text-danger"></i>';

                    if (o.estado == 1) {
                        html = '  <i title="Activo"  class="fa fa-circle text-navy"></i>';
                    }

                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var x = "Otorgar_Rol/index/" + o.id + "";
                    var y = "Editar/index/" + o.id + "";
                    var html = "";
                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-default dropdown-toggle ">Accion<span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu pull-right">';
                    /*  html += '  <li><a href="' + x + '"  >Asignar Perfil</a></li>';
                     html += '  <li class="divider"></li>';
                     */
                    html += '  <li><a href="' + y + '"  >Editar</a></li>';
                    if (o.estado == 1) {
                        html += "<li><a    onclick='EditarUsuarioEstado(" + o.id + ",0);'>Desactivar</a></li>";
                    } else {
                        html += "<li><a    onclick='EditarUsuarioEstado(" + o.id + ",1);'>Activar</a></li> ";
                    }
                    html += ' </ul>';
                    html += ' </div>';
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

function listarUsuarios() {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaUsuario(result);
        },
        url: "Listar/listar"
    });
}

function EditarUsuarioEstado(id_usuario, tipo_estado) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id: id_usuario, estado: tipo_estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Usuario", errorThrown, "error");
        },
        success: function (result) {

            listarUsuarios();
        },
        url: "Listar/CambiarEstadoUsuario"
    });

}
