$(document).ready(function () {
    recargaLaPagina();
    $("#btn_otorgar_rol").click(function () {
        if (validarRequeridoForm($("#frm_otorgar_rol"))) {
            OtorgarRolUser();
        }
    });

    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });

});


function OtorgarRolUser() {

    var resultado = "No se ha procesado la peticion";

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_otorgar_rol').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            //     // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {

            resultado = result[0].respuesta;
            //  console.log(resultado);
        },
        url: "../OtorgarRol" //Hace referencia a la funcion del controlador que realiza la aacion de eitar.
    });


    if (resultado != "Registro actualizado") {
        alerta("Perfil", resultado, "error");
    } else {
        alerta("Perfil", resultado, "success");
        // tiempo de espera para que alcanse a leer el mensaje
        setTimeout(function () {
            $("#btn_back").trigger("click");
        }, 5000);
    }

}
