$(document).ready(function () {
    var d = new Date();
    var mes = d.getMonth() + 1;
    var year = d.getFullYear();

    $("#btn_generar_excel").click(function () {
        var id_ejecutivo = $("#id_ejecutivo").val();
        var id_empresa = $("#id_empresa").val();
        var fecha = $("#mes").val();
        
        if(fecha ==""){
            fecha = "0";
        }
        if(id_ejecutivo ==""){
            id_ejecutivo = "0";
        }
        if (id_empresa == "") {
            id_empresa = "0";
        }
       
        var url_final = "";
        url_final = "CuentaSupervisor/generarExcelGestiones/" + id_ejecutivo +"/" + id_empresa + "/" + fecha ;

        window.open(url_final, '_blank');
    }); 

    for (var x = 1; x <= mes; x++) {
        var texto = '';
        switch (x) {
            case 1:
                texto = 'Enero';
                x = '01';
                break;
            case 2:
                texto = 'Febrero';
                x = '02';
                break;
            case 3:
                texto = 'Marzo';
                x = '03';
                break;
            case 4:
                texto = 'Abril';
                x = '04';
                break;
            case 5:
                texto = 'Mayo';
                x = '05';
                break;
            case 6:
                texto = 'Junio';
                x = '06';
                break;
            case 7:
                texto = 'Julio';
                x = '07';
                break;
            case 8:
                texto = 'Agosto';
                x = '08';
                break;
            case 9:
                texto = 'Septiembre';
                x = '09';
                break;
            case 10:
                texto = 'Octubre';
                break;
            case 11:
                texto = 'Noviembre';
                break;
            case 12:
                texto = 'Diciembre';
                break;
        }
        $('#mes').append('<option value="' + year + '-' + x + '" >' + texto + ' ' + (year) + '</option>');
    }

    for (var x = (mes + 1); x <= 12; x++) {
        var texto = '';
        switch (x) {
            case 1:
                texto = 'Enero';
                x = '01';
                break;
            case 2:
                texto = 'Febrero';
                x = '02';
                break;
            case 3:
                texto = 'Marzo';
                x = '03';
                break;
            case 4:
                texto = 'Abril';
                x = '04';
                break;
            case 5:
                texto = 'Mayo';
                x = '05';
                break;
            case 6:
                texto = 'Junio';
                x = '06';
                break;
            case 7:
                texto = 'Julio';
                x = '07';
                break;
            case 8:
                texto = 'Agosto';
                x = '08';
                break;
            case 9:
                texto = 'Septiembre';
                x = '09';
                break;
            case 10:
                texto = 'Octubre';
                break;
            case 11:
                texto = 'Noviembre';
                break;
            case 12:
                texto = 'Diciembre';
                break;
        }
        $('#mes').append('<option value="' + (year - 1) + '-' + x + '">' + texto + ' ' + (year - 1) + '</option>');
    }

    $('.footable').footable();
    $('#tabla_cuenta').footable();
    $('#tbl_gestion_empresa').footable();
    cargaCBXall();
    listar();
    listarGestionClientes();
    llamaDatosGestiones();
    listDominios();

    $("#btnListarClientes").on("click",function(){
        showMdlClientesEjecutivos();
    });

    $("#btn_buscar").on("click", function(){
        listar();
    });

});

function cargaCBXall() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: {id_holding: $('#holding').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "id_empresa");
        },
        url: "CuentaSupervisor/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "id_ejecutivo");
        },
        url: "CuentaSupervisor/getUsuarioTipoEcutivoComercial"
    });
}

function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
}

function llamaDatosGestiones() {
     $.ajax({
         async: false,
         cache: false,
         dataType: "json",
         type: 'POST',
         //data: $('#frm').serialize(),
         error: function (jqXHR, textStatus, errorThrown) {
             // code en caso de error de la ejecucion del ajax
             //  console.log(textStatus + errorThrown);
         },
         success: function (result) {
             cargaTablagestiones(result);
         },
         url: "CuentaSupervisor/getHistorialGestionDownload"
     });
}



function cargaTablaClientes(data){
    if ($.fn.dataTable.isDataTable('#tbl_cuenta')) {
         $('#tbl_cuenta').DataTable().destroy();
    }
     
    $('#tbl_cuenta').DataTable({
         "aaData":data,
         "aoColumns":[
            {"mDataProp": "fecha_ultima_gestion"},
            {"mDataProp": "nombre_cliente"},
            {"mDataProp": "holding"},
            {
                "mDataProp": null, "bSortable": true, "mRender": function (o) {
                     var html = "";
                     var title = "";
                     var color_icon_titulo="";
                                        
                        if (o.q_dias <= 10) {
                             title = "Con gestión";
                             color_icon_titulo = "#1ab394";
                        }
                        else if (o.q_dias > 15) {
                             title = "Sin gestión hace más de 15 días";
                             color_icon_titulo = "#656767";
                        } else if (o.q_dias > 10 && o.q_dias < 15) {
                             title = "Sin gestión entre los últimos 10 y 15 días";
                             color_icon_titulo = "#ffb000";
                        } else {
                             title = "Sin gestión en los últimos 10 días";
                             color_icon_titulo = "#656767";
                        }
                    icono_base = "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='" + title + "' class='fa fa-circle' style='color: " + color_icon_titulo + "; font-size: 15px;cursor:help;'></i>";
                    html += icono_base;
                    return html;
                }
            },
            {
                "mDataProp": null, "bSortable": true, "mRender": function (o) {
                    var html = "";
                    var texto_titulo = "";
                    var icono_contacto ="";
                    if(o.tiene_contacto ==1){
                        icono_contacto = "<i class='fa fa-user' data-toggle='tooltip' data-placement='auto right' title='Cliente con contacto.' style='cursor: help; color:#337ab7; font-size:20px;'></i>";
                     
                    }
                    switch (o.estado_empresa) {
                        case '1':
                            texto_titulo = "Potencial";
                            break;
                        case '2':
                            texto_titulo = "Activo (6+)";
                            break;
                        case '3':
                            texto_titulo = "Activo (6-)";
                            break;
                    }
                    html += texto_titulo +'&nbsp' + icono_contacto;
                    return html;
                }
            },
            {"mDataProp": "rut_empresa"},
            {"mDataProp": "ejecutivo"},
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var id = o.ID;
                    var opciones_accion = "";
                    var icono_ver ="";

                    opciones_accion = "<a href='editar_cuenta/index/" + id + "'><i class='fa fa-eye'></i></a>";
                    icono_ver += "<i class='fa fa-eye'></i>";
                    html+=opciones_accion;
                    return html;
                }
            },
        ],
        pageLength: 15,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
               {
                    extend: 'copy'
                }, {
                extend: 'csv'
                }, {
                extend: 'excel',
                title: 'Empresa'
                }, {
                extend: 'pdf',
                title: 'Empresa'
                }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        "columnDefs": [
            { "width": "10%", "targets": 0 }
          ],
          order: false
        
    })
}

function listar() {
    $.ajax({
        async: false,
        cache: true,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        beforeSend: function () {
            $("#loading-container").html('<img src="../../assets/img/loading.gif">');
        },
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaClientes(result);
        },
        url: "CuentaSupervisor/getCliente"
    });
}

function cargaTablGestionClientes(data) {
    if ($.fn.dataTable.isDataTable('#tbl_gestion_empresa')) {
        $('#tbl_gestion_empresa').DataTable().destroy();
    }

    $('#tbl_gestion_empresa').DataTable({
        "aaData": data,
        "aoColumns": [
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var texto_gestion = "";
                    var tipogestion_fa = "";
                    var color_icon_gestion_fa = "";

                    if(o.tipo_gestion==1){
                        texto_gestion = "Gestión Manual";
                        tipogestion_fa = "fa fa-tasks";
                        color_icon_gestion_fa = "#656767";
                    }else if(o.compromiso_accion_color == 1){
                        texto_gestion = "Gestión Automática sin Compromiso";
                        tipogestion_fa = "fa fa-cog";
                        color_icon_gestion_fa = "#FFB000";
                    }else{
                        texto_gestion = "Gestión Automática";
                        tipogestion_fa = "fa fa-cog";
                        color_icon_gestion_fa = "#656767";
                    }

                    html += "<i data-toggle='tooltip' data-placement='auto right' data-html='true' title='" + texto_gestion + "' class='" + tipogestion_fa + "' style='color: " + color_icon_gestion_fa + "; font-size: 15px; cursor:help;'></i>"; 
                    
                    return html;
                }
            },
            { "mDataProp": "razon_social_empresa" },
            { "mDataProp": "holding" },
            { "mDataProp": "id_ejecutivo" },
            { "mDataProp": "DESC_ACCION" },
            { "mDataProp": "asunto_accion" },
            { "mDataProp": "fecha_evento" },
            {"mDataProp": "comentario_accion"},
            {"mDataProp": "fecha_compromiso_accion"},
            { "mDataProp": "DESC_COMPROMISO" },
            { "mDataProp": "fase" },
            { "mDataProp": "tipo_accion" },
            { "mDataProp": "id_producto" }
            
           
       ],
        pageLength: 15,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Gestiones'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [0, 'desc']

    })
}

function listarGestionClientes() {
    $.ajax({
        async: false,
        cache: true,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablGestionClientes(result);
        },
        url: "CuentaSupervisor/getHistorialGestion"
    });
}

function cargaTablagestiones(data) {

     if ($.fn.dataTable.isDataTable('#tbl_gestion')) {
         $('#tbl_gestion').DataTable().destroy();
     }

     $('#tbl_gestion').DataTable({
         "aaData": data,
         "bFilter": false,
         "bPaginate": false,
         "bInfo": false,
         "aoColumns": [
             {"mDataProp": "id_ejecutivo"},
             {"mDataProp": "razon_social_empresa"},
             {"mDataProp": "holding"},
             {"mDataProp": "DESC_ACCION"},
             { "mDataProp": "asunto_accion" },
             { "mDataProp": "fecha_evento" },
             {"mDataProp": "comentario_accion"},
             {"mDataProp": "fecha_compromiso_accion"},
             { "mDataProp": "DESC_COMPROMISO" },
             { "mDataProp": "fase" },
             { "mDataProp": "tipo_accion" },
             { "mDataProp": "id_producto" }
            
                      ],
         pageLength: 25,
         responsive: false,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [
             {
                 extend: 'copy'
             }, 
            
             {
                 extend: 'excel',
                 title: 'Gestiones'
             }, 
             {
                 extend: 'print',
                 customize: function (win) {
                     $(win.document.body).addClass('white-bg');
                     $(win.document.body).css('font-size', '10px');
                     $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                 }
             }
         ]
     });

     $('[data-toggle="tooltip"]').tooltip();
 }

function listDominios() {
     $.ajax({
         async: false,
         cache: false,
         dataType: "json",
         type: 'POST',
         //data: $('#frm').serialize(),
         error: function (jqXHR, textStatus, errorThrown) {
             // code en caso de error de la ejecucion del ajax
             //  console.log(textStatus + errorThrown);
         },
         success: function (result) {
             cargaTablaDominios(result);
         },
         url: "CuentaSupervisor/getHistorialDominiosDownload"
     });
 }

 function cargaTablaDominios(data) {

     if ($.fn.dataTable.isDataTable('#tbl_dominio_sin_asignar')) {
         $('#tbl_dominio_sin_asignar').DataTable().destroy();
     }

     $('#tbl_dominio_sin_asignar').DataTable({
         "aaData": data,
         "bFilter": false,
         "bPaginate": false,
         "bInfo": false,
         "aoColumns": [
             {"mDataProp": "dominio"}
                      ],
         pageLength: 25,
         responsive: false,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [
             {
                 extend: 'copy'
             }, 
             {
                 extend: 'excel',
                 title: 'Dominios'
             }, 
             {
                 extend: 'print',
                 customize: function (win) {
                     $(win.document.body).addClass('white-bg');
                     $(win.document.body).css('font-size', '10px');
                     $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                 }
             }
         ]
     });

     $('[data-toggle="tooltip"]').tooltip();
 }

function showMdlClientesEjecutivos(){
    $("#mdlListaClientes").modal('show');
    $.ajax({
         async: false,
         cache: false,
         dataType: "json",
         type: 'POST',
         //data: $('#frm').serialize(),
         error: function (jqXHR, textStatus, errorThrown) {
             // code en caso de error de la ejecucion del ajax
             //  console.log(textStatus + errorThrown);
         },
         success: function (result) {
             if ($.fn.dataTable.isDataTable('#tbl_clientes_ejecutivos')) {
                $('#tbl_clientes_ejecutivos').DataTable().destroy();
             }

             $('#tbl_clientes_ejecutivos').DataTable({
                paging: true,
                ordering: true,
                "searching": true,
                info: false,
                 "aaData": result,
                 "bFilter": false,
                 "bPaginate": false,
                 "bInfo": false,
                 "aoColumns": [
                     {"mDataProp": "razon_social_empresa"},
                     {"mDataProp": "ejecutivo"},
                     {"mDataProp": "gestion"},
                     {"mDataProp": "rut_empresa"}
                              ],
                 pageLength: 25,
                 responsive: false,
                 dom: '<"html5buttons"B>lTfgitp',
                 buttons: [
                     {
                         extend: 'copy'
                     }, 
                     {
                         extend: 'excel',
                         title: 'Clientes'
                     }, 
                     {
                         extend: 'print',
                         customize: function (win) {
                             $(win.document.body).addClass('white-bg');
                             $(win.document.body).css('font-size', '10px');
                             $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                         }
                     }
                 ]
             });
             $('[data-toggle="tooltip"]').tooltip();
         },
         url: "CuentaSupervisor/getClientesConEjecutivos"
     });
}