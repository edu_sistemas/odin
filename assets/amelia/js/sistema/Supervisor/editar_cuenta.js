$(document).ready(function() {
    $('.footable').footable();

    $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

	//Habilitar formulario-----------------------
	$( "#form-editar-cuenta" ).prop( "disabled", true );

	//Deshabilitar formulario-----------------------
	$('#form-editar-cuenta input').attr('disabled', 'disabled');
	$('#form-editar-cuenta select').attr('disabled', 'disabled');
	$('#form-editar-cuenta textarea').attr('disabled', 'disabled');

    cargar_contactos();

	// CARGA CBX
	cargaAjaxComboHolding();
	cargaAjaxComboClasificacionCuenta();
	cargaAjaxComboEjecutivoAsignado();
	cargaAjaxComboRubroEmpresa();
	cargaAjaxComboSubRubroEmpresa();
	cargaAjaxComboTamanoEmpresa();
	cargaAjaxComboPaisCuenta();
    cargaAjaxComboComuna();   

    // Abrir Modal
    CargaInicialModal();

    $('#dv').keyup(function (e) {
        comprobarRut();
    });
    $('#Var_rut_empresa').keyup(function (e) {
        comprobarRut();
    });

});

//Abrir modal al inicio
function CargaInicialModal(){
    var URLactual = window.location; 
    var n = (URLactual).toString().lastIndexOf("/")
    var index = ((URLactual).toString().substring(n+1, n.length)).toString();
    var id_empresa = index.split(".",1);
    $("#txtnivel").val(3); 
    $.ajax({
    async: true,
    cache: false,
    dataType: "json",
    type: 'POST',
    data: { id_empresa: id_empresa },
    error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
            //alerta('Nueva Cuenta', jqXHR.responseText, 'error');
            alert(textStatus + errorThrown+jqXHR.responseText);
        },
        success: function (datos_contacto) {
            $("#idcontacto").val(0);

            var cc = 0;
            for(c=0;c<datos_contacto.length;c++)
            {
                if(datos_contacto[c]['crm_nivel_contacto']== 3){
                    cc++;
                }
            }
            if (cc == 3){
                 $("#txtnivel").val(4); 
            }
             if (datos_contacto.length == 8){
                 alerta('Contactos Completos','Esta empresa posee todos los nivles de contacto completos, elimine uno para agregar', 'warning');
             }else{
                    if (index.lastIndexOf(".") > 0) {
                     $('#modalcontactos').modal("show");
                    }
                 }
            },
        url: "../cargar_contactos"
    }); 

    $("#txtmail").val($("#hidden_correo_sug").val());
    $("#txtnombre").val($("#hidden_nombre_sug").val());
    $("#btnGuardarContacto").html('Agregar Contacto');
}

//Deshabilitar formulario-----------------------
function deshab() {
  frm = document.forms['form-editar-cuenta'];
  for(i=0; ele=frm.elements[i]; i++)
    ele.disabled=false;
}

function modalcontacto(accion,id,nivel){
 LimpiarForm('form-contactos');
 $("#idcontacto").val(id);
 if(accion==2) {
        //Llamar datos desde BD
        carga_data_contacto(id);
        $("#btnGuardarContacto").html('Guardar Cambios');
    }else{
        $("#btnGuardarContacto").html('Agregar Contacto');
        $("#txtnivel").val(nivel);
    }
    $("#modalcontactos").modal('toggle')
}

function carga_data_contacto(id){
  $.ajax({
    async: false,
    cache: false,
    dataType: "json",
    type: 'POST',
    data: { id_contacto: id },
    error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
            //alerta('Nueva Cuenta', jqXHR.responseText, 'error');
            alert(textStatus + errorThrown+jqXHR.responseText);
        },
        success: function (result) {
           //Procesar Respuesta
           $("#txtnombre").val(result[0].nombre_contacto);
           $("#txtcargo").val(result[0].crm_cargo_contacto);
           $("#txtdepto").val(result[0].crm_departamento_contacto);
           $("#txtmail").val(result[0].correo_contacto);
           $("#txtcantidad").val(result[0].crm_cantidad_hijos_contacto);
           $("#txtdescripcion").val(result[0].crm_descripcion_contacto);
           $("#txtnivel").val(result[0].crm_nivel_contacto);
           $("txtestado select").val(result[0].estado_contacto);
           $("#txtfonooficina").val(result[0].crm_telefono_oficina_contacto);
           $("#txtfonomovil").val(result[0].crm_telefono_movil_contacto);
           $("#txtcumpleanos").val(result[0].crm_fecha_cumpleanos_contacto);
           $("#txtfax").val(result[0].crm_fax_contacto);
       },
       url: "../getContacto"
   }); 
}

function cargar_contactos(){
    var id_empresa=$("#Var_id_empresa").val();
    var html="<table class='table table-stripped'><thead><tr><th>Nivel Contacto</th><th>Contacto</th><th></th></tr></thead><tbody>";
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_empresa: id_empresa },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
            //alerta('Nueva Cuenta', jqXHR.responseText, 'error');
            alert(textStatus + errorThrown+jqXHR.responseText);
        },
        success: function (datos_contacto) {
            // Nivel 1 y 2
            for(n=1;n<3;n++){
                titnivel = "";
                if (n == 1){
                    titnivel = "Tomador de Decisión";
                }else{
                    titnivel = "Gerente";
                }
	            html=html+"<tr><td><b>"+n+" "+titnivel+"</b></td>";
                 var cnt=0;	
                 var nom="";
                 var niv=0;
                 var idc=0;

                for(c=0;c<datos_contacto.length;c++){
                    if(datos_contacto[c]['crm_nivel_contacto']==n){
                        nom = datos_contacto[c]['nombre_contacto'];
                        niv = n;
                        idc = datos_contacto[c]['id_contacto_empresa'];
                        html=html+"<td><div id='ndc_"+idc+"'>"+nom+"</div></td><td>";
                        if(idc>0){
                            html=html+"<button class='btn btn-success btn-sm' onclick='modalcontacto(2,"+idc+",0);'>Ver</button>";
                        }
                        html=html+"</td></tr>";
                        cnt++;
                        break;
                    }
                }
                if(cnt==0) { 
                    html=html+"<td>Sin Contacto</td><td></td></tr>";
                }
            }
            for(n=3;n<=4;n++){
	            titnivel = "";
                if (n == 3){
                titnivel = "Punto de Apoyo";
                }else{
                    titnivel = "Punto de Información";
                }	
                var cnt=0;
                html=html+"<tr><td rowspan='3'><b>"+n+" "+titnivel+"</b></td>";
                for(c=0;c<datos_contacto.length;c++){
                    var nom="";
                    var niv=0;
                    var idc=0;
                    if(datos_contacto[c]['crm_nivel_contacto']==n){
                        if(cnt>0) { 
                             html=html+"<tr>"; 
                            }
                        nom = datos_contacto[c]['nombre_contacto'];
                        niv = n; 
                        idc = datos_contacto[c]['id_contacto_empresa'];
                        html=html+"<td><div id='ndc_"+idc+"'>"+nom+"</div></td><td><button class='btn btn-success btn-sm' onclick='modalcontacto(2,"+idc+","+n+");'>Ver</button></td></tr>";
                        cnt++;
                    }
                }
                for(i=cnt;i<3;i++){
                    if(cnt>0) {
                        html=html+"<tr>"; 
                    }
                    html=html+"<td>Sin Contacto</td><td></td></tr>";
                }
            }
            html = html+"</tbody></table>";
            $("#ContenedorContactos").html(html);
        },
        url: "../cargar_contactos"
   }); 
}

function LimpiarForm(formulario){
    $("#"+formulario)[0].reset();
}

//--------------------------PRUEBA HOLDING------------------------------
function cargaAjaxComboHolding() {
    $.ajax({			
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
             // console.log(jqXHR + textStatus + errorThrown);
			 //alert(errorThrown);
            },		
            success: function (result) {			
                cargaComboHolding(result);
		    //alert(result);
             },
            url: "../getComboHolding"
    });
    $("#Var_id_holding").val($("#hidden_id_holding").val()).trigger("change");
}

function cargaComboHolding(midata) {
   $("#Var_id_holding").select2({
       allowClear: true,
       data: midata
   });
}

//--------------------------PRUEBA CLASIFICACION CUENTA------------------------------
function cargaAjaxComboClasificacionCuenta() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboClasificacionCuenta(result);
        },
        url: "../getComboClasificacionCuenta"
    });
}

function cargaComboClasificacionCuenta(midata) {
   $("#Var_crm_clasificacion_cuenta").select2({
       allowClear: true,
       data: midata
    });
}

//--------------------------PRUEBA EJECUTIVO ASIGNADO------------------------------
function cargaAjaxComboEjecutivoAsignado() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaComboEjecutivoAsignado(result);
        },
        url: "../getComboEjecutivoAsignado"
    });
    $("#Var_crm_id_ejecutivo").val($("#hidden_id_ejecutivo").val()).trigger("change");
}

function cargaComboEjecutivoAsignado(midata) {
   $("#Var_crm_id_ejecutivo").select2({
       allowClear: true,
       data: midata
   });
}

//--------------------------PRUEBA RUBRO EMPRESA------------------------------
function cargaAjaxComboRubroEmpresa() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaComboRubroEmpresa(result);
        },
        url: "../getComboRubroEmpresa"
    });
    $("#tab1_detalle_cuenta_rubro").val($("#hidden_detalle_cuenta_rubro").val()).trigger("change");
}

function cargaComboRubroEmpresa(midata) {
   $("#tab1_detalle_cuenta_rubro").select2({
       allowClear: true,
       data: midata
   });
}

//--------------------------PRUEBA SUBRUBRO EMPRESA------------------------------
function cargaAjaxComboSubRubroEmpresa() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboSubRubroEmpresa(result);
        },
        url: "../getComboSubRubroEmpresa"
    });
    $("#tab1_detalle_cuenta_subrubro").val($("#hidden_detalle_cuenta_subrubro").val()).trigger("change");
}

function cargaComboSubRubroEmpresa(midata) {
   $("#tab1_detalle_cuenta_subrubro").select2({
       allowClear: true,
       data: midata
   });
}

//--------------------------PRUEBA TAMAÑO EMPRESA-----------------------------
function cargaAjaxComboTamanoEmpresa() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
           cargaComboTamanoEmpresa(result);
       },
       url: "../getComboTamanoEmpresa"
   });
}

function cargaComboTamanoEmpresa(midata) {
   $("#Var_crm_tamano_empresa").select2({
       allowClear: true,
       data: midata
   });
   $("#Var_crm_tamano_empresa").val($("#hidden_crm_tamano_empresa").val()).trigger("change");
}

//--------------------------PRUEBA PAIS------------------------------
function cargaAjaxComboPaisCuenta() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
           cargaComboPaisCuenta(result);
       },
       url: "../getComboPaisCuenta"
   });
    $("#Var_crm_id_pais").val($("#hidden_id_pais_cuenta").val()).trigger("change");
}

function cargaComboPaisCuenta(midata) {
   $("#Var_crm_id_pais").select2({
       allowClear: true,
       data: midata
   });
}

function cargaAjaxComboComuna() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboComuna(result);
        },
        url: "../getComboComuna"
    });
    $("#Var_crm_id_comuna").val($("#hidden_id_comuna").val()).trigger("change");
}

function cargaComboComuna(midata) {
    $("#Var_crm_id_comuna").select2({
        allowClear: true,
        data: midata
    });
}

function comprobarRut() {
    fth
    //     ALGORITMO 3, propiedades de la división por 11
    // 1. Multiplicar cada dígito del RUT se por 9, 8, ..., 4, 9, 8, ... de atrás hacia adelante.
    // 2. Sumar las multiplicaciones parciales.
    // 3. Suma alternada de la lista reversa de los dígitos del resultado anterior.
    // 4. El Dígito Verificador es el resultado anterior. Si es 10, se cambia por 'k'.
    // EJEMPLO.  RUT: 11.222.333
    // 1.   1   1   2   2   2   3   3   3  <--  RUT
    //    * 8   9   4   5   6   7   8   9  <--  9, 8, 7, 6, 5, 4, 9, 8, ...
    //    --------------------------------------
    //      8   9   8  10  12  21  24  27
    // 2. SUMA: 8 + 9 + 8 + 10 + 12 + 21 + 24 + 27 = 119
    // 3. SUMA ALTERNADA:  119 -> 9 - 1 + 1 = 9
    // 4. 9 <-- DÍGITO VERIFICADOR
    var rut = $("#Var_rut_empresa").val();
    var dv_form = $("#dv").val();
    var out_print = "";

    if (rut.trim() == "" || dv_form.trim() == "") {
        out_print = "Debe ingresar RUT y DV";
    } else {
        if (rut != "" && dv_form != "") {
            out_print = "";
            if (dv_form == "k") {
                dv_form = "10";
            }
        var rutArr = [];
        var secuencia = [9, 8, 7, 6, 5, 4, 9, 8];
        for (var i = 0; i < rut.length; i++) {
            rutArr.push(parseInt(rut.charAt(i)));
        }
        rutArr.reverse();
        var xParciales = 0;
        for (var i = 0; i < rutArr.length; i++) {
            xParciales = xParciales + (secuencia[i] * rutArr[i]);
        }
        var str_xParciales = xParciales.toString();
        var xParcialesArr = [];
        for (var i = 0; i < str_xParciales.length; i++) {
            xParcialesArr.push(parseInt(str_xParciales.charAt(i)));
        }
        xParcialesArr.reverse();
        var dvSumas = 0;
        var dvRestas = 0;
        for (var i = 0; i < xParcialesArr.length; i++) {
            if ((i + 1) % 2 == 0) {
                dvRestas = dvRestas - xParcialesArr[i];
            } else {
                dvSumas = dvSumas + xParcialesArr[i];
            }
        }
        var dv = dvSumas + dvRestas;
        if (dv < 0 || dv > 9) {
            dv = 10;
        }
        if (dv == parseInt(dv_form)) {
            out_print = "";
        } else {
            out_print = "No se ha ingresado un rut válido";
        }
        } else {
            out_print = "No se ha ingresado un rut válido";
        }
    }
    document.getElementById("out_print").innerHTML = out_print;
}