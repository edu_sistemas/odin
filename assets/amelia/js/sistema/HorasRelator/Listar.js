// JavaScript Document
$(function () {
       recargaLaPagina();
    $('#datetimepickerInicio').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
    $('#datetimepickerTermino').datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });
    $('#btn_buscar').click(function () {

        var fechaI = $('#fechaI').val();
        var fechaT = $('#fechaT').val();
        if (fechaI == '' || fechaT == '') {
            swal({
                title: "Datos faltantes",
                text: "Ingrese ambas Fechas",
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "ok",
                closeOnConfirm: true
            },
                    function () {
                        $('#fechaI').focus();
                    });

            return false;
        }
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {finicio: fechaI, ftermino: fechaT},
            error: function (jqXHR, textStatus, errorThrown) {
            },
            success: function (result) {
                cargaTablaRelator(result);
            },
            url: "Listar/listar"
        });

    });
});
function modal(id_relator, fecha_inicio, fecha_fin, nombre) {
    $("#body").html('');
    $("#periodos").html('Periodo entre ' + fecha_inicio + ' y ' + fecha_fin);
    $("#nombreRelator").html(nombre);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_relator: id_relator, finicio: fecha_inicio, ftermino: fecha_fin},
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
        },
        success: function (result) {
            //bloque de actualizacion de modal
            var tbody = $('#body');

            for (var i = 0; i < result.length; i++) {
                // create an <tr> element, append it to the <tbody> and cache it as a variable:
                var tr = $('<tr/>').appendTo(tbody);

                // append <td> elements to previously created <tr> element:
                tr.append('<td>' + (i + 1) + '</td>');
                tr.append('<td>' + result[i].nombre_curso + '</td>');
                tr.append('<td>' + result[i].horario_inicio + '</td>');
                tr.append('<td>' + result[i].horario_termino + '</td>');
                tr.append('<td>' + result[i].horas_cronologicas + '</td>');


            }
            // reset the count:

            console.log(result[0]);
        },
        url: "Listar/detalleRelator"
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_relator: id_relator, finicio: fecha_inicio, ftermino: fecha_fin},
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus);
        },
        success: function (result) {
            //bloque de actualizacion de modal
            var tbody = $('#body');


            // create an <tr> element, append it to the <tbody> and cache it as a variable:
            var tr = $('<tr/>').appendTo(tbody);

            // append <td> elements to previously created <tr> element:
            tr.append('<td></td>');
            tr.append('<td></td>');
            tr.append('<td></td>');
            tr.append('<td><strong>Total horas periodo : </strong></td>');
            tr.append('<td><strong>' + result[0].horas_total_periodo + '</strong></td>');



            // reset the count:

            console.log(result[0]);
        },
        url: "Listar/detalleRelatorTotal"
    });


    $('#myModal5').modal();
}

function cargaTablaRelator(data) {
    if ($.fn.dataTable.isDataTable('#tbl_docentes')) {
        $('#tbl_docentes').DataTable().destroy();
    }
    $('#tbl_docentes').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut"},
            {"mDataProp": "nombre_usuario"},
            {"mDataProp": "apellidos_usuario"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<i title="Inactivo" class="fa fa-circle text-danger"></i>';
                    if (o.estado_usuario == 1) {
                        html = '  <i title="Activo"  class="fa fa-circle text-navy"></i>';
                    }
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var fechaI = "'" + o.fecha_inicio + "'";
                    var fechaT = "'" + o.fecha_fin + "'";
                    var nombre = "'" + o.nombre_usuario + ' ' + o.apellidos_usuario + "'";
                    var html = "";
                    html += '  <button type="button" class="btn btn-outline btn-default" onclick="modal(' + o.id_relator + ',' + fechaI + ',' + fechaT + ',' + nombre + ')">Dellate</button>';
                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}