$(document).ready(function () {

    buscarFiltro();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_nuevo_contacto").click(function () {
        $("#div_frm_add").css("display", "block");
        $("#div_tbl_contacto_alumno").css("display", "none");
        $("#btn_nuevo_contacto").css("display", "none");
    });


    $("#btn_guardar_contacto").click(function () {
        if (validarRequeridoForm($("#frm_nuevo_contacto_colaborador"))) {
            RegistarNuevoContacto();
        }
    });

    $("#btn_cancelar_contacto").click(function () {
        $("#div_frm_add").css("display", "none");
        $("#div_tbl_contacto_colaborador").css("display", "block");
        $("#btn_nuevo_contacto").css("display", "block");
    });

    $("#btn_buscar").click(function () {
        buscarFiltro();
    });
});

function RegistarNuevoContacto() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    var url = "Listar/guardarDatosContacto";


    if ($("#btn_guardar_contacto").text() == "Actualizar") {
        url = "Listar/actualzarDatosContacto";
    }


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_nuevo_contacto_colaborador').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Nuevo Contacto', errorThrown, 'error');
        },
        success: function (result) {

            //  alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {

                $("#div_frm_add").css("display", "none");
                $("#div_tbl_contacto_colaborador").css("display", "block");
                $("#btn_nuevo_contacto").css("display", "block");

                ContactoColaborador($("#txt_id_colaborador").val(), $("#modal_nombre_contacto").text());

                $("#btn_limpiar").click();
                $("#btn_guardar_contacto").text("Guardar");

            }
        },
        url: url
    });


}



function buscarFiltro() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#filtros_form').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Colaborador", errorThrown, "error");
        },
        success: function (result) {
            cargaBusqueda(result);
        },
        url: "Listar/buscarPorFiltro"
    });

}


// Crea tabla del listado pro Jquery luis
function cargaBusqueda(data) {

    if ($.fn.dataTable.isDataTable('#tbl_colaboradores')) {
        $('#tbl_colaboradores').DataTable().destroy();
    }

    $('#tbl_colaboradores').DataTable({
        "aaData": data,
        "aoColumns": [
        	{
                "mDataProp": "rut_completo"
            }, {
                "mDataProp": "nombre"
            }, {
                "mDataProp": "apellidos"
            }, {
                "mDataProp": "departamento"
            }, {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
//                    html += "<div class='btn-group'>";
//                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
//                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
//                    var x = "Editar/index/" + o.id_colaborador;
//                    html += "<li><a href='" + x + "'>Modificar</a></li>";
//                    if (o.estado == 0) {
//                        html += "<li><a    onclick='EditarColaboradorEstado(" + o.id_colaborador + ",0);'>Activo</a></li>";
//                    } else {
//                        html += "<li><a    onclick='EditarColaboradorEstado(" + o.id_colaborador + ",1);'>Desvinculado</a></li> ";
//                    }
//
//                    html += "</ul>";
//                    html += "</div>";
                    
                    var x = "Detalle/index/" + o.id_colaborador;
                    html += "<a href='" + x + "'>"+'<button class="btn btn-primary">Detalles Colaborador</button>'+"</a>";
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Perfiles'
            }, {
                extend: 'pdf',
                title: 'Perfiles'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

}


function EditarColaboradorEstado(id_colaborador, tipo_estado) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id: id_colaborador, estado: tipo_estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Colaborador", errorThrown, "error");
        },
        success: function (result) {

            buscarFiltro();
        },
        url: "Listar/CambiarEstadoColaborador"
    });

}

function ContactoColaborador(id, nombre_contacto) {
    $("#modal_nombre_contacto").text(nombre_contacto);
    $("#txt_id_colaborador").val(id);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_colaborador: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Colaborador", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosContactoColaborador(result);
            $("#modal_contacto_alummno").modal("show");
        },
        url: "Listar/verDatosContacto"
    });
}

function cargaDatosContactoColaborador(data) {
    var html = "";
    $('#tbl_contacto_colaborador tbody').html(html);
    $.each(data, function (i, item) {
        html += '        <tr>';
        html += '        <td>' + item.correo_contacto + '</td> ';
        html += '        <td>' + item.telefono_contacto + '</td> ';
        html += '        <td>' + item.fecha_registro + '</td> ';
        html += '        <td>';

        if (item.estado_contacto == 0) {
            html += '<button class="btn btn btn-primary readonly btn-circle"  title="Activo" type="button"><i class="fa fa-check"></i></button>';
        } else {
            html += '<button class="btn btn-danger btn-circle" readonly title="Inactivo" type="button"><i class="fa fa-times"></i></button>';
        }

        html += '</td>';
        html += '<td>';

        if (item.estado_contacto == 0) {
            html += '<button class="btn btn-warning btn-circle btn-sm" type="button" title="desactivar" onclick="desactivarContactoColaborador(' + item.id_contacto_colaborador + ',0)" ><i class="fa fa-minus-circle"></i></button>';
        } else {
            html += '<button class="btn btn-primary btn-circle btn-sm" type="button" title="Activar" onclick="desactivarContactoColaborador(' + item.id_contacto_colaborador + ',1)" ><i class="fa fa-minus-circle"></i></button>';
        }

        var datos = "";
        datos = "'" + item.id_contacto_colaborador + "','" + item.correo_contacto + "','" + item.telefono_contacto + "','" + item.id_colaborador + "'";

        html += '<button onclick="ubicarCamposUpdateContactoColaborador(' + datos + ')" class="btn btn-info btn-circle btn-sm" type="button" title="Editar" ><i class="fa fa-edit"></i>  </button>';
        html += '<button   onclick="EliminarContactoColaborador(' + datos + ')" class="btn btn-danger btn-circle btn-sm" type="button" title="Elimiar" ><i class="fa fa-times-circle-o"></i>  </button>';
        html += '</td> ';
        html += '</tr>';
    });

    $('#tbl_contacto_colaborador tbody').html(html);
}

function desactivarContactoColaborador(id_contacto_colaborador, estatus) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto: id_contacto_colaborador,
            estado: estatus
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Colaborador", errorThrown, "error");
        },
        success: function (result) {

            ContactoColaborador($("#txt_id_colaborador").val(), $("#modal_nombre_contacto").text());
        },
        url: "Listar/desactivarDatosContacto"
    });
}

function ubicarCamposUpdateContactoColaborador(id_contacto_colaborador, correo_contacto, telefono_contacto, txt_id_colaborador) {
    $("#div_frm_add").css("display", "block");
    $("#div_tbl_contacto_colaborador").css("display", "none");
    $("#btn_nuevo_contacto").css("display", "none");
    $("#txt_id_contacto_colaborador").val(id_contacto_colaborador);
    $("#txt_id_colaborador").val(txt_id_colaborador);
    $("#txt_correo_contacto").val(correo_contacto);
    $("#txt_telefono").val(telefono_contacto);
    $("#btn_guardar_contacto").text("Actualizar");
}


function EliminarContactoColaborador(id_contacto_colaborador, correo_contacto, telefono_contacto, id_colaborador) {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto_colaborador: id_contacto_colaborador,
            correo_contacto: correo_contacto,
            telefono_contacto: telefono_contacto,
            id_colaborador: id_colaborador
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Colaborador", errorThrown, "error");
        },
        success: function (result) {

            ContactoColaborador(id_colaborador, $("#modal_nombre_contacto").text());
        },
        url: "Listar/eliminarDatosContacto"
    });

}

