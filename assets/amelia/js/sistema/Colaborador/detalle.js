var lat;
var lng;
var map;
var currentTab="Nueva Dirección";
var currCenter;
$(document).ready(function () {
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
    $("#btn_eliminar_direccion").prop("disabled",true);
    $('.nav-tabs a').on('shown.bs.tab', function(event){
        currentTab = $(event.target).text();
    });

    $("#btn_guardar_direccion").click(function () {
        if(currentTab == "Nueva Dirección"){
            if (validarRequeridoForm($("#frm_direccion_registro"))) {
                setDireccionColaborador();
            }
        }
        if(currentTab == "Editar Dirección"){
            if (validarRequeridoForm($("#frm_direccion_editar"))) {
                updateDireccionColaborador();
            }
        }
        
        if(currentTab == "Teléfonos"){
            if (validarRequeridoForm($("#frm_telefono"))) {
                insertarTelefono();
            }
        }
        
        if(currentTab == "Correos Electrónicos"){
            if (validarRequeridoForm($("#frm_mail"))) {
                insertarMail();
            }
        }
        
    });
    
    $("#btn_guardar_contrato").click(function () {
        if (validarRequeridoForm($("#frm_agregar_contrato"))) {
            alert("oka");
        }
    });
    
    $("#btn_eliminar_direccion").click(function() {
    		swal({
      		  title: "¿Está seguro?",
      		  text: "Si la borra hace no recupera dar a conocer",
      		  type: "warning",
      		  showCancelButton: true,
      		  //confirmButtonColor: "#DD6B55",
      		  confirmButtonText: "Borrar!",
      		  cancelButtonText: "Cancelar",
      		  closeOnConfirm: true
      		},
      		function(){
      			eliminarDireccion($("#d_direccion_e").val());
      		});
    	
	});

    $("#datos-de-contacto").click(function () {
        DireccionColaborador();
    });
    
    $("#datos-de-contratacion").click(function () {
        ContratoColaborador();
    });
    
    $("#datos-basicos").click(function () {
        BasicosColaborador();
    });

    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });

    $('#d_direccion_e').change(function () {


        if ($('#d_direccion_e').val() != "") {
            getSelectedDireccion();
            $("#btn_eliminar_direccion").prop("disabled",false);
        } else {
        	$("#btn_eliminar_direccion").prop("disabled",true);
            // $('#empresa').html('').select2({
            //     placeholder: "Seleccione",
            //     allowClear: true,
            //     data: [{id: '', text: ''}]
            // });
        }
    });
    
    $("#tel-anim").click(function() {
			$("#elmapa").css({display: "none"});
			$("#id_c_t").val($("#id_colaborador").val());
			getTelefonos($("#id_colaborador").val());
	});
    
    $("#mail-anim").click(function() {
		$("#elmapa").css({display: "none"});
		$("#id_c_m").val($("#id_colaborador").val());
		getMails($("#id_colaborador").val());
    });
    
    $("#nuev-anim").click(function() {
		$("#elmapa").css({display: "block"});
    });
    
    $("#edit-anim").click(function() {
		$("#elmapa").css({display: "block"});
    });
    
    $("#btn_agregar_contrato").click(function() {
		$("#contratos-main").animate({
			height: "toggle"
		}, 300, function() {
			$("#contratos-agregar").animate({
				height: "toggle"
			}, 300, function() {
				
			});
		});
    });
    
    $("#btn_cancelar_agregar_contrato").click(function() {
		$("#contratos-agregar").animate({
			height: "toggle"
		}, 300, function() {
			$("#contratos-main").animate({
				height: "toggle"
			}, 300, function() {
				
			});
		});
    });
    
    $("#btn_basico_editar_datos").click(function() {
    	
    	
    	var datos = BasicosColaborador();  
    	$("#basico-botones-1").hide();
    	$("#basico-botones-2").show();
 	
        $("#basico-rut").html('').html('<input type="text" id="basico-rut-editar" value="'+ datos[0].rut +'" onkeypress="return solonumero(event)" > - <input type="text" style="width:20px;" id="basico-dv-editar" value="'+ datos[0].rut_dv +'">');
        $("#basico-nombre").html('').html('Nombres: <input type="text" id="basico-nombre-editar" style="width: 18%" value="'+ datos[0].nombre +'"> Apellido Paterno: <input type="text" id="basico-apellido-paterno-editar" style="width: 18%" value="'+ datos[0].apellido_pat +'"> Apellido Materno: <input type="text" id="basico-apellido-materno-editar" style="width: 18%" value="'+ datos[0].apellido_mat +'">');
        $("#basico-fecha-nacimiento").html('').html('<input type="date" id="basico-fecha-nacimiento-editar" value="'+ datos[0].fecha_nacimiento_sin_formato +'">');
        $("#basico-genero").html('').html('<select id="basico-genero-editar"></select>');
        $("#basico-estado-civil").html('').html('<select id="basico-estado-civil-editar"></select>');
        $("#basico-nacionalidad").html('').html('<select id="basico-nacionalidad-editar"></select>');
        
        cargaGenero(datos[0].id_genero);
        cargaEstadoCivil(datos[0].id_estado_civil);
        cargaNacionalidad(datos[0].id_nacionalidad);
        
	});
    
    $("#btn_basico_cancelar_datos").click(function() {
    	
    	$("#basico-botones-2").hide();
    	$("#basico-botones-1").show();
    	
        $("#basico-rut").html('');
        $("#basico-nombre").html('');
        $("#basico-fecha-nacimiento").html('');
        $("#basico-genero").html('');
        $("#basico-estado-civil").html('');
        $("#basico-nacionalidad").html('');
        
        BasicosColaborador();
	});
    
	$("#btn_basico_guardar_datos").click(function() {
	    	
		swal({
			  title: "¿Está seguro?",
			  text: "Haga clic en Guardar para confirmar los cambios",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonText: "Guardar",
			  cancelButtonText: "Cancelar",
			  closeOnConfirm: true
			},
			function(){
				$.ajax({
			        async: false,
			        cache: false,
			        dataType: "json",
			        type: 'POST',
			        data: {
			        	id_colaborador: $("#id_colaborador").val(),
			        	rut: $("#basico-rut-editar").val(),
			        	dv: $("#basico-dv-editar").val(),
			        	nombre: $("#basico-nombre-editar").val(),
			        	apellido_paterno: $("#basico-apellido-paterno-editar").val(),
			        	apellido_materno: $("#basico-apellido-materno-editar").val(),
			        	fecha_nacimiento: $("#basico-fecha-nacimiento-editar").val(),
			        	genero: $("#basico-genero-editar").val(),
			        	estado_civil: $("#basico-estado-civil-editar").val(),
			        	nacionalidad: $("#basico-nacionalidad-editar").val()
			        },
			        error: function (jqXHR, textStatus, errorThrown) {
			            // code en caso de error de la ejecucion del ajax
			            //console.log(textStatus + errorThrown);

			            alerta('Direccion Colaborador', jqXHR.responseText, 'error');
			        },
			        success: function (result) {
			            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
			            BasicosColaborador();
			            $("#basico-botones-2").hide();
			        	$("#basico-botones-1").show();
			            // if (  result[0].reset==1){
			            //       $('#btn_back').trigger("click");
			            // }
			        },
			        url: "../updateBasicosColaborador"
			    });
			});        
	});
    
    $('#basico-fecha-nacimiento-editar').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    
    $("#tab-familia").click(function() {
    	cargaParentescos();
	});
});


function searchDireccion() {

    var busqueda;

    if(currentTab == "Nueva Dirección"){
        busqueda = $('#d_calle').val() + ' ' + $('#d_numero').val() + ', ' + $('#d_comuna option:selected').text();
    }
    if(currentTab == "Editar Dirección"){
        busqueda = $('#d_calle_e').val() + ' ' + $('#d_numero_e').val() + ', ' + $('#d_comuna_e option:selected').text();
    }
    $('#pac-input').val(busqueda);
}

function setDireccionColaborador() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_direccion_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Direccion Colaborador', jqXHR.responseText, 'error');
        },
        success: function (result) {
        	$("#d_calle").val("");
        	$("#d_numero").val("");
        	$("#d_departamento").val("");
        	$("#d_comuna").val("0");
        	$("#d_comuna").trigger("change");
        	
        	$("#d_calle_e").val("");
        	$("#d_numero_e").val("");
        	$("#d_departamento_e").val("");
        	$("#d_comuna_e").val("0");
        	$("#d_comuna_e").trigger("change");
        	cargaDirecciones();
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "../setDireccionColaborador"
    });
}

function updateDireccionColaborador() {
	
	var id_direccion = $("#d_direccion_e").val();
	$.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_direccion_editar').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Direccion Colaborador', jqXHR.responseText, 'error');
        },
        success: function (result) {
        	$("#d_calle").val("");
        	$("#d_numero").val("");
        	$("#d_departamento").val("");
        	$("#d_comuna").val("0");
        	$("#d_comuna").trigger("change");
        	
        	$("#d_calle_e").val("");
        	$("#d_numero_e").val("");
        	$("#d_departamento_e").val("");
        	$("#d_comuna_e").val("0");
        	$("#d_comuna_e").trigger("change");
        	cargaDirecciones();
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "../updateDireccionColaborador"
    });
}

    function DireccionColaborador() {

    $("#modal_direccion_colaborador").modal("show");
    google.maps.event.trigger(map, 'resize');
    cargaComunas();
    cargaDirecciones();
    cargaTipoDato();
    $('#id_c').val($('#id_colaborador').val());
    map.setCenter(currCenter);
    $('#nombre_colaborador_modal').html($('#nombre_colaborador').html());
}
    
    function ContratoColaborador() {

        $("#modal_colaborador_contrato").modal("show");
        $('#nom_colaborador_contrato').html($('#nombre_colaborador').html());
        $("#contratos-main").css({display: "block"});
        $("#contratos-agregar").css({display: "none"});
    }
    
    function BasicosColaborador() {
    	
    	var datosArray = [];
    	$('#nom_colaborador_basico').text($('#nombre_colaborador').text());
    	 $.ajax({
    	        async: false,
    	        cache: false,
    	        dataType: "json",
    	        type: 'POST',
    	        data: {id_colaborador: $('#id_colaborador').val()},
    	        error: function (jqXHR, textStatus, errorThrown) {

    	            alerta("Colaborador", errorThrown, "error");
    	        },
    	        success: function (result) {
    	        	datosArray = result;
    	            //cargaDatosContactoColaborador(result);
    	            $("#modal_colaborador_basico").modal("show");
    	            
    	            $("#basico-rut").text(result[0].rut_completo);
    	            $("#basico-nombre").text(result[0].nombre_completo);
    	            $("#basico-fecha-nacimiento").text(result[0].fecha_nacimiento);
    	            $("#basico-genero").text(result[0].genero);
    	            $("#basico-estado-civil").text(result[0].estado_civil);
    	            $("#basico-nacionalidad").text(result[0].nacionalidad);
    	        },
    	        url: "../getDatosBasicosColaborador"
    	    });
    	 return datosArray;
    }

function cargaComunas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
          //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'd_comuna');
            cargaCombo(result, 'd_comuna_e');
        },
        url: "../getComunaCBX"
    });
}

function cargaParentescos(){
	
	
	$.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
          //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

        	cargaCBX("familiar-parentesco", result);
        },
        url: "../getParentescoCBX"
    });
}


function cargaGenero(seleccionado) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
          //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

        	cargaCBXSelected("basico-genero-editar", result, seleccionado);
        },
        url: "../getGeneroCBX"
    });
}

function cargaEstadoCivil(seleccionado) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
          //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

        	cargaCBXSelected("basico-estado-civil-editar", result, seleccionado);
        },
        url: "../getEstadoCivilCBX"
    });
}

function cargaNacionalidad(seleccionado) {
	$.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
          //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCBXSelected("basico-nacionalidad-editar", result, seleccionado);
        },
        url: "../getNacionalidadCBX"
    });
}

function cargaDirecciones() {
	$('#d_direccion_e').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{id: '', text: ''}]
    });
	$("#btn_eliminar_direccion").prop("disabled",true);
    var id_colaborador = $('#id_colaborador').val();
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_colaborador: id_colaborador
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "d_direccion_e");
        },
        url: "../getDireccionesByIdCBX"
    });
}

function cargaCombo(midata, item) {
    $("#"+ item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    //$("#"+ item +" option[value='0']").remove();

}

function getSelectedDireccion() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_direccion: $('#d_direccion_e').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
             console.log(textStatus + errorThrown);
        },
        success: function (result) {

            fillDireccion(result);
        },
        url: "../getDireccionById"
    });

}

function getTelefonos(id_colaborador) {
	$.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_colaborador: id_colaborador},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
             console.log(textStatus + errorThrown);
        },
        success: function (result) {

            loadTableTelefono(result);
        },
        url: "../getTelefonosById"
    });
}

function getMails(id_colaborador) {
	$.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_colaborador: id_colaborador},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
             console.log(textStatus + errorThrown);
        },
        success: function (result) {

            loadTableMail(result);
        },
        url: "../getMailsById"
    });
}

function loadTableTelefono(data) {
	    
    var html = "";
    var tipo_entre ='';
        $('#tbl_telefonos tbody').html(html);
        $.each(
        	data,
                        function (i, item) {
                        	
                        	if(item.id_telefono != null){
                        		html += '<tr><td>';
                        		html += i+1;
                        		html += '</td><td>';
                        		html += item.numero;
                        		html += '</td><td>';
                        		html += item.nombre_tipo;
                        		html += '</td><td>';
                        		html += '<button type="button" onclick="eliminarTelefono(';
                        		html += item.id_telefono;
                        		html += ')" class="btn btn-danger btn-xs">Eliminar</button>';
                        		html += '</td></tr>';
                        	}
                        });

    $('#tbl_telefonos tbody').html(html);
}

function loadTableMail(data) {
    
    var html = "";
    var tipo_entre ='';
        $('#tbl_mails tbody').html(html);
        $.each(
        	data,
                        function (i, item) {
                        	
                        	if(item.id_email != null){
                        		html += '<tr><td>';
                        		html += i+1;
                        		html += '</td><td><a href="mailto:';
                        		html += item.email;
                        		html += '">';
                        		html += item.email;
                        		html += '</a></td><td>';
                        		html += item.nombre_tipo;
                        		html += '</td><td>';
                        		html += '<button type="button" onclick="eliminarMail(';
                        		html += item.id_email;
                        		html += ')" class="btn btn-danger btn-xs">Eliminar</button>';
                        		html += '</td></tr>';
                        	}
                        });

    $('#tbl_mails tbody').html(html);
}

function insertarTelefono() {
	$.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_telefono').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Direccion Colaborador', jqXHR.responseText, 'error');
        },
        success: function (result) {
        	$("#tipoTelefonoCBX").val("0");
        	$("#telefono").val("");
        	$("#tipoTelefonoCBX").trigger("change");
        	getTelefonos($('#id_colaborador').val());
            //alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "../setTelefonoColaborador"
    });
}

function insertarMail() {
	$.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_mail').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Mail Colaborador', jqXHR.responseText, 'error');
        },
        success: function (result) {
        	$("#tipoMailCBX").val("0");
        	$("#email").val("");
        	$("#tipoMailCBX").trigger("change");
        	getMails($('#id_colaborador').val());
            //alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "../setMailColaborador"
    });
}

function eliminarDireccion(id_direccion) {
	
	$.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_direccion: id_direccion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Direccion Colaborador', jqXHR.responseText, 'error');
        },
        success: function (result) {
        	$("#d_calle_e").val("");
        	$("#d_numero_e").val("");
        	$("#d_departamento_e").val("");
        	$("#d_comuna_e").val("0");
        	$("#d_comuna_e").trigger("change");
        	cargaDirecciones();
            //alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "../eliminarDireccionColaborador"
    });
}

function eliminarTelefono(id_telefono) {
	
	swal({
		  title: "¿Está seguro?",
		  text: "¿Desea borrar el número telefónico?",
		  type: "warning",
		  showCancelButton: true,
		  //confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Borrar",
		  cancelButtonText: "Cancelar",
		  closeOnConfirm: true
		},
		function(){
			$.ajax({
		        async: false,
		        cache: false,
		        dataType: "json",
		        type: 'POST',
		        data: {id_telefono: id_telefono},
		        error: function (jqXHR, textStatus, errorThrown) {
		            // code en caso de error de la ejecucion del ajax
		            //console.log(textStatus + errorThrown);

		            alerta('Direccion Colaborador', jqXHR.responseText, 'error');
		        },
		        success: function (result) {
		        	getTelefonos($('#id_colaborador').val());
		            //alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

		            // if (  result[0].reset==1){
		            //       $('#btn_back').trigger("click");
		            // }
		        },
		        url: "../eliminarTelefonoColaborador"
		    });
		});
}

function eliminarMail(id_email) {
	
	swal({
		  title: "¿Está seguro?",
		  text: "¿Desea borrar el correo electrónco?",
		  type: "warning",
		  showCancelButton: true,
		  //confirmButtonColor: "#DD6B55",
		  confirmButtonText: "Borrar",
		  cancelButtonText: "Cancelar",
		  closeOnConfirm: true
		},
		function(){
			$.ajax({
		        async: false,
		        cache: false,
		        dataType: "json",
		        type: 'POST',
		        data: {id_email: id_email},
		        error: function (jqXHR, textStatus, errorThrown) {
		            // code en caso de error de la ejecucion del ajax
		            //console.log(textStatus + errorThrown);

		            alerta('Email Colaborador', jqXHR.responseText, 'error');
		        },
		        success: function (result) {
		        	getMails($('#id_colaborador').val());
		            //alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

		            // if (  result[0].reset==1){
		            //       $('#btn_back').trigger("click");
		            // }
		        },
		        url: "../eliminarMailColaborador"
		    });
		});
}

function fillDireccion(data) {
    $('#d_calle_e').val(data[0].calle);
    $('#d_numero_e').val(data[0].numero);
    $('#d_departamento_e').val(data[0].departamento);
    $("#d_comuna_e").val(data[0].id_comuna).trigger("change");
    $('#latitud_e').val(data[0].latitud);
    $('#longitud_e').val(data[0].longitud);
    $('#btn_confirmar_mapa').trigger("click");
}

// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.


function initAutocomplete() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.4394247, lng: -70.64906500000001},
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  currCenter = map.getCenter();

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

        var location = place.geometry.location;
        lat = location.lat();
        lng = location.lng();

        document.getElementById('latitud').value = lat;
        document.getElementById('longitud').value = lng;

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        icon: icon,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });

  document.getElementById('btn_confirmar_mapa').onclick = function (e) {

        searchDireccion();
        var input = document.getElementById('pac-input');

        google.maps.event.trigger(input, 'focus')
        google.maps.event.trigger(input, 'keydown', {
            keyCode: 13,

        });
        e.preventDefault();
    };
}