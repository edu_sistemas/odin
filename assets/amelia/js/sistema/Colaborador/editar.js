
$(document).ready(function () {

    $("#btn_editar_alumno").click(function () {
        if (validarRequeridoForm($("#frm_alumno_editar"))) {
            EditarAlm();
        }
    });


    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });

});




function EditarAlm() {

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_alumno_editar').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Categoria', jqXHR.responseText, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "../EditarAlumno"
    });




}
