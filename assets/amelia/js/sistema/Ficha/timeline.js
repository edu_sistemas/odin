$(document).ready(function () {
    cargaTodosDatosExtras();
    llamaDatosFicha();
    $('#data_5 .input-daterange').datepicker({
        format: "dd-mm-yyyy",
        language: 'es',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $("#btn_cerrarmodal_doc").on('click', function () {
        $('#modal_documentos_ficha').modal('hide');
        llamaDatosFicha();
    });


    $('#modal-principal').click(function (event) {
        event.stopPropagation();
    });


    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_arriendo").click(function () {
        location.href = 'Arriendo';
    });

    $("#btn_buscar_ficha").click(function () {
        llamaDatosFicha();
    });

    $("#cbx_holding").select2({
        placeholder: "Todos",
        allowClear: true
    });


    $("#btn_cerrar_modal_docmento").click(function () {
        llamaDatosFicha();
    });


    $("#btn_nuevo_contacto").click(function () {
        $("#div_frm_add").css("display", "block");
        $("#div_tbl_contacto_empresa").css("display", "none");
        $("#btn_nuevo_contacto").css("display", "none");
    });

    $("#btn_guardar_contacto").click(function () {
        if (validarRequeridoForm($("#frm_nuevo_contacto_empresa"))) {
            RegistarNuevoContacto();
        }
    });

    $("#btn_insert_accion_sence").click(function () {
        if (validarRequeridoForm($("#frm_id_accion_sence"))) {
            ingresarIDAccionSence();
        }
    });

    $("#btn_cancelar_contacto").click(function () {

        $("#div_frm_add").css("display", "none");
        $("#div_tbl_contacto_empresa").css("display", "block");
        $("#btn_nuevo_contacto").css("display", "block");
    });

    $("#fileupload").click(function (event) {

        $(function () {
            'use strict';
            $('#fileupload').fileupload({
                url: "Timeline/upload_file",
                dataType: 'json',
                type: 'POST',
                cache: false,
                maxFileSize: 20000000,
                formData: {
                    id_ficha: $("#modal_id_ficha").text()
                },
                done: function (e, data) {
                    // $('<p/>').text(data.files[0].name).appendTo('#files');
                    //$("#modal_id_ficha").val(data.result.Ficha);
                    DocumentosFicha($("#modal_id_ficha").text(), idestado);
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width', progress + '%');
                    $('#porsncetaje_carga').text(progress + '%');
                }

            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

        });


    });


});
function  cargaTodosDatosExtras() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            cargaComboGenerico(result, 'ejecutivo');
        },
        url: "Timeline/getEjecutivo"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            cargaComboGenerico(result, 'periodo');
        },
        url: "Timeline/getPeriodo"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result);
        },
        url: "Timeline/getEstadoCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo2(result);
        },
        url: "Timeline/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo3(result);
        },
        url: "Timeline/getHoldingCBX"
    });

}
function cargaComboGenerico(midata, id) {

    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

    $("#" + id + " option[value='0']").remove();

}


function cargaCombo(midata) {
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true,
        data: midata
    });

    $("#estado option[value='0']").remove();
}

function cargaCombo2(midata) {
    $('#empresa').html('').select2({data: [{id: '', text: ''}]});
    $("#empresa").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#empresa option[value='0']").remove();
}

function cargaCombo3(midata) {

    $("#holding").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#holding option[value='0']").remove();

}


function validaPreEnvioFicha(ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {



            if (result[0].mensaje == "ok") {
                sendFicha(ficha);

            } else {

                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);


            }

        },
        url: "Timeline/validaFicha"
    });




}


function validacionEliminar(ficha) {



    swal({
        title: "Estás seguro?",
        text: "Eliminarás toda la informacion cargada a la Ficha",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Eliminarlo!",
        cancelButtonText: "No, Cancelar!"
    }, function (isConfirm) {

        if (isConfirm) {
            DeleteFicha(ficha);
            llamaDatosFicha();
        }
    });

}

function sendFicha(ficha) {



    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            enviaCorreo();
            llamaDatosFicha();
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function () {
                        llamaDatosFicha();
                    });
        },
        url: "Timeline/enviarFicha"
    });

}

function  DeleteFicha(ficha) {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: ficha
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', jqXHR.responseText, 'error');
        },
        success: function (result) {

            swal("Ficha", "Tu Ficha ha sido eliminada!", "success");
            llamaDatosFicha();
        },
        url: "Timeline/eliminarFicha"
    });


}
var idestado = "";


function getFileAdjuntos() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#id_ficha").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', jqXHR.responseText, 'error');
        },
        success: function (result) {
            cargaTbLAdjunto(result);
        },
        url: "Timeline/getFileAdjunto"
    });
}

function cargaTbLAdjunto(data) {
    if ($.fn.dataTable.isDataTable('#tbl_adjuntos')) {
        $('#tbl_adjuntos').DataTable().destroy();
    }
    $('#tbl_adjuntos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_doc"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<a href="../../' + o.ruta_ducumento + '" target="_blank">' + o.nombre_documento + '</a>';
                    return html;
                }
            },
            {"mDataProp": "fecha_registro"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var x = "'Editar/index/" + o.id_otic + "'";
                    return '<button class="btn-primary"  onclick="location.href=' + x + '">Editar</button>';
                }
            }
        ],
        pageLength: 5,
        responsive: true
    });
}

function llamaDatosFicha() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaFicha(result);
        },
        url: "Timeline/buscarFicha"
    });
}

function enviaCorreo() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "Timeline/enviaEmail"
    });
}


function cargaTablaFicha(data) {

    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }
    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_ficha"},
            {"mDataProp": "num_ficha"},
            {"mDataProp": "categoria"},
            {"mDataProp": "modalidad"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += o.fecha_inicio + '-' + o.fecha_fin;
                    return html;
                }
            },
            {"mDataProp": "descripcion_producto"},
            {"mDataProp": "otic"},
            {"mDataProp": "empresa"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '  <p data-toggle="tooltip" data-placement="top" title data-original-title="Sence: ' + o.valor_sence + ' • Empresa: ' + o.valor_empresa + '" >' + o.valor_total + '</p>';
                    return html;
                }
            },
            {"mDataProp": "estado"},

            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = o.cant_alumno;
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
            	var html = "";
            	var html = '<button class="btn btn-success" onclick="abrirTimeline('+o.id_ficha+')">Ver</button>';
                return html;
            	}
            }
        ],
        pageLength: 25,
        responsive: false,

        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
        , "order": [[0, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function abrirTimeline(id_ficha) {
	$("#modal-timeline").modal("show");
	
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosTimeline(result);
        },
        url: "Timeline/getEventosFicha"
    });
}

function setIdAccion(id_ficha, num_ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {

            $("#modal_id_sence").modal("show");
            $("#id_ficha_modal_accion").html(num_ficha);
            $("#id_ficha_s").val(id_ficha);
            $("#combo_oc").html("");
            $.each(result, function (i, item) {
                $("#combo_oc").append("<option value=" + item.id + ">" + item.text + "</option>");
            });

            if (result.length == 0) {
                $("#combo_oc").prop('disabled', true);
                $("#combo_oc").prop('disabled', true);
                $("#btn_insert_accion_sence").prop('disabled', true);
                alerta("ID Acción Sence", "los id acción sence ya se ingresaron o no existen ordenes de compras asosiadas a la ficha", "error");
            }

        },
        url: "Timeline/getOcByFichaCBX"
    });
}

function ingresarIDAccionSence() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_id_accion_sence').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
        },
        url: "Timeline/setIDAccion"
    });
}

function cargaDatosTimeline(data) {


    var html = "";
    $('#eventos-timeline').html(html);
    $.each(data, function (i, item) {
       var estado_titulo, estado_comentario;
       var colores = ["primary", "secondary", "success", "warning","primary", "secondary", "success", "warning","primary", "secondary", "success", "warning"];


       if(parseInt(item.estado_timeline) >= 1){
           for (var i = 0; i < parseInt(item.estado_timeline); i++) {
               if(i == 6){
                   estado_titulo = "Pagado";
               }
               if(i == 5){
                   estado_titulo = "Facturado";
               }
               if(i == 4){
                   estado_titulo = "En proceso de Facturación";
               }
               if(i == 3){
                   estado_titulo = "Cerrado";
               }
               if(i == 2){
                   estado_titulo = "En Ejecución";
               }
               if(i == 1){
                   estado_titulo = "Aprobada";
               }
               if(i == 0){
                   estado_titulo = "Ingresada";
               }

                estado_comentario = item.comentario_orden_compra;
        
                html += '<li class="timeline-item"><div class="timeline-badge '+ colores[i] +'"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'+estado_titulo+'</h4><p><small class="text-muted"></small></p></div><div class="timeline-body"><p></p></div></div></li>';
           }
       }
       
       
    });
    $('#eventos-timeline').html(html);
}

function EliminarDocumentoFicha(id_documento, nombre_documento, fecha_registro, id, id_estado) {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_documento: id_documento,
            nombre_documento: nombre_documento,
            fecha_registro: fecha_registro
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {

            DocumentosFicha(id, id_estado);
            // $("#modal_nombre_contacto").text()
        },
        url: "Timeline/eliminarDocumentoFicha"
    });
}

function motivoRechazo(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Alumno", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosMotivoRechazo(result);
            $("#modal_motivo_rechazo").modal("show");
        },
        url: "Timeline/getMotivoRechazo"
    });

}

function cargaDatosMotivoRechazo(data) {
    //  console.log(data)
    $('#modal_id_ficha2').text(data[0].id_ficha);
    $('#motivo_rechazo').val(data[0].motivo_rechazo);
}