$(document).ready(function() {
    recargaLaPagina();
    cargaCbxAll();
    $('#fecha_inicio').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('#fecha_termino').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });



    //var hoy = new Date();

    $("#frm_ficha_registro").submit(function() {
        RegistraFicha();
        event.preventDefault();
    });


    $("#sala").prop("disabled", true);

    $("#id_solicitud_tablet").prop("disabled", true);


    $("#btn_reset").click(function() {
        limpiarFormulario();
    });

    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });

    $('#curso').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{ id: '', text: '' }]
    });

    $('#empresa').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{ id: '', text: '' }]
    });

    $('#relacionar_ficha_con').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{ id: '', text: '' }]
    });

    cargaEmpresas();

    $('#chk_sence').on('ifChecked', function(event) {
        cargaCodigoSenceCurso();
    });

    $("#btn_agregar_c").click(function() {
        $("#modal_agregar_contacto").modal('show');
        cargaComboTipoContacto();
        cargaComboNivelContacto();
    });


    $("#frm_nuevo_contacto_empresa").submit(function() {
        agregarcontacto();
        event.preventDefault();
    });

    $('#empresa').change(function() {
        if ($('#empresa').val() != "") {
            cargaDatosEmpresa();
            $("#btn_agregar_c").prop("disabled", false);
            $("#id_empresa_agregar").val($('#empresa').val());
        } else {
            $('#empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
            $("#btn_agregar_c").prop("disabled", true);
        }
    });

    $('#nombre_contacto_empresa').change(function() {
        $("#telefono_contacto_empresa").prop("disabled", false);
        $("#telefono_contacto_empresa").children('option').remove();
        seleccionarTlfContacto($("#nombre_contacto_empresa").val());
    });

    $('#curso').change(function() {
        if ($('#curso').val() != "") {
            $('#chk_sence').on('ifChecked', function(event) {
                cargaCodigoSenceCurso();
            });
            cargaVersionCurso();
        } else {
            $('#cod_sence').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
        }
        if ($('#ejecutivo').val() != "" && $('#modalidad').val() == 6 && $('#curso').val() != "") {
            $("#id_solicitud_tablet").html('');
            getSolicitudesTabletCBX($('#ejecutivo').val(), $('#curso').val());
            $("#id_solicitud_tablet").prop("disabled", false);

        } else {
            $("#id_solicitud_tablet").prop("disabled", true);
        }
    });

    $('#holding').change(function() {
        if ($('#holding').val() != "") {
            $('#empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
            cargaEmpresasByHolding();
        } else {
            $('#empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
        }
    });

    $('#cod_sence').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{ id: '', text: '' }]
    });

    $("#cod_sence").attr("requerido", false);
    $("#cod_sence").prop("disabled", true);
    $("#curso").prop("disabled", true);
    $("#version").prop("disabled", true);
    $("#version").attr("requerido", true);
    $('#version').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#direccion_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#nombre_contacto_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#telefono_contacto_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });

    $("#direccion_empresa").prop("disabled", true);
    $("#nombre_contacto_empresa").prop("disabled", true);
    $("#telefono_contacto_empresa").prop("disabled", true);

    contarCaracteres("comentario", "text-out", 1000);

    $('#txt_num_o_c').tagsinput({

        maxTags: 100,
        trimValue: true,
        maxChars: 20,
        allowDuplicates: false,
        preventPost: false
    });

    $('#chk_sin_orden_compra').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    }).on('ifChanged', function(e) { // Get the field name        

        var isChecked = e.currentTarget.checked;
        if (isChecked == true) {

            $('#txt_num_o_c').tagsinput('removeAll');
            $('#txt_num_o_c').addClass("disabled");
            //  $("#otic").val(1).trigger("change");
            //  $("#otic").prop("disabled", true);
            //$("#otic").addClass("disabled");
            $("#txt_num_o_c").prop("disabled", true);
            $("#txt_num_o_c").val("");
            $("#txt_num_o_c").attr("requerido", false);
            $('#chk_sin_orden_compra').val(1);

            //  cargaCodigoSenceCurso();
        } else {
            $("#txt_num_o_c").attr("requerido", true);
            // $("#otic").prop("disabled", false);
            // $("#otic").removeClass("disabled");
            $("#txt_num_o_c").prop("disabled", false);
            $("#txt_num_o_c").val("");
            $('#txt_num_o_c').tagsinput('focus');
            $('#chk_sin_orden_compra').val(0);
            //$('#chk_sence').iCheck('check');
            $('#chk_sin_orden_compra').iCheck('uncheck');
        }
    });

    $('#chk_sence').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    }).on('ifChanged', function(e) {

        var isChecked = e.currentTarget.checked;
        if (isChecked == true) {
            $("#txt_num_o_c").attr("requerido", false);
            $("#otic").prop("disabled", false);
            $("#otic").removeClass("disabled");
            // $("#txt_num_o_c").prop("disabled", false);
            $("#txt_num_o_c").attr("requerido", true);
            $("#txt_num_o_c").val("");
            $('#txt_num_o_c').tagsinput('focus');
            $("#cod_sence").attr("requerido", true);
            $("#cod_sence").prop("disabled", false);
            $('#chk_sence').val(1);
            $('#chk_sin_orden_compra').val(0);

            cargaCodigoSenceCurso();
        } else {
            $("#txt_num_o_c").attr("requerido", true);
            $('#txt_num_o_c').tagsinput('removeAll');
            $('#txt_num_o_c').addClass("disabled");
            $("#otic").val(1).trigger("change");
            $("#otic").prop("disabled", true);
            //  $("#txt_num_o_c").prop("disabled", true);
            $("#txt_num_o_c").val("");
            $("#txt_num_o_c").attr("requerido", false);
            $("#cod_sence").attr("requerido", false);
            $("#cod_sence").prop("disabled", true);
            $('#chk_sence').val(0);
            $('#chk_sin_orden_compra').val(1);
        }
    });

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('#modalidad').change(function() {
        if ($('#modalidad').val() != "") {

            JuegovalidacionCamposporModalidad();
            $("#curso").prop("disabled", false);
            $("#version").prop("disabled", false);
            //  habilitaCantidadOrdenCompra();
            cargaCurso();
        } else {
            $("#curso").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#curso").prop("disabled", true);
        }
    });

    $('#ejecutivo').change(function() {
        if ($('#ejecutivo').val() != "" && $('#modalidad').val() == 6 && $('#curso').val() != "") {
            $("#id_solicitud_tablet").html('');
            getSolicitudesTabletCBX($('#ejecutivo').val(), $('#curso').val());
            $("#id_solicitud_tablet").prop("disabled", false);

        } else {
            $("#id_solicitud_tablet").prop("disabled", true);
        }
    });

    $('#sede').change(function() {
        if ($('#sede').val() != "") {
            $("#sala").prop("disabled", false);
            cargaSalas();
            cargaLugarEjecucion();
        } else {
            $("#sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#sala").prop("disabled", true);
        }
        if ($('#sede').val() == "6") {
            $('#requiere_equipos_radiobutton').removeClass('hidden');
            $("#requiere_equipos_1").prop('required', true);
        } else {
            $('#requiere_equipos_radiobutton').addClass('hidden');
            $("#requiere_equipos_1").prop('required', false);
        }
    });

    $('#cod_sence').change(function() {
        if ($('#cod_sence').val() != "") {
            cargaModalidad();
        } else {
            $("#sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#sala").prop("disabled", true);
        }
    });
    $("#otic").prop("disabled", true);

});

function seleccionarTlfContacto(id_contacto) {
    $("telefono_contacto_empresa").html('');
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto: id_contacto
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "telefono_contacto_empresa");
        },
        url: "Agregar/getTelefonosContactoEmpresaCBX"
    });
}

function JuegovalidacionCamposporModalidad() {

    if ($('#modalidad').val() == 6) {

        $("#id_solicitd_tablet").prop("disabled", false);
        $("#id_solicitd_tablet").attr("requerido", true);

    } else {

        $("#id_solicitd_tablet").attr("requerido", false);
        $("#id_solicitd_tablet").val("");
        $("#id_solicitd_tablet").prop("disabled", true);

    }



}

function habilitaCantidadOrdenCompra() {

    /*
     * 
     * E-learning ,tablet , distancias , autoinstruccionales  solo pueden ttener una sola orden de compra 
     * 
     * 
     */

    $('#txt_num_o_c').tagsinput('removeAll');
    $('#txt_num_o_c').tagsinput('destroy');
    /*
     <option value="1">E-Learning Asíncrona</option>
     <option value="2">E-Learning - Presencial</option>
     <option value="3">E-Learning Sincrona</option>
     <option value="4">Presencial</option>
     <option value="6">A distancia</option>
     <option value="7">Sin especificar</option>
     <option value="8">Proyectos Sociales</option>
     <option value="9">Mineria</option>
     <option value="10">+Capaz</option> */


    switch ($('#modalidad').val()) {
        case "1":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });

            break;
        case "2":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;
        case "3":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;
        case "4":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;
        case "5":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;


        case "6":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;

        case "7":
            $('#txt_num_o_c').tagsinput({
                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;
        case "8":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;
        case "9":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;
        case "10":
            $('#txt_num_o_c').tagsinput({

                maxTags: 8,
                trimValue: true,
                maxChars: 20,
                allowDuplicates: false,
                preventPost: false
            });
            break;

    }




}

function cargaDatosEmpresa() {

    $('#direccion_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#nombre_contacto_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#telefono_contacto_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $("#direccion_empresa").prop("disabled", false);
    $("#nombre_contacto_empresa").prop("disabled", false);
    // $("#telefono_contacto_empresa").prop("disabled", false);


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            empresa: $('#empresa').val()
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "direccion_empresa");
        },
        url: "Agregar/getDireccionesEmpresaCBX"
    });



    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            empresa: $('#empresa').val()
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "nombre_contacto_empresa");
        },
        url: "Agregar/getNombreContactosEmpresaCBX"
    });


}

function cargaComboTipoContacto() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCBX("tipo_contacto_cbx", result);
            cargaCBX("tipo_contacto_cbx_edit", result);
        },
        url: "Agregar/getTipoContactoCBX"
    });
}

function cargaComboNivelContacto() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCBX("nivel_contacto_cbx", result);
            //cargaCBX("nivel_contacto_cbx_edit", result);
        },
        url: "Agregar/getNivelContactoCBX"
    });
}

function agregarcontacto() {
    console.log($('#empresa').val() + ',' + $('#nombre_contacto_agregar').val());
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#frm_nuevo_contacto_empresa").serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            alerta('Nuevo Contacto', errorThrown, 'error');
        },
        success: function(result) {
            console.log(result);
            alerta('Nuevo', result[0].mensaje, result[0].tipo_alerta);
            swal({
                title: 'Nuevo',
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar'
            }).then((result) => {
                $("#frm_nuevo_contacto_empresa")[0].reset();
                $('#modal_agregar_contacto').modal('hide');
                $("#empresa").trigger("change");
            })

        },
        url: "Agregar/guardarDatosContacto"
    });
}


function cargaVersionCurso() {


    $('#version').html('').select2({ data: [{ id: '', text: '' }] });


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { curso: $('#curso').val() },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {

            cargaCombo(result, "version");

        },
        url: "Agregar/getVersionCBX"
    });

}


function cargaModalidad() {



    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_code_sence: $('#cod_sence').val() },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            // console.log(result);
            $('#modalidad').val(result[0].id_modalidad);
            $('#modalidad').trigger("change");

            $('#curso').val(result[0].id_curso);
            $('#curso').trigger("change");
        },
        url: "Agregar/getModalidadByCodeSence"
    });


}



function cargaLugarEjecucion() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_sede: $('#sede').val() },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {

            if (result[0].id == 3 || result[0].id == 6) {

                $('#lugar_ejecucion').val("");
            } else {

                $('#lugar_ejecucion').val(result[0].text);
            }
        },
        url: "Agregar/getLugarEjecucion"
    });
}

function cargaCodigoSenceCurso() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_curso: $('#curso').val()
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            $('#cod_sence').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: result
            });
        },
        url: "Agregar/getCodigoSenceByCursoCBX"
    });
}

function limpiarFormulario() {
    location.reload();
}

function validacionesExtras() {
    var result = true;
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_termino = $("#fecha_termino").val();


    var res = fecha_inicio.split("-");
    fecha_inicio = res[2] + '-' + res[1] + '-' + res[0];
    res = fecha_termino.split("-");
    fecha_termino = res[2] + '-' + res[1] + '-' + res[0];

    var f = new Date();

    var mes = (f.getMonth() + 1);

    if (mes < 10) {
        mes = "0" + mes;
    }
    var dia = f.getDate();
    if (dia < 10) {
        dia = "0" + dia;
    }

    var f_actual = f.getFullYear() + "-" + mes + "-" + dia;

    if (fecha_inicio < f_actual) {
        alerta("Fecha", "Fecha Inicio no puede ser menor a la fecha Actual", "warning");
        result = false;
    }

    if (fecha_inicio > fecha_termino) {
        alerta("Fecha", "Fecha Inicio no puede ser mayor a la fecha de  fecha_termino", "warning");
        result = false;
    }

    /*
     if (fecha_cierre < fecha_termino) {
     alerta("Fecha", "Fecha Cierre no puede ser menor a la de Término", "warning");
     result = false;
     } else {
     
     result = true;
     }
     */


    return result;

}

function RegistraFicha() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    // console.log('imagen en base64: '+ document.getElementById("logo").value);

    var h_inicio = $('#txt_hora_inicio').val().split(':');
    var h_termino = $('#txt_hora_termino').val().split(':');

    var hoy = new Date();
    hoy.setHours(0, 0, 0, 0);
    var fech_inicial = $('#fecha_inicio').val().split('-');
    var fInicio = new Date(fech_inicial[1] + '-' + fech_inicial[0] + '-' + fech_inicial[2]);

    console.log("fi: " + fInicio);
    console.log("hoy: " + hoy);


    try {
        var h_inicio_date = (new Date(0, 0, 0, parseInt(h_inicio[0]), parseInt(h_inicio[1]), 0, 0)).getTime();
        var h_termino_date = (new Date(0, 0, 0, parseInt(h_termino[0]), parseInt(h_termino[1]), 0, 0)).getTime();
        console.log(h_inicio_date);
        console.log(h_termino_date);
    } catch (error) {
        alerta('Hora de inicio y fin', 'Debe ingresar una hora válida', 'error');
    } finally {
        if (h_inicio_date > h_termino_date) {
            alerta('Hora de inicio y fin', 'La hora de inicio debe ser menor a la hora de término', 'error');
        } else {
            if (isNaN(h_inicio_date) || isNaN(h_termino_date)) {
                alerta('Hora de inicio y fin', 'Debe ingresar una hora válida', 'error');
            } else {

                var f_inicio = $('#fecha_inicio').val().split('-');
                var f_termino = $('#fecha_termino').val().split('-');

                try {
                    var f_inicio_date = (new Date(parseInt(f_inicio[2]), parseInt(f_inicio[1]), parseInt(f_inicio[0]), 0, 0, 0)).getTime();
                    var f_termino_date = (new Date(parseInt(f_termino[2]), parseInt(f_termino[1]), parseInt(f_termino[0]), 0, 0, 0)).getTime();


                } catch (error) {
                    alerta('Fecha de inicio y fin', 'Debe ingresar una fecha válida', 'error');
                } finally {
                    if (f_inicio_date > f_termino_date) {

                        alerta('fecha de inicio y fin', 'La fecha de inicio debe ser menor a la fecha de término', 'error');
                    } else {

                        if (fInicio < hoy) {
                            alerta('fecha de inicio', 'La fecha de inicio no puede ser menor a la fecha actual', 'error');
                        } else {
                            if (isNaN(f_inicio_date) || isNaN(f_termino_date)) {
                                alerta('fecha de inicio y fin', 'Debe ingresar una fecha válida', 'error');
                            } else {
                                $.ajax({
                                    async: false,
                                    cache: false,
                                    dataType: "json",
                                    type: 'POST',
                                    data: $('#frm_ficha_registro').serialize(),
                                    error: function(jqXHR, textStatus, errorThrown) {
                                        // code en caso de error de la ejecucion del ajax
                                        //console.log(textStatus + errorThrown);
                                        alerta('Ficha', errorThrown, 'error');
                                    },
                                    success: function(resultado) {


                                        console.log(resultado);
                                        if (resultado[0].tipo_alerta != "warning") {

                                            swal({
                                                title: resultado[0].titulo,
                                                text: resultado[0].mensaje + ", Desea ir a cargar los Participantes? ",
                                                type: resultado[0].tipo_alerta,
                                                showCancelButton: true,
                                                confirmButtonColor: "#DD6B55",
                                                confirmButtonText: "Sí,Cargar Participantes!",
                                                cancelButtonText: "Más tarde!"
                                            }).then((result) => {
                                                location.href = "OrdenCompra/index/" + resultado[0].ficha_creada;
                                            }).catch(function(result) {
                                                history.back();
                                            })
                                        } else {

                                            alerta(resultado[0].titulo, resultado[0].mensaje, resultado[0].tipo_alerta);

                                            if (resultado[0].reset == 1) {
                                                $('#btn_reset').trigger("click");

                                            }
                                        }

                                    },
                                    url: "Agregar/Registra_ficha"
                                });
                            }
                        }

                    }
                }

            }

        }

    }
}

function cargaCurso() {

    $('#curso').html('').select2({ data: [{ id: '', text: '' }] });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            modalidad: $('#modalidad').val()
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "curso");
        },
        url: "Agregar/getCursoCBX"
    });

}



function cargaEmpresas() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //     data: {id_holding: $('#holding').val()},
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {

            cargaCombo(result, "empresa");
        },
        url: "Agregar/getEmpresaCBX"
    });

}


function cargaEmpresasByHolding() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_holding: $('#holding').val() },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {

            cargaCombo(result, "empresa");
        },
        url: "Agregar/getEmpresaCBXbyHolding"
    });

}


function cargaSalas() {
    $('#select_sala').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{ id: '', text: '' }]
    });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_sede: $('#sede').val() },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "sala");
        },
        url: "Agregar/getSalaCBX"
    });


}

function getSolicitudesTabletCBX(id_ejecutivo_comercial, id_curso) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ejecutivo_comercial: id_ejecutivo_comercial, id_curso: id_curso },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "id_solicitud_tablet");
        },
        url: "Agregar/getSolicitudesTabletCBX"
    });
}

function cargaCbxAll() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "cod_sence");
        },
        url: "Agregar/getCodeSenceCBX"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "holding");
        },
        url: "Agregar/getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "modalidad");
        },
        url: "Agregar/getModalidadCBX"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {

            cargaCombo(result, "otic");
        },
        url: "Agregar/getOticCBX"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "Agregar/getUsuarioTipoEcutivoComercial"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "sede");
        },
        url: "Agregar/getSedeCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "relacionar_ficha_con");
        },
        url: "Agregar/getFichasRelacionarCBX"
    });
}

function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();

    if ("otic" == item) {
        $("#otic option[value='0']").remove();
    }

    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");

}