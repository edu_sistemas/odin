$(document).ready(function () {
    recargaLaPagina();
    inicializaControles();
    cargaCbxAll();
    $("#modalLoading").css('display','none');
    $('#btn_aprobar_carga').prop("disabled", true);
    $('#btn_cancelar').prop("disabled", true);

    $("#btn_carga").click(function () {
        if (validarRequeridoForm($("#frm_carga_orden_compra"))) {
            //  $("#id_asignar_oc").trigger("click");

            validarBipartitoYSubir();
        }
    });

    $("#inlineCheckbox1").click(function () {
        if ($("#inlineCheckbox1").is(':checked')) {
            $("#inlineCheckbox2").prop('checked', false);
        } else {
            // $("#inlineCheckbox2").prop('checked', true);
        }
    });

    $("#inlineCheckbox2").click(function () {
        if ($("#inlineCheckbox2").is(':checked')) {
            $("#inlineCheckbox1").prop('checked', false);
        } else {
            //    $("#inlineCheckbox1").prop('checked', true);
        }
    });

    $("#precontrato").click(function () {
        var check = $('#precontrato').is(':checked');

        if (check) {
            $("#precontrato").val("1");
           
        } else {
            $("#precontrato").val("0");
           
        }

    });

    var precontrato = false;

    if ($("#precontrato").val() == 1) {
        precontrato = true;

    }


    $("#precontrato").prop("checked", precontrato);


    $("#cbx_orden_compra").change(function () {
        if ($("#cbx_orden_compra").val().trim() != "") {
            seleccionaEmpresa();
        } else {
            $("#empresa").val("").trigger("change");
        }
    });

    $("#id_asignar_oc").click(function () {

        if ($("#empresa").val().trim() == "") {
            $("#empresa").focus().after("<span id ='"
                    + $("#empresa").attr("name")
                    + "_error' style='font-weight:bold;color:#FF0000 !important;' class='error'>"
                    + $("#empresa").attr('mensaje')
                    + " </span>"
                    );
            setTimeout(function () {
                $("#" + $("#empresa").attr("name") + "_error").remove();

            }, 2000);

            return;

        } else {
            $("#" + $("#empresa").attr("name") + "_error").remove();
        }
        if ($("#cbx_orden_compra").val().trim() == "") {

            $("#cbx_orden_compra").focus().after("<span id ='"
                    + $("#cbx_orden_compra").attr("name")
                    + "_error' style='font-weight:bold;color:#FF0000 !important;' class='error'>"
                    + $("#cbx_orden_compra").attr('mensaje')
                    + " </span>"
                    );
            setTimeout(function () {
                $("#" + $("#cbx_orden_compra").attr("name") + "_error").remove();
            }, 2000);

            return;
        } else {
            $("#" + $("#cbx_orden_compra").attr("name") + "_error").remove();
        }


        if ($("#otic").val().trim() == "") {
            $("#otic").focus().after("<span id ='"
                    + $("#otic").attr("name")
                    + "_error' style='font-weight:bold;color:#FF0000 !important;' class='error'>"
                    + $("#otic").attr('mensaje')
                    + " </span>"
                    );

            setTimeout(function () {
                $("#" + $("#otic").attr("name") + "_error").remove();
            }, 2000);

            return;

        } else {
            $("#" + $("#otic").attr("name") + "_error").remove();
        }
        if (!$("#inlineCheckbox1").is(':checked') && !$("#inlineCheckbox2").is(':checked')) {
            $("#error_comite").focus().after("<span id ='"
                    + $("#inlineCheckbox1").attr("name")
                    + "_error' style='font-weight:bold;color:#FF0000 !important;' class='error'>"
                    + $("#inlineCheckbox1").attr('mensaje')
                    + " </span>"
                    );
            setTimeout(function () {
                $("#" + $("#inlineCheckbox1").attr("name") + "_error").remove();
            }, 2000);
            return;
        } else {
            $("#" + $("#inlineCheckbox1").attr("name") + "_error").remove();
        }
        ActualizarOrdenCompra();
    });

    $("#btn_cancelar").click(function () {
        swal({
            title: "Estas Seguro?",
            text: "Eliminará todos los registros previamente cargados",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy Seguro!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                limpia_carga();
                mostrar_lista_carga_orden_compra();
            }
        });
    });

    $("#btn_aprobar_carga").click(function () {

        swal({
            title: "Estas Seguro?",
            text: "Solo se cargaran los registros que no contiene errores",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: 'Si, Estoy Seguro!',
            cancelButtonText: "Cancelar!",
            showLoaderOnConfirm: true,
            preConfirm: function (email) {

                return new Promise(function (resolve, reject) {

                    setTimeout(function () {
                        aceptar_carga();

                        resolve();

                    }, 2000);
                });
            },
            allowOutsideClick: false
        }).then(function (email) {
            resetForm();

        })


    });

    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });


    $("#empresa").val($("#id_empresa_original").val()).trigger("change");
    $("#otic").val($("#id_otic_original").val()).trigger("change");
});




function ActualizarOrdenCompra() {
    var comite = 0;

    if ($("#inlineCheckbox2").is(':checked')) {
        comite = 1;
    }

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            cbx_orden_compra: $("#cbx_orden_compra").val(),
            id_empresa: $("#empresa").val(),
            id_otic: $("#otic").val(),
            aplica_bipartito: comite,
            precontrato: $("#precontrato").val(),

        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $("#id_asignar_oc").focus().after("<span id ='" + $("#id_asignar_oc").attr("name") + "_success' style='font-weight:bold;color:rgb(26,179,148) !important;' class='success'>" + result[0].mensaje + "</span>");
            setTimeout(function () {
                $("#" + $("#id_asignar_oc").attr("name") + "_success").remove();
            }, 4000);
        },
        url: "../setEmpresaByOC"
    });
}

function seleccionaEmpresa() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {cbx_orden_compra: $("#cbx_orden_compra").val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $("#otic").val(result[0].id_otic).trigger("change");
            $("#empresa").val(result[0].id_empresa).trigger("change");
           

            if (result[0].aplica_bipartito == 1 || result[0].aplica_bipartito == 0) {
                if (result[0].aplica_bipartito == 1) {
                    $("#inlineCheckbox2").prop('checked', true);
                    $("#inlineCheckbox1").prop('checked', false);
                } else {

                    $("#inlineCheckbox1").prop('checked', true);
                    $("#inlineCheckbox2").prop('checked', false);
                }

            }



        },
        url: "../getEmpresaByOC"
    });

}

function cargaCbxAll() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_holding: $('#id_holding').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "../getEmpresaCBXbyHolding"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "otic");
        },
        url: "../getOticCBX"
    });


}

function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
}

function resetForm() {
    $("#cbx_orden_compra").val('').change();
    $("#btn_cleanr_file").trigger("click");
    $('#btn_aprobar_carga').prop("disabled", true);
    $('#btn_cancelar').prop("disabled", true);

}

function validarBipartitoYSubir() {


    if ($("#empresa").val().trim() == "") {
        $("#empresa").focus().after("<span id ='"
                + $("#empresa").attr("name")
                + "_error' style='font-weight:bold;color:#FF0000 !important;' class='error'>"
                + $("#empresa").attr('mensaje')
                + " </span>"
                );
        setTimeout(function () {
            $("#" + $("#empresa").attr("name") + "_error").remove();

        }, 2000);

        return;

    } else {
        $("#" + $("#empresa").attr("name") + "_error").remove();
    }
    if ($("#cbx_orden_compra").val().trim() == "") {

        $("#cbx_orden_compra").focus().after("<span id ='"
                + $("#cbx_orden_compra").attr("name")
                + "_error' style='font-weight:bold;color:#FF0000 !important;' class='error'>"
                + $("#cbx_orden_compra").attr('mensaje')
                + " </span>"
                );
        setTimeout(function () {
            $("#" + $("#cbx_orden_compra").attr("name") + "_error").remove();
        }, 2000);

        return;
    } else {
        $("#" + $("#cbx_orden_compra").attr("name") + "_error").remove();
    }


    if ($("#otic").val().trim() == "") {
        $("#otic").focus().after("<span id ='"
                + $("#otic").attr("name")
                + "_error' style='font-weight:bold;color:#FF0000 !important;' class='error'>"
                + $("#otic").attr('mensaje')
                + " </span>"
                );

        setTimeout(function () {
            $("#" + $("#otic").attr("name") + "_error").remove();
        }, 2000);

        return;

    } else {
        $("#" + $("#otic").attr("name") + "_error").remove();
    }
    if (!$("#inlineCheckbox1").is(':checked') && !$("#inlineCheckbox2").is(':checked')) {
        $("#error_comite").focus().after("<span id ='"
                + $("#inlineCheckbox1").attr("name")
                + "_error' style='font-weight:bold;color:#FF0000 !important;' class='error'>"
                + $("#inlineCheckbox1").attr('mensaje')
                + " </span>"
                );
        setTimeout(function () {
            $("#" + $("#inlineCheckbox1").attr("name") + "_error").remove();
        }, 2000);
        return;
    } else {
        $("#" + $("#inlineCheckbox1").attr("name") + "_error").remove();
    }
    ActualizarOrdenCompra();

    $('#btn_carga').prop("disabled", true);
    // $('#file_carga_empresa').prop("disabled", true);
    limpia_carga();
    $('#btn_carga').html('<i class="fa fa-upload"></i> Cargando..');
    var l = $('#btn_carga').ladda();

    l.ladda('start');

    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_carga_orden_compra")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Alumnos', jqXHR.responseText, 'error');

        },
        success: function (result) {

            console.log(result);
            l.ladda('stop');
            $('#btn_carga').prop("disabled", false);
            $('#btn_carga').html('<i class="fa fa-upload"></i> Subir ');
            if (result.valido) {
                mostrar_lista_carga_orden_compra();
                $('#btn_aprobar_carga').prop("disabled", false);
                $('#btn_cancelar').prop("disabled", false);
                $('#file_carga_empresa').prop("disabled", false);
            } else {
                alerta('Orden de Compra', result.mensaje, 'error');
            }
        },
        url: '../cargar_archivo_datos_alumnos'
    });
}

function limpia_carga() {

    /*
     
     Descripcion : Limpia la tabla temporal antes de cargar el archivo excel
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (result) {

        },
        url: "../limpiar_carga_anterior"
    });
}

function  mostrar_lista_carga_orden_compra() {

    /*Muestra el docuemento cargado con mensaje dependiendo del tipo de validacion */

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Orden de Compra', jqXHR.responseText, 'error');
        },
        success: function (result) {

            cagar_tabla_temporal(result);
        },
        url: "../listar_carga_orden_de_compra"
    });
}

function cagar_tabla_temporal(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut_alumno"},
            {"mDataProp": "nombre_alumno"},
            {"mDataProp": "apellido_paterno"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "porcentaje_franquicia"},
//            {"mDataProp": "costo_otic"},
//            {"mDataProp": "costo_empresa"},
//            {"mDataProp": "valor_agregado"},
            {"mDataProp": "valor_cobrado"},
            {"mDataProp": "mensaje"}
        ],
        responsive: false

    });
}

function aceptar_carga() {

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: true,
        dataType: 'json',
        type: 'POST',
        data: {id: 654654},
        beforeSend: function () {
            $("#modalLoading").css("display", "block");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#modalLoading").css("display", "none");
            alerta('Orden Compra', jqXHR.responseText, 'error');
        },
        success: function (result) {
            $("#modalLoading").css("display", "none");
            limpia_carga();
            mostrar_lista_carga_orden_compra();
            swal({
                title: "Orden de Compra",
                text: "Alumnos Cargados!",
                type: "success"
            });
            
            
        },
        url: "../aceptar_carga_orden_compra_alumnos"
    });
}




function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}
