$(document).ready(function() {
    recargaLaPagina();

    $('#fecha_termino_nueva').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    var fecha_inicio_nueva = $('#fecha_inicio_nueva').val();
    var hoy = new Date();
    hoy.setHours(0, 0, 0, 0);

    fecha_inicio_nueva = fecha_inicio_nueva.split('-');
    var year_i = fecha_inicio_nueva[1];
    var month_i = fecha_inicio_nueva[0];
    var day_i = fecha_inicio_nueva[2];

    fecha_i_nueva = new Date(year_i + '-' + month_i + '-' + day_i);

    console.log(fecha_i_nueva);
    console.log(hoy);
    if (fecha_i_nueva < hoy) {
        $("#fecha_inicio_nueva").attr("readonly", "readonly");

    } else {

        $("#fecha_inicio_nueva").removeAttr("readonly");
        $('#fecha_inicio_nueva').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

    }



    inicializaControles();
    $('#btn_aceptar_extencion').click(function() {

        if (validarRequeridoForm($("#frm_carga_extencion_plazo"))) {
            if (validacionesExtras()) {
                EnviarExtencionPlazo();
            }
        }
    });

    contarCaracteres("comentario", "text-out", 200);

});

function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}


function validacionesExtras() {
    var result = true;
    var fecha_inicio_nueva = $("#fecha_inicio_nueva").val();
    var fecha_termino_antigua = $("#fecha_termino").val();

    var fecha_termino_nueva = $("#fecha_termino_nueva").val();

    var f = new Date();
    var mes = (f.getMonth() + 1);
    if (mes < 10) {
        mes = "0" + mes;
    }

    var dia = f.getDate();
    if (dia < 10) {
        dia = "0" + dia;
    }
    var temfecha_termino_nueva = fecha_termino_nueva.split('-');
    var temfecha_termino_antigua = fecha_termino_antigua.split('-');
    var fecha_termino_antigua = temfecha_termino_antigua[2] + "-" + temfecha_termino_antigua[1] + "-" + temfecha_termino_antigua[0];
    var fecha_termino_nueva = temfecha_termino_nueva[2] + "-" + temfecha_termino_nueva[1] + "-" + temfecha_termino_nueva[0];
    var f_actual = f.getFullYear() + "-" + mes + "-" + dia;

    if (fecha_termino_nueva <= f_actual) {
        alerta("Fecha", "Fecha de extensión no puede ser igual o menor a hoy", "warning");
        result = false;
    }

    if (fecha_termino_antigua == fecha_termino_nueva) {

        alerta("Fecha", "Fecha de extensión  no puede ser igual a fecha de término actual", "warning");
        result = false;
    }

    return result;

}


function EnviarExtencionPlazo() {

    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_carga_extencion_plazo")[0]),
        error: function(jqXHR, textStatus, errorThrown) {
            alerta('Extensión Plazo', jqXHR.responseText, 'error');
        },
        success: function(result) {
            enviaCorreo(result);

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {


                setTimeout(function() {
                    history.back();

                }, 2000);


            }
        },
        url: '../cargar_datos_extetencion_plazo'
    });


}

function enviaCorreo(data) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { datos: data },
        error: function(jqXHR, textStatus, errorThrown) {

        },
        success: function(result) {
            // cargaTablaFicha(result);
        },
        url: "../enviaEmail"
    });
}