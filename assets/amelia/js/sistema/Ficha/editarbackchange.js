var tem_id_orden_compra = "";
var tem_num_orden_compra = "";
var tem_otic_selected = "";
var id_curso_selected = "";

$(document).ready(function () {
    // recargaLaPagina(); NO SE USA
    cargaCbxAll();
    $("#btn_registrar").click(function () {
        if (validarRequeridoForm($("#frm_ficha_registro"))) {
            if (validacionesExtras()) {
                if ($("#span-tags-input").text() == "") {
                    swal("Error", "La ficha debe contener al menos 1 orden de compra", "error");
                } else {
                    EditarFicha();
                }
            }
        }
    });

    $("#btn_back").click(function () { // no se usa
        // retrocede una pagina
        limpiarFormulario();
    });

    contarCaracteres("comentario", "text-out", 1000);
    $("#comentario").trigger("change");

    $('#holding').change(function () {
        if ($('#holding').val() != "") {
            cargaEmpresas();
        } else {
            $('#empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
        }
    });

    $('#relacionar_ficha_con').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{ id: '', text: '' }]
    });

    $('#txt_num_o_c').tagsinput('add', $('#oculto_num_o_c').val()); // no deberia ir aqui se pierde

    var confirmed = true;

    $('#txt_num_o_c').on('beforeItemRemove', function (event) {
        var tag = event.item;
        var mis_ordenes = $('#oculto_num_o_c').val();
        var array_ordenes = mis_ordenes.split(",");
        //event.cancel = confirmed;

        if (array_ordenes.length <= 1) {
            swal({
                title: "Advertencia",
                text: "No puede eliminar la ultima Orden de compra",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Entiendo!",
                closeOnConfirm: true
            });
            //alert('asdasd');
            event.cancel = confirmed;
            return false;
        }
        event.cancel = confirmed;

        if (confirmed) {
            swal({
                title: "Estás seguro?",
                text: "Eliminarás todos los alumnos registrados en esta orden de compra",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminarlo!",
                cancelButtonText: "No, Cancelar!",
                cancelButtonColor: "#DD6B55",
                closeOnConfirm: true
            }, function (isConfirm) {
                confirmed = false;
                if (isConfirm) {

                    var mis_ordenes = $('#txt_num_o_c').val();
                    var array_ordenes = mis_ordenes.split(",");
                    var indice_num_oc = array_ordenes.indexOf(tag);

                    var mis_id_orden = $("#id_num_o_c").val();
                    var array_id_orden = mis_id_orden.split(",");
                    // obtengo el elemento que necesito eliminar
                    var id_orden_compra_eliminar = array_id_orden[indice_num_oc];
                    // borro el elemento del array
                    array_id_orden.splice(indice_num_oc, 1);
                    $('#txt_num_o_c').tagsinput('remove', tag);
                    // guardo los valores nuevos
                    $("#id_num_o_c").val(array_id_orden.toString());

                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {
                            id_ficha: $('#id_ficha').val(),
                            id_orden_compra: id_orden_compra_eliminar,
                            num_orden_compra: tag
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // code en caso de error de la ejecucion del ajax
                            //console.log(textStatus + errorThrown);
                            alerta('Ficha', errorThrown, 'error');
                        },
                        success: function (result) {

                            setTimeout(function () {
                                swal({
                                    title: result[0].titulo,
                                    text: result[0].mensaje,
                                    type: result[0].tipo_alerta,
                                    showCancelButton: false,
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                    function () {
                                        location.reload();
                                    });
                            }, 1000);
                        },
                        url: "../deleteOC"
                    });

                } else {
                    confirmed = true;
                }
            });
        } else {
            event.cancel = confirmed;
            confirmed = true;
        }

    });

    if ($("#oculto_num_o_c").val() == "" || $("#oculto_num_o_c").val() == "sin orden de compra335") {
        $('#chk_sin_orden_compra').iCheck('check');
        $('#txt_num_o_c').tagsinput('add', $('#oculto_num_o_c').val()); // jess para q muestre el sin orden compra 335
    }

    $('#chk_sin_orden_compra').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    }).on('ifChanged', function (e) {
        var isChecked = e.currentTarget.checked;
        if (isChecked == true) {
            tem_otic_selected = $("#otic").val();
            /*$("#otic").val(1).trigger("change");
             $("#otic").prop("disabled", true);
             */

            tem_id_orden_compra = $("#id_num_o_c").val();
            tem_num_orden_compra = $("#txt_num_o_c").val();
            // $('#txt_num_o_c').tagsinput('removeAll');

            // jessica
            //$("#otic").prop("disabled", false); // que tiene q ver?
            //$("#otic").removeClass("disabled"); // que tiene q ver?

            $("#txt_num_o_c").val("");
            $('#txt_num_o_c').tagsinput('removeAll');
            $('#txt_num_o_c').addClass("disabled");
            $("#txt_num_o_c").prop("disabled", true);
            $("#txt_num_o_c").attr("requerido", false);

            $("#cod_sence").attr("requerido", false);
            $("#cod_sence").prop("disabled", true);
            $("#cod_sence").val(25).trigger("change");

            $("#otic").attr("requerido", false);
            $("#otic").prop("disabled", true);
            $("#otic").val(1).trigger("change");

            $("#chk_sin_orden_compra").iCheck('check');
            $('#chk_sin_orden_compra').val(1);

            $('#chk_sence').prop("disabled", true);
            $("#chk_sence").iCheck('uncheck');
            $('#chk_sence').val(0); // no funciona

            // falta colocar la orden de compra generica

        } else {
            /*  $("#otic").val(1).trigger("change");
             $("#otic").prop("disabled", true);*/
            //  $("#txt_num_o_c").val("");
            // $("#otic").val(tem_otic_selected).trigger("change");
            //$("#txt_num_o_c").tagsinput('add', tem_num_orden_compra);

            // jessica
            //$("#id_num_o_c").val(tem_id_orden_compra);  // que tiene q ver?

            $("#txt_num_o_c").prop("disabled", false);
            $("#txt_num_o_c").attr("requerido", true);
            $('#chk_sin_orden_compra').val(0);

            $('#chk_sence').prop("disabled", false);
        }
    });

    $('#chk_sence').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    }).on('ifChanged', function (e) {
        var isChecked = e.currentTarget.checked;
        if (isChecked == true) {

            //$("#txt_num_o_c").val("");
            //alert(""+$("#txt_num_o_c").val()+"");


            // jessica 
            //  cargaCodigoSenceCurso(); //funcion q no tiene nada
            $('#txt_num_o_c').tagsinput('add', $('#oculto_num_o_c').val()); //traigo las ordenes de compra q estan cn el sence
            cargaOtic(); // sence y otic 
            cargaSence(); // sence y otic 
            $("#cod_sence").prop("disabled", false);
            $("#cod_sence").attr("requerido", true);
            $("#cod_sence").removeClass("disabled");

            $("#otic").prop("disabled", false);
            $("#otic").removeClass("disabled");
            $("#otic").attr("requerido", true);

            $("#txt_num_o_c").prop("disabled", false);
            $("#txt_num_o_c").attr("requerido", true);
            $("#txt_num_o_c").val($("#span-tags-input").text());
            $('#txt_num_o_c').tagsinput('focus');

            $('#chk_sin_orden_compra').prop("disabled", true);
            $("#chk_sin_orden_compra").iCheck('uncheck');
            $('#chk_sin_orden_compra').val(0);

            $("#chk_sence").iCheck('check');
            $('#chk_sence').val(1);

        } else {
            // jessica 

            $("#cod_sence").val(25).trigger("change");
            $("#cod_sence").attr("requerido", false);
            $("#cod_sence").prop("disabled", true);

            $("#otic").val(1).trigger("change");
            $("#otic").prop("disabled", true);
            $("#otic").attr("requerido", false);

            $('#txt_num_o_c').tagsinput('removeAll');
            $('#txt_num_o_c').addClass("disabled");
            $("#txt_num_o_c").val("");
            $("#txt_num_o_c").attr("requerido", false);

            $('#chk_sin_orden_compra').prop("disabled", false);
            $("#chk_sin_orden_compra").iCheck('check');
            $('#chk_sin_orden_compra').val(1);

            $('#chk_sence').val(0);
        }
    });

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#modalidad').change(function () {
        if ($('#modalidad').val() != "") {

            JuegovalidacionCamposporModalidad();

            $("#curso").prop("disabled", false);
            $("#version").prop("disabled", false);
            // habilitaCantidadOrdenCompra();
            cargaCurso();
        } else {
            $("#curso").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#curso").prop("disabled", true);
        }
    });

    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });

    inicializaControles();

    $('#sede').change(function () {
        if ($('#sede').val() != "") {
            $("#sala").prop("disabled", false);
            cargaSalas();
            cargaLugarEjecucion();
        } else {
            $("#sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#sala").prop("disabled", true);
        }
    });

    $('#cod_sence').change(function () {
        if ($('#cod_sence').val() != "") {

            cargaModalidad();

        } else {
            $("#sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#sala").prop("disabled", true);
        }
    });
    // ME PARECE Q NO SE USA 
    /*    $('#direccion_empresa').html('').select2({data: [{id: '', text: ''}], placeholder: "Seleccione"});
        $('#nombre_contacto_empresa').html('').select2({data: [{id: '', text: ''}], placeholder: "Seleccione"});
        $('#telefono_contacto_empresa').html('').select2({data: [{id: '', text: ''}], placeholder: "Seleccione"});
        $("#direccion_empresa").prop("disabled", true);
        $("#nombre_contacto_empresa").prop("disabled", true);
        $("#telefono_contacto_empresa").prop("disabled", true);
    */
    $('#empresa').change(function () {
        if ($('#empresa').val() != "" && $('#empresa').val() != null) {
            cargaDatosEmpresa();
        } else {
            $('#empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
        }
    });

    $('#nombre_contacto_empresa').change(function () {
        $("#telefono_contacto_empresa").prop("disabled", false);
        $("#telefono_contacto_empresa").children('option').remove();
        cargartlfcontacto();
    });

    $('#curso').change(function () {
        if ($('#curso').val() != "" && $('#curso').val() != null) {
            cargaVersionCurso();
        } else {
            $('#version').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
        }
    });
    // REPETIDO DE LA LINEA 48 q se pierde y se debe usar dependiendo de los check  $('#txt_num_o_c').tagsinput('add', $('#oculto_num_o_c').val());
});

function JuegovalidacionCamposporModalidad() {
    if ($('#modalidad').val() == 6) {
        $("#id_solicitd_tablet").prop("disabled", false);
        $("#id_solicitd_tablet").attr("requerido", true);
    } else {
        $("#id_solicitd_tablet").attr("requerido", false);
        $("#id_solicitd_tablet").val("");
        $("#id_solicitd_tablet").prop("disabled", true);
    }
}

function cargaDatosEmpresa() {
    $('#direccion_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#nombre_contacto_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#telefono_contacto_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $("#direccion_empresa").prop("disabled", false);
    $("#nombre_contacto_empresa").prop("disabled", false);
    $("#telefono_contacto_empresa").prop("disabled", false);

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            empresa: $('#empresa').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "direccion_empresa");
        },
        url: "../getDireccionesEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            empresa: $('#empresa').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "nombre_contacto_empresa");
            cargartlfcontacto();
        },
        url: "../getNombreContactosEmpresaCBX"
    });

    /*  $.ajax({
          async: true,
          cache: false,
          dataType: "json",
          type: 'POST',
          data: {
              id_contacto: $('#nombre_contacto_empresa').val()
          },
          error: function (jqXHR, textStatus, errorThrown) {
              // code en caso de error de la ejecucion del ajax
              //  console.log(textStatus + errorThrown);
          },
          success: function (result) {
              console.log(result + "telefono_contacto_empresa");
              cargaCombo(result, "telefono_contacto_empresa");
          },
          url: "../getTelefonosContactoEmpresaCBX"
      });
  */

}

function cargartlfcontacto() {
    console.log($('#nombre_contacto_empresa').val());
    $("telefono_contacto_empresa").html('');
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto: $('#nombre_contacto_empresa').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result + "telefono_contacto_empresa");
            cargaCombo(result, "telefono_contacto_empresa");
        },
        url: "../getTelefonosContactoEmpresaCBX"
    });
}
// NO SE USA LO BORRO??

function habilitaCantidadOrdenCompra() {

    /*
     * 
     * E-learning ,tablet , distancias , autoinstruccionales  solo pueden ttener una sola orden de compra 
     * 
     * 
     */


    /*
     <option value="1">E-Learning Asíncrona</option>
     <option value="2">E-Learning - Presencial</option>
     <option value="3">E-Learning Sincrona</option>
     <option value="4">Presencial</option>
     <option value="6">A distancia</option>
     <option value="7">Sin especificar</option>
     <option value="8">Proyectos Sociales</option>
     <option value="9">Mineria</option>
     <option value="10">+Capaz</option> */

    /* COMENTADO PORQ NO SE USA SE BORRA???
    
        switch ($('#modalidad').val()) {
            case "1" :
                var total = $('#txt_num_o_c').val().split(",");
                if (total.length > 1) {
                    $('#txt_num_o_c').tagsinput('removeAll');
                    $('#txt_num_o_c').tagsinput('destroy');
                }
                $('#txt_num_o_c').tagsinput('refreshdestroy');
                $('#txt_num_o_c').tagsinput({
                   maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "2" :
                var total = $('#txt_num_o_c').val().split(",");
                if (total.length > 1) {
                    $('#txt_num_o_c').tagsinput('removeAll');
                    $('#txt_num_o_c').tagsinput('destroy');
                }
                $('#txt_num_o_c').tagsinput('refresh');
                $('#txt_num_o_c').tagsinput({
                  maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "3" :
                var total = $('#txt_num_o_c').val().split(",");
                if (total.length > 1) {
                    $('#txt_num_o_c').tagsinput('removeAll');
                    $('#txt_num_o_c').tagsinput('destroy');
                }
                $('#txt_num_o_c').tagsinput('refresh');
                $('#txt_num_o_c').tagsinput({
                    maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "4" :
                $('#txt_num_o_c').tagsinput('refresh');
                $('#txt_num_o_c').tagsinput({
                   maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "5" :
                $('#txt_num_o_c').tagsinput('refresh');
                $('#txt_num_o_c').tagsinput({
                   maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "6" :
                var total = $('#txt_num_o_c').val().split(",");
                if (total.length > 1) {
                    $('#txt_num_o_c').tagsinput('removeAll');
                    $('#txt_num_o_c').tagsinput('destroy');
                }
                $('#txt_num_o_c').tagsinput('refresh');
                $('#txt_num_o_c').tagsinput({
                   maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "7" :
                $('#txt_num_o_c').tagsinput('refresh');
                $('#txt_num_o_c').tagsinput({
                   maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "8" :
                $('#txt_num_o_c').tagsinput({
                    maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "9" :
                $('#txt_num_o_c').tagsinput('refresh');
                $('#txt_num_o_c').tagsinput({
                   maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
            case "10" :
                $('#txt_num_o_c').tagsinput('refresh');
                $('#txt_num_o_c').tagsinput({
                   maxTags: 100,
                    trimValue: true,
                    maxChars: 20,
                    allowDuplicates: false,
                    preventPost: false
                });
                break;
    
        }
    */
}

function cargaModalidad() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_code_sence: $('#cod_sence').val() },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //cargaCombo(result, "modalidad");
            $('#modalidad').val(result[0].id_modalidad);

            $('#id_curso').val(result[0].id_curso);
            $('#modalidad').trigger("change");
            cargaCurso();
        },
        url: "../getModalidadByCodeSence"
    });


}

function cargaLugarEjecucion() {



    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_sede: $('#sede').val() },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            if (result[0].id == 3 || result[0].id == 6) {

                //  $('#lugar_ejecucion').val("");
            } else {

                $('#lugar_ejecucion').val(result[0].text);
            }
        },
        url: "../getLugarEjecucion"
    });
}

function cargaSalas() {

    $('#sala').html('').select2({ data: [{ id: '', text: '' }] });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_sede: $('#sede').val() },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "sala");
            //console.log(result);
        },
        url: "../getSalaCBX"
    });
}

function tieneSence() {
    console.log($("#cod_sence").val() + 'de dnd sale');
    if ($("#cod_sence").val() != "25") {
        $("#chk_sence").val(1);
        $("#chk_sence").iCheck('check');

        $("#cod_sence").prop("disabled", false);
        $("#cod_sence").attr("requerido", true);
        $("#cod_sence").removeClass("disabled");

        $("#otic").prop("disabled", false);
        $("#otic").removeClass("disabled");
        $("#otic").attr("requerido", true);

        $("#txt_num_o_c").prop("disabled", false);
        $("#txt_num_o_c").attr("requerido", true);
        $("#txt_num_o_c").val($("#span-tags-input").text());

        $('#chk_sin_orden_compra').prop("disabled", true);
        $("#chk_sin_orden_compra").iCheck('uncheck');
        $('#chk_sin_orden_compra').val(0);
        // cargaCodigoSenceCurso(); no tiene nada
    } else {
        $("#chk_sence").val(0);
        $("#chk_sence").iCheck('uncheck');

        $("#cod_sence").val(25).trigger("change");
        $("#cod_sence").attr("requerido", false);
        $("#cod_sence").prop("disabled", true);

        $("#otic").val(1).trigger("change");
        $("#otic").prop("disabled", true);
        $("#otic").attr("requerido", false);

        $('#txt_num_o_c').tagsinput('removeAll');
        $('#txt_num_o_c').addClass("disabled");
        $("#txt_num_o_c").val("");
        $("#txt_num_o_c").attr("requerido", false);

        $('#chk_sin_orden_compra').prop("disabled", false);
    }
}

function inicializaControles() {

    //$("#txt_num_o_c").val($("#oculto_num_o_c").val());
    // TODO ESTE IF NO SE USA NI SIRVE

    // TODO ESTE IF NO SE USA NI SIRVE
    /*  if ($("#chk_sin_orden_compra").val() != "0") {
          $("#chk_sin_orden_compra").val(1);
          $("#chk_sin_orden_compra").iCheck('check');
      }
  */
    tieneSence();
    var id_dias = $("#id_dias").val();

    var res = id_dias.split(",");

    for (var i = 0; i < res.length; i++) {

        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;

        }

    }



}
function cargaCodigoSenceCurso() {
    //    $.ajax({
    //        async: true,
    //        cache: false,
    //        dataType: "json",
    //        type: 'POST',
    //        data: {
    //            id_curso: $('#curso').val()
    //        },
    //        error: function (jqXHR, textStatus, errorThrown) {
    //            // code en caso de error de la ejecucion del ajax
    //            //  console.log(textStatus + errorThrown);
    //        },
    //        success: function (result) {
    //
    //            $('#cod_sence').select2({
    //                placeholder: "Seleccione",
    //                allowClear: true,
    //                data: result
    //            });
    //
    //            $("#cod_sence option[value='0']").remove();
    //            $("#cod_sence").val($("#id_cod_sence").val()).trigger("change");
    //
    //        },
    //        url: "../getCodigoSenceByCursoCBX"
    //    });
}

function limpiarFormulario() {
    location.reload();
}

function eliminarOC(ficha, oc, num_oc) {
    var resultado = false;
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: ficha,
            id_orden_compra: oc,
            num_orden_compra: num_oc
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonText: "OK",
                closeOnConfirm: false
            },
                function () {
                    location.reload();
                });
        },
        url: "../deleteOC"
    });
    return resultado;
}

function cargaVersionCurso() {
    $('#version').html('').select2({ data: [{ id: '', text: '' }] });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { curso: $('#curso').val() },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "version");
        },
        url: "../getVersionCBX"
    });
}

function cargaEmpresas() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_holding: $('#holding').val() },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "empresa");
        },
        url: "../getEmpresaCBX"
    });
}

function EditarFicha() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_ficha_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonText: "OK",
                closeOnConfirm: false
            },
                function () {
                    location.reload();
                });
            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }
        },
        url: "../Editar_ficha"
    });
}

function validacionesExtras() {
    var result = false;
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_termino = $("#fecha_termino").val();
    //  var fecha_cierre = $("#fecha_cierre").val();

    var res = fecha_inicio.split("-");
    fecha_inicio = res[2] + '-' + res[1] + '-' + res[0];
    res = fecha_termino.split("-");
    fecha_termino = res[2] + '-' + res[1] + '-' + res[0];

    var f = new Date();
    var mes = (f.getMonth() + 1);

    if (mes < 10) {
        mes = "0" + mes;
    }

    var dia = f.getDate();
    if (dia < 10) {
        dia = "0" + dia;
    }

    var f_actual = f.getFullYear() + "-" + mes + "-" + dia;
    if (fecha_inicio < f_actual) {
        alerta("Fecha", "Fecha Inicio no puede ser menor a la fecha Actual", "warning");
        result = false;
    } else {
        result = true;
    }

    if (fecha_inicio > fecha_termino) {
        alerta("Fecha", "Fecha Inicio no puede ser mayor a la fecha de  fecha_termino", "warning");
        result = false;
    } else {
        result = true;
    }
    return result;
}

function cargaCurso() {
    $('#curso').html('').select2({ data: [{ id: '', text: '' }] });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            modalidad: $('#modalidad').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaCombo(result, "curso");
        },
        url: "../getCursoCBX"
    });
}

function cargaSence() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "cod_sence");
            $("#cod_sence option[value='0']").remove();
            $("#cod_sence").val($("#id_codigo_sence").val()).trigger("change");
            tieneSence();
        },
        url: "../getCodeSenceCBX"
    });
}

function cargaOtic() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "otic");
        },
        url: "../getOticCBX"
    });
}

function cargaCbxAll() {
    $('#direccion_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#nombre_contacto_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $('#telefono_contacto_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
    $("#direccion_empresa").prop("disabled", false);
    $("#nombre_contacto_empresa").prop("disabled", false);
    $("#telefono_contacto_empresa").prop("disabled", false);

    cargaSence();

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "holding");
        },
        url: "../getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "empresa");
        },
        url: "../getEmpresaCBX"
    });

    cargaOtic();

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "modalidad");
            //$("#modalidad option[value='0']").remove();
            //$("#modalidad").val($("#id_modalidad").val()).trigger("change");
            cargaCurso();
            //habilitaCantidadOrdenCompra();
        },
        url: "../getModalidadCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "../getUsuarioTipoEcutivoComercial"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "sede");
        },
        url: "../getSedeCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "sala");
        },
        url: "../getSalaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "relacionar_ficha_con");
        },
        url: "../getFichasRelacionarCBX"
    });
}

function cargaCombo(midata, item) {
    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
    $("#" + item + " option[value='0']").remove();
   // $("#" + item).val($("#id_" + item).val()).trigger("change");
}
