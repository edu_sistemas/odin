$(document).ready(function () {
    recargaLaPagina();
    cargaCbxAll();
    inicializaControles();
    cargaSalas();
	
 
    $("#btn_registrar").click(function () {
        if (validarRequeridoForm($("#editar"))) {
            if (validacionesExtras()) {
                actualizarDatos();
                enviar();
            }
        }
    });

    $("#volver").click(function () {
        window.location.replace("../../Listar");
    });

    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });

    $('#sede').change(function () {
        if ($('#id_sede').val() != "") {
            cargaSalascambio();
            cargaLugarEjecucioncambio();
        } else {
            $("#sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
        }
    });
    contarCaracteres("comentario", "text-out", 1000);
});

function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}


function actualizarDatos() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#editar').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Ok"
                        //	closeOnConfirm: false
            },
                    function (isConfirm) {
                        if (isConfirm) {

                            location.href = "../../Listar";
                        }
                    }
            );

            if (result[0].reset == 1) {
                //  $('#volver').trigger("click")
            }
        },
        url: "../editarDatosRecurrentes"
    });
}

function validacionesExtras() {

    var result = true;
    if (!$('#lunes').is(':checked') && !$('#martes').is(':checked') && !$('#miercoles').is(':checked') &&
            !$('#jueves').is(':checked') && !$('#viernes').is(':checked') && !$('#sabado').is(':checked') && !$('#domingo').is(':checked')) {
        swal({
            title: 'Días del curso',
            text: 'Por lo menos debe marcar un dia',
            type: 'error'
        });
    }
    return result;
}

function cargaLugarEjecucion() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_sede: $('#id_sede').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            if (result[0].id == 3 || result[0].id == 6) {

                $('#lugar_ejecucion').val("");
            } else {

                $('#lugar_ejecucion').val(result[0].text);
            }
        },
        url: "../getLugarEjecucion"
    });
}

function cargaLugarEjecucioncambio() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_sede: $('#sede').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            if (result[0].id == 3 || result[0].id == 6) {

                $('#lugar_ejecucion').val("");
            } else {

                $('#lugar_ejecucion').val(result[0].text);
            }
        },
        url: "../getLugarEjecucion"
    });
}

function cargaCbxAll() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //cargaCombo(result, "sede");
            cargaCBXSelected("sede", result, $('#id_sede').val());
        },
        url: "../getSedeCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "relacionar_ficha_con");
        },
        url: "../getFichasRelacionarCBX"
    });
}

function cargaSalas() {

    $('#sala').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{
                id: '',
                text: ''
            }]
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_sede: $('#id_sede').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //cargaCombo(result, "sala");
            cargaCBXSelected("sala", result, $('#id_sala').val());
        },
        url: "../getSalaCBX"
    });
}

function cargaSalascambio() {

    $('#sala').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{
                id: '',
                text: ''
            }]
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_sede: $('#sede').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "sala");
        },
        url: "../getSalaCBX"
    });
}

function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    $("#" + item).val($("#id_" + item).val()).trigger("change");

    if ("otic" == item) {
        $("#otic option[value='0']").remove();
    }

}

function enviar() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: {datos: data},+
        data: $('#editar').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

        },
        url: "../enviaEmail"
    });
}