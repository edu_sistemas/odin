$(document).ready(function() {


    recargaLaPagina();

    cargaTodosDatosExtras();
    llamaDatosFicha();

    $('#data_5 .input-daterange').datepicker({
        format: "dd-mm-yyyy",
        language: 'es',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $("#btn_cerrarmodal_doc").on('click', function() {
        $('#modal_documentos_ficha').modal('hide');
        llamaDatosFicha();
    });


    $('#modal-principal').click(function(event) {
        event.stopPropagation();
    });


    $("#btn_nuevo").click(function() {
        location.href = 'Agregar';
    });

    $("#btn_arriendo").click(function() {
        location.href = 'Arriendo';
    });

    $("#btn_buscar_ficha").click(function() {
        llamaDatosFicha();
    });

    $("#cbx_holding").select2({
        placeholder: "Todos",
        allowClear: true
    });

    $("#holding").change(function() {
        if ($("#holding").val() != "") {
            cargaEmpresaBYHolding();
        } else {
            cargaTodosDatosExtras();
        }
    });

    $("#btn_cerrar_modal_docmento").click(function() {
        llamaDatosFicha();
    });


    $("#btn_nuevo_contacto").click(function() {
        $("#div_frm_add").css("display", "block");
        $("#div_tbl_contacto_empresa").css("display", "none");
        $("#btn_nuevo_contacto").css("display", "none");
    });

    $("#btn_guardar_contacto").click(function() {
        if (validarRequeridoForm($("#frm_nuevo_contacto_empresa"))) {
            RegistarNuevoContacto();
        }
    });

    $("#btn_insert_accion_sence").click(function() {
        if (validarRequeridoForm($("#frm_id_accion_sence"))) {
            ingresarIDAccionSence();
        }
    });

    $("#combo_decicion").change(function() {

        switch ($("#combo_decicion").val()) {
            case "1":
                $("#label_orden_compra").text("Nueva orden de Compra");
                $("#new_orden_compra").attr("requerido", false);
                $("#div_orden_compra").removeClass("hidden");
                $("#div_combo_oc").addClass("hidden");
                $("#btn_change_orden_compra").removeAttr("disabled");
                break;
            case "2":
                $("#label_orden_compra").text("Cambiar orden de Compra");
                $("#new_orden_compra").attr("requerido", true);
                $("#div_orden_compra").removeClass("hidden");
                $("#div_combo_oc").removeClass("hidden");
                $("#btn_change_orden_compra").removeAttr("disabled");
                break;
            case "3":
                $("#label_orden_compra").text("Eliminar orden de Compra");
                $("#new_orden_compra").attr("requerido", false);
                $("#div_orden_compra").addClass("hidden");
                $("#div_combo_oc").removeClass("hidden");
                $("#btn_change_orden_compra").removeAttr("disabled");
                break;
            default:
                $("#div_combo_oc").addClass("hidden");
                $("#new_orden_compra").attr("requerido", false);
                $("#label_orden_compra").text("Eliminar orden de Compra");
                $("#new_orden_compra").attr("requerido", false);
                $("#div_orden_compra").addClass("hidden");
                $("#btn_change_orden_compra").attr("disabled", "disabled");

                break;
        }

    });

    $("#btn_change_orden_compra").click(function(event) {

        if (validarRequeridoForm($("#frm_change_oc"))) {
            switch ($("#combo_decicion").val()) {
                case "1":
                    cambiarOrdenCompra();
                    break;
                case "2":
                    cambiarOrdenCompra();
                    break;
                case "3":
                    eliminarOrdenCompra();
                    break;
            }


        }
    });

    $("#btn_cancelar_contacto").click(function() {
        $("#div_frm_add").css("display", "none");
        $("#div_tbl_contacto_empresa").css("display", "block");
        $("#btn_nuevo_contacto").css("display", "block");
    });

    $("#fileupload").click(function(event) {
        $(function() {
            'use strict';
            $('#fileupload').fileupload({
                url: "Listar/upload_file",
                dataType: 'json',
                type: 'POST',
                cache: false,
                maxFileSize: 20000000,
                formData: {
                    id_ficha: $("#modal_id_ficha").text()
                },
                done: function(e, data) {
                    DocumentosFicha($("#modal_id_ficha").text(), idestado);
                },
                progressall: function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width', progress + '%');
                    $('#porsncetaje_carga').text(progress + '%');
                }
            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

        });
    });



    setInterval(function() {

        setTimeout(function() {
            // Amarillo
            $(".claseSemaforo").css("border-color", "#BD7700");
            $(".claseSemaforo").css("background-color", "#BD7700");

        }, 800);

        setTimeout(function() {

            // verde
            $(".claseSemaforo").css("border-color", "#1BB394");
            $(".claseSemaforo").css("background-color", "#1BB394");
        }, 450);

        setTimeout(function() {
            // rojo
            $(".claseSemaforo").css("border-color", "#B40019");
            $(".claseSemaforo").css("background-color", "#B40019");

        }, 200);

    }, 1200);

    $("#modalLoading").css('display', 'none');
});

function cargaEmpresaBYHolding() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_holding: $("#holding").val() },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function(result) {

            cargaCombo2(result);
        },
        url: "Listar/getEmpresaByHoldingCBX"
    });

}

function cargaTodosDatosExtras() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function(result) {

            cargaComboGenerico(result, 'ejecutivo');
        },
        url: "Listar/getEjecutivo"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function(result) {

            cargaComboGenerico(result, 'periodo');
        },
        url: "Listar/getPeriodo"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function(result) {

            cargaCombo(result);
        },
        url: "Listar/getEstadoCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown);
        },
        success: function(result) {

            cargaCombo2(result);
        },
        url: "Listar/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {

            cargaCombo3(result);
        },
        url: "Listar/getHoldingCBX"
    });

}

function cargaComboGenerico(midata, id) {

    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

    $("#" + id + " option[value='0']").remove();

}


function cargaCombo(midata) {
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true,
        data: midata
    });

    $("#estado option[value='0']").remove();
}

function cargaCombo2(midata) {
    $('#empresa').html('').select2({ data: [{ id: '', text: '' }] });
    $("#empresa").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#empresa option[value='0']").remove();
}

function cargaCombo3(midata) {

    $("#holding").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#holding option[value='0']").remove();

}


function validaPreEnvioFicha(ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: ficha },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            if (result[0].mensaje == "ok") {

                $.ajax({
                    async: true,
                    cache: false,
                    dataType: "json",
                    type: 'POST',
                    beforeSend: function() {
                        $("#modalLoading").css("display", "block");
                    },
                    complete: function() {
                        $("#modalLoading").css("display", "none");
                    },
                    data: { id_ficha: ficha },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $("#modalLoading").css("display", "none");
                        alerta("Ficha", jqXHR.responseText, "error");
                    },
                    success: function(result) {
                        $("#modalLoading").css("display", "none");
                        enviaCorreo(result);

                        if (result[0].requiere_equipos == 1) {
                            enviaCorreoRequiereEQuipos(ficha);
                        }
                        swal({
                                title: result[0].titulo,
                                text: result[0].mensaje,
                                type: result[0].tipo_alerta
                            },
                            function() {
                                location.reload();
                            });

                    },
                    url: "Listar/enviarFicha"
                });
                llamaDatosFicha();

            } else {
                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            }
        },
        url: "Listar/validaFicha"
    });
}

function validacionEliminar(ficha) {
    swal({
        title: "Estás seguro?",
        text: "Eliminarás toda la informacion cargada a la Ficha",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Eliminarlo!",
        cancelButtonText: "No, Cancelar!"
    }, function(isConfirm) {
        if (isConfirm) {
            DeleteFicha(ficha);
            llamaDatosFicha();
        }
    });
}

function validacionProcesoFacturacion(ficha) {


    swal({
        title: "Estás seguro?",
        text: "Notificarás al Área de Administración y Finanzas para que inicie el proceso de facturación y no podrás realizar más modificaciones sobre esta Ficha!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Facturar!",
        cancelButtonText: "No, Cancelar!"
    }, function(isConfirm) {
        if (isConfirm) {

            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_ficha: ficha
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    //console.log(textStatus + errorThrown);
                    alerta('Ficha', jqXHR.responseText, 'error');
                },
                success: function(result) {


                    enviaCorreoFacturacion(result);
                    setTimeout(Facturar, 1000);

                },
                url: "Listar/enviarFichaFacturacion"
            });

        }
    });
}

function DeleteFicha(ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: ficha
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', jqXHR.responseText, 'error');
        },
        success: function(result) {

            swal("Ficha", "Tu Ficha ha sido eliminada!", "success");
            llamaDatosFicha();
        },
        url: "Listar/eliminarFicha"
    });


}



function Facturar() {
    swal({
        title: "Ficha",
        text: "Tu Ficha ha sido enviada a Facturar!",
        type: "success",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "ok!",
        cancelButtonText: "No, Cancelar!"
    }, function(isConfirm) {
        llamaDatosFicha();
    });
}

var idestado = "";

function getFileAdjuntos() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#id_ficha").val()
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', jqXHR.responseText, 'error');
        },
        success: function(result) {
            cargaTbLAdjunto(result);
        },
        url: "Listar/getFileAdjunto"
    });
}

function cargaTbLAdjunto(data) {
    if ($.fn.dataTable.isDataTable('#tbl_adjuntos')) {
        $('#tbl_adjuntos').DataTable().destroy();
    }
    $('#tbl_adjuntos').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "num_doc" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    html = '<a href="../../' + o.ruta_ducumento + '" target="_blank">' + o.nombre_documento + '</a>';
                    return html;
                }
            },
            { "mDataProp": "fecha_registro" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var x = "'Editar/index/" + o.id_otic + "'";
                    return '<button class="btn-primary"  onclick="location.href=' + x + '">Editar</button>';
                }
            }
        ],
        pageLength: 5,
        responsive: true
    });
}

function llamaDatosFicha() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaFicha(result);
        },
        url: "Listar/buscarFicha"
    });
}

function enviaCorreo(data) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { datos: data },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            // cargaTablaFicha(result);
        },
        url: "Listar/enviaEmail"
    });
}

function enviaCorreoRequiereEQuipos(ficha) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: ficha },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            // cargaTablaFicha(result);
        },
        url: "Listar/enviaCorreoRequiereEQuipos"
    });
}

function enviaCorreoFacturacion(data) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { datos: data },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            // cargaTablaFicha(result);
        },
        url: "Listar/sendMailFacturacion"
    });
}

function cargaTablaFicha(data) {

    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }

    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [{
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {

                    var html = "";
                    var boton = "class='btn btn-warning dropdown-toggle'";
                    var a = "Resumen/index/" + o.id_ficha;
                    var b = "ResumenArriendo/index/" + o.id_ficha;
                    var c = "Adicionales/index/" + o.id_ficha;

                    var x = "Editar/index/" + o.id_ficha;
                    var y = "OrdenCompra/index/" + o.id_ficha;

                    var z = "EditarArriendo/index/" + o.id_ficha;
                    var w = "Rectificacion/index/" + o.id_ficha;

                    var extencionplazo = "ExtensionPlazo/index/" + o.id_ficha;

                    var a = "Resumen/index/" + o.id_ficha;
                    var rec = "Recurrente/index/" + o.id_ficha;
                    var b = "ResumenArriendo/index/" + o.id_ficha;


                    if (o.id_estado == 70) {

                        boton = "class='btn btn-black dropdown-toggle'";
                        html += "<div class='btn-group'>";
                        html += "<button data-toggle='dropdown' " + boton + ">Acción<span class='caret'></span></button>";
                        html += "<ul id='list_accion' class='dropdown-menu'>";
                        if (o.id_estado > 10) {
                            html += '<li><a href="../Timeline/Timeline/index/' + o.id_ficha + '"  /*onclick="abrirTimeline(' + o.id_ficha + ')" */  >REALTIME</a></li>';
                        }
                        if (o.id_categoria == 1) {
                            html += "<li><a href='" + a + "'>Resumen</a></li>";
                        }
                        if (o.id_categoria == 2) {
                            html += "<li><a href='" + b + "'>Resumen</a></li>";
                        }

                        html += "<li><a href='#' onclick='getMotivoAnulacion(" + o.id_ficha + "," + o.num_ficha + ");'>Ver motivo Anulación</a></li>";

                        html += "</ul>";
                        html += "</div>";
                    } else {

                        if (o.id_estado == 30 || o.id_estado == 50) {
                            boton = "class='btn btn-danger dropdown-toggle'";
                        }

                        if (o.id_estado == 20) {
                            boton = "class='btn btn-white dropdown-toggle'";
                        }

                        if (o.id_estado >= 40 && (o.id_estado != 50)) {
                            boton = "class='btn btn-primary dropdown-toggle'";
                        }

                        if (o.id_estado == 430) {
                            boton = "class='btn btn-warning dropdown-toggle'";
                        }


                        if (o.enviar_facturar != '') {
                            boton = "class='btn btn-primary dropdown-toggle claseSemaforo'";
                        }

                        html += "<div class='btn-group'>";
                        html += "<button data-toggle='dropdown' " + boton + ">Acción<span class='caret'></span></button>";
                        html += "<ul id='list_accion' class='dropdown-menu'>";

                        if (o.id_estado > 10) {
                            html += '<li><a href="../Timeline/Timeline/index/' + o.id_ficha + '"  /*onclick="abrirTimeline(' + o.id_ficha + ')" */  >REALTIME</a></li>';
                            html += "<li><a href='" + rec + "'>Editar</a></li>";
                        }
                        if (o.id_estado >= 60 && o.id_estado <= 420) {

                            html += "<li><a href='Anular/Index/" + o.id_ficha + "'>Anular</a></li>";
                        }



                        if (o.id_categoria == 1) {
                            html += "<li><a href='" + a + "'>Resumen</a></li>";

                        }
                        if (o.id_categoria == 2) {
                            html += "<li><a href='" + b + "'>Resumen</a></li>";

                        }


                        switch (parseInt(o.id_estado)) {
                            case 10:
                                if (o.id_categoria == 1) {
                                    html += '<li><a href="#"  onclick="validaPreEnvioFicha(' + o.id_ficha + ')"   >Enviar</a></li>';
                                    html += "<li><a href='" + x + "'>Editar</a></li>";
                                    html += "<li><a href='" + y + "'>Cargar / Editar Alumnos</a></li>";

                                }
                                if (o.id_categoria == 2) {
                                    html += '<li><a href="#"  onclick="validaPreEnvioFicha(' + o.id_ficha + ')"   >Enviar</a></li>';
                                    html += "<li><a href='" + z + "'>Editar</a></li>";
                                }
                                html += "<li><a href='" + c + "'>Costos/Servicios Adicionales</a></li>";
                                html += '<li class="divider"></li>';

                                break;
                            case 30:
                                if (o.id_categoria == 1) {
                                    html += '<li><a href="#"  onclick="validaPreEnvioFicha(' + o.id_ficha + ')"   >Enviar</a></li>';
                                    html += "<li><a href='" + rec + "'>Editar</a></li>";
                                    html += "<li><a href='" + y + "'>Cargar / Editar Alumnos</a></li>";
                                    html += "<li><a href='#' onclick='motivoRechazo(" + o.id_ficha + "," + o.num_ficha + ");'>Ver motivo Anulación</a></li>";
                                }

                                break;
                            case 50:
                                if (o.id_categoria == 1) {
                                    //                                    html += '<li><a href="#"  onclick="validaPreEnvioFicha(' + o.id_ficha + ')"   >Enviar</a></li>';
                                    // html += "<li><a href='" + x + "'>Editar</a></li>";

                                    html += "<li><a href='" + y + "'>Cargar / Editar Alumnos</a></li>";
                                    html += '<li class="divider"></li>';
                                    html += "<li><a href='#' onclick='changeOrdenCompra(" + o.id_ficha + ", " + o.num_ficha + ");'>Cambiar Orden Compra</a></li>";
                                }

                                if (o.id_categoria == 2) {
                                    html += "<li><a href='" + z + "'>Editar</a></li>";
                                }

                                html += "<li><a href='#' onclick='motivoRechazo(" + o.id_ficha + "," + o.num_ficha + ");'>Ver motivo corrección</a></li>";
                                break;
                        }

                        if (o.id_categoria == 1 && (o.id_estado >= 40)) {
                            html += "<li><a href='#' onclick='changeOrdenCompra(" + o.id_ficha + ", " + o.num_ficha + ");'>Cambiar Orden Compra</a></li>";
                            html += '<li class="divider"></li>';
                            html += "<li><a href='" + w + "'>Rectificación</a></li>";
                            html += "<li><a href='" + extencionplazo + "'>Extender Plazo</a></li>";
                            if (o.codigo_sence == null) {
                                html += "";
                            } else {
                                html += "<li><a href='#' onclick='setIdAccion(" + o.id_ficha + ", " + o.num_ficha + ");'>Ingresar ID Acción Sence</a></li>";
                            }
                        }

                        if (o.id_estado == 10) {
                            html += '<li><a href="#" onclick="validacionEliminar(' + o.id_ficha + ')" >Eliminar</a></li>';
                        }

                        // aparece esta opcion cuando una ficha llega a su cierre para que el ejecutivo  comercial envia a facurar las fichas   
                        if (o.enviar_facturar != '' & o.id_estado != 10) {
                            html += '<li><a href="#" onclick="validacionProcesoFacturacion(' + o.id_ficha + ')" >Envia a Facturación</a></li>';
                        }


                        html += "</ul>";
                        html += "</div>";


                    }
                    return html;
                }
            },
            { "mDataProp": "back_office" },

            { "mDataProp": "id_ficha" },

            /*  {"mDataProp": null, "bSortable": false, "mRender": function (o) {
             
             var html = "";
             html += '  <p data-toggle="tooltip" data-placement="top" title data-original-title="EJECUTIVO: ' + o.ejecutivo + ' • INGRESADA POR: ' + o.back_office + '" >' + o.back_office + '</p>';
             return html;
             
             }
             },*/
            { "mDataProp": "num_ficha" },
            { "mDataProp": "enviar_facturar", className: "hidden" },
            { "mDataProp": "categoria" },
            { "mDataProp": "modalidad" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    html += o.fecha_inicio + ' / ' + o.fecha_fin;
                    return html;
                }
            },
            { "mDataProp": "descripcion_producto" },
            { "mDataProp": "otic" },
            { "mDataProp": "empresa" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    html += '  <p data-toggle="tooltip" data-placement="top" title data-original-title="Sence: ' + o.valor_sence + ' • Empresa: ' + o.valor_empresa + '" >' + o.valor_total + '</p>';
                    return html;
                }
            },
            { "mDataProp": "estado" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    html = "<a href='#' title='Subir o editar documentos' onclick='DocumentosFicha(" + o.id_ficha + "," + o.id_estado + ");'>" + o.cant_documento + "</a>";
                    return html;
                }
            },
            { "mDataProp": "cant_alumno" }


            /*,
             {"mDataProp": null, "bSortable": false, "mRender": function (o) {
             var html = "";
             html = "<a href='#' title='Ver Alumnos' onclick='AlumnosFicha(" + o.id_ficha + ");'>" + o.cant_alumno + "</a>";
             html = o.cant_alumno;
             return html;
             }
             }*/

        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Edutecno'
        }, {
            extend: 'pdf',
            title: 'Edutecno'
        }, {
            extend: 'print',
            customize: function(win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }
        }],
        order: [
            [4, "desc"],
            [2, "desc"]
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function DocumentosFicha(id, id_estado) {
    $("#modal_id_ficha").text(id);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            cargaDatosDocumentoFicha(result, id_estado, id);
            $("#modal_documentos_ficha").modal("show");
            $("#div_frm_add").css("display", "none");
            $("#div_tbl_contacto_empresa").css("display", "block");
            $("#btn_nuevo_contacto").css("display", "block");
        },
        url: "Listar/verDocumentosFicha"
    });
}

function setIdAccion(id_ficha, num_ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id_ficha },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {

            $("#modal_id_sence").modal("show");
            $("#id_ficha_modal_accion").html(num_ficha);
            $("#id_ficha_s").val(id_ficha);
            $("#combo_oc").html("");
            $.each(result, function(i, item) {
                $("#combo_oc").append("<option value=" + item.id + ">" + item.text + "</option>");
            });

            if (result.length == 0) {
                $("#combo_oc").prop('disabled', true);
                $("#combo_oc").prop('disabled', true);
                $("#btn_insert_accion_sence").prop('disabled', true);
                alerta("ID Acción Sence", "los id acción sence ya se ingresaron o no existen ordenes de compras asosiadas a la ficha", "error");
            }

        },
        url: "Listar/getOcByFichaCBX"
    });
}

function changeOrdenCompra(id_ficha, num_ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id_ficha },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {

            $("#modal_change_oc").modal("show");
            $("#modal_change_oc #id_ficha_modal_accion").text(num_ficha);
            $("#modal_change_oc #id_ficha_s").val(id_ficha);
            $("#modal_change_oc #combo_oc").html("");
            $("#id_ficha_frm_change_oc").val(id_ficha);

            $.each(result, function(i, item) {
                $("#modal_change_oc #combo_oc").append("<option value=" + item.id + ">" + item.text + "</option>");
            });

            if (result.length == 0) {
                $("#modal_change_oc #combo_oc").prop('disabled', true);
                $("#modal_change_oc #combo_oc").prop('disabled', true);
                $("#modal_change_oc #btn_insert_accion_sence").prop('disabled', true);
                alerta("ID Acción Sence", "los id acción sence ya se ingresaron o no existen ordenes de compras asosiadas a la ficha", "error");
            }

        },
        url: "Listar/getOcByFichaCBX"
    });
}

function ingresarIDAccionSence() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_id_accion_sence').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
        },
        url: "Listar/setIDAccion"
    });
}

function cambiarOrdenCompra() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_change_oc').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#btn_cerrar_modal_orden_compra').trigger("click");
            // changeOrdenCompra($('#id_ficha_frm_change_oc').val(), $("#modal_change_oc #id_ficha_modal_accion").text());
            resetFormModalORdenCompra();

        },
        url: "Listar/SetcambiarOrdenCompra"
    });
}

function eliminarOrdenCompra() {

    swal({
        title: 'Estas Seguro que deseas Eliminar esta orden de compra?',
        text: "Se eliminarán los alumnos asociados a esta orden de compra!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Eliminar!",
        closeOnConfirm: false
    }, function() {
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: $('#frm_change_oc').serialize(),
            error: function(jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                alerta("Ficha", errorThrown, "error");
            },
            success: function(result) {
                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                $('#btn_cerrar_modal_orden_compra').trigger("click");
                //  changeOrdenCompra($('#id_ficha_frm_change_oc').val(), $("#modal_change_oc #id_ficha_modal_accion").text());
                resetFormModalORdenCompra();
                llamaDatosFicha();

            },
            url: "Listar/SetcambiarOrdenCompra"
        });
    });
}

function resetFormModalORdenCompra() {
    $('#frm_change_oc #combo_decicion').val('').trigger("change");
    $('#frm_change_oc #new_orden_compra').val('');
}

function cargaDatosDocumentoFicha(data, id_estado, id) {


    var html = "";
    $('#tbl_documentos_ficha tbody').html(html);
    $.each(data, function(i, item) {
        html += '        <tr>';
        if (item.nombre_documento % 2 == 0) {
            html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a></td> ';
        } else {
            html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a></td> ';
        }

        html += '   <td>' + item.fecha_registro + '</td> ';
        html += '<td>';
        var datos = "";
        datos = "'" + item.id_documento + "','" + item.nombre_documento + "','" + item.fecha_registro + "','" + id + "','" + id_estado + "'";
        if (id_estado == 10) {
            html += '<button   onclick="EliminarDocumentoFicha(' + datos + ')" class="btn btn-danger btn-sm" title="Eliminar" >Eliminar</button>';
        }
        html += '</td> ';
        html += '</tr>';
    });
    $('#tbl_documentos_ficha tbody').html(html);
}

function EliminarDocumentoFicha(id_documento, nombre_documento, fecha_registro, id, id_estado) {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_documento: id_documento,
            nombre_documento: nombre_documento,
            fecha_registro: fecha_registro
        },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {

            DocumentosFicha(id, id_estado);
            // $("#modal_nombre_contacto").text()
        },
        url: "Listar/eliminarDocumentoFicha"
    });
}

function motivoRechazo(id, num_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            cargaDatosMotivoRechazo(result, num_ficha);
            $("#modal_motivo_rechazo").modal("show");
        },
        url: "Listar/getMotivoRechazo"
    });

}

function getMotivoAnulacion(id, num_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            cargaDatosMotivoRechazo(result, num_ficha);
            $("#modal_motivo_rechazo").modal("show");
        },
        url: "Listar/getMotivoAnulacion"
    });

}



function cargaDatosMotivoRechazo(data, num_ficha) {

    $('#modal_id_ficha2').text(num_ficha);
    $('#motivo_rechazo').val(data[0].motivo_rechazo);
}

//-------------------JS PARA MODAL TIMELINE --------------------------

function abrirTimeline(id_ficha) {
    $("#modal-timeline").modal("show");
    $("ficha").text(id_ficha);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id_ficha },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            cargaDatosTimeline(result);
            getLogFicha(id_ficha);
        },
        url: "Listar/getEventosFicha"
    });
}

function getLogFicha(id_ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id_ficha },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            cargaTablaLog(result);
        },
        url: "Listar/getLogFicha"
    });
}

function cargaTablaLog(data) {
    var html = "";
    $('#tbl_log_timeline tbody').html(html);
    $.each(data, function(i, item) {
        html += '<tr><th>';
        html += i + 1;
        html += '</th><th>';
        html += item.fecha;
        html += '</th><td>';
        html += item.accion;
        html += '</td><td>';
        if (item.usuario == "No hay Registros") {
            item.usuario = "--";
        }
        html += item.usuario;
        html += '</td></tr>';
    });
    $('#tbl_log_timeline tbody').html(html);
}

function cargaDatosTimeline(data) {


    var html = "";
    $('#tb_resumen_ficha').html(html);
    $.each(data, function(i, item) {
        html += '<tr><th>Holding:</th>';
        html += '<td>';
        html += item.holding;
        html += '<td>';

        html += '<th>Empresa:</th>';
        html += '<td>';
        html += item.empresa;
        html += '<td></tr>';

        html += '<tr><th>Rut:</th>';
        html += '<td>';
        html += item.rut_empresa;
        html += '<td>';

        html += '<th>Curso:</th>';
        html += '<td>';
        html += item.descripcion_producto;
        html += '<td></tr>';

        html += '<tr><th>Versión:</th>';
        html += '<td>';
        html += item.descripcion_version;
        html += '<td>';

        html += '<th>Modalidad:</th>';
        html += '<td>';
        html += item.modalidad;
        html += '<td></tr>';

        html += '<tr><th>Código Sence:</th>';
        html += '<td>';
        html += item.codigo_sence;
        html += '<td>';

        html += '<th>Fecha inicio:</th>';
        html += '<td>';
        html += item.fecha_inicio;
        html += '<td></tr>';

        html += '<tr><th>Fecha Término:</th>';
        html += '<td>';
        html += item.fecha_fin;
        html += '<td>';

        html += '<th>Horario:</th>';
        html += '<td>';
        html += item.hora_inicio + ' a ' + item.hora_termino;
        html += '<td></tr>';

        html += '<tr><th>Sede:</th>';
        html += '<td>';
        html += item.nombre_sede;
        html += '<td>';

        html += '<th>Sala:</th>';
        html += '<td>';
        html += item.nombre_sala;
        html += '<td></tr>';

        html += '<tr><th>Dirección:</th>';
        html += '<td>';
        html += item.direccion_sede;
        html += '<td>';

        html += '<th>Días:</th>';
        html += '<td>';
        html += item.dias;
        html += '<td></tr>';

        html += '<tr><th>Ejecutivo Comercial:</th>';
        html += '<td>';
        html += item.ejecutivo;
        html += '<td>';

        html += '<th>Observaciones:</th>';
        html += '<td>';
        html += item.comentario_orden_compra;
        html += '<td></tr>';


    });
    $('#tb_resumen_ficha').html(html);


    html = "";
    $('#eventos-timeline').html(html);
    $.each(data, function(i, item) {

        var estado_titulo, estado_comentario, fecha;
        var colores = ["success", "secondary"];

        if (parseInt(item.estado_timeline) >= 1) {
            for (var i = 0; i < parseInt(item.estado_timeline); i++) {
                if (i == 5) {
                    estado_titulo = "Facturado";
                    fecha = item.fecha_facturado_estado;
                }
                if (i == 4) {
                    estado_titulo = "En proceso de Facturación";
                    fecha = item.fecha_cierre_estado;
                }
                if (i == 3) {
                    estado_titulo = "Cerrado";
                    fecha = item.fecha_cierre_estado;
                }
                if (i == 2) {
                    estado_titulo = "En Ejecución";
                    fecha = item.fecha_inicio_estado;
                }
                if (i == 1) {
                    estado_titulo = "Aprobada";
                    fecha = item.fecha_apb_publicacion;
                }
                if (i == 0) {
                    estado_titulo = "Ingresada";
                    fecha = item.fecha_ingreso_estado;
                }

                //estado_comentario = item.comentario_orden_compra;


                html += '<li class="timeline-item"><div class="timeline-badge ' + colores[0] + '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">' + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i>' + fecha + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
            }

            for (var i = parseInt(item.estado_timeline); i < 6; i++) {
                if (i == 5) {
                    estado_titulo = "Facturado";
                }
                if (i == 4) {
                    estado_titulo = "En proceso de Facturación";
                }
                if (i == 3) {
                    estado_titulo = "Cerrado";
                }
                if (i == 2) {
                    estado_titulo = "En Ejecución";
                }
                if (i == 1) {
                    estado_titulo = "Aprobada";
                }
                if (i == 0) {
                    estado_titulo = "Ingresada";
                }

                //estado_comentario = item.comentario_orden_compra;

                html += '<li class="timeline-item"><div class="timeline-badge ' + colores[1] + '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">' + estado_titulo + '</h4><p><small class="text-muted"></small></p></div><div class="timeline-body"><p></p></div></div></li>';
            }


        }


    });
    $('#eventos-timeline').html(html);
}