var itemsFichaCBX = [];
$( document ).ready(function() {
    cargaItemsFichaCBX();
    $('#frm_agregar_item').submit(function(){
        agregarItem();
        event.preventDefault();
    });
    $('#frm_editar_item').submit(function(){
        editarItem();
        event.preventDefault();
    });
    
});

function cargaItemsFichaCBX(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("id_item", result);
            itemsFichaCBX = result;
        },
        url: "../getAdicionalesFichaCBX"
    });
}

function agregarItem(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#frm_agregar_item").serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            location.reload();
        },
        url: "../setItemFicha"
    });
}

function editarItem(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $("#frm_editar_item").serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            location.reload();
        },
        url: "../editarItemFicha"
    });
}

function eliminarItem(id_item){
    swal({
        title: 'Estás seguro(a)?',
        text: "Esta acción no se puede deshacer",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Borrar',
        cancelButtonText: 'Borrar'
      }).then(function () {
        // swal(
        //   'Deleted!',
        //   'Your file has been deleted.',
        //   'success'
        // )
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_item: id_item},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                location.reload();
            },
            url: "../eliminarItemFicha"
        });
      });
}

function modalEditarItem(id_item){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_item: id_item},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('#id_item_ficha_editar').val(id_item);
            cargaCBXSelected('id_item_editar', itemsFichaCBX, result[0].id_item_ficha); 
            $('#detalle_editar').text(result[0].observaciones);
            $('#valor_editar').val(result[0].valor);
            $('#modalEditar').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#modalEditar').modal('show');
        },
        url: "../getItemFicha"
    });
}