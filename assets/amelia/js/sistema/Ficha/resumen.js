$(document).ready(function () {
    recargaLaPagina();
    cargaCbxAll();
    $("#btn_back").click(function () {
        // retrocede una pagina
        limpiarFormulario();
        // history.back();
    });

    contarCaracteres("comentario", "text-out", 1000);
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $("#cbx_orden_compra").change(function () {
        var id_ficha = "";
        var id_oc = "";
        id_ficha = $("#id_ficha").val();
        id_oc = $("#cbx_orden_compra").val();

        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {ficha: id_ficha, oc: id_oc},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                cargaTablaDatosAlumnos(result);
            },
            url: "../cargaDatosOC"
        });
    });
    selectIdEdutecno();
    inicializaControles();

//    $("#btn_aceptar").click(function () {
//        var ficha = $("#id_ficha").val();
//        validaPreEnvioFicha(ficha);
//    });

    $("#btn_aceptar").click(function () {
        if (validarRequeridoForm($("#frm_respuesta_enviar"))) {

            var ficha = $("#id_ficha").val();
            var respuesta = $("textarea#correccion").val();
            var rechazo = $("#id_motivo_rechazo").val();


            //console.log(respuesta);
            validaPreEnvioFicha(ficha, respuesta, rechazo);

        }
    });

    $("#btn_solo_enviar").click(function () {

        var ficha = $("#id_ficha").val();
        var respuesta = $("textarea#correccion").val('0');
        var rechazo = $("#id_motivo_rechazo").val('0');
        //console.log(validaPreEnvioFicha);

        validaPreEnvioFicha(ficha, respuesta, rechazo);

    });


    $("#btn_enviar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        abrirRechazo(ficha);
    });

    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });

    cargaAdicionales();


    listarFacturas();

    listarnc();

    listarnd();
});

function abrirRechazo(id) {
    $("#modal_id_ficha_rechazo").text($("#num_ficha").val());
    //$("#id_ficha_rechazo").val(id);
    $("#modal_rechazo").modal("show");
}

function selectIdEdutecno() {
    var id_ficha = $('#id_ficha').val();
    var id_edutecno = $('#id_edutecno').val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
//        	$("#modal_edutecno").modal("show");
//        	$("#modal_id_ficha2").html(id_ficha);
//        	$("#id_ficha_edutecno").val(id_ficha);

            cargaCBXSelected('edutecno_cbx', result, $("#id_edutecno_original").val());

        },
        url: "../getidEdutecnoCBX"
    });
}

function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() {


    var id_ficha = "";
    var id_oc = "";
    id_ficha = $("#id_ficha").val();
    id_oc = $("#cbx_orden_compra").val();
    /* Informacion actual */
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosAlumnos(result);
        },
        url: "../cargaDatosOC"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../cargaDatosOCDocumentoResumen"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaLastDocumentos(result);
        },
        url: "../cargaLastDocumentoResumen"
    });

    /* End  Informacion actual */


    /* Rectificaciones */
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaRectficacionesAlumnos(result);
        },
        url: "../getRectificacionesAlumnosbyFicha"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentosRectificacione(result);
        },
        url: "../getRectificacionesAlumnosDocbyFicha"
    });

    /* End Rectificaciones */

    /* Extensiones */

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaRectficacionesExtensiones(result);
        },
        url: "../getRectificacionesExtensionbyFicha"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentosExtensiones(result);
        },
        url: "../getRectificacionesExtensionDocbyFicha"
    });
    /* END  Extensiones */
}

/* Datos Actuales */
function cargaTablaDatosAlumnos(data) {

    if (data.length > 0) {
        $("#total_alumnos").html(data[0].total_alumnos);
        $("#valor_final").html(data[0].valor_final);
        $("#total_sence").html(data[0].total_sence);
        $("#total_empresa").html(data[0].total_empresa);
        $("#total_sence_empresa").html(data[0].total_sence_empresa);
        $("#total_agregado").html(data[0].total_agregado);
        $("#total_becados").html(data[0].total_becados);
        $("#total_oc").html(data[0].total_oc);
    }

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }



    $('#tbl_orden_compra_carga_alumno').DataTable({
        initComplete: function () {
            this.api().columns([5]).every(function () {
                var column = this;
                var select = $('<select><option value="">Filtrar Por OC</option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.cells('', column[0]).render('display').sort().unique().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        },
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut"},
            {"mDataProp": "nombre"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "fq"},
//            {"mDataProp": "valor_otic"},
//            {"mDataProp": "valor_empresa"},
//            {"mDataProp": "valor_agregado"},
            {"mDataProp": "valor_total"},
            {"mDataProp": "num_orden_compra"}
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            {"mDataProp": "fecha_registro"}
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"clear">',
        order: [2, "desc"]

    });

    $('[data-toggle="tooltip"]').tooltip();

}


function cargaTablaLastDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_last_documetnos')) {
        $('#tbl_orden_compra_last_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_last_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            // {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            {"mDataProp": "fecha_registro"}

        ],
        pageLength: 10,
        responsive: false,
        dom: '<"clear">',
        order: [1, "desc"]

    });

    $('[data-toggle="tooltip"]').tooltip();

}
/* end Datos Actuales */


/* Rectificaciones */
function cargaTablaRectficacionesAlumnos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno_rectificacion')) {
        $('#tbl_orden_compra_carga_alumno_rectificacion').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno_rectificacion').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_rectificacion"},
            {"mDataProp": "rut"},
            {"mDataProp": "nombre_alumno"},
            {"mDataProp": "apellido_paterno"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "porcentaje_franquicia"},
//            {"mDataProp": "costo_otic"},
//            {"mDataProp": "costo_empresa"},
//            {"mDataProp": "valor_agregado"},
            {"mDataProp": "valor_total"},
            {"mDataProp": "causa"},
            {"mDataProp": "fecha_rectificacion"},
            {"mDataProp": "estado"}
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentosRectificacione(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documentos_rectificaciones')) {
        $('#tbl_orden_compra_documentos_rectificaciones').DataTable().destroy();
    }

    $('#tbl_orden_compra_documentos_rectificaciones').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            {"mDataProp": "fecha_registro"}
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"clear">',
        order: [2, "desc"]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

/* End Rectificaciones */


/* extensiones */

function cargaTablaRectficacionesExtensiones(data) {

    if ($.fn.dataTable.isDataTable('#tbl_rectificacion_extension_plazo')) {
        $('#tbl_rectificacion_extension_plazo').DataTable().destroy();
    }

    $('#tbl_rectificacion_extension_plazo').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_rectificacion"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "fecha_cierre"},
            {"mDataProp": "fecha_extension"},
            {"mDataProp": "num_orden_compra"},
            {"mDataProp": "fecha_rectificacion"},
            {"mDataProp": "estado"}

        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [5, "desc"]
    });

    $('[data-toggle="tooltip"]').tooltip();

}
function cargaTablaDatoDocumentosExtensiones(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documentos_extensiones')) {
        $('#tbl_orden_compra_documentos_extensiones').DataTable().destroy();
    }

    $('#tbl_orden_compra_documentos_extensiones').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            {"mDataProp": "fecha_registro"}
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"clear">',
        order: [2, "desc"]

    });

    $('[data-toggle="tooltip"]').tooltip();

}

/* End extensiones */




function listarFacturas() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { num_ficha: $('#num_ficha').val() },
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaFacturas(result);
        },
        url: "../listarfacturas"
    });
}

function listarnc() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { num_ficha: $('#num_ficha').val() },
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablanc(result);
        },
        url: "../listarnc"
    });
}
function listarnd() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { num_ficha: $('#num_ficha').val() },
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTabland(result);
        },
        url: "../listarnd"
    });
}


function cargaTablanc(data) {

    if ($.fn.dataTable.isDataTable('#tbl_notas_creditos')) {
        $('#tbl_notas_creditos').DataTable().destroy();
    }


    $('#tbl_notas_creditos').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "fecha_emision" },
            { "mDataProp": "n_factura" },

            { "mDataProp": "num_nota_credito" },

            { "mDataProp": "monto_credito" },
            { "mDataProp": "tipo_facturacion" },

            { "mDataProp": "tipo_entrega" }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                }
            }
        ],
        order: [1, 'desc']
    });
}

function cargaTabland(data) {

    if ($.fn.dataTable.isDataTable('#tbl_nota_debito')) {
        $('#tbl_nota_debito').DataTable().destroy();
    }


    $('#tbl_nota_debito').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "fecha_emision" },
            { "mDataProp": "n_factura" },

            { "mDataProp": "num_nota_debito" },

            { "mDataProp": "monto_debito" },
            { "mDataProp": "tipo_facturacion" },

            { "mDataProp": "tipo_entrega" }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                }
            }
        ],
        order: [1, 'desc']
    });
}

function cargaTablaFacturas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_facturas')) {
        $('#tbl_facturas').DataTable().destroy();
    }


    $('#tbl_facturas').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "tipo_facturacion" },
            { "mDataProp": "n_factura" },

            { "mDataProp": "monto_factura" },

            { "mDataProp": "nombre_estado" },
            { "mDataProp": "tipo_entrega" },

            { "mDataProp": "num_orden_compra" },
            { "mDataProp": "razon_social_empresa" },
            { "mDataProp": "nombre_otic" }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                }
            }
        ],
        order: [1, 'desc']
    });
}



function validaPreEnvioFicha(ficha, respuesta, rechazo) {
    $('#modal_rechazo').modal('toggle');
    // event.preventDefault();

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: ficha, respuesta: respuesta, rechazo: rechazo},

        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {

            if (result[0].mensaje == "ok") {

                aceptar(ficha);

            } else {
                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            }

        },
        url: "../validaFicha"
    });
}
function aceptar(ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", jqXHR.textResponse, "error");
        },
        success: function (result) {
            enviaCorreo(result);
            if (result[0].requiere_equipos == 1) {
                enviaCorreoRequiereEQuipos(ficha);
            }
            //  location.reload();

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            setTimeout(function () {

                history.back();
            }, 2000);


            /* swal({
             title: result[0].titulo,
             text: result[0].mensaje,
             type: result[0].tipo_alerta
             }, function () {
             enviaCorreo(result);
             
             //  
             });*/


            // aceptar(result);

        },
        url: "../enviarFicha"
    });

}
function enviaCorreoRequiereEQuipos(ficha) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "../enviaCorreoRequiereEQuipos"
    });
}
function enviaCorreo(data) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {datos: data},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "../enviaEmail"
    });
}

function cargaAdicionales() {
    var id_ficha = $('#id_ficha').val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            var contenido = '';
            $('#tbl_adicionales tbody').html('');
            $.each(result, function (i, item) {
                if (item.nombre == null) {
                    contenido += '';
                } else {
                    contenido += '<tr><td>';
                    contenido += item.nombre;
                    contenido += '</td><td>';
                    contenido += item.valor_formateado;
                    contenido += '</td></tr>';
                }
            });
            $('#tbl_adicionales tbody').html(contenido);
            contenido = "";
            $('#total_adicionales').text(result[0].valor_total);
        },
        url: "../getAdicionalesFicha"
    });

}