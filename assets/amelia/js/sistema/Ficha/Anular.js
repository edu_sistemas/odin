$(document).ready(function () {
    recargaLaPagina();
    inicializaControles();
    $("#btn_enviar").click(function () {

        var tr = $('#tbl_documentos_ficha > tbody').find('tr').length;
        if (validarRequeridoForm($("#anulacion"))) {
            if (tr >= '1') {
                Enviar_ficha_app();
            }
        }
    });
    contarCaracteres("motivo_anulacion", "text-out2", 1000);
    $('#btn_aprobar_carga').prop("disabled", true);
    $('#btn_cancelar').prop("disabled", true);
    $("#fileupload").click(function (event) {
        $(function () {
            'use strict';
            $('#fileupload').fileupload({
                url: "../cargar_archivo",
                dataType: 'json',
                type: 'POST',
                cache: false,
                maxFileSize: 20000000,
                formData: {
                    id_ficha: $("#id_ficha").val()
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width', progress + '%');
                    $('#porsncetaje_carga').text(progress + '%');
                },
                done: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 0);
                    setTimeout(function () {
                        $('#progress .progress-bar').css('width', progress + '%');
                        $('#porsncetaje_carga').text(progress + '%');
                    }, 3000);
                    DocumentosFicha($('#id_ficha').val());
                }
            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
    });
    DocumentosFicha($('#id_ficha').val());
});

function DocumentosFicha(id) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosDocumentoFicha(result, id);
            //$("#modal_documentos_ficha").modal("show");
            //$("#div_frm_add").css("display", "none");
            //$("#div_tbl_contacto_empresa").css("display", "block");
            //$("#btn_nuevo_contacto").css("display", "block");
        },
        url: "../verDocumentosFicha"
    });
}

function cargaDatosDocumentoFicha(data, id) {


    var html = "";
    $('#tbl_documentos_ficha tbody').html(html);
    $.each(data, function (i, item) {
        html += '        <tr>';
        if (item.nombre_documento % 2 == 0) {
            html += ' <td><a href="../../../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a></td> ';
        } else {
            html += ' <td><a href="../../../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a></td> ';
        }

        html += '   <td>' + item.fecha_registro + '</td> ';

        html += '</tr>';
    });
    $('#tbl_documentos_ficha tbody').html(html);
}

function Enviar_ficha_app() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#anulacion').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
//			alerta('Empresa', errorThrown, 'error');
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                confirmButtonText: "Ok"
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            location.href = "../../Listar";
                        }
                    });
        },
        url: "../enviar_Ficha"
    });

}

function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}
