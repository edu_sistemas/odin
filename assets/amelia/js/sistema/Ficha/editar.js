const id_ficha = $(location).attr('href').split("/").reverse().splice(0, 1).toString();
$(document).ready(function () {
    $("#id_ficha").val(id_ficha);
    //AJAX PADRE
    getFichaByID();

    /*aceptar edición */
    $("#btn_registrar").click(function () {
        if (validarRequeridoForm($("#frm_ficha_registro"))) {
            if (validacionesExtras()) {
                if ($("#span-tags-input").text() == "") {
                    swal("Error", "La ficha debe contener al menos 1 orden de compra", "error");
                } else {
                    EditarFicha();
                }
            }
        }
    });

    /*limita la cantidad de caracteres en la observación*/
    contarCaracteres("comentario", "text-out", 1000);
    $("#comentario").trigger("change");

    //Listener del holding: al cambiar su valor cargan las empresas relacionadas al holding en id="#empresa"
    $('#holding').change(function () {
        if ($('#holding').val() != "") {
            $("#empresa").html("").append("<option></option>");
            getCBXByID_select2("empresa", "sp_empresa_select_cbx_by_holding", $("#holding").val());
        } else {
            $('#empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
        }
    });

    //Listener de la empresa: al cambiar su valor cargan los contactos relacionadas de la empresa seleccionada
    $('#empresa').change(function () {
        if ($('#empresa').val() != "") {
            //direccion
            $("#direccion_empresa").html("").append("<option></option>");
            getCBXByID_select2("direccion_empresa", "sp_empresa_select_direccion_cbx", $("#empresa").val());
            //contacto
            $("#nombre_contacto_empresa").html("").append("<option></option>");
            getCBXByID_select2("nombre_contacto_empresa", "sp_empresa_select_contact_nombre_by_cbx", $("#empresa").val());
        } else {
            $('#direccion_empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
        }
    });

    //Listener del contacto de la empresa: al cambiar de valor carcha el o los telefonos correspondientes del contacto
    $('#nombre_contacto_empresa').change(function () {
        if ($('#nombre_contacto_empresa').val() != "") {
            $("#telefono_contacto_empresa").html("").append("<option></option>");
            getCBXByID_select2("telefono_contacto_empresa", "sp_empresa_select_contact_telefono_by_cbx", $("#nombre_contacto_empresa").val());
        } else {
            $('#telefono_contacto_empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{ id: '', text: '' }]
            });
        }
    });

    //Listener del checkbox sence
    $("#chk_sence").change(function () {
        if (this.checked) {
            $("#cod_sence").attr("disabled", false);
            $("#otic").attr("disabled", false);
        } else {
            $("#cod_sence").attr("disabled", "disabled");
            $("#otic").attr("disabled", "disabled");
        }
    });

    //Muestra todas las ordenes de compra que estan relacionadas a la ficha
    $('#txt_num_o_c').tagsinput('add', $('#oculto_num_o_c').val());

    //Controlamos que si la ficha ya tiene una orden de compra ingresada, el checkbox de "sin O.C.?" no se puede utilizar
    if ($("#txt_num_o_c").val() != "") {
        $("#chk_sin_orden_compra").prop("disabled", "disabled");
    }

    //Listener del checbok "sin O.C.?"
    $("#chk_sin_orden_compra").change(function () {
        if (this.checked) {
            $("#tags-input").attr("disabled", "disabled");
        } else {
            $("#tags-input").attr("disabled", false);
        }
    });

    //Listener de Modalidad del curso, al cambiar el valor, se listan los cursos relacionados a la modalidad
    $('#modalidad').change(function () {
        if ($('#modalidad').val() != "") {
            $("#curso").html("").append("<option></option>");
            getCBXByID_select2("curso", "sp_curso_select_cbx_ficha", $('#modalidad').val());
        } else {
            $("#curso").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
        }
    });


    //Listener de curso, al cambiar su valor carga las versiones disponibles en cbx version
    $('#curso').change(function () {
        if ($('#curso').val() != "") {
            $("#version").html("").append("<option></option>");
            getCBXByID_select2("version", "sp_version_select_cbx_by_curso", $("#curso").val());
        } else {
            $("#version").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
        }
    });

    //Listener de codigo sence, al cambiar su valor se selecciona la modalidad y el curso del codigo seleccionado
    $('#cod_sence').change(function () {
        if ($('#cod_sence').val() != "") {
            var data = getCBXByID_select2("", "sp_modalidad_select_by_code_sence", $('#cod_sence').val());
            $("#modalidad").val(data[0].id_modalidad).trigger("change");
            $("#curso").val(data[0].id_curso).trigger("change");
        }
    });

    //Listener de Sede, al cambiar su valor carga las salas correspondientes
    $('#sede').change(function () {
        if ($('#sede').val() != "") {
            $("#sala").html("").append("<option></option>");
            getCBXByID_select2("sala", "sp_sala_select_by_ficha_cbx", $("#sede").val());
            var data = getCBXByID_select2("", "sp_sede_select_direccion", $("#sede").val());
            var text = data[0].id == 3 || data[0].id == 6 ? "" : data[0].text;
            $("#lugar_ejecucion").val(text);
        } else {
            $("#sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
        }
    });

    // Este listener es para controlar y llamar los id de solicitud tablet asociados al ejecutivo
    $("#ejecutivo,#modalidad,#curso").change(function () {
        if ($("#ejecutivo").val() != "" && $("#modalidad").val() == 6 && $("#curso").val() != "") {
            $("#id_solicitud_tablet").html("").append("<option></option>");
            getSolicitudesTabletCBX($("#ejecutivo").val(), $("#curso").val());
            $("#id_solicitud_tablet").prop("disabled", false);
        } else {
            $("#id_solicitud_tablet").html("").append("<option></option>");
            $("#id_solicitud_tablet").prop("disabled", true);
        }
    });

    // Esto es para eliminar ordenes de compra (Autor: Luis Jarpa)

    var confirmed = true;

    $('#txt_num_o_c').on('beforeItemRemove', function (event) {
        var tag = event.item;
        var mis_ordenes = $('#oculto_num_o_c').val();
        var array_ordenes = mis_ordenes.split(",");

        if (array_ordenes.length <= 1) {
            swal({
                title: "Advertencia",
                text: "No puede eliminar la ultima Orden de compra",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Entiendo!",
                closeOnConfirm: true
            });
            //alert('asdasd');
            event.cancel = confirmed;
            return false;
        }
        event.cancel = confirmed;

        if (confirmed) {
            swal({
                title: "Estás seguro?",
                text: "Eliminarás todos los alumnos registrados en esta orden de compra",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminarlo!",
                cancelButtonText: "No, Cancelar!",
                cancelButtonColor: "#DD6B55",
                closeOnConfirm: true
            }, function (isConfirm) {
                confirmed = false;
                if (isConfirm) {

                    var mis_ordenes = $('#txt_num_o_c').val();
                    var array_ordenes = mis_ordenes.split(",");
                    var indice_num_oc = array_ordenes.indexOf(tag);

                    var mis_id_orden = $("#id_num_o_c").val();
                    var array_id_orden = mis_id_orden.split(",");
                    // obtengo el elemento que necesito eliminar
                    var id_orden_compra_eliminar = array_id_orden[indice_num_oc];
                    // borro el elemento del array
                    array_id_orden.splice(indice_num_oc, 1);
                    $('#txt_num_o_c').tagsinput('remove', tag);
                    // guardo los valores nuevos
                    $("#id_num_o_c").val(array_id_orden.toString());

                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {
                            id_ficha: $('#id_ficha').val(),
                            id_orden_compra: id_orden_compra_eliminar,
                            num_orden_compra: tag
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // code en caso de error de la ejecucion del ajax
                            //console.log(textStatus + errorThrown);
                            alerta('Ficha', errorThrown, 'error');
                        },
                        success: function (result) {

                            setTimeout(function () {
                                swal({
                                    title: result[0].titulo,
                                    text: result[0].mensaje,
                                    type: result[0].tipo_alerta,
                                    showCancelButton: false,
                                    confirmButtonText: "OK",
                                    closeOnConfirm: false
                                },
                                    function () {
                                        location.reload();
                                    });
                            }, 1000);
                        },
                        url: "../deleteOC"
                    });
                } else {
                    confirmed = true;
                }
            });
        } else {
            event.cancel = confirmed;
            confirmed = true;
        }
    });

    // jess para q muestre el sin orden compra 335
    if ($("#oculto_num_o_c").val() == "" || $("#oculto_num_o_c").val() == "sin orden de compra335") {
        $('#chk_sin_orden_compra').iCheck('check');
        $('#txt_num_o_c').tagsinput('add', $('#oculto_num_o_c').val());
    }
    // para plugin datepicker
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });
    // para plugin clockpicker
    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });
});

// INICIO AJAX PADRE
function getFichaByID() {
    $.ajax({
        url: "../getFichaByID",
        type: 'POST',
        dataType: "json",
        data: { id_ficha: id_ficha },
        success: function (result) {
            console.log(result);

            // holding
            getCBX_select2("holding", "sp_holding_select_cbx");
            $("#holding").val(result[0].id_holding).trigger("change");
            $("#holding option[value='" + result[0].id_holding + "']").attr("selected", "selected");

            // empresa
            getCBXByID_select2("empresa", "sp_empresa_select_cbx_by_holding", result[0].id_holding);
            $("#empresa").val(result[0].id_empresa).trigger("change");
            $("#empresa option[value='" + result[0].id_empresa + "']").attr("selected", "selected");

            // direccion empresa
            $('#direccion_empresa').html('').select2({ data: [{ id: '', text: '' }], placeholder: "Seleccione" });
            $("#direccion_empresa").append('<option value="1">' + result[0].direccion_empresa + '</option>');
            $("#direccion_empresa").val(1).trigger("change");
            $("#direccion_empresa option[value='" + result[0].direccion_empresa + "']").attr("selected", "selected");

            //nombre contacto
            getCBXByID_select2("nombre_contacto_empresa", "sp_empresa_select_contact_nombre_by_cbx", result[0].id_empresa);
            $("#nombre_contacto_empresa").val(result[0].id_contacto_empresa).trigger("change");
            $("#nombre_contacto_empresa option[value='" + result[0].id_contacto_empresa + "']").attr("selected", "selected");

            //telefono contacto
            getCBXByID_select2("telefono_contacto_empresa", "sp_empresa_select_contact_telefono_by_cbx", result[0].id_contacto_empresa);
            $("#telefono_contacto_empresa").val(result[0].telefono_contacto).trigger("change");
            $("#telefono_contacto_empresa option[value='" + result[0].telefono_contacto + "']").attr("selected", "selected");

            //direccion envio diplomas
            $("#lugar_entrega_diplomas").val(result[0].direccion_diplomas);

            //tiene sence?
            $("#chk_sence").val(result[0].aplica_sence);

            //codigo sence
            getCBX_select2("cod_sence", "sp_code_sence_select_ficha_cbx");
            $("#cod_sence").val(result[0].codigo_sence).trigger("change");
            $("#cod_sence option[value='" + result[0].codigo_sence + "']").attr("selected", "selected");
            $("#id_codigo_sence").val(result[0].codigo_sence);

            //otic
            getCBX_select2("otic", "sp_otic_select_cbx");
            $("#otic").val(result[0].id_otic).trigger("change");
            $("#otic option[value='" + result[0].id_otic + "']").attr("selected", "selected");
            $("#id_otic").val(result[0].id_otic);

            //n° orden compra
            $("#id_num_o_c").val(result[0].id_orden_compra);
            $("#oculto_num_o_c").val(result[0].num_orden_compra);

            //Sin O.C.?
            $("#chk_sin_orden_compra").val(result[0].sin_oc);

            //modalidad
            $("#id_modalidad").val(result[0].id_modalidad);
            getCBX_select2("modalidad", "sp_modalidad_select_cbx");
            cargaModalidad();

            //curso
            getCBXByID_select2("curso", "sp_curso_select_cbx_ficha", $('#modalidad').val());
            $("#curso").val(result[0].id_curso).trigger("change");
            $("#curso option[value='" + result[0].id_curso + "']").attr("selected", "selected");

            //version
            $("#id_version").val(result[0].id_version);
            getCBXByID_select2("version", "sp_version_select_cbx_by_curso", result[0].id_curso);
            $("#version").val(result[0].id_version).trigger("change");
            $("#version option[value='" + result[0].id_version + "']").attr("selected", "selected");

            //fecha inicio
            $("#fecha_inicio").val(result[0].fecha_inicio);

            //fecha termino
            $("#fecha_termino").val(result[0].fecha_fin);

            //hora inicio
            $("#txt_hora_inicio").val(result[0].hora_inicio);

            //hora termino
            $("#txt_hora_termino").val(result[0].hora_termino);

            //sede
            $("#id_sede").val(result[0].id_sede)
            getCBX_select2("sede", "sp_sede_select_cbx");
            $("#sede").val(result[0].id_sede).trigger("change");
            $("#sede option[value='" + result[0].id_sede + "']").attr("selected", "selected");

            //sala
            $("#id_sala").val(result[0].id_sala);
            getCBXByID_select2("sala", "sp_sala_select_by_ficha_cbx", result[0].id_sede);
            $("#sala").val(result[0].id_sala).trigger("change");
            $("#sala option[value='" + result[0].id_sala + "']").attr("selected", "selected");

            //lugar ejecucion
            $("#lugar_ejecucion").val(result[0].lugar_ejecucion);

            // dias
            var id_dias = result[0].id_dias;
            var res = id_dias.split(",");
            for (var i = 0; i < res.length; i++) {
                switch (res[i]) {
                    case "1":
                        $("#lunes").prop("checked", true);
                        break;
                    case "2":
                        $("#martes").prop("checked", true);
                        break;
                    case "3":
                        $("#miercoles").prop("checked", true);
                        break;
                    case "4":
                        $("#jueves").prop("checked", true);
                        break;
                    case "5":
                        $("#viernes").prop("checked", true);
                        break;
                    case "6":
                        $("#sabado").prop("checked", true);
                        break;
                    case "7":
                        $("#domingo").prop("checked", true);
                        break;
                }
            }

            //ejecutivo comercial
            $("#id_ejecutivo").val(result[0].id_ejecutivo);
            getCBX_select2("ejecutivo", "sp_usuario_select_tipo_eje_comercial_cbx");
            $("#ejecutivo").val(result[0].id_ejecutivo).trigger("change");
            $("#ejecutivo option[value='" + result[0].id_ejecutivo + "']").attr("selected", "selected");

            //observaciones
            $("#comentario").text(result[0].comentario_orden_compra);

            //relacionar con ficha
            $("#id_relacionar_ficha_con").val(result[0].id_ficha_relacion);
            getCBX_select2("relacionar_ficha_con", "sp_ficha_relacionadas_select_cbx");
            $("#relacionar_ficha_con").val(result[0].id_ficha_relacion).trigger("change");
            $("#relacionar_ficha_con option[value='" + result[0].id_ficha_relacion + "']").attr("selected", "selected");

            //tiene sence?
            tieneSence();

            //n° solicitud tablet
            if (result[0].id_ejecutivo != "" && result[0].id_modalidad == 6 && result[0].id_curso != "" && result[0].id_solicitud_tablet != null) {
                $("#id_solicitud_tablet").html('');
                getSolicitudesTabletCBX(result[0].id_ejecutivo, result[0].id_curso);
                $("#id_solicitud_tablet").prop("disabled", false);
                data = getCBXByID_select2("", "sp_tablet_solicitud_tablet_by_id", result[0].id_solicitud_tablet);
                $("#id_solicitud_tablet").append('<option value="' + data[0].id_solicitud + '">' + data[0].id_solicitud + ' | ' + data[0].modelo + ' | ' + data[0].cantidad + 'Unidades | ' + data[0].fecha_solicitud + ' | ' + data[0].aplicaciones_requeridas);
                $("#id_solicitud_tablet").val(data[0].id_solicitud).trigger("change");
                $("#id_solicitud_tablet option[value='" + data[0].id_solicitud + "']").attr("selected", "selected");

            } else {
                $("#id_solicitud_tablet").prop("disabled", true);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar los datos del select: " + errorThrown,
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
}
// FIN AJAX PADRE

function getSolicitudesTabletCBX(id_ejecutivo_comercial, id_curso) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ejecutivo_comercial: id_ejecutivo_comercial, id_curso: id_curso },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo("id_solicitud_tablet", result);
        },
        url: "../getSolicitudesTabletCBX"
    });
}

function cargaModalidad() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_code_sence: $('#cod_sence').val() },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            if ($('#cod_sence').val() != 25) {
                $('#modalidad').val(result[0].id_modalidad);
                $('#id_curso').val(result[0].id_curso);
                $('#modalidad').trigger("change");
            } else {
                $('#modalidad').val($("#id_modalidad").val());
                $('#id_curso').val(result[0].id_curso);
                $('#modalidad').trigger("change");
            }
        },
        url: "../getModalidadByCodeSence"
    });
}

function tieneSence(result) {
    if ($("#cod_sence").val() != "25" && $("#cod_sence").val() != null) {
        $("#chk_sence").val(1);
        $("#chk_sence").iCheck('check');

        $("#cod_sence").prop("disabled", false);
        $("#cod_sence").attr("requerido", true);
        $("#cod_sence").removeClass("disabled");

        $("#otic").prop("disabled", false);
        $("#otic").removeClass("disabled");
        $("#otic").attr("requerido", true);

        $("#txt_num_o_c").prop("disabled", false);
        $("#txt_num_o_c").attr("requerido", true);
        $("#txt_num_o_c").val($("#span-tags-input").text());

        $('#chk_sin_orden_compra').prop("disabled", true);
        $("#chk_sin_orden_compra").iCheck('uncheck');
        $('#chk_sin_orden_compra').val(0);
    } else {
        $("#chk_sence").val(0);
        $("#chk_sence").iCheck('uncheck');

        $("#cod_sence").val(25).trigger("change");
        $("#cod_sence").attr("requerido", false);
        $("#cod_sence").prop("disabled", true);

        $("#otic").val(1).trigger("change");
        $("#otic").prop("disabled", true);
        $("#otic").attr("requerido", false);

        $('#txt_num_o_c').tagsinput('removeAll');
        $('#txt_num_o_c').addClass("disabled");
        $("#txt_num_o_c").val("");
        $("#txt_num_o_c").attr("requerido", false);

        $('#chk_sin_orden_compra').prop("disabled", false);
    }
}

function EditarFicha() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_ficha_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonText: "OK",
                closeOnConfirm: false
            },
                function () {
                    location.reload();
                });
            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }
        },
        url: "../Editar_ficha"
    });
}

/* FUNCIONES QUE PERMITEN LA EDICIÓN*/
function validacionesExtras() {
    var result = false;
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_termino = $("#fecha_termino").val();
    //  var fecha_cierre = $("#fecha_cierre").val();

    var res = fecha_inicio.split("-");
    fecha_inicio = res[2] + '-' + res[1] + '-' + res[0];
    res = fecha_termino.split("-");
    fecha_termino = res[2] + '-' + res[1] + '-' + res[0];

    var f = new Date();
    var mes = (f.getMonth() + 1);

    if (mes < 10) {
        mes = "0" + mes;
    }

    var dia = f.getDate();
    if (dia < 10) {
        dia = "0" + dia;
    }

    var f_actual = f.getFullYear() + "-" + mes + "-" + dia;
    if (fecha_inicio < f_actual) {
        alerta("Fecha", "Fecha Inicio no puede ser menor a la fecha Actual", "warning");
        result = false;
    } else {
        result = true;
    }

    if (fecha_inicio > fecha_termino) {
        alerta("Fecha", "Fecha Inicio no puede ser mayor a la fecha de  fecha_termino", "warning");
        result = false;
    } else {
        result = true;
    }
    return result;
}
