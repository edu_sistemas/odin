$(document).ready(function () {
       recargaLaPagina();
    cargaCbxAll();

    // $("#btn_min_max").trigger("click");
    $("#btn_registrar").click(function () {

        if (validarRequeridoForm($("#frm_ficha_registro"))) {
            if (validacionesExtras()) {
                setArriendo();
            }
        }
    });
    $("#sala").prop("disabled", true);
    $('#holding').change(function () {

        if ($('#holding').val() != "") {
            cargaEmpresas();
        } else {
            $('#empresa').html('').select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{id: '', text: ''}]
            });
        }
    });

    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });

    $('#data_5 .input-daterange').datepicker({
        language: 'es',
        format: "yyyy-mm-dd",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $("#curso").prop("disabled", true);

    contarCaracteres("comentario", "text-out", 1000);

    calcValores("n_dias");
    calcValores("valor_dia");
    calcValores("n_alumnos");
    calcValores("valor_break");

    $('#txt_num_o_c').tagsinput({
        confirmKeys: [13, 44],
        tagClass: 'big',
        maxTags: 3,
        trimValue: true,
        maxChars: 8,
        allowDuplicates: false,
        preventPost: false
    });


    $('#chk_sin_orden_compra').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    }).on('ifChanged', function (e) {        // Get the field name        

        var isChecked = e.currentTarget.checked;
        if (isChecked == true) {
            $('#txt_num_o_c').tagsinput('removeAll');
            $('#txt_num_o_c').addClass("disabled");
            $("#otic").val(1).trigger("change");
            $("#otic").prop("disabled", true);
            //$("#otic").addClass("disabled");
            $("#txt_num_o_c").prop("disabled", true);
            $("#txt_num_o_c").val("");
            $("#txt_num_o_c").attr("requerido", false);
        } else {
            $("#otic").prop("disabled", false);
            $("#otic").removeClass("disabled");

            $("#txt_num_o_c").prop("disabled", false);
            $("#txt_num_o_c").attr("requerido", true);
            $("#txt_num_o_c").val("");
            $('#txt_num_o_c').tagsinput('focus');
        }
    });


    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('#modalidad').change(function () {
        if ($('#modalidad').val() != "") {
            $("#curso").prop("disabled", false);
            cargaCurso();
        } else {
            $("#curso").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#curso").prop("disabled", true);
        }
    });

    if ($("#break").is(':checked')) {
        $('#valor_break').prop('readonly', false);
        $("#break").val('1');
    } else {
        $('#valor_break').prop('readonly', true);
        $("#break").val('0');
    }

    $("#break").click(function () {
        if ($("#break").is(':checked')) {
            $("#break").val('1');
            $('#valor_break').prop('readonly', false);
        } else {
            $('#valor_break').prop('readonly', true);
            $('#valor_break').val(0);
            $("#break").val('0');
            updateTotal();
        }
    });


    $('#sede').change(function () {
        if ($('#sede').val() != "") {

            $("#sala").prop("disabled", false);

            cargaSalas();
            //    cargaLugarEjecucion();
        } else {

            $("#sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#sala").prop("disabled", true);
        }
    });



    $("#otic").prop("disabled", true);

});

function cargaSalas() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_sede: $('#sede').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "sala");
        },
        url: "Arriendo/getSalaCBX"
    });
}


function validacionesExtras() {
    var result = true;
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_termino = $("#fecha_termino").val();



    var f = new Date();
    var mes = (f.getMonth() + 1);

    if (mes < 10) {
        mes = "0" + mes;

    }

    var dia = f.getDate();

    if (dia < 10) {
        dia = "0" + dia;

    }
    var f_actual = f.getFullYear() + "-" + mes + "-" + dia;



    if (fecha_inicio < f_actual) {
        alerta("Fecha", "Fecha Inicio no puede ser menor a la fecha Actual", "warning");
        result = false;
    }

    if (fecha_inicio > fecha_termino) {
        alerta("Fecha", "Fecha Inicio no puede ser mayor a la fecha de  fecha_termino", "warning");
        result = false;
    }


    /*if (fecha_cierre < fecha_termino) {
     alerta("Fecha", "Fecha Cierre no puede ser menor a la de Término", "warning");
     result = false;
     } else {
     
     result = true;
     }*/



    return result;

}
function setArriendo() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    // console.log('imagen en base64: '+ document.getElementById("logo").value);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_ficha_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                // $('#btn_reset').trigger("click");
                history.back();
            }

        },
        url: "Arriendo/setArriendo"
    });
}

function cargaCurso() {



    $('#curso').html('').select2({data: [{id: '', text: ''}]});

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            modalidad: $('#modalidad').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "curso");
        },
        url: "Agregar/getCursoCBX"
    });


}

function cargaCbxAll() {



    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "Arriendo/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "ejecutivo");
        },
        url: "Arriendo/getUsuarioTipoEcutivoComercial"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "holding");
        },
        url: "Arriendo/getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "sede");
        },
        url: "Arriendo/getSedeCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "sala");
        },
        url: "Arriendo/getSalaCBX"
    });

}

function cargaCombo(midata, item) {


    $("#" + item).html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{id: '', text: ''}]
    });

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");
}

function cargaEmpresas() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_holding: $('#holding').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "Arriendo/getEmpresaCBXbyHolding"
    });

}

function calcValores(idtextarea) {

    $("#" + idtextarea).keyup(function ()
    {
        updateTotal();
    });

    $("#" + idtextarea).change(function ()
    {
        updateTotal();
    });
}

function updateTotal() {
    var n_dias = $('#n_dias').val();
    var valor_dia = $('#valor_dia').val();

    var total_dia = n_dias * valor_dia;
    $('#total_dia').val(total_dia);


    var n_alumnos = $('#n_alumnos').val();
    var valor_break = $('#valor_break').val();
    var total_en_break = n_alumnos * valor_break * n_dias;

    $('#total_en_break').val(total_en_break);




    var valorVenta = total_dia + total_en_break;
    $('#valorVenta').val(valorVenta);

    var iva = parseInt(valorVenta) * 0.19;
    var totalVenta = parseInt(valorVenta) * 1.19;

    $('#iva').val(iva);
    $('#totalVenta').val(totalVenta);
}
