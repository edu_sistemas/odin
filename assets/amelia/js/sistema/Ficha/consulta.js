$(document).ready(function () {
    recargaLaPagina();
    cargaTodosDatosExtras();
    llamaDatosFicha();
    $('#data_5 .input-daterange').datepicker({
        format: "dd-mm-yyyy",
        language: 'es',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#modal_documentos_ficha').click(function () {
        location.reload();
    });

    $('#modal-principal').click(function (event) {
        event.stopPropagation();
    });

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_arriendo").click(function () {
        location.href = 'Arriendo';
    });

    $("#btn_buscar_ficha").click(function () {
        llamaDatosFicha();
    });

    $("#cbx_holding").select2({
        placeholder: "Todos",
        allowClear: true
    });

    $("#btn_nuevo_contacto").click(function () {
        $("#div_frm_add").css("display", "block");
        $("#div_tbl_contacto_empresa").css("display", "none");
        $("#btn_nuevo_contacto").css("display", "none");
    });

    $("#btn_cancelar_contacto").click(function () {
        $("#div_frm_add").css("display", "none");
        $("#div_tbl_contacto_empresa").css("display", "block");
        $("#btn_nuevo_contacto").css("display", "block");
    });

    $("#fileupload").click(function (event) {

        $(function () {
            'use strict';
            $('#fileupload').fileupload({
                url: "Listar/upload_file",
                dataType: 'json',
                type: 'POST',
                cache: false,
                maxFileSize: 20000000,
                formData: {
                    id_ficha: $("#modal_id_ficha").html()
                },
                done: function (e, data) {

                    // $('<p/>').text(data.files[0].name).appendTo('#files');
                    //$("#modal_id_ficha").val(data.result.Ficha);
                    DocumentosFicha($("#modal_id_ficha").val());


                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width', progress + '%');
                    $('#porsncetaje_carga').text(progress + '%');
                }

            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

        });

    });
});

function  cargaTodosDatosExtras() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            cargaComboGenerico(result, 'ejecutivo');
        },
        url: "Consulta/getEjecutivo"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            cargaComboGenerico(result, 'periodo');
        },
        url: "Consulta/getPeriodo"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result);
        },
        url: "Consulta/getEstadoCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo2(result);
        },
        url: "Consulta/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo3(result);
        },
        url: "Consulta/getHoldingCBX"
    });

}

function  DeleteFicha(ficha) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: ficha
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', jqXHR.responseText, 'error');
        },
        success: function (result) {

            swal("Ficha", "Tu Ficha ha sido eliminada!", "success");
            llamaDatosFicha();
        },
        url: "Listar/eliminarFicha"
    });


}

function getFileAdjuntos() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#id_ficha").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', jqXHR.responseText, 'error');
        },
        success: function (result) {
            cargaTbLAdjunto(result);
        },
        url: "Listar/getFileAdjunto"
    });
}

function cargaTbLAdjunto(data) {
    if ($.fn.dataTable.isDataTable('#tbl_adjuntos')) {
        $('#tbl_adjuntos').DataTable().destroy();
    }
    $('#tbl_adjuntos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_doc"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<a href="../../' + o.ruta_ducumento + '" target="_blank">' + o.nombre_documento + '</a>';
                    return html;
                }
            },
            {"mDataProp": "fecha_registro"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var x = "'Editar/index/" + o.id_otic + "'";
                    return '<button class="btn-primary"  onclick="location.href=' + x + '">Editar</button>';
                }
            }
        ],
        pageLength: 5,
        responsive: true
    });
}

function llamaDatosFicha() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaFicha(result);
        },
        url: "Consulta/buscarFicha"
    });

}

function cargaTablaFicha(data) {

    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }

    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {


                    var html = "";
                    var boton = "class='btn btn-primary dropdown-toggle'";
                    var a = "Resumen/index/" + o.id_ficha;
                    var b = "ResumenArriendo/index/" + o.id_ficha;

                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' " + boton + ">Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu '>";
                    var w = "Rectificacion/index/" + o.id_ficha;


                    var a = "Resumen/index/" + o.id_ficha;
                    var b = "ResumenArriendo/index/" + o.id_ficha;
                    if (o.id_categoria == 1) {
                        html += "<li><a href='" + a + "'>Ver Ficha</a></li>";

                        html += '<li class="divider"></li>';

                        html += "<li><a href='" + w + "'>Rectificación</a></li>";
                    }

                    if (o.id_categoria == 2) {
                        html += "<li><a href='" + b + "'>Ver Ficha</a></li>";

                    }
                    html += ' </div>';
                    return html;
                }
            },
            {"mDataProp": "num_ficha"},
            {"mDataProp": "back_office"},
            {"mDataProp": "ejecutivo"},
            {"mDataProp": "categoria"},
            {"mDataProp": "modalidad"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += o.fecha_inicio + ' / ' + o.fecha_fin;
                    return html;
                }
            },
            {"mDataProp": "descripcion_producto"},
            {"mDataProp": "otic"},
            {"mDataProp": "empresa"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '  <p data-toggle="tooltip" data-placement="top" title data-original-title="Sence: ' + o.valor_sence + ' • Empresa: ' + o.valor_empresa + '" >' + o.valor_total + '</p>';
                    return html;
                }
            },
            {"mDataProp": "estado"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = "<a href='#' title='Subir o editar documentos' onclick='DocumentosFicha(" + o.id_ficha + "," + o.id_estado + ");'>" + o.cant_documento + "</a>";
                    return html;
                }
            },

            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = "<a href='#' title='Ver Alumnos' onclick='AlumnosFicha(" + o.id_ficha + ");'>" + o.cant_alumno + "</a>";
                    html = o.cant_alumno;
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [1, "desc"]
    });


}

function DocumentosFicha(id) {
    $("#modal_id_ficha").text(id);
    $("#txt_id_ficha").val(id);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosDocumentoFicha(result, id);
            $("#modal_documentos_ficha").modal("show");
            $("#div_frm_add").css("display", "none");
            $("#div_tbl_contacto_empresa").css("display", "block");
            $("#btn_nuevo_contacto").css("display", "block");
        },
        url: "Listar/verDocumentosFicha"
    });

}

function cargaDatosDocumentoFicha(data, id) {
    var html = "";
    $('#tbl_documentos_ficha tbody').html(html);
    $.each(data, function (i, item) {
        html += '        <tr>';
        if (item.nombre_documento % 2 == 0) {
            html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a></td> ';
        } else {
            html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a></td> ';
        }

        html += '   <td>' + item.fecha_registro + '</td> ';
        html += '<td>';

        html += '</td> ';
        html += '</tr>';
    });
    $('#tbl_documentos_ficha tbody').html(html);
}

function cargaComboGenerico(midata, id) {

    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

    $("#" + id + " option[value='0']").remove();
}

function cargaCombo(midata) {
    $("#estado").select2({
        placeholder: "Seleccione un estado",
        allowClear: true,
        data: midata
    });
    $("#estado option[value='0']").remove();
}

function cargaCombo2(midata) {
    $('#empresa').html('').select2({data: [{id: '', text: ''}]});
    $("#empresa").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#empresa option[value='0']").remove();
}
function cargaCombo3(midata) {
    $("#holding").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#holding option[value='0']").remove();
}
