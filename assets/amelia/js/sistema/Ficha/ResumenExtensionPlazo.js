$(document).ready(function () {
    recargaLaPagina();
    cargaCbxAll();
    inicializaControles();
    $("#btn_enviar_rectificacion").click(function () {
        sendRectificacion();
    });
});


function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() {


    var id_rectificacion = "";

    id_rectificacion = $("#id_rectificacion").val();

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosAlumnosPorRectificar(result);
        },
        url: "../cargaDatosOCRectificacion"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentosRectificacion(result);
        },
        url: "../cargaDatosOCDocumentoResumenRectificaciones"
    });


}

function cargaTablaDatosAlumnosPorRectificar(data) {

    if ($.fn.dataTable.isDataTable('#tbl_rectificacion_extension_plazo')) {
        $('#tbl_rectificacion_extension_plazo').DataTable().destroy();
    }

    $('#tbl_rectificacion_extension_plazo').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_rectificacion"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "fecha_cierre"},
            {"mDataProp": "fecha_extension"},
            {"mDataProp": "num_orden_compra"}

        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentosRectificacion(data) {

    if ($.fn.dataTable.isDataTable('#tbl_rectificacion_extension_plazo_documento')) {
        $('#tbl_rectificacion_extension_plazo_documento').DataTable().destroy();
    }

    $('#tbl_rectificacion_extension_plazo_documento').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            }

        ],
        pageLength: 5,
        responsive: false,
        dom: '<"clear">'

    });

    $('[data-toggle="tooltip"]').tooltip();

}

function sendRectificacion() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: $("#id_rectificacion").val()},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Rectificación", errorThrown, "error");
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function () {
                        location.href = "../../Rectificaciones";
                    });
        },
        url: "../enviarRectificacion"
    });

}

