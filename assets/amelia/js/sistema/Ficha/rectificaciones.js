$(document).ready(function () {
    recargaLaPagina();
    llamaDatosRectificacion();

    $("#btn_cerrarmodal_doc").on('click', function () {

        llamaDatosRectificacion();
    });



    $("#btn_buscar_ficha").click(function () {
        llamaDatosRectificacion();
    });

    $("#btn_nuevo_contacto").click(function () {
        $("#div_frm_add").css("display", "block");
        $("#div_tbl_contacto_empresa").css("display", "none");
        $("#btn_nuevo_contacto").css("display", "none");
    });

    $("#btn_guardar_contacto").click(function () {
        if (validarRequeridoForm($("#frm_nuevo_contacto_empresa"))) {
            RegistarNuevoContacto();
        }
    });

    $("#btn_cancelar_contacto").click(function () {

        $("#div_frm_add").css("display", "none");
        $("#div_tbl_contacto_empresa").css("display", "block");
        $("#btn_nuevo_contacto").css("display", "block");
    });

    $("#fileupload").click(function (event) {



        $(function () {
            'use strict';
            $('#fileupload').fileupload({
                url: "Rectificaciones/uploadFile",
                dataType: 'json',
                type: 'POST',
                cache: false,
                maxFileSize: 20000000,
                formData: {
                    id_rectificacion: $("#modal_id_ficha_rectificacion").text()
                },
                done: function (e, data) {

                    // $('<p/>').text(data.files[0].name).appendTo('#files');
                    //$("#modal_id_ficha").val(data.result.Ficha);
                    DocumentosRectificacion($("#modal_id_ficha_rectificacion").text());


                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width', progress + '%');
                    $('#porsncetaje_carga').text(progress + '%');
                }

            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

        });


    });



});


function llamaDatosRectificacion() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaRectificacion(result);
        },
        url: "Rectificaciones/buscarRectificacion"
    });
}


function cargaTablaRectificacion(data) {

    if ($.fn.dataTable.isDataTable('#tbl_rectificacion')) {
        $('#tbl_rectificacion').DataTable().destroy();
    }

    $('#tbl_rectificacion').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_rectificacion"},
            {"mDataProp": "num_ficha"},
            {"mDataProp": "num_orden_compra"},
            {"mDataProp": "tipo_rectificacion"},
            {"mDataProp": "fecha_registro"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "nombre_modalidad"},
            {"mDataProp": "razon_social_empresa"},
            {"mDataProp": "nombre_otic"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = "<a href='#' title='Subir o editar documentos' onclick='DocumentosRectificacion(" + o.id_rectificacion + ");'>" + o.doc + "</a>";
                    return html;
                }
            },

            {"mDataProp": "descripcion_estado_edutecno"},

            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle ">Accion<span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu pull-right">';
                    var resumen = "";

                    if (o.tipo_rectificacion == "Extensión de Plazo") {
                        resumen = "ResumenRectificacionExtension/index/" + o.id_rectificacion;

                    } else {

                        resumen = "ResumenRectificacion/index/" + o.id_rectificacion;
                    }

                    html += "<li><a href='" + resumen + "'>Ver</a></li>";

                    if (o.id_estado == 30 || o.id_estado == 50) {
                        html += "<li><a href='#' onclick='motivoRechazo(" + o.id_rectificacion + ");'>Ver motivo rechazo</a></li>";
                    }

                    html += ' </ul>';
                    html += ' </div>';
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        "order": [[0, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

function DocumentosRectificacion(id) {
    $("#modal_id_ficha_rectificacion").text(id);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Rectificación", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosDocumentoRectificacion(result, id);
            $("#modal_documentos_ficha_rectificacion").modal("show");
            $("#div_frm_add").css("display", "none");
            $("#div_tbl_contacto_empresa").css("display", "block");
            $("#btn_nuevo_contacto").css("display", "block");
        },
        url: "Rectificaciones/verDocumentosRectificacion"
    });
}

function cargaDatosDocumentoRectificacion(data) {


    var html = "";
    $('#tbl_documentos_ficha_rectificacion tbody').html(html);

    $.each(data, function (i, item) {
        html += '        <tr>';
        if (item.nombre_documento % 2 == 0) {
            html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a></td> ';
        } else {
            html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a></td> ';
        }

        html += '   <td>' + item.fecha_registro + '</td> ';

        html += '   <td>' + item.tipo_documento + '</td> ';


        html += '</tr>';
    });
    $('#tbl_documentos_ficha_rectificacion tbody').html(html);
}

function motivoRechazo(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Rectificación", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosMotivoRechazo(result);
            $("#modal_motivo_rechazo").modal("show");
        },
        url: "Rectificaciones/getMotivoRechazo"
    });

}

function cargaDatosMotivoRechazo(data) {
    //  console.log(data)
    $('#modal_id_rectificacion').text(data[0].id_rectificacion);
    $('#motivo_rechazo').val(data[0].motivo_rechazo);
}
 