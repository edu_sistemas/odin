$(document).ready(function () {
    recargaLaPagina();
    cargaCbxAll();

    $("#btn_registrar").click(function () {

        if (validarRequeridoForm($("#frm_ficha_registro"))) {
            if (validacionesExtras()) {
                updateArriendo();
            }
        }
    });

    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });

    $('#data_5 .input-daterange').datepicker({
        language: 'es',
        format: "yyyy-mm-dd",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    contarCaracteres("comentario", "text-out", 1000);

    calcValores("n_dias");
    calcValores("valor_dia");
    calcValores("n_alumnos");
    calcValores("valor_break");

    if ($("#isBreak").val() == "1") {
        $("#break").prop("checked", true);
    } else {
        $("#break").prop("checked", false);
    }

    if ($("#break").is(':checked')) {
        $('#valor_break').prop('readonly', false);
        $("#break").val('1');
    } else {
        $('#valor_break').prop('readonly', true);
        $("#break").val('0');
    }

    $("#break").click(function () {
        if ($("#break").is(':checked')) {
            $("#break").val('1');
            $('#valor_break').prop('readonly', false);
        } else {
            $('#valor_break').prop('readonly', true);
            $('#valor_break').val(0);
            $("#break").val('0');
            updateTotal();
        }
    });

    cargaDias();


    $('#sede').change(function () {
        if ($('#sede').val() != "") {

            $("#sala").prop("disabled", false);

            cargaSalas();
            //    cargaLugarEjecucion();
        } else {

            $("#sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#sala").prop("disabled", true);
        }
    });


});

function cargaSalas() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_sede: $('#sede').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo4(result, "sala");

        },
        url: "../getSalaCBX"
    });
}


function validacionesExtras() {
    var result = true;
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_termino = $("#fecha_termino").val();

    var f = new Date();
    var mes = (f.getMonth() + 1);

    if (mes < 10) {
        mes = "0" + mes;

    }

    var dia = f.getDate();

    if (dia < 10) {
        dia = "0" + dia;

    }
    var f_actual = f.getFullYear() + "-" + mes + "-" + dia;



    if (fecha_inicio < f_actual) {
        alerta("Fecha", "Fecha Inicio no puede ser menor a la fecha Actual", "warning");
        result = false;
    }

    if (fecha_inicio > fecha_termino) {
        alerta("Fecha", "Fecha Inicio no puede ser mayor a la fecha de  fecha_termino", "warning");
        result = false;
    }


    /*if (fecha_cierre < fecha_termino) {
     alerta("Fecha", "Fecha Cierre no puede ser menor a la de Término", "warning");
     result = false;
     } else {
     
     result = true;
     }*/



    return result;

}
function updateArriendo() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    // console.log('imagen en base64: '+ document.getElementById("logo").value);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_ficha_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                //   $('#btn_reset').trigger("click");
                history.back();
            }

        },
        url: "../updateArriendo"
    });
}



function cargaCbxAll() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "holding");
        },
        url: "../getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "../getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo3(result, "sede");
        },
        url: "../getSedeCBX"
    });



    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo2(result, "ejecutivo");
        },
        url: "../getUsuarioTipoEcutivoComercial"
    });
}

function cargaCombo(midata, item) {
    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();

    $("#" + item).val($("#getEmpresaSelected").val()).trigger("change");
}

function cargaCombo2(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();

    $("#" + item).val($("#getEjecutivoSelected").val()).trigger("change");
}

function cargaCombo3(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();

    $("#" + item).val($("#getSedeSelected").val()).trigger("change");
}

function cargaCombo4(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();

    $("#" + item).val($("#getSalaSelected").val()).trigger("change");
}

function cargaDias() {

    var id_dias = $("#id_dias").val();

    var res = id_dias.split(",");

    for (var i = 0; i < res.length; i++) {

        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;

        }

    }
}

function calcValores(idtextarea) {

    $("#" + idtextarea).keyup(function ()
    {
        updateTotal();
    });

    $("#" + idtextarea).change(function ()
    {
        updateTotal();
    });
}

function updateTotal() {
    var n_dias = $('#n_dias').val();
    var valor_dia = $('#valor_dia').val();

    var total_dia = n_dias * valor_dia;
    $('#total_dia').val(total_dia);


    var n_alumnos = $('#n_alumnos').val();
    var valor_break = $('#valor_break').val();
    var total_en_break = n_alumnos * valor_break * n_dias;

    $('#total_en_break').val(total_en_break);




    var valorVenta = total_dia + total_en_break;
    $('#valorVenta').val(valorVenta);

    var iva = parseInt(valorVenta) * 0.19;
    var totalVenta = parseInt(valorVenta) * 1.19;

    $('#iva').val(iva);
    $('#totalVenta').val(totalVenta);
}
