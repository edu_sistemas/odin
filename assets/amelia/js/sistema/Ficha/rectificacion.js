var contador = 0;
var contador_oc = 1;
$(document).ready(function () {
    recargaLaPagina();
    contarCaracteres("comentario", "text-out", 1000);
    $('#btn_aprobar_carga').prop("disabled", true);
    $('#btn_cancelar').prop("disabled", true);
    $("#btn_carga").click(function () {
        if (validarRequeridoForm($("#frm_carga_orden_compra"))) {
            send_file();
        }
    });
    $("#btn_agegar_oc").click(function () {
        generaFormularioOC();
        $("#btn_agegar_oc").prop('disabled', true);
    });
    $("#btn_cancelar").click(function () {
        swal({
            title: "Estas Seguro?",
            text: "Eliminará todos los registros previamente cargados",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy Seguro!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                limpia_carga();
                mostrar_lista_carga_orden_compra();
            }
        });
    });
    // inicializaControles();
    $("#btn_aprobar_carga").click(function () {

        if (validarRequeridoForm($("#frm_carga_orden_compra"))) {
            swal({
                title: "Estas Seguro?",
                text: "Solo se cargaran los registros que no contiene errores",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: 'Si, Estoy Seguro!',
                cancelButtonText: "Cancelar!",
                showLoaderOnConfirm: true,
                preConfirm: function (email) {

                    return new Promise(function (resolve, reject) {

                        setTimeout(function () {
                            aceptar_carga();
                            resolve();
                        }, 2000);
                    });
                },
                allowOutsideClick: false
            }).then(function (email) {
                resetForm();
                swal({
                    title: "Orden de Compra",
                    text: "Alumnos Actualizados!",
                    type: "success"
                });
            })
        }

    });
});
function resetForm() {

    $("#cbx_orden_compra").val('').change();
    $("#btn_cleanr_file").trigger("click");
    $('#btn_aprobar_carga').prop("disabled", true);
    $('#btn_cancelar').prop("disabled", true);
}


function send_file() {

    $('#btn_carga').prop("disabled", true);
    // $('#file_carga_empresa').prop("disabled", true);
    limpia_carga();
    $('#btn_carga').html('<i class="fa fa-upload"></i> Cargando..');
    var l = $('#btn_carga').ladda();
    l.ladda('start');
    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_carga_orden_compra")[0]),
        error: function (jqXHR, textStatus, errorThrown) {

            /* console.log(jqXHR);
             console.log(textStatus);
             console.log(errorThrown);*/

            alerta('Alumnos', jqXHR.responseText, 'error');
            // console.log(jqXHR.responseText);


        },
        success: function (result) {
            l.ladda('stop');
            $('#btn_carga').prop("disabled", false);
            $('#btn_carga').html('<i class="fa fa-upload"></i> Subir ');
            if (result.valido) {
                mostrar_lista_carga_orden_compra();
                $('#btn_aprobar_carga').prop("disabled", false);
                $('#btn_cancelar').prop("disabled", false);
                $('#file_carga_empresa').prop("disabled", false);
            } else {
                alerta('Orden de Compra', result.mensaje, 'error');
            }
        },
        url: '../cargar_archivo_datos_alumnos'
    });
}


function limpia_carga()
{

    /*
     
     Descripcion : Limpia la tabla temporal antes de cargar el archivo excel
     */

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (result) {

        },
        url: "../limpiar_carga_anterior"
    });
}

function  mostrar_lista_carga_orden_compra() {

    /*Muestra el docuemento cargado con mensaje dependiendo del tipo de validacion */

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Orden de Compra', jqXHR.responseText, 'error');
        },
        success: function (result) {

            cagar_tabla_temporal(result);
        },
        url: "../listar_carga_orden_de_compra"
    });
}
function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cagar_tabla_temporal(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }




    $('#tbl_orden_compra_carga_alumno').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut_alumno"},
            {"mDataProp": "nombre_alumno"},
            {"mDataProp": "apellido_paterno"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "porcentaje_franquicia"},
//            {"mDataProp": "costo_otic"},
//            {"mDataProp": "costo_empresa"},
//            {"mDataProp": "valor_agregado"},
            {"mDataProp": "valor_cobrado"},
            {"mDataProp": "mensaje"}
        ],
        pageLength: 25,
        responsive: false

    });
}


function aceptar_carga() {

    $.ajax({
        cache: false,
        async: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_carga_orden_compra")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Rectificación', jqXHR.responseText, 'error');
        },
        success: function (result) {

            if (contador == 0) {
                enviaCorreo(result);
                contador = 1;
            }


            limpia_carga();
            mostrar_lista_carga_orden_compra();
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            }, function () {
                history.back();
            });
        },
        url: '../aceptar_carga_orden_compra_alumnos'
    });
}

function enviaCorreo(data) {
    console.log(data);
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {datos: data},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {



            history.back();
            // cargaTablaFicha(result);
        },
        url: "../enviaEmail"
    });
}

function generaFormularioOC() {
    var formulario = "";
    formulario += '<form class="nuevas-oc" id="oc-new-oc" >';
    formulario += '<div class="ibox-title" >';
    formulario += '<h5>Nueva orden de Compra</h5>';
    formulario += '<div class="ibox-tools">';
    formulario += '</div>';
    formulario += '</div>';
    formulario += '<div class="col-lg-12" style="margin-top: 10px;">';
    formulario += '<input type="hidden" value="' + $("#id_ficha").val() + '" id="x_id_ficha" name="x_id_ficha" hidden>';
    formulario += '<label class="col-lg-2 control-label">Razón Social</label>';
    formulario += '<div class="col-lg-4">';
    formulario += '<select id="razon_social" name="razon_social" data-placeholder="Elija una razón social" class="chosen-select" tabindex="-1" required></select>';
    formulario += '</div> ';
    formulario += '</div>';
    formulario += '<div class="col-lg-12" style="margin-top: 10px;">';
    formulario += '<label class="col-lg-2 control-label">Otic</label>';
    formulario += '<div class="col-lg-3">';
    formulario += '<select id="otic" name="otic" data-placeholder="Elija una otic" class="chosen-select" tabindex="-1"></select>';
    formulario += '</div>';
    formulario += '</div>';
    formulario += '<div class="col-lg-12" style="margin-top: 10px;">';
    formulario += '<div class="form-group">';
    formulario += '<label class="col-lg-2 control-label">N° Orden de Compra</label>';
    formulario += '<div class="col-lg-3">';
    formulario += '<input type="text" class="form-control" id="numero_oc" name="numero_oc">';
    formulario += '</div>';
    formulario += '</div>';
    formulario += '</div>';
    formulario += '<div class="col-lg-12" style="margin-top: 10px;">';
    formulario += '<div class="form-group">';
    formulario += '<label class="col-lg-2 control-label">Comite Bipartito</label>';
    formulario += '<div class="col-lg-3">';
    formulario += '<label class="checkbox-inline"> ';
    formulario += '<input type="checkbox" name="comite_bipartito" value="0" id="inlineCheckbox1"';
    formulario += 'mensaje="Debe seleccionar si tiene Comite Bipartito"  ';
    formulario += '> No';
    formulario += '</label>';
    formulario += '<label class="checkbox-inline">';
    formulario += '<input type="checkbox" name="comite_bipartito" value="1" id="inlineCheckbox2"';
    formulario += 'mensaje="Debe seleccionar si tiene Comite Bipartito"  ';
    formulario += '>SI';
    formulario += '</label> ';
    formulario += '<label id="error_comite"></label> ';
    formulario += '</div>';
    formulario += '</div>';
    formulario += '</div>';
    formulario += '<div class="col-lg-12" style="margin-top: 10px;">';
    formulario += '<div class="form-group">';
    formulario += '<label class="col-lg-2 control-label">Observaciones</label>';
    formulario += '<div class="col-lg-10">';
    formulario += '<textarea class="form-control" name="observaciones" id="observaciones"></textarea>';
    formulario += '</div>';
    formulario += '</div>';
    formulario += '<button type="button" class="btn btn-success" onclick="agregarNuevaOC()" >Agregar</button>';
    formulario += '</div>';
    formulario += '<div class="col-lg-12" style="border-top: 1px solid gray !important;margin-top: 10px;"><br>';
    formulario += '</div>';
    formulario += '</div>';
    formulario += '</form>';
    $('#oc-container').append(formulario);
    var razones_sociales = getRazonsocialByid_HoldingCBX();
    cargaCBX('razon_social', razones_sociales);
    var otics = getOticsCBX();
    cargaCBX('otic', otics);
}

function agregarNuevaOC() {

    if (!$(".nuevas-oc #inlineCheckbox2").is(':checked') && !$(".nuevas-oc #inlineCheckbox1").is(':checked')) {
        alerta('Nueva O.C.', 'Debe ingresar empresa, Otic , n° de O.C. y COMITE BIPARTITO', 'error');
        return;
    }
    if ($(".nuevas-oc #inlineCheckbox2").is(':checked') && $(".nuevas-oc #inlineCheckbox1").is(':checked')) {
        alerta('Nueva O.C.', 'Debe seleccionar solo una opción COMITE BIPARTITO', 'warning');
        return;
    }

    if (
            $(".nuevas-oc #razon_social").val() == '0'
            || $(".nuevas-oc #razon_social").val() == ''
            || $(".nuevas-oc #otic").val() == '0'
            || $(".nuevas-oc #otic").val() == ''
            || $(".nuevas-oc #numero_oc").val() == ''
            ) {
        alerta('Nueva O.C.', 'Debe ingresar empresa, Otic y n° de O.C.', 'error');
    } else {
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: $("#oc-new-oc").serialize(),
            error: function (jqXHR, textStatus, errorThrown) {
                alerta('Nueva O.C.', jqXHR.responseText, 'error');
            },
            success: function (result) {
                swal({
                    title: result[0].titulo,
                    text: result[0].mensaje,
                    type: result[0].tipo_alerta,
                    showCancelButton: false,
                    confirmButtonText: 'Aceptar'
                }).then(function () {
                    location.reload();
                });
            },
            url: "../setNuevaOC"
        });
    }
}

function getRazonsocialByid_HoldingCBX() {
    var id_holding = $('#id_holding').val();
    var x_result = [];
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_holding},
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (result) {
            x_result = result;
        },
        url: "../getRazonSocialCBX"
    });
    return x_result;
}

function getOticsCBX() {
    var x_result = [];
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (result) {
            x_result = result;
        },
        url: "../getOticsCBX"
    });
    return x_result;
}