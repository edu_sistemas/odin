$(document).ready(function () {    
    recargaLaPagina();
    cargaTodosDatosExtras();
    $('[data-toggle="tooltip"]').tooltip();
    $("#btn_limpiar_form").click(function () {
        location.reload();
    });

    /*
    $("#btn_buscar_ficha").click(function () {
        llamaDatosVentas();
    });

    llamaDatosVentas();
    */
    
    // new
    setTimeout(function(){ $("#btn_buscar_ficha").click(); }, 500);    
});


function  cargaTodosDatosExtras() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            cargaComboGenerico(result, 'ejecutivo');
        },
        url: "VentaAnual/getEjecutivo"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            cargaComboGenerico(result, 'periodo');
        },
        url: "VentaAnual/getPeriodoAnual"
    });

}
function cargaComboGenerico(midata, id) {

    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#" + id + " option[value='0']").remove();
}

/*
function llamaDatosVentas() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaVentas(result);
        },
        url: "VentaAnual/consultarVentas"
    });
}
*/

function cargaTablaVentas() {
    var html = "";
    $('#tbl_ventas tbody').html(html);
    $.each(dataResult, function (i, item) { // dataResult es una variable global creada en ini.js
        html += '<tr>';
        html += '   <td>' + item.nombre_ejecutivo + '</td> ';
        html += '   <td>' + item.periodo + '</td> ';
        html += '   <td>' + item.total_arriendo + '</td> ';
        html += '   <td>' + item.total_venta_capacitacion + '</td> ';
        html += '   <td>' + item.total_suma_venta + '</td> ';
        var x = "";
        if (item.id_ejecutivo != 0) {
            x = "abrePopUP(" + item.id_periodo + "," + item.id_ejecutivo + ")";
            html += '   <td><button class="btn btn-default btn-rounded"  onclick="' + x + '">Detalle</button></td> ';
        } else {
            html += '   <td></td> ';
        }
        html += '</tr>';
    });

    $('#tbl_ventas tbody').html(html);
    $('[data-toggle="tooltip"]').tooltip();
}

function abrePopUP(id_periodo, id_ejecutivo) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            ejecutivo: id_ejecutivo,
            periodo: id_periodo
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaVentasEjecutivo(result);
        },
        url: "VentaAnual/consultarVentasPorEjecutivo"
    });

}

function cargaCombo2(midata) {
    $('#empresa').html('').select2({data: [{id: '', text: ''}]});
    $("#empresa").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
    $("#empresa option[value='0']").remove();
}

function cargaTablaVentasEjecutivo(data) {
    var html = "";
    $('#tbl_ventas_ejecutivo tbody').html(html);
    $.each(data, function (i, item) {
        if (i == 0) {
            $('#periodo_venta').text(item.periodo);
            $('#nombre_ejecutivo').text(item.nombre_ejecutivo);
        }
        html += '        <tr>';
        html += '   <td>' + item.empresa + '</td> ';
        html += '   <td>' + item.otic + '</td> ';
        html += '   <td>' + item.total_arriendo + '</td> ';
        html += '   <td>' + item.total_venta_capacitacion + '</td> ';
        html += '   <td>' + item.total_suma_venta + '</td> ';
        html += '</tr>';
    });

    $('#tbl_ventas_ejecutivo tbody').html(html);
    $("#myModal5").modal();
    $('[data-toggle="tooltip"]').tooltip();
}
