$(document).ready(function () {
    recargaLaPagina();

    $('#data_5 .input-daterange').datepicker({
        format: "dd-mm-yyyy",
        language: 'es',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    /*
    $("#btn_buscar_ficha").click(function () {
        llamaDatosInformeVentaCursos();
    });

    llamaDatosInformeVentaCursos();
    */

    // new
    setTimeout(function(){ $("#btn_buscar_ficha").click(); }, 500);    
});

/*
function llamaDatosInformeVentaCursos() {
    if ($("#optionsRadios1").is(':checked')) {
        $("#columna_fecha").text("Fecha Inscripción");
        $("#columna_fecha2").text("Fecha Inscripción");
    } else {
        $("#columna_fecha").text("Fecha Inicio Curso");
        $("#columna_fecha2").text("Fecha Inicio Curso");
    }

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaInformeVentaCursos(result);
        },
        url: "InformeVentaCursos/consultarInformeVentaCursos"
    });
}
*/

function cargaTablaInformeVentaCursos() {

    if ($.fn.dataTable.isDataTable('#tbl_informe_venta_curso')) {
        $('#tbl_informe_venta_curso').DataTable().destroy();
    }


    $('#tbl_informe_venta_curso').DataTable({
        "aaData": dataResult,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "id_solicitud_tablet"},
            {"mDataProp": "fecha"},
            {"mDataProp": "nombre_holding"},
            {"mDataProp": "nombre_fantasia"},
            {"mDataProp": "rut_empresa"},
            {"mDataProp": "empresa"},
            {"mDataProp": "direccion_empresa"},
            {"mDataProp": "rut_otic"},
            {"mDataProp": "nombre_otic"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "modalidad"},
            {"mDataProp": "sub_linea"},
            {"mDataProp": "monto_total"},
            {"mDataProp": "distribucion_a"},
            {"mDataProp": "ejecutivo"}
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'InformeVentaCursos'
            }, {
                extend: 'pdf',
                title: 'InformeVentaCursos'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        "order": [[1, "desc"]]
    });
}