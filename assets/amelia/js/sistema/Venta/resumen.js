$(document).ready(function () {
       recargaLaPagina();
    cargaCbxAll();
    $("#btn_back").click(function () {
        // retrocede una pagina
        limpiarFormulario();
        // history.back();
    });


    $("#btn_aceptar").click(function () {
        if (validarRequeridoForm($("#frm_rechazo"))) {
            rechazarFicha();
            $('#modal_rechazo').modal('hide');
            history.back();
        } else {
            event.stopPropagation();
        }
    });



    $("#btn_rechazar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        abrirRechazo(ficha);
    });

    $("#btn_aprobar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        aprobarFicha(ficha);
    });
 

    contarCaracteres("comentario", "text-out", 200);
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    inicializaControles();

    $("#btn_enviar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        validaPreEnvioFicha(ficha);
    });

    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });
});


function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() { 
    var id_ficha = "";
    var id_oc = "";
    id_ficha = $("#id_ficha").val();
    id_oc = $("#cbx_orden_compra").val();

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosAlumnos(result);
        },
        url: "../cargaDatosOC"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../cargaDatosOCDocumentoResumen"
    });

}

function cargaTablaDatosAlumnos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut"},
            {"mDataProp": "nombre"},
            {"mDataProp": "fq"},
            {"mDataProp": "valor_otic"},
            {"mDataProp": "valor_empresa"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "valor_total"},
            {"mDataProp": "num_orden_compra"}
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            }

        ],
        pageLength: 5,
        responsive: false,
        dom: '<"clear">'

    });

    $('[data-toggle="tooltip"]').tooltip();

}


function abrirRechazo(id) {
    $("#modal_id_ficha_rechazo").text(id);
    $("#id_ficha_rechazo").val(id);
    $("#modal_rechazo").modal("show");
}

function rechazarFicha() {
    
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_rechazo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function (isConfirm) {
                        if (isConfirm) {
                           //  history.back();
                        }
                    });


        },
        url: "../rechazarFicha"
    });

}

function aprobarFicha(id) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            history.back();
                        }
                    });
        },
        url: "../aprobarFicha"
    });
}
