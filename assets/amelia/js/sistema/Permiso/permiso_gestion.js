var PermiHidnn;
var PermiHidnn2;

$(document).ready(function () {
    recargaLaPagina();

    $('#permisos_no_otrotgados').bootstrapDualListbox({
        selectorMinimalHeight: 160
    });

    $('#btn_derecha').click(function () {
        $('#cbx_permisos_no_otorgados_visible option:selected').remove().appendTo('#cbx_permisos_otorgados_visible');
        $('#cbx_permisos_no_otorgados').html($('#cbx_permisos_no_otorgados_visible').html());
        $('#cbx_permisos_otorgados').html($('#cbx_permisos_otorgados_visible').html());

    });

    $('#btn_izquierda').click(function () {

        $('#cbx_permisos_otorgados_visible option:selected').remove().appendTo('#cbx_permisos_no_otorgados_visible');
        $('#cbx_permisos_otorgados').html($('#cbx_permisos_otorgados_visible').html());
        $('#cbx_permisos_no_otorgados').html($('#cbx_permisos_no_otorgados_visible').html());
    });



    listarItemsNoOtorgadoslbx();
    listarItemsOtorgadoslbx();

    $('#search1').bind('keypress', function (e) {

        var inicio = $('#search1').val() + String.fromCharCode(e.keyCode);
        actualizaLista(inicio);

    });

    $('#search1').bind('keyup', function (e) {
        var actual = $('#search1').val().trim();
        if (actual == "") {
            var inicio = actual.substr(0, actual.length - 1);
            actualizaLista(inicio);
        }

    });

    PermiHidnn = $('#cbx_permisos_otorgados');
    $('#cbx_permisos_otorgados_visible').html(PermiHidnn.html());

    $('#search2').bind('keypress', function (a) {

        var inicio2 = $('#search2').val() + String.fromCharCode(a.keyCode);
        actualizaLista2(inicio2);

    });

    $('#search2').bind('keyup', function (a) {

        var actual2 = $('#search2').val().trim();

        if (actual2 == "") {
            var inicio2 = actual2.substr(0, actual2.length - 1);
            actualizaLista2(inicio2);
        }
    });

    PermiHidnn2 = $('#cbx_permisos_no_otorgados');
    $('#cbx_permisos_no_otorgados_visible').html(PermiHidnn2.html());

    $("#btn_derecha").click(function () {
        OtorgarPermisosPerfil();
    });

    $("#btn_izquierda").click(function () {
        QuitarPermisosPerfil();
    });

});

function actualizaLista2(inicio2) {
    var y = PermiHidnn2.clone();
    inicio2 = inicio2.toLowerCase();
    $('#cbx_permisos_no_otorgados_visible').html("");
    var verifica2 = inicio2.trim();

    if (verifica2.length == 0) {
        $('#cbx_permisos_no_otorgados_visible').html($('#cbx_permisos_no_otorgados').html());
        return;
    }
    y.find('option[title^="' + inicio2 + '"]').each(function () {
        $('#cbx_permisos_no_otorgados_visible').append($(this));
    });
}



function actualizaLista(inicio) {
    var x = PermiHidnn.clone();
    inicio = inicio.toLowerCase();

    $('#cbx_permisos_otorgados_visible').html("");

    var verifica = inicio.trim();

    if (verifica.length == 0) {
        $('#cbx_permisos_otorgados_visible').html($('#cbx_permisos_otorgados').html());
        return;
    }


    x.find('option[title^="' + inicio + '"]').each(function () {
        $('#cbx_permisos_otorgados_visible').append($(this));
    });
}

function listarItemsNoOtorgadoslbx() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id: $("#id_perfil").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCBX("cbx_permisos_no_otorgados", result);

            $("#cbx_permisos_no_otorgados option[value='']").remove();
        },
        url: "../listarItemsNoOtorgados"
    });
}

function listarItemsOtorgadoslbx() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id: $("#id_perfil").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {


        },
        success: function (result) {
            cargaCBX("cbx_permisos_otorgados", result);
            $("#cbx_permisos_otorgados option[value='']").remove();

        },
        url: "../listarItemsOtorgados"
    });

}

function OtorgarPermisosPerfil() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_perfil: $("#id_perfil").val(),
            cbx_permisos_otorgados_visible: $("#cbx_permisos_otorgados_visible").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {

            resultado = result[0].respuesta;

            toastr.success('Permisos otorgados!');

        },
        url: "../OtorgarPermisosPerfil" //Hace referencia a la funcion del controlador que realiza la aacion de eitar.
    });
}

function QuitarPermisosPerfil() {



    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_perfil: $("#id_perfil").val(),
            cbx_permisos_no_otorgados_visible: $("#cbx_permisos_no_otorgados_visible").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            resultado = result[0].respuesta;

            toastr.warning('Permisos removidos!');

        },
        url: "../QuitarPermisosPerfil" //Hace referencia a la funcion del controlador que realiza la aacion de eitar.
    });
}
