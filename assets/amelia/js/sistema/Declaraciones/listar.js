$(document).ready(function () {
	$('#demo').hide();
	$('#exp').hide();

	recargaLaPagina();
	llamaDatosControles();

	$("#confirmarR").click(function () {
		if (validarRequeridoForm($("#frm_dj_registro"))) {
			registrarDJ();
		}
	});

	$("#reset").click(function () {
		location.reload();

	});


	$("#id_ficha_v").select2({
		placeholder: "Todos",
		allowClear: true
	});

	$("#id_ficha_v").change(function () {

		if ($("#id_ficha_v").val() != "") {
			llamaDatosAlumnos($("#id_ficha_v").val());
		}
	});
});


function cargaCombo(midata) {
	$('#id_ficha_v').select2({
		placeholder: "Todos",
		allowClear: true,
		data: midata
	});
}

function llamaDatosControles() {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'GET',
		//  data: $('#frm').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaCombo(result, 'id_ficha_v');
		},
		url: "Listar/getFichas"
	});
}

function llamaDatosAlumnos(id) {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {
			id_ficha: id
		},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			console.log(textStatus + errorThrown);
		},
		success: function (result) {
			mostrartablaAlumnos(result);

		},
		url: "Listar/alumnosListar"
	});
}


function mostrartablaAlumnos(result) {
	$('#demo').show();
	$('#exp').show();

	if ($.fn.dataTable.isDataTable('#listaAlm')) {
		$('#listaAlm').DataTable().destroy();
	}
	var cnt = 0;
	var ta = $('#listaAlm').DataTable({
		data: result,
		"ordering": false,
		"aoColumns": [{
				"mDataProp": null,
				 sWidth: "10%"
			},
			{
				"mDataProp": "rut",
				sWidth: "25%"
			},
			{
				"mDataProp": "nombre",
				sWidth: "45%"
			},
			{
				"mDataProp": null,
				"bSortable": false,
				sWidth: "20%",
				"mRender": function (o) {
					var html = "";
					if (o.declaracion_jurada === '1') {
						html += "<div  class='i-checks'>";
						html += "<input id='chkAst" + cnt + "' type='checkbox' checked value='1' name='chkAst" + cnt + "'>";
						html += "<i></i>";
					} else {
						html += "<div  class='i-checks'>";
						html += "<input id='chkAst" + cnt + "' type='checkbox' value='0' name='chkAst" + cnt + "'>";
						html += "<i></i>";
					}
					html += "<div class='btn-group'>";
					html += "<label>";
					html += "<input id='id_alm" + cnt + "' type='hidden' value='" + o.id_detalle_alumno + "' name='id_alm" + cnt + "'>";
					html += "<i></i>";
					html += "  </label>";
					html += "</div>";
					cnt++;
					return html;
				}
			}
		],
		"columnDefs": [{
				"searchable": false,
				"orderable": false,
				"targets": 0
			}],
		"order": [
			[1, 'asc']
		],
		pageLength: 2000000,
		responsive: false,
		dom: '<"html5buttons"B>lTfgitp',
		"bLengthChange": false,
		buttons: [{
				extend: 'copy'
			}, {
				extend: 'csv'
			}, {
				extend: 'excel',
				title: 'DJ'
			}, {
				extend: 'pdf',
				title: 'DJ'
			}, {
				extend: 'print',
				customize: function (win) {
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					$(win.document.body).find('table')
							.addClass('compact')
							.css('font-size', 'inherit');
				}
			}]
	});

	ta.on('order.dt search.dt', function () {
		ta.column(0, {
			search: 'applied',
			order: 'applied'
		}).nodes().each(function (cell, h) {
			cell.innerHTML = h + 1;
		});
	}).draw();

	$('.i-checks').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green'
	});
	$('#numRows').val(ta.column(0).data().length);
	var num_row = $('#numRows').val();
	if (num_row == 0) {
		$('#confirmarR').prop('disabled', true);
	} else {
		$('#confirmarR').prop('disabled', false);
	}
}

function registrarDJ() {
	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#frm_dj_registro').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//alerta('Asistencia', errorThrown, 'Debe seleccionar un relator !');
		},
		success: function (result) {

			swal({
				title: result[0].titulo,
				text: result[0].mensaje,
				type: result[0].tipo_alerta,
				confirmButtonText: "Ok"
			},
					function (isConfirm) {
						if (isConfirm) {
							location.href = "../Declaraciones/Listar";
						}
					});
		},
		url: "Listar/alumnosRegistrarDJ/"
	});
}