$( document ).ready(function() {
    listarEncuestas();

    $('#btn_nueva_encuesta').click(function(){
        $("#tabla_encuestas").animate({
            height: "toggle"
        }, 200, function () {
            $(this).stop(true, true);
            $("#frm_encuesta_satisfaccion").animate({
                height: "toggle"
            }, 200, function () {
                $(this).stop(true, true);
            });
            cargaCBXRelator();
            getEncuesta();
            $('#btn_nueva_encuesta').hide();
        });
    });

    $('#btn_descartar_encuesta').click(function(){
        $("#frm_encuesta_satisfaccion").animate({
            height: "toggle"
        }, 200, function () {
            $(this).stop(true, true);
            $("#tabla_encuestas").animate({
                height: "toggle"
            }, 200, function () {
                $(this).stop(true, true);
            });
            $('#btn_nueva_encuesta').show();
        });
    });

    $('#frm_encuesta_satisfaccion').submit(function(){
        guardarEncuesta();
        event.preventDefault();
    });


    $('#tbl_datos_alumnos input:radio').addClass('input_hidden');
    $('#tbl_datos_alumnos label').click(function() {
        $(this).addClass('selected').siblings().removeClass('selected');
    });

});

function listarEncuestas(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: $('#id_ficha').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('#nombre_curso').text(result[0].nombre_curso);
            $('#num_ficha').text(result[0].num_ficha);
            $('#razon_social').text(result[0].razon_social);
            $('#fecha_inicio').text(result[0].fecha_inicio);
            $('#dias_de_clases').text(result[0].dias);
            $('#relator').text(result[0].relator);
            $('#horario').text(result[0].hora_inicio+'hrs. a '+result[0].hora_termino+'hrs.');
            cargaTablaAlumnos(result);
        },
        url: "../listarAlumnos"
    });
}

function cargaTablaAlumnos(data) {
    var html="";
    $("#tbl_datos_alumnos tbody").html('');
    $.each(data, function( i, item ) {
        html += '<tr>';
        html += '<td>';
        html += (i+1);
        html += '</td><td>';
        html += item.nombre_alumno;
        html += '</td><td>';
        html += item.telefono_alumno;
        html += '</td><td>';
        html += '<select name="encuestador_'+item.id_alumno+'" id="encuestador_'+item.id_alumno+'"><option value="1" selected>Marianela</option><option value="2">Viviam</option><option value="3">Rafaela</option></select>';
        html += '</td><td>';
        html += '<input type="radio" name="radio_relator'+item.id_alumno+'" id="radio_relator_'+item.id_alumno+'_1" value="1">';
        html += '<label for="radio_relator_'+item.id_alumno+'_1"><img class="like_button" src="../../../../assets/img/like.png" /></label>';
        html += '<input type="radio" name="radio_relator'+item.id_alumno+'" id="radio_relator_'+item.id_alumno+'_2" value="0">';
        html += '<label for="radio_relator_'+item.id_alumno+'_2"><img class="like_button" src="../../../../assets/img/dislike.png" /></label>';
        html += '</td><td>';
        html += '<input type="radio" name="radio_equipos'+item.id_alumno+'" id="radio_equipos_'+item.id_alumno+'_1" value="1">';
        html += '<label for="radio_equipos_'+item.id_alumno+'_1"><img class="like_button" src="../../../../assets/img/like.png" /></label>';
        html += '<input type="radio" name="radio_equipos'+item.id_alumno+'" id="radio_equipos_'+item.id_alumno+'_2" value="0">';
        html += '<label for="radio_equipos_'+item.id_alumno+'_2"><img class="like_button" src="../../../../assets/img/dislike.png" /></label>';
        html += '</td><td>';
        html += '<input type="radio" name="radio_curso'+item.id_alumno+'" id="radio_curso_'+item.id_alumno+'_1" value="1">';
        html += '<label for="radio_curso_'+item.id_alumno+'_1"><img class="like_button" src="../../../../assets/img/like.png" /></label>';
        html += '<input type="radio" name="radio_curso'+item.id_alumno+'" id="radio_curso_'+item.id_alumno+'_2" value="0">';
        html += '<label for="radio_curso_'+item.id_alumno+'_2"><img class="like_button" src="../../../../assets/img/dislike.png" /></label>';
        html += '</td><td>';
        html += '<textarea name="comentarios_'+item.id_alumno+'" id="comentarios_'+item.id_alumno+'" cols="50" rows="3"></textarea>';
        html += '</td><td>';
        html += '<button type="button" onclick="guardarEncuestaAlumno('+item.id_alumno+', '+item.id_ficha+')" class="btn btn-primary btn-sm">Guardar</button>';
        html += '</td>';
        html += '</tr>';
    });
    $("#tbl_datos_alumnos tbody").html(html);
}

    function verEncuesta(id_encuesta){
        $(".modal-title").append(id_encuesta);
        $(".modal-title2").append($('#num_ficha_hidden').val());
        $('#modal_encuesta').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#modal_encuesta').modal('show');

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_encuesta: id_encuesta},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                setRespuestas(result);
            },
            url: "../getEncuestaById"
        });
    }

    function setRespuestas(data){
        $('#tbl_respuestas :radio').attr('disabled', false);
        $.each( data, function( i, item ) {
            if(item.id_pregunta == 20){
                $('#respuesta'+item.id_pregunta).text(item.respuesta);
            }else{
                $('#tbl_respuestas input[name=respuesta'+item.id_pregunta+'][value="'+item.respuesta+'"]').prop('checked', true);
            }
        });
        $('#tbl_respuestas :radio:not(:checked)').attr('disabled', true);
        $('#tbl_respuestas textarea').attr('disabled', true);
        //$('').attr('disabled', true);
    }

    function getEncuesta(){
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                showEncuesta(result);
            },
            url: "../getEncuestaSatisfaccion"
        });
    }

function showEncuesta(data){
    var categorias = [];
    var preguntas = [];
    var html = "";
    $('#frm_encuesta_satisfaccion table').html('');

    //AQUI SE METEN LAS CATEGORIAS EN UN ARRAY, PARA PODER CONSTRUIR LA TABLA POSTERIORMENTE
    for (var index = 0; index < (data[0].id_categorias.split("&")).length; index++) {
        var categoria= {};
        categoria.id_categoria = data[0].id_categorias.split("&")[index];
        categoria.nombre_categoria = data[0].categorias.split("&")[index];
        categorias.push(categoria);
    }

    //LO MISMO SE HACE CON LAS PREGUNTAS
    $.each( data, function( i, item ) {
        var pregunta ={};
        pregunta.id_pregunta = item.id_pregunta;
        pregunta.num_pregunta = item.num_pregunta;
        pregunta.id_categoria_encuesta = item.id_categoria_encuesta;
        pregunta.pregunta = item.pregunta;
        preguntas.push(pregunta);
    });

    //AQUI PRIMERO SE INSERTAN LAS CATEGORIAS EN LA TABLA CON SU RESPECTIVO ID Y CSS
    $.each( categorias, function( i, item ) {
        if(item.id_categoria == 6){
            html += '<tr id="categoria'+item.id_categoria+'" style="color:#fff;background-color:#2e74b4;"><td style="width:72%; text-align:left">';
            html += item.nombre_categoria;
            html += '</td><td align="center" style="width: 4%;"></td><td align="center" style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center"  style="width: 4%;"></td><td align="center" style="width: 4%;"></td></tr>';
        }else{
            html += '<tr id="categoria'+item.id_categoria+'" style="color:#fff;background-color:#2e74b4;"><td style="width:72%; text-align:left">';
            html += item.nombre_categoria;
            html += '</td><td align="center" style="width: 4%;">1</td><td align="center" style="width: 4%;">2</td><td align="center"  style="width: 4%;">3</td><td align="center"  style="width: 4%;">4</td><td align="center"  style="width: 4%;">5</td><td align="center"  style="width: 4%;">6</td><td align="center" style="width: 4%;">7</td></tr>';
        }
    });
    $('#frm_encuesta_satisfaccion table').html(html);
        
    // Y AQUÍ SE INSERTA CADA PREGUNTA EN SU RESPECTIVA CATEGORIA, PREGUNTANDO POR EL ID DE CATEGORIA QUE INSERTAMOS EN EL $.each ANTERIOR
    $.each( preguntas.reverse(), function( i, item ) {
        var innerhtml = "";
        if(item.id_categoria_encuesta == 6){
            innerhtml += '<tr><td><br></td><tr>';
            innerhtml += '<tr id="pregunta'+item.id_pregunta+'"><td>';
            innerhtml += '<textarea id="respuesta'+item.id_pregunta+'" name="respuesta'+item.id_pregunta+'" rows="4" cols="50"></textarea>';
            innerhtml += '</td></tr>';
            $("tr#categoria"+item.id_categoria_encuesta).after(innerhtml);
        }else{
            innerhtml += '<tr style="border-bottom: 0.7px solid #bfbfbf;" id="pregunta'+item.id_pregunta+'"><td style="width:72%;color:#000;" align="left">';
            if(item.num_pregunta == 0){
                innerhtml += item.pregunta;
            }else{
                innerhtml += item.num_pregunta +'.\t'+ item.pregunta;
            }
            innerhtml += '</td>';
            for (var index = 0; index < 7; index++) {
                innerhtml += '<td style="width: 4%;text-align:center;" ><input type="radio" name="respuesta'+item.id_pregunta+'" value="'+(index+1)+'"></td>';
            }
            innerhtml += '</tr>';
            $("tr#categoria"+item.id_categoria_encuesta).after(innerhtml);
        }
    });
}

function guardarEncuesta(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_encuesta_satisfaccion').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            location.reload();
        },
        url: "../setEncuestaSatisfaccion"
    });
}

function cargaCBXRelator(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data:{id_ficha: $('#id_ficha').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX('id_relator', result);
        },
        url: "../getRelatoresCBX"
    });
}

function guardarEncuestaAlumno(id_alumno, id_ficha){
    swal({
        title: "Está segura(o)?",
        text: "Si guarda esta encuesta se quitará de esta lista",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Guardar encuesta!",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false
      },
      function(){
          
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_ficha: id_ficha, id_alumno: id_alumno, id_encuestador: $('#encuestador_'+id_alumno+'').val(), relator: $('input[name=radio_relator'+id_alumno+']:checked').val(), equipos: $('input[name=radio_equipos'+id_alumno+']:checked').val(), curso: $('input[name=radio_curso'+id_alumno+']:checked').val(), comentarios: $('textarea[name=comentarios_'+id_alumno+']').val()},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                if(result[0].respuesta == '0'){
                    swal({
                        title: "Error al ingresar la encuesta!",
                        text: "El alumno ya registra una encuesta con esta ficha en el sistema",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Aceptar",
                        closeOnConfirm: false
                      },
                      function(){
                        location.reload();
                      });
                }else{
                    swal({
                        title: "Encuesta Registrada",
                        text: "El ID de la encuesta es: "+result[0].respuesta+".",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Aceptar",
                        closeOnConfirm: false
                      },
                      function(){
                        location.reload();
                      });
                }
            },
            url: "../setEncuestaPrimerDia"
        });
      });
}