$(document).ready(function () {


    $("#btn_buscar_ficha").click(function () {


        listarEtiquetas();

    });




});


function cargaTablaEtiquetas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }

    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut"},
            {"mDataProp": "nombre"},
            {"mDataProp": "apellido_paterno"},
            {"mDataProp": "apellido_materno"},
            {"mDataProp": "promedio"},
            {"mDataProp": "empresa"}

        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Etiquetas'
            }, {
                extend: 'pdf',
                title: 'Etiquetas'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [0, 'asc']
    });


}

function listarEtiquetas() {

    $.ajax({
        async: false,
        cache: true,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaEtiquetas(result);
        },
        url: "Listar/buscarEtiquetas"
    });
}

 