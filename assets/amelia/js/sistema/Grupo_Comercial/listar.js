$(document).ready(function () {

    llamaGruposComerciales();
    llamaResumenGruposComerciales();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $('#select_grupo_comercial').change(function () {

        $.ajax({
            url: "Listar/get_grupo_comercial",
            type: 'POST',
            dataType: "json",
            data: { id_grupo_comercial: $('#select_grupo_comercial').val() },
            beforeSend: function () {
                $("#cargando").html('<img style="text-align: center;" src="../../assets/img/timer.gif">');
            },
            success: function (result) {
                cargaGrupoComercial(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                $('#render-html').html('<p style="color:red;text-align:center;">Error en la carga, intente nuevamente.</p>');
            },
            complete: function () {
                $("#cargando").html("");
            },
            async: true,
            cache: false
        });
        event.preventDefault();
    });

    $('#frm_nuevo_usuario').submit(function () {

        $.ajax({
            url: "Listar/set_usuario_grupo_comercial",
            type: 'POST',
            dataType: "json",
            data: $('#frm_nuevo_usuario').serialize(),
            success: function (result) {
                if (result[0].respuesta != 0) {
                    swal({
                        title: 'Ingresado',
                        text: "Grupo ingresado correctamente",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar',
                        allowOutsideClick: false
                    }).then(function () {
                        location.reload();
                    });
                } else {
                    swal({
                        title: 'Error',
                        text: "Hubo un error al ingresar un nuevo grupo comercial",
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar',
                        allowOutsideClick: false
                    }).then(function () {
                        location.reload();
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                swal({
                    title: 'Error',
                    text: "Hubo un error en la solicitud",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                    allowOutsideClick: false
                }).then(function () {
                    location.reload();
                });
            },
            async: true,
            cache: false
        });
        event.preventDefault();
    });

    $('#frm_nuevo_grupo').submit(function () {

        $.ajax({
            url: "Listar/set_grupo_comercial",
            type: 'POST',
            dataType: "json",
            data: $('#frm_nuevo_grupo').serialize(),
            success: function (result) {
                if (result[0].respuesta != 0) {
                    swal({
                        title: 'Ingresado',
                        text: "Grupo ingresado correctamente",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar',
                        allowOutsideClick: false
                    }).then(function () {
                        location.reload();
                    });
                } else {
                    swal({
                        title: 'Error',
                        text: "Hubo un error al ingresar un nuevo grupo comercial",
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar',
                        allowOutsideClick: false
                    }).then(function () {
                        location.reload();
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                swal({
                    title: 'Error',
                    text: "Hubo un error en la solicitud",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                    allowOutsideClick: false
                }).then(function () {
                    location.reload();
                });
            },
            async: true,
            cache: false
        });
        event.preventDefault();
    });

    $("#myModal").modal({
        backdrop: 'static',
        keyboard: false
    });


});

function llamaGruposComerciales() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, 'select_grupo_comercial');
            cargaCombo(result, 'grupo_comercial');
        },
        url: "Listar/get_grupos_comerciales_cbx"
    });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, 'usuario');
            cargaCombo(result, 'usuario2');
        },
        url: "Listar/get_usuarios_cbx"
    });

}

function cargaGrupoComercial(data) {
    var contenido = "";
    $('#render-html').html(contenido);
    if (data.length == 0) {
        contenido = "No existen registros según su criterio de búsqueda";
    } else {
        contenido += '<table id="table_grupo_comercial" class="table table-hover" style="border: 1px solid #e7eaec;">';
        contenido += '<thead>';
        contenido += '<tr>';
        contenido += '<th>N° </th>';
        contenido += '<th>Nombre</th>';
        contenido += '<th>Tipo de Usuario</th>';
        contenido += '<th>Correo Electrónico</th>';
        contenido += '<th>Acción</th>';
        contenido += '</tr>';
        contenido += '</thead>';
        contenido += '<tbody>';
        $.each(data, function (i, item) {
            var color = item.jefe_grupo == 1 ? 'green' : '';
            contenido += '<tr style="color:' + color + ';">';
            contenido += '<td>';
            contenido += (i + 1);
            contenido += '</td>';
            contenido += '<td>';
            contenido += item.nombre;
            contenido += '</td>';
            contenido += '<td>';
            contenido += item.tipo_usuario;
            contenido += '</td>';
            contenido += '<td><a href="mailto:' + item.correo + '">';
            contenido += item.correo;
            contenido += '</a></td>';
            contenido += '<td>';
            contenido += '<div class="dropdown">';
            contenido += '<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Acción</button>';
            contenido += '<ul class="dropdown-menu pull-right">';
            contenido += '<li><a onclick="UsuarioJefe(' + item.id_usuario + ','+item.id_grupo+')">Marcar/Desmarcar como Jefe</a></li>';
            contenido += '<li><a onclick="quitarUsuario(' + item.id_usuario + ','+item.id_grupo+',';
            contenido += "'" + item.nombre + "'";
            contenido += ')">Quitar Usuario</a></li>';
            contenido += '</ul>';
            contenido += '</div>';
            contenido += '</td>';
            contenido += '</tr>';
        });
        contenido += '</tbody>';
        contenido += '</table>';
    }
    $('#render-html').html(contenido);
}

function llamaResumenGruposComerciales() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaResumenGruposComerciales(result);
        },
        url: "Listar/get_grupocomercial_resumen"
    });
}

function cargaResumenGruposComerciales(data) {
    console.log(data);
    var contenido = "";
    //Contenedor Grupo 1
    $('#grupo1').html(contenido);
    if (data.length == 0) {
        contenido = "No existen registros según su criterio de búsqueda";
    } else {
        $("#nombregrupo1").text(data[0].nombre_grupo_comercial);
        var nombresgrupo = data[0].nombre.split(";");
        console.log(nombresgrupo);
        $.each(nombresgrupo, function (i, item) {
            contenido += '<li>' + item + '</li>';
        });
        $('#grupo1').html(contenido);
    }

    //Contenedor Grupo 2    
    contenido = "";
    if (data.length == 0) {
        contenido = "No existen registros según su criterio de búsqueda";
    } else {
        $("#nombregrupo2").text(data[1].nombre_grupo_comercial);
        var nombresgrupo = data[1].nombre.split(";");
        console.log(nombresgrupo);
        $.each(nombresgrupo, function (i, item) {
            contenido += '<li>' + item + '</li>';
        });
        $('#grupo2').html(contenido);

    }
    //Contenedor Grupo 3
    contenido = "";
    if (data.length == 0) {
        contenido = "No existen registros según su criterio de búsqueda";
    } else {
        $("#nombregrupo3").text(data[2].nombre_grupo_comercial);
        var nombresgrupo = data[2].nombre.split(";");
        console.log(nombresgrupo);
        $.each(nombresgrupo, function (i, item) {
            contenido += '<li>' + item + '</li>';
        });
        $('#grupo3').html(contenido);
    }
    //Contenedor Grupo 4
    contenido = "";
    if (data.length == 0) {
        contenido = "No existen registros según su criterio de búsqueda";
    } else {
        $("#nombregrupo4").text(data[3].nombre_grupo_comercial);
        var nombresgrupo = data[3].nombre.split(";");
        console.log(nombresgrupo);
        $.each(nombresgrupo, function (i, item) {
            contenido += '<li>' + item + '</li>';
        });
        $('#grupo4').html(contenido);
    }
    //Contenedor Grupo 5
    contenido = "";
    if (data.length == 0) {
        contenido = "No existen registros según su criterio de búsqueda";
    } else {
        $("#nombregrupo5").text(data[4].nombre_grupo_comercial);
        var nombresgrupo = data[4].nombre.split(";");
        console.log(nombresgrupo);
        $.each(nombresgrupo, function (i, item) {
            contenido += '<li>' + item + '</li>';
        });
        $('#grupo5').html(contenido);
    }

    //Contenedor Grupo 6
    contenido = "";
    if (data.length == 0) {
        contenido = "No existen registros según su criterio de búsqueda";
    } else {
        $("#nombregrupo6").text(data[5].nombre_grupo_comercial);
        var nombresgrupo = data[5].nombre.split(";");
        console.log(nombresgrupo);
        $.each(nombresgrupo, function (i, item) {
            contenido += '<li>' + item + '</li>';
        });
        $('#grupo6').html(contenido);
    }
}

function quitarUsuario(id_usuario, grupo_comercial, nombre) {
    swal({
        title: 'Está seguro(a)?',
        text: "Se quitará al siguiente usuario del grupo comercial: " + nombre,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        allowOutsideClick: false
    }).then(function () {
        $.ajax({
            url: "Listar/quitar_usuario_grupo_comercial",
            type: 'POST',
            dataType: "json",
            data: { id_usuario: id_usuario, id_grupo_comercial: grupo_comercial},
            success: function (result) {
                if (result[0].respuesta != 0) {
                    swal({
                        title: 'Eliminado',
                        text: "Usuario eliminado correctamente",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar',
                        allowOutsideClick: false
                    }).then(function () {
                        location.reload();
                    });
                } else {
                    swal({
                        title: 'Error',
                        text: "Hubo un error al quitar el usuario",
                        type: 'error',
                        showCancelButton: false,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar',
                        allowOutsideClick: false
                    }).then(function () {
                        location.reload();
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                swal({
                    title: 'Error',
                    text: "Hubo un error en la solicitud",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                    allowOutsideClick: false
                }).then(function () {
                    location.reload();
                });

            },
            async: true,
            cache: false
        });
    });
}

function UsuarioJefe(id_usuario, id_grupo_comercial){
    $.ajax({
        url: "Listar/marcar_jefe_grupo_comercial",
        type: 'POST',
        dataType: "json",
        data: { id_usuario: id_usuario, id_grupo_comercial: id_grupo_comercial},
        success: function (result) {
            if (result[0].respuesta != 0) {
                swal({
                    title: 'Listo',
                    text: "Acción realizada correctamente",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                    allowOutsideClick: false
                }).then(function () {
                    location.reload();
                });
            } else {
                swal({
                    title: 'Error',
                    text: "Hubo un error al ejecutar la acción",
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar',
                    allowOutsideClick: false
                }).then(function () {
                    location.reload();
                });
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: 'Error',
                text: "Hubo un error en la solicitud",
                type: 'error',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Aceptar',
                allowOutsideClick: false
            }).then(function () {
                location.reload();
            });

        },
        async: true,
        cache: false
    });
}

function cargaCombo(midata, item) {
    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
}
