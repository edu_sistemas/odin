$(document).ready(function () {

    $('#tabla_gestion').hide();
    DatosDistribucionVentas(1);



    $("#btnMostrarTodos").click(function(){
        $("#modalTodos").modal();
    });

    $('#ejecutivos').on('change', function() {

     $("#tblclasifGestion tbody tr").each(function (index) {
     $(this).remove();
     });

     $("#tblclasifVenta tbody tr").each(function (index) {
     $(this).remove();
     });

     DatosDistribucionClientes();
     DatosDistribucionGestion();
     DatosTablaCompromisos(this.value);
     DatosTablaMetas(this.value);

 });

    cargaSelectEjecutivos();
    DatosTablaCompromisos(0);
    DatosTablaMetas(0);

});


var nombre_empresa = [];
var ventas_empresa = [];

var ventas_anterior = [];
var ventas_actual = [];

var nombre_tipo_cliente=["Con Ventas","Potencial", "Sin Ventas"];
var datos_tipo_cliente=[0,0,0];

var nombre_tipo_gestion=["Con Gestión","Sin Gestión"];
var datos_tipo_gestion=[0,0];


var desde='';
var hasta='';


var anio_anterior = (moment().year())-1;
var anio_actual = moment().year();

/* Declaracion de Graficos */

var barData_ranking = {
    labels: nombre_empresa,
    datasets: [
    {
        label: 'Ventas',
        backgroundColor: 'rgba(26,179,148,0.5)',
        borderColor: "rgba(26,179,148,0.7)",
        pointBackgroundColor: "rgba(26,179,148,1)",
        pointBorderColor: "#fff",
        data: ventas_empresa
    }
    ]
};

/*
var barOptions_ranking = {
    responsive: true,
    scales: {
        yAxes: [{
            ticks: {
                callback: function (value) {
                    return "$ " + parseInt(value).toLocaleString();
                }
            }
        }],
        xAxes: [{
            ticks: {
                callback: function(value) {
                    return value.substr(0, 10);//truncate
                },
            }
        }]
    },
    tooltips: {
        mode: 'label',
        callbacks: {
            title: function(tooltipItems, data) {
                var idx = tooltipItems[0].index;
                return data.labels[idx];//do something with title
            },
            label: function(tooltipItems, data) {
                return "$ " + parseInt(tooltipItems.yLabel).toLocaleString()
            }
        }
    }
};


/*****************************************/

/*
var lineData_venta = {
    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    datasets: [
    {
        label: anio_actual,
        backgroundColor: 'rgba(26,179,148,0.5)',
        borderColor: "rgba(26,179,148,0.7)",
        pointBackgroundColor: "rgba(26,179,148,1)",
        pointBorderColor: "#fff",
        data: ventas_actual
    },
    {
        label: anio_anterior,
        backgroundColor: 'rgba(220, 220, 220, 0.5)',
        pointBorderColor: "black",
        data: ventas_anterior
    }
    ]
};

var lineOptions_venta = {
    responsive: true,
    scales: {
        yAxes: [{
            ticks: {
                callback: function (value) {
                    return "$ " + parseInt(value).toLocaleString();
                }
            }
        }]},
        tooltips: {
            mode: 'label',
            callbacks: {
                label: function (tooltipItem, data) {
                    return data.datasets[tooltipItem.datasetIndex].label + ": " + "$ " + parseInt(tooltipItem.yLabel).toLocaleString();
                }
            }
        }
    };
    */

    /*****************************************/


    var pieData_distribucion = {
        labels: nombre_tipo_cliente,
        datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["rgba(26,179,148,0.5)", "rgba(225, 220, 20, 0.5)", "rgba(220, 220, 220, 0.5)"],
          data: datos_tipo_cliente
      }
      ]
  };


    var pieData_distribucion_ges = {
        labels: nombre_tipo_gestion,
        datasets: [
        {
          label: "Population (millions)",
          backgroundColor: ["rgba(26,179,148,0.5)", "rgba(220, 220, 220, 0.5)"],
          data: datos_tipo_gestion
      }
      ]
  };



  var pieOptions_distribucion = {
    responsive: true,
    legend: {
        position: 'top'
    },
    animation: {
        animateRotate: false,
        animateScale: true
    }
};



var gdc = document.getElementById("chartPieClasifVenta").getContext("2d");
window.chartClasifVenta = new Chart(gdc, {type: 'pie', data: pieData_distribucion, options: pieOptions_distribucion});

var gdcg = document.getElementById("chartPieClasifGest").getContext("2d");
window.chartClasifGestion = new Chart(gdcg, {type: 'pie', data: pieData_distribucion_ges, options: pieOptions_distribucion});

/* Fin Declaracion de Graficos */


/*

function CargaCalendario()
{

    var start =  moment().subtract(moment().month(),'month').startOf('month');
    var end = moment();

    desde=start.format("YYYY-MM-DD");
    hasta=end.format("YYYY-MM-DD");

    function cb(start, end) {
        $('#fechas').html('Desde: ' + start.format('DD-MM-YYYY') + ' Hasta: ' + end.format('DD-MM-YYYY'));
    }

    $('#rangofechas').daterangepicker({
        startDate: start,
        endDate: end,
        format: 'YYYY-MM-DD',
        ranges: {
        //   'Today': [moment(), moment()],
        //   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //'7 Días': [moment().subtract(6, 'days'), moment()],
        '30 Días': [moment().subtract(29, 'days'), moment()],
        'Mes Actual': [moment().startOf('month'), moment().endOf('month')],
        'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
        'Este Año' : [moment().subtract(moment().month(),'month').startOf('month'), moment()]
    },
    "locale": {
      "format": "DD-MM-YYYY",
      "separator": " - ",
      "applyLabel": "Aplicar",
      "cancelLabel": "Cancelar",
      "fromLabel": "Desde",
      "toLabel": "Hasta",
      "customRangeLabel": "Personalizar",
      "weekLabel": "S",
      "daysOfWeek": [
      "Do",
      "Lu",
      "Ma",
      "Mi",
      "Ju",
      "Vi",
      "Sa"
      ],
      "monthNames": [
      "Enero",
      "Febrero",
      "Marzo",
      "Abril",
      "Mayo",
      "Junio",
      "Julio",
      "Agosto",
      "Septiembre",
      "Octubre",
      "Noviembre",
      "Diciembre"
      ],
      "firstDay": 1
  }
}, cb);

    cb(start, end);
    
    $('#rangofechas').on('apply.daterangepicker', function(ev, picker) {

        //listener cambio de fechas seleccionadas
       //alert(picker.startDate.format('YYYY-MM-DD')+' a '+picker.endDate.format('YYYY-MM-DD'));

       desde=picker.startDate.format('YYYY-MM-DD');
       hasta=picker.endDate.format('YYYY-MM-DD');

       DatosRankingVentas();


   });

}
*/

function cargaSelectEjecutivos() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaComboEjecutivos(result);

        },
        url: "edashboard/getListaEjecutivos"
    });
}



function cargaComboEjecutivos(midata) {

    cargaCBX('ejecutivos', midata);

    if(midata.length<2)
    {
        $("#ejecutivos option[value='']").remove();
        $("#divEjecutivos").hide();
    }
    else
    {
        $("#ejecutivos option[value='']").text("Todos");
        $("#ejecutivos option[value='']").val("0");
    }

    DatosDistribucionGestion();   
    DatosDistribucionClientes();
}

//************** grafico pie numero 1 ***************//

function DatosDistribucionClientes()
{

    var ejecutivo=$("#ejecutivos").val();

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ejecutivo: ejecutivo },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            if(datos_tipo_cliente.length>0) { datos_tipo_cliente.length=0; }

            //alert("con ventas: " + result[0].conventas + "sinventas: " + result[0].sinventas + "total; " + result[0].total);
            
            var conventas=(parseInt(result[0].conventas));
            var sinventas=(parseInt(result[0].sinventas));
            var totventas=(parseInt(result[0].conventas) + parseInt(result[0].sinventas));
            var potencial=(parseInt(result[0].total) - totventas);
            

            datos_tipo_cliente.push(conventas);
            datos_tipo_cliente.push(potencial);
            datos_tipo_cliente.push(sinventas);



            window.chartClasifVenta.update();
            

        },
        url: "edashboard/getDistribucionCliente"
    });
}


document.getElementById("chartPieClasifVenta").onclick = function(evt)
{   


        var activePoints = chartClasifVenta.getElementsAtEvent(evt);

    if(activePoints.length > 0)
    {

        var clickedElementindex = activePoints[0]["_index"];

        var DatosFiltradosEjeVenta = chartClasifVenta.data.labels[clickedElementindex];

        var value = chartClasifVenta.data.datasets[0].data[clickedElementindex];
    }

            var filtradoVentas='';

            switch(DatosFiltradosEjeVenta)
            {
                case 'Potencial':
                filtradoVentas = 1;
                break;
                case 'Con Ventas':
                filtradoVentas = 2;
                break;
                case 'Sin Ventas':
                filtradoVentas = 3;
                break;
            }

//alert(filtradoVentas);
DatosDistribucionVentas(filtradoVentas);

}

function DatosDistribucionVentas(filtradoVentas)
{

    var ejecutivo=$("#ejecutivos").val();

    $("#tblclasifVenta tbody tr").each(function (index) {
    $(this).remove();
 });

    $('#tblclasifVenta tbody').append('<tr id="trwaitvent"><td colspan="4" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos</h3></td></tr>'); 

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { filtradoVentas: filtradoVentas , ejecutivo: ejecutivo},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

        $('#tabla_venta').show();

        $('#trwaitvent').remove();
        
         $(result).each(function (index) 
         {

               $('#tblclasifVenta tbody').append('<tr><td>'+result[index].Empresa+'</td><td>'+result[index].UltimaVenta+'</td><td>'+result[index].Fecha+'</td><td>$'+result[index].Monto+'</td></tr>'); 
});
         $('#tabla_gestion').hide();

        },
        url: "edashboard/getDatosFiltradosClientes"
    });
}

//************** fin grafico pie numero 1 ***************//


//************** grafico pie numero 2 ***************//

function DatosDistribucionGestion()
{

        var ejecutivo=$("#ejecutivos").val();

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ejecutivo: ejecutivo },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            if(datos_tipo_gestion.length>0) { datos_tipo_gestion.length=0; }

            var singestion=(result[0].SinGestion);
            var congestion=(result[0].ConGestion);

            datos_tipo_gestion.push(congestion);
            datos_tipo_gestion.push(singestion);

            //alert(congestion + singestion);

            window.chartClasifGestion.update();
            

        },
        url: "edashboard/getDistribucionGestion"
    });
}


document.getElementById("chartPieClasifGest").onclick = function(evt)
{   


        var activePoints = chartClasifGestion.getElementsAtEvent(evt);

    if(activePoints.length > 0)
    {

        var clickedElementindex = activePoints[0]["_index"];

        var DatosFiltradosEjeGestion = chartClasifGestion.data.labels[clickedElementindex];

        var value = chartClasifGestion.data.datasets[0].data[clickedElementindex];
    }

            var filtradoGestion='';

            switch(DatosFiltradosEjeGestion)
            {
                case 'Con Gestión':
                filtradoGestion = 1;
                break;
                case 'Sin Gestión':
                filtradoGestion = 2;
                break;
            }

DatosDistribucionGestiones(filtradoGestion);

}

function DatosDistribucionGestiones(filtradoGestion)
{

    var ejecutivo=$("#ejecutivos").val();

    $("#tblclasifGestion tbody tr").each(function (index) {
     $(this).remove();
 });

    $('#tblclasifGestion tbody').append('<tr id="trwaitGest"><td colspan="3" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>'); 

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { filtradoGestion: filtradoGestion, ejecutivo: ejecutivo},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

        $('#tabla_gestion').show();
        
        $('#trwaitGest').remove();

         $(result).each(function (index) 
         {
                $('#tblclasifGestion tbody').append('<tr><td>'+result[index].Empresa+'</td><td>'+result[index].TipoGestion+'</td><td>'+result[index].Fecha+'</td></tr>'); 

});
         $('#tabla_venta').hide();

        },
        url: "edashboard/getDatosFiltradosGestiones"
    });
}

//************** Compromisos ***************//


function DatosTablaCompromisos(ejecutivo)
{

    $("#tblclasifCompromiso tbody tr").each(function (index) {
    $(this).remove();
});

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ejecutivo: ejecutivo},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

         $(result).each(function (index) 
         {

            //alert(result[index].accion_sin_compromiso);
            var icono_sin_compromiso='';
            var id_accion='';

            if (result[index].accion_sin_compromiso == '0') 
            {
                id_accion='funchref("'+result[index].id_empresa+'","'+result[index].id+'")';
                icono_sin_compromiso='fa fa-exclamation text-warning fa-x2';
            } 
            else 
            {
                id_accion='';
                icono_sin_compromiso='';
            }
       

                $('#tblclasifCompromiso tbody').append("<tr onclick='"+id_accion+"'><td>"+result[index].cliente+"</td><td><i data-toggle='tooltip' title='Agregar Gestión' class='"+icono_sin_compromiso+"'></i> &nbsp;"+result[index].descripcion+"</td><td>"+result[index].fecha+"</td></tr>"); 
});

        },
        url: "edashboard/getDatosFiltradosTablaCompromisos"
    });
}

function funchref(id_empresa,id){
    //alert(id_empresa, id);
    window.location.href='Editar_gestion/index/'+id_empresa+'.'+id+'';
}
//************** FIN Compromisos ***************//

//************** Metas ***************//

function DatosTablaMetas(ejecutivo)
{
    //alert(ejecutivo);

    $("#tblclasifMetas tbody tr").each(function (index) {
    $(this).remove();
});

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ejecutivo: ejecutivo},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

        $(result).each(function (index) 
         {
            var icono_mes='';
            var icono_acum='';
            var boldglobal='';


            switch (result[index].empresa){
            case 'Global':
            boldglobal='font-weight: bold; font-size: 14px; border: 2px solid #ccc;';
            break;   
            default : '';             
            }

            switch (result[index].iconmes){
            case '1':
            icono_mes='fa fa-circle text-info fa-x2';
            break;
            case '2':
            icono_mes='fa fa-circle text-warning fa-x2';
            break;
            case '3':
            icono_mes='fa fa-circle text-danger fa-x2';
            break; 
            case '0':
            icono_mes='fa fa-circle text-primary fa-x2';
            break;
            //default : '';                 
            }
            switch (result[index].iconacum){
            case '1':
            icono_acum='fa fa-circle text-info fa-x2';
            break;          
            case '2':
            icono_acum='fa fa-circle text-warning fa-x2';
            break;          
            case '3':
            icono_acum='fa fa-circle text-danger fa-x2';
            break;
            case '0':
            icono_acum='fa fa-circle text-primary fa-x2';
            break;
            //default : '';
            }

            $('#tblclasifMetas tbody').append('<tr  style="'+boldglobal+'"><td>'+result[index].empresa+'</td><td><i class="'+icono_mes+'"></i> &nbsp; &nbsp;'+result[index].porcenmes+'%</td><td><i class="'+icono_acum+'"></i> &nbsp; &nbsp;'+result[index].porcenacum+'%</td></tr>'); 

});

        },
        url: "edashboard/getDatosFiltradosTablaMetas"
    });
}

//************** FIN Metas ***************//


/* Fin Grafico Distribucion Clientes */


/** Historico Gestiones **/

function DatosHistoricoGestion()
{

    var ejecutivo=$("#ejecutivos").val();

    $("#tblHistGestion tbody tr").each(function (index) {
     $(this).remove();
 });

    $("#tblHistGestionTotal tbody tr").each(function (index) {
     $(this).remove();
 });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ejecutivo: ejecutivo },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {



         $(result).each(function (index) 
         {
            var icon='';

            switch(result[index].tipo_evento)
            {
                case '1':
                icon='fa fa-phone';
                break;
                case '2':
                icon='fa fa-envelope';
                break;
                case '3':
                icon='fa fa-wechat';
                break;
                case '4':
                icon='fa fa-file-text';
                break;
                case '5':
                icon='fa fa-check-square';
                break;
                case '6':
                icon='fa fa-spinner';
                break;
            }
            if(index<4)
            {
                $('#tblHistGestion tbody').append('<tr><td><a href="cuenta">'+result[index].nombre+'</a></td><td>'+result[index].fecha_evento+' &nbsp; &nbsp; &nbsp; <i class="'+icon+' fa-x2"></i></td></tr>');
                //$('#tblHistGestion tr:last').after('<tr><td><a href="cuenta">'+result[index].nombre+'</a></td><td>'+result[index].fecha_evento+' &nbsp; &nbsp; &nbsp; <i class="'+icon+' fa-x2"></i></td></tr>');    
            }

            $('#tblHistGestionTotal tbody').append('<tr><td><a href="cuenta">'+result[index].nombre+'</a></td><td>'+result[index].fecha_evento+' &nbsp; &nbsp; &nbsp; <i class="'+icon+' fa-x2"></i></td></tr>');
            //$('#tblHistGestionTotal tr:last').after('<tr><td><a href="cuenta">'+result[index].nombre+'</a></td><td>'+result[index].fecha_evento+' &nbsp; &nbsp; &nbsp; <i class="'+icon+' fa-x2"></i></td></tr>');

        });

     },
     url: "edashboard/getHistoricoGestion"
 });
}


/** Fin Historico Gestiones **/