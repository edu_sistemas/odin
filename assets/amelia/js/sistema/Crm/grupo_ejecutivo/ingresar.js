
$(document).ready(function () {
    $("#btn_registrar").click(function () {

        if (validarRequeridoForm($("#frm_grupo_registro"))) {
            RegistraGrupo();
        }

    });

    contarCaracteres("descripcion", "text-out", 200);

    cargaSelectUsuarios();

});

function RegistraGrupo() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     
     */

     $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_grupo_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(  textStatus + errorThrown);
            alerta('Grupo Ejecutivo', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }
        },
        url: "Agregar/RegistraGrupo"
    });

 }


 function cargaSelectUsuarios() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaComboUsuarios(result);

        },
        url: "Agregar/getListaUsuario"
    });
}



function cargaComboUsuarios(midata) {

$("#responsable").select2({
        allowClear: true,
        //data: midata
    });

cargaCBX("responsable", midata);

    

    //$("#perfil option[value='0']").remove();
    
    //$("#responsable_grupo").val($("#id_grupo_seleccionado").val()).trigger("change");

}

