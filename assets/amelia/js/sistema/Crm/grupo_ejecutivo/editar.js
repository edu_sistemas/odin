
$(document).ready(function () {

    $("#btn_editar_grupo").click(function () {
        if (validarRequeridoForm($("#frm_grupo_ejecutivo"))) {
            EditarGrupo();
        }
    });

    cargaSelectUsuarios();

    contarCaracteres("descripcion", "text-out", 200);

    $("#descripcion").change();
    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });



});


function EditarGrupo() {

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_grupo_ejecutivo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(  textStatus + errorThrown);
            alerta('Grupo Ejecutivo', errorThrown, 'error');
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }

        },
        url: "../EditarGrupo"
    });

}


function cargaSelectUsuarios() {
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaComboUsuarios(result);

        },
        url: "../getListaUsuario"
    });
    }



    function cargaComboUsuarios(midata) {

        $("#responsable").select2({
            allowClear: true,
            data: midata
        });

    //$("#perfil option[value='0']").remove();
    
    $("#responsable").val($("#responsable_seleccionado").val()).trigger("change");

}




