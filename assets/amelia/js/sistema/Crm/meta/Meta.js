$(document).ready(function(){


    $("#btn_carga").click(function () {
        if ($("#file_carga_meta").val() != "") {

            send_file();
        } else {
            alerta('Metas', "Favor Seleccione un archivo de carga válido", 'warning');
        }
    });


});
        

function send_file() {

    //$('#btn_carga').prop("disabled", true);
    // $('#file_carga_empresa').prop("disabled", true);


    //limpia_carga();

    //$('#btn_carga').html('<i class="fa fa-upload"></i> Cargando..');
    var l = $('#btn_carga').ladda();

    l.ladda('start');

    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_carga_meta")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            //alerta('Empresa', jqXHR.responseText, 'error');
            console.log(jqXHR.responseText);
        },
        success: function (result) {
            l.ladda('stop');
            //$('#btn_carga').prop("disabled", false);
            //$('#btn_carga').html('<i class="fa fa-upload"></i> Subir ');
            if (result.valido) {
                alerta('Metas', result.mensaje+', '+result.registros+' Registros Cargados.' , 'success');
                //$('#btn_aprobar_carga').prop("disabled", false);
                //$('#btn_cancelar').prop("disabled", false);
                //$('#file_carga_meta').prop("disabled", false);
            } else {
                l.ladda('stop');
                alerta('Metas', result.mensaje, 'error');
            }
        },
        url: 'Meta/cargar_archivo_metas'
    });
}








