
$(document).ready(function () {


    $('.footable').footable();
    $('#tabla_cuenta').footable();

    llamaDatosGestiones(0);
    listDominios();

    $("#btnListarClientes").on("click",function(){
        showMdlClientesEjecutivos();
    });

    $("#btnCargaMasiva").on("click",function(){
        //console.log($("#excel").val());
       
        $("#mdlCargaMasiva").modal('toggle');
        $("#form-masiva")[0].reset();
         
    });

    $('#ejecutivos').on('change', function() 
    {
        var url=$("#RutaBase").val()+'/index/'+(this.value==""?0:this.value);
        location.href=url;
    });

    $('input.filtro_cliente_ejecutivo').on( 'keyup click', function () {
        var table = $('#tbl_clientes_ejecutivos').DataTable();
        table.search( this.value ).draw();
    } );

    var ejecutivo=$("#Var_ejecutivo").val();

    cargaSelectEjecutivos(ejecutivo);
    llamaDatosGestiones(ejecutivo);


    $("#form-masiva").on('submit',function(e){
        e.preventDefault();
        if($("#excel").val()=="")
        {
            swal("Carga Masiva Empresas","Debe seleccionar un archivo.","info");
            return 0;
        }

        var ext=$("#excel").val().substr($("#excel").val().lastIndexOf(".")+1,3);

        if(ext!="xls") { 
            swal("Carga Masiva Empresas","El archivo debe tener extension xls o xlsx.","info");
            return 0;
        }

        var datos = new FormData($("#form-masiva")[0]);

        $("#btn-cmasiva").html("<i class='fa fa-circle-o-notch fa-spin fa-fw'></i> Procesando");
        $("#btn-cmasiva").attr("disabled", "disabled");

        var url=$("#RutaBase").val()+'/CargaEmpresas';

        $.ajax({
            url: url,
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            type: "POST",
            dataType: "json",
            success: function(resultado){

                if(resultado.resultado=='OK')
                {
                    $("#mdlCargaMasiva").modal('toggle');
                    swal({
                        title: "Carga Masiva Empresas",
                        text: resultado.mensaje,
                        type: "success",
                        showCancelButton: false
                    },
                    function(){
                        window.location.reload(true);
                    });
                }
                else
                {
                    swal("Carga Masiva Empresas",resultado.mensaje,"warning");
                }

                $("#btn-cmasiva").html("Cargar");
                $("#btn-cmasiva").removeAttr("disabled");

            },  
            error: function(jqXHR, textStatus, errorThrown) {
          //console.log(jqXHR, textStatus, errorThrown);
          console.log(jqXHR);
          $("#btn-cmasiva").html("Cargar");
          $("#btn-cmasiva").removeAttr("disabled");
      }
  });

    });


});


/*
function cargaeEmpresasEjecutivos(ejecutivo){
    
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ejecutivo: ejecutivo },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
            //alerta('Nueva Cuenta', jqXHR.responseText, 'error');
        },
        success: function (result) {
        },
        url: "cuenta/getCliente"
    }); 
        
}
*/


function cargaSelectEjecutivos(ejecutivo) {

    var url=$("#RutaBase").val()+'/getListaEjecutivos';

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //  data: $('#frm').serialize(),
        data: { ejecutivo: 0 },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            //alert(result);
            cargaComboEjecutivos(result,ejecutivo);
        },
        url: url
    });
}

function cargaComboEjecutivos(midata,ejecutivo) {

    cargaCBX('ejecutivos', midata);
    //Cambio de texto seleccione a Todos
    //$('select option:contains("Seleccione")').text('Todos');


    $("#ejecutivos").val((ejecutivo==0?"":ejecutivo));



    //alert(midata.length);
    /*
    if(midata.length<2)
    {
        //$("#ejecutivos option[value='']").remove();
        $("#ejecutivos option:selected" ).text();
        //$("#divEjecutivos").hide();
    }
    else
    {
        $("#ejecutivos option:selected" ).text();
        $("#ejecutivos option[value='']").text("Seleccione");
        //$("#ejecutivos option[value='']").val("0");
    }
    */

}    

/* ELIMINAR CUENTA */

function confirmaEliminaCuenta(id)
{
    swal({
      title: "Desea Continuar?",
      text: "Esta acción eliminara la cuenta!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: false
  },
  function(){
      EliminarCuenta(id);
      location.reload();	  
  });
}


function EliminarCuenta(id){
	
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_empresa: id },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
            //alerta('Nueva Cuenta', jqXHR.responseText, 'error');
        },
        success: function (result) {
            if (result[0].reset == 1) {

                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);		  
				//$('#div_general').load(document.URL + ' #div_general');
              var tr = $("#tr_"+id); 
              tr.remove(); 
          }
      },
      url: "cuenta/getEliminarCuenta"
  });	

}

/* FIN ELIMINAR CUENTA */

/* ELIMINAR CUENTA */

function confirmaEliminaGestion(id)
{
    swal({
      title: "Desea Continuar?",
      text: "Esta acción eliminara la gestión!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: false
  },
  function(){
      EliminarGestion(id);  
  });


}


function EliminarGestion(id){

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_gestion: id },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
            //alerta('Nueva Cuenta', jqXHR.responseText, 'error');
        },
        success: function (result) {
            if (result[0].reset == 1) {

                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);		  
				//$('#div_general').load(document.URL + ' #div_general');

              var tr = $("#tr_"+id);
					// tr.next().remove();
					// tr.remove(); 


					/* var tr2=$(".footable-row-detail");
                  tr2.remove(); */

              }
          },
          url: "cuenta/getEliminarGestion"
      });	

    location.reload();

}

/* FIN ELIMINAR CUENTA */


function llamaDatosGestiones(ejecutivo) {

    var url=$("#RutaBase").val()+'/getHistorialGestionDownload';

    $.ajax({
     async: false,
     cache: false,
     dataType: "json",
     type: 'POST',
     data: {ejecutivo: ejecutivo},
     error: function (jqXHR, textStatus, errorThrown) {
             // code en caso de error de la ejecucion del ajax
             console.log(jqXHR);
         },
         success: function (result) {

            cargaTablagestiones(result);
        },
        url: url
    });
}

function cargaTablagestiones(data) {

     //console.log(data);

     if ($.fn.dataTable.isDataTable('#tbl_gestion')) {
         $('#tbl_gestion').DataTable().destroy();
     }

     $('#tbl_gestion').DataTable({
         "aaData": data,
         "bFilter": false,
         "bPaginate": false,
         "bInfo": false,
         "aoColumns": [
         {"mDataProp": "id_ejecutivo"},
         {"mDataProp": "razon_social_empresa"},
         {"mDataProp": "DESC_ACCION"},
         {"mDataProp": "asunto_accion"},
         {"mDataProp": "DESC_EVENTO"},
         {"mDataProp": "fecha_evento"},
         {"mDataProp": "DESC_COMPROMISO"},
         {"mDataProp": "fecha_compromiso_accion"},
         {"mDataProp": "comentario_accion"},
         {"mDataProp": "tipo_accion"},
         {"mDataProp": "fase"},
         {"mDataProp": "id_producto"}

         ],
         pageLength: 25,
         responsive: false,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [
         {
             extend: 'copy'
         }, 
         {
             extend: 'excel',
             title: 'Gestiones'
         }, 
         {
             extend: 'print',
             customize: function (win) {
                 $(win.document.body).addClass('white-bg');
                 $(win.document.body).css('font-size', '10px');
                 $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
             }
         }
         ]
     });

     $('[data-toggle="tooltip"]').tooltip();
 }


 function listDominios() {

   var url=$("#RutaBase").val()+'/getHistorialDominiosDownload';

   $.ajax({
     async: false,
     cache: false,
     dataType: "json",
     type: 'POST',
         //data: $('#frm').serialize(),
         error: function (jqXHR, textStatus, errorThrown) {
             // code en caso de error de la ejecucion del ajax
             //  console.log(textStatus + errorThrown);
         },
         success: function (result) {
             cargaTablaDominios(result);
         },
         url: url
     });
}

function cargaTablaDominios(data) {

 if ($.fn.dataTable.isDataTable('#tbl_dominio_sin_asignar')) {
     $('#tbl_dominio_sin_asignar').DataTable().destroy();
 }

 $('#tbl_dominio_sin_asignar').DataTable({
     "aaData": data,
     "bFilter": false,
     "bPaginate": false,
     "bInfo": false,
     "aoColumns": [
     {"mDataProp": "dominio"}
     ],
     pageLength: 25,
     responsive: false,
     dom: '<"html5buttons"B>lTfgitp',
     buttons: [
     {
         extend: 'copy'
     }, 
     {
         extend: 'excel',
         title: 'Dominios'
     }, 
     {
         extend: 'print',
         customize: function (win) {
             $(win.document.body).addClass('white-bg');
             $(win.document.body).css('font-size', '10px');
             $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
         }
     }
     ]
 });

 $('[data-toggle="tooltip"]').tooltip();
}


function showMdlClientesEjecutivos(){
    $("#mdlListaClientes").modal('show');

    var url=$("#RutaBase").val()+'/getClientesConEjecutivos';

    $.ajax({
     async: false,
     cache: false,
     dataType: "json",
     type: 'POST',
         //data: $('#frm').serialize(),
         error: function (jqXHR, textStatus, errorThrown) {
             // code en caso de error de la ejecucion del ajax
             //  console.log(textStatus + errorThrown);
         },
         success: function (result) {
             if ($.fn.dataTable.isDataTable('#tbl_clientes_ejecutivos')) {
                $('#tbl_clientes_ejecutivos').DataTable().destroy();
            }

            $('#tbl_clientes_ejecutivos').DataTable({
                paging: true,
                ordering: true,
                "searching": true,
                info: false,
                "aaData": result,
                "bFilter": false,
                "bPaginate": false,
                "bInfo": false,
                columnDefs: [
                {
                    targets: [5,6,7,8,9,10,11,12],
                    visible: false
                }
                ],
                "aoColumns": [
                {"mDataProp": "razon_social_empresa"},
                {"mDataProp": "holding"},
                {"mDataProp": "ejecutivo"},
                {"mDataProp": "rut_empresa"},
                {"mDataProp": "gestion"},
                {"mDataProp": "nombre_contacto"},
                {"mDataProp": "telefono_contacto"},
                {"mDataProp": "direccion_empresa"},
                {"mDataProp": "fecha_gestion"},
                {"mDataProp": "fase"},
                {"mDataProp": "accion"},
                {"mDataProp": "compromiso"},
                {"mDataProp": "fecha_compromiso"}
                ],
                pageLength: 25,
                responsive: false,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                {
                 extend: 'copy'
             }, 
             {
                 extend: 'excel',
                 title: 'Clientes',
                 
             }, 
             {
                 extend: 'print',
                 customize: function (win) {
                     $(win.document.body).addClass('white-bg');
                     $(win.document.body).css('font-size', '10px');
                     $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                 }
             }
             ]
         });
            $('[data-toggle="tooltip"]').tooltip();
        },
        url: url
    });
}