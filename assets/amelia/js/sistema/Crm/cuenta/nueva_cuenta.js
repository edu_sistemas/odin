	$(document).ready(function() {
    $('.footable').footable();
		
	$('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
        });
	
	$("#btn-nueva-cuenta").click(function(){
		if (validarRequeridoForm($("#form-nueva-cuenta"))) {
				guardar_nueva_cuenta();
		}
    });
	
	// CARGA CBX
	
	cargaAjaxComboHolding();
	cargaAjaxComboClasificacionCuenta();
	cargaAjaxComboEjecutivoAsignado();
	cargaAjaxComboRubroEmpresa();
	cargaAjaxComboSubRubroEmpresa();
	cargaAjaxComboTamanoEmpresa();
	cargaAjaxComboPaisCuenta();
    cargaAjaxComboComuna()
	
	// FIN CARGA CBX
	
	
});

function guardar(diferenciador){
    if(diferenciador == 1){
        $('#myModal').modal('toggle');
     }              
}

function guardar_nueva_cuenta(){


	 $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#form-nueva-cuenta').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
            console.log($('#form-nueva-cuenta').serialize());
			//alerta('Nueva Cuenta', jqXHR.responseText, 'error');
			
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
				//alert(result[0].titulo);
            if (result[0].reset == 1) {
               
            }
			
        },
		url: "nueva_cuenta/Set_Cuenta_Nueva"
		

    }); 				
}

//--------------------------PRUEBA HOLDING------------------------------
function cargaAjaxComboHolding() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboHolding(result);
        },
        url: "nueva_cuenta/getComboHolding"
    });
}


function cargaComboHolding(midata) {
		
		 $("#Var_id_holding").select2({
			allowClear: true,
			data: midata
});
		
        //cargaCBX("Var_id_holding",midata);
    }

//--------------------------FIN PRUEBA HOLDING--------------------------

//--------------------------PRUEBA CLASIFICACION CUENTA------------------------------
function cargaAjaxComboClasificacionCuenta() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboClasificacionCuenta(result);

        },
        url: "nueva_cuenta/getComboClasificacionCuenta"
    });
}


function cargaComboClasificacionCuenta(midata) {
		
		 $("#Var_crm_clasificacion_cuenta").select2({
			allowClear: true,
			data: midata
});
		
        //cargaCBX("Var_id_holding",midata);
    }

function cargaAjaxComboComuna() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //alert(result);
            cargaComboComuna(result);

        },
        url: "nueva_cuenta/getComboComuna"
    });
}    

function cargaComboComuna(midata) {
        
         $("#Var_crm_comuna").select2({
            allowClear: true,
            data: midata
});
        
        //cargaCBX("Var_id_holding",midata);
    }    

//--------------------------FIN PRUEBA CLASIFICACION CUENTA--------------------------

//--------------------------PRUEBA EJECUTIVO ASIGNADO------------------------------
function cargaAjaxComboEjecutivoAsignado() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboEjecutivoAsignado(result);

        },
        url: "nueva_cuenta/getComboEjecutivoAsignado"
    });
}


function cargaComboEjecutivoAsignado(midata) {
		
		 $("#Var_crm_id_ejecutivo").select2({
			allowClear: true,
			data: midata
});
		
        //cargaCBX("Var_id_holding",midata);
    }

//--------------------------FIN PRUEBA EJECUTIVO ASIGNADO--------------------------

//--------------------------PRUEBA RUBRO EMPRESA------------------------------
function cargaAjaxComboRubroEmpresa() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboRubroEmpresa(result);

        },
        url: "nueva_cuenta/getComboRubroEmpresa"
    });
}


function cargaComboRubroEmpresa(midata) {
		
		 $("#tab1_detalle_cuenta_rubro").select2({
			allowClear: true,
			data: midata
});
		
        //cargaCBX("Var_id_holding",midata);
    }

//--------------------------FIN PRUEBA RUBRO EMPRESA--------------------------

//--------------------------PRUEBA SUBRUBRO EMPRESA------------------------------
function cargaAjaxComboSubRubroEmpresa() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboSubRubroEmpresa(result);

        },
        url: "nueva_cuenta/getComboSubRubroEmpresa"
    });
}


function cargaComboSubRubroEmpresa(midata) {
		
		 $("#tab1_detalle_cuenta_subrubro").select2({
			allowClear: true,
			data: midata
});
		
        //cargaCBX("Var_id_holding",midata);
    }

//--------------------------FIN PRUEBA SUBRUBRO EMPRESA--------------------------

//--------------------------PRUEBA TAMAÑO EMPRESA------------------------------
function cargaAjaxComboTamanoEmpresa() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
			
			cargaComboTamanoEmpresa(result);
			
        },
        url: "nueva_cuenta/getComboTamanoEmpresa"
    });
}


function cargaComboTamanoEmpresa(midata) {
		
				
		 $("#Var_crm_tamano_empresa").select2({
			allowClear: true,
			data: midata
});

		
        //cargaCBX('Var_crm_clasificacion_cuenta',midata);
    }

//--------------------------FIN PRUEBA TAMAÑO EMPRESA--------------------------


//--------------------------PRUEBA PAIS------------------------------
function cargaAjaxComboPaisCuenta() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
			
			cargaComboPaisCuenta(result);
			
        },
        url: "nueva_cuenta/getComboPaisCuenta"
    });
	//$("#Var_crm_tamano_empresa").val($("#hidden_crm_tamano_empresa").val()).trigger("change");
}


function cargaComboPaisCuenta(midata) {
		
				
		 $("#Var_crm_id_pais").select2({
			allowClear: true,
			data: midata
});

		
        //cargaCBX('Var_crm_clasificacion_cuenta',midata);
    }

//--------------------------FIN PRUEBA PAIS--------------------------



