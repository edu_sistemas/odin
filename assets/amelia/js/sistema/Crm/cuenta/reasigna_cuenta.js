var PermiHidnn;
var PermiHidnn2;

$(document).ready(function () {

    $('#btn_derecha').click(function () {
        $('#cuentasorigen_visible option:selected').remove().appendTo('#cuentasdestino_visible');
        $('#cuentasorigen').html($('#cuentasorigen_visible').html());
        $('#cuentasdestino').html($('#cuentasdestino_visible').html());

    });

    $('#btn_izquierda').click(function () {
        $('#cuentasdestino_visible option:selected').remove().appendTo('#cuentasorigen_visible');
        $('#cuentasdestino').html($('#cuentasdestino_visible').html());
        $('#cuentasorigen').html($('#cuentasorigen_visible').html());
    });

    $("#btnReasignarCuenta").click(function(){
      if (validarRequeridoForm($("#form"))) {
      }
  });
listBitacora();

/*    PermiHidnn = $('#cuentasdestino');
$('#cuentasdestino_visible').html(PermiHidnn.html()); */

$('#search1').bind('keypress', function (e) {

    var inicio = $('#search1').val() + String.fromCharCode(e.keyCode);
    actualizaLista(inicio);

});

$('#search1').bind('keyup', function (e) {
    var actual = $('#search1').val().trim();
    if (actual == "") {
        var inicio = actual.substr(0, actual.length - 1);
        actualizaLista(inicio);
    }

});

$('#search2').bind('keypress', function (a) {

    var inicio2 = $('#search2').val() + String.fromCharCode(a.keyCode);
    actualizaLista2(inicio2);

});

$('#search2').bind('keyup', function (a) {

    var actual2 = $('#search2').val().trim();

    if (actual2 == "") {
        var inicio2 = actual2.substr(0, actual2.length - 1);
        actualizaLista2(inicio2);
    }
});


    function actualizaLista2(inicio2) {
        var y = PermiHidnn2.clone();
        inicio2 = inicio2.toLowerCase();
        $('#cuentasorigen_visible').html("");
        var verifica2 = inicio2.trim();

        if (verifica2.length == 0) {
            $('#cuentasorigen_visible').html($('#cuentasorigen').html());
            return;
        }
        y.find('option[title^="' + inicio2 + '"]').each(function () {
            $('#cuentasorigen_visible').append($(this));
        });
    }

    function actualizaLista(inicio) {
        var x = PermiHidnn.clone();
        inicio = inicio.toLowerCase();

        $('#cuentasdestino_visible').html("");

        var verifica = inicio.trim();

        if (verifica.length == 0) {
            $('#cuentasdestino_visible').html($('#cuentasdestino').html());
            return;
        }


        x.find('option[title^="' + inicio + '"]').each(function () {
            $('#cuentasdestino_visible').append($(this));
        });
    }

    cargaAjaxComboejecutivodestino();

    PermiHidnn2 = $('#cuentasorigen');
    $('#cuentasorigen_visible').html(PermiHidnn2.html());

    PermiHidnn = $('#cuentasdestino');
    $('#cuentasdestino_visible').html(PermiHidnn.html());	

});

function removeOptions(selectbox)
{
    var i;
    for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
    {
        selectbox.remove(i);
    }
}


function ejesel(id)
{
	var menues = $(".table tr"); 
    menues.css('background','#F4F6FA').css('color','#38283C');
    $('#tr_'+id).css('background','#3E2C42').css('color','#FFF');

};



function limpiar(varinput, e) {

    if(e.keyCode == 27)
        $("#"+varinput).val('');
};



function reasignarcuentas(){
	
	var x = document.getElementById("cuentasdestino").length;
    if (x == 0) {

       //alerta("Debe seleccionar al menos una empresa para ser reasignada");
       alerta("Error","Debe seleccionar al menos una empresa para ser reasignada","warning");
       return false;


   }
   else
   {
       var valores = document.getElementById("cuentasdestino");
            //alert(x);
            var valorescbx = document.getElementById("ejecutivodestino").value;
            //alert(valorescbx);

            for (var i =0; i<x; i++){
                //alert(valores[i].value);
                reasignarCuentaUpdate(valorescbx, valores[i].value);
            }
            $('#div_general').load(document.URL + ' #div_general');
        }

    }


    function listarEmpresasporEjecutivo(id_ejecutivo) {

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_ejecutivo : id_ejecutivo},
            error: function (jqXHR, textStatus, errorThrown) {
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCBX("cuentasorigen", result);		

            $("#cuentasorigen option[value='']").remove();
            removeOptions(document.getElementById("ejecutivodestino"));
            cargaAjaxComboejecutivodestino();
            $("#ejecutivodestino").find("option[value="+id_ejecutivo+"]").remove();
            removeOptions(document.getElementById("cuentasdestino"));

            $("#search1").val('');
            $("#search2").val('');

        },
        url: 'reasigna_cuenta/ListarEmpresas'
    });

        PermiHidnn2 = $('#cuentasorigen');
        $('#cuentasorigen_visible').html(PermiHidnn2.html());

        PermiHidnn = $('#cuentasdestino');
        $('#cuentasdestino_visible').html(PermiHidnn.html());	
    }




    function popupGestiones(id_ejecutivo) {

 
    $("#nameejec").html($('#tr_'+id_ejecutivo+' td:first-child').html());

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_ejecutivo : id_ejecutivo},
            error: function (jqXHR, textStatus, errorThrown) {
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

             $('#tbl_gestion td').remove();

            $(result).each(function (index)
            {

                $('#tbl_gestion tbody').append('<tr style="width: 50%;"><td>'+result[index].razon_social_empresa+'</td><td style="width: 25%;">'+result[index].DESC_ACCION+'</td><td style="width: 25%;">'+result[index].fecha_evento+'</td></tr>'); 

            })

            $('#myModalReasignacuenta').modal('toggle');
        },

        url: 'reasigna_cuenta/popupGestiones'
    });
    }



function cargaAjaxComboejecutivodestino() {

  $.ajax({
   async: false,
   cache: false,
   dataType: "json",
   type: 'GET',
             //  data: $('#frm').serialize(),
             error: function (jqXHR, textStatus, errorThrown) {
                 // code en caso de error de la ejecucion del ajax
                 //  console.log(textStatus + errorThrown);
             },
             success: function (result) {

               cargaComboejecutivodestino(result);
				 //$("#ejecutivodestino").find("option[value='42']").remove();             
               },
               url: "reasigna_cuenta/getComboEjecutivoDestino"
           });
}

function cargaComboejecutivodestino(midata) {

 $("#ejecutivodestino").select2({
    allowClear: true,
    data: midata

});


			//cargaCBX("ejecutivodestino",midata);
		}
		
		
        function reasignarCuentaUpdate(id_nuevo_ejecutivo, id_empresa) {

            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {id_nuevo_ejecutivo : id_nuevo_ejecutivo,
                    id_empresa : id_empresa},
                    error: function (jqXHR, textStatus, errorThrown) {
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $("#search1").val('');
            $("#search2").val('');

        },
        url: 'reasigna_cuenta/ReasignarCuentas'
    });
        }


// -----------Expotar------------- //

function listBitacora() {
     $.ajax({
         async: false,
         cache: false,
         dataType: "json",
         type: 'POST',
         //data: $('#frm').serialize(),
         error: function (jqXHR, textStatus, errorThrown) {
             // code en caso de error de la ejecucion del ajax
             //  console.log(textStatus + errorThrown);
         },
         success: function (result) {
             cargaBitacora(result);
         },
         url: "reasigna_cuenta/getReasignaCuentaBitacora"
     });
 }

function cargaBitacora(data) {

     if ($.fn.dataTable.isDataTable('#reasignar_bitacora')) {
         $('#reasignar_bitacora').DataTable().destroy();
     }

     $('#reasignar_bitacora').DataTable({
         "aaData": data,
         "bFilter": false,
         "bPaginate": false,
         "bInfo": false,
         "aoColumns": [
             {"mDataProp": "razon_social_empresa"},
             {"mDataProp": "rut_empresa"},
             {"mDataProp": "UserDestino"},
             {"mDataProp": "fecha_inicio"},
             {"mDataProp": "FechaTermino"},
             {"mDataProp": "venta"},
             {"mDataProp": "UserCambio"},
             {"mDataProp": "FechaCambio"},
                      ],
         pageLength: 25,
         responsive: true,
         dom: '<"html5buttons"B>lTfgitp',
         buttons: [
             {
                 extend: 'excel',
                 title: 'Reasignar_bitacora',
             }, 
         ]
     });

     $('[data-toggle="tooltip"]').tooltip();
 }		




