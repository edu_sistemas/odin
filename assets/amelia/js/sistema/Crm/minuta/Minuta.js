$(document).ready(function(){


            $("#btn_guardar_minuta").click(function(){
                if (validarRequeridoForm($("#form_editar_minuta"))) {
                   guardar_minuta();
               }
           });


});
        

        function modalllamada(){
            $('#myModal1').modal('toggle');
        }


function fecha(diferenciador){

                        if(diferenciador == 1){
                            $('#data1 .input-group.date').datepicker({
                                todayBtn: "linked",
                                keyboardNavigation: false,
                                forceParse: false,
                                calendarWeeks: false,
                                autoclose: true,                
                                format: "dd-mm-yyyy"
                            }); 
                        }
}


function ejesel(id)
{
    var menues = $(".table td"); 
    menues.removeClass("active");
  $('#td_'+id).addClass("active");
};

    function listarMinutas(id_usuario) {

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_usuario : id_usuario},
            error: function (jqXHR, textStatus, errorThrown) {
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result);
            if (result[0].id_tipo_origen == 1)
            {
              $('#textarea_minuta_auto').show();
              $("#textarea_minuta_auto").html(result[0].cuerpo);
              document.getElementById('textarea_minuta').style.display = "none";
            }
            else {
              $("#textarea_minuta").html(result[0].cuerpo);
              $('#textarea_minuta_auto').hide();
              document.getElementById('textarea_minuta').style.display = "initial";
            }

            

        },
        url: 'Minuta/ListarMinutas'
    });
  
    }


    function eliminarMinuta(id) {

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id : id},
            error: function (jqXHR, textStatus, errorThrown) {
              //console.log(jqXHR +textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                //alert(result[0].titulo);
                if (result[0].reset == 1) {

                  //limpiarFormularios('form_editar_minuta');
                  
                  $('#div_general').load(document.URL + ' #div_general');


              }
        },
        url: 'Minuta/EliminarMinuta'
    });
  
    }




function cargaminutafiltrada(midata) {

   $("#textarea_minuta").select2({
    allowClear: true,
    data: midata
});
}



        function guardar_minuta(){

                  $.ajax({
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: 'POST',
                    data: $('#form_editar_minuta').serialize(),
                    error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
            console.log($('#form_editar_minuta').serialize());
            //alerta('Cliente Potencial', jqXHR.responseText, 'error');
            //alert(textStatus + ' - ' + errorThrown);
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                //alert(result[0].titulo);
                if (result[0].reset == 1) {

                  limpiarFormularios('form_editar_minuta');
                  
                  $('#div_general').load(document.URL + ' #div_general');
                  $('#myModal1').modal('toggle');


              }
          },
          url: 'Minuta/guardarMinuta'


      });   
                  
}


function limpiarFormularios(formulario){


    var form = formulario;
    
  $(':input','#'+form)
  .not(':button, :submit, :reset, :hidden, #fecha_minuta')
  .val('')
  .removeAttr('checked')
  .removeAttr('selected');

}



function confirmaEliminaCuenta(id)
{
   swal({
     title: "Desea Continuar?",
     text: "Esta acción eliminara la minuta!",
     type: "warning",
     showCancelButton: true,
     confirmButtonColor: "#DD6B55",
     confirmButtonText: "Si!",
     cancelButtonText: "No!",
     closeOnConfirm: false
 },
 function(){
     eliminarMinuta(id);
 
 });
}



