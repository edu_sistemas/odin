var array_venta_anio_actual = [];
var array_venta_anio_anterior = [];
$(document).ready(function () {
//MetasGlobales(0);



$("#btn_generar_excel_grupos_ejecutivos").click(function () {

    var fecha = $("#fecha_sdashboard").val();
    var url_final = "";


    if (fecha == "") {
        fecha = "0";
    }

    url_final = "sdashboard/generarExcelGruposEjecutivos/" + fecha;

    window.open(url_final, '_blank');
}); 

$("#btn_generar_excel_clasificacion_ejecutivos").click(function () {

    var fecha = $("#fecha_sdashboard").val();
    var id_grupo = $("#id_grupo_hidden").val();
    var url_final = "";


    if (fecha == "") {
        fecha = "0";
    }

    url_final = "Sdashboard/generarExcelClasificacionEjecutivos/" + id_grupo +"/"+ fecha;
        //console.log("este es el id grupo:"+id_grupo);
        window.open(url_final, '_blank');
    });

$("#btn_generar_excel_ventas_rubro").click(function () {

    var fecha = $("#fecha_sdashboard").val();
    var url_final = "";


    if (fecha == "") {
        fecha = "0";
    }

    url_final = "sdashboard/generarExcelVentasRubro/" + fecha;

    window.open(url_final, '_blank');
});

$("#btn_generar_excel_linea_negocio").click(function () {

    var fecha = $("#fecha_sdashboard").val();
    var url_final = "";


    if (fecha == "") {
        fecha = "0";
    }

    url_final = "sdashboard/generarExcelLineaNegocio/" + fecha;

    window.open(url_final, '_blank');
}); 

ventaAcomulada();


$("#btn-fecha-sdashboard").click(function(){

 if ($('#fecha_sdashboard').val().trim().length == 0) 
 {
    alert('Debe seleccionar Mes.');
    return;
}



var fecha = $("#fecha_sdashboard").val();

var ejecutivo=0;

ListGruposEjecutivos(fecha,ejecutivo);
ListRubros(fecha);
ListLinNegocio(fecha);
SelectGruposEjecutivosFiltrados(-1,0);
MetasGlobales(fecha);

DatosFiltradosEventosKpi(0,fecha);

});  


});

var fechaini = $("#fecha_sdashboard").val();

//cargaSelectEjecutivos(0);
ListGruposEjecutivos(fechaini,0);
ListRubros(fechaini);
ListLinNegocio(fechaini);
SelectGruposEjecutivosFiltrados(-1,fechaini);
MetasGlobales(0);

DatosFiltradosEventosKpi(0,fechaini);

/*
       function fecha()
       {
            
            $('#data1 .input-group.date').datepicker({
                minViewMode: 1,
                keyboardNavigation: false,
                forceParse: false,
                forceParse: false,
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm"
            }).on('changeDate',function(e){

                var fecha = $("#fecha_sdashboard").val();
                 ListGruposEjecutivos(fecha);
                 ListRubros(fecha);
                 ListLinNegocio(fecha);
                 SelectGruposEjecutivosFiltrados(-1,0);
                 MetasGlobales(fecha);
                });
        }
        */


        function fecha()
        {

            $('#data1 .input-group.date').datepicker({
                minViewMode: 1,
                keyboardNavigation: false,
                forceParse: false,
                forceParse: false,
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm"
            });
        }


        function MetasGlobales(fecha)
        {
            swal({
                title: 'Espere un momento...',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            });

            setTimeout(function(){

                $.ajax({
                    async: true,
                    cache: false,
                    dataType: "json",
                    type: 'POST',
                    data: {fecha: fecha},
                    error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown + jqXHR);
            // swal.hideLoading();
            swal.close();
        },
        success: function (result) {

             swal.hideLoading();
            swal.close();
            var peso="$";

            $("#Var_mensual_meta").html("$ "+result[0].meta_mes);
            $("#Var_mensual_venta").html("$ "+result[0].venta_mes);
            $("#Var_mensual_porcen").html(result[0].porcenmes+"%");
            $("#Var_acum_meta").html("$ "+result[0].meta_acum);
            $("#Var_acum_venta").html("$ "+result[0].venta_acum);
            $("#Var_acum_porcen").html(result[0].porcenacum+"%");

            $("#Var_dif_mes").html("$ "+result[0].dif_mes);
            $("#Var_dif_ano").html("$ "+result[0].dif_ano);
            $("#Var_meta_sig_mes").html("$ "+result[0].meta_mes_sig);

            $("#Var_mensual_metax").html("$ "+result[0].meta_anual);
            $("#Var_mensual_ventax").html("$ "+result[0].meta_acum_mes);

            var colorwidgetAcum='';
            var colorwidgetMes='';
            var mes = '';
            var ano = '';        

            var fecha = $("#fecha_sdashboard").val();

            var mes = fecha.substring(5, 7);
            var ano = fecha.substring(0, 4);




            switch (mes){
                case '01':
                mesname='Ene';
                break;
                case '02':
                mesname='Feb';
                break;
                case '03':
                mesname='Mar';
                break;
                case '04':
                mesname='Abr';
                break;
                case '05':
                mesname='May';
                break;
                case '06':
                mesname='Jun';
                break;
                case '07':
                mesname='Jul';
                break;
                case '08':
                mesname='Ago';
                break;  
                case '09':
                mesname='Sep';
                break;
                case '10':
                mesname='Oct';
                break;
                case '11':
                mesname='Nov';
                break;
                case '12':
                mesname='Dic';
                break;                                                                                                
            }





            switch (result[0].iconmes){
                case '1':
                colorwidgetMes='widget style1 lazur-bg';
                break;
                case '2':
                colorwidgetMes='widget style1 yellow-bg';
                break;
                case '3':
                colorwidgetMes='widget style1 red-bg';
                break;
                case '0':
                colorwidgetMes='widget style1 red-bg';
                break;                                
            }

            switch (result[0].iconacum){
                case '1':
                colorwidgetAcum='widget style1 lazur-bg';
                break;
                case '2':
                colorwidgetAcum='widget style1 yellow-bg';
                break;
                case '3':
                colorwidgetAcum='widget style1 red-bg';
                break;
                case '0':
                colorwidgetAcum='widget style1 red-bg';
                break;                                
            }

            $("#Var_icon_mes").removeClass();
            $("#Var_icon_acum").removeClass();
            $("#Var_icon_mes").addClass(colorwidgetMes);
            $("#Var_icon_acum").addClass(colorwidgetAcum);
            $("#Var_mes_ano").html("<small>Mensual "+mesname+" "+ano+" </small>");
            $("#Var_mes").html("<small>Venta Mensual "+mesname+" </small>");
            $("#Var_ano").html("<small>Acumulado "+ano+" </small>");
            $("#Var_ano_porcen").html("<small>Venta Anual "+ano+" </small>");

            $("#Var_mes_anox").html("<small>Acumulado a "+mesname+" "+ano+" </small>");

        },
        url: "sdashboard/getMetasGlobales"
    });

}, 1000);

}




function ListGruposEjecutivos(fecha,ejecutivo)
{


    $("#tblListGruposEjecutivos tbody tr").each(function (index) {
       $(this).remove();

   });

    $('#tblListGruposEjecutivos tbody').append('<tr id="trwaitListoGrupo"><td colspan="6" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>'); 

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#form_fecha_sdashboard').serialize(),
        data: { fecha: fecha, ejecutivo: ejecutivo },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            $('#trwaitListoGrupo').remove();

            $(result).each(function (index) 
            {

                var iconmes='';
                var iconacum='';

                switch (result[index].iconmes){
                    case '1':
                    iconmes='fa fa-circle text-info fa-x4';
                    break;
                    case '2':
                    iconmes='fa fa-circle text-warning fa-x4';
                    break;
                    case '3':
                    iconmes='fa fa-circle text-danger fa-x4';
                    break; 
                    case '0':
                    iconmes='fa fa-circle text-primary fa-x4';
                    break;          
                }

                switch (result[index].iconacum){
                    case '1':
                    iconacum='fa fa-circle text-info fa-x4';
                    break;
                    case '2':
                    iconacum='fa fa-circle text-warning fa-x4';
                    break;
                    case '3':
                    iconacum='fa fa-circle text-danger fa-x4';
                    break; 
                    case '0':
                    iconacum='fa fa-circle text-primary fa-x4';
                    break;             
                } 
                var concatvar = result[index].id_grupo+",'"+fecha+"'";
                //alert(concatvar);
                //$('#tblListGruposEjecutivos tbody').append('<tr><td>'+result[index].id_grupo+'</td><td><i class="'+iconmes+'"></i></td><td>'+result[index].porcenmes+'</td><td><i class="'+iconacum+'"></i></td><td>'+result[index].porcenacum+'</td><td>$'+result[index].venta_mes+'</td><td>$'+result[index].venta_acum+'</td><td>$'+result[index].meta_mes+'</td><td>$'+result[index].meta_acum+'</td></tr>'); 
                //$('#tblListGruposEjecutivos tbody').append('<tr id="tr_'+result[index].id_grupo+'" onclick="SelectGruposEjecutivosFiltrados('+concatvar+');""><td style="width: 15%" data-placement="top"  data-toggle="tooltip" title="'+result[index].nombre_grupo_comercial+'">'+result[index].grupo+'</td><td style="width: 91px;  padding-left:15px;"><i class="'+iconmes+'"></i><p class="pull-right">'+result[index].porcenmes+'%</p></td><td style="width: 91px;"><i class="'+iconacum+'"></i><p class="pull-right">'+result[index].porcenacum+'%</p></td><td data-toggle="tooltip" data-placement="bottom" title="Meta Mes: $'+result[index].meta_mes+'" style="text-align:right;">$'+result[index].venta_mes+'</td><td  data-toggle="tooltip" data-placement="bottom" title="Meta Acum.: $'+result[index].meta_acum+'" style="text-align:right;">$'+result[index].venta_acum+'</td></tr>'); 
                $('#tblListGruposEjecutivos tbody').append('<tr id="tr_'+result[index].id_grupo+'" onclick="SelectGruposEjecutivosFiltrados('+concatvar+');""><td style="width: 15%" data-placement="top"  data-toggle="tooltip" title="'+result[index].nombre_grupo_comercial+'">'+result[index].grupo+'</td><td style="width: 91px;  padding-left:15px;"><i class="'+iconmes+'"></i><p class="pull-right">'+result[index].porcenmes+'%</p></td><td style="width: 91px;"><i class="'+iconacum+'"></i><p class="pull-right">'+result[index].porcenacum+'%</p></td><td data-toggle="tooltip" data-placement="bottom" title="Meta Mes: $'+result[index].meta_mes+'" style="text-align:right;">$'+result[index].venta_mes+'</td><td  data-toggle="tooltip" data-placement="bottom" title="Meta Acum.: $'+result[index].meta_acum+'" style="text-align:right;">$'+result[index].venta_acum+'</td><td style="width: 91px;  padding-left:15px;"><i></i><p class="pull-right">$'+result[index].meta_acum+'</p></td></tr>'); 


            });
        },
        url: "sdashboard/getDatosListGruposEjecutivos"
    });
}


function SelectGruposEjecutivosFiltrados(id_grupo, fecha)
{
    $("#id_grupo_hidden").val(id_grupo);
    //alert(fecha);


    $("#tblSelectGruposEjecutivos tbody tr").each(function (index) {
       $(this).remove();

       var menues = $(".table tr"); 
       menues.css('background','#FFFFFF').css('color','#38283C');
       $('#tr_'+id_grupo).css('background','#3E2C42').css('color','#FFF');

   });

    $('#tblSelectGruposEjecutivos tbody').append('<tr id="trwaitSelectGruposEjecutivos"><td colspan="6" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>'); 

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_grupo: id_grupo, fecha : fecha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            $('#trwaitSelectGruposEjecutivos').remove();

            $(result).each(function (index) 
            {

                var iconmes='';
                var iconacum='';

                switch (result[index].iconmes){
                    case '1':
                    iconmes='fa fa-circle text-info fa-x4';
                    break;
                    case '2':
                    iconmes='fa fa-circle text-warning fa-x4';
                    break;
                    case '3':
                    iconmes='fa fa-circle text-danger fa-x4';
                    break; 
                    case '0':
                    iconmes='fa fa-circle text-primary fa-x4';
                    break;          
                }

                switch (result[index].iconacum){
                    case '1':
                    iconacum='fa fa-circle text-info fa-x4';
                    break;
                    case '2':
                    iconacum='fa fa-circle text-warning fa-x4';
                    break;
                    case '3':
                    iconacum='fa fa-circle text-danger fa-x4';
                    break; 
                    case '0':
                    iconacum='fa fa-circle text-primary fa-x4';
                    break;             
                }

                //$('#tblSelectGruposEjecutivos tbody').append('<tr id="tr_'+result[index].id_usuario+'" onclick="funchrefEjecutivo('+result[index].id_usuario+');"" ><td>'+result[index].ejecutivo+'</td><td><i class="'+iconmes+'"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">'+result[index].porcenmes+'%</p></td><td><i class="'+iconacum+'"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">'+result[index].porcenacum+'%</p></td><td data-toggle="tooltip" data-placement="bottom" title="Meta Mes: $'+result[index].meta_mes+'" style="text-align:right;">$'+result[index].venta_mes+'</td><td data-toggle="tooltip" data-placement="bottom" title="Meta Acum.: $'+result[index].meta_acum+'" style="text-align:right;">$'+result[index].venta_acum+'</td></tr>'); 
                $('#tblSelectGruposEjecutivos tbody').append('<tr id="tr_'+result[index].id_usuario+'" onclick="funchrefEjecutivo('+result[index].id_usuario+');"" ><td>'+result[index].ejecutivo+'</td><td><i class="'+iconmes+'"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">'+result[index].porcenmes+'%</p></td><td><i class="'+iconacum+'"></i>&nbsp;&nbsp;&nbsp;<p class="pull-right">'+result[index].porcenacum+'%</p></td><td data-toggle="tooltip" data-placement="bottom" title="Meta Mes: $'+result[index].meta_mes+'" style="text-align:right;">$'+result[index].venta_mes+'</td><td data-toggle="tooltip" data-placement="bottom" title="Meta Acum.: $'+result[index].meta_acum+'" style="text-align:right;">$'+result[index].venta_acum+'</td><td style="width: 91px;  padding-left:15px;"><i></i><p class="pull-right">$'+result[index].meta_acum+'</p></td></tr>'); 

            });
        },
        url: "sdashboard/getDatosSelectGruposEjecutivos"
    });
}


function funchrefEjecutivo(id_user){
    //alert('edashboard/index/'+id_user+'');
    window.location.href='../Crm/edashboard/index/'+id_user+'';
}


function ListRubros(fecha)
{
    //var ejecutivo=$("#ejecutivos").val();

    $("#tblRubro tbody tr").each(function (index) {
       $(this).remove();

   });

    $('#tblRubro tbody').append('<tr id="trwaitRubro"><td colspan="3" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>'); 

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#form_fecha_sdashboard').serialize(),
        data: {fecha: fecha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            $('#trwaitRubro').remove();

            $(result).each(function (index) 
            {

                $('#tblRubro tbody').append('<tr><td style="width: 15%">'+result[index].rubro+'</td><td style="text-align:right;">$'+result[index].venta_mes+'</td><td style="text-align:right;">$'+result[index].venta_acum+'</td></tr>'); 
            });
        },
        url: "sdashboard/getRubros"
    });
}


function ListLinNegocio(fecha)
{
    $("#tbllinNegocio tbody tr").each(function (index) {
       $(this).remove();

   });

    $('#tbllinNegocio tbody').append('<tr id="trwaitLinNeg"><td colspan="4" align="center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><h3>Cargando Datos...</h3></td></tr>'); 

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#form_fecha_sdashboard').serialize(),
        data: {fecha: fecha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {

            $('#trwaitLinNeg').remove();

            $(result).each(function (index) 
            {

                $('#tbllinNegocio tbody').append('<tr><td style="width: 15%">'+result[index].linea+'</td><td>'+result[index].sub_linea+'</td><td>$'+result[index].venta_mes+'</td><td>$'+result[index].venta_acum+'</td></tr>'); 
            });
        },
        url: "sdashboard/getLineaNegocio"
    });
}


function ventaAcomulada() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        // data: {id_holding: id_holding},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //   console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            for (var i = 0; i < result.length; i++) {
                if (result[i].anio == 2018) {
                    anio1 = 2018;
                    array_venta_anio_actual.push(result[i].total_suma_venta);
                } else if(result[i].anio ==2017) {
                    anio2 = 2017;
                    array_venta_anio_anterior.push(result[i].total_suma_venta);

                }

            }


            graficoComparativaVentas();

        },
        url: "sdashboard/getVentaAcumulada"
    });

}

function graficoComparativaVentas() {


    /*Grafico Ventas*/

    var lineData_venta = {
        labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        datasets: [
        {
            label: anio1,
            backgroundColor: 'rgba(26,179,148,0.5)',
            borderColor: "rgba(26,179,148,0.7)",
            pointBackgroundColor: "rgba(26,179,148,1)",
            pointBorderColor: "#fff",
            data: array_venta_anio_actual
        }, {
            label: anio2,
            backgroundColor: 'rgba(220, 220, 220, 0.5)',
            pointBorderColor: "black",
            data: array_venta_anio_anterior
        }
        ]
    };

    var lineOptions_venta = {
        responsive: true,
        scales: {
            yAxes: [{
                ticks: {
                    callback: function (value) {
                        return "$ " + parseInt(value).toLocaleString();
                    }
                }
            }]},
            tooltips: {
                mode: 'label',
                callbacks: {
                    label: function (tooltipItem, data) {
                        return data.datasets[tooltipItem.datasetIndex].label + ": " + "$ " + parseInt(tooltipItem.yLabel).toLocaleString();
                    }
                }
            }
        };



        var ctx = document.getElementById("lineChart_ventas").getContext("2d");
        new Chart(ctx, {type: 'line', data: lineData_venta, options: lineOptions_venta});

        /*End  Grafico Ventas*/

    }


    function cargaSelectEjecutivos(ejecutivo) {
        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
        //  data: $('#frm').serialize(),
        data: { ejecutivo: ejecutivo },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result);
            cargaCBX('ejecutivos', result);

            if(result.length<2)
            {
                $("#ejecutivos option[value='']").remove();
            }
            else
            {
                $("#ejecutivos option[value='']").text("Todos");
                $("#ejecutivos option[value='']").val("0");
            }
        },
        url: "Edashboard/getListaEjecutivos"
    });
    }


    function DatosFiltradosEventosKpi(ejecutivo, fecha) {

        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: { ejecutivo : ejecutivo, fecha : fecha},
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            success: function (result) {

                if(result.length>0)
                {
                    $("#Var_q_llamadas").html(result[0].kpi);
                    $("#Var_q_correos").html(result[1].kpi);
                    $("#Var_q_reuniones").html(result[2].kpi);
                }
                else
                {
                    $("#Var_q_reuniones").html("0");
                    $("#Var_q_llamadas").html("0");
                    $("#Var_q_correos").html("0");
                }
            },
            url: "sdashboard/getDatosFiltradosKpi"
        });
    }