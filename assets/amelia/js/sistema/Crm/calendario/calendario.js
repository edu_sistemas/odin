   
   /* initialize the calendar
         -----------------------------------------------------------------*/

    $(document).ready(function() {
        /* initialize the external events
         -----------------------------------------------------------------*/

     $('#ejecutivos').on('change', function() {

     //$('#div_general').load(document.URL + ' #div_general');
     //$("#calendar").fullCalendar("refetchEvents");
     cargaDatosCalendario(this.value);

$('#calendar').fullCalendar('destroy'); 
$('#calendar').fullCalendar('render');

    });



        $('#external-events div.external-event').each(function() {

            // store data so the calendar knows to render an event upon drop
            $(this).data('event', {
                title: $.trim($(this).text()), // use the element's text as the event title
                stick: true // maintain when user navigates (see docs on the renderEvent method)
            });

            // make the event draggable using jQuery UI
            $(this).draggable({
                zIndex: 1111999,
                revert: true,      // will cause the event to go back to its
                revertDuration: 0  //  original position after the drag
            });

        });

        //cargaDatosCalendario(0);
        cargaSelectEjecutivos(0);

    });        



function cargaSelectEjecutivos(ejecutivo) {

//alert(ejecutivo);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //  data: $('#frm').serialize(),
        data: { ejecutivo: ejecutivo },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
              console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //alert(result);
            cargaComboEjecutivos(result);

        },
        url: "../Crm/Calendario/getListaEjecutivos"
    });
}


function cargaComboEjecutivos(midata) {

    cargaCBX('ejecutivos', midata);

    //alert(midata.length);

    if(midata.length<2)
    {
        $("#ejecutivos option[value='']").remove();
        $("#divEjecutivos").hide();
    }
    else
    {
        $("#ejecutivos option[value='']").text("Todos");
        $("#ejecutivos option[value='']").val("0");
    }

    var ejecutivoini=$("#ejecutivos").val();

    cargaDatosCalendario(ejecutivoini);
}


function cargaDatosCalendario(ejecutivo) {

//var ejecutivo=$("#ejecutivos").val();

//alert(ejecutivo);

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ejecutivo : ejecutivo},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(jqXHR + textStatus + errorThrown);
        },
        success: function (result) {
            //alert(result[0].title)
            //cargaComboAcciones(result);

    if(result.length == 0)
    {
        alerta("Sin Eventos","No hay eventos para este usuario","warning");
        //$('#calendar').reload();

    }
    else
    {
       $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay',
            },
            editable: false,
            async: false,
            locale: 'es',
            droppable: true, // this allows things to be dropped onto the calendar
            drop: function() {
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            events: result,
            //eventRender: function(event, element) { 
                //element.attr('title', event.tooltip);

             //}

            eventRender: function(event, element) { 
                 element.qtip({ content: event.description }); 
             }
        });

       //$('#calendar').fullCalendar('removeEventSource');
      //calendar.fullCalendar('refetchEvents');
      //calendar.fullCalendar('rerenderEvents');


}
        },
        url: "../Crm/Calendario/getAcciones"
    });
}

