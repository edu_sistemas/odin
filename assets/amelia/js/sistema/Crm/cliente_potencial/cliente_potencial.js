$(document).ready(function(){
    $("#reunion_compromiso_cbx").select2();
    $("#llamada_compromiso_cbx").select2();
    $("#reunion_compromiso_cbx").select2();
    $("#llamada_seleccione_fase_cbx").select2();
    $("#correo_seleccione_fase_cbx").select2();
    $("#reunion_seleccione_fase_cbx").select2();
    $("#modalidad_llamada_cbx").select2();
    $("#modalidad_correo_cbx").select2();
    $("#modalidad_reunion_cbx").select2();
    $("#seleccione_producto_cbx_llamada").select2();
    $("#seleccione_producto_cbx_correo").select2();
    $("#seleccione_producto_cbx").select2();
    $("#tipo_llamada_cbx").select2();
    $("#reunion_tipo_reunion_cbx").select2();
    $("#tipo_correo_cbx").select2();

    $("#llamada_compromiso_cbx").on("change", function(){
        var sele=$("#llamada_compromiso_cbx option:selected").text();
        if(sele=="Otros")
        {
            $("#div_otros_llamada").show();
            $("#comentario_otros_llamada").val('');
            $("#comentario_otros_llamada").attr("requerido", true);
        }
        else
        {  
          $("#div_otros_llamada").hide();
          $("#comentario_otros_llamada").attr("requerido", false);
      }
  });


    $("#correo_compromiso_cbx").on("change", function(){
        var sele=$("#correo_compromiso_cbx option:selected").text();
        if(sele=="Otros")
        {
            $("#div_otros_correo").show();
            $("#comentario_otros_correo").val('');
            $("#comentario_otros_correo").attr("requerido", true);
        }
        else
        {  
          $("#div_otros_correo").hide();
          $("#comentario_otros_correo").attr("requerido", false);
      }
  });


    $("#reunion_compromiso_cbx").on("change", function(){
        var sele=$("#reunion_compromiso_cbx option:selected").text();
        if(sele=="Otros")
        {
            $("#div_otros_reunion").show();
            $("#comentario_otros_reunion").val('');
            $("#comentario_otros_reunion").attr("requerido", true);
        }
        else
        {  
          $("#div_otros_reunion").hide();
          $("#comentario_otros_reunion").attr("requerido", false);
      }
  });

    


    $("#modalidad_cotizacion_cbx").on("change", function(){
        cargarAjaxComboCursos();
    });

    $("#pedido_modalidad_cbx").on("change", function(){
        cargarAjaxComboCursosPedido();
    });

    $("#btn-llamada").click(function(){
        if (validarRequeridoForm($("#form_llamada"))) {
            guardar_llamada();
        }
    });

    $("#btn-correo").click(function(){
        if (validarRequeridoForm($("#form_correo"))) {
            guardar_correo();
        }
    });     

    $("#tipo_correo_interno").prop("disabled", true);
    // Se utiliza paraindicar que se envio
    // un correo interno a un departamento edutecno

    $("#btn-reunion").click(function(){
        if (validarRequeridoForm($("#form_reunion"))) {
            guardar_reunion();
        }
    });		

    $("#btn-pedido").click(function(){
        /*if (validarRequeridoForm($("#form_pedido"))) {*/
            guardar_pedido();
            /*}*/
        });		

    $("#btn_guardar_pedido").click(function(){
        if (validarRequeridoForm($("#form_pedido"))) {
            addPerson(event); 
        }
    });  

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $('.chosen-select').chosen({width: "100%"});

    /* Listener Cambio Seletect Fase */
    $('#reunion_seleccione_fase_cbx').on('change', function() {
        if(this.value != ""){
            cargaAjaxComboTipoReunion(this.value);
            cargaAjaxCompromisoReunion(this.value);
        }else{
            $("#reunion_tipo_reunion_cbx").empty();
            $("#reunion_compromiso_cbx").empty();
        }
    });

    $('#correo_seleccione_fase_cbx').on('change', function () {
        if (this.value != "") {
            cargaAjaxComboTipoCorreo(this.value);
            cargaAjaxCompromisoCorreo(this.value);
        }else{
            $("#tipo_correo_interno").empty();
            $("#tipo_correo_interno").prop("disabled", true);
            $("#correo_compromiso_cbx").empty();
        }		  
    });

    $('#llamada_seleccione_fase_cbx').on('change', function () {
        if (this.value != "") {
            cargaAjaxComboTipoLlamada(this.value);
            cargaAjaxCompromisoLlamada(this.value);
        }else{
            $("tipo_llamada_cbx").empty();
            $("#llamada_compromiso_cbx").empty();
        }
    });

    $('#modalidad_llamada_cbx').on('change', function () {

     var sele=$("#modalidad_llamada_cbx option:selected").text();

     if(sele.toUpperCase()=="SIN ESPECIFICAR")
     {
        var textFase=$("#llamada_seleccione_fase_cbx option:selected").text();

        if(textFase.toUpperCase()=="PRE-VENTA")
        {
            $("#seleccione_producto_cbx_llamada").attr("requerido", false);
        } 
        else
        {
            $("#seleccione_producto_cbx_llamada").attr("requerido", true);
        }
    }
    else
    {  
        $("#seleccione_producto_cbx_llamada").attr("requerido", true);
    }


    if (this.value != "") {
        cargaAjaxComboProductoLlamada(this.value);
    } else {
        $("seleccione_producto_cbx_llamada").empty();
    }
});

    $('#modalidad_correo_cbx').on('change', function () {
        if (this.value != "") {
            cargaAjaxComboProductoCorreo(this.value);
        } else {
            $("seleccione_producto_cbx_correo").empty();
        }
    });

    $('#modalidad_reunion_cbx').on('change', function () {
        if (this.value != "") {
            cargaAjaxComboProductoReunion(this.value);
        } else {
            $("seleccione_producto_cbx").empty();
        }
    });


});


function modalllamada(){

    cargaAjaxComboModalidad(1)
    //cargaAjaxComboProducto(1);
    cargaAjaxComboFase(1);
    // cargaAjaxCompromisos(1);
    cargaAjaxContactosEmpresa($("#idempresallamada").val(),1);
    $('#myModal1').modal('toggle');
    $('#tipo_llamada_cbx').empty();
    $('#seleccione_producto_cbx_llamada').empty();

    limpiarFormularios('form_llamada');
}

function modalcorreo(){
    cargaAjaxComboModalidad(2);
    //cargaAjaxComboProducto(2);
    cargaAjaxComboFase(2);
    // cargaAjaxCompromisos(2);
    //cargaAjaxTiposCorreoInterno(); //---PRUEBA---
    cargaAjaxContactosEmpresa($("#idempresacorreo").val(),2);
    $('#myModal2').modal('toggle');

    $('#tipo_correo_cbx').empty();
    $('#seleccione_producto_cbx_correo').empty();

    limpiarFormularios('form_correo');
}

function modalreunion(){
    cargaAjaxComboModalidad(3);
    //cargaAjaxComboProducto(3);
    cargaAjaxComboFase(3)
    // cargaAjaxCompromisos(3);
    cargaAjaxContactosEmpresa($("#idempresareunion").val(),3);  
    $('#myModal3').modal('toggle');

    $('#reunion_tipo_reunion_cbx').empty();
    $('#seleccione_producto_cbx').empty();

    limpiarFormularios('form_reunion');
}

function modalpedido(){
    cargaAjaxComboModalidadPedido();
    //cargaAjaxComboTipoCursoPedido();
    $('#myModal5').modal('toggle');
    $('#footable-pedido').footable();
    limpiarFormularios('form_pedido');
}

function cotizacion(){
    cargarAjaxComboContactos($("#idempresareunion").val());
    cargarAjaxComboModalidad();
    cargarAjaxComboFormasPago();
    cargaCotizacionesCliente($("#idempresareunion").val());
    $('#myModal4').modal('toggle');
    $("#contactos_cotizacion_cbx").val("");
    $("#cantidad_asistentes_cotizacion").val("");
    $("#descuento_cotizacion").val("");
    $("#observacion_cotizacion").val("");
    $("#valor_cotizacion").val("");
    $('#footable-cotizacion').footable();
    $("#descuento_cotizacion").val(0);
    limpiarFormularios('frmCotizacion');
} 

function fecha(diferenciador){
    //Modal llamada
    if(diferenciador == 1){
        $('#data1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,                
            format: "dd-mm-yyyy"
        }); 
    }
    if(diferenciador == 2){
        $('#data2 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,                
            format: "dd/mm/yyyy"
        }); 
    }
    if(diferenciador == 3){
        $('#data3 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,                
            format: "dd-mm-yyyy"
        }); 
    }    
    if(diferenciador == 4){
        $('#data4 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,                
            format: "dd/mm/yyyy"
        }); 
    } 
    if(diferenciador == 5){
        $('#data5 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,                
            format: "yyyy-mm-dd"
        }); 
    } 
    if(diferenciador == 6){
        $('#data6 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: false,
            autoclose: true,                
            format: "dd/mm/yyyy"
        }); 
    }             
}          

function guardar(diferenciador){
    if(diferenciador == 1){
        $('#myModal').modal('toggle');
    }
    if(diferenciador == 2){
        $('#myModal2').modal('toggle');
    }
    if(diferenciador == 3){
        $('#myModal3').modal('toggle');
    } 
    if(diferenciador == 4){
        $('#myModal4').modal('toggle');
    }  
    if(diferenciador == 5){
        $('#myModal5').modal('toggle');
    }  
    if(diferenciador == 6){
        $('#myModal6').modal('toggle');
    }                 
}

$('.editar').click(function(){
    swal({
        title: "Oportunidad ingresada",
        text: "Pendiente Apobación del Supervisor",
        timer: 3000,
    });
});

//Llamada
function guardar_llamada(){

    console.log( $('#form_llamada').serialize());


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#form_llamada').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
            //console.log($('#form_llamada').serialize());
            alert('Cliente Potencial', jqXHR.responseText, 'error');
            alert(textStatus + ' - ' + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
			//alert(result[0].titulo);
            if (result[0].reset == 1) {
                limpiarFormularios('form_llamada');
                $('#myModal1').modal('toggle');
            }
        },
        url: "../Agregar_Accion_Llamada"
    }); 
}
//Correo
function guardar_correo(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#form_correo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
            //console.log($('#form_llamada').serialize());
            alert('Cliente Potencial', jqXHR.responseText, 'error');
            alert(textStatus + ' - ' + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            //alert(result[0].titulo);
            if (result[0].reset == 1) {
                limpiarFormularios('form_correo');
                $('#myModal2').modal('toggle');
            }
        },
        url: "../Agregar_Accion_Correo"
    }); 
}

//Reunión
function guardar_reunion(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#form_reunion').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
            console.log($('#form_reunion').serialize());
			//alerta('Cliente Potencial', jqXHR.responseText, 'error');
			//alert(textStatus + ' - ' + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
			//alert(result[0].titulo);
            if (result[0].reset == 1) {
                limpiarFormularios('form_reunion');
                $('#myModal3').modal('toggle');
            }
        },
        url: "../Agregar_Accion_Reunion"
    }); 	
}

//Pedido
function guardar_pedido(){
	// var contador = $('#pedido_identificador').val(); 
	var campo0 = $("#idempresareunion").val();
    $("#contenido tbody tr").each(function (index){
        var campo1, campo2, campo3, campo4, campo5;
        campo1=$(this).find('input').eq(0).val();
        campo2=$(this).find('input').eq(1).val();
        $(this).children("td").each(function (index2){
            switch (index2){
                case 0: campo1 = campo1;        //Modalidad
                break;
                case 1: campo2 = campo2;        //Tipo
                break;
                /*
                case 0: campo1 = $(this).text(); //Modalidad
                break;
                case 1: campo2 = $(this).text(); //Tipo
                break;
                */
                case 2: campo3 = $(this).text(); //Asistentes
                break;
                case 3: campo4 = $(this).text(); //Comentario
                break;
                case 4: campo5 = $(this).text(); //Fecha
                break;							
            }
        });
        //alert(campo1 + ' - ' + campo2 + ' - ' + campo3 + ' - ' + campo4 + ' - ' + campo5);
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data:{
                idempresareunion: campo0,
                pedido_modalidad_cbx: campo1,
                pedido_tipo_curso_cbx: campo2,
                asistentes_pedidos: campo3,
                pedido_comentario: campo4,
                pedido_fecha_reunion: campo5
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(textStatus + errorThrown);
                console.log(campo0 + ' - ' + campo1 + ' - ' + campo2 + ' - ' + campo3 + ' - ' + campo4 + ' - ' + campo5);
		    	//alerta('Cliente Potencial', jqXHR.responseText, 'error');
             alert(textStatus + ' - ' + errorThrown);
         },
         success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
				//alert(result[0].titulo);
                if (result[0].reset == 1) {
                 limpiarFormularios('form_pedido');
                 $('#myModal5').modal('toggle');
             }
         },
         url: "../Agregar_Accion_Pedido"
     }); 	   
    });
     // return false;	 
 }	

 function cargaAjaxCompromisoLlamada(idfase) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data: { fase: idfase },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboCompromisoLlamada(result);
        },
        url: "../getCompromisoLlamada"
    });
}

function cargaComboCompromisoLlamada(midata) {
    cargaCBX("llamada_compromiso_cbx", midata);
}

function cargaAjaxCompromisoReunion(idfase) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data: { fase: idfase },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboCompromisoReunion(result);
        },
        url: "../getCompromisooReunion"
    });
}

function cargaComboCompromisoReunion(midata) {
    cargaCBX("reunion_compromiso_cbx", midata);
    
}

function cargaAjaxCompromisoCorreo(idfase) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data: { fase: idfase },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboCompromisoCorreo(result);
        },
        url: "../getCompromisoCorreo"
    });
}

function cargaComboCompromisoCorreo(midata) {
    cargaCBX("correo_compromiso_cbx", midata);
}

function cargaAjaxContactosEmpresa(empresa,accion) {
    var comboCompromiso="";
    switch(accion){
        case 1:
        comboCompromiso="llamada_contacto_origen_cbx";
        break;
        case 2:
        comboCompromiso="correo_contacto_origen_cbx";
        break;
        case 3:
        comboCompromiso="reunion_contacto_origen_cbx";
        break;
    }
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_empresa: empresa }, 
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alert(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX(comboCompromiso, result);
            if(result.lenght==1) {  $("#"+comboCompromiso+" option[value='']").remove(); }
        },
        url: "../getContactoOrigenCbx"
    });
}

function cargaAjaxTiposCorreoInterno() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("tipo_correo_interno",result);
        },
        url: "../getTiposCorreoInternoCbx"
    });
}

function cargaAjaxComboFase(accion) {
    var comboFase = "";
    switch (accion) {
        case 1:
        comboFase = "llamada_seleccione_fase_cbx";
        break;
        case 2:
        comboFase = "correo_seleccione_fase_cbx";
        break;
        case 3:
        comboFase = "reunion_seleccione_fase_cbx";
        break;
    }

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX(comboFase, result);
        },
        url: "../getFase"
    });
}

function cargaAjaxComboModalidad(accion) {
    var comboModalidad = "";
    switch (accion) {
        case 1:
        comboModalidad = "modalidad_llamada_cbx";
        break;
        case 2:
        comboModalidad = "modalidad_correo_cbx";
        break;
        case 3:
        comboModalidad = "modalidad_reunion_cbx";
        break;
    }

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX(comboModalidad, result);
        },
        url: "../getModalidad"
    });

}

/*
function cargaAjaxComboProducto(accion) {
    $.ajax({
        url: "../getProductos",
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(jqXHR + textStatus + errorThrown);
            //alert(errorThrown);
        },
        success: function (result) {
            switch (accion) {
                case 1:
                    $("#seleccione_producto_cbx_llamada").select2({
                        data: result,

                    });
                break;
                case 2:
                    $("#seleccione_producto_cbx_correo").select2({
                        data: result,

                    });
                break;
                case 3:
                    $("#seleccione_producto_cbx").select2({
                        data: result,

                    });
                break;
            }
        },
    });
}*/

function cargaAjaxComboProductoLlamada(idmodalidad) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data: { modalidad: idmodalidad },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboModalidadLlamada(result);
        },
        url: "../getProductos",
    });
}

function cargaComboModalidadLlamada(midata) {
    cargaCBX("seleccione_producto_cbx_llamada", midata);
}

function cargaAjaxComboProductoCorreo(idmodalidad) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data: { modalidad: idmodalidad },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboModalidadCorreo(result);
        },
        url: "../getProductos",
    });
}

function cargaComboModalidadCorreo(midata) {
    cargaCBX("seleccione_producto_cbx_correo", midata);
}

function cargaAjaxComboProductoReunion(idmodalidad) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data: { modalidad: idmodalidad },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboModalidadReunion(result);
        },
        url: "../getProductos",
    });
}

function cargaComboModalidadReunion(midata) {
    cargaCBX("seleccione_producto_cbx", midata);
}

function cargaAjaxComboTipoLlamada(idfase) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data: { fase: idfase },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboTipoLlamada(result);
        },
        url: "../getTipoLlamada"
    });
}

function cargaComboTipoLlamada(midata) {
    cargaCBX("tipo_llamada_cbx", midata);
}

function cargaAjaxComboTipoReunion(idfase) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data:{ fase: idfase }, 

        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboTipoReunion(result);
        },
        url: "../getTipoReunion"
    });
    console.log(idfase);
}

function cargaComboTipoReunion(midata) {
    cargaCBX("reunion_tipo_reunion_cbx",midata);
}	

function cargaAjaxComboTipoCorreo(idfase) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        data: { fase: idfase },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboTipoCorreo(result);
        },
        url: "../getTipoCorreo"
    });
}

function cargaComboTipoCorreo(midata) {
    cargaCBX("tipo_correo_cbx", midata);
}	

function cargaAjaxComboModalidadPedido() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
		//data:{ modalidad: idmodalidad }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboModalidadPedido(result);
        },
        url: "../getTipoPedido"
    });
}	

function cargaComboModalidadPedido(midata) {
    cargaCBX("pedido_modalidad_cbx",midata);
    nombres(midata);
}

function cargaAjaxComboTipoCursoPedido() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
		//data:{ TipoCurso: TipoCurso }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboTipoCursoPedido(result);
        },
        url: "../getTipoCurso"
    });
}	

function cargaComboTipoCursoPedido(midata) {
  cargaCBX("pedido_tipo_curso_cbx",midata);
}

// Agregar filas a tabla de pedidos
function addPerson(e) {
    // var contador = $('#pedido_identificador').val();
    e.preventDefault();
    const row = createRow({
        pedido_identificador: $('#pedido_identificador').val(),
        pedido_modalidad_nom: $('#pedido_modalidad_cbx option:selected').text(),
        pedido_modalidad_cbx: $('#pedido_modalidad_cbx').val(),
        pedido_tipo_curso_nom: $('#pedido_tipo_curso_cbx option:selected').text(),
        pedido_tipo_curso_cbx: $('#pedido_tipo_curso_cbx').val(),
        pedido_cant_asistentes: $('#pedido_cant_asistentes').val(),
        pedido_comentario: $('#pedido_comentario').val(),
        pedido_fecha_reunion: $('#pedido_fecha_reunion').val()	
    });
    $('#contenido tbody').append(row);
    // contador++;
    // $('#pedido_identificador').val(contador); 
    clean();
}

function limpiarFormularios(formulario){
    var form = formulario;
    $(':input','#'+form)
    .not(':button, :submit, :reset, :hidden, #pedido_seleccione_cliente, #pedido_fecha_reunion, #fecha_ingreso_cotizacion, #fecha_termino_cotizacion, #llamada_fecha_llamada, #llamada_fecha_compromiso, #fecha_correo, #correo_fecha_compromiso, #reunion_fecha_reunion, #reunion_fecha_compromiso')
    .val('')
    .removeAttr('checked')
    .removeAttr('selected');
    if (form =="form_pedido") { 
	    // $('#contenido').empty();
        $("#contenido tbody tr").each(function (index) {
            $(this).remove();
        });
    }
}

function createRow(data) {
    return (
        `<tr><input type="hidden" value="`+data.pedido_modalidad_cbx+`" id=""><input type="hidden" value="`+data.pedido_tipo_curso_cbx+`" id="">` +
        `<td>${data.pedido_modalidad_nom}</td>` +
        `<td>${data.pedido_tipo_curso_nom}</td>` +
        `<td>${data.pedido_cant_asistentes}</td>` +
        `<td>${data.pedido_comentario}</td>` +
        `<td>${data.pedido_fecha_reunion}</td>` +
        `<td><button type="button" class="btn btn-primary btn-xs" id="deleteRegistro${data.pedido_identificador}" onclick="eliminartr(${data.pedido_identificador})">eliminar</button></td>` +
        `</tr>`
        );
}

function clean() {
  $('#pedido_modalidad_cbx').val('');
  $('#pedido_tipo_curso_cbx').val('');
  $('#pedido_cant_asistentes').val('');
  $('#pedido_comentario').val('');
  $('#pedido_modalidad_cbx').focus();
}		

function eliminartr(id){
    var id = id;
    var tr = $("#deleteRegistro"+id).closest('tr'); 
    tr.css("background-color","#3e2c42"); 
    tr.css("color","#fff");
    tr.fadeOut(400, function(){ 
        tr.remove(); }); return false; 	
}			

function nombres(data){
    var descripcion = data[0].text;
    var id = id;
    var tr = $("#deleteRegistro"+id).closest('tr'); 
    tr.css("background-color","#3e2c42"); 
    tr.fadeOut(400, function(){ 
	//tr.remove(); 
}); 
    return false; 	
}	

// $('#myModal5').modal('toggle');

function cargarFechaCompromiso(nombreSelect, nombreInputFecha){
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        data: { id_compromiso: $("#"+nombreSelect).val() }, 
        //  data: $('#frm').serialize(),
        //data:{ TipoCurso: TipoCurso }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log();
            $("#"+nombreInputFecha).val(result[0].Fecha)
        },
        url: "../getFechaCierreCompromiso"
    });
}

//Cotización
function cargarAjaxComboContactos(empresa) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_empresa: empresa }, 
        //  data: $('#frm').serialize(),
        //data:{ TipoCurso: TipoCurso }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboContactos(result);
        },
        url: "../getContactoOrigenCbx"
    });
} 

function cargaComboContactos(midata) {
  cargaCBX("contactos_cotizacion_cbx",midata);
}

function cargarAjaxComboModalidad() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        //data:{ TipoCurso: TipoCurso }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboModalidadCotizacion(result);
        },
        url: "../getTipoPedido"
    });
} 

function cargaComboModalidadCotizacion(midata){
  cargaCBX("modalidad_cotizacion_cbx", midata);
}

function cargarAjaxComboCursos() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //  data: $('#frm').serialize(),
        data:{ id_modalidad: $("#modalidad_cotizacion_cbx").val() }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboCursoCotizacion(result);
        },
        url: "../getCursosByModalidad"
    });
} 

function cargaComboCursoCotizacion(midata){
  cargaCBX("curso_cotizacion_cbx", midata);
}

function cargarAjaxComboCursosPedido() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //  data: $('#frm').serialize(),
        data:{ id_modalidad: $("#pedido_modalidad_cbx").val() }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboCursoPedido(result);
        },
        url: "../getCursosByModalidad"
    });
} 

function cargaComboCursoPedido(midata){
    cargaCBX("pedido_tipo_curso_cbx", midata);
}

function cargarAjaxComboFormasPago() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        //data:{ TipoCurso: TipoCurso }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaComboFormasPagoCotizacion(result);
        },
        url: "../getFormasPago"
    });
} 

function cargaComboFormasPagoCotizacion(midata){
  cargaCBX("formas_pago_cotizacion_cbx", midata);
}

function guardarCotizacion(){
    if(Date.parse($("#fecha_ingreso_cotizacion").val()) <= Date.parse($("#fecha_termino_cotizacion").val())){
        if($("#valor_cotizacion").val().length > 0){
            $.ajax({
                async: true,
                cache: false,
                dataType: "json",
                type: 'POST',
                //  data: $('#frm').serialize(),
                data:{ 
                    idempresacotizacion: $("#idempresacotizacion").val(),
                    contactos_cotizacion_cbx: $("#contactos_cotizacion_cbx").val(),
                    fecha_ingreso_cotizacion: $("#fecha_ingreso_cotizacion").val(),
                    fecha_termino_cotizacion: $("#fecha_termino_cotizacion").val(),
                    curso_cotizacion_cbx: $("#curso_cotizacion_cbx").val(),
                    cantidad_asistentes_cotizacion: $("#cantidad_asistentes_cotizacion").val(),
                    formas_pago_cotizacion_cbx: $("#formas_pago_cotizacion_cbx").val(),
                    descuento_cotizacion: $("#descuento_cotizacion").val(),
                    observacion_cotizacion: $("#observacion_cotizacion").val(),
                    valor_total: $("#valor_cotizacion").val()
                }, 
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(jqXHR);
                },
                success: function (result) {
                    console.log(result);
                    swal("Exito!","Cotización generada correctamente!.","success");
                    limpiarFormularios('formulario_cot');
                    //$('#myModal4').modal('toggle');
                    cargaCotizacionesCliente($("#idempresacotizacion").val());
                    $('#formulario_cot').collapse('hide');
                    $('#lista_cot').collapse('show');
                },
                url: "../addCotizacion"
            });
        }else{
            swal("Error!","El valor total de la cotización, no puede quedar vacío.","error");
        }
    }else{
        swal("Error!", "Fecha de inicio debe ser menor a la fecha de termino", "error");
    }
}

function cargaCotizacionesCliente(id_empresa){
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //  data: $('#frm').serialize(),
        data:{ id_empresa:  id_empresa}, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //var strHtml = "";
            $('#cotizacionesCliente tbody').html("");
            $.each(result, function(i, item){
                //strHtml += "<div class=\"col-md-4\"><a href=\"../getCotizacionById/"+item.IdCotizacion+"\" target=\"_blank\"><i class=\"fa fa-file-excel-o fa-2x\"></i> <br/><label style=\"width:40px;\">"+item.NombreCliente+" "+item.Fecha+"</label></a></div>";
                //$("#cotizacionesCliente").html($("#cotizacionesCliente").html() +" " + "<div class=\"col-md-4\"><a href=\"../getCotizacionById/"+item.IdCotizacion+"\" target=\"_blank\"><i class=\"fa fa-file-excel-o fa-2x\"></i> <br/><label style=\"width:40px;\">"+item.NombreCliente+" "+item.Fecha+"</label></a></div>");
                //jQuery('#cotizacionesCliente').append("<div class=\"col-md-4\"><a href=\"../getCotizacionById/"+item.IdCotizacion+"\" target=\"_blank\"><i class=\"fa fa-file-excel-o fa-2x\"></i> <br/><label style=\"width:40px;\">"+item.NombreCliente+" "+item.Fecha+"</label></a></div>");
                
                var fila="<tr>";
                fila+="<td><div class='dropdown'>";
                fila+="<a class='dropdown-toggle' type='button' data-toggle='dropdown'>";
                fila+="<i class='fa fa-file-excel-o fa-2x'></i> <br/><label style='width:100%;'>"+item.NombreCliente+" "+item.Fecha+"</label>";
                fila+="</a>";
                fila+="<ul class='dropdown-menu'>";
                fila+="<li><a href='../getCotizacionById/"+item.IdCotizacion+"' target='_blank'>Exportar a PDF</a></li>";
                fila+="<li><a href='../getCotizacionWordById/"+item.IdCotizacion+"' target='_blank'>Exportar a Word</a></li>";
                fila+="</ul>";
                fila+="</div>";
                fila+="</tr>";

                $('#cotizacionesCliente tbody').append(fila);

                //$('#cotizacionesCliente tbody').append("<tr><td><a href='../getCotizacionById/"+item.IdCotizacion+"' target='_blank'><i class='fa fa-file-excel-o fa-2x'></i> <br/><label style='width:100%;'>"+item.NombreCliente+" "+item.Fecha+"</label></a></td></tr>");
            });

        //$("#cotizacionesCliente").html(strHtml);
    },
    url: "../getCotizaciones"
});
}

function genValorTotal(){
    if (validarRequeridoForm($("#frmCotizacion"))) {
        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'GET',
            //  data: $('#frm').serialize(),
            data:{ 
                id_curso:   $("#curso_cotizacion_cbx").val(),
                cantidad_asistentes: $("#cantidad_asistentes_cotizacion").val(),
                descuento:$("#descuento_cotizacion").val()
            }, 
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(textStatus + errorThrown);
            },
            success: function (result) {
                console.log(result[0].valor);
                $("#valor_cotizacion").val(result[0].valor);
            },
            url: "../getValorTotalCurso"
        });
    }
}
