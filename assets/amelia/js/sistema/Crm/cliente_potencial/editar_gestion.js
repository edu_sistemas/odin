        $(document).ready(function(){


         $("#btn-llamada").click(function(){
          if (validarRequeridoForm($("#form_editar_gestion"))) {
                  //alert ($("#llamada_fecha_compromiso").val());
                  guardar_llamada();
                }
              });   

         $("#tipo_correo_interno").prop("disabled", true);	


            /*

              BOTON PEDIDO

            $("#btn-pedido").click(function(){
             if (validarRequeridoForm($("#form_pedido"))) {
                 guardar_pedido();
             }
             $('#myModal5').modal('toggle');	
             $('#myModal5').modal('toggle');
           });	 */			

           $('.chosen-select').chosen({width: "100%"});



	        /* Listener Cambio Seletect Fase */ //---PRUEBA---

          var tipo_accion = parseInt($('#hidden_id_accion_gestion').val());
          
          //console.log(tipo_accion);


          $('#fase').on('change', function() {

            if(this.value != "")
            { 
             cargaAjaxCompromisos(this.value);

             if(tipo_accion==3)
             {
              cargaAjaxComboTipoReunion(this.value);
            }
            else if(tipo_accion==2)
            {
             cargaAjaxTiposLlamada(tipo_accion,this.value);
             if($("#hidden_subtipo_correo_reunion").val()!="0")
             {
               cargaAjaxTiposCorreoInterno(tipo_accion,this.value);
             }
             else
             {
               $("#subtipo_correo_reunion").empty();
               $("#subtipo_correo_reunion").prop("disabled", true);
             }

             $('#llamada_tipo_llamada').on('change', function() {

              var tls=$("#llamada_tipo_llamada option:selected").text();
              if(tls == "Interno Edutecno" )
              {
                cargaAjaxTiposCorreoInterno(this.value);
                $("#subtipo_correo_reunion").prop("disabled", false);
              }      
              else
              {
                $("#subtipo_correo_reunion").empty();
                $("#subtipo_correo_reunion").prop("disabled", true);   
              } 
            });

           }
           else if(tipo_accion==1)
           {
            cargaAjaxTiposLlamada(tipo_accion,this.value);
          }
        }

      });  


          $('#id_modalidad').on('change', function() {

            var sele=$("#id_modalidad option:selected").text();

            if(sele.toUpperCase()=="SIN ESPECIFICAR")
            {
             var textFase=$("#fase option:selected").text();

             if(textFase.toUpperCase()=="PRE-VENTA")
             {
              $("#id_producto").attr("requerido", false);
            } 
            else
            {
             $("#id_producto").attr("requerido", true);
           }

         }
         else
         {  
          console.log('IActivo');
          $("#id_producto").attr("requerido", true);
        }

        cargaAjaxComboProducto(this.value);
      });

          $('#llamada_compromiso_cbx').on('change', function() {

            var sele=$("#llamada_compromiso_cbx option:selected").text();
            if(sele=="Otros")
            {
              $("#div_otros").show();
              $("#comentario_otros").val($("#hidden_comentario_otros").val());
              $("#comentario_otros").attr("requerido", true);
            }
            else
            {  
              $("#div_otros").hide();
              $("#comentario_otros").attr("requerido", false);
            }

          });


          /*
          switch (tipo_accion) 
          {
            case 2: 
            $('#llamada_tipo_llamada').on('change', function() {

              if(this.value == 5 )
              {
                cargaAjaxComboTipoLlamada(this.value);
                cargaAjaxCompromisos(this.value);

                //cargaAjaxTiposCorreoInterno(this.value);
                $("#subtipo_correo_reunion").prop("disabled", false);

              }      
              else
              {
                $("#subtipo_correo_reunion").empty();
                $("#subtipo_correo_reunion").prop("disabled", true);   
              }  

            });   

            break;
            case 3: 
            $('#llamada_tipo_llamada').on('change', function() 
            {
             if(this.value != "")
             { 
               cargaAjaxComboTipoReunion(this.value);
               cargaAjaxCompromisos(this.value);
             }
             else
             {
              $("#subtipo_correo_reunion").empty();
            }

          });
            break;            
          }
          */
          


          var contacto_empresa = parseInt($('#idempresallamada').val());

          var fase=$("#hidden_fase").val();

            //llena acciones
            //alert(tipo_accion);
            nombresDinamicos(tipo_accion);
            cargaAjaxTiposLlamada(0);
            cargaAjaxComboModalidad();
            var modalidad=$("#hidden_id_modalidad").val();
            cargaAjaxComboProducto(modalidad);

            cargaAjaxContactosEmpresa(contacto_empresa,tipo_accion);
            CargaEditorGestion();

          });


/*

        function modalpedido(){
            cargaAjaxComboModalidadPedido();
            cargaAjaxComboTipoCursoPedido();
            $('#myModal5').modal('toggle');
            $('#footable-pedido').footable();
        }



        function cotizacion(){
           $('#myModal4').modal('toggle');
           $('#footable-cotizacion').footable();

       } 
       */    

       function fecha(diferenciador){
                        //Modal llamada
                        if(diferenciador == 1){
                          $('#data1 .input-group.date').datepicker({
                            todayBtn: "linked",
                            keyboardNavigation: false,
                            forceParse: false,
                            calendarWeeks: false,
                            autoclose: true,                
                            format: "dd-mm-yyyy"
                          }); 
                        }
                        if(diferenciador == 2){
                          $('#data2 .input-group.date').datepicker({
                            todayBtn: "linked",
                            keyboardNavigation: false,
                            forceParse: false,
                            calendarWeeks: false,
                            autoclose: true,                
                            format: "dd/mm/yyyy"
                          }); 
                        }
                        if(diferenciador == 3){
                          $('#data3 .input-group.date').datepicker({
                            todayBtn: "linked",
                            keyboardNavigation: false,
                            forceParse: false,
                            calendarWeeks: false,
                            autoclose: true,                
                            format: "dd-mm-yyyy"
                          }); 
                        }    
                        if(diferenciador == 4){
                          $('#data4 .input-group.date').datepicker({
                            todayBtn: "linked",
                            keyboardNavigation: false,
                            forceParse: false,
                            calendarWeeks: false,
                            autoclose: true,                
                            format: "dd/mm/yyyy"
                          }); 
                        } 
                        if(diferenciador == 5){

                          $('#data5 .input-group.date').datepicker({
                            todayBtn: "linked",
                            keyboardNavigation: false,
                            forceParse: false,
                            calendarWeeks: false,
                            autoclose: true,                
                            format: "dd-mm-yyyy"
                          }); 


                        } 
                        if(diferenciador == 6){
                          $('#data6 .input-group.date').datepicker({
                            todayBtn: "linked",
                            keyboardNavigation: false,
                            forceParse: false,
                            calendarWeeks: false,
                            autoclose: true,                
                            format: "dd/mm/yyyy"
                          }); 
                        }             
                      }          


                      function guardar(diferenciador){
                        if(diferenciador == 1){
                          $('#myModal').modal('toggle');
                        }
                        if(diferenciador == 2){
                          $('#myModal2').modal('toggle');
                        }
                        if(diferenciador == 3){
                          $('#myModal3').modal('toggle');
                        } 
                        if(diferenciador == 4){
                          $('#myModal4').modal('toggle');
                        }  
                        if(diferenciador == 5){
                          $('#myModal5').modal('toggle');
                        }  
                        if(diferenciador == 6){
                          $('#myModal6').modal('toggle');
                        }                 
                      }
/*
                $('.editar').click(function(){
                    swal({
                        title: "Oportunidad ingresada",
                        text: "Pendiente Apobación del Supervisor",
                        timer: 3000,
                    });
                });

                */
                /****            Llamada          ****/ 
                
                function guardar_llamada(){

                  $.ajax({
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: 'POST',
                    data: $('#form_editar_gestion').serialize(),
                    error: function (jqXHR, textStatus, errorThrown) {
            //code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            //console.log($('#form_llamada').serialize());
            alert('Editar gestión', jqXHR.responseText, 'error');
            alert(textStatus + ' - ' + errorThrown);
          },
          success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
				//alert(result[0].titulo);
        if (result[0].reset == 1) {
         $('#myModal1').modal('toggle');
       }            

     },
     url: "../Agregar_Accion_Llamada"

   }); 

                }

                /************************************************************************/

                function nombresDinamicos(accion){

                  switch(accion)
                  {
                    case 1:
                    $('#subtipo_gestion').hide();
                    $('#tipo_gestion').text("Tipo de Llamada");
                    $('#titulo_fecha').text("Fecha Llamada");
                    break;
                    case 2:
                    $('#subtipo_gestion').show();
                    $('#tipo_gestion').text("Tipo de Correo");
                    $('#titulo_subtipo_correo_reunion').text("Correo Interno");
                    $('#titulo_fecha').text("Fecha Correo");
                    break;
                    case 3:
                    $('#subtipo_gestion').hide();
                    $('#tipo_gestion').text("Tipo de Reunión");
                    $('#titulo_subtipo_correo_reunion').text("Tipo de Reunión");
                    $('#titulo_fecha').text("Fecha Reunión");
                    break;
                  }
                }

                function cargaAjaxTiposLlamada(accion,idfase) {

                  switch(accion)
                  {
                    case 0:
                    $url = "../getFases"
                    break;  
                    case 1:
                    $url = "../getTiposLlamadaCbx"
                    break;
                    case 2:
                    $url = "../getTiposCorreoCbx"
                    break;
                    case 3:
                    $url = "../getFaseReunion"
                    break;
                  }

                  $.ajax({
                    data: { fase: idfase },
                    async: false,
                    dataType: "json",
                    type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR + textStatus + errorThrown);
          },
          success: function (result) {

            if(accion==0)
            {
             cargaCBXSelected("fase",result);
             $("#fase").val($("#hidden_fase").val()).trigger("change");
           }
           else
           {
             var fase = $("#id_fase").val();
             cargaCBXSelected("llamada_tipo_llamada",result);
             $("#llamada_tipo_llamada").val($("#hidden_llamada_tipo_llamada").val()).trigger("change");
           }
         },
         url: $url
       });        

                }

                function cargaAjaxCompromisos(fase) {

                  var comboCompromiso="llamada_compromiso_cbx";

                  $.ajax({
                    data: { fase: fase },
                    async: true,
                    cache: false,
                    dataType: "json",
                    type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
          },
          success: function (result) {

            cargaCBX(comboCompromiso,result);

            $("#llamada_compromiso_cbx").val($("#hidden_llamada_compromiso_cbx").val());

            var sele=$("#llamada_compromiso_cbx option:selected").text();
            if(sele=="Otros")
            {
              $("#div_otros").show();
              $("#comentario_otros").val($("#hidden_comentario_otros").val());
              $("#comentario_otros").attr("requerido", true);
            }
            else
            {  
              $("#div_otros").hide();
              $("#comentario_otros").attr("requerido", false); 
            }

          },
          url: "../getCompromisoCbx"
        });
                }


                function cargaAjaxContactosEmpresa(empresa,accion) {

                  var comboCompromiso="llamada_contacto_origen_cbx";


                  $.ajax({
                    async: true,
                    cache: false,
                    dataType: "json",
                    type: 'POST',
                    data: { id_empresa: empresa }, 
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alert(jqXHR + textStatus+ errorThrown);
          },
          success: function (result) {
            cargaCBX(comboCompromiso, result);

            if(result.lenght==1) {  $("#"+comboCompromiso+" option[value='']").remove(); }	

            $("#llamada_contacto_origen_cbx").val($("#hidden_llamada_contacto_origen_cbx").val()).trigger("change");
          },
          url: "../getContactoOrigenCbx"
        });

                }


//--------------------------PRUEBA------------------------------

function cargaAjaxTiposCorreoInterno() {

  $.ajax({
    async: true,
    cache: false,
    dataType: "json",
    type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
          },
          success: function (result) {

            cargaCBX("subtipo_correo_reunion",result);
            $("#subtipo_correo_reunion").val($("#hidden_subtipo_correo_reunion").val()).trigger("change");
          },
          url: "../getTiposCorreoInternoCbx"
        });
}

//--------------------------FIN PRUEBA--------------------------

function cargaAjaxComboTipoReunion(idfase) {

  $.ajax({
    async: false,
    cache: false,
    dataType: "json",
    type: 'GET',
        //  data: $('#frm').serialize(),
        data:{ fase: idfase }, 
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
          cargaComboTipoReunion(result);
        },
        url: "../getTipoReunion"
      });
}

function cargaComboTipoReunion(midata) 
{
  cargaCBX("llamada_tipo_llamada",midata);
  $("#llamada_tipo_llamada").val($("#hidden_llamada_tipo_llamada").val()).trigger("change");
}	

function cargarFechaCompromiso(nombreSelect, nombreInputFecha, nombreInputFechaHidden){
  $.ajax({
    async: true,
    cache: false,
    dataType: "json",
    type: 'GET',
    data: { id_compromiso: $("#"+nombreSelect).val() }, 
    error: function (jqXHR, textStatus, errorThrown) {
    },
    success: function (result) {
      console.log();
      $("#"+nombreInputFecha).val(result[0].Fecha);
      $("#"+nombreInputFechaHidden).val(result[0].Fecha);
    },
    url: "../getFechaCierreCompromiso"
  });
}


function cargaAjaxComboModalidad() {
  $.ajax({
    async: false,
    cache: false,
    dataType: "json",
    type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
          },
          success: function (result) {
            cargaCBX("id_modalidad", result);
            $("#id_modalidad").val($("#hidden_id_modalidad").val()).trigger("change");

            var textFase=$("#fase option:selected").text();
            var sele=$("#id_modalidad option:selected").text();

            if(sele=="Sin Especificar")
            {
             if(textFase=="Pre-venta")
             {
              $("#id_producto").attr("requerido", false);
            } 
            else
            {
             $("#id_producto").attr("requerido", true);
           }

         }
         else
         {  
          $("#id_producto").attr("requerido", true);
        }

      },
      url: "../getModalidad"
    });

}


function cargaAjaxComboProducto(id_modalidad) {

  $.ajax({
    async: false,
    cache: false,
    dataType: "json",
    type: 'GET',
    data: { id_modalidad: id_modalidad }, 
    error: function (jqXHR, textStatus, errorThrown) {
    },
    success: function (result) {
     cargaCBX("id_producto",result);
     $("#id_producto").val($("#hidden_id_producto").val()).trigger("change");


   },
   url: "../getProductos"
 });
}



/*
    function cargaAjaxComboModalidadPedido() {

        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'GET',
        //  data: $('#frm').serialize(),
		//data:{ modalidad: idmodalidad }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaComboModalidadPedido(result);

        },
        url: "../getTipoPedido"
    });
    }	


    function cargaComboModalidadPedido(midata) {

      cargaCBX("pedido_modalidad_cbx",midata);
      nombres(midata);
  }



  function cargaAjaxComboTipoCursoPedido() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
		//data:{ TipoCurso: TipoCurso }, 
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaComboTipoCursoPedido(result);

        },
        url: "../getTipoCurso"
    });
}	


function cargaComboTipoCursoPedido(midata) {

  cargaCBX("pedido_tipo_curso_cbx",midata);


}


// Agregar filas a tabla de pedidos

function addPerson(e) {

// var contador = $('#pedido_identificador').val();

e.preventDefault();
const row = createRow({
    pedido_identificador: $('#pedido_identificador').val(),
    pedido_modalidad_cbx: $('#pedido_modalidad_cbx').val(),
    pedido_tipo_curso_cbx: $('#pedido_tipo_curso_cbx').val(),
    pedido_cant_asistentes: $('#pedido_cant_asistentes').val(),
    pedido_comentario: $('#pedido_comentario').val(),
    pedido_fecha_reunion: $('#pedido_fecha_reunion').val()	
});
$('#contenido tbody').append(row);

 // contador++;
 // $('#pedido_identificador').val(contador); 

 clean();
}

function limpiarFormularios(formulario){


	var form = formulario;
	
  $(':input','#'+form)
  .not(':button, :submit, :reset, :hidden, #pedido_seleccione_cliente, #pedido_fecha_reunion')
  .val('')
  .removeAttr('checked')
  .removeAttr('selected');


  if (form =="form_pedido") { 
	    // $('#contenido').empty();
      $("#contenido tbody tr").each(function (index) {
         $(this).remove();
     });
  }

}


function createRow(data) {
  return (
    `<tr>` +
    `<td>${data.pedido_modalidad_cbx}</td>` +
    `<td>${data.pedido_tipo_curso_cbx}</td>` +
    `<td>${data.pedido_cant_asistentes}</td>` +
    `<td>${data.pedido_comentario}</td>` +
    `<td>${data.pedido_fecha_reunion}</td>` +
    `<td><button type="button" class="btn btn-primary btn-xs" id="deleteRegistro${data.pedido_identificador}" onclick="eliminartr(${data.pedido_identificador})">eliminar</button></td>` +
    `</tr>`
    );

}

function clean() {
  $('#pedido_modalidad_cbx').val('');
  $('#pedido_tipo_curso_cbx').val('');
  $('#pedido_cant_asistentes').val('');
  $('#pedido_comentario').val('');
  $('#pedido_modalidad_cbx').focus();
}		
*/
function eliminartr(id){
	
  var id = id;
  var tr = $("#deleteRegistro"+id).closest('tr'); 
  tr.css("background-color","#3e2c42"); 
  tr.css("color","#fff");
  tr.fadeOut(400, function(){ 
    tr.remove(); }); return false; 	



}			

function nombres(data){
	var descripcion = data[0].text;
	var id = id;
  var tr = $("#deleteRegistro"+id).closest('tr'); 
  tr.css("background-color","#3e2c42"); 
  tr.fadeOut(400, function(){ 

		//tr.remove(); 
		
  }); return false; 	

}	

// $('#myModal5').modal('toggle');


// JS NCESARIO

function CargaEditorGestion(){
  /*
  $("#fase").val($("#hidden_fase").val()).trigger("change");
  $("#llamada_tipo_llamada").val($("#hidden_llamada_tipo_llamada").val()).trigger("change");
  */
  $("#llamada_compromiso_cbx").val($("#hidden_llamada_compromiso_cbx").val()).trigger("change");
  $("#llamada_fecha_llamada").val($("#hidden_llamada_fecha_llamada").val()).trigger("change");
  $("#llamada_asunto_box").val($("#hidden_llamada_asunto_box").val()).trigger("change");
  $("#llamada_comentario_box").val($("#hidden_llamada_comentario_box").val()).trigger("change");
  
  $("#llamada_fecha_compromiso").val($("#hidden_llamada_fecha_compromiso").val()).trigger("change");

}
