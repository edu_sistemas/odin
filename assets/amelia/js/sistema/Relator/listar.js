$(document).ready(function () {
    recargaLaPagina();
    buscarFiltro();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_nuevo_contacto").click(function () {
        $("#div_frm_add").css("display", "block");
        $("#div_tbl_contacto_relator").css("display", "none");
        $("#btn_nuevo_contacto").css("display", "none");
    });


    $("#btn_guardar_contacto").click(function () {
        if (validarRequeridoForm($("#frm_nuevo_contacto_relator"))) {
            RegistarNuevoContacto();
        }
    });

    $("#btn_cancelar_contacto").click(function () {
        $("#div_frm_add").css("display", "none");
        $("#div_tbl_contacto_relator").css("display", "block");
        $("#btn_nuevo_contacto").css("display", "block");
    });

    $("#btn_buscar").click(function () {
        buscarFiltro();
    });
});

function RegistarNuevoContacto() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    var url = "Listar/guardarDatosContacto";


    if ($("#btn_guardar_contacto").text() == "Actualizar") {
        url = "Listar/actualzarDatosContacto";
    }


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_nuevo_contacto_relator').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Nuevo Contacto', errorThrown, 'error');
        },
        success: function (result) {

            //  alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {

                $("#div_frm_add").css("display", "none");
                $("#div_tbl_contacto_relator").css("display", "block");
                $("#btn_nuevo_contacto").css("display", "block");

                ContactoRelator($("#txt_id_relator").val(), $("#modal_nombre_contacto").text());

                $("#btn_limpiar").click();
                $("#btn_guardar_contacto").text("Guardar");

            }
        },
        url: url
    });


}



function buscarFiltro() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#filtros_form').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Relator", errorThrown, "error");
        },
        success: function (result) {
            cargaBusqueda(result);
        },
        url: "Listar/buscarPorFiltro"
    });

}


// Crea tabla del listado pro Jquery luis
function cargaBusqueda(data) {

    if ($.fn.dataTable.isDataTable('#tbl_relatores')) {
        $('#tbl_relatores').DataTable().destroy();
    }

    $('#tbl_relatores').DataTable({
        "aaData": data,
        "aoColumns": [{
                "mDataProp": "rut"
            }, {
                "mDataProp": "username"
            }, {
                "mDataProp": "nombre"
            }, {
                "mDataProp": "apellidos"
            }, {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var nombre_completo = "";
                    var parametros = "";
                    nombre_completo = o.nombre_relator + " " + o.apellido_paterno_relator;
                    parametros = '"' + o.id_relator + '","' + nombre_completo + '"';
                    html += "  <button class='btn btn-primary' title='Contacto Relator' onclick='ContactoRelator(" + parametros + ");' type='button'>&nbsp;&nbsp;<i class='fa fa-phone-square'></i>&nbsp;&nbsp;&nbsp;</button>";
                    return html;
                }
            }, {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "  <button class='btn btn-default' title='Cursos del  Relator' onclick='cursosRelator(" + o.id + ");' type='button'>&nbsp;&nbsp;<i class='fa fa-book'></i>&nbsp;&nbsp;&nbsp;</button>";
                    return html;
                }
            }, {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    var x = "Editar/index/" + o.id_relator;
                    html += "<li><a href='" + x + "'>Modificar</a></li>";
                    if (o.estado == 1) {
                        html += "<li><a    onclick='EditarRelatorEstado(" + o.id_relator + ",0);'>Inactivo</a></li>";
                    } else {
                        html += "<li><a    onclick='EditarRelatorEstado(" + o.id_relator + ",1);'>Activo</a></li> ";
                    }

                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Perfiles'
            }, {
                extend: 'pdf',
                title: 'Perfiles'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

}


function EditarRelatorEstado(id_relator, tipo_estado) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id: id_relator, estado: tipo_estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Relator", errorThrown, "error");
        },
        success: function (result) {

            buscarFiltro();
        },
        url: "Listar/CambiarEstadoRelator"
    });

}

function ContactoRelator(id, nombre_contacto) {
    $("#modal_nombre_contacto").text(nombre_contacto);
    $("#txt_id_relator").val(id);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_relator: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Relator", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosContactoRelator(result);
            $("#modal_contacto_alummno").modal("show");
        },
        url: "Listar/verDatosContacto"
    });
}

function cursosRelator(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_relator: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Relator", errorThrown, "error");
        },
        success: function (result) {
            cargaCursosRelator(result);
        },
        url: "Listar/verCursosRelator"
    });

}

function cargaDatosContactoRelator(data) {
    var html = "";
    $('#tbl_contacto_relator tbody').html(html);
    $.each(data, function (i, item) {
        html += '        <tr>';
        html += '        <td>' + item.correo_contacto + '</td> ';
        html += '        <td>' + item.telefono_contacto + '</td> ';
        html += '        <td>' + item.fecha_registro + '</td> ';
        html += '        <td>';

        if (item.estado_contacto == 1) {
            html += '<button class="btn btn btn-primary readonly btn-circle"  title="Activo" type="button"><i class="fa fa-check"></i></button>';
        } else {
            html += '<button class="btn btn-danger btn-circle" readonly title="Inactivo" type="button"><i class="fa fa-times"></i></button>';
        }

        html += '</td>';
        html += '<td>';

        if (item.estado_contacto == 1) {
            html += '<button class="btn btn-warning btn-circle btn-sm" type="button" title="desactivar" onclick="desactivarContactoRelator(' + item.id_contacto_relator + ',0)" ><i class="fa fa-minus-circle"></i></button>';
        } else {
            html += '<button class="btn btn-primary btn-circle btn-sm" type="button" title="Activar" onclick="desactivarContactoRelator(' + item.id_contacto_relator + ',1)" ><i class="fa fa-minus-circle"></i></button>';
        }

        var datos = "";
        datos = "'" + item.id_contacto_relator + "','" + item.correo_contacto + "','" + item.telefono_contacto + "','" + item.id_relator + "'";

        html += '<button onclick="ubicarCamposUpdateContactoRelator(' + datos + ')" class="btn btn-info btn-circle btn-sm" type="button" title="Editar" ><i class="fa fa-edit"></i>  </button>';
        html += '<button   onclick="EliminarContactoRelator(' + datos + ')" class="btn btn-danger btn-circle btn-sm" type="button" title="Elimiar" ><i class="fa fa-times-circle-o"></i>  </button>';
        html += '</td> ';
        html += '</tr>';
    });

    $('#tbl_contacto_relator tbody').html(html);
}

function cargaCursosRelator(data) {

    if (data.length == 0) {
        alerta("Atencion", "Relator no tiene cursos asignados.", "error");
    } else {
        $("#modal_cursos_relator").modal("show")
        $("#modal_nombre_relator").html(data[0].nombre_completo);

        var html = "";
        $('#tbl_cursos_relator tbody').html(html);
        $.each(data, function (i, item) {
            html += '        <tr>';
            html += '        <td>' + item.id_curso + '</td> ';
            html += '        <td>' + item.nombre_curso + '</td> ';
            html += '        <td>' + item.duracion_curso + '</td> ';
            html += '        <td>' + item.capsulas + '</td> ';
            html += '        <td>' + item.num_ficha + '</td> ';
            html += '        <td>' + item.num_orden_compra + '</td> ';
            html += '        <td>' + item.fecha_inicio + '</td> ';
            html += '        <td>' + item.fecha_fin + '</td> ';
            html += '        <td>' + item.horas_ejercidas + '</td> ';
            html += '        <td>';

            html += '</td> ';
            html += '</tr>';
        });

        $('#tbl_cursos_relator tbody').html(html);
    }
}

function desactivarContactoRelator(id_contacto_relator, estatus) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto: id_contacto_relator,
            estado: estatus
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Relator", errorThrown, "error");
        },
        success: function (result) {

            ContactoRelator($("#txt_id_relator").val(), $("#modal_nombre_contacto").text());
        },
        url: "Listar/desactivarDatosContacto"
    });
}

function ubicarCamposUpdateContactoRelator(id_contacto_relator, correo_contacto, telefono_contacto, txt_id_relator) {
    $("#div_frm_add").css("display", "block");
    $("#div_tbl_contacto_relator").css("display", "none");
    $("#btn_nuevo_contacto").css("display", "none");
    $("#txt_id_contacto_relator").val(id_contacto_relator);
    $("#txt_id_relator").val(txt_id_relator);
    $("#txt_correo_contacto").val(correo_contacto);
    $("#txt_telefono").val(telefono_contacto);
    $("#btn_guardar_contacto").text("Actualizar");
}


function EliminarContactoRelator(id_contacto_relator, correo_contacto, telefono_contacto, id_relator) {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto_relator: id_contacto_relator,
            correo_contacto: correo_contacto,
            telefono_contacto: telefono_contacto,
            id_relator: id_relator
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Relator", errorThrown, "error");
        },
        success: function (result) {

            ContactoRelator(id_relator, $("#modal_nombre_contacto").text());
        },
        url: "Listar/eliminarDatosContacto"
    });

}

