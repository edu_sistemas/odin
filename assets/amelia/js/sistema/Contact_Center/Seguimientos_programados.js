$(document).ready(function () {

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
       $('#tbl_tareas_nuevo').DataTable()
                .columns.adjust()
                .responsive.recalc();
    });

    $("#empresa").select2({
        placeholder: "Seleccione Empresa",
        value: 0,
        allowClear: true
    });

    $("#ejecutivo").select2({
        placeholder: "Seleccione Ejecutivo",
        value: 0,
        allowClear: true
    });

    $("#inp_ficha").select2({
        placeholder: "Seleccione Ficha",
        value: 0,
        allowClear: true
    });

    $('#tbl_tareas').DataTable({

        paging: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [16, 17], // column or columns numbers
                orderable: false, // set orderable for selected columns
            },
            {
                targets: [7, 8], // Columnas de Fechas
                visible: false,
            },
            {
                targets: [2],
                width: "10%"
            },
            { responsivePriority: 2, targets: -1 } //Manito siempre Visible
        ],
        info: false,
        bFilter: true,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}},
            {extend: 'excel', title: 'Seguimientos Programados', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}},
            {extend: 'pdf', title: 'Seguimientos Programados', orientation: 'landscape', pageSize: 'LEGAL', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}},

            {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }, exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}
            }
        ]

    });

    $('#tbl_tareas_nuevo').DataTable({
        autoWidth: false,
        paging: true,
        columnDefs: [
            {
                targets: [15, 16], // column or columns numbers
                orderable: false, // set orderable for selected columns
            },
            {
                targets: [2,7,8,9,10],
                width: "10%"
            },
            {
                targets: [12,13],
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).css('background-color', '#f5f5f6');
                }
            },
            { responsivePriority: 2, targets: -1 } //Manito siempre Visible
        ],
        info: false,
        bFilter: true,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}},
            {extend: 'excel', title: 'Seguimientos Programados', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}},
            {extend: 'pdf', title: 'Seguimientos Programados', orientation: 'landscape', pageSize: 'LEGAL', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}},

            {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }, exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}
            }
        ]

    });

    $("#btnBuscar").on("click", function () {
        var empresa = $("#empresa").val(),
                ejecutivo = $("#ejecutivo").val(),
                ficha = $("#inp_ficha").val();
//        if ((empresa == "0") && (ejecutivo == "0") && (!ficha)) {
//            swal("Error", "Debe completar al menos un parametro de busqueda.", "warning");
//            return;
//        }
        var activoTab1 = $("#tab-1")[0].className.includes("active");
        var activoTab2 = $("#tab-2")[0].className.includes("active");
        if (activoTab1 || activoTab2) {
            swal({
                title: 'Espere un momento...',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            });
            setTimeout(function () {
                $.ajax({
                    url: (activoTab1 ? "../Comercial/Cargar_tareas_by_filtro" : "../Comercial/Cargar_tareas_nuevas_by_filtro"),
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: "post",
                    data: {
                        empresa: empresa,
                        ejecutivo: ejecutivo,
                        ficha: ficha
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown);
                        alert(textStatus + errorThrown + jqXHR.responseText);
                    },
                    success: function (result) {
                        var table = (activoTab1 ? $('#tbl_tareas').DataTable() : $('#tbl_tareas_nuevo').DataTable());
                        table.clear().draw();
                        $.each(result, function (i, v) {
                            var indicador = (activoTab1 ? v.indicador : "D1D1D1");

                            if (activoTab1) {//tareas
                                table.row.add([
                                    (i + 1),
                                    v.nombre_apellido_tutor,
                                    v.rut_dv,
                                    v.nombre_apellido,
                                    v.ficha,
                                    v.orden_compra,
                                    v.cp_modalidad,
                                    v.cp_costo,
                                    v.dj,
                                    (v.Tiempo_conexion > 0 ? Number.parseFloat(v.Tiempo_conexion).toFixed(2) : ""),
                                    "<center>" + v.seguimiento + "</td>",
                                    "<center><i class='fa fa-circle fa-lg' style='color: #" + indicador + ";'></i></center>",
                                    '<i class="fa fa-thumbs-up" title="' + ((v.id_estado_tarea == 2 ? "Contactado" : "NO contactado")) + '" style="font-size:20px;color:' + (v.id_estado_tarea == 2 ? "green" : "gray") + ';" aria-hidden="true"></i>'
                                ]).draw(false);
                            } else if (activoTab2) {//tareas nuevas
                                table.row.add([
                                    (i + 1),
                                    v.nombre_apellido_tutor,
                                    v.rut_dv,
                                    v.nombre_apellido,
                                    v.ficha,
                                    v.orden_compra,
                                    v.cp_modalidad,
                                    v.fecha_encuentro,
                                    v.cp_costo,
                                    v.dj,
                                    (v.Tiempo_conexion > 0 ? Number.parseFloat(v.Tiempo_conexion).toFixed(2) : ""),
                                    "<center>" + v.seguimiento + "</td>",
                                    "<center><i class='fa fa-circle fa-lg' style='color: #" + indicador + ";'></i></center>",
                                    '<i class="fa fa-thumbs-up" title="' + ((v.id_estado_tarea == 2 ? "Contactado" : "NO contactado")) + '" style="font-size:20px;color:' + (v.id_estado_tarea == 2 ? "green" : "gray") + ';" aria-hidden="true"></i>'
                                ]).draw(false);
                            }
                        });
                    },
                    complete: function (jqXHR, textStatus) {
                        swal.hideLoading();
                        swal.close();
                    }
                });
            }, 1000);
        } else {
            swal("Error", "Debe seleccionar 'Tareas' o 'Tareas Nuevo'", "error");
        }
    });





});

