$(document).ready(function () {

    ActivaBusqueda(0);
    
    $("#id_empresa").select2({
        placeholder: "Seleccione Empresa",
        value: 0,
        allowClear: true
    });

    $("#num_ficha").select2({
        placeholder: "Seleccione Ficha",
        value: 0,
        allowClear: true
    });

    $("#num_oc").select2({
        placeholder: "Seleccione Orden de Compra",
        value: 0,
        allowClear: true
    });

    $('#fecha_cierre_desde').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('#fecha_cierre_hasta').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('#tbl_resultados').DataTable({

        paging: true,
        ordering: false,
        searching: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            // {extend: 'copy', text: 'Copiar'},
            {extend: 'excel', title: 'Resultados'},
            {extend: 'pdf', title: 'Resultados', orientation: 'landscape', pageSize: 'LEGAL'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ],
        pageLength: 10

    });

    $('#tbl_djs').DataTable({

        paging: true,
        ordering: true,
        searching: true,
        info: false,
        bFilter: false,
        responsive: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [4, 5, 6],
                width: "10%"
            }
        ],
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            // {extend: 'copy', text: 'Copiar'},
            {extend: 'excel', title: 'Resultados'},
            {extend: 'pdf', title: 'Resultados', orientation: 'landscape', pageSize: 'LEGAL'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ],
        pageLength: 10

    });

    $('#tbl_con').DataTable({

        paging: true,
        ordering: true,
        searching: true,
        info: false,
        bFilter: false,
        responsive: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [4, 5, 6],
                width: "10%"
            }
        ],
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            // {extend: 'copy', text: 'Copiar'},
            {extend: 'excel', title: 'Resultados'},
            {extend: 'pdf', title: 'Resultados', orientation: 'landscape', pageSize: 'LEGAL'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ],
        pageLength: 10

    });

    $('#tbl_fin').DataTable({

        paging: true,
        ordering: true,
        searching: true,
        info: false,
        bFilter: false,
        responsive: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [4, 5, 6],
                width: "10%"
            }
        ],
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            // {extend: 'copy', text: 'Copiar'},
            {extend: 'excel', title: 'Resultados'},
            {extend: 'pdf', title: 'Resultados', orientation: 'landscape', pageSize: 'LEGAL'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ],
        pageLength: 10

    });

    $('#tbl_ext').DataTable({

        paging: true,
        ordering: true,
        searching: true,
        info: false,
        bFilter: false,
        responsive: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [4, 5],
                width: "10%"
            }
        ],
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            // {extend: 'copy', text: 'Copiar'},
            {extend: 'excel', title: 'Resultados'},
            {extend: 'pdf', title: 'Resultados', orientation: 'landscape', pageSize: 'LEGAL'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ],
        pageLength: 10

    });

    $("#btnBuscar").on("click", function () {

       if($(this).html()=="Nueva Busqueda")
       {
        $(this).html("Buscar");
        ActivaBusqueda(1);
        return 0;
    }


        if ($("#fecha_cierre_desde").val().toString().trim().length == 0
                || $("#fecha_cierre_hasta").val().toString().trim().length == 0) {
            swal("Aviso", "Debe especificar las fechas", "warning");
            return;
        }
        swal({
            title: 'Espere un momento...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            onOpen: () => {
                swal.showLoading()
            }
        });
        setTimeout(function () {
            $.ajax({
                url: "../Tutor/Cargar_resultados_tutor_filtro",
                async: false,
                cache: false,
                dataType: "json",
                type: "post",
                data: {
                    id_empresa: $("#id_empresa").val(),
                    num_ficha: $("#num_ficha").val(),
                    num_oc: $("#num_oc").val(),
                    fecha_cierre_desde: $("#fecha_cierre_desde").val(),
                    fecha_cierre_hasta: $("#fecha_cierre_hasta").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    var table = $('#tbl_resultados').DataTable();
                    table.clear().draw();
                    $.each(result, function (i, v) {
                        table.row.add([
                            v.Nombre_Tutor,
                            v.Cant_Dj,
                            v.Pcje_Dj,
                            v.Cant_Con,
                            v.Pcje_Con,
                            v.Cant_Fin,
                            v.Pcje_Fin,
                            v.Cant_LlamMen,
                            v.Pcje_LlamMen,
                            v.Cant_Ped,
                            v.Pcje_Ped,
                            v.Cant_Ex,
                            v.Pcje_Ex
                        ]).draw(false);
                    });

             ActivaBusqueda(0);
                },
                complete: function (jqXHR, textStatus) {
                    swal.hideLoading();
                    swal.close();
                }
            });
        }, 1000);
    });

});


function myModalDJ() {
    if ($("#fecha_cierre_desde").val().toString().trim().length == 0
            || $("#fecha_cierre_hasta").val().toString().trim().length == 0) {
        swal("Aviso", "Debe especificar las fechas", "warning");
        return;
    }
    swal({
        title: 'Espere un momento...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onOpen: () => {
            swal.showLoading()
        }
    });
    setTimeout(function () {
        $.ajax({
            url: "../Tutor/Cargar_resultados_tutor_declaracion_jurada",
            async: false,
            cache: false,
            dataType: "json",
            type: "post",
            data: {
                id_empresa: $("#id_empresa").val(),
                num_ficha: $("#num_ficha").val(),
                num_oc: $("#num_oc").val(),
                fecha_cierre_desde: $("#fecha_cierre_desde").val(),
                fecha_cierre_hasta: $("#fecha_cierre_hasta").val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                var table = $('#tbl_djs').DataTable();
                table.clear().draw();
                $.each(result, function (i, v) {
                    table.row.add([
                        v.Num_Ficha,
                        v.Num_Oc,
                        v.Nombre_Empresa,
                        v.Nombre_Curso,
                        v.Fecha_Ini,
                        v.Fecha_Fin,
                        v.Rut_Alumno,
                        v.Nombre_Alumno,
                        v.DJ
                    ]).draw(false);
                });
                $('#myModal1').modal('toggle');
            },
            complete: function (jqXHR, textStatus) {
                swal.hideLoading();
                swal.close();
            }
        });
    }, 1000);
}

function myModalConect() {
    if ($("#fecha_cierre_desde").val().toString().trim().length == 0
            || $("#fecha_cierre_hasta").val().toString().trim().length == 0) {
        swal("Aviso", "Debe especificar las fechas", "warning");
        return;
    }
    swal({
        title: 'Espere un momento...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onOpen: () => {
            swal.showLoading()
        }
    });
    setTimeout(function () {
        $.ajax({
            url: "../Tutor/Cargar_resultados_tutor_conectividad",
            async: false,
            cache: false,
            dataType: "json",
            type: "post",
            data: {
                id_empresa: $("#id_empresa").val(),
                num_ficha: $("#num_ficha").val(),
                num_oc: $("#num_oc").val(),
                fecha_cierre_desde: $("#fecha_cierre_desde").val(),
                fecha_cierre_hasta: $("#fecha_cierre_hasta").val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                var table = $('#tbl_con').DataTable();
                table.clear().draw();
                $.each(result, function (i, v) {
                    table.row.add([
                        v.Num_Ficha,
                        v.Num_Oc,
                        v.Nombre_Empresa,
                        v.Nombre_Curso,
                        v.Fecha_Ini,
                        v.Fecha_Fin,
                        v.Rut_Alumno,
                        v.Nombre_Alumno,
                        Number.parseFloat(v.Tiempo_Conexion).toFixed(2)
                    ]).draw(false);
                });
                $('#myModal2').modal('toggle');
            },
            complete: function (jqXHR, textStatus) {
                swal.hideLoading();
                swal.close();
            }
        });
    }, 1000);
}

function myModalFin() {
    if ($("#fecha_cierre_desde").val().toString().trim().length == 0
            || $("#fecha_cierre_hasta").val().toString().trim().length == 0) {
        swal("Aviso", "Debe especificar las fechas", "warning");
        return;
    }
    swal({
        title: 'Espere un momento...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onOpen: () => {
            swal.showLoading()
        }
    });
    setTimeout(function () {
        $.ajax({
            url: "../Tutor/Cargar_resultados_tutor_finalizados",
            async: false,
            cache: false,
            dataType: "json",
            type: "post",
            data: {
                id_empresa: $("#id_empresa").val(),
                num_ficha: $("#num_ficha").val(),
                num_oc: $("#num_oc").val(),
                fecha_cierre_desde: $("#fecha_cierre_desde").val(),
                fecha_cierre_hasta: $("#fecha_cierre_hasta").val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                var table = $('#tbl_fin').DataTable();
                table.clear().draw();
                $.each(result, function (i, v) {
                    table.row.add([
                        v.Num_Ficha,
                        v.Num_Oc,
                        v.Nombre_Empresa,
                        v.Nombre_Curso,
                        v.Fecha_Ini,
                        v.Fecha_Fin,
                        v.Rut_Alumno,
                        v.Nombre_Alumno,
                        v.Prcj_Eval
                    ]).draw(false);
                });
                $('#myModal3').modal('toggle');
            },
            complete: function (jqXHR, textStatus) {
                swal.hideLoading();
                swal.close();
            }
        });
    }, 1000);
}

function myModalExt() {
    if ($("#fecha_cierre_desde").val().toString().trim().length == 0
            || $("#fecha_cierre_hasta").val().toString().trim().length == 0) {
        swal("Aviso", "Debe especificar las fechas", "warning");
        return;
    }
    swal({
        title: 'Espere un momento...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onOpen: () => {
            swal.showLoading()
        }
    });
    setTimeout(function () {
        $.ajax({
            url: "../Tutor/Cargar_resultados_tutor_extensiones",
            async: false,
            cache: false,
            dataType: "json",
            type: "post",
            data: {
                id_empresa: $("#id_empresa").val(),
                num_ficha: $("#num_ficha").val(),
                num_oc: $("#num_oc").val(),
                fecha_cierre_desde: $("#fecha_cierre_desde").val(),
                fecha_cierre_hasta: $("#fecha_cierre_hasta").val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                var table = $('#tbl_ext').DataTable();
                table.clear().draw();
                $.each(result, function (i, v) {
                    table.row.add([
                        v.Num_Ficha,
                        v.Num_Oc,
                        v.Nombre_Empresa,
                        v.Nombre_Curso,
                        v.Fecha_Ini,
                        v.Fecha_Fin,
                        v.Fecha_Ext
                    ]).draw(false);
                });
                $('#myModal4').modal('toggle');

            },
            complete: function (jqXHR, textStatus) {
                swal.hideLoading();
                swal.close();
            }
        });
    }, 1000);
}

function ActivaBusqueda(accion)
{


    if(accion==0)
    {
        $("#id_empresa").prop('disabled',true);
        $("#num_ficha").prop('disabled',true);
        $("#num_oc").prop('disabled',true);
        $("#fecha_cierre_desde").prop('disabled',true);
        $("#fecha_cierre_hasta").prop('disabled',true);
        for(c=1;c<5;c++) { $("#Ojo"+c).show(); }
            $("#btnBuscar").html("Nueva Busqueda");
    }
    else
    {
     $("#id_empresa").prop('disabled',false);
     $("#num_ficha").prop('disabled',false);
     $("#num_oc").prop('disabled',false);
     $("#fecha_cierre_desde").prop('disabled',false);
     $("#fecha_cierre_hasta").prop('disabled',false);
     for(c=1;c<5;c++) { $("#Ojo"+c).hide(); }
         $("#btnBuscar").html("Buscar");
     var table = $('#tbl_resultados').DataTable();
     table.clear().draw();
 }

}


