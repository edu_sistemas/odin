var lista = [];
var dataList = function () {
    "use strict";
    return {
        //main function
        push: function (nombreParam, valor) {
            var obj = {
                NombreParam: nombreParam,
                Valor: valor
            };
            lista.push(obj);
        },
        del: function (nombreParam) {
            var paso = false;
            for (var i = 0; i < lista.length; i++) {
                if (lista[i].NombreParam == nombreParam) {
                    lista.splice(i, 1);
                    paso = true;
                    break;
                }
            }
            return paso;
        },
        exist: function (nombreParam) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i].NombreParam == nombreParam) {
                    return true;
                }
            }
            return false;
        },
        get: function (nombreParam) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i].NombreParam == nombreParam) {
                    return lista[i].Valor;
                }
            }
            return null;
        }
    };
}();

$(document).ready(function () {

    $('.DB').on('input', function () { 
        var a = this.value;
        var r=this.value.replace(/[^0-9]/g,'');
        this.value = r;
    });

    $('.DB').on('blur', function () { 
      if(this.value<1 || this.value>30)
      {
       swal("Día Bonificación","El número ingresado debe estar entre 1 y 30.","warning");
       this.value="";
   }
});

    $('#tblSegunOtic').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });
    $('#tblSegunGrado').DataTable({
        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: false,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });
    $('#tblPauta').DataTable({
        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });
    $('#tblObjetivo').DataTable({
        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: false,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });
    
    $("input[name=inputOtic]").on("keydown", function (e) {
        if ((e.keyCode == 69 || !(e.key >= '0' && e.key <= '9')) && e.keyCode != 8) {
            return false;
        }
    });
    
    $(".diaBon").on("keydown", function (e) {
        if ((e.keyCode == 69 || !(e.key >= '0' && e.key <= '9')) && e.keyCode != 8) {
            return false;
        }
    });
});
$('.btnEditarOtic').click(function () {
    var element = $(this)[0];
    var idOtic = element.id.split("_")[1];
    $("#inputOticDJ_" + idOtic).removeAttr("disabled");
    $("#inputOticConex_" + idOtic).removeAttr("disabled");
    $("#btnGuardarOtic_" + idOtic).css("display", "");
    $("#btnCancelarOtic_" + idOtic).css("display", "");
    $("#" + element.id).css("display", "none");
    var valorOticDj = $("#inputOticDJ_" + idOtic).val();
    var valorOticConex = $("#inputOticConex_" + idOtic).val();
    dataList.push("#inputOticDJ_" + idOtic, valorOticDj);
    dataList.push("#inputOticConex_" + idOtic, valorOticConex);
});
$(".btnGuardarOtic").click(function () {
    var element = $(this)[0];
    var idOtic = element.id.split("_")[1];
    confirmAlertOtic(idOtic);
});
$(".btnCancelarOtic").click(function () {
    var element = $(this)[0];
    var idOtic = element.id.split("_")[1];
    $("#inputOticDJ_" + idOtic).prop("disabled", "true");
    $("#inputOticConex_" + idOtic).prop("disabled", "true");
    $("#btnGuardarOtic_" + idOtic).css("display", "none");
    $("#btnEditarOtic_" + idOtic).css("display", "");
    $("#" + element.id).css("display", "none");
    if ($("#hdnEditadoOtic_" + idOtic).val() == 0) {
        var valorOticDj = dataList.get("#inputOticDJ_" + idOtic);
        var valorOticConex = dataList.get("#inputOticConex_" + idOtic);
        $("#inputOticDJ_" + idOtic).val(valorOticDj);
        $("#inputOticConex_" + idOtic).val(valorOticConex);
    }
    dataList.del("#inputOticDJ_" + idOtic);
    dataList.del("#inputOticConex_" + idOtic);
    $("#hdnEditado_" + idOtic).val(0);
});
$("#btnAgregarGradoBono").click(function () {
    var table = $('#tblSegunGrado').DataTable();
    var rowId = table.rows().count() + 1;
    table.row.add([
        '<input type="text" class="form-control" name="txt_grado_bono" id="txt_grado_bono_' + rowId + '"/><input type="hidden" name="txt_grado_bono_or" id="txt_grado_bono_or_' + rowId + '"/>',
        '<input type="number" class="form-control" name="txt_grado_bono_valor_no_beca" id="txt_grado_bono_valor_no_beca_' + rowId + '"/>',
        '<input type="number" class="form-control" name="txt_grado_bono_valor_beca" id="txt_grado_bono_valor_beca_' + rowId + '"/>',
        '<button class="btnEditarGrado btn btn-primary btn-xs" id="btnEditarGrado_' + rowId + '" style="display: none" onclick="editarGrado(' + rowId + ')">Editar</button>\n\
        <button class="btnNuevoGrado btn btn-primary btn-xs" id="btnNuevoGrado_' + rowId + '" onclick="nuevoGrado(' + rowId + ')">Guardar</button>\n\
        <button class="btnGuardarGrado btn btn-primary btn-xs" id="btnGuardarGrado_' + rowId + '" style="display: none" onclick="guardarGrado(' + rowId + ')">Guardar</button>\n\
        <button class="btnCancelarGrado btn btn-danger btn-xs" id="btnCancelarGrado_' + rowId + '" onclick="cancelarGrado(' + rowId + ', false)">Cancelar</button>\n\
        <input type="hidden" value="" id="hdnGradoId' + rowId + '"/>\n\
        <input type="hidden" id="hdnEditandoGrado_' + rowId + '" value="0" />'
        ]).draw(false);
    table.order([3, 'desc']).draw();
    table.page('last').draw('page');
    $("#btnAgregarGradoBono").hide();
});
$(".btnEditarCategoria").click(function () {
    var element = $(this)[0];
    var idCategoria = element.id.split("_")[1];
    $("#txtAreaPorque_" + idCategoria).removeAttr("disabled");
    $("#txtAreaScript_" + idCategoria).removeAttr("disabled");
    $("#txtAreaPauta_" + idCategoria).removeAttr("disabled");
    $("#btnEditarCategoria_" + idCategoria).hide();
    $("#btnGuardarCategoria_" + idCategoria).show();
    $("#btnCancelarCategoria_" + idCategoria).show();
    var txtAreaPorque = $("textarea#txtAreaPorque_" + idCategoria).val();
    var txtAreaScript = $("textarea#txtAreaScript_" + idCategoria).val();
    var txtAreaPauta = $("textarea#txtAreaPauta_" + idCategoria).val();
    dataList.push("#txtAreaPorque_" + idCategoria, txtAreaPorque);
    dataList.push("#txtAreaScript_" + idCategoria, txtAreaScript);
    dataList.push("#txtAreaPauta_" + idCategoria, txtAreaPauta);
});
$(".btnGuardarCategoria").click(function () {
    var element = $(this)[0];
    var idCategoria = element.id.split("_")[1];
    var porque = $("textarea#txtAreaPorque_" + idCategoria).val();
    var script = $("textarea#txtAreaScript_" + idCategoria).val();
    var pauta = $("textarea#txtAreaPauta_" + idCategoria).val();
    if (porque.trim().length > 0 && script.trim().length > 0 && pauta.trim().length > 0) {
        swal({
            title: "Editar",
            text: "Estas seguro que desaes editar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Editar",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                url: "../Supervisor/Editar_parametros_variables_categoria/" + idCategoria,
                async: false,
                cache: false,
                dataType: "json",
                type: 'post',
                data: {
                    porque: porque,
                    script: script,
                    pauta: pauta
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                    console.log(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    if (result.Cod == 1) {
                        $("#hdnEditadoCategoria_" + idCategoria).val(1);
                        $("#btnCancelarCategoria_" + idCategoria).click();
                        swal("Editado", result.Msg, "success");
                    } else {
                        swal("Error", result.Msg, "error");
                    }
                }
            });
        });
    } else {
        swal("Error", "Debe completar todos los campos", "error");
    }
});
$(".btnCancelarCategoria").click(function () {
    var element = $(this)[0];
    var idCategoria = element.id.split("_")[1];
    $("#txtAreaPorque_" + idCategoria).prop("disabled", "true");
    $("#txtAreaScript_" + idCategoria).prop("disabled", "true");
    $("#txtAreaPauta_" + idCategoria).prop("disabled", "true");
    $("#btnEditarCategoria_" + idCategoria).show();
    $("#btnGuardarCategoria_" + idCategoria).hide();
    $("#btnCancelarCategoria_" + idCategoria).hide();
    if ($("#hdnEditadoCategoria_" + idCategoria).val() == 0) {
        var txtAreaPorque = dataList.get("#txtAreaPorque_" + idCategoria);
        var txtAreaScript = dataList.get("#txtAreaScript_" + idCategoria);
        var txtAreaPauta = dataList.get("#txtAreaPauta_" + idCategoria);
        $("textarea#txtAreaPorque_" + idCategoria).val(txtAreaPorque);
        $("textarea#txtAreaScript_" + idCategoria).val(txtAreaScript);
        $("textarea#txtAreaPauta_" + idCategoria).val(txtAreaPauta);
    }
    dataList.del("#txtAreaPorque_" + idCategoria);
    dataList.del("#txtAreaScript_" + idCategoria);
    dataList.del("#txtAreaPauta_" + idCategoria);
    $("#hdnEditadoCategoria_" + idCategoria).val(0);
});
$(".btnEditarKpi").click(function () {
    var element = $(this)[0];
    var idKpi = element.id.split("_")[1];
    $("#txtObjetivo_" + idKpi).removeAttr("disabled");
    $("#btnEditarKpi_" + idKpi).hide();
    $("#btnGuardarKpi_" + idKpi).show();
    $("#btnCancelarKpi_" + idKpi).show();
    var txtObjetivo = $("#txtObjetivo_" + idKpi).val();
    dataList.push("#txtObjetivo_" + idKpi, txtObjetivo);
});
$(".btnGuardarKpi").click(function () {

    if ($(".diaBon").val().trim().length == 0 || !(parseInt($(".diaBon").val()) >= parseInt($(".diaBon")[0].min) && parseInt($(".diaBon").val()) <= parseInt($(".diaBon")[0].max))) {
        swal("Error", "El valor ingresado debe ser entre " + $(".diaBon")[0].min + " y "+$(".diaBon")[0].max, "warning");
        return;
    }
    var element = $(this)[0];
    var idKpi = element.id.split("_")[1];
    var objetivo = $("#txtObjetivo_" + idKpi).val();
    if (objetivo.trim().length > 0) {
        swal({
            title: "Editar",
            text: "Estas seguro que desaes editar este registro?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Editar",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                url: "../Supervisor/Editar_parametros_variables_kpi/" + idKpi,
                async: false,
                cache: false,
                dataType: "json",
                type: 'post',
                data: {
                    objetivo: objetivo
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                    console.log(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    if (result.Cod == 1) {
                        $("#hdnEditadoKpi_" + idKpi).val(1);
                        $("#btnCancelarKpi_" + idKpi).click();
                        swal("Editado", result.Msg, "success");
                    } else {
                        swal("Error", result.Msg, "error");
                    }
                }
            });
        });
    } else {
        swal("Error", "Debe completar todos los campos", "error");
    }
});
$(".btnCancelarKpi").click(function () {
    var element = $(this)[0];
    var idKpi = element.id.split("_")[1];
    $("#txtObjetivo_" + idKpi).prop("disabled", "true");
    $("#btnEditarKpi_" + idKpi).show();
    $("#btnGuardarKpi_" + idKpi).hide();
    $("#btnCancelarKpi_" + idKpi).hide();

    if ($("#hdnEditadoKpi_" + idKpi).val() == 0) {
        var txtObjetivo = dataList.get("#txtObjetivo_" + idKpi);
        $("#txtObjetivo_" + idKpi).val(txtObjetivo);
    }
    dataList.del("#txtObjetivo_" + idKpi);
    $("#hdnEditadoKpi_" + idKpi).val(0);

});
function editarGrado(rowId) {
    $("#txt_grado_bono_" + rowId).removeAttr("disabled");
    $("#txt_grado_bono_valor_no_beca_" + rowId).removeAttr("disabled");
    $("#txt_grado_bono_valor_beca_" + rowId).removeAttr("disabled");
    $("#btnGuardarGrado_" + rowId).show();
    $("#btnEditarGrado_" + rowId).hide();
    $("#btnCancelarGrado_" + rowId).show();
    var txt_grado_bono_valor_no_beca = $("#txt_grado_bono_valor_no_beca_" + rowId).val();
    var txt_grado_bono_valor_beca = $("#txt_grado_bono_valor_beca_" + rowId).val();
    dataList.push("#txt_grado_bono_valor_no_beca_" + rowId, txt_grado_bono_valor_no_beca);
    dataList.push("#txt_grado_bono_valor_beca_" + rowId, txt_grado_bono_valor_beca);
}

function nuevoGrado(rowId) {
    var grado = $("#txt_grado_bono_" + rowId).val();
    var noBeca = $("#txt_grado_bono_valor_no_beca_" + rowId).val();
    var beca = $("#txt_grado_bono_valor_beca_" + rowId).val();
    if (grado.trim().length > 0 && noBeca.trim().length > 0 && beca.trim().length > 0) {
        if (noBeca >= 0 && beca >= 0) {
            $.ajax({
                url: "../Supervisor/Agregar_nuevo_grado_bono",
                async: false,
                cache: false,
                dataType: "json",
                type: 'post',
                data: {
                    grado: grado,
                    noBeca: noBeca,
                    beca: beca
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                    console.log(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    if (result.Cod == 1) {
                        $("#hdnEditandoGrado_" + rowId).val(1);
                        swal("Creado", result.Msg, "success");
                        $("#hdnGradoId" + rowId).val(result.LastId);
                        $("#btnCancelarGrado_" + rowId).attr("onclick", "cancelarGrado(" + rowId + ",true)");
                        $("#txt_grado_bono_or_" + rowId).val(grado);
                        cancelarGrado(rowId, true);
                    } else {
                        swal("Error", result.Msg, "error");
                    }
                }
            });
        } else {
            swal("Error", "No se permiten valores negativos o iguales a 0", "error");
        }
    } else {
        swal("Error", "Debe completar todos los campos antes de guardar", "error");
    }
}

function guardarGrado(rowId) {
    swal({
        title: "Editar",
        text: "Estas seguro que desea editar este registro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Editar",
        closeOnConfirm: false
    }, function () {
        var grado = $("#txt_grado_bono_" + rowId).val();
        var noBeca = $("#txt_grado_bono_valor_no_beca_" + rowId).val();
        var beca = $("#txt_grado_bono_valor_beca_" + rowId).val();
        if (noBeca.trim().length > 0 && beca.trim().length > 0) {
            if (noBeca >= 0 && beca >= 0) {
                $.ajax({
                    url: "../Supervisor/Editar_grado_bono/" + $("#hdnGradoId" + rowId).val(),
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: 'post',
                    data: {
                        grado: grado,
                        noBeca: noBeca,
                        beca: beca
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown);
                        alert(textStatus + errorThrown + jqXHR.responseText);
                        console.log(textStatus + errorThrown + jqXHR.responseText);
                    },
                    success: function (result) {
                        if (result.Cod == 1) {
                            $("#hdnEditandoGrado_" + rowId).val(1);
                            $("#txt_grado_bono_or_" + rowId).val($("#txt_grado_bono_" + rowId).val());
                            swal("Editado", result.Msg, "success");
                            cancelarGrado(rowId, true);
                        } else {
                            swal("Error", result.Msg, "error");
                        }
                    }
                });
            } else {
                swal("Error", "No se permiten valores negativos o iguales a 0", "error");
            }
        } else {
            swal("Error", "Debe completar todos los campos antes de guardar", "error");
        }
    });
}

function cancelarGrado(rowId, isEdit) {
    $("#txt_grado_bono_" + rowId).val($("#txt_grado_bono_or_" + rowId).val()); // restaura valor original
    $("#txt_grado_bono_" + rowId).prop("disabled", "true");
    //$("#txt_grado_bono_" + rowId).hide();
    //$("#tblSegunGrado tr:eq(" + rowId + ") td:first").html($("#txt_grado_bono_" + rowId).val());
    $("#txt_grado_bono_valor_no_beca_" + rowId).prop("disabled", "true");
    $("#txt_grado_bono_valor_beca_" + rowId).prop("disabled", "true");
    $("#btnCancelarGrado_" + rowId).hide();
    if ($("#hdnEditandoGrado_" + rowId).val() == 0) {
        var txt_grado_bono_valor_no_beca = dataList.get("#txt_grado_bono_valor_no_beca_" + rowId);
        var txt_grado_bono_valor_beca = dataList.get("#txt_grado_bono_valor_beca_" + rowId);
        $("#txt_grado_bono_valor_no_beca_" + rowId).val(txt_grado_bono_valor_no_beca);
        $("#txt_grado_bono_valor_beca_" + rowId).val(txt_grado_bono_valor_beca);
    }
    dataList.del("#txt_grado_bono_valor_no_beca_" + rowId);
    dataList.del("#txt_grado_bono_valor_beca_" + rowId);
    $("#hdnEditandoGrado_" + rowId).val(0);
    if (isEdit) {
        $("#btnNuevoGrado_" + rowId).hide();
        $("#btnGuardarGrado_" + rowId).hide();
        $("#btnEditarGrado_" + rowId).show();
    } else {
        $("#btnNuevoGrado_" + rowId).show();
        $("#btnGuardarGrado_" + rowId).hide();
        var table = $('#tblSegunGrado').DataTable();
        table.row(':last').remove().draw();
    }
    $("#btnAgregarGradoBono").show();
}

function confirmAlertOtic(idOtic) {
    swal({
        title: "Editar",
        text: "Estas seguro que desaes editar este registro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Editar",
        closeOnConfirm: false
    }, function () {
        var dj = $("#inputOticDJ_" + idOtic).val();
        var conex = $("#inputOticConex_" + idOtic).val();
        console.log(dj, conex);
        if (parseInt(dj) >= 0 && parseInt(conex) >= 0) {
            $.ajax({
                url: "../Supervisor/Editar_parametros_variables_otic/" + idOtic,
                async: false,
                cache: false,
                dataType: "json",
                type: 'post',
                data: {
                    dj: dj,
                    conex: conex
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                    console.log(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    if (result.Cod == 1) {
                        $("#hdnEditadoOtic_" + idOtic).val(1);
                        $("#btnCancelarOtic_" + idOtic).click();
                        swal("Editado", result.Msg, "success");
                    } else {
                        swal("Error", result.Msg, "error");
                    }

                }
            });
        } else {
            swal("Error", "No se permiten valores negativos", "error");
        }
    });
}



