$(document).ready(function () {
    /*
    27 SUPER
    36 TELETUTOR
    13 EJECUTIVO COMERCIAL
    */

    $('#tblSeguimiento').DataTable({
       columnDefs: [
       {
            targets: [12], // column or columns numbers
            orderable: false, // set orderable for selected columns
        },
         { responsivePriority: 2, targets: [12,13] } //Seguimiento siempre Visible
         ],
         autoWidth: true,
         paging: false,
         searching: true,
         info: false,
         bFilter: false,
         responsive: true,
         language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });



    $('#btn-nocontactar').on('click', function(event) {

        var v1=$("#nocontactar").val();
        var v2=$("#rut_usuario").val();

        var mensaje=(v1==1?"Se habilitara al alumno para ser contactado.":"Se marcara al alumno para no ser contactado.");

        swal({
            title: mensaje,
            text: "¿Desea Continuar?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Si",
            cancelButtonText: "No"
        }, function(isConfirm) {

            if(isConfirm)
            {

                $.ajax({
                    url: '../DatosDetalleAlumno',
                    data: { parametro: 6, valor1: v1, valor2: v2, valor3: 0 },
                    type: 'POST',
                    dataType: "json",
                    success: function(resultado)
                    {       

                        $("#nocontactar").val(resultado[0].seguimiento);

                        if(resultado[0].seguimiento==1)
                        {
                            $("#titNoContactar").html("No contactar");
                            $("#btn-nocontactar").html('<i class="fa fa-bell-slash text-danger fa-3x"></i>');
                        }
                        else
                        {
                            $("#titNoContactar").html("Disponible para contacto");
                            $("#btn-nocontactar").html('<i class="fa fa-phone text-primary fa-3x"></i>');
                        }

                        swal("Detalle Alumno", "Cambio Realizado." , "success"); 
                    },  
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    }
                });
            }
        });

    });


    $('#btnECorreo').on('click', function(event) {
        $("#dEmail").val($("#txtMailOriginal").val());
        $("#mdlEmail").modal('toggle');
    });

    LoadTelefonos();


    $("#btn-vip").click(function(){

        var rut=$("#rut_usuario").val();
        var vip=($("#estado_vip").val()=="1"?"0":"1");

        swal({
            title: "Se cambiará el estado del Alumno.",
            text: "¿Desea Continuar?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Si",
            cancelButtonText: "No"
        }, function(isConfirm) {

            if(isConfirm)
            {
                $.ajax({
                    url: '../DatosDetalleAlumno',
                    data: { parametro: 1, valor1: rut, valor2: vip, valor3: 0 },
                    type: 'POST',
                    dataType: "json",
                    success: function(resultado)
                    {

                        if(resultado[0].vip==1)
                        {
                            $("#estado_vip").val('1');
                            $("#btn-vip").html('<i class="fa fa-star fa-5x" style="color: #e3e333;"></i>');
                        }
                        else
                        {
                          $("#estado_vip").val('0');
                          $("#btn-vip").html('<i class="fa fa-star-o fa-5x"></i>');
                      }
                  },  
                  error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });


            }
        });

    });


    $("#btn-add-fono").click(function(){
        $("#titulo-modal").html("Agregar Teléfono");
        $("#id_fono").val('0');
        $("#nTelefono").val('');
        $("#mdlTelefono").modal("toggle");

    });

    $("#btn-save-fono").click(function()
    {
        var id=$("#id_fono").val();

        var para=(id==0?2:3);

        if(id==0)
        {
          var v1=$("#id_usuario").val();
          var v2=$("#nTelefono").val();
          var v3=$("#rut_alumno").val();
          var telefono=v2;
      }
      else
      {
         var v1=$("#nTelefono").val();
         var v2=id;
         var v3=0;
         var telefono=v1;
     }


     if(telefono.length<8)
     {
        swal("Teléfono Alumno", "Debe especificar un número de teléfono", "warning");
    }
    else
    {
        $.ajax({
            url: '../DatosDetalleAlumno',
            data: { parametro: para, valor1: v1, valor2: v2, valor3: v3 },
            type: 'POST',
            dataType: "json",
            success: function(resultado)
            {
                LoadTelefonos();
                $("#mdlTelefono").modal("toggle");
            },  
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });
    }


});

    $("#btn-save-email").click(function()
    {
        var email=$("#dEmail").val();
        var rut_alumno = $("#rut_alumno").val();
        if(email.length<3)
        {
            swal("Email Alumno", "Debe especificar una dirección de correo electronico.", "warning");
        }
        else
        {
            if(ValidarCorreo("dEmail"))
            {
                 $.ajax({
                url: "../GuardarEmailAlumno",
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: { rut_alumno: rut_alumno, email: email },
                success: function(respuesta)
                {
                    if(respuesta.resultado=="OK")
                    {
                        $("#txtMail").html(email);
                        $("#txtMailOriginal").val(email);  
                        $("#mdlEmail").modal('toggle');
                        swal("Detalle Alumno", "Email Registrado." , "success"); 
                    }
                    else
                    {
                        swal("Detalle Alumno", respuesta.mensaje , "error");   
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });
            }
        }


    });

    $('#tblTelefonos').on("click", '.upd', function(){
        var id=$(this).attr('id');
        var tfono=$("#fono_"+id).val();

        $("#titulo-modal").html("Editar Teléfono");
        $("#id_fono").val(id);
        $("#nTelefono").val(tfono);
        $("#mdlTelefono").modal("toggle");
    });


    $('#tblTelefonos').on("click", '.del', function(){

     var id=$(this).attr('id');

     swal({
        title: "Se eliminara el Teléfono seleccionado.",
        text: "¿Desea Continuar?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: "Si",
        cancelButtonText: "No"
    }, function(isConfirm) {

        if(isConfirm)
        {
            $.ajax({
                url: '../DatosDetalleAlumno',
                data: { parametro: 4, valor1: id, valor2: 0, valor3: 0 },
                type: 'POST',
                dataType: "json",
                success: function(resultado)
                {                        
                    LoadTelefonos();
                },  
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                }
            });

        }
    });

 });

    $('#tblTelefonos').on("click", '.fav', function(){
        var v1=$(this).attr('id');
        var v2=($("#fav_"+v1).val()=="0"?1:0);
        $.ajax({
            url: '../DatosDetalleAlumno',
            data: { parametro: 5, valor1: v1, valor2: v2, valor3: 0 },
            type: 'POST',
            dataType: "json",
            success: function(resultado)
            {                        
                LoadTelefonos();
            },  
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });

    });

    $("#form-observacion").submit(function(e) {
        e.preventDefault(); 

        if($("#comentario").val().length==0)
        {
            swal("Detalle Alumno", "Debe ingresar Observación." , "warning");   
            return 0;
        }

        var datos=$(this).serialize();

        $.ajax({

            url: "../GuardarSeguimiento",
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: datos,
            success: function(respuesta)
            {
                if(respuesta.resultado=="OK")
                {
                    $("#txtObs").html($("#comentario").val());
                    $("#comentario").val("");  
                    swal("Detalle Alumno", "Observación Registrada." , "success"); 
                }
                else
                {
                    swal("Detalle Alumno", respuesta.mensaje , "error");   
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            }
        });


    });



    $('#comentario').keyup(function() {
        var tot=300;
        var longitud = $(this).val().length;
        var resto = tot - longitud;
        $('#numero').html(resto);
        if(resto <= 0){
            $('#comentario').attr("maxlength", tot);
        }
    });


});  //Fin OnLoad//


function LoadTelefonos()
{


    var v1=$("#id_usuario").val();
    var v2=$("#rut_alumno").val();

    $("#tblTelefonos tbody").empty();

    $.ajax({
        url: "../Gestion_alumnos_ajax",
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { parametro: 5, valor1: v1, valor2: v2, valor3: 0 },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        },
        success: function (result) 
        {
            $.each(result, function (ndx, item) 
            {
                var fav="";
                if(item.id>0)
                {
                    fav+="<a id='"+item.id+"' title='Favorito' class='fav'>";
                    fav+=(item.favorito==1?'<i class="fa fa-star" style="color: #e3e333"></i>':'<i class="fa fa-star-o"></i>');
                    fav+="<input type='hidden'id='fav_"+item.id+"' value='"+item.favorito+"'></a>";
                }
                else
                {
                    fav="<i class='fa fa-star' style='color: #7AB900' title='Teléfono Ofimatica'></i>";
                }

                var btn="";
                if(item.id>0)
                {
                    btn+="<a title='Editar Número' class='upd' id='"+item.id+"'><i class='fa fa-edit'></i></a> ";
                    btn+="<a title='Eliminar Número' class='del' id='"+item.id+"'><i class='fa fa-close'></i></a>";
                    btn+="<input type='hidden' id='fono_"+item.id+"' value='"+item.numero+"'>";
                }               

                $("#tblTelefonos tbody").append("<tr><td>"+fav+"</td><td>"+item.numero+"</td><td>"+btn+"</td></tr>");
            });
        }    
    });


}

function display(){
    var perfil = $("#perfil").val();

    if (perfil == 27 || 36) {       

        $("#dejarSeguimientoBtn").css('display','');

    }else{

    }

}


function fecha(){

    //Modal llamada
    $('#data1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,                
        format: "dd-mm-yyyy"
    }); 

} 


                    /*
                    function myModal2(){
                    	$('#datos_alumnos').modal('toggle');
                    };
                    function myModal3(){
                    	$('#datos_alumnos').modal('toggle');
                    };
                    function myModal4(){
                    	$('#datos_alumnos').modal('toggle');
                    };
                    */

                    function ValidarCorreo(inputEmail) {
                        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
                        var email=$("#"+inputEmail).val();
                        if(!pattern.test(email))
                        {
                         swal("Detalle Alumno", "Ingrese un email valido.", "warning");
                         //$("#"+inputEmail).val('');
                         return false;
                     }
                     else
                     {
                        return true;
                    }
                };
