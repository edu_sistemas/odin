$(document).ready(function () {

    ActivaBusqueda(0);

    $("#empresa").select2({
        placeholder: "Seleccione Empresa",
        value: 0,
        allowClear: true
    });
    $("#curso").select2({
        placeholder: "Seleccione Curso",
        value: 0,
        allowClear: true
    });
    $("#inp_ficha").select2({
        placeholder: "Seleccione Ficha",
        value: 0,
        allowClear: true
    });
    $("#inp_oc").select2({
        placeholder: "Seleccione Orden de Compra",
        value: 0,
        allowClear: true
    });
    $("#ejec_comercial").select2({
        placeholder: "Seleccione Ejecutivo",
        value: 0,
        allowClear: true
    });
    $('#data1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });
    

    $('#tbl_reporte_basico').DataTable({
        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        responsive: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [3, 4, 6],
                width: "10%"
            }
        ],
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'excel', title: 'Seguimientos Programados', },
            {extend: 'pdf', title: 'Seguimientos Programados', orientation: 'landscape', pageSize: 'LEGAL', },
            {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                },
            }
        ]

    });

    $('#tbl_avance_cursos').DataTable({
        columnDefs: [
            {
                width: "70",
                targets: [1, 3, 4]
            }
        ],
        autoWidth: false,
        paging: true,
        ordering: true,
        info: true,
        bFilter: true,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar'},
            {extend: 'excel', title: 'Seguimientos Programados', },
            {extend: 'pdf', title: 'Seguimientos Programados', orientation: 'landscape', pageSize: 'LEGAL', },
            {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                },
            }
        ]

    });
    $("#btnReporteBasico").on("click", function () {

        swal({
            title: 'Espere un momento...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            onOpen: () => {
                swal.showLoading()
            }
        });
        setTimeout(function () {
            $.ajax({
                url: "../Comercial/Cargar_reporte_basico_avance_curso",
                async: false,
                cache: false,
                dataType: "json",
                type: "post",
                data: {
                    id_empresa: $("#empresa").val(),
                    id_curso: $("#curso").val(),
                    fecha_desde: $("#fecha_desde").val(),
                    fecha_hasta: $("#fecha_hasta").val(),
                    num_ficha: $("#inp_ficha").val(),
                    num_oc: $("#inp_oc").val(),
                    holding: $("#holding").val(),
                    ano_proceso: $("#proceso").val(),
                    id_ejec_comercial: $("#ejec_comercial").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                    swal.hideLoading();
                    swal.close();
                },
                success: function (result) {
                    if (result.length > 0) {
                        var table = $('#tbl_reporte_basico').DataTable();
                        table.clear().draw();
                        $.each(result, function (i, v) {
                            table.row.add([
                                v.razon_social_empresa,
                                v.ficha,
                                v.nombre_curso,
                                v.fecha_inicio,
                                v.cp_fechaCierre,
                                v.nombre_apellido,
                                v.rut,
                                v.costo,
                                v.porcentaje_avance_video,
                                Number.parseFloat(v.tiempo_conexion).toFixed(2)
                            ]).draw(false);
                            if ((result.length - 1) == i) {
                                swal.hideLoading();
                                swal.close();
                                $('#mdlReporteBasico').modal('toggle');
                            }
                        });
                    } else {
                        swal.hideLoading();
                        swal.close();
                        swal("Error", 'No se encontraron resultados para generar el reporte básico', "warning");
                    }
                }
            });
        }, 1000);
    });
    $("#btnBuscar").on("click", function () {

     if($(this).html()=="Nueva Busqueda")
     {
        $(this).html("Buscar");
        ActivaBusqueda(1);
        return 0;
    }

        if ($("#proceso").val().trim().length > 0 && parseInt($("#proceso").val()) < parseInt($("#proceso")[0].min)) {
            swal("Error", "El valor ingresado en 'Proceso', es menor al mínimo (" + $("#proceso")[0].min + ")", "warning");
            return;
        }

        
        
        var id_empresa = $("#empresa").val(),
                id_curso = $("#curso").val(),
                fecha_desde = $("#fecha_desde").val(),
                fecha_hasta = $("#fecha_hasta").val(),
                num_ficha = $("#inp_ficha").val(),
                num_oc = $("#inp_oc").val(),
                holding = $("#holding").val(),
                ano_proceso = $("#proceso").val(),
                id_ejec_comercial = $("#ejec_comercial").val();
        if ((!id_empresa)
                && (!id_curso)
                && (!fecha_desde)
                && (!fecha_hasta)
                && (!num_ficha)
                && (!num_oc)
                && (!holding)
                && (!ano_proceso)
                && (!id_ejec_comercial)) {
            swal("Error", "Debe ingresar al menos un parámetro para la busqueda", "warning");
            return;
        }

        if(!fecha_desde && !fecha_hasta) { swal("Error", "Debe especificar Fechas para la Búsqueda.", "warning"); return; }
        if(!fecha_desde && fecha_hasta) { swal("Error", "Debe especificar fecha Desde.", "warning"); return; }
        if(fecha_desde && !fecha_hasta) { swal("Error", "Debe especificar fecha Hasta.", "warning"); return; }

        
        swal({
            title: 'Espere un momento...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            onOpen: () => {
                swal.showLoading()
            }
        });
        setTimeout(function () {
            $.ajax({
                url: "../Comercial/Cargar_avance_curso_filtro",
                async: false,
                cache: false,
                dataType: "json",
                type: "post",
                data: {
                    id_empresa: id_empresa,
                    id_curso: id_curso,
                    fecha_desde: fecha_desde,
                    fecha_hasta: fecha_hasta,
                    num_ficha: num_ficha,
                    num_oc: num_oc,
                    holding: holding,
                    ano_proceso: ano_proceso,
                    id_ejec_comercial: id_ejec_comercial
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    var table = $('#tbl_avance_cursos').DataTable();
                    table.clear().draw();
                    if (result.length > 0) {

                        $("#btnReporteBasico").show();
                        $("#btnReporteCompleto").show();
                        $.each(result, function (i, v) {
                            table.row.add([
                                v.nombre_apellido,
                                v.rut,
                                v.nombre_curso,
                                v.fecha_inicio,
                                v.cp_fechaCierre,
                                v.costo,
                                v.evaluaciones_realizadas,
                                Number.parseFloat(v.tiempo_conexion).toFixed(2),
                            ]).draw(false);

                    ActivaBusqueda(0);

                            if ((result.length - 1) == i) {
                                swal.hideLoading();
                                swal.close();
                            }
                        });
                    } else {
                        swal.hideLoading();
                        swal.close();
                        $("#btnReporteBasico").hide();
                        $("#btnReporteCompleto").hide();
                        swal("Error", 'No se encontraron resultados según los parámetros', "warning");
                    }
                }
            });
        }, 1000);
    });
    $("#proceso").on("keydown", function (e) {
        if ((e.keyCode == 69 || !(e.key >= '0' && e.key <= '9')) && e.keyCode != 8) {
            return false;
        }
    });
    $("#btnReporteCompleto").on("click", function () {
        $("input[name='chkCampo']").attr('checked', false);
        $("#chkSeleccionarTodo").attr('checked', false);
        $("#mdlReporteCompleto").modal("toggle");
    });
    $("#chkSeleccionarTodo").on("click", function () {
        var checked = $(this)[0].checked;
        var chkCampos = $("input[name='chkCampo']");
        for (var i = 0; i < chkCampos.length; i++) {
            chkCampos[i].checked = checked;
        }
    });
    $("#btnGenerarReporteGeneral").on("click", function (e) {
        e.preventDefault();
        if ($("input[name='chkCampo']:checked").length == 0)
        {
            swal({
                title: "Avance Cursos",
                text: "Debe seleccionar al menos un campo para generar el reporte.",
                type: "warning"});
            return 0;
        }


        $('#mdlReporteCompleto').modal("toggle");
        swal({
            title: 'Este proceso puede tardar <u>varios minutos</u>, espere un momento...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            onOpen: () => {
                swal.showLoading()
            }
        });
        setTimeout(function () {
            $.ajax({
                url: "../Comercial/Reporte_general_avance_curso",
                async: false,
                cache: false,
                dataType: "json",
                type: "post",
                data: {
                    id_empresa: $("#empresa").val(),
                    id_curso: $("#curso").val(),
                    fecha_desde: $("#fecha_desde").val(),
                    fecha_hasta: $("#fecha_hasta").val(),
                    num_ficha: $("#inp_ficha").val(),
                    num_oc: $("#inp_oc").val(),
                    holding: $("#holding").val(),
                    ano_proceso: $("#proceso").val(),
                    id_ejec_comercial: $("#ejec_comercial").val(),
                    chkCts: $("#chkCts")[0].checked,
                    chkCorreo: $("#chkCorreo")[0].checked,
                    chkCosto: $("#chkCosto")[0].checked,
                    chkCurso: $("#chkCurso")[0].checked,
                    chkCodSence: $("#chkCodSence")[0].checked,
                    chkDJ: $("#chkDJ")[0].checked,
                    chkEmpresa: $("#chkEmpresa")[0].checked,
                    chkEvaluacion: $("#chkEvaluacion")[0].checked,
                    chkFicha: $("#chkFicha")[0].checked,
                    chkFechaInicio: $("#chkFechaInicio")[0].checked,
                    chkFechaFin: $("#chkFechaFin")[0].checked,
                    chkFechaCierre: $("#chkFechaCierre")[0].checked,
                    chkFUS: $("#chkFUS")[0].checked,
                    chkFono: $("#chkFono")[0].checked,
                    chkHolding: $("#chkHolding")[0].checked,
                    chkHrsCurso: $("#chkHrsCurso")[0].checked,
                    chkIdSence: $("#chkIdSence")[0].checked,
                    chkIndicador: $("#chkIndicador")[0].checked,
                    chkNombre: $("#chkNombre")[0].checked,
                    chkOC: $("#chkOC")[0].checked,
                    chkPrimerAcceso: $("#chkPrimerAcceso")[0].checked,
                    chkRut: $("#chkRut")[0].checked,
                    chkRegistroSence: $("#chkRegistroSence")[0].checked,
                    chkSucursal: $("#chkSucursal")[0].checked,
                    chkSituacion: $("#chkSituacion")[0].checked,
                    chkSeguimientos: $("#chkSeguimientos")[0].checked,
                    chkTiempoConexion: $("#chkTiempoConexion")[0].checked,
                    chkTutor: $("#chkTutor")[0].checked,
                    chkUltimoAcceso: $("#chkUltimoAcceso")[0].checked,
                    chkVersion: $("#chkVersion")[0].checked,
                    chkPrctjeAvanceVideo: $("#chkPrctjeAvanceVideo")[0].checked,
                    chkPrctjeEvalRealizada: $("#chkPrctjeEvalRealizada")[0].checked,
                    chkPrctjeFranquicia: $("#chkPrctjeFranquicia")[0].checked,
                    chkPrctjeRegistroSence: $("#chkPrctjeRegistroSence")[0].checked,
                    chkPrcjeTiempoConexion: $("#chkPrcjeTiempoConexion")[0].checked,
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    console.log(result);
                    swal.hideLoading();
                    swal.close();
                    if (result.Cod == 1) {
                        $.fileDownload(result.Link)
                                .done(function () {
                                    alert('File download a success!');
                                })
                                .fail(function () {
                                    alert('File download failed!');
                                });
                        //window.open("../Comercial/" + result.Link);
                    } else {
                        swal("Error", result.Msg, "warning");
                    }
                }
            });
        }, 1000);
    });
});
function modalareporte() {
    $('#mdlReporteCompleto').modal('toggle');
}



function ActivaBusqueda(accion)
{


    if(accion==0)
    {
        $("#empresa").prop('disabled',true);
        $("#curso").prop('disabled',true);
        $("#fecha_desde").prop('disabled',true);
        $("#fecha_hasta").prop('disabled',true);
        $("#inp_ficha").prop('disabled',true);
        $("#inp_oc").prop('disabled',true);
        $("#holding").prop('disabled',true);
        $("#proceso").prop('disabled',true);
        $("#ejec_comercial").prop('disabled',true);
        $("#btnReporteBasico").prop('disabled',false);
        $("#btnReporteCompleto").prop('disabled',false);

        $("#btnBuscar").html("Nueva Busqueda");
    }
    else
    {
        $("#empresa").prop('disabled',false);
        $("#curso").prop('disabled',false);
        $("#fecha_desde").prop('disabled',false);
        $("#fecha_hasta").prop('disabled',false);
        $("#inp_ficha").prop('disabled',false);
        $("#inp_oc").prop('disabled',false);
        $("#holding").prop('disabled',false);
        $("#proceso").prop('disabled',false);
        $("#ejec_comercial").prop('disabled',false);
        $("#btnReporteBasico").prop('disabled',true);
        $("#btnReporteCompleto").prop('disabled',true);
       $("#btnBuscar").html("Buscar");
       var table = $('#tbl_avance_cursos').DataTable();
       table.clear().draw();
   }

}