$(document).ready(function () {

$('.footable').footable();


var color1 = '#7a4d84';
var color2 = '#ecba52';
var colors = Highcharts.getOptions().colors,
    categories = [
        "Enviada",
        "No Enviada"
    ],
    data = [
        {
            "y": 40,
            "drilldown": {
                "name": "Enviada",
                "categories": [
                    "E-Learning",
                    "Distancia"
                ],
                "data": [
                    25,
                    15
                ]
            }
        },
        {
            "y": 60,
            "drilldown": {
                "name": "No enviada",
                "categories": [
                    "E-Learning",
                    "Distancia"
                ],
                "data": [
                    40,
                    20
                ]
            }
        }
        
        
        
    ],
    browserData = [],
    versionsData = [],
    i,
    j,
    dataLen = data.length,
    drillDataLen,
    brightness;


// Build the data arrays
for (i = 0; i < dataLen; i += 1) {

    // add browser data
    browserData.push({
        name: categories[i],
        y: data[i].y,
        color: data[i].color
    });

    // add version data
    drillDataLen = data[i].drilldown.data.length;
    for (j = 0; j < drillDataLen; j += 1) {
        brightness = 0.2 - (j / drillDataLen) / 5;
        versionsData.push({
            name: data[i].drilldown.categories[j],
            y: data[i].drilldown.data[j],
            color: Highcharts.Color(data[i].color).brighten(brightness).get()
        });
    }
}

// Create the chart
Highcharts.chart('chartDeclaraciones', {
   
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    plotOptions: {
        pie: {
            shadow: false,
            center: ['50%', '50%']
        }
    },
    tooltip: {
        valueSuffix: '%'
    },
    series: [{
        name: 'DATO',
        data: browserData,
        size: '50%',
        color: '#ffffff',
        dataLabels: {
            formatter: function () {
                return this.y > 5 ? this.point.name : null;
            },
            
            distance: -30
        }
    }, {
        name: 'DATO',
        data: versionsData,
        size: '50%',
        innerSize: '70%',
        dataLabels: {
            formatter: function () {
                // display only if larger than 1
                return this.y > 1 ? '<b>' + this.point.name + ':</b> ' +
                    this.y + '%' : null;
            }
        },
        id: 'versions'
    }],
    responsive: {
        rules: [{
            condition: {
                maxWidth: 400
            },
            chartOptions: {
                series: [{
                    id: 'versions',
                    dataLabels: {
                        enabled: false
                    }
                }]
            }
        }]
    }
});


//--------------------------------//-------------------------------------//


var colors = Highcharts.getOptions().colors,
    categories = [
        "Cumple", //NO SE ABRE,
        "No Cumple"
    ],
    data = [
        {
            "y": 70,            
            "drilldown": {
                "name": "Cumple",//NO SE ABRE,
                "categories": [
                    "DATO 1",
                    "DATO 2"
                ],
                "data": [
                    50,
                    20
                ],
                "color": '#7a4d84',
            }
        },
        {
            "y": 30,
            "color": '#ecba52',
            "drilldown": {
                "name": "No Cumple",
                "categories": [
                    "> 0",
                    "Limite"
                ],
                "data": [
                    20,
                    10
                ]
            }
        }
    ],
    browserData = [],
    versionsData = [],
    i,
    j,
    dataLen = data.length,
    drillDataLen,
    brightness;


// Build the data arrays
for (i = 0; i < dataLen; i += 1) {

    // add browser data
    browserData.push({
        name: categories[i],
        y: data[i].y,
        color: data[i].color
    });

    // add version data
    drillDataLen = data[i].drilldown.data.length;
    for (j = 0; j < drillDataLen; j += 1) {
        brightness = 0.2 - (j / drillDataLen) / 5;
        versionsData.push({
            name: data[i].drilldown.categories[j],
            y: data[i].drilldown.data[j],
            color: Highcharts.Color(data[i].color).brighten(brightness).get()
        });
    }
}

// Create the chart
Highcharts.chart('chartConectadas', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    yAxis: {
        title: {
            text: ''
        }
    },
    plotOptions: {
        pie: {
            shadow: false,
            center: ['50%', '50%']
        }
    },
    tooltip: {
        valueSuffix: '%'
    },
    series: [{
        name: 'DATO',
        data: browserData,
        size: '50%',
        dataLabels: {
            formatter: function () {
                return this.y > 5 ? this.point.name : null;
            },
            color: '#ffffff',
            distance: -30
        }
    }, {
        name: 'DATO',
        data: versionsData,
        size: '50%',
        innerSize: '70%',
        dataLabels: {
            formatter: function () {
                // display only if larger than 1
                return this.y > 1 ? '<b>' + this.point.name + ':</b> ' +
                    this.y + '%' : null;
            }
        },
        id: 'versions'
    }],
    responsive: {
        rules: [{
            condition: {
                maxWidth: 400
            },
            chartOptions: {
                series: [{
                    id: 'versions',
                    dataLabels: {
                        enabled: false
                    }
                }]
            }
        }]
    }
});


//--------------------------------//-------------------------------------//
Highcharts.chart('chartEvaluaciones', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },

    tooltip: {
        headerFormat: '',
        pointFormat: '<b> {point.name}:</b><br/>' +
            '<b>{point.z}</b><br/>'
    },
    series: [{
    	size: '50%',
        minPointSize: 10,
        innerSize: '30%',
        zMin: 0,
        name: 'chartEvaluaciones',
        data: [{
        	color: '#7a4d84',
            name: 'Listos 70% / 150',
            y: 70,
            z: 70
        }, {
        	color: '#7a4d84',
            name: 'Pendientes 30% / 80',
            y: 30,
            z: 30
        }]
    }]
});


//--------------------------------//-------------------------------------//
Highcharts.chart('chartSeguimiento', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<b> {point.name}:</b><br/>' +
            		 '<b>{point.z}</b><br/>'
    },
    series: [{
    	color: '#7a4d84',
    	size: '50%',
        minPointSize: 10,
        innerSize: '30%',
        zMin: 0,
        name: 'chartSeguimiento',
        data: [{
        	
            name: 'Tiene 40% / 507',
            y: 40,
            z: 40
        }, {
            name: 'No tiene 60% / 481',
            y: 60,
            z: 60
        }]
    }]
});

//--------------------------------//-------------------------------------//
Highcharts.chart('chartLlamados', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<b> {point.name}:</b><br/>' +
            		 '<b>{point.z}</b><br/>'
    },
    series: [{
    	size: '50%',
        minPointSize: 10,
        innerSize: '30%',
        zMin: 0,
        name: 'chartLlamados',
        data: [{
        	color: '#7a4d84',
            name: 'Cumple 80% / 54',
            y: 80,
            z: 80
        }, {
            name: 'No cumple 20% / 10',
            y: 20,
            z: 20
        }]
    }]
});

//--------------------------------//-------------------------------------//
Highcharts.chart('chartRespuestas', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<b> {point.name}:</b><br/>' +
            		 '<b>{point.z}</b><br/>'
    },
    series: [{
    	size: '50%',
        minPointSize: 10,
        innerSize: '30%',
        zMin: 0,
        name: 'chartRespuestas',
        data: [{
        	color: '#7a4d84',
            name: 'Cumple 90% / 194',
            y: 90,
            z: 90
        }, {
            name: 'No cumple 10% / 25',
            y: 10,
            z: 10
        }]
    }]
});

//--------------------------------//-------------------------------------//

Highcharts.chart('chartLlamadaMayor', {
    chart: {
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<b> {point.name}:</b><br/>' +
            		 '<b>{point.z}</b><br/>'
    },
    series: [{
    	size: '50%',
        minPointSize: 10,
        innerSize: '30%',
        zMin: 0,
        name: 'chartLlamadaMayor',        
        data: [{        	
            name: 'Mayores a 5 segundos 75% / 50',
            y: 75,
            z: 75,
            color: "rgba(26,179,148,0.5)"
        }, {
            name: 'Mayores a 5 segundos 25% / 4',
            y: 25,
            z: 25,
            color: "#E0DBBA"
        }]
    }]
});

//"#966b10", :      AMARILLO CLARO
//"#a3693a", :      AMARILLO OSCURO
//"#38283c", :      MORADO OSCURO
//"#531a61", :      MORADO OSCURO-2
//"#ecba52", :      AMARILLO CLARO
//"#7a4d84", :      MORADO CLARO

})