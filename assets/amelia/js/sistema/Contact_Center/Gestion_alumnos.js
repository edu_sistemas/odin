$(document).ready(function(){

	$('#tblEvaluaciones').footable();

	
	$('.checkbox').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});

	if($("#nocontactar").val()=="1")
	{
		$('#nocontactar').iCheck('check');

	}
	else
	{
		$('#nocontactar').iCheck('uncheck');
	}

	if($("#Tareas").val()=="1")
	{
		$(".bt").show();
		//$("#botones-tareas").show();
	}
	else
	{
		$(".bt").hide();
		//$("#botones-tareas").hide();
	}

	ajaxCargaCategorias(1,0,0,0);
	//ajaxCargaCategorias(2);
	//ajaxCargaCategorias(3);

	displayBtnLlamar();

	CargaDatosCurso();
	CargaSeguimientos();



	$("#cboCursos").change(function(){
		
		var curso=$(this).val();
		var alumno=$("#id_alumno").val();
		var user=$("#id_user").val();

		var orden_tarea=0;

		$("#tblEvaluaciones tbody").empty();

		for(c=3;c<=4;c++)
		{
			if(c==3)
			{
				v1=curso;
				v2=user;
				v3=alumno;
			}
			else
			{
				v1=alumno;
				v2=curso;
				v3=0;
			}


			//if(c==4) { v2=v1; v1=$("#id_alumno").val(); }

			$.ajax({
				url: "../Gestion_alumnos_ajax",
				async: false,
				cache: false,
				dataType: "json",
				type: 'POST',
				data: { parametro: c, valor1: v1, valor2: v2, valor3: v3 },
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
				},
				success: function (result) 
				{
					if(c==3)
					{
						var item = result[0];
						$("#dcCurso").html(item.curso);
						$("#dcLink").html("<a target='_blank' href='"+item.linkmoodle+"'>"+item.curso+"</a>");
						$("#dcModalidad").html(item.modalidad);
						$("#dcFicha").html(item.ficha);
						$("#dcEjecutivo").html(item.ejecutivo);
						$("#dcCosto").html(item.costo);
						$("#dcHorasConexion").html(item.horas_conexion+" Horas.");
						$("#dcDJ").html(item.dj);
						$("#dcAvanceVideo").html(item.avance_video+"%");
						$("#dcPrimerIngreso").html(item.primer_ingreso);
						$("#dcUltimoIngreso").html(item.ultimo_ingreso);
						$("#dcEncuesta").html(item.encuesta);
						$("#dcEvaluacionesRealizadas").html(item.evaluaciones_realizadas);
						$("textarea#dcPorque").html(item.porque);
						$("textarea#dcScript").html(item.script);
						$("textarea#dcPauta").html(item.pauta_seguimiento);
						$("#dcFus").html(item.fus);
						orden_tarea=item.orden_tarea;
						BotonesAvance(orden_tarea,item.nuevo);

						$("#id_tarea").val(item.id_tarea);
						$("#tarea_nuevo").val(item.nuevo);

						if(item.nuevo==1)
						{
							$("#tit_nc").show();
							$("#categoria1").val('1');
							ajaxCargaCategorias(2,1,0);
							setTimeout(function(){ 
								$("#categoria2").val('1'); 
								ajaxCargaCategorias(3,1,1);
							}, 1500);
						}
						else
						{
							$("#tit_nc").hide();
							$("#categoria1").val('');
							$("#categoria2").empty();
							$("#categoria3").empty();
						}
					}
					else
					{
						$.each(result, function (ndx, item) 
						{
							if(item.cp_modulo!=null)
							{
								$("#tblEvaluaciones tbody").append("<tr><td>"+item.cp_modulo+"</td><td>"+item.cp_nota+"</td></tr>");
							}
						});
						$('#tblEvaluaciones').trigger('footable_initialize');
					}

				}    
			});

		}

		

	});


	$("#categoria1").change(function(){
		var valor=$(this).val();
		ajaxCargaCategorias(2,valor,0);
		$("#categoria3").empty();
	});

	$("#categoria2").change(function(){
		var valor=$("#categoria1").val();
		var valor2=$(this).val();
		ajaxCargaCategorias(3,valor,valor2);
	});


	$("#categoria1Edit").change(function(){
		var valor=$(this).val();
		ajaxCargaCategorias(2,valor,0);
		$("#categoria3Edit").empty();
	});

	$("#categoria2Edit").change(function(){
		var valor=$("#categoria1Edit").val();
		var valor2=$(this).val();
		ajaxCargaCategorias(3,valor,valor2);
	});

	$("#form-llamado").submit(function(e) {
		e.preventDefault(); 

		if($("#categoria1").val()=="")
		{
			swal("Seguimiento", "Seleccione categoría 1" , "warning");	
			return 0;
		}
		else
		{
			if($("#categoria2").val()=="")
			{
				swal("Seguimiento", "Seleccione categoría 2" , "warning");	
				return 0;
			}
			else
			{
				if($("#categoria3").val()=="")
				{
					swal("Seguimiento", "Seleccione categoría 3" , "warning");	
					return 0;
				}
			}
		}

		/*
		if($("#comentario").val().length<10)
		{
			swal("Seguimiento", "Debe ingresar Comentario." , "warning");	
			return 0;
		}
		*/

		var datos=$(this).serialize();

		//console.log(datos);

		//return 0;

		$.ajax({

			url: "../GuardarSeguimiento",
			async: false,
			cache: false,
			dataType: "json",
			type: 'POST',
			data: datos,
			success: function(respuesta)
			{
				if(respuesta.resultado=="OK")
				{
					ocultarModal();
					swal("Seguimiento", respuesta.mensaje , "success");	
					CargaSeguimientos();
				}
				else
				{
					swal("Seguimiento", respuesta.mensaje , "error");	
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
			}
		});


	});


	$("#form-llamado-edit").submit(function(e) {
		e.preventDefault(); 

		if($("#categoria1Edit").val()=="")
		{
			swal("Seguimiento", "Seleccione categoría 1" , "warning");	
			return 0;
		}
		else
		{
			if($("#categoria2Edit").val()=="")
			{
				swal("Seguimiento", "Seleccione categoría 2" , "warning");	
				return 0;
			}
			else
			{
				if($("#categoria3Edit").val()=="")
				{
					swal("Seguimiento", "Seleccione categoría 3" , "warning");	
					return 0;
				}
			}
		}

		var datos=$(this).serialize();
		console.log(datos);

		$.ajax({

			url: "../GuardarSeguimiento",
			async: false,
			cache: false,
			dataType: "json",
			type: 'POST',
			data: datos,
			success: function(respuesta)
			{

				if(respuesta.resultado=="OK")
				{
					$("#mdlEditar").modal("toggle");
					swal("Seguimiento", respuesta.mensaje , "success");	
					CargaSeguimientos();
				}
				else
				{
					swal("Seguimiento", respuesta.mensaje , "error");	
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
			}
		});


	});


	$('#comentario').keyup(function() {
		var tot=300;
		var longitud = $(this).val().length;
		var resto = tot - longitud;
		$('#numero').html(resto);
		if(resto <= 0){
			$('#comentario').attr("maxlength", tot);
		}
	});


	$("#tblSeguimiento").on('click','.bUpd',function(){
		var id=$(this).attr('id');

		$.ajax({
			url: "../Gestion_alumnos_ajax",
			async: false,
			cache: false,
			dataType: "json",
			type: 'POST',
			data: { parametro: 9, valor1: id, valor2: 0, valor3: 0 },
			success: function (result) 
			{
				console.log(result);

				$("#categoria2Edit").empty();
				$("#categoria3Edit").empty();

				$("#categoria1Edit").val(result[0].origen);
				ajaxCargaCategorias(2,result[0].origen,0,0);
				$("#categoria2Edit").val(result[0].motivo);
				ajaxCargaCategorias(3,result[0].origen,result[0].motivo,0);
				$("#categoria3Edit").val(result[0].respuesta);

				$("#id_seguimientoEdit").val(id);


				$("#comentarioEdit").val(result[0].comentario);
				$("#mdlEditar").modal("toggle")
			},  
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
			}
		});
	});

	$("#tblSeguimiento").on('click','.bDel',function(){

		var id=$(this).attr('id');
		swal({
			title: 'Se eliminara el seguimiento seleccionado.',
			text: "¿Desea Continuar?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: "Si",
			cancelButtonText: "No"
		}, function(isConfirm) {

			if(isConfirm)
			{
				$.ajax({
					url: '../GuardarSeguimiento',
					data: { id_seguimiento: id , accion: 3 },
					type: 'POST',
					dataType: "json",
					success: function(resultado)
					{                        
						CargaSeguimientos();
						swal("Seguimiento Alumno", "Registro Eliminado." , "success"); 
					},  
					error: function(jqXHR, textStatus, errorThrown) {
						console.log(jqXHR);
					}
				});

			}

		});
	});


});

function displayBtnLlamar(){

	var perfil = $("#perfil").val();

	if (perfil == 13) {
		$("#tblTelefonos2").css('display','');
	}else{
		$("#tblTelefonos").css('display','');
	}
}

function GenerarLlamado(fono){
	$("#divLlamar").fadeIn(2000);

	$("#fsel").html(fono);
	$("#telefono").val(fono);
	var extension=$("#extension").val();
	fono = fono.replace("+","").replace("(","").replace(")","").replace(" ","").replace("+","");

	var url="https://"+$("#ip_elastix").val()+"/wse/call.php?exten="+extension+"&number="+fono;

	var w=1;
	var h=1;

	var left = (screen.width - w) / 2;
	var top = (screen.height - h) / 4;

	var delay=2000

	if (fono % 1 == 0) 
	{
		var popLlamado = window.open('../Llamado/'+$("#ip_elastix").val()+'_'+extension+'_'+fono,'Llamado Contact Center', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no,width='+w+',height='+h+',top='+top+',left='+left);
		popLlamado.opener = window;
		popLlamado.focus();
		setTimeout(function(){ popLlamado.close();  }, 2000);
	}
	else
	{
		swal("Validación", "El formato del numero: \n "+fono+",\n no es valido." , "warning");	
	}
	




	/*
	var url="http://"+$("#ip_elastix").val()+"/wse/call.php";
	
	$.ajax({
		async: true,
		url: url,
		type: 'GET',
		data: {exten : extension, number: fono },
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(jqXHR);
		},
		success: function (result) {

		},
		
	}); 
	*/


}

$('.ancla').click(function(e){				
	e.preventDefault();		//evitar el eventos del enlace normal
	var strAncla=$(this).attr('href'); //id del ancla
	$('body,html').stop(true,true).animate({				
		scrollTop: $(strAncla).offset().top
	},500);		
});



function ocultarModal(){
	setTimeout(function() {
		$("#divLlamar").fadeOut(500);
	},500);

	$("#categoria1").val("");
	$("#categoria2").empty();
	$("#categoria3").empty();
	$("#comentario").val("");

}


function ajaxCargaCategorias(categoria,valor,valor2,valor3){

   //var perfil = $("#perfil").val();
   $.ajax({
   	async: false,
   	cache: false,
   	dataType: "json",
   	type: 'POST',
   	    //  data: $('#frm').serialize(),
   	    data: {categoria : categoria, valor: valor, valor2: valor2, valor3: valor3 },
   	    error: function (jqXHR, textStatus, errorThrown) {
   	        // code en caso de error de la ejecucion del ajax
   	        console.log(jqXHR);
   	    },
   	    success: function (result) {

   	    	switch (categoria){
   	    		case 1: 
   	    		cargaCombocategoria(result);
   	    		cargaCombocategoriaEdit(result);
   	    		break;
   	    		case 2:
   	    		cargaCombocategoria2(result);
   	    		cargaCombocategoria2Edit(result);
   	    		break;
   	    		case 3:
   	    		cargaCombocategoria3(result);
   	    		cargaCombocategoria3Edit(result);
   	    		break;
   	    	}
   	    },
   	    url: "../../Comun/cargaCombocategoriaCbx"
   	});    
}


/*---------------CARGA-COMBOBOX-CATEGORIA-1---------------*/
function cargaCombocategoria(result) {
	cargaCBX("categoria1",result);
	$('#categoria1').find('option:first').text('Seleccione Categoría');
}
/*-------------FIN-CARGA-COMBOBOX-CATEGORIA-1--------------*/


/*---------------CARGA-COMBOBOX-CATEGORIA-2---------------*/
function cargaCombocategoria2(result) {
	cargaCBX("categoria2",result);
	$('#categoria2').find('option:first').text('Seleccione Categoría 2');
}
/*-------------FIN-CARGA-COMBOBOX-CATEGORIA-2--------------*/


/*---------------CARGA-COMBOBOX-CATEGORIA-3---------------*/
function cargaCombocategoria3(result) {
	cargaCBX("categoria3",result);
	$('#categoria3').find('option:first').text('Seleccione Categoría 3');
}
/*-------------FIN-CARGA-COMBOBOX-CATEGORIA-3--------------*/



/*---------------CARGA-COMBOBOX-CATEGORIA-1-EDITAR---------------*/
function cargaCombocategoriaEdit(result) {
	cargaCBX("categoria1Edit",result);
}
/*-------------FIN-CARGA-COMBOBOX-CATEGORIA-1-EDITAR--------------*/


/*---------------CARGA-COMBOBOX-CATEGORIA-2-EDITAR---------------*/
function cargaCombocategoria2Edit(result) {
	cargaCBX("categoria2Edit",result);
	$('#categoria2').find('option:first').text('Seleccione Categoría 2');
}
/*-------------FIN-CARGA-COMBOBOX-CATEGORIA-2-EDITAR--------------*/


/*---------------CARGA-COMBOBOX-CATEGORIA-3-EDITAR---------------*/
function cargaCombocategoria3Edit(result) {
	cargaCBX("categoria3Edit",result);
	$('#categoria3').find('option:first').text('Seleccione Categoría 3');
}
/*-------------FIN-CARGA-COMBOBOX-CATEGORIA-3-EDITAR--------------*/

function CargaDatosCurso()
{
	
	$("#cboCursos").empty();
	$("#tblEvaluaciones tbody").empty();

	var alumno=$("#id_alumno").val();
	var curso=$("#id_curso").val();

	var user=$("#id_user").val();

	var v1=alumno;
	var v2=0;
	var v3=0;

	var orden_tarea=0;

	for(c=2;c<=4;c++)
	{
		
		if(c==3)
		{
			v1=curso;
			v2=user;
			v3=alumno;
		}
		else
		{
			v1=alumno;
			v2=curso;
			v3=0;
		}

		$.ajax({
			url: "../Gestion_alumnos_ajax",
			async: false,
			cache: false,
			dataType: "json",
			type: 'POST',
			data: { parametro: c, valor1: v1, valor2: v2, valor3: v3 },
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
			},
			success: function (result) 
			{
				if(c==2)
				{
					$.each(result, function (ndx, item) 
					{
						$("#cboCursos").append("<option value='"+item.id_curso+"'>"+item.nombre_curso+"</option>");
					});
					
					$("#cboCursos").val(curso);
					v1=v2=parseInt($("#cboCursos").val());
				}
				else if(c==3)
				{
					var item = result[0];

					console.log(result[0]);
					$("#dcCurso").html(item.curso);
					$("#dcLink").html("<a target='_blank' href='"+item.linkmoodle+"'>"+item.curso+"</a>");
					$("#dcModalidad").html(item.modalidad);
					$("#dcFicha").html(item.ficha);
					$("#dcEjecutivo").html(item.ejecutivo);
					$("#dcCosto").html(item.costo);
					$("#dcHorasConexion").html(item.horas_conexion+" Horas.");
					$("#dcDJ").html(item.dj);
					$("#dcAvanceVideo").html(item.avance_video+"%");
					$("#dcPrimerIngreso").html(item.primer_ingreso);
					$("#dcUltimoIngreso").html(item.ultimo_ingreso);
					$("#dcEncuesta").html(item.encuesta);
					$("#dcEvaluacionesRealizadas").html(item.evaluaciones_realizadas);
					$("textarea#dcPorque").val(item.porque);
					$("textarea#dcScript").val(item.script);
					$("textarea#dcPauta").val(item.pauta_seguimiento);
					$("#dcFus").html(item.fus);
					orden_tarea=item.orden_tarea;
					v1=parseInt($("#id_alumno").val());
					BotonesAvance(orden_tarea,item.nuevo);
					$("#id_tarea").val(item.id_tarea);
					$("#tarea_nuevo").val(item.nuevo);

					if(item.nuevo==1)
					{
						$("#tit_nc").show();
						$("#categoria1").val('1');
						ajaxCargaCategorias(2,1,0);
						setTimeout(function(){ 
							$("#categoria2").val('1'); 
							ajaxCargaCategorias(3,1,1);
						}, 1500);
					}
					else
					{
						$("#tit_nc").hide();
						$("#categoria1").val('');
						$("#categoria2").empty();
						$("#categoria3").empty();
					}

				}
				else
				{
					$.each(result, function (ndx, item) 
					{
						if(item.cp_modulo!=null)
						{
							$("#tblEvaluaciones tbody").append("<tr><td>"+item.cp_modulo+"</td><td>"+item.cp_nota+"</td></tr>");
						}
					});
					$('#tblEvaluaciones').trigger('footable_initialize');
				}

			}    
		});

	}

}

function CargaSeguimientos()
{
	$("#tblSeguimiento tbody").empty();

	var rut_alumno=$("#rut_alumno").val();

	$.ajax({
		url: "../Gestion_alumnos_ajax",
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: { parametro: 7, valor1: rut_alumno, valor2: 0, valor3: 0 },
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(jqXHR);
		},
		success: function (result) 
		{
			$.each(result, function (ndx, item) 
			{
				//console.log(item);
				var fila="<tr>";
				fila+="<td>"+item.origen+"</td>";
				fila+="<td>"+item.motivo+"</td>";
				fila+="<td>"+item.respuesta+"</td>";
				fila+="<td>"+item.comentario+"</td>";
				fila+="<td>"+item.telefono+"</td>";
				fila+="<td>"+item.fecha+"</td>";
				fila+="<td>"+item.tutor+"</td>";
				/* when the user requests to remove comments
				if($("#perfil").val()!="13")
				{
					fila+="<td align='center'>";
					fila+="<button id='"+item.id+"' class='bUpd btn btn-sm btn-success'><i class='fa fa-edit'></i></button> ";
					fila+=" <button id='"+item.id+"' class='bDel btn btn-sm btn-danger'><i class='fa fa-remove'></i></button>";
					fila+="</td>";
				}
				fila+="</tr>";
				*/				
				$("#tblSeguimiento tbody").append(fila);
			});
		}    
	});
}

function BotonesAvance(orden_tarea,nuevo)
{

	if($("#Tareas").val()=="1")
	{
		if(orden_tarea==0)
		{
			$(".bt").hide();
		}
		else
		{	
			
			var user=($("#perfil").val()=="27")?"0":$("#id_user").val();

			$.ajax({
				url: "../Gestion_alumnos_ajax",
				async: true,
				cache: false,
				dataType: "json",
				type: 'POST',
				data: { parametro: 8, valor1: orden_tarea, valor2: user, valor3: nuevo },
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
				},
				success: function (result) 
				{	
					console.log(result.length);
					if(result.length>0) 
					{
						$(".bt").show();
					}
					else
					{
						$(".bt").hide();
					}

					$.each(result, function (ndx, item) 
					{

						if(item.tipo=="Ant")
						{
							$("#btn-ant").attr('href',item.alumno+'_'+item.curso);
						}
						else
						{
							$("#btn-sig").attr('href',item.alumno+'_'+item.curso);
						}
					});
				}    
			});
		}
	}
}
