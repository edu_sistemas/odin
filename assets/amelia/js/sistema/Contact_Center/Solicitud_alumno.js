$(document).ready(function () {

	$('.clockpicker').clockpicker({
		format: "HH:MM:ss"
	});

	$('#data1 .input-group.date').datepicker({
		todayBtn: "linked",
		keyboardNavigation: false,
		forceParse: false,
		calendarWeeks: false,
		autoclose: true,
		format: "yyyy-mm-dd"
	});

});

function validarForm(){
	if (validarRequeridoForm($("#formSolicitud"))) {
	//alert('asdf');
	generarSolicitud();
}
}


function generarSolicitud(){

	//alert($('#inputAlumno').val());
	//alert('asdf');
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#formSolicitud').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(jqXHR);
			swal({
				title: "Solicitud",
				text: "Solicitud no ingresada",
				type: "warning",
				showCancelButton: false,
				confirmButtonColor: "#a5dc86",
				confirmButtonText: "Ok",
				closeOnConfirm: false
			});
		},
		success: function (result) {
			alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
			if (result[0].reset == 1) {
				$('#formSolicitud').trigger("reset");
			}
		},
		url: "../../Comun/generarSolicitudCon"
	});  

	//console.log($('#textComentario').val());
}
/*
function exito(){
	$('#formSolicitud').trigger("reset");

	swal({
		title: "Solicitud",
		text: "Solicitud ingresada con éxito.",
		type: "success",
		showCancelButton: false,
		confirmButtonColor: "#a5dc86",
		confirmButtonText: "Ok",
		closeOnConfirm: false
	});
}*/

