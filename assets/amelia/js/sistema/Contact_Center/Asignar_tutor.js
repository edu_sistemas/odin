var lista = [];
var alumnos = function () {
    "use strict";
    return {
        //main function
        push: function (id, idOC) {
            var data = {
                rut: id,
                oc: idOC
            };
            lista.push(data);
        },
        del: function (id) {
            var paso = false;
            for (var i = 0; i < lista.length; i++) {
                if (lista[i].rut == id) {
                    lista.splice(i, 1);
                    paso = true;
                    break;
                }
            }
            return paso;
        },
        exist: function (id) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i].rut == id) {
                    return true;
                }
            }
            return false;
        },
        clear: function () {
            lista = [];
        }
    };
}();
$(document).ready(function () {


    $("#filt_modalidad").select2({
        placeholder: "Seleccione Modalidad",
        value: 0,
        allowClear: true
    });
    $("#filt_tcosto").select2({
        placeholder: "Seleccione Tipo Costo",
        value: 0,
        allowClear: true
    });

    $("#filt_fichas").select2({
        placeholder: "Seleccione Ficha",
        value: 0,
        allowClear: true
    });

    $("#filt_oc").select2({
        placeholder: "Seleccione Orden de Compra",
        value: 0,
        allowClear: true
    });

    $('#data1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('#tbl_asignaciones').DataTable({

        paging: true,
        ordering: true,
        info: true,
        bFilter: true,
        responsive: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [7, 8],
                width: "10%"
            }
        ],
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Detalle por Ficha'},
            {extend: 'pdf', title: 'Detalle por Ficha'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]


    });

    $('#tbl_asignaciones tbody').on('mouseover', 'tr', function () {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'hover',
            html: true
        });
    });


    $('#tbl_asignados_tutor').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        searching: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Alumnos Asignados por Tutor'},
            {extend: 'pdf', title: 'Alumnos Asignados por Tutor'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]

    });

    $('#tbl_asignados_empresa').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        searching: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Alumnos Asignados por Empresa'},
            {extend: 'pdf', title: 'Alumnos Asignados por Empresa'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]

    });

    $('#tbl_alumnos_ficha').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        searching: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    $("#tbl_tutores").DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    $("#tbl_tutores_todos").DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    $("#btnTutores").on("click", function () {
        if (lista.length == 0) {
            $('#myModal3').modal('toggle');
            swal(
                    {
                        title: "Error",
                        text: "Debe seleccionar un alumnno",
                        type: "warning"
                    }
            ).then((result) => {
                if (result) {
                    $('#myModal3').modal('toggle');
                }
            });
            return;
        }
        $('#myModal3').modal('toggle');
        $('#myModal1').modal('toggle');
        $.ajax({
            url: "../Supervisor/Carga_tutores",
            async: false,
            cache: false,
            dataType: "json",
            type: 'get',
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                var table = $('#tbl_tutores').DataTable();
                table.clear().draw();
                $.each(result, function (i, v) {
                    table.row.add([
                        v.nombre_usuario,
                        v.q_alumnos,
                        "<button type='button' class='btn btn-primary btn-xs' onclick='asignarTutor(" + v.id_usuario + ")'>Asignar</button>"
                    ]).draw();
                });
            }
        });
    }
    );

    $("#sltClasifFicha").on("change", function () {
        $.ajax({
            url: "../Supervisor/Set_clasif_ficha/" + $("#hdnIdFichaClas").val(),
            async: false,
            cache: false,
            dataType: "text",
            type: 'post',
            data: {
                clas: $(this).val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                if (result == 1) {

                    swal({
                        title: 'Correcto',
                        text: "Registro modificado correctamente",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: "OK"
                    }).then((result) => {
                        if (result) {
                            document.location.reload(true);
                        }
                    });

                } else {
                    swal("Error", "Ocurrió un error al intentar modificar el registro", "error");
                }
            }
        });
    });

    $("#btnBuscar").on("click", function () {
        swal({
            title: 'Espere un momento...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            onOpen: () => {
                swal.showLoading()
            }
        });
        setTimeout(function () {
            $.ajax({
                url: "../Supervisor/Filtrar_fichas",
                async: false,
                cache: false,
                dataType: "json",
                type: 'post',
                data: {
                    id_modalidad: $("#filt_modalidad").val(),
                    fecha_fin: $("#fecha_cierre").val(),
                    id_cc: $("#filt_tcosto").val(),
                    ficha: $("#filt_fichas").val(),
                    oc: $("#filt_oc").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                    console.log(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    var table = $('#tbl_asignaciones').DataTable();
                    table.clear().draw();
                    $.each(result.detalleFicha, function (i, v) {
                        var diferencia = (parseInt(v.cantidad_alumnos) - parseInt(v.cantidad_alumnos_asignados));

                        var colorMano = (diferencia == 0 ? "primary" : "danger");
                        var ttt = (diferencia == 0 ? "No hay alumnos por Asignar." : diferencia + " Alumnos por Asignar")
                        table.row.add([
                            "<a><i class='fa fa-search' onclick='myModalficha(" + v.id_ficha + "," + v.num_ficha + ");'></i></a>",
                            v.num_ficha,
                            v.num_orden_compra,
                            v.empresa,
                            v.nombre_ejecutivo,
                            v.modalidad,
                            v.nombre_curso,
                            v.fecha_inicio,
                            v.fecha_fin,
                            Number.parseFloat(v.costo_sence).toFixed(2) + "%",
                            v.cantidad_alumnos,
                            "<a><i class='fa fa-edit' onclick='myModalClaFe(" + v.clasif_ficha + "," + v.id_ficha + ");'></i></a>   " + (v.clasif_ficha == -1 ? "N/A" : v.clasif_ficha),
                            "<button id='bm_" + v.id_ficha + "' type='button' class='btn btn-" + colorMano + " btn-xs' onclick='showMdlTodos(" + v.id_ficha + ", "+v.num_ficha+")'> <span data-toggle='tooltip' data-placement='top' title='" + ttt + "'><i class='fa fa-hand-o-left' aria-hidden='true'></i></span></button>"
                        ]).draw(false);
                    });
                },
                complete: function (jqXHR, textStatus) {
                    swal.hideLoading();
                    swal.close();
                }
            });
        }, 1000);
    });

    $("#btnListarTodos").on("click", function () {
        cargarDataInicial(true, true);
    });

    cargarDataInicial();

});

function cargarDataInicial(detalleFicha = false, limpiaFiltro = false) {
    swal({
        title: 'Espere un momento...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onOpen: () => {
            swal.showLoading()
        }
    });
    setTimeout(function () {
        $.ajax({
            url: "../Supervisor/Carga_inicial_data_supervisor/"+detalleFicha+"/"+limpiaFiltro,
            async: false,
            cache: false,
            dataType: "json",
            type: 'get',
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown, jqXHR.responseText);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                console.log(result);
                if (detalleFicha) {
                    var table = $('#tbl_asignaciones').DataTable();
                    table.clear().draw();
                    $.each(result.detalleFicha, function (i, v) {

                        var diferencia = (parseInt(v.cantidad_alumnos) - parseInt(v.cantidad_alumnos_asignados));

                        var colorMano = (diferencia == 0 ? "primary" : "danger");
                        var ttt = (diferencia == 0 ? "No hay alumnos por Asignar." : diferencia + " Alumnos por Asignar")

                        table.row.add([
                            "<a><i class='fa fa-search' onclick='myModalficha(" + v.id_ficha + "," + v.num_ficha + ");'></i></a>",
                            v.num_ficha,
                            v.num_orden_compra,
                            v.empresa,
                            v.nombre_ejecutivo,
                            v.modalidad,
                            v.nombre_curso,
                            v.fecha_inicio,
                            v.fecha_fin,
                            Number.parseFloat(v.costo_sence).toFixed(2) + "%",
                            v.cantidad_alumnos,
                            "<a><i class='fa fa-edit' onclick='myModalClaFe(" + v.clasif_ficha + "," + v.id_ficha + ");'></i></a>   " + (v.clasif_ficha == -1 ? "N/A" : v.clasif_ficha),
                            "<button id='bm_" + v.id_ficha + "' type='button' class='btn btn-" + colorMano + " btn-xs' onclick='showMdlTodos(" + v.id_ficha + ", "+v.num_ficha+")'> <span data-toggle='tooltip' data-placement='top' title='" + ttt + "'><i class='fa fa-hand-o-left' aria-hidden='true'></i></span></button>"
                        ]).node().id = v.id_ficha;
                        table.draw(false);
                    });
                }
                var tblAsignTutor = $("#tbl_asignados_tutor").DataTable();
                tblAsignTutor.clear().draw();
                $.each(result.alumnosTutor, function (i, v) {
                    var sence = ((v.alumnos_sence * 100) / v.cantidad_alumnos);
                    var empresa = ((v.alumnos_empresa * 100) / v.cantidad_alumnos);
                    var beca = ((v.alumnos_beca * 100) / v.cantidad_alumnos);
                    tblAsignTutor.row.add([
                        v.nombre_usuario_tutor,
                        v.alumnos_sence + ' / ' + sence.toFixed(2) + '%',
                        v.alumnos_empresa + ' / ' + empresa.toFixed(2) + '%',
                        v.alumnos_beca + ' / ' + beca.toFixed(2) + '%',
                        v.cantidad_alumnos,
                        v.total_fichas_clasificadas
                    ]).draw(false);
                });

                var tblAsignEmpresa = $("#tbl_asignados_empresa").DataTable();
                tblAsignEmpresa.clear().draw();
                // console.log(result.alumnosEmpresa);
                $.each(result.alumnosEmpresa, function (i, v) {
                    var total = (parseInt(v.alumnos_sence) + parseInt(v.alumnos_empresa) + parseInt(v.alumnos_becado));
                    if (total > 0) {
                        var sence = ((v.alumnos_sence * 100) / v.cantidad_alumnos);
                        var empresa = ((v.alumnos_empresa * 100) / v.cantidad_alumnos);
                        var beca = ((v.alumnos_becado * 100) / v.cantidad_alumnos);
                        tblAsignEmpresa.row.add([
                            v.razon_social_empresa,
                            v.alumnos_sence + ' / ' + sence.toFixed(2) + '%',
                            v.alumnos_empresa + ' / ' + empresa.toFixed(2) + '%',
                            v.alumnos_becado + ' / ' + beca.toFixed(2) + '%',
                            total
                        ]).draw(false);
                    }
                });
            }
            , complete: function (jqXHR, textStatus) {
                swal.hideLoading();
                swal.close();
            }
        })
    }, 1000);
}

function showMdlTodos(idFicha, num_ficha) {
    $.ajax({
        url: "../Supervisor/Carga_alumnos_by_ficha/" + idFicha,
        async: false,
        cache: false,
        dataType: "json",
        type: 'get',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
            alert(textStatus + errorThrown + jqXHR.responseText);
            console.log(textStatus + errorThrown + jqXHR.responseText);
        },
        success: function (result) {
            console.log(result);
            if (result.length > 0) {
                $('#myModal4').modal('toggle');
                $.ajax({
                    url: "../Supervisor/Carga_tutores",
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: 'get',
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown);
                        alert(textStatus + errorThrown + jqXHR.responseText);
                        console.log(textStatus + errorThrown + jqXHR.responseText);
                    },
                    success: function (result2) {
                        var table = $('#tbl_tutores_todos').DataTable();
                        table.clear().draw();
                        $.each(result2, function (i, v) {
                            table.row.add([
                                v.nombre_usuario,
                                v.q_alumnos,
                                "<button type='button' class='btn btn-primary btn-xs' onclick='asignarTutorTodos(" + v.id_usuario + ", " + idFicha + ")'>Asignar</button>"
                            ]).draw();
                        });
                    }
                });
            } else {
                //swal("Error", "Los alumnos de esta ficha, ya fueron asignados a un tutor. Dirijase a 'Reasignar Tutor' para volver a asignar los alumnos a otro tutor.", "warning");

                swal({
                    title: 'Error',
                    text: "Los alumnos de esta ficha ya fueron asignados a un tutor. Diríjase a 'Reasignar Tutor' para volver a asignar los alumnos a otro tutor.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: "Reasignar",
                    cancelButtonText: "OK"
                }).then((result) => {
                    if (result) {
                        location.href = "Reasignar_alumnos/"+num_ficha;
                    }
                });
            }
        }
    });

}

/**
 * asignara todos los alumnos de una ficha a un tutor
 */
function asignarTutorTodos(idTutor, idFicha) {
    $('#myModal4').modal('toggle');
    swal({
        title: 'Espere un momento...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onOpen: () => {
            swal.showLoading()
        }
    });
    setTimeout(function () {
        $.ajax({
            url: "../Supervisor/Asignar_alumnos_tutor_by_ficha",
            async: false,
            cache: false,
            dataType: "json",
            type: 'post',
            data: {
                id_ficha: idFicha,
                id_tutor: idTutor
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
                swal.hideLoading();
                swal.close();
            },
            success: function (result) {
                console.log(result);
                swal.hideLoading();
                swal.close();
            }
            , complete: function (jqXHR, textStatus) {
                swal({
                    title: 'Asignación OK',
                    text: "Alumnos asignados correctamente.",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: "OK"
                }).then((result) => {
                    if (result) {
                        //document.location.reload(true);
                        setTimeout(function () {
                            cargarDataInicial(true,false);
                        }, 1000);
                    }
                });
            }
        });
    }, 1000);
}

/**
 * asigna los alumnos de lista al tutor determinado
 */
function asignarTutor(idTutor) {
    $('#myModal1').modal('toggle');
    swal({
        title: 'Espere un momento...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onOpen: () => {
            swal.showLoading()
        }
    });
    setTimeout(function () {
        $.ajax({
            url: "../Supervisor/Asignar_tutor",
            async: false,
            cache: false,
            dataType: "json",
            type: 'post',
            data: {
                id_tutor: idTutor,
                data_alumnos: lista,
                id_ficha: $("#hdnIdFicha").val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
                swal.hideLoading();
                swal.close();
            },
            success: function (result) {
                alumnos.clear();
                console.log(result);
                //$('#myModal1').modal('toggle');
                swal.hideLoading();
                swal.close();

            }
            , complete: function (jqXHR, textStatus) {
                swal({
                    title: 'Asignación OK',
                    text: "Alumnos asignados correctamente.",
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: "OK"
                }).then((result) => {
                    if (result) {
                        //document.location.reload(true);
                        setTimeout(function () {
                            cargarDataInicial(true,false);
                        }, 1000);
                    }
                });
            }
        });
    }, 1000);
}

function fecha() {
    //Modal llamada
    $('#data1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "dd-mm-yyyy"
    });
}

function modalasignar() {
    console.log(lista);
    $('#myModal1').modal('toggle');
}

function myModalClaFe(clas, idFicha) {
    $('#myModal2').modal('toggle');
    $("#sltClasifFicha").val(clas);
    $("#hdnIdFichaClas").val(idFicha);
}

/**
 * se eejcuta al seleccionar un alumno
 */
function checkAlumno(elemento) {
    var data = $(elemento)[0].value.toString().split("-");
    if ($(elemento)[0].checked) {
        alumnos.push(data[0], data[1]);
    } else {
        alumnos.del(data[0]);
    }
}

function myModalficha(id, num_ficha) {
    alumnos.clear();
    $.ajax({
        url: "../Supervisor/Carga_alumnos_by_ficha/" + id,
        async: false,
        cache: false,
        dataType: "json",
        type: 'get',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
            alert(textStatus + errorThrown + jqXHR.responseText);
            console.log(textStatus + errorThrown + jqXHR.responseText);
        },
        success: function (result) {
            $("#lblNumFicha").html(num_ficha);
            $("#hdnIdFicha").val(id);
            var table = $('#tbl_alumnos_ficha').DataTable();
            table.clear().draw();
            if (result.length > 0) {
                $('#myModal3').modal('toggle');
                $.each(result, function (i, v) {
                    table.row.add([
                        v.rut_dv,
                        v.nombre_alumno,
                        v.num_orden_compra,
                        v.costo,
                        "<input type='checkbox' onchange='checkAlumno(this)' value='" + v.rut_alumno + "-" + v.id_orden_compra + "'>"
                    ]).draw();
                });
            } else {
                //swal("Error", "Los alumnos de esta ficha, ya fueron asignados a un tutor. Dirijase a 'Reasignar Tutor' para volver a asignar los alumnos a otro tutor.", "warning");
                swal({
                    title: 'Error',
                    text: "Los alumnos de esta ficha ya fueron asignados a un tutor. Diríjase a 'Reasignar Tutor' para volver a asignar los alumnos a otro tutor.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: "Reasignar",
                    cancelButtonText: "OK"
                }).then((result) => {
                    if (result) {
                        location.href = "Reasignar_alumnos/"+num_ficha;
                    }
                });
            }
        }
    });

}




