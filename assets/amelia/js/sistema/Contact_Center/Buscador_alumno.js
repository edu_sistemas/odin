$(document).ready(function(){

	//$('#tblResumenTotal').footable(); 

	$('#tblResumenTotal').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: true,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Resumen Holding'},
            {extend: 'pdf', title: 'Resumen Holding'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });

	$('#tblEvaluacionResultado').footable();

	$('#tblResultadoBusquedaEmpresa').footable();
	$('#tblResultadoBusquedaAlumno').footable();

	/*
	$("#buscar-rut").select2({
		placeholder: "Seleccione un RUT",
		allowClear: true
	});

	$("#buscar-nombre").select2({
		placeholder: "Seleccione un Nombre",
		allowClear: true
	});
	*/

	$(".buscar-mail").select2({
		placeholder: "Seleccione Correo",
		allowClear: true
	});

	$("#buscar-oc").select2({
		placeholder: "Seleccione Orden de Compra",
		allowClear: true
	});

	$("#buscar-ficha").select2({
		placeholder: "Seleccione Ficha",
		allowClear: true
	});

	$("#buscar-empresa").select2({
		placeholder: "Seleccione Empresa",
		allowClear: true
	});

	$(".columnHide").css('display', 'none');


	$("#btn-buscar").click(function(){

	    var rut=$("#buscar-rut").val();
	    var nombre=$("#buscar-nombre").val();
	    var email=$("#buscar-email").val();
	    var oc=$("#buscar-oc").val();
	    var ficha=$("#buscar-ficha").val();
	    var empresa=$("#buscar-empresa").val();


	    if($.trim(rut)=="" && $.trim(nombre)=="" && $.trim(email)=="" && $.trim(oc)=="" && $.trim(ficha)=="" && $.trim(empresa)=="")
	    //if(rut.length<3 && nombre.length<3 && email.length<3 && oc.length<3 && ficha.length<3 && empresa.length<3)
	    {
	        swal("Buscador Alumnos", "Debe especificar al menos 1 criterio de Búsqueda", "warning");
	    }
	    else
	    {
	        var cnt=1;
	        var resultadoAlumno;
	        var resultadoEmpresa;


	        swal({
	            title: 'Espere un momento...',
	            allowOutsideClick: false,
	            allowEscapeKey: false,
	            allowEnterKey: false,
	            onOpen: () => {
	                swal.showLoading()
	            }
	        });
	        setTimeout(function () {

	                for(i=1;i<=2;i++)
	                {
	                    
	                    $.ajax({
	                        url: "../Comun/BuscarAlumnos",
	                        dataType: "json",
	                        type: 'POST',
	                        async: false,
	                        data: { parametro: cnt, rut: rut, nombre: nombre, email: email, oc: oc, ficha: ficha, empresa: empresa },
	                        error: function (jqXHR, textStatus, errorThrown) {
	                            console.log(jqXHR);
	                        },
	                        success: function (result) 
	                        {   
	                            
	                            if(cnt==1) { resultadoAlumno=result; } else { resultadoEmpresa=result; } 
	                        }    
	                    });
	                    cnt++;  
	                }

	                if(resultadoAlumno.length==0)
	                {
	                    swal("Buscador Alumnos", "La búsqueda no entrego resultados.", "info");
	                }
	                else
	                {
	                    $("#tblResultadoBusquedaEmpresa tbody").empty();
	                    $("#tblResultadoBusquedaAlumno tbody").empty();
	                    
	                    var htmltblEmpresa="";
	                    var htmltblAlumno="";

	                    

	                    $.each(resultadoEmpresa, function (i, item) 
	                    {
	                        htmltblEmpresa+="<tr>";
	                        htmltblEmpresa+="<td NOWRAP>"+item.empresa+"</td>";
	                        htmltblEmpresa+="<td>"+item.ficha+"</td>";
	                        htmltblEmpresa+="<td>"+item.oc+"</td>";
	                        htmltblEmpresa+="<td>"+item.curso+"</td>";
	                        htmltblEmpresa+="<td>"+item.fecha_inicio+"</td>";
	                        htmltblEmpresa+="<td>"+item.fecha_termino+"</td>";
	                        htmltblEmpresa+="</tr>";
	                    });

	                    $.each(resultadoAlumno, function (i, item) 
	                    {
	                        htmltblAlumno+="<tr>";
	                        htmltblAlumno+="<td NOWRAP>"+item.rut+"</td>";
	                        htmltblAlumno+="<td><a href='../Comun/Detalle_alumno/"+item.id_usuario+"'>"+item.nombre+"</a></td>";
	                        htmltblAlumno+="<td>"+item.costo+"</td>";
	                        htmltblAlumno+="<td>"+item.dj+"</td>";
	                        htmltblAlumno+="<td>"+item.situacion+"</td>";
	                        htmltblAlumno+="<td>"+item.vip+"</td>";
	                        htmltblAlumno+="<td>"+item.tutor+"</td>";
	                        htmltblAlumno+="<td><i class='fa fa-search lupa' onclick='openEval("+item.id_usuario+","+item.id_curso+")'></i></td>";
	                        htmltblAlumno+="</tr>";
	                    });

	                    $("#tblResultadoBusquedaEmpresa tbody").append(htmltblEmpresa);
	                    $("#tblResultadoBusquedaAlumno tbody").append(htmltblAlumno);

	                    $('#tblResultadoBusquedaEmpresa').trigger('footable_initialize');
	                    $('#tblResultadoBusquedaAlumno').trigger('footable_initialize');

	                    swal.hideLoading();
	                    swal.close();

	                    $("#modalBuscador").modal('show');

	                }

	        }, 1000);

	    }

	    });

	$("#buscar-rut").append("<option value=''>Seleccione...</option>");
	$("#buscar-nombre").append("<option value=''>Seleccione...</option>");
	$("#buscar-oc").append("<option value=''>Seleccione...</option>");
	$("#buscar-ficha").append("<option value=''>Seleccione...</option>");
	$("#buscar-empresa").append("<option value=''>Seleccione...</option>");


	//BusquedaAlumnoCbx(1);
	BusquedaAlumnoCbx(2);
	BusquedaAlumnoCbx(7);
	BusquedaAlumnoCbx(8);


	$("#cboCursos").change(function(){
		

		var curso=$(this).val();
		var alumno=$("#aux_alumno").val();
		var user=$("#aux_user").val();
		

		$("#tblEvaluacionResultado tbody").empty();

		for(c=3;c<=4;c++)
		{
			if(c==3)
			{
				v1=curso;
				v2=user;
				v3=alumno;
			}
			else
			{
				v1=alumno;
				v2=curso;
				v3=0;
			}

			$.ajax({
				url: "../Comun/Gestion_alumnos_ajax",
				async: false,
				cache: false,
				dataType: "json",
				type: 'POST',
				data: { parametro: c, valor1: v1, valor2: v2, valor3: v3 },
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
				},
				success: function (result) 
				{
					if(c==3)
					{
						var item = result[0];
						$("#deNombre").html(item.nombre);
						$("#deRut").html(item.rut);
						$("#deCurso").html(item.curso);
						$("#deFicha").html(item.ficha);
						$("#deEjecutivo").html(item.ejecutivo);
						$("#deCosto").html(item.costo);
						$("#deHorasConexion").html(item.horas_conexion+" Horas.");
						$("#deDJ").html(item.dj);
						$("#deAvanceVideo").html(item.avance_video+"%");
						$("#dePrimerIngreso").html(item.primer_ingreso);
						$("#deUltimoIngreso").html(item.ultimo_ingreso);
						$("#deEncuesta").html(item.encuesta);
						$("#deEvaluacionesRealizadas").html(item.evaluaciones_realizadas);
					}
					else
					{
						$.each(result, function (ndx, item) 
						{
							if(item.cp_modulo!=null)
							{
								$("#tblEvaluacionResultado tbody").append("<tr><td>"+item.cp_modulo+"</td><td>"+item.cp_nota+"</td></tr>");
							}
						});
						$('#tblEvaluacionResultado').trigger('footable_initialize');
					}

				}    
			});

		}

	});

});

function openEval(alumno,curso){
	$('#modalBuscador').modal('hide');
	
	$("#cboCursos").empty();

	$("#tblEvaluacionResultado tbody").empty();

	//Auxiliar para eventos busqueda modales//
	$("#aux_alumno").val(alumno);

	/*
	var v1=id;
	var v2=0;
	var v3=0;
	*/

	for(c=2;c<=4;c++)
	{
		if(c==3)
		{
			v1=curso;
			v2=0;
			v3=alumno;
		}
		else
		{
			v1=alumno;
			v2=curso;
			v3=0;
		}

		$.ajax({
			url: "../Comun/Gestion_alumnos_ajax",
			async: false,
			cache: false,
			dataType: "json",
			type: 'POST',
			data: { parametro: c, valor1: v1, valor2: v2, valor3: v3 },
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR);
			},
			success: function (result) 
			{
				if(c==2)
				{
					$.each(result, function (ndx, item) 
					{
						$("#cboCursos").append("<option value='"+item.id_curso+"'>"+item.nombre_curso+"</option>");
					});

					$("#cboCursos option:last").attr("selected",true);
					v1=v2=parseInt($("#cboCursos").val());
				}
				else if(c==3)
				{
					var item = result[0];
					$("#deNombre").html(item.nombre);
					$("#deRut").html(item.rut);
					$("#deCurso").html(item.curso);
					$("#deFicha").html(item.ficha);
					$("#deEjecutivo").html(item.ejecutivo);
					$("#deCosto").html(item.costo);
					$("#deHorasConexion").html(item.horas_conexion+" Horas.");
					$("#deDJ").html(item.dj);
					$("#deAvanceVideo").html(item.avance_video+"%");
					$("#dePrimerIngreso").html(item.primer_ingreso);
					$("#deUltimoIngreso").html(item.ultimo_ingreso);
					$("#deEncuesta").html(item.encuesta);
					$("#deEvaluacionesRealizadas").html(item.evaluaciones_realizadas);
					v1=parseInt($("#aux_alumno").val());
				}
				else
				{
					$.each(result, function (ndx, item) 
					{
						if(item.cp_modulo!=null)
						{
							$("#tblEvaluacionResultado tbody").append("<tr><td>"+item.cp_modulo+"</td><td>"+item.cp_nota+"</td></tr>");
						}
					});
					$('#tblEvaluacionResultado').trigger('footable_initialize');
				}

			}    
		});

	}

	$('#modalEVAL').modal('show');
}


function BusquedaAlumnoCbx(parametro)
{
	var tutor=$("#id_user").val();
	var perfil=$("#id_perfil").val();



	$.ajax({
		url: "../Comun/Buscar_alumnos_vista",
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: { parametro: parametro, tutor: tutor, perfil: perfil },
		error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
            return false;
        },
        success: function (result) 
        {
        
        	$.each(result, function (i, item) 
        	{

        		switch(parametro)
        		{
        			case 1:
        			$("#buscar-rut").append("<option value='"+item.rut+"'>"+item.rut+"</option>");
        			$("#buscar-nombre").append("<option value='"+item.nombre+"'>"+item.nombre+"</option>");
        			break;
        			case 8:
        			$("#buscar-oc").append("<option value='"+item.id+"'>"+item.text+"</option>");
        			break;
        			case 7:
        			$("#buscar-ficha").append("<option value='"+item.id+"'>"+item.text+"</option>");
        			break;
        			case 2:
        			$("#buscar-empresa").append("<option value='"+item.id+"'>"+item.text+"</option>");
        			break;
        		}

        	});

        }    
    });
}