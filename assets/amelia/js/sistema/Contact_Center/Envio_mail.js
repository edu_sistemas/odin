var lista = [];
var alumnos = function () {
    "use strict";
    return {
        //main function
        push: function (id) {
            lista.push(id);
        },
        del: function (id) {
            var paso = false;
            for (var i = 0; i < lista.length; i++) {
                if (lista[i] == id) {
                    lista.splice(i, 1);
                    paso = true;
                    break;
                }
            }
            return paso;
        },
        exist: function (id) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i] == id) {
                    return true;
                }
            }
            return false;
        }
    };
}();
$(document).ready(function () {

    var tbl_fichas = $('#tbl_fichas').DataTable({
        paging: false,
        searching: true,
        ordering: false,
        info: false,
        bFilter: true,
        responsive: false,
        sDom: '<"top">rt<"clear">',
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });
    var tblAlumnosFicha = $('#tblAlumnosFicha').DataTable({
        paging: false,
        searching: true,
        ordering: false,
        info: false,
        bFilter: false,
        responsive: false,
        sDom: '<"top">rt<"clear">',
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    $("#txtBuscarFicha").keyup(function (e) {
        e.preventDefault();
        tbl_fichas.search(this.value).draw();

    });

    $("#txtBuscarAlumnos").keyup(function (e) {
        e.preventDefault();
        tblAlumnosFicha.search(this.value).draw();
    });



    $('#summernote').summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']]
        ],
        hint: {
            mentions: ['Nombre_Alumno', 'Nombre_Curso ', 'Fecha_Termino_Curso '],
            match: /\B@(\w*)$/,
            search: function (keyword, callback) {
                callback($.grep(this.mentions, function (item) {
                    return item.indexOf(keyword) == 0;
                }));
            },
            content: function (item) {
                return '@' + item;
            }
        }
    });
    $("input[name='chkFicha']").on('change', function () {
        var check = $(this)[0].checked;
        $.ajax({
            url: "../Comun/enviomail_get_alumnos/" + $(this)[0].value,
            async: false,
            cache: false,
            dataType: "json",
            type: 'get',
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                //$("#tblAlumnosFicha tbody").html("");
                var tblAlumnosFicha = $("#tblAlumnosFicha").DataTable();
                tblAlumnosFicha.clear().draw();
                $.each(result, function (i, v) {
                    var checked = check ? "checked" : "";
                    if (check) {
                        if (!alumnos.exist(v.id_detalle_alumno))
                            alumnos.push(v.id_detalle_alumno);
                    } else {
                        if (alumnos.exist(v.id_detalle_alumno))
                            alumnos.del(v.id_detalle_alumno);
                    }

                    tblAlumnosFicha.row.add([
                        v.nombre_completo,
                        "<input type='checkbox' name='chkAlumno' id='chkAlumno" + v.id_detalle_alumno + "' value='" + v.id_detalle_alumno + "' " + checked + " onchange='checkedAlumno(this)' />"
                    ]).draw(false);
                    //$("#tblAlumnosFicha tbody").append("<tr><td>" + v.nombre_completo + "</td><td><input type='checkbox' name='chkAlumno' id='chkAlumno" + v.id_detalle_alumno + "' value='" + v.id_detalle_alumno + "' " + checked + " onchange='checkedAlumno(this)' /></td></tr>");
                });
            }
        });
    });
    $("#sltPlantillas").on("change", function () {
        $.ajax({
            url: "../Comun/enviomail_get_plantilla",
            async: false,
            cache: false,
            dataType: "html",
            type: 'post',
            data: {
                plantilla: $("#sltPlantillas").val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR + textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                // console.log(result);
                $("#summernote").summernote("code", result);
            }
        });
    });



    $("#btnEnviar").on("click", function () {

        var txtplantilla = $("#sltPlantillas").val();

        if (txtplantilla == null)
        {
            swal("Envio Mail", "Debe seleccionar Plantilla.", "warning");
            return 0;
        }

        if (lista.length == 0)
        {
            swal("Envio Mail", "Debe seleccionar al menos 1 alumno.", "warning");
            return 0;
        }

        var vplantilla = $('#summernote').summernote('code');
        var vcc = $("#txtCC").val();
         var asunto = $("#txtAsunto").val();

        $.ajax({
            url: "../Comun/enviomail_enviar_plantilla",
            async: false,
            cache: false,
            dataType: "json",
            type: 'post',
            data: {
                ids: lista,
                plantilla: vplantilla,
                cc: vcc,
                asunto: asunto
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown + jqXHR.responseText);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                console.log(result);
                if (result.length > 0)
                    swal("Enviado", "Se enviaron correctamente los correos", "success");
                else
                    swal("Aviso", "Ninguno de los alumnos tiene un correo de contacto deifnido", "warning");
            }
        });
    });


    $("#btnCheck").on("click", function () {
        console.log("alumnos length:" + lista.length);
    });


    $("#btnGuardarGestion").on("click", function () {

        swal({
            title: 'Espere un momento...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            onOpen: () => {
                swal.showLoading()
            }
        });
        setTimeout(function () {
            var cat1 = $("#sltCategoria1").val();
            var cat2 = $("#sltCategoria2").val();
            var cat3 = $("#sltCategoria3").val();
            var come = $("textarea#comentario").val();

            if (cat1 == "" || cat2 == "" || cat3 == "" || $.trim(come) == "")
            {
           swal("Dejar Gestión", "Debe completar los datos del formulario.", "warning");
                return 0;
            }

            if (lista.length == 0)
            {
           swal("Dejar Gestión", "Debe seleccionar al menos 1 alumno.", "warning");
                return 0;
            }


            $.ajax({
                url: "../Comun/Agregar_gestion_envio_mail",
                async: false,
                cache: false,
                dataType: "json",
                type: "post",
                data: {
                    categoria1: cat1,
                    categoria2: cat2,
                    categoria3: cat2,
                    comentario: come,
                    noContactar: $("#nocontactar")[0].checked,
                    alumnos: lista
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal.hideLoading();
                    swal.close();
                    console.log(textStatus, errorThrown);
                    console.error(jqXHR.responseText);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    swal.hideLoading();
                    swal.close();
                    swal({
                        title: "Aviso",
                        text: result.Msg,
                        type: "success"
                    }).then((result) => {
                        if (result) {
                            window.location.reload();
                        }
                    });
                }
            });
        }, 1000);

    });
    $("#sltCategoria1").on("change", function () {
        $.ajax({
            url: "../Comun/cargaCombocategoriaCbx",
            async: false,
            cache: false,
            dataType: "json",
            type: 'post',
            data: {
                categoria: 2,
                valor: $("#sltCategoria1").val(),
                valor2: 0
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                console.log(result);
                cargaCBX("sltCategoria2", result);
            }
        });
    })

    $("#sltCategoria2").on("change", function () {
        $.ajax({
            url: "../Comun/cargaCombocategoriaCbx",
            async: false,
            cache: false,
            dataType: "json",
            type: 'post',
            data: {
                categoria: 3,
                valor: $("#sltCategoria1").val(),
                valor2: $("#sltCategoria2").val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                console.log(result);
                cargaCBX("sltCategoria3", result);
            }
        });
    })
});
/**
 * obtiene los alumnos de una ficha segun su ID
 */
function getAlumnosFicha(id) {
    $.ajax({
        url: "../Comun/enviomail_get_alumnos/" + id,
        async: false,
        cache: false,
        dataType: "json",
        type: 'get',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
            alert(textStatus + errorThrown + jqXHR.responseText);
        },
        success: function (result) {

            var tblAlumnosFicha = $("#tblAlumnosFicha").DataTable();
            tblAlumnosFicha.clear().draw();
            $.each(result, function (i, v) {
                var checked = "";
                if (alumnos.exist(v.id_detalle_alumno))
                    checked = "checked";
                else
                    checked = "";
                tblAlumnosFicha.row.add([
                    v.nombre_completo,
                    "<input type='checkbox' name='chkAlumno' id='chkAlumno" + v.id_detalle_alumno + "' value='" + v.id_detalle_alumno + "' " + checked + " onchange='checkedAlumno(this)' />"
                ]).draw(false);

                //$("#tblAlumnosFicha tbody").append("<tr><td>" + v.nombre_completo + "</td><td><input type='checkbox' name='chkAlumno' id='chkAlumno" + v.id_detalle_alumno + "' value='" + v.id_detalle_alumno + "' " + checked + " onchange='checkedAlumno(this)' /></td></tr>");
            });
        }
    });
}

function checkedAlumno(element) {
    if (element.checked) {
        alumnos.push(element.value);
    } else {
        alumnos.del(element.value);
    }
}

function activaEmail() {

    $("#sltPlantillas").prop('selectedIndex', 0);
    $("#txtCC").val('');
    $("#summernote").summernote("code", '');

    setTimeout(function () {
        $("#botonera").fadeOut(500);
    }, 500);
    setTimeout(function () {
        $("#correoInterno").fadeIn(1100);
    }, 1100);
}

function activaGestion() {

    $("#sltCategoria1").val('');
    $("#sltCategoria2").val('');
    $("#sltCategoria3").val('');
    $("#comentario").val('');

    setTimeout(function () {
        $("#botonera").fadeOut(500);
    }, 500);
    setTimeout(function () {
        $("#correoExterno").fadeIn(1100);
    }, 1100);
}

function volverBotonera() {
    setTimeout(function () {
        $("div.btnSeleccionado").fadeOut(500);
    }, 500);
    setTimeout(function () {
        $("#botonera").fadeIn(1100);
    }, 1100);
}
