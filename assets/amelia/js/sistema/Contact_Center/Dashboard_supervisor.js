$(document).ready(function () {

    ajaxCargaGrafico();

});

function constructTblTutor(data) {
    $('#tblTutor').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'excel', title: 'Resumen Dashboard'},
            {extend: 'pdf', title: 'Resumen Dashboard'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });
    var tbl = $("#tblTutor").DataTable();
    tbl.clear().draw();
    $.each(data, function (i, v) {
        tbl.row.add([
            v.nombre_apellido_tutor,
            v.dj_enviadas,
            v.conect,
            v.evaluacion,
            v.seguimiento,
            v.llamadas,
            v.resp_tot,
            v.n_alumnos
        ]);
        tbl.draw(false);
    });

}

function ajaxCargaGrafico() {
    var perfil = $("#perfil").val();

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'get',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            alert(textStatus + errorThrown + jqXHR.responseText);
        },
        success: function (result) {
            if (result.length > 0) {
                cargaDeclaraciones(result[1]);
                cargaConectadas(result[2]);
                cargaEvaluaciones(result[3]);
                cargaSeguimiento(result[4]);
                cargaLlamados(result[5]);
                cargaRespuestas(result[6]);
                cargaLlamadaMayor(result[7]);
                constructTblTutor(result[0]);
            } else {
                swal("Error", "No existen elementos para mostrar", "warning");
            }
        },

        url: "../Contact_center/Comun/getDatosGraficos"
    });
    /*  if (perfil == 27) { 
     $("#contentTblTutor").css('display', '');
     }   */
}

//--------------------------------//-------------------------------------//
function cargaDeclaraciones(data) {
    var result = data[0];
    var dataResult = jsonToArray(result);
    var paso = false;
    for (var i = 0; i < dataResult.length; i++) {
        if (parseInt(dataResult[i]) > 0) {
            paso = true;
        }
    }
    if (!paso) {
        $("#chartDeclaraciones").html("<center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>");
        $("#chartDeclaraciones").addClass("pasoG");               
        return;
    }

    var SdeltaE = parseInt(result.q_dj_env) - (parseInt(result.q_dj_env_e_learn) + parseInt(result.q_dj_env_dist));
    var PdeltaE = 100 - (parseFloat(result.porcen_env_e_learn) + parseFloat(result.porcen_env_dist));

    var SdeltaN = parseInt(result.q_dj_no_env) - (parseInt(result.q_dj_no_env_e_learn) + parseInt(result.q_dj_no_env_dist));
    var PdeltaN = 100 - (parseFloat(result.porcen_no_env_e_learn) + parseFloat(result.porcen_no_env_dist));

    var colors = Highcharts.getOptions().colors,
            categories = [
                "Enviadas",
                "No Enviadas"
            ],
            data = [
                {
                    "y": parseInt(result.q_dj_env), // -- interno 1
                    "color": "#f89001",
                    "drilldown": {
                        "name": "Enviada",
                        "categories": [
                            "E-Learning" + ' ' + parseInt(result.porcen_env_e_learn) + '%/' + result.q_dj_env_e_learn,
                            "Distancia" + ' ' + result.porcen_env_dist + '%/' + result.q_dj_env_dist,
                            //"No Informado" + ' ' + PdeltaE + '%/' + SdeltaE
                        ],
                        "data": [
                            parseInt(result.q_dj_env_e_learn), //-- externo 1
                            parseInt(result.q_dj_env_dist), //-- externo 2
                            //parseInt(SdeltaE) //-- externo 3
                        ]
                    },
                    tooltip_data: ["Enviada"]
                },
                {
                    "y": parseInt(result.q_dj_no_env), // -- interno 2
                    "color": '#4F1977',
                    "drilldown": {
                        "name": "No enviada",
                        "categories": [
                            "E-Learning" + ' ' + parseInt(result.porcen_no_env_e_learn) + '%/' + result.q_dj_no_env_e_learn,
                            "Distancia" + ' ' + result.porcen_no_env_dist + '%/' + result.q_dj_no_env_dist,
                            //"No Informado" + ' ' + parseInt(PdeltaN) + '%/' + SdeltaN
                        ],
                        "data": [
                            parseInt(result.q_dj_no_env_e_learn), //-- externo 4
                            parseInt(result.q_dj_no_env_dist), //-- externo 5
                            //parseInt(SdeltaN) //-- externo 6
                        ]
                    },
                    tooltip_data: ["No Enviada"]
                }

            ],
            browserData = [],
            versionsData = [],
            i,
            j,
            dataLen = data.length,
            drillDataLen,
            brightness;


    // Build the data arrays
    for (i = 0; i < dataLen; i += 1) {

        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color,
            tooltip_data: data[i].tooltip_data
        });

        // add version data
        drillDataLen = data[i].drilldown.data.length;
        for (j = 0; j < drillDataLen; j += 1) {
            brightness = 0.2 - (j / drillDataLen) / 5;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
                color: Highcharts.Color(data[i].color).brighten(brightness).get(),
                tooltip_data: data[i].tooltip_data
            });
        }
    }

    // Create the chart
    Highcharts.chart('chartDeclaraciones', {

        chart: {
            type: 'pie',
            maxWidth: 635
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%']
            }
        },
        tooltip: {
            useHTML: true,
            formatter: function () {
                if (this.series.index == 0)
                {
                    return this.point.name + '<br> Total: <b>' + this.y + '</b>';
                } else
                {
                    return this.point.name + '<br>' + this.point.tooltip_data + ': <b>' + this.y + '</b>';
                }
            }
        },
        series: [{
                name: 'DATO',
                data: browserData,
                size: '50%',
                color: '#ffffff',
                dataLabels: {
                    formatter: function () {
                        return this.y > 0 ? '<b>'+ this.y + ' ' + this.point.name + '</b> ' : this.point.name;
                    },
                    distance: -30
                }
            }, {
                name: 'DATO',
                data: versionsData,
                size: '50%',
                innerSize: '70%',
                dataLabels: {
                    formatter: function () {
                        // display only if larger than 1
                        return this.y > 0 ? '<b>' + this.point.name + '</b> ' : null;
                    }
                },
                id: 'versions'
            }
        ],
        responsive: {
            rules: [{
                    condition: {
                        maxWidth: 400
                    },
                    chartOptions: {
                        series: [{
                                id: 'versions',
                                dataLabels: {
                                    enabled: false
                                }
                            }]
                    }
                }]
        }
    });


}

//--------------------------------//-------------------------------------//

function cargaConectadas(data2) {
    var result = data2[0];
    var dataResult = jsonToArray(result);
    var paso = false;
    for (var i = 0; i < dataResult.length; i++) {
        if (parseInt(dataResult[i]) > 0) {
            paso = true;
        }
    }
    if (!paso) {
        $("#chartConectadas").html("<center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>");
        $("#chartConectadas").addClass("pasoG");                
        return;
    }
    var data = [{
            'id': '0.0',
            'parent': '',
            'name': 'Conexión'
        }, {
            'id': '1.3',
            'parent': '0.0',
            'name': 'Cumple:' + '<br>' + ' ' + parseInt(result.q_conect_cumple),
            'value': parseInt(result.q_conect_cumple),
            'color': '#f89001',
            'porcen': result.porcen_conect_cumple + '%'
        }, {
            'id': '1.1',
            'parent': '0.0',
            'name': 'No Cumple:' + '<br>' + ' ' + parseInt(result.q_conect_no_cumple),
            'value': parseInt(result.q_conect_no_cumple),
            'color': '#4F1977',
            'porcen': result.porcen_conect_no_cumple + '%'
        },
        {
            'parent': '1.1',
            'name': '> 0:' + '<br>' + ' ' + parseInt(result.q_conect_no_cumple_igual_1),
            'value': parseInt(result.q_conect_no_cumple_igual_1),
            'porcen': result.porcen_conect_no_cumple_igual_1 + '%'
        },

        {
            'parent': '1.1',
            'name': 'Límite:' + '<br>' + ' ' + parseInt(result.q_conect_no_cumple_may_1),
            'value': parseInt(result.q_conect_no_cumple_may_1),
            'porcen': result.porcen_conect_no_cumple_may_1 + '%'
        }
    ];

    // Splice in transparent for the center circle
    Highcharts.getOptions().colors.splice(0, 0, 'transparent');


    Highcharts.chart('chartConectadas', {

        chart: {
            //height: '60%'
            type: 'pie'
        },

        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        series: [{
                type: "sunburst",
                data: data,
                allowDrillToNode: true,
                cursor: 'pointer',
                dataLabels: {
                    format: '{point.name}',
                    filter: {
                        property: 'innerArcLength',
                        operator: '>',
                        value: 16
                    }
                },
                levels: [{
                        level: 1,
                        levelIsConstant: false,
                        dataLabels: {
                            rotationMode: 'parallel',
                            filter: {
                                property: '',
                                operator: '>',
                                value: 64
                            }
                        }
                        ,
                        levelSize: {
                            unit: 'pixels',
                            value: 10
                        }
                    }, {
                        level: 2,
                        colorByPoint: true,
                        dataLabels: {
                            rotationMode: 'parallel'
                        },
                        levelSize: {
                            unit: 'pixels',
                            value: 70
                        }
                    },
                    {
                        level: 3,
                        dataLabels: {
                            rotationMode: 'parallel'
                        },
                        colorVariation: {
                            key: 'brightness',
                            to: -0.5
                        },
                        levelSize: {
                            unit: 'pixels',
                            value: 80
                        }
                    }, {
                        level: 4,
                        colorVariation: {
                            key: 'brightness',
                            to: 0.5
                        }
                    }]

            }],
        tooltip: {
            headerFormat: "",
            pointFormat: '<b>{point.porcen}</b>/<b>{point.value}</b>'
        },
    });
}

//--------------------------------//-------------------------------------//

function cargaEvaluaciones(p_data) {
    var result = p_data[0];
    var dataResult = jsonToArray(result);
    var paso = false;
    for (var i = 0; i < dataResult.length; i++) {
        if (parseInt(dataResult[i]) > 0) {
            paso = true;
        }
    }
    if (!paso) {
        $("#chartEvaluaciones").html("<center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>");
        $("#chartEvaluaciones").addClass("pasoG");                
        return;
    }
    Highcharts.chart('chartEvaluaciones', {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },

        tooltip: {
            headerFormat: '',
            pointFormat: '<b> {point.name}</b>'
        },
        series: [{
                size: '50%',
                minPointSize: 10,
                innerSize: '30%',
                zMin: 0,
                name: 'chartEvaluaciones',
                data: [{
                        color: '#f89001',
                        name: 'Listos ' + result.porcen_eval_ok + '% / ' + result.q_eval_ok,
                        y: parseInt(result.q_eval_ok)// -- interno 1
                    }, {
                        color: "#4F1977",
                        name: 'Pendientes ' + result.porcen_eval_no_ok + '% / ' + result.q_eval_no_ok,
                        y: parseInt(result.q_eval_no_ok)// -- interno 2
                    }]
            }]
    });

}

function cargaSeguimiento(p_data) {
    var result = p_data[0];
    var dataResult = jsonToArray(result);
    var paso = false;
    for (var i = 0; i < dataResult.length; i++) {
        if (parseInt(dataResult[i]) > 0) {
            paso = true;
        }
    }
    if (!paso) {
        $("#chartSeguimiento").html("<center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>");
        $("#chartSeguimiento").addClass("pasoG");        
        return;
    }
    Highcharts.chart('chartSeguimiento', {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<b> {point.name}</b>'
        },
        series: [{
                size: '50%',
                minPointSize: 10,
                innerSize: '30%',
                zMin: 0,
                name: 'chartSeguimiento',
                data: [{
                        color: '#f89001',
                        name: 'Tiene ' + result.porcen_seg_tiene + '% / ' + result.q_seg_tiene,
                        y: parseInt(result.q_seg_tiene)
                    }, {
                        color: "#4F1977",
                        name: 'No tiene ' + result.porcen_seg_no_tiene + '% / ' + result.q_seg_no_tiene,
                        y: parseInt(result.q_seg_no_tiene)
                    }]
            }]
    });
}

//--------------------------------//-------------------------------------//

function cargaLlamados(p_data) {
    var result = p_data[0];
    var dataResult = jsonToArray(result);
    var paso = false;
    for (var i = 0; i < dataResult.length; i++) {
        if (parseInt(dataResult[i]) > 0) {
            paso = true;
        }
    }
    if (!paso) {
        $("#chartLlamados").html("<center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>");
        $("#chartLlamados").addClass("pasoG");
        return;
    }
    Highcharts.chart('chartLlamados', {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<b> {point.name}</b>'
        },
        series: [{
                size: '50%',
                minPointSize: 10,
                innerSize: '30%',
                zMin: 0,
                name: 'chartLlamados',
                data: [{
                        color: '#f89001',
                        name: 'Cumple ' + result.porcen_llam_cump + '% / ' + result.q_llam_cump,
                        y: parseInt(result.q_llam_cump)// -- interno 1
                    }, {
                        color: "#4F1977",
                        name: 'No cumple ' + result.porcen_llam_no_cump + '% / ' + result.q_llam_no_cump,
                        y: parseInt(result.q_llam_no_cump)// -- interno 1
                    }]
            }]
    });
}

//--------------------------------//-------------------------------------//

function cargaRespuestas(p_data) {
    var result = p_data[0];
    var dataResult = jsonToArray(result);
    var paso = false;
    for (var i = 0; i < dataResult.length; i++) {
        if (parseInt(dataResult[i]) > 0) {
            paso = true;
        }
    }
    if (!paso) {
        $("#chartRespuestas").html("<center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>");
        $("#chartRespuestas").addClass("pasoG");
        return;
    }
    Highcharts.chart('chartRespuestas', {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<b> {point.name}</b>'
        },
        series: [{
                size: '50%',
                minPointSize: 10,
                innerSize: '30%',
                zMin: 0,
                name: 'chartRespuestas',
                data: [{
                        color: '#f89001',
                        name: 'Entrantes ' + result.porcen_tot_gestion_entrante + '% / ' + result.q_tot_gestion_entrante,
                        y: parseInt(result.q_tot_gestion_entrante)//-- interno 1
                    }, {
                        color: "#4F1977",
                        name: 'Salientes ' + result.porcen_tot_gestion_saliente + '% / ' + result.q_tot_gestion_saliente,
                        y: parseInt(result.q_tot_gestion_saliente)//-- interno 2
                    }]
            }]
    });
}

//--------------------------------//-------------------------------------//


function cargaLlamadaMayor(p_data) {
    var result = p_data[0];
    var dataResult = jsonToArray(result);
    var paso = false;
    for (var i = 0; i < dataResult.length; i++) {
        if (parseInt(dataResult[i]) > 0) {
            paso = true;
        }
    }
    if (!paso) {
        $("#chartLlamadaMayor").html("<center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>");
        $("#chartLlamadaMayor").addClass("pasoG");
        return;
    }
    Highcharts.chart('chartLlamadaMayor', {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },
        tooltip: {
            headerFormat: '',
            pointFormat: '<b> {point.name}</b>'
        },
        series: [{
                size: '50%',
                minPointSize: 10,
                innerSize: '30%',
                zMin: 0,
                name: 'chartLlamadaMayor',
                data: [{
                        color: '#f89001',
                        name: 'Mayores a 30 segundos: ' + parseInt(result.porcen_llam_may_30) + '% / ' + result.q_llam_may_30,
                        y: parseInt(result.q_llam_may_30)
                    }, {
                        color: "#4F1977",
                        name: 'Menor a 30 segundos: ' + parseInt(result.porcen_llam_men_30) + '% / ' + result.q_llam_men_30,
                        y: parseInt(result.q_llam_men_30)
                    }, {
                        color: "#616161",
                        name: 'No Contestadas: ' + parseInt(result.porcen_llam_no_contestadas) + '% / ' + result.q_llam_no_contestadas,
                        y: parseInt(result.q_llam_no_contestadas)
                    }]
            }]
    });
}

//--------------------------------//-------------------------------------//

var jsonToArray = function (obj) {
    var arr = [];
    for (var x in obj)
        if (obj.hasOwnProperty(x)) {
            arr.push(obj[x]);
        }
    return arr;
}