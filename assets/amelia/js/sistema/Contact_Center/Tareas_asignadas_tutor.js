jQuery(document).ready(function (e) {

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
     $('#tbl_tareas_nuevo').DataTable()
     .columns.adjust()
     .responsive.recalc();
 });

    $("#empresa").select2({
        placeholder: "Seleccione Empresa",
        value: 0,
        allowClear: true
    });
    $("#filt_modalidad").select2({
        placeholder: "Seleccione Modalidad",
        value: 0,
        allowClear: true
    });
    $("#filt_tcosto").select2({
        placeholder: "Seleccione Tipo Costo",
        value: 0,
        allowClear: true
    });
    $("#inp_ficha").select2({
        placeholder: "Seleccione Ficha",
        value: 0,
        allowClear: true
    });
    $("#inp_oc").select2({
        placeholder: "Seleccione Orden de Compra",
        value: 0,
        allowClear: true
    });

    $('#summernote').summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']]
            ],
            hint: {
                mentions: ['Nombre_Alumno', 'Nombre_Curso', 'Fecha_Termino_Curso'],
                match: /\B@(\w*)$/,
                search: function (keyword, callback) {
                    callback($.grep(this.mentions, function (item) {
                        return item.indexOf(keyword) == 0;
                    }));
                },
                content: function (item) {
                    return '@' + item;
                }
            }
        });

    $("#sltPlantillas").on("change", function () {
        if ($(this).val().trim().length == 0) {
            $('#summernote').summernote('code', '');
            return;
        }
        $.ajax({
            url: "../Comun/enviomail_get_plantilla",
            async: false,
            cache: false,
            dataType: "html",
            type: 'post',
            data: {
                plantilla: $("#sltPlantillas").val()
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                $("#summernote").summernote("code", result);
            }
        });
    });

    $('#tbl_tareas').DataTable({
        //autoWidth: false,
        paging: true,
        columnDefs: [
        {
                targets: [15, 16, 17, 18], // column or columns numbers
                orderable: false, // set orderable for selected columns
            },
            {
                targets: [6, 7], // Columnas de Fechas
                visible: false,
            },
            {
                targets: [1], // Columnas de Fechas
                width: "10%",
            },
            { responsivePriority: 2, targets: -1 } //Manito siempre Visible
            ],
            searching: true,
            info: false,
            bFilter: false,
            responsive: true,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            select: {
                style: 'single'
            },
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
            {extend: 'copy', text: 'Copiar', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}},
            {extend: 'excel', title: 'Tareas Tutor', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}},
            {extend: 'pdf', title: 'Tareas Tutor', orientation: 'landscape', pageSize: 'LEGAL', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]}},

            {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
                }, exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}
            }
            ],
            rowReorder: true,
            pageLength: 10

        });

    $('#tbl_tareas_nuevo').DataTable({
        autoWidth: false,
        paging: true,
        columnDefs: [
        {
                targets: [14, 15, 16, 17], // column or columns numbers
                orderable: false, // set orderable for selected columns
            },
            {
                targets: [1, 6,7,8,9],
                width: "10%"
            },
            { responsivePriority: 2, targets: -1 } //Manito siempre Visible
            ],
            searching: true,
            info: false,
            bFilter: false,
            responsive: true,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
            {extend: 'copy', text: 'Copiar', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}},
            {extend: 'excel', title: 'Tareas Tutor (Nuevos)', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}},
            {extend: 'pdf', title: 'Tareas Tutor (Nuevos)', orientation: 'landscape', pageSize: 'LEGAL', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
                }, exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]}
            }
            ],
            pageLength: 10

        });

    $("#btnBuscar").on("click", function () {
        var empresa = $("#empresa").val(),
        modalidad = $("#filt_modalidad").val(),
        costo = $("#filt_tcosto").val(),
        ficha = $("#inp_ficha").val(),
        oc = $("#inp_oc").val(),
        curso = $("#curso").val();

        var activoTab1 = $("#tab-1")[0].className.includes("active");
        var activoTab2 = $("#tab-2")[0].className.includes("active");
        if (activoTab1 || activoTab2) {
            swal({
                title: 'Espere un momento...',
                allowOutsideClick: false,
                allowEscapeKey: false,
                allowEnterKey: false,
                onOpen: () => {
                    swal.showLoading()
                }
            });
            setTimeout(function () {
                $.ajax({
                    url: (activoTab1 ? "../Tutor/Cargar_tareas_by_filtro" : "../Tutor/Cargar_tareas_nuevas_by_filtro"),
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: "post",
                    data: {
                        empresa: empresa,
                        modalidad: modalidad,
                        costo: costo,
                        ficha: ficha,
                        oc: oc,
                        curso: curso
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus + errorThrown);
                        alert(textStatus + errorThrown + jqXHR.responseText);
                    },
                    success: function (result) {
                        var idTbl = (activoTab1 ? "#tbl_tareas" : "#tbl_tareas_nuevo");
                        var table = $(idTbl).DataTable();
                        table.clear().draw();
                        $.each(result, function (i, v) {
                            var indicador = (activoTab1 ? v.indicador : "D1D1D1");
                           
                            if (activoTab1) {//tareas
                                 console.log(v);
                                table.row.add([
                                    (i + 1),
                                    v.rut_dv,
                                    v.nombre_apellido,
                                    v.ficha,
                                    v.orden_compra,
                                    v.cp_modalidad,
                                    v.fecha_desde,
                                    v.fecha_hasta,
                                    v.fecha_inicio,
                                    v.fecha_fin,
                                    v.fecha_cierre,
                                    v.cp_costo,
                                    v.dj,
                                    (v.Tiempo_conexion.length > 0 ? Number.parseFloat(v.Tiempo_conexion).toFixed(2) : ""),
                                    v.seguimiento,
                                    '<i class="fa fa-circle fa-lg" style="color: #' + indicador + '"></i>',
                                    '<a href=""><i class="fa fa-search"></i></a>',
                                    '<a href=""><i class="fa fa-envelope"></i></a>',
                                    '<i class="fa fa-thumbs-up" title="' + ((v.id_estado_tarea == 2 ? "Contactado" : "NO contactado")) + '" style="font-size:20px;color:' + (v.id_estado_tarea == 2 ? "green" : "gray") + ';" aria-hidden="true"></i>'
                                    ]).draw(false);

                            } else if (activoTab2) {//tareas nuevas
                                table.row.add([
                                    (i + 1),
                                    v.rut_dv,
                                    v.nombre_apellido,
                                    v.ficha,
                                    v.orden_compra,
                                    v.cp_modalidad,
                                    v.fecha_encuentro,
                                    v.fecha_inicio,
                                    v.fecha_fin,
                                    v.cp_costo,
                                    v.dj,
                                    (v.Tiempo_conexion.length > 0 ? Number.parseFloat(v.Tiempo_conexion).toFixed(2) : ""),
                                    v.seguimiento,
                                    '<i class="fa fa-circle fa-lg" style="color: #' + indicador + '"></i>',
                                    '<a href=""><i class="fa fa-search"></i></a>',
                                    '<a href=""><i class="fa fa-envelope"></i></a>',
                                    '<i class="fa fa-thumbs-up" title="' + ((v.id_estado_tarea == 2 ? "Contactado" : "NO contactado")) + '" style="font-size:20px;color:' + (v.id_estado_tarea == 2 ? "green" : "gray") + ';" aria-hidden="true"></i>'
                                    ]).draw(false);
                            }
                        });
                    },
                    complete: function (jqXHR, textStatus) {
                        swal.hideLoading();
                        swal.close();
                    }
                });
}, 1000);
} else {
    swal("Error", "Debe seleccionar 'Tareas' o 'Tareas Nuevo'", "error");
}
});

$("#btnEnviar").on("click", function () {
    if ($("#sltPlantillas").val().toString().trim().length == 0)
    {
        $("#mdlMail").modal('toggle');
        swal({
            title: "Error",
            text: "Debe seleccionar una plantilla",
            type: "error"
        }).then(function () {
            $("#mdlMail").modal('toggle');
        });
        return;
    }
    if ($("#hdnMail").val().toString().trim().length == 0) {
        $("#mdlMail").modal('toggle');
        swal({
            title: "Error",
            text: "Debe escribir el correo electrónico del alumnos",
            type: "error"
        }).then(function () {
            $("#mdlMail").modal('toggle');
        });
        return;
    }
    if (!isEmail($("#hdnMail").val())) {
        $("#mdlMail").modal('toggle');
        swal({
            title: "Error",
            text: "Debe escribir un correo electrónico válido",
            type: "error"
        }).then(function () {
            $("#mdlMail").modal('toggle');
        });
        return;
    }

    $.ajax({
        url: "../Tutor/Enviar_mail",
        async: false,
        cache: false,
        dataType: "json",
        type: 'post',
        data: {
            plantilla: $('#summernote').summernote('code'),
            cc: $("#txtCC").val(),
            asunto: $("#txtAsunto").val(),
            email: $("#hdnMail").val(),
            id_tarea: $("#hdnTarea").val(),
            nombre_curso: $("#hdnNombreCurso").val(),
            fecha_termino_curso: $("#hdnFechaTerminoCurso").val(),
            nombre_alumno: $("#txtMail").html()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
            alert(textStatus + errorThrown + jqXHR.responseText);
            console.log(textStatus, errorThrown, jqXHR.responseText);

        },
        success: function (result) {
           /* console.log(result);
            console.log(result['response']['message']);*/
            $("#mdlMail").modal('toggle');
            $('#summernote').summernote('code', '');
            if (result['response']['resultado'])
                swal("Aviso", result['response']['message'], "info");
            else
                swal("Error", result['response']['message'], "error");
        }
    });
});
});


function setMailToSend(idTarea, nombre, mail, curso, fecha_termino_curso) {
    $("#txtMail").html(nombre);
    $("#hdnMail").val(mail);
    $("#hdnTarea").val(idTarea);
    $("#mdlMail").modal('toggle');
    $("#hdnNombreCurso").val(curso);
    $("#hdnFechaTerminoCurso").val(fecha_termino_curso);
    $("#sltPlantillas").val("");
    $('#summernote').summernote('code', '');
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}