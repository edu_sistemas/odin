$(document).ready(function () {

	$('.footable').footable();

	$(".filtro-empresa").select2({
		placeholder: "Seleccione una Empresa",
		allowClear: true
	});

	$(".filtro-ficha").select2({
		placeholder: "Seleccione una ficha",
		allowClear: true
	});

	$(".filtro-oc").select2({
		placeholder: "Seleccione una Orden de Compra",
		allowClear: true
	});

	$(".filtro-modalidad").select2({
		placeholder: "Seleccione una Modalidad",
		allowClear: true
	});

	$(".filtro-curso").select2({
		placeholder: "Seleccione una Modalidad",
		allowClear: true
	});

	$(".columnHide").css('display', 'none');


	// GRAFICO  //


	var colors = Highcharts.getOptions().colors,
	  categories = [
	  "Enviada",
	  "No Enviada"
	  ],
	  data = [
	  {
	      "y": parseInt(result.dato1),// -- interno 1
	      "color": "#e6b54a", 
	      "drilldown": {
	          "name": "Enviada",
	          "categories": [
	          "E-Learning",
	          "Distancia"
	          ],
	          "data": [
	          parseInt(result.dato3),//-- externo 1
	          parseInt(result.dato4) //-- externo 2
	          ]
	      }
	  },
	  {
	      "y": parseInt(result.dato2),// -- interno 1
	      "color": '#850f5b',
	      "drilldown": {
	          "name": "No enviada",
	          "categories": [
	          "E-Learning",
	          "Distancia"
	          ],
	          "data": [
	          parseInt(result.dato5),//-- externo 3
	          parseInt(result.dato6) //-- externo 4
	          ]
	      }
	  }

	  ],
	  browserData = [],
	  versionsData = [],
	  i,
	  j,
	  dataLen = data.length,
	  drillDataLen,
	  brightness;


	  // Build the data arrays
	  for (i = 0; i < dataLen; i += 1) {

	      // add browser data
	      browserData.push({
	          name: categories[i],
	          y: data[i].y,
	          color: data[i].color
	      });

	      // add version data
	      drillDataLen = data[i].drilldown.data.length;
	      for (j = 0; j < drillDataLen; j += 1) {
	          brightness = 0.2 - (j / drillDataLen) / 5;
	          versionsData.push({
	              name: data[i].drilldown.categories[j],
	              y: data[i].drilldown.data[j],
	              color: Highcharts.Color(data[i].color).brighten(brightness).get()
	          });
	      }
	  }

	  // Create the chart
	  Highcharts.chart('chartDeclaraciones', {

	      chart: {
	          type: 'pie'
	      },
	      title: {
	          text: ''
	      },
	      subtitle: {
	          text: ''
	      },
	      yAxis: {
	          title: {
	              text: ''
	          }
	      },
	      plotOptions: {
	          pie: {
	              shadow: false,
	              center: ['50%', '50%']
	          }
	      },
	      tooltip: {
	          valueSuffix: '%'
	      },
	      series: [{
	          name: 'DATO',
	          data: browserData,
	          size: '50%',
	          color: '#ffffff',
	          dataLabels: {
	              formatter: function () {
	                  return this.y > 5 ? this.point.name : null;
	              },
	              
	              distance: -30
	          }
	      }, {
	          name: 'DATO',
	          data: versionsData,
	          size: '50%',
	          innerSize: '70%',
	          dataLabels: {
	              formatter: function () {
	                  // display only if larger than 1
	                  return this.y > 1 ? '<b>' + this.point.name + ':</b> ' +
	                  this.y + '%' : null;
	              }
	          },
	          id: 'versions'
	      }],
	      responsive: {
	          rules: [{
	              condition: {
	                  maxWidth: 400
	              },
	              chartOptions: {
	                  series: [{
	                      id: 'versions',
	                      dataLabels: {
	                          enabled: false
	                      }
	                  }]
	              }
	          }]
	      }
	  });

	//  GRAFICO  //
})