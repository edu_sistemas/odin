$(document).ready(function () {


    var tabla = $('#tblResultado').DataTable({
    	columnDefs: [
        { 
            targets: [3,4], 
            visible: false, 
        }
        ],
    	paging: false,
    	ordering: true,
    	info: false,
    	bFilter: false,
    	responsive: true,
    	searching: false,
    	language: {
    		"sProcessing": "Procesando...",
    		"sLengthMenu": "Mostrar _MENU_ registros",
    		"sZeroRecords": "No se encontraron resultados",
    		"sEmptyTable": "Ningún dato disponible en esta tabla",
    		"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    		"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    		"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    		"sInfoPostFix": "",
    		"sSearch": "Buscar:",
    		"sUrl": "",
    		"sInfoThousands": ",",
    		"sLoadingRecords": "Cargando...",
    		"oPaginate": {
    			"sFirst": "Primero",
    			"sLast": "Último",
    			"sNext": "Siguiente",
    			"sPrevious": "Anterior"
    		},
    		"oAria": {
    			"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
    			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
    		}
    	},
    	select: {
    		style: 'single'
    	},
    	dom: '<"html5buttons"B>lTfgitp',
    	buttons: [
    	{extend: 'copy', text: 'Copiar',  exportOptions: { columns: ":visible" }},
    	{extend: 'excel', title: 'Resultado Periodo Tutor', exportOptions: { columns: [0,3,4,5] } },
    	{extend: 'pdf', title: 'Resultado Periodo Tutor', exportOptions: { columns: ":visible" } },

    	{
    		extend: 'print',
    		customize: function (win) {
    			$(win.document.body).addClass('white-bg');
    			$(win.document.body).css('font-size', '10px');

    			$(win.document.body).find('table')
    			.addClass('compact')
    			.css('font-size', 'inherit');
    		},
            exportOptions: { columns: ":visible" }
    	}
    	],
    	rowReorder: true,
    	pageLength: 10

    });

    var tablaalumnos = $('#tblAlumnosporTutor').DataTable({
    	columnDefs: [
    	{ 
    		width: "10%", 
    		targets: [2,6,7] 
    	},
        { 
            targets: [1], 
            visible: false 
        }
    	],
    	autoWidth:false,
    	paging: true,
    	ordering: true,
    	info: false,
    	bFilter: false,
    	responsive: true,
    	searching: true,
    	language: {
    		"sProcessing": "Procesando...",
    		"sLengthMenu": "Mostrar _MENU_ registros",
    		"sZeroRecords": "No se encontraron resultados",
    		"sEmptyTable": "Ningún dato disponible en esta tabla",
    		"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    		"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    		"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    		"sInfoPostFix": "",
    		"sSearch": "Buscar:",
    		"sUrl": "",
    		"sInfoThousands": ",",
    		"sLoadingRecords": "Cargando...",
    		"oPaginate": {
    			"sFirst": "Primero",
    			"sLast": "Último",
    			"sNext": "Siguiente",
    			"sPrevious": "Anterior"
    		},
    		"oAria": {
    			"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
    			"sSortDescending": ": Activar para ordenar la columna de manera descendente"
    		}
    	},
    	select: {
    		style: 'single'
    	},
    	dom: '<"html5buttons"B>lTfgitp',
    	buttons: [
    	//{extend: 'copy', text: 'Copiar'},
    	{extend: 'excel', title: 'Detalle Bono Tutor', exportOptions: { columns: [1,2,3,4,5,6,7,8,9,10,11,12] }},
    	{extend: 'pdf', title: 'Detalle Bono Tutor', exportOptions: { columns: ":visible" }},

    	{
    		extend: 'print',
    		customize: function (win) {
    			$(win.document.body).addClass('white-bg');
    			$(win.document.body).css('font-size', '10px');

    			$(win.document.body).find('table')
    			.addClass('compact')
    			.css('font-size', 'inherit');
    		}, exportOptions: { columns: ":visible" }
    	}
    	],
		// rowReorder: true,
		pageLength: 10

	});



    var Categorias= [];
    var Actual=[];
    var Meta=[];

    tabla.clear().draw();


    var p_id_tutor=$("#id_tutor").val();

    $.ajax({
    	url: "CalculoBonosAjax",
    	dataType: "json",
    	type: 'POST',
    	async: true,
    	data: { p_parametro: 3, p_id_tutor: p_id_tutor, p_periodo: '' },
    	error: function (jqXHR, textStatus, errorThrown) {
    		console.log(jqXHR);
    	},
    	success: function (result) 
    	{	

    		$.each(result, function (i, item) 
    		{
    			Categorias.push(item.nom_mes);
    			Actual.push(parseInt(item.actual,0));
    			Meta.push(parseInt(item.meta,0));

    			tabla.row.add([
    				item.tutor,
					"$"+new Intl.NumberFormat("es-ES").format(item.actual),
					"$"+new Intl.NumberFormat("es-ES").format(item.meta),
                    parseInt(item.actual),
                    parseInt(item.meta),
					item.periodo,
					"<center><button class='btn btn-primary btn-sm detalle' id='"+item.periodo+"'>Detalle</button></center>"
					]).draw(false);
    		});

    		CargaGrafico(Categorias,Actual,Meta);
    	}    
    });


    $("#tblResultado").on('click','.detalle',function(){

    	var p_periodo=$(this).attr('id');

    	swal({
    		title: 'Espere un momento...',
    		allowOutsideClick: false,
    		allowEscapeKey: false,
    		allowEnterKey: false,
    		onOpen: () => {
    			swal.showLoading()
    		}
    	});
    	setTimeout(function () {


    		tablaalumnos.clear().draw();

    		$.ajax({
    			url: "CalculoBonosAjax",
    			dataType: "json",
    			type: 'POST',
    			async: false,
    			data: { p_parametro: 4, p_id_tutor: p_id_tutor, p_periodo: p_periodo },
    			error: function (jqXHR, textStatus, errorThrown) {
    				console.log(jqXHR);
    			},
    			success: function (result) 
    			{	

    				$.each(result, function (i, item) 
    				{
    					tablaalumnos.row.add([
    						"$"+new Intl.NumberFormat("es-ES").format(item.bono),
                            parseInt(item.bono),
    						item.tutor,
    						item.rut,
    						item.nombre,
    						item.ficha,
    						item.modalidad,
    						item.fecha_inicio,
    						item.fecha_termino,
    						item.costo,
    						item.dj,
    						item.tiempo_coneccion,
    						item.evaluacion+"%"
    						]).draw(false);
    				});

    				swal.hideLoading();
    				swal.close();

    				$("#modalDetalle").modal("toggle");


    			}    
    		});

    	}, 1000);

    });

});


function CargaGrafico(Cat,Act,Met)
{
	Highcharts.setOptions({
		lang: {
			thousandsSep: '.'
		}
	});
	//Create the chart
	Highcharts.chart('bonoMen', {
		chart: {
			type: 'column'
		},
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		xAxis: {
			categories: Cat,
			crosshair: true
		},
		yAxis: {
			min: 0,
			title: {
				text: '$'
			},
			labels: {
				formatter: function () {
					return '$'+new Intl.NumberFormat("es-ES").format(this.value);
				}
			}
		},
		tooltip: {
			headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
			'<td style="padding:0"><b>${point.y} </b></td></tr>',
			footerFormat: '</table>',
			shared: true,
			useHTML: true
		},
		plotOptions: {
			column: {
				pointPadding: 0.2,
				borderWidth: 0
			}
		},
		series: [{
			name: 'Actual',
			data: Act

		}, {
			name: 'Meta',
			data: Met

		}]
	});
}


function detalleBono(){
	$('#divResultado').css("display","");
}