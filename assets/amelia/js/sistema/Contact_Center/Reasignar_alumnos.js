var lista = [];
var reasignaciones = function () {
    "use strict";
    return {
        //main function
        push: function (data) {
            lista.push(data);
        },
        del: function (data) {
            var paso = false;
            for (var i = 0; i < lista.length; i++) {
                if (lista[i] == data) {
                    lista.splice(i, 1);
                    paso = true;
                    break;
                }
            }
            return paso;
        },
        exist: function (data) {
            for (var i = 0; i < lista.length; i++) {
                if (lista[i] == data) {
                    return true;
                }
            }
            return false;
        },
        clear: function () {
            lista = [];
        }
    };
}();
$(document).ready(function () {
    

    $("#empresa").select2({
        placeholder: "Seleccione Empresa",
        value: 0,
        allowClear: true
    });
    $("#inp_ficha").select2({
        placeholder: "Seleccione Ficha",
        value: 0,
        allowClear: true
    });
    $("#inp_oc").select2({
        placeholder: "Seleccione Orden Compra",
        value: 0,
        allowClear: true
    });

    $('#tbl_tutores_todos').DataTable({
        paging: true,
        searching: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10

    });

    var tbl_alumnos_reasignar = $('#tbl_alumnos_reasignar').DataTable({

        paging: true,
        searching: false,
        ordering: true,
        info: true,
        bFilter: false,
        responsive: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: 4,
                width: "10%"
            },
            {
                targets: 6,
                orderable: false,
                'searchable':false,
                'className': 'dt-body-center',
//                'render': function (data, type, full, meta){
//                    return $('<div/>').text(data).html();
//                }
            }
        ],
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10,
//        dom: 'Bfrtip',
//        select: {style: 'multi'}
//        buttons: [
//            {
//                text: 'Seleccionar Todos',
//                action: function () {
//                    tbl_alumnos_reasignar.rows().select();
//                }
//            },
//            {
//                text: 'Deseleccionar Todos',
//                action: function () {
//                    tbl_alumnos_reasignar.rows().deselect();
//                }
//            }
//        ]

    });

    $("#btnBuscar").on("click", function () {
        
        var inp_rut = $("#inp_rut").val();
        var inp_ficha = $("#inp_ficha").val();
        var inp_oc = $("#inp_oc").val();
        var empresa = $("#empresa").val();
        console.log(inp_rut, inp_ficha, inp_oc, empresa);
        if ((!inp_rut)
                && (!inp_ficha)
                && (!inp_oc)
                && (!empresa)) {
            location.reload();
            return;
        }

        $.ajax({
            url: $("#hdnBaseUrl").val()+"/Filtrar_alumnos_reasignar",
            async: false,
            cache: false,
            dataType: "json",
            type: 'post',
            data: {
                rut: inp_rut,
                ficha: inp_ficha,
                oc: inp_oc,
                empresa: empresa
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown + jqXHR.responseText);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                var tbl = $("#tbl_alumnos_reasignar").DataTable();
                tbl.clear().draw();
                $.each(result, function (i, v) {
                    tbl.row.add([
                        v.nombre_tutor,
                        v.razon_social_empresa,
                        v.id_ficha,
                        v.num_orden_compra,
                        v.rut_alumno_fmt,
                        v.nombre_alumno,
                        "<input type='checkbox' name='chkAlumno' value='"+v.id_asignacion+"' />"
                    ]).node().id = v.id_asignacion;
                    tbl.draw(false);
                });
            }
        });
    });

    $("#chkSelTodosAlumnos").on("change", function () {
        
        var rows = tbl_alumnos_reasignar.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
        
//        var chks = document.getElementsByName('chkAlumno');
//        for (var i = 0; i < chks.length; i++) {
//            chks[i].checked = $(this)[0].checked;
//            console.log(i+1);
//        }
    });
});


function modalasignar() {

    var rows = $('#tbl_alumnos_reasignar').DataTable().rows({ 'search': 'applied' }).nodes();
    var count = $('input[type="checkbox"]', rows);
    var selectAnyChk = false;
    for (var i = 0; i < count.length; i++) {
        if(count[i].checked){
            selectAnyChk = true;
            break;
        }
    }
    if (selectAnyChk) {
        $('#myModal2').modal('toggle');
        $.ajax({
            url: $("#hdnBaseUrl").val()+"/Carga_tutores",
            async: false,
            cache: false,
            dataType: "json",
            type: 'get',
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                var table = $('#tbl_tutores_todos').DataTable();
                table.clear().draw();
                $.each(result, function (i, v) {
                    table.row.add([
                        v.nombre_usuario,
                        v.q_alumnos,
                        "<button type='button' class='btn btn-primary btn-xs' onclick='asignarTutor(" + v.id_usuario + ")' >Asignar</button>"
                    ]).draw();
                });
            }
        });
        reasignaciones.clear();
            for (var i = 0; i < count.length; i++) {
                if(count[i].checked)
                reasignaciones.push(count[i].value);
            }
    } else {
        swal("Error", "Debe seleccionar al menos un registro de la tabla", "error");
    }
}

function asignarTutor(idTutor) {
    $('#myModal2').modal('toggle');
    swal({
        title: 'Espere un momento...',
        allowOutsideClick: false,
        allowEscapeKey: false,
        allowEnterKey: false,
        onOpen: () => {
            swal.showLoading()
        }
    });
    setTimeout(function () {

        $.ajax({
            url: $("#hdnBaseUrl").val()+"/Reasignar_alumnos_tutor",
            async: false,
            cache: false,
            dataType: "json",
            type: 'post',
            data: {
                id_tutor: idTutor,
                data_reasignaciones: lista
            },
            error: function (jqXHR, textStatus, errorThrown) {
                swal.hideLoading();
                swal.close();
                console.log(textStatus + errorThrown + jqXHR.responseText);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                console.log(result);
                swal.hideLoading();
                swal.close();
                swal(
                        {
                            title: "Reasignación OK",
                            text: "Alumnos reasignados correctamente",
                            type: "success"
                        }
                ).then((result) => {
                    window.location.reload();
                });
            }
        });
    }, 1000);
}