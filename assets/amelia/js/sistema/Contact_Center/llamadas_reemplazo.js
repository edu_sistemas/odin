$(document).ready(function () {

    $('#fecha-termino').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('#indicadoresTutor').DataTable({

        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 5,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Llamadas Reemplazo'},
            {extend: 'pdf', title: 'Llamadas Reemplazo'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });

    $('#indicadoresEmpresa').DataTable({
        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 5,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Llamadas Reemplazo'},
            {extend: 'pdf', title: 'Llamadas Reemplazo'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });

    var tblIndicadoresAlumnos = $('#indicadoresAlumnos').DataTable({
        autoWidth: false,
        columnDefs: [
            {
                targets: [8, 11], // column or columns numbers
                orderable: false, // set orderable for selected columns
            },
            {
                targets: [0, 6],
                width: "10%"
            }
        ],
        paging: true,
        ordering: true,
        info: false,
        bFilter: false,
        responsive: true,
        searching: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 5,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar'},
            {extend: 'csv'},
            {extend: 'excel', title: 'Llamadas Reemplazo'},
            {extend: 'pdf', title: 'Llamadas Reemplazo'},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });

    $(".filtro-holding").select2({
        placeholder: "Seleccione Holding",
        allowClear: true
    });

    $(".filtro-empresa").select2({
        placeholder: "Seleccione Empresa",
        allowClear: true
    });

    $(".filtro-ficha").select2({
        placeholder: "Seleccione Ficha",
        allowClear: true
    });

    $(".filtro-oc").select2({
        placeholder: "Seleccione Orden de Compra",
        allowClear: true
    });

    $(".filtro-modalidad").select2({
        placeholder: "Seleccione Modalidad",
        allowClear: true
    });

    $(".filtro-curso").select2({
        placeholder: "Seleccione Curso (Curso|Horas|Modalidad)",
        allowClear: true
    });

    $(".columnHide").css('display', 'none');

    $("#btnBuscar").on("click", function () {
        swal({
            title: 'Espere un momento...',
            allowOutsideClick: false,
            allowEscapeKey: false,
            allowEnterKey: false,
            onOpen: () => {
                swal.showLoading()
            }
        });
        setTimeout(function () {
            $.ajax({
                url: "../Comun/Carga_detalle_llamadas_mayores",
                async: false,
                cache: false,
                dataType: "json",
                type: "post",
                data: {
                    id_holding: $("#filtro-holding").val(),
                    id_empresa: $("#filtro-empresa").val(),
                    num_ficha: $("#filtro-ficha").val(),
                    num_oc: $("#filtro-oc").val(),
                    id_curso: $("#filtro-curso").val(),
                    fecha_cierre: $("#fecha-termino").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus + errorThrown);
                    alert(textStatus + errorThrown + jqXHR.responseText);
                    console.log(textStatus + errorThrown + jqXHR.responseText);
                },
                success: function (result) {
                    var tblAlumnos = $("#indicadoresAlumnos").DataTable();
                    tblAlumnos.clear().draw();
                    $.each(result, function (i, v) {
                        if (result[i].length > 0) {
                            switch (result[i][0].id_resulset) {
                                case "0":
                                    $.each(result[i], function (i, v) {
                                        tblAlumnos.row.add([
                                            v.rut,
                                            v.nombre_apellido,
                                            v.nombre_apellido_tutor,
                                            v.razon_social_empresa,
                                            v.ficha,
                                            v.curso,
                                            v.cp_fechaCierre,
                                            v.cp_costo,
                                            "<center><i class='fa fa-circle fa-lg' style='color: #" + v.indicador + "'></i></center>",
                                            v.seguimiento,
                                            Number.parseFloat(v.registro_sence).toFixed(3),
                                            "<a href='Detalle_alumno/" + v.cp_idUsuario + "'><i class='fa fa-eye'></i></a>",
                                            v.DJ,
                                            v.fecha_limite_entrega,
                                            v.cp_modulo,
                                            v.cp_nota,
                                            v.Promedio,
                                            v.Evaluaciones_Realizadas,
                                            v.CTS,
                                            v.q_seguim_salientes,
                                            v.q_llamadas_realizadas_PBX,
                                            v.q_seguim_entrantes,
                                            v.q_cant_llamadas_recibidas_PBX
                                        ]).draw(false);
                                    });
                                    break;
                            }
                        }
                    });
                },
                complete: function (jqXHR, textStatus) {
                    swal.hideLoading();
                    swal.close();
                }
            });
        }, 1000);
    });

    tblIndicadoresAlumnos.columns('.columnHide').visible(false, false);
    tblIndicadoresAlumnos.columns.adjust().draw(false);

//CARGA GRAFICOS
    ajaxCargaGrafico();
//CARGA CBX FILTROS
    ajaxCargaFiltros(1);
    ajaxCargaFiltros(2);
    ajaxCargaFiltros(3);
    ajaxCargaFiltros(4);
    ajaxCargaFiltros(5);
})

function ajaxCargaGrafico() {
    $.ajax({
        url: "../Comun/Carga_detalle_llamadas_mayores",
        async: false,
        cache: false,
        dataType: "json",
        type: "post",
        data: {
            id_holding: $("#filtro-holding").val(),
            id_empresa: $("#filtro-empresa").val(),
            num_ficha: $("#filtro-ficha").val(),
            num_oc: $("#filtro-oc").val(),
            id_curso: $("#filtro-curso").val(),
            fecha_cierre: $("#fecha-termino").val(),
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
            alert(textStatus + errorThrown + jqXHR.responseText);
            console.log(textStatus + errorThrown + jqXHR.responseText);
        },
        success: function (result) {
            console.log(result);
            var length = parseInt(Object.keys(result).length);
            $.each(result, function (i, v) {
                if (result[i].length > 0) {
                    console.log(result[i][0].id_resulset);
                    switch (result[i][0].id_resulset) {
                        case "24":
                            cargaReemplazoPorcen(result[i][0]);
                            break;
                        case "25":
                            var tblTutor = $("#indicadoresTutor").DataTable();
                            tblTutor.clear().draw();
                            $.each(result[i], function (i, v) {
                                tblTutor.row.add([
                                    v.nombre_apellido_tutor,
                                    v.may_5,
                                    v.men_5
                                ]).draw(false);
                            });
                            break;
                        case "26":
                            var tblEmpresa = $("#indicadoresEmpresa").DataTable();
                            tblEmpresa.clear().draw();
                            $.each(result[i], function (i, v) {
                                tblEmpresa.row.add([
                                    v.razon_social_empresa,
                                    v.may_5,
                                    v.men_5
                                ]).draw(false);
                            });
                            break;
                        case "-1"://Deshabilitado para carga inicial
                            var tblAlumnos = $("#indicadoresAlumnos").DataTable();
                            tblAlumnos.clear().draw();
                            $.each(result[i], function (i, v) {
                                tblAlumnos.row.add([
                                    v.rut,
                                    v.nombre_apellido,
                                    v.nombre_apellido_tutor,
                                    v.razon_social_empresa,
                                    v.ficha,
                                    v.curso,
                                    v.cp_fechaCierre,
                                    v.cp_costo,
                                    "<center><i class='fa fa-circle fa-lg' style='color: #" + v.indicador + "'></i></center>",
                                    v.seguimiento,
                                    Number.parseFloat(v.registro_sence).toFixed(3),
                                    "<a href='Detalle_alumno/" + v.cp_idUsuario + "'><i class='fa fa-eye'></i></a>",
                                    v.DJ,
                                    v.fecha_limite_entrega,
                                    v.cp_modulo,
                                    v.cp_nota,
                                    v.Promedio,
                                    v.Evaluaciones_Realizadas,
                                    v.CTS,
                                    v.q_seguim_salientes,
                                    v.q_llamadas_realizadas_PBX,
                                    v.q_seguim_entrantes,
                                    v.q_cant_llamadas_recibidas_PBX
                                ]).draw(false);
                            });
                            break;
                    }
                }

            });
        }
    });
}

//--------------------------------//-------------------------------------//
function cargaReemplazoPorcen(result) {
    var dataResult = jsonToArray(result);
    console.log("s", dataResult);
    var paso = false;
    for (var i = 0; i < dataResult.length - 1; i++) {
        if (parseInt(dataResult[i]) > 0) {
            paso = true;
        }
    }
    if (!paso) {
        $("#cargaReemplazoPorcen").html("<center><span style='color: red;'><b>No existen datos para este gráfico</b></span></center>");
        $("#cargaReemplazoPorcen").addClass("pasoG");
        return;
    }
    Highcharts.chart('cargaReemplazoPorcen', {
        chart: {
            type: 'pie'
        },
        title: {
            text: ''
        },

        tooltip: {
            headerFormat: '',
            pointFormat: '<b> {point.name}:</b><br/>' +
                    '<b>{point.y}</b><br/>'
        },
        series: [{
                size: '50%',
                minPointSize: 10,
                innerSize: '30%',
                zMin: 0,
                name: 'cargaReemplazoPorcen',
                data: [{
                        color: '#e6b54a',
                        name: '> 300 seg  ' + result.porcen_llam_may_30 + '% / ' + result.q_llam_may_300,
                        y: parseFloat(result.porcen_llam_may_30)// -- interno 1
                    }, {
                        color: "#850f5b",
                        name: 'No contestadas ' + result.porcen_llam_no_contestadas + '% / ' + result.q_llam_no_contestadas,
                        y: parseFloat(result.porcen_llam_no_contestadas)// -- interno 2
                    }, {
                        color: "#850f5b",
                        name: '< 5 seg ' + result.porcen_llam_men_5 + '% / ' + result.q_llam_men_5,
                        y: parseFloat(result.porcen_llam_men_5)// -- interno 2
                    }, {
                        color: "#850f5b",
                        name: '5 a 30 seg ' + result.porcen_may_5_men_30 + '% / ' + result.q_llam_may_5_men_30,
                        y: parseFloat(result.porcen_may_5_men_30)// -- interno 2
                    }, {
                        color: "#850f5b",
                        name: '30 a 300 seg ' + result.porcen_may_30_men_300 + '% / ' + result.q_llam_may_30_men_300,
                        y: parseFloat(result.porcen_may_30_men_300)// -- interno 2
                    }]
            }]
    });
}
//--------------------------------//-------------------------------------//


//--------------------------------//-------------------------------------//
//-------------------------CARGA DE COMBOBOX-----------------------------//
//--------------------------------//-------------------------------------//

function ajaxCargaFiltros(filtro) {

    //var perfil = $("#perfil").val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //  data: $('#frm').serialize(),
        data: {filtro: filtro},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {

            switch (filtro) {
                case 1:
                    cargaComboHolding(result);
                    break;
                case 2:
                    cargaComboEmpresa(result);
                    break;
                case 3:
                    cargaComboFicha(result);
                    break;
                case 4:
                    cargaComboOC(result);
                    break;
            }
        },
        url: "../Comun/getComboFiltrosCbx"
    });
}

/*---------------CARGA-COMBOBOX-HOLDING---------------*/
function cargaComboHolding(result) {
    cargaCBX("filtro-holding", result);
    //$('#filtro-holding').find('option:first').text('Seleccione Holding');
}
/*-------------FIN-CARGA-COMBOBOX-HOLDING--------------*/


/*---------------CARGA-COMBOBOX-EMPRESA---------------*/
function cargaComboEmpresa(result) {
    cargaCBX("filtro-empresa", result);
    //$('#filtro-empresa').find('option:first').text('Seleccione Empresa');
}
/*-------------FIN-CARGA-COMBOBOX-EMPRESA--------------*/


/*---------------CARGA-COMBOBOX-FICHA---------------*/
function cargaComboFicha(result) {
    cargaCBX("filtro-ficha", result);
    //$('#filtro-ficha').find('option:first').text('Seleccione Ficha');
}
/*-------------FIN-CARGA-COMBOBOX-FICHA--------------*/


/*---------------CARGA-COMBOBOX-ORDEN-COMPRA---------------*/
function cargaComboOC(result) {
    cargaCBX("filtro-oc", result);
    //$('#filtro-oc').find('option:first').text('Seleccione Orden de Compra');
}
/*-------------FIN-CARGA-COMBOBOX-ORDEN-COMPRA--------------*/


/*---------------CARGA-COMBOBOX-CURSO---------------*/
function cargaComboCurso(result) {
    cargaCBX("filtro-curso", result);
    //$('#filtro-curso').find('option:first').text('Seleccione Curso');
}
/*-------------FIN-CARGA-COMBOBOX-CURSO--------------*/
var jsonToArray = function (obj) {
    var arr = [];
    for (var x in obj)
        if (obj.hasOwnProperty(x)) {
            arr.push(obj[x]);
        }
    return arr;
}
