var rutsChecks = [];
var isBusqueda = true;
$(document).ready(function () {

    //$('#tblResultado').footable();
    $('#tblResultado').DataTable({
        autoWidth:false,
        paging: true,
        ordering: true,
        info: true,
        bFilter: false,
        responsive: true,
        processing: true,
        serverSide: true,
        deferLoading: 0,
        ajax: {
            url: "../Comercial/getTablaBusquedaPagging",
            type: 'POST',
            data: function (d) {
                d.empresa = $("#hdnEmpresa").val();
                d.ficha = $("#hdnFicha").val();
                d.oc = $("#hdnOc").val();
                d.modalidad = $("#hdnModalidad").val();
                d.column = d.order[0].column;
                d.dir = d.order[0].dir;
                d.isBusqueda = isBusqueda;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown, jqXHR.responseText);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
//            success: function (data) {
//                console.log(data);
//            },
            complete: function () {
                for (var i = 0; i < rutsChecks.length; i++) {
                    var idICheck = "#chk" + rutsChecks[i].idICheck;
                    $(idICheck).iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green'
                    }).on('ifClicked', function (e) {
                        var target = e.currentTarget;
                        console.log(target.value);
                        if (target.checked)
                            $('#chkAll').iCheck('uncheck');
                        $.ajax({
                            url: "../Comercial/SC_Checked",
                            async: false,
                            cache: false,
                            dataType: "json",
                            type: "post",
                            data: {
                                select: !target.checked,
                                datos: target.value
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus, errorThrown, jqXHR.responseText);
                                alert(textStatus + errorThrown + jqXHR.responseText);
                            },
                            success: function (result) {
                                console.log(result);
                            }
                        });
                    });
                    console.log(rutsChecks[i], rutsChecks[i].isChecked)
                    if (rutsChecks[i].isChecked) {
                        $(idICheck).iCheck('check');
                    } else {
                        $(idICheck).iCheck('uncheck');
                    }
                }
                isBusqueda = false;
            },
            dataSrc: function (json) {
                console.log("dataSrc",json);
                rutsChecks = json.data;
                return json.data;
            }
        },
        columns: [
            {data: 'tutor'},
            {data: 'rut'},
            {data: 'nombre_apellido'},
            {data: 'ficha'},
            {data: 'num_orden_compra'},
            {data: 'cp_modalidad'},
            {data: 'cp_fechainicio'},
            {data: 'cp_fechaCierre'},
            {data: 'cp_costo'},
            {data: 'dj'},
            {
                data: 'Tiempo_conexion',
                render: function (data, type, row) {
                    return Number.parseFloat(data).toFixed(2);
                }
            },
            {data: 'seguimiento'},
            {
                data: null,
                render: function (data, type, row) {
                    var str = '<input type="checkbox" value="@valor" id="chk@idChk" class="i-checks" name="">';
                    str = str.replace("@valor", data.rut + "_" + data.ficha + "_" + data.num_orden_compra);
                    str = str.replace("@idChk", data.idICheck);
                    return str;
                }
            }
        ],
        columnDefs: [
            {
                targets: [12], // column or columns numbers
                orderable: false, // set orderable for selected columns
            },
            { 
                width: "20%", 
                targets: 1 
            },
            { 
                width: "15%", 
                targets: [6,7] 
            }
        ],
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        pageLength: 10
    });


    $(".filtro-empresa").select2({
        placeholder: "Seleccione Empresa",
        allowClear: true
    });

    $(".filtro-ficha").select2({
        placeholder: "Seleccione Ficha",
        value: 0,
        allowClear: true
    });

    $(".filtro-oc").select2({
        placeholder: "Seleccione Orden de Compra",
        allowClear: true
    });

    $(".filtro-modalidad").select2({
        placeholder: "Seleccione Modalidad",
        allowClear: true
    });

    $(".accion").select2({
        placeholder: "Seleccione Acción",
        allowClear: true
    });


    $('#chkAll').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    }).on('ifClicked', function (e) {
        var isChecked = !e.currentTarget.checked;
        if (isChecked) {
            $('.i-checks').iCheck('check');
        } else {
            $('.i-checks').iCheck('uncheck');
        }
        $.ajax({
            url: "../Comercial/SC_SelectAll",
            async: false,
            cache: false,
            dataType: "json",
            type: 'post',
            data: {
                selectAll: isChecked
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown, jqXHR.responseText);
                alert(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                //swal("Aviso", result.Msg, "info");
                console.log(result.Msg);
            }
        });
    });


    $("td").click(function () {
        $(this).addClass("selected");

    });

    $("#selectAccion").change(function () {
        var input = $('#selectAccion').val()
        //alert(input);
        if (input == 3) {
            $('#divFicha').css("display", "");
        } else {
            $('#divFicha').css("display", "none");
        }
        if (input == 6) {
            $('#divFecha').css("display", "");
        } else {
            $('#divFecha').css("display", "none");
        }
    });


    //CARGA CBX FILTROS
    ajaxCargaFiltros(2);
    ajaxCargaFiltros(7);
    ajaxCargaFiltros(4);
    ajaxCargaFiltros(5);


})

function fecha() {
    $('#inputFecha').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });
}

function resultadoBusqueda() {

    var modalidad = $('#filtro-modalidad').val();
    var empresa = $('#filtro-empresa').val();
    var ficha = $("#filtro-ficha").val();
    var oc = $("#filtro-oc").val();


    //if (modalidad.trim() !== '' || empresa.trim() !== '' || ficha.trim() !== '' || oc.trim() !== '' ) {
    if (modalidad.trim().length > 0 || empresa.trim().length > 0 || ficha.trim().length > 0 || oc.trim().length > 0) {
        $('#divResultado').css("display", "");
        $('#tblResultado').css("display", "");
        $('#selecAccion').css("display", "");
        //alert(5555);			
        //cargaTablaBusqueda();
        $(".footable>tbody>tr").each(function (index, elem) {
            $(elem).remove();
        });

//        swal({
//            title: 'Espere un momento...',
//            allowOutsideClick: true,
//            allowEscapeKey: false,
//            allowEnterKey: false,
//            timer: 5000,
//            onOpen: () => {
//                swal.showLoading()
//            }
//        });

        setTimeout(function () {

            cargaTablaBusqueda(empresa, ficha, oc, modalidad);

        }, 1000);


    } else {
        swal("Error", "Filtros vacíos, seleccione al menos uno.", "warning");
    }
}

function validarNot() {
    if (validarRequeridoForm($("#formGenNot"))) {
        generarNot();
    }
}

function generarNot() {

    //alert($('#inputAlumno').val());
    //alert('asdf');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#formGenNot').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus, errorThrown, jqXHR.responseText);
            //console.log($('#formGenNot').serialize());
            //alerta('Nueva Cuenta', jqXHR.responseText, 'error');
            //alert('ERROR');
        },
        success: function (result) {
            console.log(result);
            //alert(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            //alert('EXITO');
            $('#modalAcc').modal('hide');
            $('#formGenNot').trigger("reset");

            swal({
                title: "Notificación",
                text: "Notificación creada con éxito.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#a5dc86",
                confirmButtonText: "Ok",
                closeOnConfirm: false
            });
        },
        url: "../Comercial/generarNot"
    });

    //console.log($('#textComentario').val());
}

function camposChecked() {

    $.ajax({
        url: "../Comercial/validar_generar_not",
        async: false,
        cache: false,
        dataType: "json",
        type: "post",
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown,jqXHR.responseText);
            alert(textStatus + errorThrown + jqXHR.responseText);
        },
        success: function (result) {
            console.log(result);
            if (result.validado) {
                //Limpieza de campos al mostrar modal
                $("#selectAccion").val('');
                $("#inputFicha").val('');
                $('#divFicha').hide();
                $("#textComentario").val('');
                $("#modalAcc").modal('toggle');
            } else {
                swal("Error", result.msg, "warning");
            }
        }
    });

}


function cargaTablaBusqueda(empresa, ficha, oc, modalidad, result = null) {

    $("#hdnEmpresa").val(empresa);
    $("#hdnFicha").val(ficha);
    $("#hdnOc").val(oc);
    $("#hdnModalidad").val(modalidad);
    isBusqueda = true;
    $('#chkAll').iCheck('uncheck');

    var tableResultado = $('#tblResultado').DataTable();
    tableResultado.ajax.reload();
}


//--------------------------------//-------------------------------------//
//-------------------------CARGA DE COMBOBOX-----------------------------//
//--------------------------------//-------------------------------------//

function ajaxCargaFiltros(filtro) {

    //var perfil = $("#perfil").val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //  data: $('#frm').serialize(),
        data: {filtro: filtro},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {

            switch (filtro) {
                case 2:
                    cargaComboEmpresa(result);
                    break;
                case 7:
                    cargaComboFicha(result);
                    break;
                case 4:
                    cargaComboOC(result);
                    break;
                case 5:
                    cargaComboCurso(result);
                    break;
            }
        },
        url: "../Comercial/getComboFiltrosCbx"
    });
}



/*---------------CARGA-COMBOBOX-EMPRESA---------------*/
function cargaComboEmpresa(result) {
    cargaCBX("filtro-empresa", result);
    //$('#filtro-empresa').val($('#filtro-empresa option:first-child').val(0)).trigger('change')
    //$('#filtro-empresa').find('option:first').text('Seleccione Empresa');
}
/*-------------FIN-CARGA-COMBOBOX-EMPRESA--------------*/


/*---------------CARGA-COMBOBOX-FICHA---------------*/
function cargaComboFicha(result) {
    cargaCBX("filtro-ficha", result);
    //$('#filtro-ficha').val($('#filtro-ficha option:first-child').val(0)).trigger('change')
    //$('#filtro-ficha').find('option:first').text('Seleccione Ficha');
}
/*-------------FIN-CARGA-COMBOBOX-FICHA--------------*/


/*---------------CARGA-COMBOBOX-ORDEN-COMPRA---------------*/
function cargaComboOC(result) {
    cargaCBX("filtro-oc", result);
    //$('#filtro-oc').val($('#filtro-oc option:first-child').val(0)).trigger('change')
    //$('#filtro-oc').find('option:first').text('Seleccione Orden de Compra');
}
/*-------------FIN-CARGA-COMBOBOX-ORDEN-COMPRA--------------*/


/*---------------CARGA-COMBOBOX-CURSO---------------*/
function cargaComboCurso(result) {
    cargaCBX("filtro-modalidad", result);
    //$('#filtro-modalidad').val($('#filtro-modalidad option:first-child').val(0)).trigger('change')
    //$('#filtro-curso').find('option:first').text('Seleccione Curso');
}
/*-------------FIN-CARGA-COMBOBOX-CURSO--------------*/


/*-------------CARGA TABLA DETALLE ALUMNO--------------*/