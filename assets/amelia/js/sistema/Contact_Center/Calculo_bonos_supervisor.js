$(document).ready(function () {
	
	var tres = $('#tblResultado').DataTable({
		columnDefs: [
        { 
            targets: [3,4], 
            visible: false 
        }
        ],
		paging: true,
		ordering: true,
		info: false,
		bFilter: false,
		responsive: true,
		searching: true,
		language: {
			"sProcessing": "Procesando...",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sZeroRecords": "No se encontraron resultados",
			"sEmptyTable": "Ningún dato disponible en esta tabla",
			"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix": "",
			"sSearch": "Buscar:",
			"sUrl": "",
			"sInfoThousands": ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [
		{extend: 'copy', text: 'Copiar', exportOptions: { columns: ":visible" } },
		{extend: 'excel', title: 'Detalle Bono Tutor' , exportOptions: { columns: [0,3,4,5] } },
		{extend: 'pdf', title: 'Detalle Bono Tutor', exportOptions: { columns: ":visible" } },

		{
			extend: 'print',
			customize: function (win) {
				$(win.document.body).addClass('white-bg');
				$(win.document.body).css('font-size', '10px');

				$(win.document.body).find('table')
				.addClass('compact')
				.css('font-size', 'inherit');
			},
			exportOptions: { columns: ":visible" }
		}
		],
		pageLength: 10

	});

	var tabla = $('#tblAlumnosporTutor').DataTable({
		
		columnDefs: [
		{ 
			width: "90", 
			targets: [3,7,8] 
		},
		{ 
            targets: [1], 
            visible: false 
        }
		],
		autoWidth:false,
		paging: true,
		ordering: true,
		info: false,
		bFilter: false,
		responsive: true,
		searching: false,
		language: {
			"sProcessing": "Procesando...",
			"sLengthMenu": "Mostrar _MENU_ registros",
			"sZeroRecords": "No se encontraron resultados",
			"sEmptyTable": "Ningún dato disponible en esta tabla",
			"sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			"sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
			"sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
			"sInfoPostFix": "",
			"sSearch": "Buscar:",
			"sUrl": "",
			"sInfoThousands": ",",
			"sLoadingRecords": "Cargando...",
			"oPaginate": {
				"sFirst": "Primero",
				"sLast": "Último",
				"sNext": "Siguiente",
				"sPrevious": "Anterior"
			},
			"oAria": {
				"sSortAscending": ": Activar para ordenar la columna de manera ascendente",
				"sSortDescending": ": Activar para ordenar la columna de manera descendente"
			}
		},
		
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [
		// {extend: 'copy', text: 'Copiar'},
		{extend: 'excel', title: 'Detalle Bono Tutor', exportOptions: { columns: [1,2,3,4,5,6,7,8,9,10,11,12] } },
    {extend: 'pdf', title: 'Detalle Bono Tutor', exportOptions: { columns: ":visible" } },

    {
    	extend: 'print',
    	customize: function (win) {
    		$(win.document.body).addClass('white-bg');
    		$(win.document.body).css('font-size', '10px');

    		$(win.document.body).find('table')
    		.addClass('compact')
    		.css('font-size', 'inherit');
    	},
    	exportOptions: { columns: ":visible" }
    }
    ],
    rowReorder: true,
    pageLength: 10

});

	$(".alumno").select2({
		placeholder: "Seleccione Alumno",
		allowClear: true
	});


	$("#tblResultado").on('click','.detalle',function(){

		var datos=$(this).attr('id').split("@");

		var p_id_tutor=datos[0];
		var p_periodo = datos[1];

		swal({
			title: 'Espere un momento...',
			allowOutsideClick: false,
			allowEscapeKey: false,
			allowEnterKey: false,
			onOpen: () => {
				swal.showLoading()
			}
		});
		setTimeout(function () {

			tabla.clear().draw();

			$.ajax({
				url: "CalculoBonosAjax",
				dataType: "json",
				type: 'POST',
				async: false,
				data: { p_parametro: 4, p_id_tutor: p_id_tutor, p_periodo: p_periodo },
				error: function (jqXHR, textStatus, errorThrown) {
					console.log(jqXHR);
				},
				success: function (result) 
				{	
					$.each(result, function (i, item) 
					{

						tabla.row.add([
							"$"+new Intl.NumberFormat("es-ES").format(item.bono),
							parseInt(item.bono),
							item.tutor,
							item.rut,
							item.nombre,
							item.ficha,
							item.modalidad,
							item.fecha_inicio,
							item.fecha_termino,
							item.costo,
							item.dj,
							item.tiempo_coneccion,
							item.evaluacion+"%"
							]).draw(false);
					});

					swal.hideLoading();
					swal.close();

					$("#modalDetalle").modal('toggle');
				}    
			});

		}, 1000);

		

	});

	
})