$(document).ready(function () {
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
       $('#tbl_tareas_nuevo').DataTable()
                .columns.adjust()
                .responsive.recalc();
    });

    $('#tbl_tareas').DataTable({

        paging: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [17, 18, 19], // column or columns numbers
                orderable: false, // set orderable for selected columns
            },
            {
                targets: [7, 8], // Columnas de Fechas
                visible: false,
            },
            {
                targets:[2,9,10,11],
                width: "15%"
            },
            { responsivePriority: 2, targets: -1 } //Manito siempre Visible
        ],
        info: false,
        bFilter: false,
        responsive: true,
        searching: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        select: {
            style: 'single'
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]}},
            {extend: 'excel', title: 'Tareas Supervisor', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]}},
            {extend: 'pdf', title: 'Tareas Supervisor', orientation: 'landscape', pageSize: 'LEGAL', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]}},

            {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }, exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]}
            }
        ],
        rowReorder: true,
        pageLength: 10

    });

    $('#tbl_tareas_nuevo').DataTable({

        paging: true,
        autoWidth: false,
        columnDefs: [
            {
                targets: [16, 17, 18], // column or columns numbers
                orderable: false, // set orderable for selected columns
            },
            {
                targets: [2,7,8,9,10],
                width: "10%"
            },
            {
                targets: [12,13,14],
                createdCell: function (td, cellData, rowData, row, col) {
                    $(td).css('background-color', '#f5f5f6');
                }
            },
            { responsivePriority: 2, targets: -1 } //Manito siempre Visible
        ],
        info: false,
        bFilter: false,
        responsive: true,
        searching: true,
        language: {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy', text: 'Copiar', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ,15]}},
            {extend: 'excel', title: 'Tareas Supervisor (Nuevos)', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ,15]}},
            {extend: 'pdf', title: 'Tareas Supervisor (Nuevos)', orientation: 'landscape', pageSize: 'LEGAL', exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]}},

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }, exportOptions: {columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ,15]}
            }
        ],
        pageLength: 10

    });

    $("#btnDown").on("click", function () {
        var row = $('#tbl_tareas tbody tr.selected')[0];
        if (typeof (row) === "undefined") {
            swal("Error", "Debe seleccionar un registro de la tabla.", "error");
            return;
        }

        var fila = parseInt($('#tbl_tareas tbody tr.selected td')[0].innerText);
        //var ant=fila-1;
        var sig = fila + 1;

        var tSelect = $('#tarea_' + fila).val();
        var oSelect = $('#orden_' + fila).val();
        var tReemplazo = $('#tarea_' + sig).val();
        var oReemplazo = $('#orden_' + sig).val();

        $.ajax({
            url: "../Supervisor/Tarea_supervisor_cambio",
            async: false,
            cache: false,
            dataType: "json",
            type: "post",
            data: {tselect: tSelect, oselect: oSelect, treemplazo: tReemplazo, oreemplazo: oReemplazo},
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                if (result.Cod == 1) {
                    var table = $('#tbl_tareas').DataTable();
                    var index = table.row(row).index();
                    var page = table.row(row).page();
                    var order = 1;
                    var data1 = table.row(index).data();
                    data1.order += order;
                    var data2 = table.row(index + order).data();
                    data2.order += -order;
                    var posAux = data1[0];
                    data1[0] = data2[0];
                    data2[0] = posAux;
                    table.row(index).data(data2);
                    table.row(index + order).data(data1);
                    table.row(index).select(false);
                    table.row(index + order).select(true);
                    table.page(page).draw(false);
                    $('#tarea_' + fila).val(tReemplazo);
                    $('#tarea_' + sig).val(tSelect);

                    swal("Exito!", "Tarea movida una posición hacia abajo.", "success");

                } else {
                    swal("Error", result.Msg, "warning");
                }
            }
        });
    });

    $("#btnUp").on("click", function () {
        var row = $('#tbl_tareas tbody tr.selected')[0];
        if (typeof (row) === "undefined") {
            swal("Error", "Debe seleccionar un registro de la tabla.", "error");
            return;
        }

        var fila = parseInt($('#tbl_tareas tbody tr.selected td')[0].innerText);
        var ant = fila - 1;

        var tSelect = $('#tarea_' + fila).val();
        var oSelect = $('#orden_' + fila).val();
        var tReemplazo = $('#tarea_' + ant).val();
        var oReemplazo = $('#orden_' + ant).val();

        $.ajax({
            url: "../Supervisor/Tarea_supervisor_cambio",
            async: false,
            cache: false,
            dataType: "json",
            type: "post",
            data: {tselect: tSelect, oselect: oSelect, treemplazo: tReemplazo, oreemplazo: oReemplazo},
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus, errorThrown, jqXHR.responseText);
            },
            success: function (result) {
                if (result.Cod == 1) {
                    var table = $('#tbl_tareas').DataTable();
                    var index = table.row(row).index();
                    var page = table.row(row).page();
                    var order = -1;
                    var data1 = table.row(index).data();
                    data1.order += order;
                    var data2 = table.row(index + order).data();
                    data2.order += -order;
                    var posAux = data1[0];
                    data1[0] = data2[0];
                    data2[0] = posAux;
                    table.row(index).data(data2);
                    table.row(index + order).data(data1);
                    table.row(index).select(false);
                    table.row(index + order).select(true);
                    table.page(page).draw(false);
                    $('#tarea_' + fila).val(tReemplazo);
                    $('#tarea_' + ant).val(tSelect);

                    swal("Exito!", "Tarea movida una posición hacia arriba.", "success");

                } else {
                    swal("Error", result.Msg, "warning");
                }
            }
        });
    });


    $("#btnTop").on("click", function () {
        var row = $('#tbl_tareas tbody tr.selected')[0];
        if (typeof (row) === "undefined") {
            swal("Error", "Debe seleccionar un registro de la tabla.", "error");
            return;
        }

        var fila = parseInt($('#tbl_tareas tbody tr.selected td')[0].innerText);
        var ant = fila - 1;

        console.log(fila);

        var tSelect = $('#tarea_' + fila).val();
        var oSelect = -1;
        var tReemplazo = 0;
        var oReemplazo = 0;



        $.ajax({
            url: "../Supervisor/Tarea_supervisor_cambio",
            async: false,
            cache: false,
            dataType: "json",
            type: "post",
            data: {tselect: tSelect, oselect: oSelect, treemplazo: tReemplazo, oreemplazo: oReemplazo},
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus + errorThrown);
                alert(textStatus + errorThrown + jqXHR.responseText);
                console.log(textStatus + errorThrown + jqXHR.responseText);
            },
            success: function (result) {
                if (result.Cod == 1) {
                    swal("Exito!", "Tarea movida a la primera posición.", "success");
                    document.location.reload(true);
                } else {
                    swal("Error", result.Msg, "warning");
                }
            }
        });
    });


});

//function refeshTable(idTabla, filas) {
//    var table = $('#').DataTable();
//    table.clear().draw();
//    $.each(filas, function (i, v) {
//        table.row.add([
//            "<a><i class='fa fa-search' onclick='myModalficha(" + v.id_ficha + ");'></i></a>",
//            v.num_ficha,
//            v.num_orden_compra,
//            v.empresa,
//            v.nombre_ejecutivo,
//            v.modalidad,
//            v.nombre_curso,
//            v.fecha_inicio,
//            v.fecha_fin,
//            v.costo_sence.split('.')[0] + "%",
//            v.cantidad_alumnos,
//            "<a><i class='fa fa-edit' onclick='myModalClaFe(" + v.clasif_ficha + "," + v.id_ficha + ");'></i></a>   " + (v.clasif_ficha == -1 ? "N/A" : v.clasif_ficha),
//            "<button type='button' class='btn btn-primary btn-xs' onclick='showMdlTodos(" + v.id_ficha + ")'>Asignar</button>"
//        ]).draw(false);
//    });
//}


