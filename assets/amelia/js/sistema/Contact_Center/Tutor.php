<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tutor extends MY_Controller {

    // Variables paramétricas
    private $nombre_item_singular = '';
    private $package = 'back_office/Contact_center';
    private $model = 'Tutor_model';
    private $model2 = 'Comun_model';
    // informacion adicional para notificaciones   
    private $model3 = '../Crm/Notificaciones_model';
    private $controller = 'Tutor';
    private $ind = '';

    function __construct() {
        parent::__construct();
        $this->load->model($this->package . '/' . $this->model, 'modelo');

        $this->load->model($this->package . '/' . $this->model2, 'modelo2');
// informacion adicional para notificaciones  
        $this->load->model($this->package . '/' . $this->model3, 'modelo3');
        $this->load->library('session');
    }

    public function Index() {
        //Redirecciona al Buscador 
        redirect(base_url() . 'back_office/Contact_center/Tutor/Buscador');
    }

    public function Buscador() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Tutor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Principal',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Buscador_alumno.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Principal";
        if (!in_array_r($data['activo'], $this->arr_cortaFuego)) {
            redirect('back_office/Home', 'refresh');
        }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['id_user'] = $this->session->userdata('id_user');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Buscador_alumno_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $datos = array(
            'parametro' => 6,
            'tutor' => $this->session->userdata('id_user'),
            'perfil' => $this->session->userdata('id_perfil')
        );
        $data['Resumen'] = $this->modelo2->Buscar_alumnos_vista($datos);

        $this->load->view('amelia_1/template', $data);
    }

    public function Graficos() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Tutor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Graficos',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert/sweetalert.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/modules/sunburst.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert/sweetalert.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Dashboard_tutor.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Graficos";
        if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/Tutor/Dashboard_tutor_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    // informacion adicional para notificaciones    

    public function getNotificaciones() {
        $arr_sesion = array(
            'user_id' => $this->session->userdata('id_user'),
            'user_perfil' => $this->session->userdata('id_perfil')
        );

        $datos = $this->modelo3->get_arr_listar_notificaciones($arr_sesion);
        return $datos;
    }

    public function Detalle_indicadores() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Tutor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Graficos',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/footable/footable.core.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/footable/footable.all.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Detalle_indicadores_tutor.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Graficos";
if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Detalle_indicadores_tutor_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Tareas() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Tutor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Tareas',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/summernote/summernote.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/summernote/summernote.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Tareas_asignadas_tutor.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Tareas";
        if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Tareas_asignadas_tutor_V.php';
        $data['empresas_cbx'] = $this->modelo->get_data_cbx(array("filtro" => 2, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        // $data['empresas_cbx'] = $this->modelo->get_empresas_tareas(array("id_user" => $datos_menu['user_id'], "id_perfil" => $datos_menu['user_perfil']));
        $data['modalidades_cbx'] = $this->modelo->get_modalidad_cbx();
        $data['costos_cbx'] = $this->modelo->get_costos_cbx();
//        $data['ficha_cbx'] = $this->modelo->get_ficha_oc_cbx(array("p_parametro" => 1, 'p_id_tutor' => $datos_menu['user_id']));
//        $data['oc_cbx'] = $this->modelo->get_ficha_oc_cbx(array("p_parametro" => 2, 'p_id_tutor' => $datos_menu['user_id']));
        $data['ficha_cbx'] = $this->modelo->get_data_cbx(array("filtro" => 3, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['oc_cbx'] = $this->modelo->get_data_cbx(array("filtro" => 4, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['estadisticas_tareas'] = $this->modelo->get_estadisticas_tareas(array("id_tutor" => $datos_menu['user_id']));
        $data['estadisticas_tareas_nuevas'] = $this->modelo->get_estadisticas_tareas(array("id_tutor" => $datos_menu['user_id']));
        $data['tareas'] = $this->modelo->get_tareas(
                array(
                    "empresa" => 0,
                    "modalidad" => 0,
                    "costo" => 0,
                    "ficha" => "",
                    "oc" => "",
                    "curso" => "",
                    "tipo_tarea" => 1,
                    "tutor" => $this->session->userdata('id_user')
        ));
        $data['tareas_nuevas'] = $this->modelo->get_tareas(
                array(
                    "empresa" => 0,
                    "modalidad" => 0,
                    "costo" => 0,
                    "ficha" => "",
                    "oc" => "",
                    "curso" => "",
                    "tipo_tarea" => 2,
                    "tutor" => $this->session->userdata('id_user')
        ));
        $data['plantillas'] = $this->modelo->enviomail_get_plantillas();
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Consulta_bonos() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Tutor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Consulta Bonos',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/highcharts/highcharts.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/highcharts/highcharts.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Calculo_bonos_tutor.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js')
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Consulta Bonos";
        if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Consulta_bono_tutor_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();

        $datos = array('p_parametro' => 1, 'p_id_tutor' => $this->session->userdata('id_user'), 'p_periodo' => '');
        $data['BonoActual'] = $this->modelo->CalculoBono($datos);
        
        $data['id_tutor'] = $this->session->userdata('id_user');

        $this->load->view('amelia_1/template', $data);
    }

    public function Resultados() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Tutor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Resultados',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array
            (
            array('src' => base_url() . 'assets/amelia/css/plugins/c3/c3.min.css'),
            array('src' => base_url() . 'assets/amelia/css/Crm/crm.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/dataTables/datatables.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/select2/select2.min.css'),
            array('src' => base_url() . 'assets/amelia/css/plugins/sweetalert2/sweetalert2.css')
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array
            (
            array('src' => base_url() . 'assets/amelia/js/plugins/chartJs/Chart.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Contact_center/Resultado_tutor.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/select2/select2.full.min.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/datapicker/bootstrap-datepicker.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/dataTables/datatables.min.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/funciones.js'),
            array('src' => base_url() . 'assets/amelia/js/plugins/sweetalert2/sweetalert2.js'),
            array('src' => base_url() . 'assets/amelia/js/sistema/Crm/tooltips/tooltips.js'),
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Resultados";
        if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }

        $data['empresas_cbx'] = $this->modelo->get_data_cbx(array("filtro" => 2, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
//         $data['ficha_cbx'] = $this->modelo->get_ficha_oc_cbx(array("p_parametro" => 1, 'p_id_tutor' => $datos_menu['user_id']));
//        $data['oc_cbx'] = $this->modelo->get_ficha_oc_cbx(array("p_parametro" => 2, 'p_id_tutor' => $datos_menu['user_id']));
        $data['ficha_cbx'] = $this->modelo->get_data_cbx(array("filtro" => 3, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        $data['oc_cbx'] = $this->modelo->get_data_cbx(array("filtro" => 4, "user_id" => $datos_menu['user_id'], "perfil_id" => $datos_menu['user_perfil']));
        

        $fecha = date("Y-m-d");
        $mes = 1;
        $data['resultados'] = $this->modelo->get_resultados_tutor(
                array(
                    "id_empresa" => 0,
                    "num_ficha" => 0,
                    "num_oc" => 0,
                    "fecha_cierre_desde" => date("Y-m-d", strtotime("$fecha -$mes month")),
                    "fecha_cierre_hasta" => date("Y-m-d"),
                    "numero_tabla" => 0,
                    "id_user" => $this->session->userdata('id_user'),
                    "id_perfil" => $this->session->userdata('id_perfil')
                )
        );
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Resultado_tutor_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    public function Envio_mails() {

        ///  array ubicaciones
        $arr_page_breadcrumb = array
            (
            array(
                'href' => base_url() . $this->ind . 'back_office' . '/Contact_center/Tutor',
                'label' => 'Contact Center',
                'icono' => ''
            ),
            array(
                'href' => '#',
                'label' => 'Envío Mails',
                'icono' => 'fa-phone'
            ),
        );

        $arr_theme_css_files = array(
        );

        //  js necesarios para la pagina
        $arr_theme_js_files = array(
        );




        // informacion adicional para la pagina
        $data['page_title'] = '';
        $data['page_title_small'] = '';
        $data['panel_title'] = $this->nombre_item_singular;
        $data['arr_page_breadcrumb'] = $arr_page_breadcrumb;
        $data['script_adicional'] = $arr_theme_js_files;
        $data['style_adicional'] = $arr_theme_css_files;

        ////**** Obligatorio ****** //////

        $datos_menu['user_id'] = $this->session->userdata('id_user');
        $datos_menu['user_perfil'] = $this->session->userdata('id_perfil');
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['activo'] = "Envio Mails";
        if(!in_array_r($data['activo'], $this->arr_cortaFuego)){ redirect('back_office/Home','refresh'); }
        ///  cargo el menu en la session
        //  $data['contenido']              =   $this->modelo->get_arr_listar_ficha($arr_sesion);
        //   Carga Menu dentro de la pagina
        $data['menus'] = $this->session->userdata('menu_usuario');
        ///  fin carga menu
        $data['id_perfil'] = $this->session->userdata('id_perfil');
        $data['page_menu'] = 'dashboard';
        $data['main_content'] = $this->package . '/' . $this->controller . '/Envio_mails_V.php';
        // informacion adicional para notificaciones   
        $data['notificaciones'] = $this->getNotificaciones();
        $this->load->view('amelia_1/template', $data);
    }

    // <editor-fold defaultstate="collapsed" desc="Solicitudes Modulo Resultados">

    public function Cargar_resultados_tutor_filtro() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 0,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );

        $result = $this->modelo->get_resultados_tutor($data);

        echo json_encode($result);
    }

    public function Cargar_resultados_tutor_declaracion_jurada() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 1,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );
        $result = $this->modelo->get_resultados_tutor($data);
        echo json_encode($result);
    }

    public function Cargar_resultados_tutor_conectividad() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 2,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );
        $result = $this->modelo->get_resultados_tutor($data);
        echo json_encode($result);
    }

    public function Cargar_resultados_tutor_finalizados() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 3,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );
        $result = $this->modelo->get_resultados_tutor($data);
        echo json_encode($result);
    }

    public function CalculoBonosAjax() {
        $data = array(
            "p_parametro" => $this->input->post("p_parametro"),
            "p_id_tutor" => $this->input->post("p_id_tutor"),
            "p_periodo" => $this->input->post("p_periodo")
        );

        $result = $this->modelo->CalculoBono($data);

        echo json_encode($result);
    }

    public function Cargar_resultados_tutor_extensiones() {
        $data = array(
            "id_empresa" => $this->input->post("id_empresa"),
            "num_ficha" => $this->input->post("num_ficha"),
            "num_oc" => $this->input->post("num_oc"),
            "fecha_cierre_desde" => $this->input->post("fecha_cierre_desde"),
            "fecha_cierre_hasta" => $this->input->post("fecha_cierre_hasta"),
            "numero_tabla" => 4,
            "id_user" => $this->session->userdata('id_user'),
            "id_perfil" => $this->session->userdata('id_perfil')
        );
        $result = $this->modelo->get_resultados_tutor($data);
        echo json_encode($result);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Solicitudes Tareas">

    public function Cargar_tareas_by_filtro() {
        $data = array(
            "empresa" => $this->input->post('empresa'),
            "modalidad" => $this->input->post('modalidad'),
            "costo" => $this->input->post('costo'),
            "ficha" => $this->input->post('ficha'),
            "oc" => $this->input->post('oc'),
            "curso" => $this->input->post('curso'),
            "tipo_tarea" => 1,
            "tutor" => $this->session->userdata('id_user')
        );

        $res = $this->modelo->get_tareas($data);

        echo json_encode($res);
    }

    public function Cargar_tareas_nuevas_by_filtro() {
        $data = array(
            "empresa" => $this->input->post('empresa'),
            "modalidad" => $this->input->post('modalidad'),
            "costo" => $this->input->post('costo'),
            "ficha" => $this->input->post('ficha'),
            "oc" => $this->input->post('oc'),
            "curso" => $this->input->post('curso'),
            "tipo_tarea" => 2,
            "tutor" => $this->session->userdata('id_user')
        );

        $res = $this->modelo->get_tareas($data);

        echo json_encode($res);
    }

    public function Enviar_mail() {
        $this->load->library('Libreriaemail');
        $idTarea = $this->input->post('id_tarea');
        $datacc = $this->input->post('cc');
        if(strpos($datacc, ',') !== true) $datacc.=',';
        $emailCC = explode(",", $datacc);
        $cc = array();
        for ($i = 0; $i < count($emailCC); $i++) {
            if(strlen(trim($emailCC[$i])) > 0)
            $cc[] = array('email' => trim($emailCC[$i]), 'nombre' => '-');
        }

        $html = $this->input->post('plantilla');
        $email = $this->input->post('email');
        $asunto = $this->input->post('asunto');

        $datos = array(
            "Nombre_Alumno" => $this->input->post('nombre_alumno'),
            "Nombre_Curso" => base64_decode($this->input->post('nombre_curso')),
            "Fecha_Termino_Curso" => $this->input->post('fecha_termino_curso')
        );

        $htmlEnvio = "";
        $htmlEnvio = str_replace("@Nombre_Alumno", $datos['Nombre_Alumno'], $html);
        $htmlEnvio = str_replace("@Nombre_Curso", $datos['Nombre_Curso'], $htmlEnvio);
        $htmlEnvio = str_replace("@Fecha_Termino_Curso", $datos['Fecha_Termino_Curso'], $htmlEnvio);
        $data_email = array(
            "destinatarios" => array(array('email' => $email, 'nombre' => $datos['Nombre_Alumno'])),
            "cc_a" => $cc,
            "asunto" => $asunto,
            "body" => $htmlEnvio,
            "AltBody" => $asunto
        );
     //   $res = $this->libreriaemail->Mailink($data_email["asunto"], $data_email["body"], $data_email["AltBody"], $data_email["destinatarios"], $data_email["cc_a"], array());
        $res = $this->libreriaemail->CallAPISendMailContact($data_email);
        echo $res;
    }

    // </editor-fold>
}

?>