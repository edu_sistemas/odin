var procedencia = "";
$(document).ready(function () {

    $('#btn_nuevo').click(function () {
        location.href = 'Agregar';
    });
    $('#btn_volver').click(function () {
        $('#sec_comentarios').animate({
            height: "toggle"
        }, 300, function () {
            $('#sec_tabla').animate({
                height: "toggle"
            }, 300, function () {

            });
        });
    });
    $('#btn_descartar_programa').click(function () {
        $('#sec_nuevo').animate({
            height: "toggle"
        }, 300, function () {
            $('#sec_tabla').animate({
                height: "toggle"
            }, 300, function () {
                $('#nuevo_comentario').val('');
            });
        });
    });
    $('#btn_descartar_editar_programa').click(function () {
        $('#sec_editar').animate({
            height: "toggle"
        }, 300, function () {
            $('#sec_tabla').animate({
                height: "toggle"
            }, 300, function () {
//$('#nuevo_comentario').val('');
            });
        });
    });
    $('#btn_nuevo_registro').click(function () {
        $('#sec_tabla').animate({
            height: "toggle"
        }, 300, function () {
            $('#sec_nuevo').animate({
                height: "toggle"
            }, 300, function () {

            });
        });
    });
    $('#agregar_programa').click(function () {
        procedencia = "agregar";
        $('#sec_nuevo').animate({
            height: "toggle"
        }, 300, function () {

        });
        $('#sec_agregar_programa').animate({
            height: "toggle"
        }, 300, function () {

        });
    });
    $('#agregar_programa_editar').click(function () {
        procedencia = "editar";
        $('#sec_editar').animate({
            height: "toggle"
        }, 300, function () {

        });
        $('#sec_agregar_programa').animate({
            height: "toggle"
        }, 300, function () {

        });
    });
    $('#btn_descartar_agregar_programa').click(function () {
        if (procedencia == "agregar") {
            $('#sec_agregar_programa').animate({
                height: "toggle"
            }, 300, function () {
                $('#txt_nombre_programa').val('');
            });
            $('#sec_nuevo').animate({
                height: "toggle"
            }, 300, function () {

            });
        }

        if (procedencia == "editar") {
            $('#sec_agregar_programa').animate({
                height: "toggle"
            }, 300, function () {
                $('#txt_nombre_programa').val('');
            });
            $('#sec_editar').animate({
                height: "toggle"
            }, 300, function () {

            });
        }



    });
    $('#btn_guardar_agregar_programa').click(function () {

        if ($('#txt_nombre_programa').val() == '') {
            alerta('Error', 'Debe ingresar el nombre del programa!!!!', 'error');
        } else {
            guardarAgregarPrograma();
        }
    });
    $('#btn_guardar_programa').click(function () {

        if ($('#nuevo_comentario').val() == '' || $('#programas_cbx').val() == '') {
            alerta('Error', 'Debe ingresar todos los campos!!!!', 'error');
        } else {
            agregarProgramaSala();
            $('#nuevo_comentario').val('');
        }
    });
    $('#btn_editar_programa').click(function () {

        if ($('#editar_comentario').val() == '' || $('#programas_cbx_editar').val() == '') {
            alerta('Error', 'Debe ingresar todos los campos!!!!', 'error');
        } else {
            updateRegistroSala();
            $('#nuevo_comentario').val('');
        }
    });
    listarSalas();
});
function cargaTablaUsuario(data) {

    if ($.fn.dataTable.isDataTable('#tbl_docentes')) {
        $('#tbl_docentes').DataTable().destroy();
    }


    $('#tbl_docentes').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_sala"},
            {"mDataProp": "nombre"},
            {"mDataProp": "numero_sala"},
            {"mDataProp": "detalle"},
            {"mDataProp": "capacidad"},
            {"mDataProp": "fecha_registro"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = '<i title="Inactivo" class="fa fa-circle text-danger"></i>';
                    if (o.estado == 0) {
                        html = '  <i title="Activo"  class="fa fa-circle text-navy"></i>';
                    }
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {

                    var y = "Editar/index/" + o.id_sala + "";
                    var z = "../Programa/Listar/index/" + o.id_sala + "";
                    var html = "";
                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-default dropdown-toggle ">Accion<span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu pull-right">';
                    html += '  <li><a onclick="openModalProgramas(' + o.id_sala + ', ' + o.numero_sala + ')">Ver Programas</a></li>';
                    html += '  <li><a href="' + y + '"  >Editar</a></li>';
                    if (o.estado == 0) {
                        html += "<li><a    onclick='EditarSalaEstado(" + o.id_sala + ",1);'>Desactivar</a></li>";
                    } else {
                        html += "<li><a    onclick='EditarSalaEstado(" + o.id_sala + ",0);'>Activar</a></li> ";
                    }
                    html += ' </ul>';
                    html += ' </div>';
                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

function openModalProgramas(id_sala, num_sala) {

    $("#num_sala").text("Sala N° " + num_sala);
    $("#num_sala_hidden").text(num_sala);
    $("#id_sala").text(id_sala);
    $("#hidden_id_sala").val(id_sala);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_sala: id_sala
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_programas").modal("show");
            loadTableProgramasSala(result);
        },
        url: "Listar/getProgramasbySala"
    });
    getProgramasCBX();
}


function getProgramasCBX() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("programas_cbx", result);
            cargaCBX("programas_cbx_editar", result);
        },
        url: "Listar/getProgramasCBX"
    });
}

function loadTableProgramasSala(data) {

    $("#sec_tabla").css({
        'display': 'block'
    });
    $("#sec_comentarios").css({
        'display': 'none'
    });
    $("#sec_nuevo").css({
        'display': 'none'
    });
    $("#sec_agregar_programa").css({
        'display': 'none'
    });
    $("#sec_editar").css({
        'display': 'none'
    });
    var html = "";
    $('#tbl_programas_sala tbody').html(html);
    $.each(data, function (i, item) {
        var EditarRegistro = "'" + item.id_programa_sala + "', '" + item.id_programa + "', '" + item.observacion.replace(new RegExp("\n", "g"), " ") + "'";
        var mostrarComentarios = "'" + item.observacion.replace(new RegExp("\n", "g"), " ") + "', '" + item.programa + "', '" + item.nombre_usuario + "', '" + item.fecha_registro + "'";
        html += '<tr>';
        html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
        html += '<td style="">' + item.programa + '</td>';
        html += '<td style=""><b>' + item.nombre_usuario + '</b>, con fecha ' + item.fecha_registro + '</td>';
        html += '<td style="width: 1px; white-space: nowrap;">';
        html += '<a onclick="mostrarComentarios(' + mostrarComentarios + ')"><i style="color:blue;text-align:center;font-size:130%;" class="fa fa-comments-o"><p style="font-family: Arial, Helvetica, sans-serif;font-size:60%;font-weight:bold;">Comentarios</p></i></a>';
        html += '<a onclick="cargarEditarRegistro(' + EditarRegistro + ')"><i style="color:green;text-align:center;font-size:130%;" class="fa fa-edit"><p style="font-family: Arial, Helvetica, sans-serif;font-size:60%;font-weight:bold;">Editar</p></i></a>  ';
        html += '<a onclick="borrarRegistro(' + item.id_programa_sala + ')"><i style="color:red;text-align:center;font-size:130%;" class="fa fa-trash"><p style="font-family: Arial, Helvetica, sans-serif;font-size:60%;font-weight:bold;">Eliminar</p></i></a></td>';
        html += '</tr>';
    });
    $('#tbl_programas_sala tbody').html(html);
}


function cargarEditarRegistro(id_programa_sala, id_programa, observacion) {

    $("#hidden_id_programa_sala_editar").val(id_programa_sala);
    $('#editar_comentario').text(observacion);
    $('#programas_cbx_editar').val(id_programa);
    $('#sec_tabla').animate({
        height: "toggle"
    }, 300, function () {
        $('#sec_editar').animate({
            height: "toggle"
        }, 300, function () {

        });
    });
}

function borrarRegistro(id_programa_sala) {
    swal({
        title: "¿Estás seguro?",
        text: "El registro se borrará y no se incluirá en las consultas!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Borralo!",
        cancelButtonText: "No, no lo borres porfi!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
            function (isConfirm) {
                if (isConfirm) {

                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {id_programa_sala: id_programa_sala},
                        error: function (jqXHR, textStatus, errorThrown) {
                            // code en caso de error de la ejecucion del ajax
                            console.log(jqXHR, textStatus, errorThrown);
                            // alerta("Sala", errorThrown, "error");
                        },
                        success: function (result) {
                            swal("Eliminado!", "El registro ha sido eliminado.", "success");
                            openModalProgramas($("#id_sala").text(), $("#num_sala_hidden").text())

                        },
                        url: "Listar/desactivarRegistroPrograma"
                    });
                } else {
                    swal("Cancelado", "El registro no se ha borrado. De la que te salvaste!", "error");
                }
            });
}

function mostrarComentarios(comentarios, programa, nombre_usuario, fecha_registro) {
    $('#sec_comentarios p').html('<b>Comentarios de la instalación de <u>' + programa + '</u>, por <u>' + nombre_usuario + '</u>, el <u>' + fecha_registro + '</u>.</b>');
    $('#sec_comentarios_comentario p').text(comentarios);
    $('#sec_tabla').animate({
        height: "toggle"
    }, 300, function () {
        $('#sec_comentarios').animate({
            height: "toggle"
        }, 300, function () {

        });
    });
}

function agregarProgramaSala() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_agregar_programa_sala').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Programa", errorThrown, "error");
        },
        success: function (result) {

            openModalProgramas($("#id_sala").text(), $("#num_sala_hidden").text());
        },
        url: "Listar/AgregarProgramaSala"
    });
}

function guardarAgregarPrograma() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_agregar_programa').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Programa", errorThrown, "error");
        },
        success: function (result) {

            getProgramasCBX();
            if (procedencia == "agregar") {
                $('#sec_agregar_programa').animate({
                    height: "toggle"
                }, 300, function () {
                    $('#txt_nombre_programa').val('');
                });
                $('#sec_nuevo').animate({
                    height: "toggle"
                }, 300, function () {

                });
            }

            if (procedencia == "editar") {
                $('#sec_agregar_programa').animate({
                    height: "toggle"
                }, 300, function () {
                    $('#txt_nombre_programa').val('');
                });
                $('#sec_editar').animate({
                    height: "toggle"
                }, 300, function () {

                });
            }


        },
        url: "Listar/AgregarPrograma"
    });
}

function listarSalas() {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaUsuario(result);
        },
        url: "Listar/listar"
    });
}

function EditarSalaEstado(id_sala, tipo_estado) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id: id_sala, estado: tipo_estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Sala", errorThrown, "error");
        },
        success: function (result) {

            listarSalas();
        },
        url: "Listar/CambiarEstadoSala"
    });
}

function updateRegistroSala() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_editar_programa_sala').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Programa", errorThrown, "error");
        },
        success: function (result) {

            openModalProgramas($("#id_sala").text(), $("#num_sala_hidden").text());
        },
        url: "Listar/UpdateProgramaSala"
    });
}
