// JavaScript Document
$(document).ready(function () {

    $("#btn_registrar").click(function () {
		
        if (validarRequeridoForm($("#frm_docente_registro"))) {
            RegistraSala();
        }
    });   
    
});


function RegistraSala() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_docente_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            alerta('Sala', errorThrown, 'error');
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar'
            }).then((result) => {
                location.reload();
            })

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }

        },
        url: "../EditarSala"
    });
} 

