$(document).ready(function () {

    llamaDatosHoldings();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_buscar_holding").click(function () {
        llamaDatosHoldings();
    });

    // $("#cbx_holding").select2({
    //     placeholder: "Todos",
    //     allowClear: true
    // });

    //llamaDatosControles();
});



function llamaDatosHoldings() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaHoldings(result);
        },
        url: "Listar/buscarHolding"
    });
}


// function llamaDatosControles() {
//     $.ajax({
//         async: false,
//         cache: false,
//         dataType: "json",
//         type: 'GET',
//         //  data: $('#frm').serialize(),
//         error: function (jqXHR, textStatus, errorThrown) {
//             // code en caso de error de la ejecucion del ajax
//             console.log(textStatus + errorThrown);
//         },
//         success: function (result) {

//             cargaCombo(result);
//         },
//         url: "Listar/getHoldingCBX"
//     });
// }


function cargaTablaHoldings(data) {

    if ($.fn.dataTable.isDataTable('#tbl_holding')) {
        $('#tbl_holding').DataTable().destroy();
    }

    $('#tbl_holding').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_holding"},
            {"mDataProp": "nombre_holding"},
            {"mDataProp": "descripcion_holding"},
            {"mDataProp": "fecha_registro"},
            {"mDataProp": "estado"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var x = "'Editar/index/" + o.id_holding + "'";
                    return '<button class="btn-primary"  onclick="location.href=' + x + '">Editar</button>';
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });



}


// function cargaCombo(midata) {

//     $("#cbx_holding").select2({
//         placeholder: "Todos",
//         allowClear: true,
//         data: midata
//     });


// }
