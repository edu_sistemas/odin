$(document).ready(function () {


    $("#btn_editar_holding").click(function () {
        if (validarRequeridoForm($("#frm_holding_registro"))) {
            Registra_holding();
        }
    });


    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });


    contarCaracteres("descripcion_holding", "text-out", 200);

    $("#estado").val("1");



    $("#estado_holding").click(function () {
        var shekx = $('#estado_holding').is(':checked');

        if (shekx) {
            $("#estado").val("1");
            $("#estado_label").html("Activado");
        } else {
            $("#estado").val("0");
            $("#estado_label").html("Desactivado");
        }

    });

    var estado = false;

    if ($("#estado").val() == 1) {
        estado = true;

    }


    $("#estado_holding").prop("checked", estado);

});

// function checkIt() {
//     if(document.getElementById("estado_holding").checked == true){
//         document.getElementById("estado_holding").checked = false ;
//         document.getElementById("estado").value = "0";
//     }else{
//         document.getElementById("estado_holding").checked = true ;
//         document.getElementById("estado").value = "1";
//     }



// }


function Registra_holding() {

    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_holding_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);

            alerta('Holding', jqXHR.responseText, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            // if (  result[0].reset==1){
            //       $('#btn_back').trigger("click");
            // }
        },
        url: "Agregar/Registra_holding"
    });




}