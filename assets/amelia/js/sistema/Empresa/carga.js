$(document).ready(function () {

    $('#btn_aprobar_carga').prop("disabled", true);
    $('#btn_cancelar').prop("disabled", true);

    $("#btn_carga").click(function () {
        if ($("#file_carga_empresa").val() != "") {

            send_file();
        } else {
            alerta('Empresa', "Favor Seleccione un archivo de carga válido", 'warning');
        }
    });

    $("#btn_cancelar").click(function () {
        swal({
            title: "Estas Seguro?",
            text: "Eliminará todos los registros previamente cargados",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy Seguro!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {

            if (isConfirm) {

                limpia_carga();
                mostrar_lista_carga_empresa();
            }

        });

    });



    $("#btn_aprobar_carga").click(function () {

        swal({
            title: "Estas Seguro?",
            text: "Solo se cargaran los registros que no contiene errores",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, Estoy Seguro!",
            cancelButtonText: "Cancelar!",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                aceptar_carga();
            }

        });
    });





});

/*
 window.onunload = finalizar();
 
 
 function finalizar() {
 
 
 return false;
 }
 */



function send_file() {

    $('#btn_carga').prop("disabled", true);
    // $('#file_carga_empresa').prop("disabled", true);


    limpia_carga();

    $('#btn_carga').html('<i class="fa fa-upload"></i> Cargando..');
    var l = $('#btn_carga').ladda();

    l.ladda('start');

    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_carga_empresa")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Empresa', jqXHR.responseText, 'error');

            // console.log(jqXHR.responseText);


        },
        success: function (result) {
            l.ladda('stop');
            $('#btn_carga').prop("disabled", false);
            $('#btn_carga').html('<i class="fa fa-upload"></i> Subir ');
            if (result.valido) {
                mostrar_lista_carga_empresa();
                $('#btn_aprobar_carga').prop("disabled", false);
                $('#btn_cancelar').prop("disabled", false);
                $('#file_carga_empresa').prop("disabled", false);
            } else {
                alerta('Empresa', result.mensaje, 'error');
            }
        },
        url: 'Carga/cargar_archivo_datos_empresa'
    });
}


function limpia_carga()
{

    /*
     
     Descripcion : Limpia la tabla temporal antes de cargar el archivo excel
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {

        },
        success: function (result) {

        },
        url: "Carga/limpiar_carga_anterior"
    });
}

function  mostrar_lista_carga_empresa() {

    /*Muestra el docuemento cargado con mensaje dependiendo del tipo de validacion */

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Empresa', jqXHR.responseText, 'error');
        },
        success: function (result) {

            cagar_tabla_temporal(result);
        },
        url: "Carga/listar_carga_empresa"
    });
}


function cagar_tabla_temporal(data) {

    if ($.fn.dataTable.isDataTable('#tbl_empresa_carga')) {
        $('#tbl_empresa_carga').DataTable().destroy();
    }

    $('#tbl_empresa_carga').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut_empresa"},
            {"mDataProp": "razon_social"},
            {"mDataProp": "giro"},
            {"mDataProp": "direccion"},
            {"mDataProp": "descripcion"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    switch (o.estado) {
                        case '0':
                            html = '<td><div class="infont col-md-3 col-sm-4"><a><i style="color:red" class="fa fa-times"></i></a></div>' + o.mensaje + '</td>';
                            break;
                        case '1':
                            html = ' <td><div class="infont col-md-3 col-sm-4"><a><i style="color:green" class="fa fa-check"></i></a></div>' + o.mensaje + '</td>';
                            break;
                        case '2':
                            html = '<td><div class="infont col-md-3 col-sm-4"><a><i   class="fa fa-warning"></i></a></div>' + o.mensaje + '</td>';
                            break;
                        case '3':
                            html = '<td><div class="infont col-md-3 col-sm-4"><a><i   class="fa fa-warning"></i></a></div>' + o.mensaje + '</td>';
                            break;
                    }


                    return html;
                }
            }
        ],
        pageLength: 20,
        responsive: true

    });
}


function aceptar_carga() {

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'json',
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            alerta('Empresa', jqXHR.responseText, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                limpia_carga();
                mostrar_lista_carga_empresa();
            }

        },
        url: "Carga/aceptar_carga_empresa"
    });




}


 