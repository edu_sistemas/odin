$(document).ready(function () {
    recargaLaPagina(); 
    cargaContactos();
    cargaDirecciones();
    cargaVentas();

    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });

});

function cargaCombo(midata, id_cbx, id_selected) {

    $("#" + id_cbx).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + id_cbx + " option[value='0']").remove();
    $("#" + id_cbx + "").val($("#" + id_selected).val()).trigger("change");


}

function cargaContactos() {
    var id_empresa = $("#id_empresa").val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_empresa: id_empresa},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Empresa', jqXHR.responseText, 'error');
        },
        success: function (result) {
            cargaTablaContactos(result);
        },
        url: "../verDatosContacto"
    });

}

function cargaTablaContactos(data) {
    var html = "";
    $('#tbl_contacto_empresa tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr>';
        html += '<td>' + item.nombre_contacto + '</td>';
        if (item.descripcion == null) {
            html += '<td>' + '--' + '</td>';
        } else {
            html += '<td>' + item.descripcion + '</td>';
        }
        html += '<td><a href="mailto:' + item.correo_contacto + '">' + item.correo_contacto + '<a></td>';
        html += '<td>' + item.telefono_contacto + '</td>';
        html += '</tr>';
    });

    $('#tbl_contacto_empresa tbody').html(html);
}

function cargaDirecciones() {
    var id_empresa = $("#id_empresa").val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_empresa: id_empresa},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Empresa', jqXHR.responseText, 'error');
        },
        success: function (result) {
            cargaTablaDirecciones(result);
        },
        url: "../verDatosDireccion"
    });
}

function cargaTablaDirecciones(data) {
    var html = "";
    $('#tbl_direccion_empresa tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr>';
        html += '<td>' + item.direccion + '</td>';
        if (item.descripcion == null) {
            html += '<td>' + '--' + '</td>';
        } else {
            html += '<td>' + item.descripcion + '</td>';
        }
        var res = item.direccion.replace(" ", "+");
        html += '<td><a href="https://www.google.com/maps?q=' + res + '" target="_new">Ver en Google Maps<a></td>';
        html += '</tr>';
    });

    $('#tbl_direccion_empresa tbody').html(html);
}

function cargaVentas() {

    var id_empresa = $("#id_empresa").val();

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_empresa: id_empresa},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Empresa', jqXHR.responseText, 'error');
        },
        success: function (result) {
            cargaGraficoVentas(result);
        },
        url: "../verVentasEmpresa"
    });

}

function cargaGraficoVentas(data) {
    var fechas = [];
    var montos = [];
    $.each(data, function (i, item) {
        fechas.push(item.fecha_inicio);
        montos.push(item.total_venta);
    });
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: fechas,
            datasets: [{
                    label: 'Monto en ventas',
                    data: montos,
                    fill: false,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 0, 0, 1)',
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',

                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
        },
        options: {
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }
    });

    if ($.fn.dataTable.isDataTable('#tbl_ventas_empresa')) {
        $('#tbl_ventas_empresa').DataTable().destroy();
    }

    $('#tbl_ventas_empresa').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "Curso"},
            {"mDataProp": "categoria"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "Valor total venta"}
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

}