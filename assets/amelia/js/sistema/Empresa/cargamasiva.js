

$(document).ready(function () {

    $('#subidaMasiva_btn').attr('disabled', true);



    $("#subidaMasiva_btn").click(function () {
        subidaMasivaEmpresa();
    });


    $(".fileinput-remove-button").click(function () {
        $('#subidaMasiva_btn').attr('disabled', true);
    });



    $("#btn_cancelar").click(function () {
        Eliminar_empresas_tbl_temporal();
    });


    $("#btn_registrar_fin").click(function () {
        subidaMasivaEmpresa_fin();
    });



});


function habilitar_boton_subir() {

    $('#subidaMasiva_btn').attr('disabled', false);

}



function subidaMasivaEmpresa() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     
     */
    var resultado = "";


    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($('#subidaMasiva')[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(" subidaMasivaEmpresa textStatus : " + textStatus + "  errorThrown : " + errorThrown);
            //  alert(errorThrown+'asdasd');

        },
        success: function (result) {
            //  alert(result);
            //   console.log("valor   : " +result);
            if (result.contador != 0) {
                // console.log("valor contador : " +result.contador);

                mostrar_lista_carga_empresa();
            }
            //  alert(resultado);
        },
        url: "CargaMasiva/Subir_empresa"
    });


    /*  if (resultado!="Registro Exitoso"){
     alerta("Carga Masiva de archivos realizada con éxito",resultado,"success");
     $("#btn_reset").trigger("click");
     }else{
     alerta("Carga Masiva de archivos realizada con errores",resultado,"error");
     $("#btn_reset").trigger("click");
     }*/
}


function  mostrar_lista_carga_empresa() {



    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        //  data:  new FormData($('#subidaMasiva')[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log("  v mostrar_lista_carga_empresa textStatus : " + textStatus + "  errorThrown : " + errorThrown);
            //  alert(errorThrown+'asdasd');

        },
        success: function (result) {
            //  alert(result);

            cagar_tabla_decarga_temporal(result);

            //  alert(resultado);
        },
        url: "CargaMasiva/listar_carga_empresa"
    });


}

function  cagar_tabla_decarga_temporal(datos) {


    var html = "";
    var status = "";

    $("#tbl_detalle_carga tbody").html(html);

    $.each(datos, function (i, item) {

        status = '<i class="fa fa-times text" style="color:red;"></i>';

        if (item.estado == 2) {
            status = '<i class="fa fa-exclamation-triangle" style="color:rgb(255,204,0)"></i>';
        }


        if (item.estado == 1) {

            if (item.estado == 0) {
                status = '<i class="fa fa-times text" style="color:red;"></i>';
            } else {
                status = '<i class="fa fa-exclamation-triangle" style="color:rgb(255,204,0)"></i>';
            }


            status = '<i class="fa fa-check text-navy"></i>';


        }


        html += '<tr>';
        html += '<td>' + item.nombre + '</td>';
        html += '<td>' + item.mensaje + '</td>';
        html += '<td>' + status + '</td>';
        html += '</tr>';


    });


    $("#tbl_detalle_carga tbody").html(html);
    $("#myModal").modal('toggle');

}

function Eliminar_empresas_tbl_temporal() {

    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'text',
        type: 'POST',
        //  data:  new FormData($('#subidaMasiva')[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  "  v mostrar_lista_carga_empresa textStatus : " + textStatus + "  errorThrown : " + errorThrown );
            //  alert(errorThrown+'asdasd');

        },
        success: function (result) {
            //  alert(result);

            resultado = result[0].respuesta;

            //  alert(resultado);
        },
        url: "CargaMasiva/Eliminar_empresas_tbl_temporal"
    });

    if (resultado != "Registro Exitoso") {

        alerta("Empresa", resultado, "error");

    } else {

        alerta("Empresa", resultado, "success");
        $("#btn_reset").trigger("click");

    }



}


function subidaMasivaEmpresa_fin() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     
     */
    var resultado = "";


    $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        //data:  new FormData($('#myModal')[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(" subidaMasivaEmpresa textStatus : " + textStatus + "  errorThrown : " + errorThrown);
            //  alert(errorThrown+'asdasd');

        },
        success: function (result) {
            //  alert(result);
            //   console.log("valor   : " +result);
            resultado = result[0].respuesta;
            // console.log("valor contador : " +result.contador);



            //  alert(resultado);
        },
        url: "CargaMasiva/Subir_empresa_final"
    });


    if (resultado != "Registro Exitoso") {
        alerta("Carga Masiva de archivos realizada con éxito", resultado, "success");
        $("#btn_reset").trigger("click");
    } else {
        alerta("Carga Masiva de archivos realizada con errores", resultado, "error");
        $("#btn_reset").trigger("click");

    }
}


//btn btn-white
