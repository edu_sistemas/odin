$(document).ready(function () {
    var map;
    recargaLaPagina();
    llamaDatosEmpresas();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });
    
    $("#btn_nuevo_contacto").click(function () {  
        $("#frm_agregar_contacto_empresa")[0].reset();    
        $('#contactos_empresa').animate({
            height: "toggle"
        }, 300, function () {
            $('#agregar_contacto_empresa').animate({
                height: "toggle"
            }, 300, function () {
            });
        });
    });
    $("#btn_cancelar_contacto").click(function () {      
        $('#agregar_contacto_empresa').animate({
            height: "toggle"
        }, 300, function () {
            $('#contactos_empresa').animate({
                height: "toggle"
            }, 300, function () {
                $("#frm_agregar_contacto_empresa")[0].reset();
            });
        });
    });
    $("#btn_cancelar_contacto_editar").click(function () {      
        $('#editar_contacto_empresa').animate({
            height: "toggle"
        }, 300, function () {
            $('#contactos_empresa').animate({
                height: "toggle"
            }, 300, function () {
                
            });
        });
    });

    $("#btn_carga").click(function () {
        location.href = 'Carga';
    });


    $("#btn_buscar_empresa").click(function () {
        llamaDatosEmpresas();
    });

   /* $("#cbx_holding").select2({
        placeholder: "Todos",
        allowClear: true
    });
*/
    llamaDatosControles();
    
    cargaComboTipoContacto();
    cargaComboNivelContacto();


    $("#frm_agregar_contacto_empresa").submit(function () {
            RegistarNuevoContacto();
            event.preventDefault();
    });

    $("#frm_editar_contacto_empresa").submit(function () {
        actualizarContacto();
        event.preventDefault();
    });

    $("#btn_cancelar_contacto").click(function () {
        $("#div_frm_add").css("display", "none");
        $("#div_tbl_contacto_empresa").css("display", "block");
        $("#btn_nuevo_contacto").css("display", "block");
        $("#txt_id_contacto_empresa").val('');
        $("#txt_id_empresa").val('');
        $("#tipo_contacto_cbx").val('').trigger('change');
        $("#txt_cargo_contacto").val('');
        $("#txt_departamento_contacto").val('');
        $("#nivel_contacto_cbx").val('').trigger('change');
        $("#txt_nombre_contacto").val('');
        $("#txt_correo_contacto").val('');
        $("#txt_telefono").val('');
    });

    $("#btn_cancelar_contacto_edit").click(function () {
        
        $("#div_frm_add_edit").css("display", "none");
        $("#div_tbl_contacto_empresa").css("display", "block");
        $("#txt_id_contacto_empresa_edit").val('');
        $("#txt_id_empresa_edit").val('');
        $("#txt_nombre_contacto_edit").val('');
        $("#tipo_contacto_cbx").val('').trigger('change');
        $("#txt_cargo_contacto").val('');
        $("#txt_departamento_contacto").val('');
        $("#nivel_contacto_cbx_edit").val('').trigger('change');
        $("#txt_correo_contacto_edit").val('');
        $("#txt_telefono_edit").val('');
    });

    $("#agregar_direccion").click(function () {
        $("#div_tbl_direccion_empresa").animate({
            left: "+=50",
            height: "toggle"
        }, 500, function () {
            // Animation complete.
            $("#div_agregar_direccion").animate({
                left: "+=50",
                height: "toggle"
            }, 500, function () {
                // Animation complete.
                $.ajax({
                    async: false,
                    cache: false,
                    dataType: "text",
                    type: 'POST',
                    error: function (jqXHR, textStatus, errorThrown) {
                        // code en caso de error de la ejecucion del ajax
                        console.log(textStatus + errorThrown);
                    },
                    success: function (result) {
                        cargaSelectTipo(result);
                    },
                    url: "Listar/tipoEmpresa"
                });
            });
        });
    });

    $("#cancelar_direccion").click(function () {
        $("#div_agregar_direccion").animate({
            left: "+=50",
            height: "toggle"
        }, 500, function () {
            // Animation complete.
            DireccionesEmpresa($('#id_empresa_direccion').val(), $('#direc').val());
            $("#div_tbl_direccion_empresa").animate({
                left: "+=50",
                height: "toggle"
            }, 500, function () {
                // Animation complete.

            });
        });
    });

    $("#cancelar_mapa").click(function () {
        $("#div_mapa_ql").animate({
            left: "+=50",
            height: "toggle"
        }, 500, function () {
            // Animation complete.
            $("#div_tbl_direccion_empresa").animate({
                left: "+=50",
                height: "toggle"
            }, 500, function () {
                // Animation complete.
            });
        });
    });

    $('#Agregar_form').click(function () {
        if ($('#direccion').val() == '') {
            $('#direccion').focus();
            return false;
        }
        if ($('#tipo').val() == 0) {
            $('#tipo').focus();
            return false;
        }
        $.ajax({
            async: false,
            cache: false,
            dataType: "text",
            type: 'POST',
            data: {direccion: $('#direccion').val(), tipo: $('#tipo').val(), fecha: $('#fecha').val(), id_empresa: $('#id_empresa_direccion').val()},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(textStatus + errorThrown);
            },
            success: function (result) {
                alerta('success', 'asdasdasdasd');
                swal("Direccion Agregada!", "Clic para continuar!", "success");
                $('#direccion').val('');
                $('#tipo').val('Seleccionar');
            },
            url: "Listar/agregarDireccion"
        });

    });

});

function RegistarNuevoContacto() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_agregar_contacto_empresa').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Nuevo Contacto', errorThrown, 'error');
        },
        success: function (result) {

            if (result[0].reset == 1) {
                swal({
                    title: result[0].titulo,
                    text: result[0].mensaje,
                    type: result[0].tipo_alerta,
                    showCancelButton: false,
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: true,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                      
                        $('#agregar_contacto_empresa').animate({
                            height: "toggle"
                        }, 300, function () {
                            $('#contactos_empresa').animate({
                                height: "toggle"
                            }, 300, function () {
                                $("#frm_agregar_contacto_empresa")[0].reset();
                                $('#tipo_contacto_empresa').empty();
                                $('#nivel_contacto_empresa').empty();
                                getContactosEmpresa($("#id_empresa").val());
                            });
                        });
                    });
            }
        },
        url: "Listar/guardarDatosContacto"
    });


}

function cargaComboNivelContacto() {
    console.log('ajax ');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombocbx("nivel_contacto_empresa", result);
            cargaCombocbx("nivel_contacto_empresa_editar", result);
        },
        url: "Listar/getNivelContactoCBX"
    });
}


function llamaDatosEmpresas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaEmpresas(result);
        },
        url: "Listar/buscarEmpresa"
    });
}


function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result);

           // cargaCombocbx("cbx_holding", result);
        },
        url: "Listar/getHoldingCBX"
    });
}

function cargaComboTipoContacto() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombocbx("tipo_contacto_empresa", result);
            cargaCombocbx("tipo_contacto_empresa_editar", result);
        },
        url: "Listar/getTipoContactoCBX"
    });

}
function cargaTablaEmpresas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_empresa')) {
        $('#tbl_empresa').DataTable().destroy();
    }

    $('#tbl_empresa').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_empresa"},
            {"mDataProp": "rut_empresa"},
            {"mDataProp": "razon_social"},
            {"mDataProp": "giro_empresa"},
            {"mDataProp": "direccion_empresa"},
            {"mDataProp": "nombre_holding"},
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    o.id_empresa
                    html += "  <button class='btn btn-primary' title='Ventas Empresa' onclick='VentaEmpresa(" + o.id_empresa + ");' type='button'>&nbsp;&nbsp;<i class='fa fa-usd'></i>&nbsp;&nbsp;&nbsp;</button>";
                    return html;
                }
            },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    o.id_empresa
                    html += "  <button class='btn btn-success btn-sm' title='Contacto Empresa' onclick='getContactosEmpresa(" + o.id_empresa + ");' type='button'>Ver Contactos</button>";
                    return html;
                }
            },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var param = '"' + o.razon_social + '"';
                    html += "  <button class='btn btn-primary' title='Direccion Empresa' onclick='DireccionesEmpresa(" + o.id_empresa + "," + param + ");' type='button'>&nbsp;&nbsp;<i class='fa fa-building'></i>&nbsp;&nbsp;&nbsp;</button>";
                    return html;
                }
            },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {

                    var html = "";
                    html = '<i title="Inactivo" class="fa fa-circle text-danger"></i>';
                    if (o.estado == 1) {
                        html = '  <i title="Activo"  class="fa fa-circle text-navy"></i>';
                    }
                    return html;
                }
            },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = '';
                    var x = "'Editar/index/" + o.id_empresa + "'";
                    var y = "'Detalle/index/" + o.id_empresa + "'";
                    html += '<button class="btn btn-primary btn-xs"  onclick="location.href=' + x + '">Editar</button>';
                    html += '<button class="btn btn-primary btn-xs"  onclick="location.href=' + y + '">Info. Detallada</button>';
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

function getContactosEmpresa(id_empresa) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_empresa: id_empresa},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {
            if (result[0].v_contador > 0) {
                $("#modal_contactos_empresa .modal-title").text(result[0].nombre_empresa);
                $("#id_empresa").val(result[0].id_empresa);
                var contenido = "";
                $("#tbl_contactos_empresa tbody").html('');
                $.each(result, function (i, item) {
                    contenido += '<tr><td>';
                    contenido += item.id_contacto_empresa;
                    contenido += '</td><td>';
                    contenido += item.nombre_contacto;
                    contenido += '</td><td>';
                    contenido += item.cargo_contacto;
                    contenido += '</td><td>';
                    contenido += item.departamento_contacto;
                    contenido += '</td><td>';
                    contenido += item.correo_contacto;
                    contenido += '</td><td>';
                    contenido += item.telefono_contacto;
                    contenido += '</td><td>';
                    contenido += '<div class="btn-group">';
                    contenido += '<button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">Opciones <span class="caret"></span></button>';
                    contenido += '<ul class="dropdown-menu">';
                    contenido += '<li><a href="#" onclick="editarContactoEmpresa(' + item.id_contacto_empresa+')">Editar</a></li>';
                    contenido += '<li><a href="#" onclick="eliminarContactoEmpresa(' + item.id_contacto_empresa +')">Eliminar</a></li>';
                    contenido += '</ul>';
                    contenido += '</div>';
                    contenido += '</td></tr>';
                });
                $("#tbl_contactos_empresa tbody").html(contenido);
                $('#contactos_empresa').css('display', 'block');
                $('#agregar_contacto_empresa').css('display', 'none');
                $('#editar_contacto_empresa').css('display', 'none');
                $("#modal_contactos_empresa").modal('show');
            } else {
                $("#modal_contactos_empresa .modal-title").text(result[0].nombre_empresa);
                $("#id_empresa").val(result[0].id_empresa);
                $("#tbl_contactos_empresa tbody").html('');
                $('#contactos_empresa').css('display', 'block');
                $('#agregar_contacto_empresa').css('display', 'none');
                $('#editar_contacto_empresa').css('display', 'none');
                $("#modal_contactos_empresa").modal('show');
            }
        },
        url: "Listar/getContactosEmpresa"
    });
}

function editarContactoEmpresa(id_contacto_empresa) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto_empresa: id_contacto_empresa,
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {

            $("#id_contacto").val(result[0].id_contacto_empresa);
            $("#nombre_contacto_empresa_editar").val(result[0].nombre_contacto);
            $('#tipo_contacto_empresa_editar').val(result[0].id_tipo_contacto_empresa).trigger('change');
            $("#cargo_contacto_empresa_editar").val(result[0].cargo_contacto);
            $("#departamento_contacto_empresa_editar").val(result[0].departamento_contacto);
            $("#nivel_contacto_empresa_editar").val(result[0].id_nivel_contacto).trigger('change');
            $("#correo_contacto_empresa_editar").val(result[0].correo_contacto);
            $("#telefono_contacto_empresa_editar").val(result[0].telefono_contacto);

            $('#contactos_empresa').animate({
                height: "toggle"
            }, 300, function () {
                $('#editar_contacto_empresa').animate({
                    height: "toggle"
                }, 300, function () {
                });
            });
        },
        url: "Listar/getContactoEmpresa"
    });
}

function actualizarContacto() {
    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_editar_contacto_empresa').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Actualizar Contacto', errorThrown, 'error');
        },
        success: function (result) {

            //  alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                swal({
                    title: result[0].titulo,
                    text: result[0].mensaje,
                    type: result[0].tipo_alerta,
                    showCancelButton: false,
                    confirmButtonText: "Aceptar",
                    closeOnConfirm: true,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                        
                        $('#editar_contacto_empresa').animate({
                            height: "toggle"
                        }, 300, function () {
                            $('#contactos_empresa').animate({
                                height: "toggle"
                            }, 300, function () {
                                getContactosEmpresa($("#id_empresa").val());
                            });
                        });
                    });

            }
            if (result[0].reset == 0) {

                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            }
        },
        url: "Listar/actualizarContacto"
    });


}

function cargaCombo(midata) {

    $("#cbx_holding").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });


}


function cargaCombocbx(id, midata) {
    console.log(midata);
    $("#" + id).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });


}

function cargaSelectTipo(midata) {
    $('.selector-pais select').html(midata).fadeIn();
}

function EditarEmpresaEstado(id_empresa, tipo_estado) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id: id_empresa, estado: tipo_estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {

            buscarFiltro();
        },
        url: "Listar/CambiarEstadoAlumno"
    });

}

function VentaEmpresa(id_empresa) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_empresa: id_empresa},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {
            tblCargaVentaEmpresa(result);
            $("#modal_venta_empresa").modal("show");
        },
        url: "Listar/verVentasEmpresa"
    });
}

function ContactoEmpresa(id, nombre_contacto) {
    $("#logo_empresa_modal").css({width: "250px"});
    $("#modal_nombre_contacto").text(nombre_contacto);
    $("#txt_id_empresa").val(id);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_empresa: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {
            console.log(result);
            cargaDatosContactoEmpresa(result);
            $("#modal_contacto_alumno").modal("show");
            $("#div_tbl_contacto_empresa").css({display: "block"});
            $("#div_frm_add").css({display: "none"});
            $("#div_frm_add_edit").css({display: "none"});

        },
        url: "Listar/verDatosContacto"
    });
}

function DireccionesEmpresa(id, nombre) {
    $('#id_empresa_direccion').val(id);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_empresa: id},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {
            cargaDatosDireccionEmpresa(result);
            $('#direc').html(nombre);
            $("#lista_direcciones").modal("show");
        },
        url: "Listar/verDatosDireccion"
    });

}



function cargaDatosContactoEmpresa(data) {

    console.log(data);
    var html = "";
   // cargaComboTipoContacto();
    //cargaComboNivelContacto();
    $('#tbl_contacto_empresa tbody').html(html);
    $.each(data, function (i, item) {
        html += '        <tr>';
        html += '        <td>' + item.nombre_contacto + '</td> ';
        if (item.descripcion == null) {
            html += '        <td>' + '--' + '</td> ';
        } else {
            html += '        <td>' + item.descripcion + '</td> ';
        }
        html += '        <td>' + item.correo_contacto + '</td> ';
        html += '        <td>' + item.telefono_contacto + '</td> ';
        html += '        <td>' + item.fecha_registro + '</td> ';
        html += '        <td>';

        if (item.estado_contacto == 1) {
            html += '<button class="btn btn btn-primary readonly btn-circle"  title="Activo" type="button"><i class="fa fa-check"></i></button>';
        } else {
            html += '<button class="btn btn-danger btn-circle" readonly title="Inactivo" type="button"><i class="fa fa-times"></i></button>';
        }

        html += '</td>';
        html += '<td>';

        if (item.estado_contacto == 1) {
            html += '<button class="btn btn-warning btn-circle btn-sm" type="button" title="desactivar" onclick="desactivarContactoEmpresa(' + item.id_contacto_empresa + ',0)" ><i class="fa fa-minus-circle"></i></button>';
        } else {
            html += '<button class="btn btn-primary btn-circle btn-sm" type="button" title="Activar" onclick="desactivarContactoEmpresa(' + item.id_contacto_empresa + ',1)" ><i class="fa fa-minus-circle"></i></button>';
        }

        var datos = "";
        console.log(item);

        html += '<button onclick="ubicarCamposUpdateContactoEmpresa(' + item.id_contacto_empresa + ')" class="btn btn-info btn-circle btn-sm" type="button" title="Editar" ><i class="fa fa-edit"></i>  </button>';
        html += '<button   onclick="eliminarContactoEmpresa(' + item.id_contacto_empresa + ')" class="btn btn-danger btn-circle btn-sm" type="button" title="Elimiar" ><i class="fa fa-times-circle-o"></i>  </button>';
        html += '</td> ';
        html += '</tr>';
    });

    $('#tbl_contacto_empresa tbody').html(html);
}


function cargaDatosDireccionEmpresa(data) {
    if ($.fn.dataTable.isDataTable('#tbl_direcciones_empresa')) {
        $('#tbl_direcciones_empresa').DataTable().destroy();
    }

    $('#tbl_direcciones_empresa').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "direccion"},
            {"mDataProp": "descripcion"},
            {"mDataProp": "fecha_registro"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var valor = "'" + o.direccion + "'";
                    return '<buttom class="btn btn-primary btn-w-m" onclick="mapa(' + valor + ');">Ver</buttom>';
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    return '<buttom class="btn btn-primary btn-w-m" onclick="eliminarDireccion(' + o.id_direccion_empresa + ');">Eliminar</buttom>';
                }
            }


        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}



function tblCargaVentaEmpresa(data) {
    if ($.fn.dataTable.isDataTable('#tbl_venta_empresa')) {
        $('#tbl_venta_empresa').DataTable().destroy();
    }

    $('#tbl_venta_empresa').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "Curso"},
            {"mDataProp": "categoria"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "Valor total venta"}
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}


function mapa(dir) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        data: {address: dir, key: 'AIzaSyD6MwS9aPQtsx3SIDr7e9JyT0GdBv1vtOY'},
        error: function (jqXHR, textStatus, errorThrown) {
            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {
            var arr = $.map(result, function (el) {
                return el
            });
            $('#dirr').html(arr[0]['formatted_address']);
            var mapOptions = {
                center: new google.maps.LatLng(arr[0]['geometry']['location']['lat'] + 0.002, arr[0]['geometry']['location']['lng'] - 0.0043),
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"),
                    mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(arr[0]['geometry']['location']['lat'], arr[0]['geometry']['location']['lng'])
            });
            marker.setMap(map);

            $("#div_tbl_direccion_empresa").animate({
                left: "+=50",
                height: "toggle"
            }, 500, function () {
                // Animation complete.
                $("#div_mapa_ql").animate({
                    left: "+=50",
                    height: "toggle"
                }, 500, function () {

                });
                google.maps.event.trigger(map, "resize");

            });
        },
        url: "https://maps.googleapis.com/maps/api/geocode/json"
    });



}

function desactivarContactoEmpresa(id_contacto_empresa, estatus) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto: id_contacto_empresa,
            estado: estatus
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {

            ContactoEmpresa($("#txt_id_empresa").val(), $("#modal_nombre_contacto").text());
        },
        url: "Listar/desactivarDatosContacto"
    });
}

function ubicarCamposUpdateContactoEmpresa(id_contacto_empresa) {
        
    $("#div_frm_add_edit").css("display", "block");
    //cargaComboNivelContacto();
    $("#div_tbl_contacto_empresa").css("display", "none");

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_contacto_empresa: id_contacto_empresa,
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Empresa", errorThrown, "error");
        },
        success: function (result) {

            $("#txt_id_contacto_empresa_edit").val(result[0].id_contacto_empresa);
            $("#txt_nombre_contacto_edit").val(result[0].nombre_contacto);
            $('#tipo_contacto_cbx_edit').val(result[0].id_tipo_contacto_empresa).trigger('change');
            $("#txt_cargo_contacto_edit").val(result[0].cargo_contacto);
            $("#txt_departamento_contacto_edit").val(result[0].departamento_contacto);
            $("#nivel_contacto_cbx_edit").val(result[0].id_nivel_contacto).trigger('change');
            $("#txt_correo_contacto_edit").val(result[0].correo_contacto);
            $("#txt_telefono_edit").val(result[0].telefono_contacto);
        },
        url: "Listar/getContactoEmpresa"
    });
}


function eliminarContactoEmpresa(id_contacto) {

    swal({
        title: "Está seguro?",
        text: "Este contacto se eliminará permanentemente.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Eliminar!",
        closeOnConfirm: false
    },
        function () {
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: { id_contacto_empresa: id_contacto },
                error: function (jqXHR, textStatus, errorThrown) {
                    alerta("Eliminar Contacto", errorThrown, "error");
                },
                success: function (result) {
                    getContactosEmpresa($("#id_empresa").val());
                    alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                },
                url: "Listar/eliminarDatosContacto"
            });

        });
    }

    


function eliminarDireccion(id) {
    swal({
        title: "Está seguro?",
        text: "Esta direccion se eliminará permanentemente.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Eliminar!",
        closeOnConfirm: false
    },
            function () {
                $.ajax({
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: 'POST',
                    data: {id_empresa: id},
                    error: function (jqXHR, textStatus, errorThrown) {
                        alerta("Empresa", errorThrown, "error");
                    },
                    success: function (result) {
                        DireccionesEmpresa($('#id_empresa_direccion').val(), $('#direc').val());
                        alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                    },
                    url: "Listar/eliminarDireccion"
                });

            });

}
