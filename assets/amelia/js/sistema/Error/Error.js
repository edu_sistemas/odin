// JavaScript Document
$(document).ready(function () {
    if ($.get("error") == 0) {
      //  alerta('No hay actividades', 'en los criterios seleccionados', 'warning');
      swal({
          title: "No hay actividades",
          text: "en los criterios seleccionados",
          type: "warning",
          showCancelButton: false,
          confirmButtonClass: "btn-danger",
          confirmButtonText: "Ok!",
          closeOnConfirm: true
        },
        function (isConfirm) {
            if (isConfirm) {
                window.location.href = "../Curso/DepartamentoTecnico";
              }
          });

    }

});

(function ($) {
    $.get = function (key) {
        key = key.replace(/[\[]/, '\\[');
        key = key.replace(/[\]]/, '\\]');
        var pattern = "[\\?&]" + key + "=([^&#]*)";
        var regex = new RegExp(pattern);
        var url = unescape(window.location.href);
        var results = regex.exec(url);
        if (results === null) {
            return null;
        } else {
            return results[1];
        }
    }
})(jQuery);

