$(document).ready(function () {
    ListarFichas();
    $("#frm-sabana").submit(function(){
            getReporte();
        event.preventDefault();
    });
});
function ListarFichas() {
    $.ajax({
        url: "Listar/getFichas",
        type: 'POST',
        dataType: "json",
        data: $('#frm_busqueda_personalizada').serialize(),
        beforeSend: function () {
            $("#cargando").html('<img src="../../assets/img/loading.gif">');
        },
        success: function (result) {
            cargaCombo("id_ficha",result);
            $("#contenido").css("display", "block");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar las fichas: "+errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        complete: function () {
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });
}

function getReporte(){
    $.ajax({
        url: "Listar/getReporte",
        type: 'POST',
        dataType: "json",
        data: $('#frm-sabana').serialize(),
        beforeSend: function () {
            $("#cargando").html('<img src="../../assets/img/loading.gif">');
        },
        success: function (result) {
            console.log(result);
            $("#contenido").css("display", "block");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar el reporte: "+errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    
                });
        },
        complete: function () {
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });
}