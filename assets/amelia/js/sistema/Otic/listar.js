$(document).ready(function () {

    llamaDatosOtic();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_buscar_otic").click(function () {
        llamaDatosOtic();
    });

    $("#cbx_holding").select2({
        placeholder: "Todos",
        allowClear: true
    });

});



function llamaDatosOtic() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaOtic(result);
        },
        url: "Listar/buscarOtic"
    });
}


function cargaTablaOtic(data) {

    if ($.fn.dataTable.isDataTable('#tbl_otic')) {
        $('#tbl_otic').DataTable().destroy();
    }

    $('#tbl_otic').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_otic"},
            {"mDataProp": "rut_otic"},
            {"mDataProp": "nombre_otic"},
            {"mDataProp": "nombre_corto_otic"},
            {"mDataProp": "direccion_otic"},
            {"mDataProp": "descripcion_otic"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {

                    var html = "";
                    html = '<i title="Inactivo" class="fa fa-circle text-danger"></i>';
                    if (o.estado_otic == 1) {
                        html = '  <i title="Activo"  class="fa fa-circle text-navy"></i>';
                    }
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var x = "'Editar/index/" + o.id_otic + "'";


                    return '<button class="btn-primary"  onclick="location.href=' + x + '">Editar</button>';
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Otic'
            }, {
                extend: 'pdf',
                title: 'Otic'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}

