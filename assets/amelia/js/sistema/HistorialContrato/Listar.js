// JavaScript Document
$(function () {
    recargaLaPagina();
    listarContratos();
});

function listarContratos() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            cargaTablacontrato(result);
        },
        url: "Listar/listarContratos"
    });
}
function cargaTablacontrato(data) {

    if ($.fn.dataTable.isDataTable('#tbl_contratos')) {
        $('#tbl_contratos').DataTable().destroy();
    }

    $('#tbl_contratos').DataTable(
            {"aaData": data,
                "aoColumns": [
                    {"mDataProp": "id_contrato"},
                    {"mDataProp": "num_ficha"},
                    {"mDataProp": "nombre_relator"},
                    {"mDataProp": "rut_relator"},
                    {"mDataProp": "curso"},
                    {"mDataProp": "horas_pagadas"},
                    {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                            var html = "";
                            html = '<i title="Inactivo" class="fa fa-circle text-danger"></i>';
                            if (o.habilitado == 1) {
                                html = '  <i title="Activo"  class="fa fa-circle text-navy"></i>';
                            }
                            return html;
                        }
                    },
                    {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                            var nombre_relator = "'" + o.nombre + " " + o.apellidos + "'";
                            var y = 'modal(' + o.id_contrato + ');';
                            var html = "";
                            html += '  <div class="btn-group">';
                            html += '  <button data-toggle="dropdown" class="btn btn-default dropdown-toggle ">Accion<span class="caret"></span></button>';
                            html += '  <ul class="dropdown-menu pull-right">';

                            html += '  <li><a onclick="' + y
                                    + '"  >Detalles</a></li>';
                            if (o.habilitado == 1) {
                                html += "<li><a onclick='EditarContratoEstado(" + o.id_contrato + ",0);'>Eliminar</a></li>";
                            }
                            html += ' </ul>';
                            html += ' </div>';
                            return html;
                        }
                    }

                ],
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {
                        extend: 'copy'
                    },
                    {
                        extend: 'csv'
                    },
                    {
                        extend: 'excel',
                        title: 'Empresa'
                    },
                    {
                        extend: 'pdf',
                        title: 'Empresa'
                    },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass(
                                    'white-bg');
                            $(win.document.body).css('font-size',
                                    '10px');
                            $(win.document.body).find('table')
                                    .addClass('compact').css(
                                    'font-size', 'inherit');
                        }
                    }]
            });
}
function EditarContratoEstado(id_contrato, estado) {
    swal({
        title: "Esta seguro?",
        text: "El contrato se eliminará para siempre. ¿Desea Eliminar?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Si, Eliminar!",
        closeOnConfirm: false
    },
            function () {
                $.ajax({
                    async: true,
                    cache: false,
                    dataType: "json",
                    type: 'POST',
                    data: {
                        id: id_contrato,
                        estado: estado
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // code en caso de error de la ejecucion del ajax
                        alerta("Usuario", errorThrown, "error");
                    },
                    success: function (result) {

                        listarContratos();
                    },
                    url: "Listar/CambiarEstadoContrato"
                });
                swal("Eliminado!", "El contrato a sido eliminado.", "success");
            });

}
function modal(id) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id: id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            alerta("Usuario", errorThrown, "error");
        },
        success: function (result) {
            console.log(result);
            $('#id_contrato').val(result[0].id_contrato);
            $('#n_ficha').val(result[0].num_ficha);
            $('#f_emision').val(result[0].fecha_emision);
            $('#nombre_relator').val(result[0].nombre_relator);
            $('#rut_relator').val(result[0].rut_relator);
            $('#direccion_relator').val(result[0].direccion_relator);
            $('#comuna_relator').val(result[0].comuna_relator);
            $('#curso').val(result[0].curso);
            $('#hora_curso').val(result[0].horas_curso);
            $('#f_termino').val(result[0].fecha_termino);
            $('#valor_hora').val('$ ' + new Intl.NumberFormat("es-CL").format(result[0].valor_hora));
            $('#f_boleta').val(result[0].fecha_boleta);
            $('#horas_pagadas').val(result[0].horas_pagadas);
            $('#alumnos').val(result[0].cantidad_alumnos);
            $('#quien_firma').val(result[0].quien_firma);
            $('#firma').val(result[0].firma);

            $('#total').val('$ ' + new Intl.NumberFormat("es-CL").format(result[0].horas_pagadas * result[0].valor_hora));
            $('#retencion').val('$ ' + new Intl.NumberFormat("es-CL").format((result[0].horas_pagadas * result[0].valor_hora) * 0.1));
            $('#a_pagar').val('$ ' + new Intl.NumberFormat("es-CL").format((result[0].horas_pagadas * result[0].valor_hora) - ((result[0].horas_pagadas * result[0].valor_hora) * 0.1)));


            $('#myModal').modal();
        },
        url: "Listar/detalleContrato"
    });

}