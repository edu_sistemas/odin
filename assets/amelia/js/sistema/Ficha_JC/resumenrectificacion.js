var href = "../../ResumenRectificacion/pdfResumen/" + $("#id_ficha").val() + "/" + $("#id_rectificacion").val() + "/";
$(document).ready(function () {
    recargaLaPagina();
    cargaCbxAll();
    $("#btn_back").click(function () {
        // retrocede una pagina
        limpiarFormulario();
        // history.back();
    });

    contarCaracteres("comentario", "text-out", 200);
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    inicializaControles();

    $("#btn_aprobar_rectificacion").click(function () {
        aprobarRectificacion();
    });

    $('#btn_pdf').attr("href", href+'0');
    $('#cbx_oc_rectificacion').change(function () {
        $('#btn_pdf').attr("href", href + $("#cbx_oc_rectificacion").val());
    });


    $("#btn_rechazar_rectificacion").click(function () {
        var id_rectificacion = $("#id_rectificacion").val();
        abrirRechazo(id_rectificacion);
    });

    $("#btn_aceptar").click(function () {
        if (validarRequeridoForm($("#frm_rechazo"))) {
            rechazarRectificacion();
            $('#modal_rechazo').modal('hide');
            history.back();
        } else {
            event.stopPropagation();
        }

    });


    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });
});

function rechazarRectificacion() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_rechazo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {
            enviaCorreoRechazo(result);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            history.back();
                        }
                    });
        },
        url: "../rechazarRectificacion"
    });
}

function enviaCorreoRechazo(data) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {datos: data},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "../enviaEmailRechazoRectificacion"
    });
}



function abrirRechazo(id) {
    $("#modal_id_rectificacion_rechazo").text(id);
    $("#id_rectificacion_rechazo").val(id);
    $("#modal_rechazo").modal("show");
}

function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() {
    var id_rectificacion = "";
    id_rectificacion = $("#id_rectificacion").val();
    console.log(id_rectificacion);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            cargaTablaDatosAlumnos(result);
            $.ajax({
                async: true,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: { id_rectificacion: id_rectificacion },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    //  console.log(textStatus + errorThrown);
                },
                success: function (resultado) {
                    
                    cargaTablaDatosAlumnosPorRectificar(resultado, result);
                },
                url: "../cargaDatosOCRectificacion"
            });
        },
        url: "../cargaDatosOC"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../cargaDatosOCDocumentoResumen"
    });

  /*  $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosAlumnosPorRectificar(result);
        },
        url: "../cargaDatosOCRectificacion"
    });
*/
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id_rectificacion},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentosRectificacion(result);
        },
        url: "../cargaDatosOCDocumentoResumenRectificaciones"
    });
}


function cargaTablaDatosAlumnosPorRectificar(data, data1) {
    if (data.length > 0) {
        if (data1.length > 0) {
            if (data[0].tipo_rectificacion == "Eliminar Alumnos") {
                var resultado = parseInt(data1[0].c_valor_final) - parseInt(data[0].cant_total);
            }
            if (data[0].tipo_rectificacion == "Agregar Alumnos") {
                var resultado = parseInt(data1[0].c_valor_final) + parseInt(data[0].cant_total);
            }
            if (data[0].tipo_rectificacion == "Actualizar Alumnos") {
                var restar = 0;
                $.each(data, function (i, item) {
                    $.each(data1, function (i1, item1) {
                        if (item.rut == item1.rut) {
                           // console.log(item.rut);
                            restar += parseInt(item1.c_valor_total);
                           // console.log(restar);
                        }
                    });
                });
                var resultado = parseInt(data1[0].c_valor_final) - parseInt(restar) + parseInt(data[0].cant_total);
            }
        }

        const noTruncarDecimales = { maximumFractionDigits: 20 };

        // Convertimos al número en 3 formatos diferentes
        var comas = resultado.toLocaleString('en-US', noTruncarDecimales);
        // resultado = resultado;
        $("#valor_rectificado").html('<b>$' + (comas) + '</b>');
    }

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno_rectificacion')) {
        $('#tbl_orden_compra_carga_alumno_rectificacion').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno_rectificacion').DataTable({
        initComplete: function () {
            this.api().columns([7]).every(function () {
                var column = this;
                var select = $('<select><option value="">Filtrar Por OC</option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.cells('', column[0]).render('display').sort().unique().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>');
                    $("#cbx_oc_rectificacion").append('<option value="' + d + '">' + d + '</option>');
                });
            });
        },
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_rectificacion"},
            {"mDataProp": "rut"},
            {"mDataProp": "nombre_alumno"},
            {"mDataProp": "apellido_paterno"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "porcentaje_franquicia"},
//            {"mDataProp": "costo_otic"},
//            {"mDataProp": "costo_empresa"},
//            {"mDataProp": "valor_agregado"},
            {"mDataProp": "valor_total"},
            { "mDataProp": "num_orden_compra"},
            {"mDataProp": "causa"}
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();
}


function cargaTablaDatosAlumnos(data) {
    if (data.length > 0) {
        $("#total_alumnos").html(data[0].total_alumnos);
        $("#valor_final").html(data[0].valor_final);
        $("#total_sence").html(data[0].total_sence);
        $("#total_empresa").html(data[0].total_empresa);
        $("#total_sence_empresa").html(data[0].total_sence_empresa);
        $("#total_agregado").html(data[0].total_agregado);
        $("#total_becados").html(data[0].total_becados);
        $("#total_oc").html(data[0].total_oc);
    }

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno').DataTable({
        initComplete: function () {
            this.api().columns([5]).every(function () {
                var column = this;
                var select = $('<select><option value="">Filtrar Por OC</option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.cells('', column[0]).render('display').sort().unique().each(function (d, j){
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        },
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "rut"},
            {"mDataProp": "nombre"},
            {"mDataProp": "centro_costo"},
            {"mDataProp": "fq"},
//            {"mDataProp": "valor_otic"},
//            {"mDataProp": "valor_empresa"},
//            {"mDataProp": "valor_agregado"},
            {"mDataProp": "valor_total"},
            {"mDataProp": "num_orden_compra"}
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
    $('[data-toggle="tooltip"]').tooltip();
}

function cargaTablaDatoDocumentosRectificacion(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetos_rectificacion')) {
        $('#tbl_orden_compra_documetos_rectificacion').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetos_rectificacion').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            }

        ],
        pageLength: 5,
        responsive: false,
        dom: '<"clear">'

    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            }

        ],
        pageLength: 5,
        responsive: false,
        dom: '<"clear">'

    });

    $('[data-toggle="tooltip"]').tooltip();

}


function aprobarRectificacion() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: $("#id_rectificacion").val()},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Rectificación", errorThrown, "error");
        },
        success: function (result) {

            enviaCorreo(result);
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                    function () {

                        setTimeout(function () {
                            // location.href = "../../Rectificaciones";
                            history.back();
                        }, 3100);



                    });
        },
        url: "../aprobarRectificacion"
    });
}

function enviaCorreo(data) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {datos: data},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "../enviaEmail"
    });
}