$(document).ready(function () {
       recargaLaPagina();
    cargaCbxAll();

    // $("#btn_min_max").trigger("click");
    $("#btn_registrar").click(function () {

        if (validarRequeridoForm($("#frm_ficha_registro"))) {
            if (validacionesExtras()) {
                RegistraFicha();
            }
        }
    });

    $('#curso').html('').select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: [{id: '', text: ''}]
    });

    $("#curso").prop("disabled", true);

    contarCaracteres("comentario", "text-out", 200);
    
    $('#txt_num_o_c').tagsinput({
        confirmKeys: [13, 44],
        tagClass: 'big',
        maxTags: 3,
        trimValue: true,
        maxChars: 8,
        allowDuplicates: false,
        preventPost: false
    });


    $('#chk_sin_orden_compra').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    }).on('ifChanged', function (e) {        // Get the field name        

        var isChecked = e.currentTarget.checked;
        if (isChecked == true) {
            $('#txt_num_o_c').tagsinput('removeAll');
            $('#txt_num_o_c').addClass("disabled");
            $("#otic").val(1).trigger("change");
            $("#otic").prop("disabled", true);
            //$("#otic").addClass("disabled");
            $("#txt_num_o_c").prop("disabled", true);
            $("#txt_num_o_c").val("");
            $("#txt_num_o_c").attr("requerido", false);
        } else {
            $("#otic").prop("disabled", false);
            $("#otic").removeClass("disabled");

            $("#txt_num_o_c").prop("disabled", false);
            $("#txt_num_o_c").attr("requerido", true);
            $("#txt_num_o_c").val("");
            $('#txt_num_o_c').tagsinput('focus');
        }
    });

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });


    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('#modalidad').change(function () {
        if ($('#modalidad').val() != "") {
            $("#curso").prop("disabled", false);
            cargaCurso();
        } else {
            $("#curso").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#curso").prop("disabled", true);
        }
    });

});


function validacionesExtras() {
    var result = false;
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_termino = $("#fecha_termino").val();
    var fecha_cierre = $("#fecha_cierre").val();

    var f = new Date();

    var f_actual = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();



    if (fecha_inicio < f_actual) {
        alerta("Fecha", "Fecha Inicio no puede ser menor a la fecha Actual", "warning");
        result = false;
    } else {
        result = true;
    }

    if (fecha_inicio > fecha_termino) {
        alerta("Fecha", "Fecha Inicio no puede ser mayor a la fecha de  fecha_termino", "warning");
        result = false;
    } else {

        result = true;
    }


    if (fecha_cierre < fecha_termino) {
        alerta("Fecha", "Fecha Cierre no puede ser menor a la de Término", "warning");
        result = false;
    } else {

        result = true;
    }



    return result;

}
function RegistraFicha() {

    /*    dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     */

    // console.log('imagen en base64: '+ document.getElementById("logo").value);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_ficha_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }

        },
        url: "Agregar/Registra_ficha"
    });
}

function cargaCurso() {



    $('#curso').html('').select2({data: [{id: '', text: ''}]});

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            modalidad: $('#modalidad').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "curso");
        },
        url: "Agregar/getCursoCBX"
    });


}

function cargaCbxAll() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "modalidad");
        },
        url: "Agregar/getModalidadCBX"
    });



    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "Agregar/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "otic");
        },
        url: "Agregar/getOticCBX"
    });
}

function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");


}
