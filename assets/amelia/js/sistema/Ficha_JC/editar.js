var tem_id_orden_compra = "";
var tem_num_orden_compra = "";
var tem_otic_selected = "";
$(document).ready(function () {
       recargaLaPagina();
    cargaCbxAll();

    $("#btn_registrar").click(function () {
        if (validarRequeridoForm($("#frm_ficha_registro"))) {
            if (validacionesExtras()) {
                EditarFicha();
            }
        }
    });

    $("#btn_back").click(function () {
        // retrocede una pagina
        history.back();
    });

    contarCaracteres("comentario", "text-out", 200);
    $("#comentario").trigger("change");

    $('#txt_num_o_c').tagsinput({
        confirmKeys: [13, 44],
        tagClass: 'big',
        maxTags: 3,
        trimValue: true,
        maxChars: 8,
        allowDuplicates: false,
        preventPost: false
    });


    var confirmed = true;

    $('#txt_num_o_c').on('beforeItemRemove', function (event) {
        var tag = event.item;
        event.cancel = confirmed;
        if (confirmed) {
            swal({
                title: "Estás seguro?",
                text: "Eliminarás todos los alumnos registrados en esta orden de compra",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, Eliminarlo!",
                cancelButtonText: "No, Cancelar!",
                cancelButtonColor: "#DD6B55",
                closeOnConfirm: true
            }, function (isConfirm) {
                confirmed = false;
                if (isConfirm) {

                    var mis_ordenes = $('#txt_num_o_c').val();
                    var array_ordenes = mis_ordenes.split(",");
                    var indice_num_oc = array_ordenes.indexOf(tag);

                    var mis_id_orden = $("#id_num_o_c").val();
                    var array_id_orden = mis_id_orden.split(",");
                    // obtengo el elemento que necesito eliminar
                    var id_orden_compra_eliminar = array_id_orden[indice_num_oc];
                    // borro el elemento del array
                    array_id_orden.splice(indice_num_oc, 1);

                    // guardo los valores nuevos
                    $("#id_num_o_c").val(array_id_orden.toString());

                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {
                            id_ficha: $('#id_ficha').val(),
                            id_orden_compra: id_orden_compra_eliminar,
                            num_orden_compra: tag
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // code en caso de error de la ejecucion del ajax
                            //console.log(textStatus + errorThrown);
                            alerta('Ficha', errorThrown, 'error');
                        },
                        success: function (result) {

                            $('#txt_num_o_c').tagsinput('remove', tag);
                            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);


                        },
                        url: "../deleteOC"
                    });


                    /* if (eliminarOC($('#id_ficha').val(), id_orden_compra_eliminar, tag)) {
                     
                     }*/

                } else {
                    confirmed = true;
                }
            });
        } else {
            event.cancel = confirmed;
            confirmed = true;
        }

    });

    $('#chk_sin_orden_compra').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    }).on('ifChanged', function (e) {
        var isChecked = e.currentTarget.checked;
        if (isChecked == true) {


            tem_otic_selected = $("#otic").val();
            $("#otic").val(1).trigger("change");
            $("#otic").prop("disabled", true);
            tem_id_orden_compra = $("#id_num_o_c").val();
            tem_num_orden_compra = $("#txt_num_o_c").val();
            $('#txt_num_o_c').tagsinput('removeAll');
            $('#txt_num_o_c').addClass("disabled");
            $("#txt_num_o_c").prop("disabled", true);
            $("#txt_num_o_c").attr("requerido", false);

        } else {
            $("#otic").prop("disabled", false);
            $("#otic").removeClass("disabled");
            $("#txt_num_o_c").prop("disabled", false);
            $("#txt_num_o_c").attr("requerido", true);
            $("#txt_num_o_c").val("");
            $("#otic").val(tem_otic_selected).trigger("change");

            $("#id_num_o_c").val(tem_id_orden_compra);
            $("#txt_num_o_c").tagsinput('add', tem_num_orden_compra);
        }

    });


    if ($("#txt_num_o_c").val() == "") {
        $('#chk_sin_orden_compra').iCheck('check');
    }
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });


    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('#modalidad').change(function () {
        if ($('#modalidad').val() != "") {
            $("#curso").prop("disabled", false);
            cargaCurso();
        } else {
            $("#curso").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#curso").prop("disabled", true);
        }
    });


});

function eliminarOC(ficha, oc, num_oc) {
    var resultado = false;


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: ficha,
            id_orden_compra: oc,
            num_orden_compra: num_oc
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);


        },
        url: "../deleteOC"
    });


    return resultado;
}


function EditarFicha() {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_ficha_registro').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {
                $('#btn_reset').trigger("click");
            }

        },
        url: "../Registra_ficha"
    });
}

function validacionesExtras() {
    var result = false;
    var fecha_inicio = $("#fecha_inicio").val();
    var fecha_termino = $("#fecha_termino").val();
    var fecha_cierre = $("#fecha_cierre").val();

    var f = new Date();

    var f_actual = f.getFullYear() + "-" + (f.getMonth() + 1) + "-" + f.getDate();



    if (fecha_inicio < f_actual) {
        alerta("Fecha", "Fecha Inicio no puede ser menor a la fecha Actual", "warning");
        result = false;
    } else {
        result = true;
    }

    if (fecha_inicio > fecha_termino) {
        alerta("Fecha", "Fecha Inicio no puede ser mayor a la fecha de  fecha_termino", "warning");
        result = false;
    } else {

        result = true;
    }


    if (fecha_cierre < fecha_termino) {
        alerta("Fecha", "Fecha Cierre no puede ser menor a la de Término", "warning");
        result = false;
    } else {

        result = true;
    }



    return result;

}

function cargaCurso() {



    $('#curso').html('').select2({data: [{id: '', text: ''}]});

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            modalidad: $('#modalidad').val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "curso");
        },
        url: "../getCursoCBX"
    });


}

function cargaCbxAll() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "modalidad");
        },
        url: "../getModalidadCBX"
    });



    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "../getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "otic");
        },
        url: "../getOticCBX"
    });
}

function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    $("#" + item).val($("#id_" + item).val()).trigger("change");


}
