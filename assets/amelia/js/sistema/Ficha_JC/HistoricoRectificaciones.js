$(document).ready(function () {
    recargaLaPagina();
    llamaDatosRectificacion();

    $("#btn_buscar_ficha").click(function () {
        llamaDatosRectificacion();
    });

});



function aprobarRectificacion(id) {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_rectificacion: id},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            llamaDatosRectificacion();
        },
        url: "Rectificaciones/aprobarRectificacion"
    });

}


function llamaDatosRectificacion() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaRectificacion(result);
        },
        url: "HistoricoRectificaciones/buscarRectificacion"
    });
}


function cargaTablaRectificacion(data) {

    if ($.fn.dataTable.isDataTable('#tbl_rectificacion')) {
        $('#tbl_rectificacion').DataTable().destroy();
    }




    $('#tbl_rectificacion').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "num_orden_compra"},
            {"mDataProp": "tipo_rectificacion"},
            {"mDataProp": "fecha_registro"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "nombre_modalidad"},
            {"mDataProp": "razon_social_empresa"},
            {"mDataProp": "nombre_otic"},
            {"mDataProp": "descripcion_estado_edutecno"},

            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html += '  <div class="btn-group">';

                    var resumen = "";
                    if (o.tipo_rectificacion == "Extensión de Plazo") {
                        resumen = "ResumenHistoricoRectificacionExtension/index/" + o.id_rectificacion;
                    } else {
                        resumen = "ResumenHistoricoRectificacion/index/" + o.id_rectificacion;
                    }
                    html += " <a href='" + resumen + "'><button   class='btn btn-primary dropdown-toggle'>VER </button></a> ";

                    html += ' </div>';

                    html += ' </div>';
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();
}

 