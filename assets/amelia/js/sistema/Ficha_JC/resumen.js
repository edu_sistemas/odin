var id_solicitud_tablet;
$(document).ready(function () {
    id_solicitud_tablet = $("#id_solicitd_tablet").val();
    $("#modalLoading").css("display", "none");
    recargaLaPagina();
    cargaCbxAll();
    $("#btn_back").click(function () {
        // retrocede una pagina
        limpiarFormulario();
        // history.back();
    });

    $("#btn_aceptar").click(function () {
        if (validarRequeridoForm($("#frm_rechazo"))) {
            rechazarFicha();
            $('#modal_rechazo').modal('hide');
            history.back();
        } else {
            event.stopPropagation();
        }
    });

    $("#btn_rechazar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        abrirRechazo(ficha);
    });


    $("#btn_aprobar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        aprobarFicha(ficha);
    });

    $("#cbx_orden_compra").change(function () {
        var id_ficha = "";

        var id_oc = "";


        id_ficha = $("#id_ficha").val();

        id_oc = $("#cbx_orden_compra").val();


        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: { ficha: id_ficha, oc: id_oc },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                cargaTablaDatosAlumnos(result);
            },
            url: "../cargaDatosOC"
        });
    });


    contarCaracteres("comentario", "text-out", 200);
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    inicializaControles();

    $("#btn_enviar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        validaPreEnvioFicha(ficha);
    });

    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });

    cargaAdicionales();

});


function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() {


    var id_ficha = "";

    var id_oc = "";


    id_ficha = $("#id_ficha").val();

    id_oc = $("#cbx_orden_compra").val();


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ficha: id_ficha, oc: id_oc },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosAlumnos(result);
        },
        url: "../cargaDatosOC"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ficha: id_ficha, oc: id_oc },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../cargaDatosOCDocumentoResumen"
    });

}

function cargaTablaDatosAlumnos(data) {

    if (data.length > 0) {
        $("#total_alumnos").html(data[0].total_alumnos);
        $("#valor_final").html(data[0].valor_final);
        $("#total_sence").html(data[0].total_sence);
        $("#total_empresa").html(data[0].total_empresa);
        $("#total_sence_empresa").html(data[0].total_sence_empresa);
        $("#total_agregado").html(data[0].total_agregado);
        $("#total_becados").html(data[0].total_becados);
        $("#total_oc").html(data[0].total_oc);

    }
    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "rut" },
            { "mDataProp": "nombre" },
            { "mDataProp": "centro_costo" },
            { "mDataProp": "fq" },
            //            {"mDataProp": "valor_otic"},
            //            {"mDataProp": "valor_empresa"},
            //            {"mDataProp": "valor_agregado"},
            { "mDataProp": "valor_total" },
            { "mDataProp": "num_orden_compra" }
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "tipo_documento" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            }

        ],
        pageLength: 5,
        responsive: false,
        dom: '<"clear">'

    });
    $('[data-toggle="tooltip"]').tooltip();

}


function abrirRechazo(id) {
    $("#modal_id_ficha_rechazo").text($("#id_ficha_original").val());
    $("#id_ficha_rechazo").val(id);
    $("#modal_rechazo").modal("show");
}


function rechazarFicha() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_rechazo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            alerta('Ficha', errorThrown, 'error');
        },
        success: function (result) {

            enviaCorreoRechazo(result);

            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                function (isConfirm) {
                    if (isConfirm) {
                        history.back();
                    }
                });
        },
        url: "../rechazarFicha"
    });
}

function aprobarFicha(id) {

    $("#modalLoading").css("display", "block");

    // id_solicitud_tablet

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {

            if (id_solicitud_tablet != "") {
                setCodigosTablet(id_solicitud_tablet, result[0].num_ficha);
            }
            enviaCorreo(result);
            $("#modalLoading").css("display", "none");
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                function () {
                    location.reload();
                });
        },
        url: "../aprobarFicha"
    });



}

function setCodigosTablet(id_solicitud_tablet, num_ficha) {
    $.ajax({
        url: "../setCodigosTablet",
        type: 'POST',
        dataType: "json",
        data: { id_solicitud_tablet: id_solicitud_tablet, num_ficha: num_ficha },
        beforeSend: function () {
            $("#modalLoading").css("display", "block");
        },
        success: function (result) {
            $("#modalLoading").css("display", "none");
            console.log(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#modalLoading").css("display", "none");
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        },
        complete: function () {
            $("#modalLoading").css("display", "none");
        },
        async: true,
        cache: false
    });
}

function enviaCorreo(data) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { datos: data },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "../enviaEmail"
    });
}
function enviaCorreoRechazo(data) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { datos: data },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "../enviaEmailRechazo"
    });
}

function cargaAdicionales() {
    var id_ficha = $('#id_ficha').val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id_ficha },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            var contenido = '';
            $('#tbl_adicionales tbody').html('');
            $.each(result, function (i, item) {
                if (item.nombre == null) {
                    contenido += '';
                } else {
                    contenido += '<tr><td>';
                    contenido += item.nombre;
                    contenido += '</td><td>';
                    contenido += item.valor_formateado;
                    contenido += '</td></tr>';
                }
            });
            $('#tbl_adicionales tbody').html(contenido);
            contenido = "";
            $('#total_adicionales').text(result[0].valor_total);
        },
        url: "../getAdicionalesFicha"
    });
}