$(document).ready(function () {
	recargaLaPagina();
	llamaDatosFicha();
	$("#btn_buscar_ficha").click(function () {
		llamaDatosFicha();
	});
	$('[data-toggle="tooltip"]').tooltip();

	$("#holding").change(function () {
		var id_holding = $('#holding').val();
		getEmpresaByHolding(id_holding);
	});


	cargaTodosDatosExtras();
});

function  cargaTodosDatosExtras() {


	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'GET',
		//  data: $('#frm').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			// console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaComboGenerico(result, 'empresa');
		},
		url: "Listar/getEmpresaCBX"
	});


	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'GET',
		//  data: $('#frm').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaComboGenerico(result, 'holding');

		},
		url: "Listar/getHoldingCBX"
	});

	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'GET',
		// data: {id_holding: id_holding},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			// console.log(textStatus + errorThrown + jqXHR);
		},
		success: function (result) {

			cargaComboGenerico(result, 'ejecutivo');
		},
		url: "Listar/getEjecutivo"
	});

	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'GET',
		// data: {id_holding: id_holding},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown + jqXHR);
		},
		success: function (result) {
			cargaComboGenerico(result, 'periodo');
		},
		url: "Listar/getPeriodo"
	});

}

function cargaComboGenerico(midata, id) {

	$("#" + id).select2({
		placeholder: "Todos",
		allowClear: true,
		data: midata
	});

	$("#" + id + " option[value='0']").remove();

}

function getEmpresaByHolding(id_holding) {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {id_holding: id_holding},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown + jqXHR);
		},
		success: function (result) {

			cargaComboGenerico(result, 'empresa');
		},
		url: "Consulta/getEmpresaByHoldingCBX"
	});
}

function llamaDatosFicha() {
	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: $('#frm').serialize(),
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			// console.log(textStatus + errorThrown);
		},
		success: function (result) {
			cargaTablaFicha(result);
		},
		url: "Listar/buscarFicha"
	});
}
function cargaTablaFicha(data) {

	if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
		$('#tbl_ficha').DataTable().destroy();
	}

	$('#tbl_ficha').DataTable({
		"aaData": data,
		"aoColumns": [
			{"mDataProp": "num_ficha"},
			{"mDataProp": "ejecutivo"},
			{"mDataProp": "categoria"},
			{"mDataProp": "modalidad"},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {
					var html = "";
					html += o.fecha_inicio + '-' + o.fecha_fin;
					return html;
				}
			},
			{"mDataProp": "descripcion_producto"},
			{"mDataProp": "otic"},
			{"mDataProp": "empresa"},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {
					var html = "";
					html += '  <p data-toggle="tooltip" data-placement="top" title data-original-title="Sence: ' + o.valor_sence + ' • Empresa: ' + o.valor_empresa + '" >' + o.valor_total + '</p>';
					return html;
				}
			},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {
					var html = " Colo car POP UP";
					html = "<a href='#' title='Ver Alumnos' onclick='AlumnosFicha(" + o.id_ficha + ");'>" + o.cant_alumno + "</a>";
					html = o.cant_alumno;
					return html;
				}
			},
			{"mDataProp": null, "bSortable": false, "mRender": function (o) {

					var a = "Resumen/index/" + o.id_ficha;
					var b = "ResumenArriendo/index/" + o.id_ficha;

					var html = "";

					html += "<div class='btn-group'>";
					if (o.id_estado == 62) {
						html += "<button data-toggle='dropdown' class='btn btn-danger dropdown-toggle'>Acción<span class='caret'></span></button>";
					} else {
						html += "<button data-toggle='dropdown' class='btn btn-primary dropdown-toggle'>Acción<span class='caret'></span></button>";
					}
					html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";

					// faltaria programar la parte del arriendo

					if (o.id_estado == 62) {
						html += '<li><a href="ResumenAnulacion/index/' + o.id_ficha + '">Anular</a></li>';
						var con = o.num_ficha;
						$('tr td:contains(' + con + ')').each(function () {
							$(this).addClass('alert alert-danger');
						});
					}
					if (o.id_rectificacion == '') {
						if (o.id_estado == 20) {
							html += '<li><a href="#"  onclick="AprobarFicha(' + o.id_ficha + ','+ o.id_solicitud_tablet+')">Aprobar</a></li>';
							html += '<li class="divider"></li>';
						}

						if (o.id_categoria == 1 && o.id_estado != 62) {
							html += "<li><a href='" + a + "'>VER</a></li>";
						}
						if (o.id_categoria == 2 && o.id_estado != 62) {
							html += "<li><a href='" + b + "'>VER</a></li>";
						}

					} else {

						var resumen = "";

						if (o.categoria == "Extensión de Plazo") {
							resumen = "ResumenRectificacionExtension/index/" + o.id_rectificacion;
						} else {
							resumen = "ResumenRectificacion/index/" + o.id_rectificacion;
						}
						html += "<li><a href='" + resumen + "'>VER</a></li>";
					}



					html += "</ul>";
					html += "</div>";
					return html;

				}
			}
		],
		pageLength: 10,
		responsive: false,
		dom: '<"html5buttons"B>lTfgitp',
		buttons: [
			{
				extend: 'copy'
			}, {
				extend: 'csv'
			}, {
				extend: 'excel',
				title: 'Edutecno'
			}, {
				extend: 'pdf',
				title: 'Edutecno'
			}, {
				extend: 'print',
				customize: function (win) {
					$(win.document.body).addClass('white-bg');
					$(win.document.body).css('font-size', '10px');
					$(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
				}
			}
		]
	});

	$('[data-toggle="tooltip"]').tooltip();
}

function DocumentosFicha(id) {
	$("#modal_id_ficha").text(id);

	$.ajax({
		async: false,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {id_ficha: id},
		error: function (jqXHR, textStatus, errorThrown) {
			alerta("Ficha", errorThrown, "error");
		},
		success: function (result) {
			cargaDatosDocumentoFicha(result);
			$("#modal_documentos_ficha").modal("show");
			$("#div_frm_add").css("display", "none");
			$("#div_tbl_contacto_empresa").css("display", "block");
			$("#btn_nuevo_contacto").css("display", "block");
		},
		url: "Listar/verDocumentosFicha"
	});
}

function cargaDatosDocumentoFicha(data) {
	var html = "";
	$('#tbl_documentos_ficha tbody').html(html);
	$.each(data, function (i, item) {
		html += '        <tr>';
		if (item.nombre_documento % 2 == 0) {
			html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a></td> ';
		} else {
			html += ' <td><a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a></td> ';
		}

		html += '   <td>' + item.fecha_registro + '</td> ';

		html += '</tr>';
	});
	$('#tbl_documentos_ficha tbody').html(html);
}


function AprobarFicha(id, id_solicitud_tablet) {

    $("#modalLoading").css("display", "block");

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {

            if (id_solicitud_tablet != "") {
                setCodigosTablet(id_solicitud_tablet, result[0].num_ficha);
            }
            enviaCorreo(result);
            $("#modalLoading").css("display", "none");
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta
            },
                function () {
                    location.reload();
                });
        },
        url: "Listar/aprobarFicha"
    });



}

function setCodigosTablet(id_solicitud_tablet, num_ficha) {
    $.ajax({
        url: "Listar/setCodigosTablet",
        type: 'POST',
        dataType: "json",
        data: { id_solicitud_tablet: id_solicitud_tablet, num_ficha: num_ficha },
        beforeSend: function () {
            $("#modalLoading").css("display", "block");
        },
        success: function (result) {
            $("#modalLoading").css("display", "none");
            console.log(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $("#modalLoading").css("display", "none");
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        },
        complete: function () {
            $("#modalLoading").css("display", "none");
        },
        async: true,
        cache: false
    });
}

function enviaCorreo(data) {

	$.ajax({
		async: true,
		cache: false,
		dataType: "json",
		type: 'POST',
		data: {datos: data},
		error: function (jqXHR, textStatus, errorThrown) {
			// code en caso de error de la ejecucion del ajax
			//  console.log(textStatus + errorThrown);
		},
		success: function (result) {
			// cargaTablaFicha(result);
		},
		url: "Listar/enviaEmail"
	});
}


