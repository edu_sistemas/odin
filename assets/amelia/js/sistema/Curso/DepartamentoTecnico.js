// JavaScript Document
var curdate = new Date();
var fecha_reporte = '';
var externos = 0;

$(document).ready(function () {
    $('#externos_hidden').val(0);
    externos =  $('#externos_hidden').val();
   
    
    $('#fecha_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $("#fecha_reporte").val(curdate.getDate()+'-'+(curdate.getMonth()+1)+'-'+curdate.getFullYear());
    fecha_reporte = $("#fecha_reporte").val();
    $('#btnActividadesVigentes').attr("href", $('#btnActividadesVigentes').attr("href")+fecha_reporte);
    $('#btnActividadesDiarias').attr("href", $('#btnActividadesDiarias').attr("href")+fecha_reporte+'/'+0);
    $('#btnActividadesDiariasReserva').attr("href", $('#btnActividadesDiariasReserva').attr("href")+fecha_reporte+'/'+0);
    $("#fecha_reporte").on("change", function(){
        $('#btnActividadesVigentes').attr("href", $('#btnActividadesVigentes').attr("href").replace(fecha_reporte, ''));
        $('#btnActividadesDiarias').attr("href", $('#btnActividadesDiarias').attr("href").replace(fecha_reporte+'/'+externos, ''));
        $('#btnActividadesDiariasReserva').attr("href", $('#btnActividadesDiariasReserva').attr("href").replace(fecha_reporte+'/'+externos, ''));
        fecha_reporte = $("#fecha_reporte").val();
        $('#btnActividadesVigentes').attr("href", $('#btnActividadesVigentes').attr("href")+fecha_reporte);
        $('#btnActividadesDiarias').attr("href", $('#btnActividadesDiarias').attr("href")+fecha_reporte+'/'+externos);
        $('#btnActividadesDiariasReserva').attr("href", $('#btnActividadesDiariasReserva').attr("href")+fecha_reporte+'/'+externos);
    });

    $('#incluir_externos').change(function(){
        if(this.checked){
            $('#btnActividadesDiarias').attr("href", $('#btnActividadesDiarias').attr("href").replace(fecha_reporte+'/'+externos, ''));
            $('#btnActividadesDiariasReserva').attr("href", $('#btnActividadesDiariasReserva').attr("href").replace(fecha_reporte + '/' + externos, ''));
            externos = 1;
            $('#btnActividadesDiarias').attr("href", $('#btnActividadesDiarias').attr("href")+fecha_reporte+'/'+1);
            $('#btnActividadesDiariasReserva').attr("href", $('#btnActividadesDiariasReserva').attr("href") + fecha_reporte + '/' + 1);
        }else{
            $('#btnActividadesDiarias').attr("href", $('#btnActividadesDiarias').attr("href").replace(fecha_reporte+'/'+externos, ''));
            $('#btnActividadesDiariasReserva').attr("href", $('#btnActividadesDiariasReserva').attr("href").replace(fecha_reporte + '/' + externos, ''));
            externos = 0;
            $('#btnActividadesDiarias').attr("href", $('#btnActividadesDiarias').attr("href")+fecha_reporte+'/'+0);
            $('#btnActividadesDiariasReserva').attr("href", $('#btnActividadesDiariasReserva').attr("href") + fecha_reporte + '/' + 0);
        }
        
    });

   

    $("#holding").select2({
        placeholder: "Todos",
        allowClear: true
    });


    $("#btn_descargar_actividad_vigente").click(function () {
        $("#num_num_ficha").val($("#num_ficha").val());
        $("#fecha_start").val($("#start").val());
        $("#fecha_end").val($("#end").val());
        $("#id_empresa").val($("#empresa").val());
        $("#frm_Actividades_Vigentes").submit();
    });

    $("#btn_descargar_actividad_diaria").click(function () {
        $("#frm_Actividades_diarias #num_num_ficha").val($("#num_ficha").val());
        $("#frm_Actividades_diarias #fecha_start").val($("#start").val());
        $("#frm_Actividades_diarias #fecha_end").val($("#end").val());
        $("#frm_Actividades_diarias #id_empresa").val($("#empresa").val());
        $("#frm_Actividades_diarias").submit();


        

});



    llamaFichas();
    $("#btn_consultar_fecha").click(function () {
        llamaFichas();
    });
    llamaEmpresas();
    $("#empresa").select2({
        placeholder: "Todos",
        allowClear: true
    });
    $('fechatitulo').html($("#fecha_reporte").val());
    
});

$("#holding").change(function () {
    if ($("#holding").val() != "") {
        cargaEmpresaBYHolding();
    } else {
        llamaEmpresas();
    }
});

function cargaEmpresaBYHolding() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_holding: $("#holding").val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown + jqXHR);
        },
        success: function (result) {
            $('#empresa').html('').select2({data: [{id: '', text: ''}]});
            cargaCombo(result, 'empresa');
        },
        url: "DepartamentoTecnico/getEmpresaByHoldingCBX"
    });

}


function llamaFichas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_busqueda').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            TablaFichas(result);
        },
        url: "DepartamentoTecnico/fichas"
    });
}

function llamaEmpresas() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'holding');
        },
        url: "DepartamentoTecnico/getHoldingCBX"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            $('#empresa').html('').select2({data: [{id: '', text: ''}]});
            cargaCombo(result, 'empresa');
        },
        url: "DepartamentoTecnico/Empresas"
    });


}

function TablaFichas(data) {
    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }


    /*
     <th>N° Ficha</th>
     <th>Empresa</th>
     <th>F.Inicio</th>
     <th>F.Térm.</th>
     <th>Nombre Curso</th>
     <th>Hrs.</th>
     <th>Relator</th>
     <th>Dias</th>
     <th>H.I </th>
     <th>H.T </th>
     <th>Sala </th>
     <th> Al.</th>
     <th> SenceNet</th>
     */
    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "razon_social_empresa"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "fecha_cierre"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "duracion_curso"},
            {"mDataProp": "relator"},
            {"mDataProp": "dias"},
            {"mDataProp": "hora_inicio"},
            {"mDataProp": "hora_termino"},
            {"mDataProp": "sala"},
            {"mDataProp": "alumnos"},
            {"mDataProp": "id_sence"}
        ],
        pageLength: 15,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Sence'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }],
		        "order": [[1, "desc"]]	
    });
    /* 
     $('#tbl_ficha').DataTable({
     "aaData": data,
     "aoColumns": [
     {"mDataProp": "id_ficha"},
     {"mDataProp": "num_ficha"},
     {"mDataProp": "modalidad"},
     {"mDataProp": "nombre_curso"},
     {"mDataProp": "fecha_fin"},
     {"mDataProp": "empresa"},
     {"mDataProp": null, "bSortable": false, "mRender": function (o) {
     var html = "";
     html = '<a href="DepartamentoTecnico/generar/' + o.id_ficha + '" target="_blank" ><button class="btn btn-success">Generar Documento</button></a> ';
     return html;
     }
     }
     ],
     pageLength: 10,
     responsive: true,
     dom: '<"html5buttons"B>lTfgitp',
     buttons: [{
     extend: 'copy'
     }, {
     extend: 'csv'
     }, {
     extend: 'excel',
     title: 'Empresa'
     }, {
     extend: 'pdf',
     title: 'Sence'
     }, {
     extend: 'print',
     customize: function (win) {
     $(win.document.body).addClass('white-bg');
     $(win.document.body).css('font-size', '10px');
     $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
     }
     }]
     });
     
     **/
}

function cargaCombo(midata, id) {


    $("#" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

    $("#" + id + " option[value='0']").remove();
}

function llamaDatosCursobusqueda() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_busqueda').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            TablaFichas(result);
        },
        url: "DepartamentoTecnico/buscarCurso"
    });
}