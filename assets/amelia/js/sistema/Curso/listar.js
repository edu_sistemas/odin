$(document).ready(function () {

    llamaDatosCurso();

    $("#btn_nuevo").click(function () {
        location.href = 'Agregar';
    });

    $("#btn_buscar_curso").click(function () {
        llamaDatosCurso();
    });

    $("#cbx_modalidad").select2({
        placeholder: "Todos",
        allowClear: true
    });

    llamaDatosControles();
});

function EditarCursoEstado(id, estado) {
    // dataType: "json",text , html ,
    // url: "../EditarPerfil" : especifa la direccion donde quiero enviar la informacion
    // success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
    //  cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
    //result : contine el resultado del success
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_curso: id, estado_curso: estado},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(  textStatus + errorThrown);
        },
        success: function (result) {
            //console.log(result);
            llamaDatosCurso();
        },
        url: "Listar/EditarCursoEstado"
    });
}

function cargaTablaCursos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_curso')) {
        $('#tbl_curso').DataTable().destroy();
    }

    $('#tbl_curso').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "descripcion_curso"},
            {"mDataProp": "duracion_curso"},
            {"mDataProp": "nombre_modalidad"},
            {"mDataProp": "nombre_nivel"},
            {"mDataProp": "descripcion_version"},
            {"mDataProp": "codigo_sence"},
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    
                    if (o.estado_curso == 1) {
                        html = '<i title="Activo" class="fa fa-circle text-navy"><br>Activo</i>';
                    }else{
                        html = '  <i title="Inactivo"  class="fa fa-circle text-danger"><br>Inactivo</i>';
                    }
                    return html;
                }
            },
            { "mDataProp": "usuario_ultima_modificacion" },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu pull-right'>";
                    var x = "Editar/index/" + o.id_curso;
                    html += "<li><a href='" + x + "'>Modificar</a></li>";
                    /*   if (o.estado_curso == 1) {
                     html += "<li><a    onclick='EditarCursoEstado(" + o.id_curso + ",0);'>Desactivar</a></li>";
                     } else {
                     html += "<li><a    onclick='EditarCursoEstado(" + o.id_curso + ",1);'>Activar</a></li> ";
                     }*/
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Cursos'
            }, {
                extend: 'pdf',
                title: 'Curso'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}


function llamaDatosCurso() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_busqueda').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaCursos(result);
        },
        url: "Listar/buscarCurso"
    });
}


function llamaDatosControles() {


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'modalidad');
        },
        url: "Listar/getModalidad"
    });




    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'nivel');
        },
        url: "Listar/getNivel"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'version');
        },
        url: "Listar/getVersion"
    });
 

}


function cargaCombo(midata, id) {

    $("#cbx_" + id).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });


}
