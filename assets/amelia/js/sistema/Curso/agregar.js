$(document).ready(function () {
    $("#btn_registrar_curso").click(function () {
        if (validarRequeridoForm($("#frm_agregar_curso"))) {
            RegistraCurso();
        }
    });

    $("#cbx_version").change(function () {

        console.log($('#cbx_version').select2("val"));
    });

    contarCaracteres("descripcion_curso_v", "text-out", 200);

    llamaDatosControles();
});

function RegistraCurso() {

    /*   dataType: "json",text , html ,
     url: "Agregar/RegistraPerfil"" : especifa la direccion donde quiero enviar la informacion
     success : retorno el resultado otorgado por la url especificada cuando su ejecucion es correcta
     cuando esta presenta algun error pior el lado  servidor se ejecuta el Metodo error del AJAZX
     result : contine el resultado del success
     
     */

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            nombre_curso_v: $("#nombre_curso_v").val(),
            descripcion_curso_v: $("#descripcion_curso_v").val(),
            duracion_curso_v: $("#duracion_curso_v").val(),
            cbx_modalidad: $("#cbx_modalidad").val(),
            cbx_codigo_sence: $("#cbx_codigo_sence").val(),
            cbx_familia: $("#cbx_familia").val(),
            cbx_nivel: $("#cbx_nivel").val(),
            cbx_sub_linea_negocio: $("#cbx_sub_linea_negocio").val(),
            cbx_version: $('#cbx_version').select2("val"),
            cbx_slt_grado_curso: $("#cbx_slt_grado_curso").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(  textStatus + errorThrown);
            alerta('Curso', errorThrown, 'error');
        },
        success: function (result) {

            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {

                setTimeout(function () {
                    history.back();
                }, 2000);

            }
        },
        url: "Agregar/RegistraCurso"
    });

}

function llamaDatosControles() {





    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'modalidad');
        },
        url: "Agregar/getModalidad"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'familia');
        },
        url: "Agregar/getFamilia"
    });



    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'nivel');
        },
        url: "Agregar/getNivel"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, 'version');
        },
        url: "Agregar/getVersion"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'codigo_sence');
        },
        url: "Agregar/getCodigoSence"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'sub_linea_negocio');
        },
        url: "Agregar/getSubLineaNegocio"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, 'slt_grado_curso');
        },
        url: "Agregar/getGradoBono"
    });

}


function cargaCombo(midata, id) {

    $("#cbx_" + id).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });


}
