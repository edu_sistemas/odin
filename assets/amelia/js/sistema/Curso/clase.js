var listadoDoc;
var listaDocAsig;

var arrayIDSence;

$(document).ready(function () {
    listaFichas();
    llamaDatosClase();
/* consolidado
    $("#radio_eliminar").click(function () {
        if (arrayIDSence[0].id_consolidado_sence != '0') {
            $("#id_accion").val(arrayIDSence[0].id_consolidado_sence);
            $("#id_accion").prop('disabled', false);
            $("#btn_insert_accion_sence").prop('disabled', false);
        }
    });*/
    $("#radio_eliminar").click(function () {
        $("#agregar_container").hide();
        $("#btn_insert_accion_sence").hide();
        $("#btn_eliminar_accion_sence").show();
        $("#id_ingresadas").removeAttr("readonly");
        $("#id_ingresadas").removeAttr("multiple");
    });
    
    $("#radio_agregar").click(function () {
        $("#agregar_container").show();
        $("#btn_insert_accion_sence").show();
        $("#btn_eliminar_accion_sence").hide();
        $("#id_ingresadas").attr("readonly", true);
        $("#id_ingresadas").attr("multiple", true);
        if (arrayIDSence[0].id_accion_sence != '0') {
            if (arrayIDSence[0].cantidad_oc <= arrayIDSence.length) {
                $("#id_accion").val("Se ha registrado el máximo de ID's");
                $("#id_accion").prop('disabled', true);
                $("#btn_insert_accion_sence").prop('disabled', true);
            } else {
                $("#id_accion").val(""); 
            }
        }
    });

    $("#btn_insert_accion_sence_consolidado").click(function () {
        if (validarRequeridoForm($("#frm_id_accion_sence_consolidado"))) {
            ingresarIDAccionSenceConsolidado();
        }
    });

 
    $("#btn_generar_excel").click(function () {

        var id_ficha = $("#id_ficha_v").val();
        var url_final = "";


        if (id_ficha == "") {
            id_ficha = "0";
        }

        url_final = "Clase/generarExcel/" + id_ficha;

        window.open(url_final, '_blank');
    });


    $("#xbutton").click(function () {
        location.reload();
    });

    $("#reset").click(function () {
        location.reload();
    });

    $('#confirmard').click(function () {
        confirmarDocente();
       // location.reload();
    });
    $('#btn_derecha').click(function () {
        clonar();
    });

    $('#cerrarmodal').click(function () {
        llamaDatosClase();
    });
    
    $('#btn_izquierda').click(function () {
        var x = QuitarDocente();


        // if (x == 0) {
        $('#relatores_asignados_visible option:selected').remove().appendTo('#cbx_relatores_disponibles_visible');
        $('#relatores_asignados').html($('#relatores_asignados_visible').html());
        $('#cbx_relatores_disponibles').html($('#cbx_relatores_disponibles_visible').html());
        // } 

    });

    $("#btn_insert_accion_sence").click(function () {
        if (validarRequeridoForm($("#frm_id_accion_sence"))) {
            ingresarIDAccionSence();
        }
    });

    $("#btn_eliminar_accion_sence").click(function () {
        if (validarRequeridoForm($("#frm_id_accion_sence"))) {
            eliminarIDAccionSence();
        }
    });

    listadoDoc = $('#cbx_relatores_disponibles');
    $('#cbx_relatores_disponibles_visible').html(
            listadoDoc.html());
    listaDocAsig = $('#relatores_asignados');
    $('#relatores_asignados_visible').html(listaDocAsig.html());
    $("#btn_derecha").click(function () {
        AsignarRelatorClase();
    });
    // Botones para realizar busqueda
    PermiHidnn = $('#relatores_asignados');
    $('#relatores_asignados_visible').html(PermiHidnn.html());
    PermiHidnn2 = $('#cbx_relatores_disponibles');
    $('#cbx_relatores_disponibles_visible').html(
            PermiHidnn2.html());
    $('#search1').bind('keypress', function (e) {
        var inicio = $('#search1').val() + String.fromCharCode(e.keyCode);
        actualizaLista(inicio);
    });
    $('#search1').bind('keyup', function (e) {
        var actual = $('#search1').val().trim();
        if (actual == "") {
            var inicio = actual.substr(0, actual.length - 1);
            actualizaLista(inicio);
        }
    });
    $('#search2').bind('keypress', function (a) {
        var inicio2 = $('#search2').val() +
                String.fromCharCode(a.keyCode);
        actualizaLista2(inicio2);
    });
    $('#search2').bind('keyup', function (a) {
        var actual2 = $('#search2').val().trim();
        if (actual2 == "") {
            var inicio2 = actual2.substr(0, actual2.length - 1);
            actualizaLista2(inicio2);
        }
    });
    $("#id_ficha_v").select2({
        placeholder: "Todos",
        allowClear: true
    });
    $("#id_ficha_v").on("select2:unselecting", function (e) {
        llamaDatosClase();
    });
    $("#id_ficha_v").change(function () {
        if ($("#id_ficha_v").val() != "") {
            llamaDatosClase($("#id_ficha_v").val());
        } else {
            llamaDatosClase();
        }
    });
});

function listaFichas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result);
        },
        url: "Clase/getFichas"
    });
}

function clonar() {
    $('#cbx_relatores_disponibles_visible option:selected').remove().appendTo(
            '#relatores_asignados_visible');
    $('#cbx_relatores_disponibles').html(
            $('#cbx_relatores_disponibles_visible').html());
    $('#relatores_asignados').html($('#relatores_asignados_visible').html());
}

function cargaCombo(midata) {
    $("#id_ficha_v").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}

function actualizaLista2(inicio2) {
    var y = PermiHidnn2.clone();
    inicio2 = inicio2.toLowerCase();
    $('#cbx_relatores_disponibles_visible').html("");
    var verifica2 = inicio2.trim();
    if (verifica2.length == 0) {
        $('#cbx_relatores_disponibles_visible').html(
                $('#cbx_relatores_disponibles').html());
        return;
    }
    y.find('option[title^="' + inicio2 + '"]').each(function () {
        $('#cbx_relatores_disponibles_visible').append($(this));
    });
}

function actualizaLista(inicio) {
    var x = PermiHidnn.clone();
    inicio = inicio.toLowerCase();
    $('#relatores_asignados_visible').html("");
    var verifica = inicio.trim();
    if (verifica.length == 0) {
        $('#relatores_asignados_visible')
                .html($('#relatores_asignados').html());
        return;
    }
    x.find('option[title^="' + inicio + '"]').each(function () {
        $('#relatores_asignados_visible').append($(this));
    });
}

function cargaListaAsignados() {
    var j = listaDocAsig.clone();
}

function cargaListaDisponibles() {
    var y = listadoDoc.clone();
}

function cargaTablaClase(data) {

    if ($.fn.dataTable.isDataTable('#tbl_clase')) {
        $('#tbl_clase').DataTable().destroy();
    }

    $('#tbl_clase').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "descripcion_producto"},
            {"mDataProp": "cant_alumno"},
            {"mDataProp": "sala"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "fecha_fin"},
            {"mDataProp": "codigo_sence"},
            {"mDataProp": "consolidado"},
            {"mDataProp": "ids_sence"},
            {"mDataProp": "ultimo_relator"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    if (parseInt(o.cantidad_relatores) > 0) {
                        if (parseInt(o.relatores_confirmados) > 0) {
                            html += '<i class="fa fa-check-circle-o" style="font-size:14px; color: green; text-align:center;"></i>' + o.relatores_confirmados + ' confirmados<br>';
                        }
                        if (parseInt(o.relatores_sin_confirmar) > 0) {
                            html += '<i class="fa fa-check-circle-o" style="font-size:14px; color: yellow; text-align:center;"></i>' + o.relatores_sin_confirmar + ' sin confirmar';
                        }
                    } else {
                        html = '<i class="fa fa-check-circle-o" style="font-size:14px; color: red; text-align:center;"></i> Sin Relator(es)';
                    }
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (a) {
                    var html = "";
                    var disabled = "";
                    var boton = "class='btn btn-warning dropdown-toggle'";

                    /*  if (a.codigo_sence == null) {
                     disabled = "disabled";
                     } else {
                     disabled = "";
                     } */

                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-warning dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu pull-right'>";
                    var id_ficha = a.id_ficha;
                    html += "<li><a onclick='modal(" + id_ficha + ")'>Asignar Docente</a></li>";
                    html += "<li><a onclick='modal2(" + id_ficha + ")'>Consultar Horarios</a></li>";
                    /*  if (a.codigo_sence == null) {
                     html += "";
                     } else {*/
                    var x = "";

                    x = "'" + a.id_ficha + "','" + a.num_ficha + "','" + a.consolidado + "'";

                    // html += '<li class="' + disabled + '"><a href="#" onclick="setIdAccionConsolidado(' + x + ');">ID Consolidado</a></li>';
                    html += '<li class="' + disabled + '"><a href="#" onclick="setIdAccion(' + a.id_ficha + ', ' + a.num_ficha + ',\'' + a.consolidado + '\');">ID Acción Sence</a></li>';
                    html += "<li class='" + disabled + "'><a href='#' onclick='setIdAccionConsolidado(" + a.id_ficha + ", " + a.num_ficha + ",\"" + a.consolidado + "\");'>ID Acción Consolidado</a></li>";
                    // }
                    html += '<li><a href="../Curso/Clase/pdfResumen/' + id_ficha + '" target="_blank">Descargar PDF</a></li>';
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
                extend: 'copy'
            },
            {
                extend: 'csv'
            },
            {
                extend: 'excel',
                title: 'Clase'
            },
            {
                extend: 'pdf',
                title: 'Clase'
            },
            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        "order": [[0, "desc"]]
    });
}

function llamaDatosClase(id) {

    if (id == null) {
        id = "";
    }

    $.ajax({
        url: "Clase/claseListar",
        type: 'POST',
        dataType: "json",
        data: {
            id_ficha: id
        },
        beforeSend: function () {
            $("#loading-container").html('<img src="../../assets/img/timer.gif">');
        },
        success: function (result) {
            cargaTablaClase(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#loading-container').html("Error en la carga");
        },
        complete: function () {
            $("#loading-container").html("");
        },
        async: true,
        cache: false
    });
}

function modal(id_ficha) {
    $("#docente").modal();
    $("#id_ficha").val(id_ficha);
    listarRelatoresAsignados();
    listarRelatoresDisponibles();
    clonar();
}

function modal2(id_ficha) {
    $("#detalle_reserva").modal();
    // $("#id_ficha").val(id_ficha);
    DatosReservaBy(id_ficha);
}

function listarRelatoresDisponibles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#id_ficha").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // $('#id_ficha').val(result[0].id_ficha);
            cargaCBX("cbx_relatores_disponibles_visible", result);
            $("#cbx_relatores_disponibles_visible option[value='']").remove();
        },
        url: "Clase/listarRelatoresDisponibles"
    });
}

function listarRelatoresAsignados() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#id_ficha").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("relatores_asignados_visible", result);
            $("#relatores_asignados_visible option[value='']").remove();
            // clonar();
        },
        url: "Clase/listarRelatoresAsignados"
    });
}

function AsignarRelatorClase() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#id_ficha").val(),
            relatores_asignados_visible: $("#relatores_asignados_visible").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            resultado = result[0].respuesta;
            toastr.success(resultado);
        },
        url: "Clase/AsignardocenteClase" // Hace referencia a la funcion del
                // controlador que realiza la aacion
                // de eitar.
    });
}

function QuitarDocente() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#id_ficha").val(),
            relatores_asignados_visible: $("#relatores_asignados_visible").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            resultado = result[0].respuesta;
            toastr.warning(resultado);
           
        },
        url: "Clase/QuitarAsignacionDocente" // Hace referencia a la funcion
                // del controlador que realiza
                // la aacion de eitar.
    });
   
}

function confirmarDocente() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#id_ficha").val(),
            relatores_asignados_visible: $("#relatores_asignados_visible").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
         

            if (result.estado = 1) {
                $("#relatores_asignados_visible :selected").attr("selected", '');
            }
            // resultado = result[0].respuesta;
           
            // toastr.warning(resultado);
                swal({
                    title: result[0].titulo,
                    text: result[0].mensaje,
                    type: result[0].tipo_alerta,
                    showCancelButton: false,
                    confirmButtonClass: "btn-succes",
                    confirmButtonText: "Aceptar",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: true,
                    closeOnCancel: false
                },
                    function (isConfirm) {
                        if (isConfirm) {
                        location.reload();
                        }
                    });
               
                    

                
        },
        url: "Clase/confirmarEstado" // Hace referencia a la funcion del
                // controlador que realiza la aacion de
                // eitar.
    });
}

function DatosReservaBy(id_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id_ficha
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            cargaTblReservas(result);
        },
        url: "Clase/datosReserva"
    });
}

function cargaTblReservas(data) {
    var html = "";
    $('#detalle_reserva_tbl tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr>';
        html += ' <td><span class="label label-primary"> ' + item.nombre + '</span></td> ';
        html += ' <td class="text-navy">' + item.fecha + '</td> ';
        html += ' <td><i class="fa fa-clock-o"> </i> ' + item.inicio + '</td> ';
        html += '<td><i class="fa fa-clock-o"> </i> ' + item.termino + '</td> ';
        html += '</tr>';
    });
    $('#detalle_reserva_tbl tbody').html(html);
}

//METODOS PARA INGRESO ID SENCE

function setIdAccion(id_ficha, num_ficha, consolidado) {
    $("#agregar_container").show();
    $("#btn_insert_accion_sence").show();
    $("#btn_eliminar_accion_sence").hide();
    $("#id_ingresadas").attr("readonly", true);
    $("#id_ingresadas").attr("multiple", true);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            arrayIDSence = result;
            console.log(result);
            $("#modal_id_sence").modal({
                backdrop: 'static',
                keyboard: false
            });
            $("#modal_id_sence").modal("show");
            $("#id_ficha_modal_accion").html(num_ficha);
            $("#id_a_consolidado").html("ID Consolidado: " + consolidado);
            $("#id_ficha_s").val(id_ficha);
            $("#id_ingresadas").html("");

            $("#id_accion").val("");
            $("#id_accion").prop('disabled', false);
            $("#radio_agregar").prop('checked', true);
            $("#btn_insert_accion_sence").prop('disabled', false);


            if (result[0].cantidad_oc > 1){
                $("#consolidado-container").show();
            }else{
                $("#consolidado-container").hide();
            } 
            if ((result[0].cantidad_oc <= result.length) && (result[0].id_accion_sence != '0')) {
                $("#id_accion").val("Se ha registrado el máximo de ID's");
                $("#id_accion").prop('disabled', true);
                $("#btn_insert_accion_sence").prop('disabled', true);
            }

           /* if ($("#radio-id-consolidado").is(':checked')){
                $("#id_accion").val(result[0].id_consolidado_sence);
            }
*/
            $.each(result, function (i, item) {
                if (item.id_accion_sence != '0'){
                    $("#id_ingresadas").append("<option value=" + item.id_accion_sence + ">" + item.id_accion_sence + "</option>");
                }
            });
            /*   if (result.length == 0) {
             $("#combo_oc").prop('disabled', true);
             $("#id_accion").prop('disabled', true);
             $("#btn_insert_accion_sence").prop('disabled', true);
             alerta("ID Acción Sence", "los id acción sence ya se ingresaron o no existen ordenes de compras asosiadas a la ficha", "error");
             }*/
        },
        url: "Clase/getIDSenceByFicha"
    });
}

function ingresarIDAccionSence() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_id_accion_sence').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            // $("#cerrarmodal").trigger("click");
            setIdAccion($("#id_ficha_s").val(), $("#id_ficha_modal_accion").text(), $('#id_a_consolidado').text());
        },
        url: "Clase/setIDAccion"
    });
}

function eliminarIDAccionSence() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_id_accion_sence').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            // $("#cerrarmodal").trigger("click");
            setIdAccion($("#id_ficha_s").val(), $("#id_ficha_modal_accion").text(), $('#id_a_consolidado').text());
        },
        url: "Clase/delIDAccion"
    });
}

//METODOS PARA INGRESO ID SENCE CONSOLIDADO

function setIdAccionConsolidado(id_ficha, num_ficha, consolidado) {

    $("#modal_id_sence_consolidado").modal("show");
    $("#id_ficha_modal_accion_consolidado").text(num_ficha);
    $("#id_ficha_consolidado").val(id_ficha);

    $("#id_accion_consolidado").val(consolidado);


    $("#id_accion_consolidado").prop('disabled', false);
    $("#btn_insert_accion_sence_consolidado").prop('disabled', false);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id_ficha },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            if ((result[0].cantidad_oc <= 1)) {
                $("#id_accion_consolidado").val("Hay solo una orden de compra");
                $("#id_accion_consolidado").prop('disabled', true);
                $("#btn_insert_accion_sence_consolidado").prop('disabled', true);
            }

        },
        url: "Clase/getIDSenceByFicha"
    });

}

function ingresarIDAccionSenceConsolidado() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_id_accion_sence_consolidado').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $("#cerrarmodal2").trigger("click");
            llamaDatosClase();
        },
        url: "Clase/setIDAccionConsolidado"
    });
}
