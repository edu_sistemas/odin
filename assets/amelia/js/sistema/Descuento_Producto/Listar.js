$(document).ready(function () {
    recargaLaPagina();
    cargaCbxAll();
    listarDescuentoProducto();

    $('#btn_filtrar').click(function () {
        listarDescuentoProducto();
    });

    $('#btn_nuevo').click( function () {
        EditarDescuento();
    });

    $('#frm_nuevo_descuento').submit(function () {
        GuardarEditarDescuento();
        event.preventDefault();
    });

    $("#linea_negocio_filtro").change(function () {
        if ($("#linea_negocio_filtro").val() != null) {
            CargaSubLinea($("#linea_negocio_filtro").val(), "sub_linea_negocio_filtro");
        } else {
            CargaSubLinea("", "sub_linea_negocio_filtro");
        }
    });
    
    $("#sub_linea_negocio_filtro").change(function () {
        console.log($("#sub_linea_negocio_filtro").val());
        if ($("#sub_linea_negocio_filtro").val() != null) {
            $("#btn_filtrar").prop("disabled", false);
        } else {
            $("#btn_filtrar").prop("disabled", true);
            listarDescuentoProducto();
        }
    });
});

function cargaTablaDescuentos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_descuentos_productos')) {
        $('#tbl_descuentos_productos').DataTable().destroy();
    }
    $('#tbl_descuentos_productos')
        .DataTable(
        {
            "aaData": data,
            "aoColumns": [
                {
                    "mDataProp": "id"
                },
                {
                    "mDataProp": "linea_negocio"
                },
                {
                    "mDataProp": "sub_linea"
                },
                {
                    "mDataProp": "fecha_inicio"
                },
                {
                    "mDataProp": "valor_descuento"
                },
                {
                    "mDataProp": null,
                    "bSortable": false,
                    "mRender": function (o) {
                        var html = "";
                        html += '  <div class="btn-group">';
                        html += '  <button data-toggle="dropdown" class="btn btn-default dropdown-toggle ">Accion<span class="caret"></span></button>';
                        html += '  <ul class="dropdown-menu pull-right">';
                        html += '  <li ><a onclick="EditarDescuento(' + o.id + ')">Editar</a></li>';
                        html += ' </ul>';
                        html += ' </div>';
                        return html;
                    }
                }

            ],
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'Empresa'
                },
                {
                    extend: 'pdf',
                    title: 'Empresa'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass(
                            'white-bg');
                        $(win.document.body).css('font-size',
                            '10px');
                        $(win.document.body).find('table')
                            .addClass('compact').css(
                            'font-size', 'inherit');
                    }
                }]
        });
}

function cargaCombo(midata, item) {
    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
    $("#" + item).val(0).trigger("change");
}

function EditarDescuento(data) {
    $("#modal_modificar_descuento").modal('show');
    if (data > 0 && data != '') {
        $('#id_descuentos_producto').val(data);
        $("#group_linea_negocio").hide();
        $("#sub_linea_negocio").prop("required", false);

    } else {
        $("#sub_linea_negocio").prop("required", true);
        $("#group_linea_negocio").show();
        CargaSubLinea("", "sub_linea_negocio");
    }
}

function listarDescuentoProducto() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_filtrarxxxx').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            console.log(result);
            cargaTablaDescuentos(result);
        },
        url: "Listar/listarDescuentoProducto"
    });
}


function cargaCbxAll() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            console.log(result);
            cargaCombo(result, "linea_negocio_filtro");
        },
        url: "Listar/getLineaNegocio"
    });
    CargaSubLinea("", "sub_linea_negocio_filtro");
}

function CargaSubLinea(datos, who) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_linea: datos},
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $("#" + who).children('option').remove();
            cargaCombo(result, who);
        },
        url: "Listar/getSubLineaNegocio"
    });
}

function GuardarEditarDescuento(datos, who) {
    console.log($('#frm_nuevo_descuento').serialize());
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_nuevo_descuento').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $("#cerrarmodal").trigger("click");
            listarDescuentoProducto();
        },
        url: "Listar/editarDescuento"
    });
}



