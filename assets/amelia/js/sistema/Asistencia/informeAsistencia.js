$(document).ready(function () {

    recargaLaPagina();
    llamaDatosFichas();
    llamaDatosEmpresa();
    $("#btn_buscar_ficha").click(function () {
        llamaDatosFichas();
    });

    $("#reset").click(function () {
        location.reload();
    });

    $('#close_modal_detalle').click(function () {
        $('#modal_detalle').modal('toggle');
        $('.modal-backdrop').remove();
        $("#asistencia").show();
    });

    $('a#generaExcel').on('click', function () {
        var idficha = $(this).attr("ficha");
        var url = "../Asistencia/InformeAsistencia/generarExcel/" + idficha;
        window.open(url);
    });
});


function llamaDatosEmpresa() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            getSalaidCBXemp(result);
        },
        url: "InformeAsistencia/getEmpresas"
    });
}

function getSalaidCBXemp(midata) {
    $("#empresa_cbx").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}

function llamaDatosFichas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            mostrarFichas(result);
            // console.log(result);
        },
        url: "InformeAsistencia/getfichaData"
    });
}

function mostrarFichas(result) {
    //  llamaDatosCapsula(id); 
    $('#tbl_ficha').show();
    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }
    var t = $('#tbl_ficha').DataTable({
        data: result,
        "ordering": false,
        "aoColumns": [{
                "mDataProp": "num_ficha"
            },
            {
                "mDataProp": "nombre_curso"
            },
            {
                "mDataProp": "razon_social_empresa"
            },
            {
                "mDataProp": "inicio"
            },
            {
                "mDataProp": "termino"
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    var x = o.id_ficha;
                    html += "<li id='start' class='lis'><a onclick='modal(" + x + ")'>Consultar Asistencia</a></li>";
                    var url = "";
                    url = "../Asistencia/InformeAsistencia/generarExcel/" + x;
                    html += "<li id='start' class='lis'><a   href='" + url + "'>Descargar Asistencia</a></li>";
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        "order": [
            [1, 'asc']
        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresas'
            }, {
                extend: 'pdf',
                title: 'Empresas'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }]
    });
}


/**
 * Comment
 */
function descargarExcel(id) {

}

function llamaDatosAsistencia(id) {

    $.ajax({
        beforeSend: function () {},
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id: id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            mostrarAsistencia(result);
            $('#txt_nombre_curso').val(result[0]['nombre_curso']);
            $('#txt_duracio_horas').val(result[0]['horas']);
            $('#txt_nombre_dias').val(result[0]['dias_nombre']);
            $('#txt_horas').val(result[0]['horas']);
            $('#txt_inicio').val(result[0]['inicio']);
            $('#txt_termino').val(result[0]['termino']);
            $('#txt_alumnos').val(result[0]['cant_alumnos']);
            $('#txt_sala').val(result[0]['nombre_sala']);
        },
        url: "InformeAsistencia/getAsistencia"
    });
}

var counter = 1;

function newColum() {

    t.row.add([
        counter + '.1',
        counter + '.2',
        counter + '.3',
        counter + '.4'
    ]).draw(false);

    counter++;

}

function mostrarAsistencia(result) {
    //  llamaDatosCapsula(id); 
    $('#listaAlm').show();
    if ($.fn.dataTable.isDataTable('#listaAlm')) {
        $('#listaAlm').DataTable().destroy();
    }
    var t = $('#listaAlm').DataTable({
        data: result,
        "ordering": false,
        "aoColumns": [{
                "mDataProp": "rut"
            },
            {
                "mDataProp": "nombre"
            },
            {
                "mDataProp": "Asistencia"
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function (o) {
                    var html = "";
                    html += '<a onclick="modal_dtl(' + o.id_detalle_alumno + ',' + o.id_ficha + ')"<button class="btn btn-info btn-rounded" id="dtl_asistencia">Detalle Asistencia</button>';
                    return html;
                }
            }
        ],
        "order": [
            [1, 'asc']
        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Asistencia'
            }, {
                extend: 'pdf',
                title: 'Asistencia'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }]
    });
}

function modal(id) {
    $("#asistencia").modal();
    llamaDatosAsistencia(id);
}

function llamaDatosAsistenciaDetalle(id_ficha, id_alumno) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id_ficha,
            id_alumno: id_alumno

        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //((console.log(jqXHR);
        },
        success: function (result) {

            $('#titulo').text(result[0]['nombre']);
            mostrarAsistenciadetalle(result);
        },
        url: "InformeAsistencia/getAsistenciaDetalle"
    });
}

function modal_dtl(id_ficha, id_alumno) {
    $('#modal_detalle').modal({
        backdrop: 'static',
        keyboard: false
    });
    $("#asistencia").hide();
    llamaDatosAsistenciaDetalle(id_ficha, id_alumno);
}

function mostrarAsistenciadetalle(result) {

    var Largo = (result[0]['asistencia']).split(',');
    var Largo1 = Largo.length;
    console.log(Largo1);

    var html = "";

    $('#listaAlmdetalle tbody').html(html);
    $.each(result, function (i, item) {

        for (h = 0; h < Largo1; h++) {
            html += '<tr>';
            html += '<td><div><label>' + item.bloques.split(',')[h] + '</label></div></td>';

            var asistencia = item.asistencia.split(',')[h];
            if (asistencia == '1') {
                html += '<td><div class="form-group has-success"><label class="col-sm-2 control-label">Presente</label></div></td>';
            } else {
                html += '<td><div class="form-group has-error"><label class="col-sm-2 control-label">Ausente</label></div></td>';
            }
            html += '</tr>';
        }
    });

    $('#listaAlmdetalle tbody').html(html);
}