var fichas_tabla = [];
$(document).ready(function () {
    recargaLaPagina();
    llamaLibrosPersonalizados();

    $('#nuevaP').click(function () {
        caragamodalNuevoP();
        $('#modalNuevoP').modal('show');
    });
    $("#btn_agregar").click(function () {
        agregaFicha();
    });
    $('#modalNuevoP').on('hidden.bs.modal', function () {
        location.reload();
    });
    $('#modalModificarP').on('hidden.bs.modal', function () {
        location.reload();
    });
    $('#btn_agregar_modificar').click(function () {
        var ficha = $("#selectFichas_modificar option:selected").val();
        if (ficha == '') {
            return false;
        }
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_ficha: ficha, id_libro: $('#hiddenId').val()},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                // console.log(textStatus + errorThrown);
                console.log(jqXHR, textStatus, errorThrown);
            },
            success: function (result) {
                if (result[0].reset == 0) {
                    swal(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                }

                modificarP($('#hiddenId').val());
            },
            url: "PersoLibro/insertNewFicha"
        });
    });
    $("#guardarCambios").click(function () {
        guardarLibro();
    });
    $("#guradarCambios_modificar").click(function () {
        location.reload();
    });
});

function guardarLibro() {
    if ($('#nombre_perso').val() == '') {
        swal("Atención!", "Debe ingresar el nombre de la Personalización", "warning");
    } else {
        if (fichas_tabla.length < 2) {
            swal("Atención!", "Debe ingresar almenos 2 fichas", "warning");
        } else {
            //agregar la personalizacion a la bbdd
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {fichas: fichas_tabla, nombre: $('#nombre_perso').val()},
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    // console.log(textStatus + errorThrown);
                    console.log(jqXHR, textStatus, errorThrown);
                },
                success: function (result) {
                    swal({
                        title: "Libro Personalizado",
                        text: "Ha sido ingresado correctamente!",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Aceptar!",
                        closeOnConfirm: false
                    },
                            function () {
                                $('#nombre_perso').val('');
                                $('#table_fichas tbody').html('<tr><td colspan="6" style="text-align:center"><strong>No hay fichas Agregadas</strong></td></tr>');
                                fichas_tabla = [];
                                location.reload();
                            });
                },
                url: "PersoLibro/insertPerso"
            });
        }
    }
}

function agregaFicha() {

    var ficha = $("#selectFichas option:selected").val();
    var encontrado = false;
    if (fichas_tabla.length == 0) {
        getFichaByID(ficha);
    } else {
        for (var i = 0; i < fichas_tabla.length; i++) {
            if (fichas_tabla[i].id_ficha == ficha) {
                encontrado = true;
                break;
            }
        }
        if (encontrado) {
            swal('La ficha ya está en la tabla', '', 'error');
        } else {
            getFichaByID(ficha);
        }
    }


}

function getFichaByID(ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id: ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            if (fichas_tabla.length > 0) {
                if (result[0].fecha_inicio != fichas_tabla[0].fecha_inicio || result[0].nombre != fichas_tabla[0].nombre) {
                    swal('La fecha y el docente no coinciden', '', 'error');
                } else {
                    $('#table_fichas tbody tr:last').after('<tr><td>' + result[0].fichas + '</td><td>' + result[0].nombre_curso + '</td><td>' + result[0].fecha_inicio + '</td><td>' + result[0].fecha_fin + '</td><td>' + result[0].nombre + '</td><td style="text-align:center"><a class="btn btn-danger btn-rounded" onclick="eliminarFila(' + result[0].id_ficha + ',this);">X</a></td></tr>');
                    fichas_tabla.push(result[0]);
                }
            } else {
                $('#table_fichas tbody tr:last').after('<tr><td>' + result[0].fichas + '</td><td>' + result[0].nombre_curso + '</td><td>' + result[0].fecha_inicio + '</td><td>' + result[0].fecha_fin + '</td><td>' + result[0].nombre + '</td><td style="text-align:center"><a class="btn btn-danger btn-rounded" onclick="eliminarFila(' + result[0].id_ficha + ',this);">X</a></td></tr>');
                if (fichas_tabla.length == 0) {
                    $('#table_fichas tbody tr:first').remove();
                }
                fichas_tabla.push(result[0]);
            }
        },
        url: "PersoLibro/DatosFicha"
    });
}

function caragamodalNuevoP() {
    //buscar las fichas de los presenciales
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {



            $('#selectFichas').select2({
                placeholder: "Todos",
                allowClear: true,
                data: result
            });

        },
        url: "PersoLibro/ListarFichasPresenciales"
    });
    //   
    $('#modalNuevoP').modal('show');
}


function llamaLibrosPersonalizados() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            TablaLibros(result);
        },
        url: "PersoLibro/buscarLibro"
    });
}

function TablaLibros(data) {

    if ($.fn.dataTable.isDataTable('#tbl_libros')) {
        $('#tbl_libros').DataTable().destroy();
    }

    $('#tbl_libros').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "fichas"},
            {"mDataProp": "nombre_perso"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "fecha_fin"},
            {"mDataProp": "nombre"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";

                    html += '<li><a onclick="modificarP(' + o.id_libro_personalizado + ')">Modificar</a></li>';
                    html += '<li><a href="PersoLibro/generar/' + o.id_ficha + '" target="_blank" >Crear PDF</a> </li>';
                    html += '<li><a onclick="EliminarLibroP(' + o.id_libro_personalizado + ')">Eliminar</a></li>';



                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Sence'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
}
function eliminarFila(id, elem) {
    //alert(id);
    //removeItemFromArr(fichas_tabla, "" + id + "");
    for (var i = 0; i < fichas_tabla.length; i++) {
        if (fichas_tabla[i].id_ficha == id) {
            fichas_tabla.splice(i, 1);
            break;
        }
    }
    $(elem).parent().parent().remove();
    if (fichas_tabla.length == 0) {
        $('#table_fichas tbody').append('<tr><td colspan="6" style="text-align:center"><strong>No hay fichas Agregadas</strong></td></tr>');
    }
}

function modificarP(id_libro_personalizado) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {

            cargaCBX('selectFichas_modificar', result);
        },
        url: "PersoLibro/ListarFichasPresenciales"
    });

    $('#hiddenId').val(id_libro_personalizado);

    $("#selectFichas_modificar").select2();

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_libro_personalizado: id_libro_personalizado
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            //$('ocdoc').text(num_orden_compra);
            $("#modalModificarP").modal("show");
            $('#nombre_perso_modificar').val(result[0].nombre);
            $('#nombre_perso_modificar').prop('readonly', true);
            loadTableModificarP(result);
            if (result.length <= 2) {
                $(".btn-rounded").attr({"disabled": true, "onclick": ""});
            } else {
                $(".btn-rounded").attr("disabled", false);
            }
        },
        url: "persoLibro/getLibrosPersonalizadosByID"
    });
}

function loadTableModificarP(data) {
    var html = "";
    $('#table_fichas_modificar tbody').html(html);
    $
            .each(
                    data,
                    function (i, item) {
                        html += '<tr><td>';
                        html += item.num_ficha;
                        html += '</td><td>';
                        html += item.nombre_curso;
                        html += '</td><td>';
                        html += item.fecha_inicio;
                        html += '</td><td>';
                        html += item.fecha_fin;
                        html += '</td><td>';
                        html += item.relator;
                        html += '</td><td>';
                        html += '<a class="btn btn-danger btn-rounded" onclick="eliminarFicha(' + item.id_ficha_libro + ');">X</a>';
                        html += '</td></tr>';
                    });

    $('#table_fichas_modificar tbody').html(html);
}

function eliminarFicha(id_ficha_libro) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha_libro: id_ficha_libro
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            modificarP($('#hiddenId').val());

        },
        url: "persoLibro/deleteFichaLibro"
    });
}

function EliminarLibroP(id_libro_personalizado) {
    swal({
        title: "Está seguro?",
        text: "¿Desea eliminar Este Registro?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: true
    },
            function () {
                $.ajax({
                    async: false,
                    cache: false,
                    dataType: "json",
                    type: 'POST',
                    data: {
                        id_libro_personalizado: id_libro_personalizado
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // code en caso de error de la ejecucion del ajax
                        console.log(textStatus + errorThrown);
                    },
                    success: function (result) {
                        llamaLibrosPersonalizados();
                    },
                    url: "persoLibro/deleteLibro"
                });
            });
}