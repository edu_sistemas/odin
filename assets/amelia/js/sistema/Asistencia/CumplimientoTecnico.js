$(document).ready(function () {
    $("#confirmarR").click(function () {
        registrarAsistencia();
    });

    $("#btn_enviar_encuesta").click(function () {
        if (validarRequeridoForm($("#frm_encuesta"))) {
            registrarEncuesta();
        }
    });

    llamaDatosControles();

    // llamaDatosCapsula();

    $("#id_ficha_v").select2({
        placeholder: "Todos",
        allowClear: true
    });

    $("#id_ficha_v").change(function () {
        if ($("#id_ficha_v").val() != "") {
            llamaDatosCapsula($("#id_ficha_v").val());
        }
    });

    $('#capsulas').hide();

    // $('.i-checks').iCheck({
    //     checkboxClass: 'icheckbox_square-green',
    //     radioClass: 'iradio_square-green',
    // });

    $("#btn_todos_si").click(function(){
        $("#listaAlm input:radio").each(function () {
            if($(this).val() == 1){
                $(this).prop("checked", true);
                $(this).parent().removeClass('btn-outline');
            }else{
                $(this).prop("checked", false);
                $(this).parent().removeClass('btn-outline').addClass('btn-outline');
            }
        });
    });

    $('#listaAlm input:radio').addClass('input_hidden');
    $('#listaAlm .checkera').click(function () {
        $(this).removeClass('btn-outline').siblings().addClass('btn-outline');
    });

    $("#btn_todos_no").click(function () {
        $("#listaAlm input:radio").each(function () {
            if ($(this).val() == 0) {
                $(this).prop("checked", true);
                $(this).parent().removeClass('btn-outline');
            }else{
                $(this).prop("checked", false);
                $(this).parent().removeClass('btn-outline').addClass('btn-outline');
            }
        });
    });

});

function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        // data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result);
        },
        url: "Listar/getFichas"
    });
}

function cargaCombo(midata) {

    $("#id_ficha_v").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

}

function llamaDatosCapsula(id) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            mostrarCapsulas(result);

        },
        url: "Listar/getfichaData"
    });
}

function llamaDatosConfirmarAsistencia(id, capsula) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id,
            id_capsula: capsula
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // console.log(result);
            $('#nombre_curso').text(result[0].nombre_curso);

        },
        url: "Listar/getfichaData"
    });
}

function mostrarCapsulas(result) {

    // llamaDatosCapsula(id);

    $('#capsulas').show();

    if ($.fn.dataTable.isDataTable('#capsulas')) {
        $('#capsulas').DataTable().destroy();
    }

    var t = $('#capsulas')
            .DataTable(
                    {
                        data: result,
                        "ordering": false,
                        "aoColumns": [
                            {
                                "mDataProp": null
                            },
                            {
                                "mDataProp": "nombre_curso"
                            },
                            {
                                "mDataProp": "num_ficha"
                            },
                            {
                                "mDataProp": "horario_inicio"
                            },
                            {
                                "mDataProp": "horario_termino"
                            },
                            {
                                "mDataProp": null,
                                "bSortable": false,
                                "mRender": function (o) {

                                    var x = o.id_ficha;
                                    var y = o.id_capsula;
                                    var z = o.nombre_curso;
                                    var w = o.nombre_relator;
                                    var v = o.salaDirec;
                                    
                                    var html = "";
                                    if (o.encuesta == 0) {
                                        html += '<button data-toggle="dropdown" onclick="abrirEncuesta('
                                                + x
                                                + ', '
                                                + y
                                                + ', \''
                                                + z
                                                + '\', \''
                                                + w
                                                + '\', \''
                                                + v
                                                + '\');" class="btn btn-primary">Hacer Encuesta</button>';
                                    } else {
                                        html += "<pre>Encuesta Respondida</pre>";
                                    }

                                    return html;

                                }
                            }

                        ],
                        "columnDefs": [{
                                "searchable": false,
                                "orderable": false,
                                "targets": 0
                            }],
                        "order": [[1, 'asc']],
                        pageLength: 25,
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [
                            {
                                extend: 'copy'
                            },
                            {
                                extend: 'csv'
                            },
                            {
                                extend: 'excel',
                                title: 'Capsulas'
                            },
                            {
                                extend: 'pdf',
                                title: 'Capsulas'
                            },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass(
                                            'white-bg');
                                    $(win.document.body).css('font-size',
                                            '10px');

                                    $(win.document.body).find('table')
                                            .addClass('compact').css(
                                            'font-size', 'inherit');
                                }
                            }],
                    });

    t.on('order.dt search.dt', function () {
        t.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

}

function abrirEncuesta(id_ficha, capsula, nombre_curso, nombre_relator, salaDirec) {
    $("#capsulainput").val(capsula);
    $("#id_ficha").val(id_ficha);
    $("#nombre_curso").text(nombre_curso);
    $("#modal_encuesta_nombre_relator").append(nombre_relator);
    $("#modal_encuesta_sala_curso").append(salaDirec);
    


    $("#asistencia").modal();

    // llamaDatosConfirmarAsistencia(id, capsula);

    // llamaDatosAlumnos(id, capsula);
}

function llamaDatosAlumnos(id, capsula) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id,
            id_capsula: capsula
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // mostrartablaAlumnos(result);
        },
        url: "Listar/alumnosListar"
    });
}

function mostrartablaAlumnos(result) {

    $('#capsulainput').val(result[0]['id_capsula']);

    // llamaDatosCapsula(id);

    $('#listaAlm').show();

    if ($.fn.dataTable.isDataTable('#listaAlm')) {
        $('#listaAlm').DataTable().destroy();
    }
    var cnt = 0;
    var t = $('#listaAlm')
            .DataTable(
                    {
                        data: result,
                        "ordering": false,
                        "aoColumns": [
                            {
                                "mDataProp": null
                            },
                            {
                                "mDataProp": "rut"
                            },
                            {
                                "mDataProp": "nombre"
                            },
                            {
                                "mDataProp": null,
                                "bSortable": false,
                                "mRender": function (o) {
                                    var html = "";
                                    if (o.estado_asistencia === '1') {
                                        html += "<div  class='i-checks'>";
                                        html += "<input id='chkAst"
                                                + cnt
                                                + "' type='checkbox' checked value='1' name='chkAst"
                                                + cnt + "'>";
                                        html += "<i></i>";
                                    } else {
                                        html += "<div  class='i-checks'>";
                                        html += "<input id='chkAst"
                                                + cnt
                                                + "' type='checkbox' value='0' name='chkAst"
                                                + cnt + "'>";
                                        html += "<i></i>";
                                    }
                                    html += "<div class='btn-group'>";
                                    html += "<label>";
                                    html += "<input id='id_alm" + cnt
                                            + "' type='hidden' value='"
                                            + o.id_detalle_alumno
                                            + "' name='id_alm" + cnt + "'>";
                                    html += "<i></i>";
                                    html += "  </label>";
                                    html += "</div>";
                                    cnt++;
                                    return html;
                                }
                            }],
                        "columnDefs": [{
                                "searchable": false,
                                "orderable": false,
                                "targets": 0
                            }],
                        "order": [[1, 'asc']],
                        pageLength: 20000,
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        "bLengthChange": false,
                        buttons: [
                            {
                                extend: 'copy'
                            },
                            {
                                extend: 'csv'
                            },
                            {
                                extend: 'excel',
                                title: 'Capsulas'
                            },
                            {
                                extend: 'pdf',
                                title: 'Capsulas'
                            },
                            {
                                extend: 'print',
                                customize: function (win) {
                                    $(win.document.body).addClass(
                                            'white-bg');
                                    $(win.document.body).css('font-size',
                                            '10px');

                                    $(win.document.body).find('table')
                                            .addClass('compact').css(
                                            'font-size', 'inherit');
                                }
                            }],
                    });

    t.on('order.dt search.dt', function () {
        t.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $('#numRows').val(t.column(0).data().length);

}

function registrarEncuesta() {

    var noRespondido = false;

    for (var i = 1; i < 14; i++) {
        var r = $('input[name="r' + i + '"]:checked').val();

        if (r == null) {
            noRespondido = true;
            break;
        }
    }

    if (noRespondido) {
        alerta("Error", "Debe responder toda la encuesta", "error");
    } else {
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: $('#frm_encuesta').serialize(),
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                alerta('Asistencia', errorThrown, 'error');

            },
            success: function (result) {

                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

                if (result[0].reset == 1) {
                    $('#reset').trigger("click");
                }

            },
            url: "CumplimientoTecnico/RegistrarEncuesta"
        });

    }
}
