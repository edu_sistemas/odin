// JavaScript Document
$(document).ready(function () {
    recargaLaPagina();
    llamaLibros();

    $('.btnGenerarPDF').on('click', function () {

        var idficha = $(this).attr("ficha");
        $("." + idficha).trigger("change");
        $(".IDSence_" + idficha).trigger("change");


        var url = "LibroClases/generar/" + idficha;
        var relator = $("select." + idficha).find("option:selected").eq(0).text();
        var opcion_sence = $("select.IDSence_" + idficha).find("option:selected").eq(0).val();
        var encodedUrl = encodeURIComponent(relator);

        var url_final = "";

        url_final = url + "/" + encodedUrl + "/" + opcion_sence;
        window.open(url_final, '_blank');
    });

    $('.btnGenerarEncuesta').on('click', function () {

        var idficha = $(this).attr("ficha");
        $("." + idficha).trigger("change");
        $(".IDSence_" + idficha).trigger("change");


        var url = "LibroClases/generarEncuesta/" + idficha;
        var relator = $("select." + idficha).find("option:selected").eq(0).text();
        var opcion_sence = $("select.IDSence_" + idficha).find("option:selected").eq(0).val();
        var encodedUrl = encodeURIComponent(relator);

        var url_final = "";

        url_final = url + "/" + encodedUrl + "/" + opcion_sence;
        console.log(url_final);
        window.open(url_final, '_blank');
    });

    $('.btnGenerarFormularioAsignacion').on('click', function () {

        var idficha = $(this).attr("ficha");
        $("." + idficha).trigger("change");
        $(".IDSence_" + idficha).trigger("change");


        var url = "LibroClases/generarFormularioAsignacion/" + idficha;
        var relator = $("select." + idficha).find("option:selected").eq(0).text();
        var opcion_sence = $("select.IDSence_" + idficha).find("option:selected").eq(0).val();
        var encodedUrl = encodeURIComponent(relator);

        var url_final = "";

        url_final = url + "/" + encodedUrl + "/" + opcion_sence;
        console.log(url_final);
        window.open(url_final, '_blank');
    });

});
function myFunction(objeto) {
    var idficha = $(objeto).attr("ficha");
    $("." + idficha).trigger("change");
    $(".IDSence_" + idficha).trigger("change");


    var url = "LibroClases/generar/" + idficha;
    var relator = $("select." + idficha).find("option:selected").eq(0).text();
    var opcion_sence = $("select.IDSence_" + idficha).find("option:selected").eq(0).val();
    var encodedUrl = encodeURIComponent(relator);
    var url_final = "";

    url_final = url + "/" + encodedUrl + "/" + opcion_sence;

    //console.log(url_final);
    window.open(url_final, '_blank');
}
function myFunction2(objeto) {
    var idficha = $(objeto).attr("ficha");
    $("." + idficha).trigger("change");
    $(".IDSence_" + idficha).trigger("change");


    var url = "LibroClases/generarEncuesta/" + idficha;
    var relator = $("select." + idficha).find("option:selected").eq(0).text();
    var opcion_sence = $("select.IDSence_" + idficha).find("option:selected").eq(0).val();
    var encodedUrl = encodeURIComponent(relator);

    var url_final = "";

    url_final = url + "/" + encodedUrl + "/" + opcion_sence;

    //console.log(url_final);
    window.open(url_final, '_blank');
}
function llamaLibros() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            TablaLibros(result);
        },
        url: "LibroClases/buscarLibro"
    });
}

function TablaLibros(data) {

    if ($.fn.dataTable.isDataTable('#tbl_libros')) {
        $('#tbl_libros').DataTable().destroy();
    }

    $('#tbl_libros').DataTable({
        "aaData": data,
        paging: false,
        "aoColumns": [
            { "mDataProp": "num_ficha" },
            { "mDataProp": "nombre_curso" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "fecha_termino" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var nombres = o.nombre_relator.split(',');
                    var total = nombres.length;
                    //	console.log(o.num_ficha + ' Cantidad de profes -> ' + total);
                    html += '<select class="' + o.id_ficha + '" id="relatores" name="relatores">';
                    for (var x = 0; x < total; x++) {
                        html += '<option>' + nombres[x] + '</option>';
                    }
                    html += '</select>';
                    return html;
                }
            },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html += '<select class="IDSence_' + o.id_ficha + '" id="IDSence" name="IDSence">';
                    if (o.codigo_sence == null || o.codigo_sence == '') {

                        html += '<option value="0"> Sin ID Sence </option>';

                    } else {
                        html += '<option value="0"> Sin ID Sence </option>';
                        html += '<option value="1"> ID Consolidado </option>';
                        html += '<option value="2"> IDS DesConsolidado </option>';
                    }

                    html += '</select>';
                    return html;
                }
            },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var j = $("select#relatores" + o.id_ficha).find("option").eq(0).text();
                    $("input#" + o.id_ficha).val(j);
                    html += '<div class="dropdown">';
                    html += '<button class="btn btn-success dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-bars" aria-hidden="true"></i></button>';
                    html += '<ul class="dropdown-menu pull-right">';

                    html += '<li><a href="#" onclick="myFunction(this)" ficha="' + o.id_ficha + '" class="btnGenerarPDF"  >Generar Libro de Clases</a></li>';
                    html += '<li><a href="#" class="btnGenerarEncuesta" ficha="' + o.id_ficha + '">Generar Encuesta de Satisfacción</a></li>';
                    html += '<li><a href="#" class="btnGenerarFormularioAsignacion" ficha="' + o.id_ficha + '">Formulario Asignación Relator</a></li></ul></div>';
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Empresa'
        }, {
            extend: 'pdf',
            title: 'Sence'
        }, {
            extend: 'print',
            customize: function (win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }
        }],
        order: [0, "desc"]
    });
}