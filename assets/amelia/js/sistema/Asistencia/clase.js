$(document).ready(function() {
    recargaLaPagina();
    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    /*
        var url = window.location.href.split('/');

        $('#sala').val(url[7]);
        $('#reserva').val(url[8]);
    */
    $('#confirmarR').click(function() {
        if (validarRequeridoForm($("#frm_clase"))) {
            registrarClase();
        }
    });

    $('#reset').click(function() {
        location.href = "../../../Listar";
    });

});

/**
 * función para enviar los datos del form
 */
function registrarClase() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_clase').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            //cargaCombo(result, "direccion_empresa");
            swal({
                    title: result[0].titulo,
                    text: result[0].mensaje,
                    type: result[0].tipo_alerta,
                    confirmButtonText: "Ok"
                },
                function(isConfirm) {
                    if (isConfirm) {
                        history.back();
                    }
                });

        },
        url: "../../registrarClase"
    });
}