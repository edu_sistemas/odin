$(document).ready(function() {
    //	recargaLaPagina();
    $('#mensaje').hide();
    $('#reset_superior').hide();
    $('#btn_agregar_alumnos').hide();
    $('#btn_agregar_quitar_clase').hide();

    $("#confirmarR").click(function() {

        if (validarRequeridoForm($("#frmLista"))) {
            $('#select_relator').attr('disabled', false);
            registrarAsistencia();
            $('#select_relator').attr('disabled', true);
        }
    });

    $("#confirmarEditSala").click(function() {

        if (validarRequeridoForm($("#frmeditaSalas"))) {
            registrarSala();
            //alert('esto algun dia lo programaré');
        }
    });

    $('#reset_superior').click(function() {
        location.reload();
        $("#id_ficha_v").prop("disabled", false);
    });

    llamaDatosControles();
    //    llamaDatosCapsula();
    $("#id_ficha_v").select2({
        placeholder: "Todos",
        allowClear: true
    });

    $('#nuevoAlumno').hide();
    $('#btn_add_alumno').click(function() {
        $('#nuevoAlumno').slideToggle("slow");
    });

    $("#btn_agregar_quitar_clase").click(function() {

        var id_sala = $('#sala').val();
        var id_reserva = $('#reserva').val();

        location.href = 'Clase/Index/' + id_sala + '/' + id_reserva;
    });

    $("#id_ficha_v").change(function() {

        //$("#id_ficha_v").prop("disabled", true);
        //var html1 = "";
        //html1 += '<div class="alert alert-info" role="alert"> ';
        //html1 += '<strong> Para consultar otra ficha, presione el botón Volver.</strong>';
        //html1 += '<p></p>';
        //html1 += '</div>';
        //$('#mensaje').html(html1);
        //$('#mensaje').show();
        //$('#reset_superior').show();
        $('#btn_agregar_alumnos').show();
        $('#btn_agregar_quitar_clase').show();

        if ($("#id_ficha_v").val() != "") {
            llamaDatosCapsula($("#id_ficha_v").val());
        }
    });
    $('#capsulas').hide();

    $('.clockpicker').clockpicker({
        donetext: 'Listo'
    });

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('#confirmarEdit').click(function() {
        if (validarRequeridoForm($("#frmeditaHorarios"))) {
            CapsulaEditSave();
        }
    });
    $('#mensajerut').hide();
    $('#guardar_datos').click(function() {

        if (validarRequeridoForm($("#frm_Agregar_alumno"))) {
            var rut = $('#txt_rut').val();
            var dv = $('#txt_dv').val();
            if (rut == '' || dv == '') {
                $('#mensajerut').show();
            } else {
                $('#mensajerut').hide();
                agregaralumnoExtra();
            }
        }

    });

    $('#txt_rut').keyup(function() {
        comprobarRut();
    });
    $('#txt_dv').keyup(function() {
        comprobarRut();
    });
});

function llamaDatosControles() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, 'id_ficha_v');
        },
        url: "Listar/getFichas"
    });
}

function cargaCombo(midata) {
    $('#id_ficha_v').select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}

function cargaCombo2(data) {


    var items = "";
    $("#select_relator").html(items);
    items = ' <option value="" readonly selected>Seleccionen relator...</option> ';
    $.each(data, function() {
        items += "<option value='" + this.id + "'>" + this.text + "</option>";
    });
    $("#select_relator").append(items);
}

function llamaDatosCapsula(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            mostrarCapsulas(result);
            //$('#sala').val(result[0].nombre);
            $('#sala').val(result[0].id_sala);
            $('#reserva').val(result[0].id_detalle_sala);


        },
        url: "Listar/getfichaData"
    });
}

function llamaDatosConfirmarAsistencia(id, capsula) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id,
            id_capsula: capsula
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            // console.log(result);
            $('#nombre_curso').text(result[0].nombre_curso);

        },
        url: "Listar/getfichaData"
    });
}

function mostrarCapsulas(result) {
    //  llamaDatosCapsula(id); 

    if ($.fn.dataTable.isDataTable('#capsulas')) {
        $('#capsulas').DataTable().destroy();
    }
    $('#capsulas').show();
    var t = $('#capsulas').DataTable({
        data: result,
        "ordering": false,
        "aoColumns": [{
                "mDataProp": null,
                "sWidth": "5%"
            },
            {
                "mDataProp": "nombre_curso"
            },
            {
                "mDataProp": "salaDirec"
            },
            {
                "mDataProp": "num_ficha"
            },
            {
                "mDataProp": "horario_inicio"
            },
            {
                "mDataProp": "horario_termino"
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "sClass": "text-center",
                "sWidth": "7%",
                "mRender": function(p) {
                    var html = "";
                    var asistencia = p.asistencia;
                    if (asistencia > 0) {
                        html += "<i class='fa fa-check-circle' style='font-size:20px;color:green;'></i>";
                    } else {
                        html += "<i class='fa fa-times-circle' style='font-size:20px;color:red;'></i>";
                    }
                    return html;
                }
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    html += "<div class='btn-group'>";
                    html += "<button data-toggle='dropdown' class='btn btn-outline btn-success dropdown-toggle'>Acción<span class='caret'></span></button>";
                    html += "<ul id='list_accion' class='dropdown-menu  pull-right'>";
                    var x = o.id_ficha;
                    var y = o.id_capsula;
                    var zz = o.id_sala;
                    var aa = o.id_detalle_sala;
                    var bb = o.id_disponibilidad_detalle;
                    html += "<li><a onclick='modal(" + x + "," + y + ")'>Pasar Asistencia</a></li>";
                    var now = o.fecha_bd;
                    var reserva = o.inicio_completo;
                    if (reserva >= now) {
                        html += "<li><a onclick='modalHorario(" + y + ")'>Editar Horario de clases</a></li>";
                    }
                    //html += "<li><a onclick='modalSala(" + zz + "," + aa + "," + bb + ")'>Editar Sala</a></li>";
                    html += "<li><a onclick='eliminarSala(" + y + ")'>Eliminar Clase</a></li>";
                    html += "</ul>";
                    html += "</div>";
                    return html;
                }
            }
        ],
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0
        }],
        "order": [
            [1, 'asc']
        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Capsulas'
        }, {
            extend: 'pdf',
            title: 'Capsulas'
        }, {
            extend: 'print',
            customize: function(win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
            }
        }]
    });
    t.on('order.dt search.dt', function() {
        t.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1;
        });
    }).draw();
}

/**
 * Comment
 */
function modalHorario(capsula) {
    $('#edidcion_horario').modal();
    llamaDatosCapsulaEdit(capsula);
}
/*function modalSala(id_sala, id_detalle_sala, id_disponibilidad_detalle) {

    $('#edicion_sala').modal();
    llamadatosSala(id_sala, id_detalle_sala, id_disponibilidad_detalle);

}*/
function eliminarSala(id_capsula) {
    var res;
    /*swal({
     title: "Precaución",
     text: "¿Esta seguro de eliminar el bloque?",
     type: "warning",
     confirmButtonText: "Si, eliminar",
     showCancelButton: true,
     cancelButtonText: "NO"
     },
     function (isConfirm) {
     if (isConfirm) {
     
     res = eliminarBloque(id_disponibilidad_detalle);
     swal(res[0].titulo,res[0].mensaje,res[0].tipo_alerta);
     
     }
     });*/
    swal({
        title: "Precaución",
        text: "¿Esta seguro de eliminar el bloque?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: "Si, eliminar"
    }).then(function() {
        res = eliminarBloque(id_capsula);
        swal(res[0].titulo, res[0].mensaje, res[0].tipo_alerta);
    });
}

function eliminarBloque(id_capsula) {
    //alert("debo hacer eliminar esto");
    var res;
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {

            id_c: id_capsula
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function(result) {
            res = result;
            //console.log(result);			
            if ($("#id_ficha_v").val() != "") {
                llamaDatosCapsula($("#id_ficha_v").val());
            }
        },
        url: "Listar/eliminarSala"
    });
    return res;
}

function llamadatosSala(id_sala, id_detalle_sala, id_disponibilidad_detalle) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {

        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function(result) {
            $('#sala_edit option').remove();
            $('#detalle_sala').val(id_detalle_sala);
            $('#disponibilidad_detalle').val(id_disponibilidad_detalle);
            //console.log(result);
            for (var i = 0; i < result.length; i++) {
                if (id_sala == result[i].id_sala) {
                    $('#sala_edit').append($('<option>', {
                        value: result[i].id_sala,
                        text: result[i].nombre,
                        selected: true
                    }));
                } else {
                    $('#sala_edit').append($('<option>', {
                        value: result[i].id_sala,
                        text: result[i].nombre
                    }));
                }
            }
            //console.log(result);

        },
        url: "Listar/editarSala"
    });

}



/*function registrarSala() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frmeditaSalas').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            //console.log(result);
            $('#edicion_sala').modal('hide');
            if ($("#id_ficha_v").val() != "") {
                llamaDatosCapsula($("#id_ficha_v").val());
            }

        },
        url: "Listar/editarSalaSave"
    });


}*/

function llamaDatosCapsulaEdit(capsula) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {

            id_capsula: capsula
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function(result) {
            $('#txt_hora_inicio_editar').val(result[0]['hora_inicio']);
            $('#txt_hora_termino_editar').val(result[0]['hora_termino']);
            $('#fecha_inicio_edit').val(result[0]['fecha_inicio']);
            $('#capsule_id').val(capsula);
        },
        url: "Listar/editarHorario"
    });
}

function CapsulaEditSave() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frmeditaHorarios').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function(result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                confirmButtonColor: '#3085d6',
                confirmButtonText: "Ok"
            }).then(function() {
                $('#edidcion_horario').modal('toggle');
                location.href = "../Asistencia/Listar";
            });

        },
        url: "Listar/editarHorarioSave"
    });
}

function modal(id, capsula) {
    // $('#asistencia').trigger("reset");
    $("#asistencia").modal();
    llamaDatosConfirmarAsistencia(id, capsula);
    llamaRelatores(id);
    llamaDatosAlumnos(id, capsula);

}

function llamaDatosAlumnos(id, capsula) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id,
            id_capsula: capsula
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function(result) {
            mostrartablaAlumnos(result);
            if (result.length > 0) {
                $('#fecha_clase').text(result[0].clase);
                $('#clase_fin').text(result[0].clase_fin);
                $('#id_oc').val(result[0].orden_compra);
                $('#id_capsula_alm').val(result[0].id_capsula);
                $('#id_relator_alm').val(result[0].id_relator);
                $('#id_ficha_alm').val(result[0].id_ficha);
                if (result[0].id_relator) {
                    $('#select_relator').val(result[0].id_relator);
                    $('#select_relator').attr('disabled', true);
                } else {
                    $('#select_relator').attr('disabled', false);
                }
            } else {
                $('#listaAlm').DataTable().destroy();
            }

        },
        url: "Listar/alumnosListar"
    });
}

function llamaRelatores(id) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo2(result);

            if (result[0] == null) {
                $('#confirmarR').prop('disabled', true);
                $('#select_relator').hide();
                var html2 = "";
                html2 += '<div class="alert alert-danger" role="alert"> ';
                html2 += '<strong> No se puede pasar lista, si no hay relator asignado al curso !</strong>';
                html2 += '<p></p>';
                html2 += '</div>';
                // agrega los encabezados de las columnas    
                $('#mensaje').html(html2);
            } else {
                var html2 = "";
                $('#confirmarR').prop('disabled', false);
                $('#mensaje').html(html2);
                $('#select_relator').show();
            }
        },
        url: "Listar/getRelatoresCBX"
    });
}

function mostrartablaAlumnos(result) {
    if (result.length > 0) {
        $('#capsulainput').val(result[0]['id_capsula']);
    }
    //  llamaDatosCapsula(id); 
    $('#listaAlm').show();
    if ($.fn.dataTable.isDataTable('#listaAlm')) {
        $('#listaAlm').DataTable().destroy();
    }
    var cnt = 0;
    var cnt2 = 0;

    var ta = $('#listaAlm').DataTable({
        data: result,
        "ordering": false,
        "aoColumns": [{
                "mDataProp": null
            },
            {
                "mDataProp": "rut"
            },
            {

                "mDataProp": "nombre"
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    if (o.estado_asistencia == 1 || (o.estado_asistencia == null && o.atrasado == null)) {
                        html += "<div  class='i-checks'>";
                        html += "<input id='chkAst" + cnt + "' type='checkbox' checked value='1' name='chkAst" + cnt + "'>";
                        html += "<i></i>";
                    } else {
                        html += "<div  class='i-checks'>";
                        html += "<input id='chkAst" + cnt + "' type='checkbox' value='0' name='chkAst" + cnt + "'>";
                        html += "<i></i>";
                    }
                    html += "<div class='btn-group'>";
                    html += "<label>";
                    html += "<input id='id_alm" + cnt + "' type='hidden' value='" + o.id_detalle_alumno + "' name='id_alm" + cnt + "'>";
                    html += "<i></i>";
                    html += "  </label>";
                    html += "</div>";
                    cnt++;
                    if (o.num_orden_compra == 'O.C Alumno Extra') {
                        html += "<p class='alert alert-danger'>El alumno no figura en una O.C. que se pueda facturar.</p>";
                    }
                    return html;
                }
            },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    if (o.atrasado === '1') {
                        html += "<div  class='i-checks'>";
                        html += "<input id='chkatrasado" + cnt2 + "' type='checkbox' checked value='1' name='chkatrasado" + cnt2 + "'>";
                        html += "<i></i>";
                    } else {
                        html += "<div  class='i-checks'>";
                        html += "<input id='chkatrasado" + cnt2 + "' type='checkbox' value='0' name='chkatrasado" + cnt2 + "'>";
                        html += "<i></i>";
                    }
                    cnt2++;
                    return html;
                }
            }
        ],
        "columnDefs": [{
            "searchable": false,
            "orderable": false,
            "targets": 0
        }],
        "order": [
            [1, 'asc']
        ],
        pageLength: 20000,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        "bLengthChange": false,
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Capsulas'
        }, {
            extend: 'pdf',
            title: 'Capsulas'
        }, {
            extend: 'print',
            customize: function(win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                    .addClass('compact')
                    .css('font-size', 'inherit');
            }
        }]
    });

    ta.on('order.dt search.dt', function() {
        ta.column(0, {
            search: 'applied',
            order: 'applied'
        }).nodes().each(function(cell, h) {
            cell.innerHTML = h + 1;
        });
    }).draw();
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    });
    $('#numRows').val(ta.column(0).data().length);
}

function registrarAsistencia() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frmLista').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //alerta('Asistencia', errorThrown, 'Debe seleccionar un relator !');
        },
        success: function(result) {
            $('#asistencia').modal('hide');
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                confirmButtonColor: '#3085d6',
                confirmButtonText: "Ok"
            }).then(function() {
                if ($("#id_ficha_v").val() != "") {
                    llamaDatosCapsula($("#id_ficha_v").val());
                }
            });

        },
        url: "Listar/alumnosRegistrarLista/"
    });
}

function comprobarRut() {

    //     ALGORITMO 3, propiedades de la división por 11

    // 1. Multiplicar cada dígito del RUT se por 9, 8, ..., 4, 9, 8, ... de atrás hacia adelante.
    // 2. Sumar las multiplicaciones parciales.
    // 3. Suma alternada de la lista reversa de los dígitos del resultado anterior.
    // 4. El Dígito Verificador es el resultado anterior. Si es 10, se cambia por 'k'.


    // EJEMPLO.  RUT: 11.222.333

    // 1.   1   1   2   2   2   3   3   3  <--  RUT
    //    * 8   9   4   5   6   7   8   9  <--  9, 8, 7, 6, 5, 4, 9, 8, ...
    //    --------------------------------------
    //      8   9   8  10  12  21  24  27

    // 2. SUMA: 8 + 9 + 8 + 10 + 12 + 21 + 24 + 27 = 119

    // 3. SUMA ALTERNADA:  119 -> 9 - 1 + 1 = 9

    // 4. 9 <-- DÍGITO VERIFICADOR

    var rut = $("#txt_rut").val();
    var dv_form = $("#txt_dv").val();

    var out_print = "";

    if (rut.trim() == "" || dv_form.trim() == "") {
        out_print = "Debe ingresar RUT y DV";
    } else {
        if (rut != "" && dv_form != "") {
            out_print = "";

            if (dv_form == "k") {
                dv_form = "10";
            }
            var rutArr = [];
            var secuencia = [9, 8, 7, 6, 5, 4, 9, 8];

            for (var i = 0; i < rut.length; i++) {
                rutArr.push(parseInt(rut.charAt(i)));
            }
            rutArr.reverse();

            var xParciales = 0;

            for (var i = 0; i < rutArr.length; i++) {
                xParciales = xParciales + (secuencia[i] * rutArr[i]);
            }
            var str_xParciales = xParciales.toString();
            var xParcialesArr = [];
            for (var i = 0; i < str_xParciales.length; i++) {
                xParcialesArr.push(parseInt(str_xParciales.charAt(i)));
            }
            xParcialesArr.reverse();

            var dvSumas = 0;
            var dvRestas = 0;

            for (var i = 0; i < xParcialesArr.length; i++) {
                if ((i + 1) % 2 == 0) {
                    dvRestas = dvRestas - xParcialesArr[i];
                } else {
                    dvSumas = dvSumas + xParcialesArr[i];
                }
            }
            var dv = dvSumas + dvRestas;
            if (dv < 0 || dv > 9) {
                dv = 10;
            }
            if (dv == parseInt(dv_form)) {
                out_print = "";
            } else {
                out_print = "No se ha ingresado un rut válido";
            }
        } else {
            out_print = "No se ha ingresado un rut válido";
        }
    }
    document.getElementById("out_print").innerHTML = out_print;
}

function agregaralumnoExtra() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_Agregar_alumno').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //alerta('Asistencia', errorThrown, 'Debe seleccionar un relator !');
        },
        success: function(result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                confirmButtonText: "Ok"
            }).then(function() {
                location.href = "../Asistencia/Listar";
            });
        },
        url: "Listar/alumnoExtraRegistrar"
    });
}