// JavaScript Document

/* Ultima edicion: 2018 - 04 - 04[Jessica Roa]<jroa@edutecno.com>
 * Fecha creacion: 2018 - 04 - 04[Jessica Roa]<jroa@edutecno.com>
 */
$(document).ready(function () {
    $("#btn_buscar_ficha").click(function () {
        llamaFichas();
    });
    llamaFichas();
    llamaFichasHoy()
});

function myFunction(objeto) {
    var idficha = $(objeto).attr("ficha");
    var objetado = $(objeto).attr("objetado");

    if(objetado == 1){
        swal({
            title: "Advertencia",
            text: "La ficha se encuentra en estado objetado. Sólo debe gestionarla en el caso de que el curso se este llevando a cabo efectivamente.",
            type: "warning",
            showCancelButton: false,
            confirmButtonText: "Aceptar",
            closeOnConfirm: true
        },
            function () {
                var url = "../Asistencia/InformePrimerDia/generar/" + idficha;
                window.open(url, '_blank');
            });
    }else{
        var url = "../Asistencia/InformePrimerDia/generar/" + idficha;
        window.open(url, '_blank');
    }

    
}


function llamaFichas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            TablaFichas(result);
        },
        url: "InformePrimerDia/fichas"
    });
}

function llamaFichasHoy() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            tablaHoy(result);
        },
        url: "InformePrimerDia/fichasHoy"
    });
}

function TablaFichas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }

    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [

            { "mDataProp": "num_ficha" },
            { "mDataProp": "modalidad" },
            { "mDataProp": "nombre_curso" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "fecha_fin" },
            { "mDataProp": "empresa" },
            {
                "mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '<button id="btnGenerarPDF"  onclick="myFunction(this)" ficha="' + o.id_ficha + '" objetado="'+o.objetado+'" class="btn btn-success" title="Descargar"><i class="fa fa-cloud-download"></i> Descargar Lista</button>';
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Sence'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
        , order: [0, 'desc']
    });
}

function tablaHoy(data) {
    
    var contenido = "";
    $.each(data, function (i, item) {
            contenido += '<tr>';
            contenido += '<td>';
            contenido += item.num_ficha;
            contenido += '</td>';
            contenido += '<td>';
            contenido += item.sala;
            contenido += '</td>';
            contenido += '<td>';
            contenido += item.nombre_curso;
            contenido += '</td>';
            contenido += '<td>';
            contenido += item.empresa;
            contenido += '</td>';
            contenido += '<td style="background-color:'+item.fondo_hora_inicio+';">';
            contenido += item.hora_inicio;
            contenido += '</td>';
            contenido += '<td style="background-color:'+item.fondo_hora_termino+';">';
            contenido += item.hora_termino;
            contenido += '</td>';
            contenido += '<td>';
        contenido += '<button id="btnGenerarPDF"  onclick="myFunction(this)" ficha="' + item.id_ficha + '" objetado="' + item.objetado +'"  class="btn btn-success" title="Descargar"><i class="fa fa-cloud-download"></i> Descargar Lista</button>';
            contenido += '</td>';
            contenido += '</tr>';
    });
    $('#tbl_hoy tbody').html(contenido);
}