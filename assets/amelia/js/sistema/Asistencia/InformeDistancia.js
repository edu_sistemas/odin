// JavaScript Document
$(document).ready(function () {
    recargaLaPagina();
    $("#btn_buscar_ficha").click(function () {
        llamaFichas();
    });
    cargaCBXall();

    llamaFichas();





});
function myFunction(objeto) {
    var idficha = $(objeto).attr("ficha");
    $("." + idficha).trigger("change");
    $(".centro_costo" + idficha).trigger("change");

    $(".notas_" + idficha).trigger("change");
    $(".asistencia_" + idficha).trigger("change");

    var url = "../Asistencia/InformeDistancia/generar/" + idficha;
    // var orden_compra = $("select." + idficha).find("option:selected").eq(0).text();
    var orden_compravalue = $("select." + idficha).find("option:selected").eq(0).val();
    var centro_costo_value = $("select.centro_costo" + idficha).find("option:selected").eq(0).val();

    var notas_value = $("select.notas_" + idficha).find("option:selected").eq(0).val();
    var asistencia_value = $("select.asistencia_" + idficha).find("option:selected").eq(0).val();

    centro_costo_value = centro_costo_value.split(',');
    var centro_costo_value1 = centro_costo_value[0];
    var centro_costo_value2 = centro_costo_value[1];

    var url_final = "";

    url_final = url + "/" + orden_compravalue + "/" + centro_costo_value1 + "/" + centro_costo_value2 + "/" + notas_value + "/" + asistencia_value;

    //console.log(url_final);
    window.open(url_final, '_blank');
}

function myFunction2(objeto) {
    var idficha = $(objeto).attr("ficha");
    var id_modalidad = $(objeto).attr("id_modalidad");
    $("." + idficha).trigger("change");
    $(".centro_costo" + idficha).trigger("change");

    var url = "../Asistencia/InformeDistancia/generarDJ/" + idficha +"/"+id_modalidad;
    var orden_compra = $("select." + idficha).find("option:selected").eq(0).text();
    var orden_compravalue = $("select." + idficha).find("option:selected").eq(0).val();
    var centro_costo_value = $("select.centro_costo" + idficha).find("option:selected").eq(0).val();
    centro_costo_value = centro_costo_value.split(',');
    var centro_costo_value1 = centro_costo_value[0];
    var centro_costo_value2 = centro_costo_value[1];

    var url_final = "";

    url_final = url + "/" + orden_compravalue + "/" + centro_costo_value1 + "/" + centro_costo_value2;

    //console.log(url_final);
    window.open(url_final, '_blank');
}
function llamaFichas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {

            TablaFichas(result);
        },
        url: "InformeDistancia/fichas"
    });
}

function cargaCBXall() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            cargaCornete(result, "empresa");
        },
        url: "InformeDistancia/getEmpresasCBX"
    });
}

function cargaCornete(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();

    if ("otic" == item) {
        $("#otic option[value='1']").remove();
    }

}

function TablaFichas(data) {
    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }
    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [

            {"mDataProp": "num_ficha"},
            {"mDataProp": "modalidad"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "fecha_fin"},
            {"mDataProp": "empresa"},

            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var id_orden_compra = o.id_orden_compra.split(',');
                    var num_orden_compra = o.num_orden_compra.split(',');

                    //	console.log(o.num_ficha + ' Cantidad de profes -> ' + total);
                    html += '<select class="' + o.id_ficha + '" id="ordenes_compra" name="ordenes_compra">';
                    for (var x = 0; x < id_orden_compra.length; x++) {
                        html += '<option value="' + id_orden_compra[x] + '">' + num_orden_compra[x] + '</option>';
                    }
                    html += '</select>';
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html += '<select class="centro_costo' + o.id_ficha + '" id="centro_costo" name="centro_costo">';
                    html += '<option value="1,6">Sence</option>';
                    html += '<option value="2,0">Empresa</option>';
                    html += '<option value="3,0">Becado</option>';
                    html += '<option value="4,0">Eliminados</option>';
                    html += '</select>';

                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html += '<select class="notas_' + o.id_ficha + '" id="notas" name="notas">';
                    html += '<option value="1">SÍ</option>';
                    html += '<option value="0">NO</option>';
                    html += '</select>';

                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '<select class="asistencia_' + o.id_ficha + '" id="asistencia" name="asistencia">';

                    if (o.codigo_sence == null || o.codigo_sence == '') {
                        html += '<option value="0">Interna</option>';
                    } else {
                        html += '<option value="1">Sence</option>';
                        html += '<option value="0">Interna</option>';
                        html += '<option value="2">Según CC.CC</option>';
                    }
                    html += '</select>';
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '<button id="btnGenerarPDF" onclick="myFunction(this)" ficha="' + o.id_ficha + '" class="btn btn-success" title="Descargar"><i class="fa fa-cloud-download"></i></button>';
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                html += '<button id="btnDejota" onclick="myFunction2(this)" ficha="' + o.id_ficha + '" id_modalidad="' + o.id_modalidad + '" class="btn btn-success" title="Descargar">DJ <i class="fa fa-cloud-download"></i></button>';
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Sence'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }], order: [0, 'desc']
    });
}