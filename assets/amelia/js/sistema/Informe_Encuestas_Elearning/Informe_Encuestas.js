var d = new Date();
$( document ).ready(function() {
    cargaDeCBX();

    //Controla el menú activo de <nav>
    $('.botones-nav').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
        $('.dropdown li').siblings().removeClass('active');
        $('#render-html').html("");
    });
    $('.dropdown li').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
        $('.dropdown').siblings().removeClass('active');
        $(this).parent().parent().addClass('active');
        $('#render-html').html("");
    });

    $("#select_sala").prop("disabled", true);
    $('#select_sede').change(function () {
        if ($('#select_sede').val() != "") {
            $("#select_sala").prop("disabled", false);
            cargaSalas();
        } else {
            $("#select_sala").select2({
                placeholder: "Seleccione",
                allowClear: true,
                data: [{}]
            });
            $("#select_sala").prop("disabled", true);
        }
    });

    //Se crea una variable de tipo DATE para rescatar el año actual y mostrar año actual y años anteriores en combobox
    var n = d.getFullYear();
    for (var i = n; i >= n-5; i--) {
        $('#select_year').append('<option value="'+i+'">'+i+'</option>');
    }

    //Al cargar la página, se selecciona el informe por ficha por defecto
    $(window).load(function(){
        $('#porFicha_formulario').removeClass('hidden');
        $('#porFicha_formulario').siblings().addClass('hidden');
        $('#titulo_seccion').text('Informe Por Ficha');
    });

    //Metodos para mostrar/esconder div's al hacer click en el menú <nav>
    $("#porFicha").click(function(){
        $('#porFicha_formulario').removeClass('hidden');
        $('#porFicha_formulario').siblings().addClass('hidden');
        $('#titulo_seccion').text('Informe Por Ficha');
    });
    $("#porFechaDeCierre").click(function(){
        $('#porFechaDeCierre_formulario').removeClass('hidden');
        $('#porFechaDeCierre_formulario').siblings().addClass('hidden');
        $('#titulo_seccion').text('Informe Por Fecha de Cierre');
    });
    $("#porTutor").click(function(){
        $('#porTutor_formulario').removeClass('hidden');
        $('#porTutor_formulario').siblings().addClass('hidden');
        $('#titulo_seccion').text('Informe Por Tutor');
    });
    $("#porYear").click(function(){
        $('#porYear_formulario').removeClass('hidden');
        $('#porYear_formulario').siblings().addClass('hidden');
        $('#titulo_seccion').text('Ranking Anual Tutor');
    });
    $("#porMes").click(function(){
        $('#porMes_formulario').removeClass('hidden');
        $('#porMes_formulario').siblings().addClass('hidden');
        $('#titulo_seccion').text('Ranking Mensual Tutor');
    });
    $("#porEdutecno").click(function(){
        $('#porEdutecno_formulario').removeClass('hidden');
        $('#porEdutecno_formulario').siblings().addClass('hidden');
        $('#titulo_seccion').text('Informe Por Empresa Edutecno');
    });
    $("#busquedaPersonalizada").click(function(){
        $('#busquedaPersonalizada_formulario').removeClass('hidden');
        $('#busquedaPersonalizada_formulario').siblings().addClass('hidden');
        $('#titulo_seccion').text('Busqueda Personalizada');
    });
 

    //Métodos de cada formulario al dispararse el evento 'submit'
    $('#frm_buscar_por_ficha').submit(function(){
        $.ajax({
            url: "Informe_Encuestas/comprobarFicha",
            type: 'POST',
            dataType: "json",
            data: $('#frm_buscar_por_ficha').serialize(),
            beforeSend: function(){
                $("#cargando").html("Un momento por favor");
            },
            success: function (result) {
                if(result.length > 0){
                    $.ajax({
                        url: "Informe_Encuestas/get_resultados_encuesta_satisfaccion_por_ficha_elearning",
                        type: 'POST',
                        dataType: "json",
                        data: $('#frm_buscar_por_ficha').serialize(),
                        beforeSend: function () {
                            $("#cargando").html("Un momento por favor");
                        },
                        success: function (result) {
                            if (result.length > 0){
                                cargaInformePorFicha(result);
                            }else{
                                $('#render-html').html("Ficha es Presencial!");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            // code en caso de error de la ejecucion del ajax
                            //  console.log(textStatus + errorThrown);
                            $('#render-html').html("Error en la búsqueda de la ficha");
                        },
                        complete: function () {
                            $("#cargando").html("");
                        },
                        async: true,
                        cache: false
                    });
                }else{
                    $('#render-html').html("Ficha no encontrada");
                }
                
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                $('#render-html').html("Error en la búsqueda de la ficha");
            },
            complete: function(){
                $("#cargando").html("");
            },
            async: true,
            cache: false
        });
    event.preventDefault();
    });
    $('#frm_buscar_por_fecha_de_cierre').submit(function(){
        $.ajax({
            url: "Informe_Encuestas/get_resultados_encuesta_satisfaccion_por_fecha_de_cierre",
            type: 'POST',
            dataType: "json",
            data: $('#frm_buscar_por_fecha_de_cierre').serialize(),
            beforeSend: function(){
                $("#cargando").html("Un momento por favor");
            },
            success: function (result) {
                cargaInformePorFechaDeCierre(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                $('#render-html').html("Error en la búsqueda de la ficha");
            },
            complete: function(){
                $("#cargando").html("");
            },
            async: true,
            cache: false
        });
    event.preventDefault();
    });
    $('#frm_buscar_por_tutor').submit(function(){
        $.ajax({
            url: "Informe_Encuestas/get_resultados_encuesta_satisfaccion_por_tutor",
            type: 'POST',
            dataType: "json",
            data: $('#frm_buscar_por_tutor').serialize(),
            beforeSend: function(){
                $("#cargando").html("Un momento por favor");
            },
            success: function (result) {
                cargaInformePorTutor(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                $('#render-html').html("Error en la búsqueda de la ficha");
            },
            complete: function(){
                $("#cargando").html("");
            },
            async: true,
            cache: false
        });
    event.preventDefault();
    });
    $('#frm_ranking_tutor_anual').submit(function(){
        $.ajax({
            url: "Informe_Encuestas/get_resultados_anual_encuesta_satisfaccion",
            type: 'POST',
            dataType: "json",
            data: $('#frm_ranking_tutor_anual').serialize(),
            beforeSend: function(){
                $("#cargando").html("Un momento por favor");
            },
            success: function (result) {
                cargaRankingAnual(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                $('#render-html').html("Error en la búsqueda de la ficha");
            },
            complete: function(){
                $("#cargando").html("");
            },
            async: true,
            cache: false
        });
    event.preventDefault();
    });
    $('#frm_ranking_tutor_mensual').submit(function(){
        $.ajax({
            url: "Informe_Encuestas/get_resultados_mensual_encuesta_satisfaccion",
            type: 'POST',
            dataType: "json",
            data: $('#frm_ranking_tutor_mensual').serialize(),
            beforeSend: function(){
                $("#cargando").html("Un momento por favor");
            },
            success: function (result) {
                cargaRankingMensual(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                $('#render-html').html("Error en la búsqueda de la ficha");
            },
            complete: function(){
                $("#cargando").html("");
            },
            async: true,
            cache: false
        });
    event.preventDefault();
    });
    $('#frm_buscar_por_edutecno').submit(function(){
        $.ajax({
            url: "Informe_Encuestas/get_resultados_encuesta_satisfaccion_por_edutecno",
            type: 'POST',
            dataType: "json",
            data: $('#frm_buscar_por_edutecno').serialize(),
            beforeSend: function(){
                $("#cargando").html("Un momento por favor");
            },
            success: function (result) {
                cargaInformePorEdutecno(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
                $('#render-html').html("Error en la búsqueda de la ficha");
            },
            complete: function(){
                $("#cargando").html("");
            },
            async: true,
            cache: false
        });
    event.preventDefault();
    });
    $('#frm_busqueda_personalizada').submit(function(){
            $.ajax({
                url: "Informe_Encuestas/get_resultados_encuesta_satisfaccion_personalizada",
                type: 'POST',
                dataType: "json",
                data: $('#frm_busqueda_personalizada').serialize(),
                beforeSend: function(){
                    $("#cargando").html("Un momento por favor");
                },
                success: function (result) {
                    cargaBusquedaPersonalizada(result);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    //  console.log(textStatus + errorThrown);
                    $('#render-html').html("Error en la búsqueda");
                },
                complete: function(){
                    $("#cargando").html("");
                },
                async: true,
                cache: false
            });
    event.preventDefault();
    });
    


    //DatePicker especial para rango de fechas
    $('#data_5 .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    //DateIcker para fechas con solo mes y año
    $('#calendario_mes, #calendario_mes_x, #calendario_mes_e, #calendario_mes_f').datepicker({
        format: "mm-yyyy",
        minViewMode: 1,
        maxViewMode: 1,
        todayBtn: false,
        language: "es"
    });

    // evento keypress bloqueado para que no ingresen valores por teclado
    $('#calendario_mes, #calendario_mes_x, #calendario_mes_e, #calendario_mes_f').keypress(function(e) {
        e.preventDefault();
    });

});

function cargaInformePorFicha(data) {
    var contenido = "";
    $('#render-html').html(contenido);
    if(data.length == 0){
        contenido = "Ficha no encontrada";
    }else{
        contenido += '<img id="pdf-icon"src="../../assets/img/icon-pdf.ico" style="width:40px; cursor:pointer;" class="pull-right" onclick="generatePDF(\'table_buscar_por_ficha\',0);" />';
        contenido += '<table id="table_buscar_por_ficha" class="table table-hover" style="border: 1px solid #e7eaec;">';
        contenido += '<tr><th>Ficha</th><th>'+data[0].num_ficha+'</th></tr>';
        contenido += '<tr><th>Tutor</th><th>'+data[0].tutor+'</th></tr>';
        contenido += '<tr><th>Empresa</th><th>'+data[0].empresa+'</th></tr>';
        contenido += '<tr><th>Impresión General</th>';
        contenido += data[0].impresion_general < 4 ? '<th style="color:red;">'+data[0].impresion_general : '<th style="color:blue;">'+data[0].impresion_general;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Curso</th>';
        contenido += data[0].sobre_el_curso < 4 ? '<th style="color:red;">'+data[0].sobre_el_curso : '<th style="color:blue;">'+data[0].sobre_el_curso;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Manual</th>';
        contenido += data[0].sobre_el_manual < 4 ? '<th style="color:red;">'+data[0].sobre_el_manual : '<th style="color:blue;">'+data[0].sobre_el_manual;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Tutor</th>';
        contenido += data[0].sobre_el_tutor < 4 ? '<th style="color:red;">'+data[0].sobre_el_tutor : '<th style="color:blue;">'+data[0].sobre_el_tutor;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Instituto</th>';
        contenido += data[0].sobre_el_instituto < 4 ? '<th style="color:red;">'+data[0].sobre_el_instituto : '<th style="color:blue;">'+data[0].sobre_el_instituto;
        contenido += '</th></tr>';
        contenido += '<tr style="font-size:150%;"><th><b>Promedio Total Ficha</b></th>';
        contenido += data[0].promedio_total_ficha < 4 ? '<th style="color:red;">'+data[0].promedio_total_ficha : '<th style="color:blue;">'+data[0].promedio_total_ficha;
        contenido += '</th></tr>';
        contenido += '<tr><th><b>Total encuestas ingresadas</b></th>';
        contenido += '<th>'+data[0].total_encuestas_ingresadas;
        contenido += '</th></tr>';
        contenido += '</table>';
        contenido += '<hr>';
        contenido += '<div style="text-align:left; font-size:120%;">';
        contenido += '<h3><u>Comentarios de los alumnos</u></h3>'
        contenido += '<table class="table" id="table_comentarios">';
        contenido += '<tr><th></th><td></td></tr>';
        var x = 0;
        $.each( data, function( i, item ) {
            if(item.comentarios != ''){
                contenido += '<tr><th>'+(x+1)+'</th><td><i>"'+item.comentarios+'"</i></td></tr>';
                x++;
            }
        });
        contenido += '</table>';
    }
    $('#render-html').html(contenido);
}
function cargaDeCBX(){
    $.ajax({
        url: "Informe_Encuestas/get_tutores_cbx",
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $("#cargando").html("Un momento por favor");
        },
        success: function (result) {
            cargaCombo(result, 'select_tutor');
            cargaCombo(result, 'select_tutor_x');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#render-html').html("Error en la carga de los tutores");
        },
        complete: function(){
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });

    $.ajax({
        url: "Informe_Encuestas/get_edutecno_cbx",
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $("#cargando").html("Un momento por favor");
        },
        success: function (result) {
            cargaCombo(result, 'select_edutecno');
            cargaCombo(result, 'select_edutecno_2');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#render-html').html("Error en la carga de las empresas edutecno");
        },
        complete: function(){
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });

    $.ajax({
        url: "Informe_Encuestas/get_empresa_cbx",
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $("#cargando").html("Un momento por favor");
        },
        success: function (result) {
            cargaCombo(result, 'select_empresa');
            cargaCombo(result, 'select_empresa_y');
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#render-html').html("Error en la carga de las empresas");
        },
        complete: function(){
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });

    $.ajax({
        url: "Informe_Encuestas/get_ejecutivo_cbx",
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $("#cargando").html("Un momento por favor");
        },
        success: function (result) {
            cargaCombo(result, 'select_ejecutivo');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#render-html').html("Error en la carga de los ejecutivos comerciales");
        },
        complete: function(){
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });

    $.ajax({
        url: "Informe_Encuestas/get_sede_cbx",
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $("#cargando").html("Un momento por favor");
        },
        success: function (result) {
            cargaCombo(result, 'select_sede');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#render-html').html("Error en la carga de las sedes");
        },
        complete: function(){
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });

    $.ajax({
        url: "Informe_Encuestas/getCodeSenceCBX",
        type: 'POST',
        dataType: "json",
        beforeSend: function(){
            $("#cargando").html("Un momento por favor");
        },
        success: function (result) {
            cargaCombo(result, 'codigo_curso');
            cargaCombo(result, 'codigo_curso_y');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#render-html').html("Error en la carga de los ejecutivos comerciales");
        },
        complete: function(){
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });
}

function cargaInformePorFechaDeCierre(data){
    var contenido = "";
    $('#render-html').html(contenido);
    if(data.length == 0){
        contenido = "No existen registros según su criterio de búsqueda";
    }else{
        contenido += '<img id="pdf-icon" src="../../assets/img/icon-pdf.ico" style="width:40px; cursor:pointer;" class="pull-right" onclick="generatePDF(\'table_buscar_por_fecha_de_cierre\',1);" />';
        contenido += '<table id="table_buscar_por_fecha_de_cierre" class="table table-hover" style="border: 1px solid #e7eaec;">';
        contenido += '<thead>';
        contenido += '<tr>';
        contenido += '<th>N° de Ficha</th>';
        contenido += '<th>Tutor</th>';
        contenido += '<th>Empresa</th>';
        contenido += '<th>Fecha de Inicio</th>';
        contenido += '<th>Fecha de Cierre</th>';
        contenido += '<th>Total Encuestas</th>';
        contenido += '<th>Impresión General</th>';
        contenido += '<th>Sobre el Curso</th>';
        contenido += '<th>Sobre el Manual</th>';
        contenido += '<th>Sobre el Tutor</th>';
        contenido += '<th>Sobre el Instituto</th>';
        contenido += '<th>Promedio Ficha</th>';
        contenido += '<th></th>';
        contenido += '</tr>';
        contenido += '</thead>';
        contenido += '<tbody>';
        $.each( data, function( i, item ) {
                contenido += '<tr>';
                contenido += '<td>';
                contenido += item.num_ficha;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.tutor;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.empresa;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.fecha_inicio;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.fecha_cierre;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.total_encuestas_ingresadas;
                contenido += '</td>';
                contenido += item.impresion_general < 4 ? '<td style="color:red;">'+item.impresion_general: '<td>'+item.impresion_general;
                contenido += '</td>';
                contenido += item.sobre_el_curso < 4 ? '<td style="color:red;">'+item.sobre_el_curso: '<td>'+item.sobre_el_curso;
                contenido += '</td>';
                contenido += item.sobre_el_manual < 4 ? '<td style="color:red;">'+item.sobre_el_manual: '<td>'+item.sobre_el_manual;
                contenido += '</td>';
                contenido += item.sobre_el_tutor < 4 ? '<td style="color:red;">'+item.sobre_el_tutor: '<td>'+item.sobre_el_tutor;
                contenido += '</td>';
                contenido += item.sobre_el_instituto < 4 ? '<td style="color:red;">'+item.sobre_el_instituto: '<td>'+item.sobre_el_instituto;
                contenido += '</td>';
                contenido += item.promedio_total_ficha < 4 ? '<td style="color:red;"><b>'+item.promedio_total_ficha: '</b><td><b>'+item.promedio_total_ficha+'</b>';
                contenido += '</td>';
        });
        contenido += '</tbody>';
        contenido += '</table>';
    }
    $('#render-html').html(contenido);
}

function cargaInformePorTutor(data) {
    var contenido = "";
    $('#render-html').html(contenido);
    if(data.length == 0){
        contenido = "No existen registros según su criterio de búsqueda";
    }else{
        contenido += '<img id="pdf-icon" src="../../assets/img/icon-pdf.ico" style="width:40px; cursor:pointer;" class="pull-right" onclick="generatePDF(\'table_buscar_por_tutor\',2);" />';
        contenido += '<table id="table_buscar_por_tutor" class="table table-hover" style="border: 1px solid #e7eaec;">';
        contenido += '<tr><th>Tutor</th><th>'+data[0].tutor+'</th></tr>';
        contenido += '<tr><th>Impresión General</th>';
        contenido += data[0].impresion_general < 4 ? '<th style="color:red;">'+data[0].impresion_general : '<th style="color:blue;">'+data[0].impresion_general;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Curso</th>';
        contenido += data[0].sobre_el_curso < 4 ? '<th style="color:red;">'+data[0].sobre_el_curso : '<th style="color:blue;">'+data[0].sobre_el_curso;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Manual</th>';
        contenido += data[0].sobre_el_manual < 4 ? '<th style="color:red;">'+data[0].sobre_el_manual : '<th style="color:blue;">'+data[0].sobre_el_manual;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Tutor</th>';
        contenido += data[0].sobre_el_tutor < 4 ? '<th style="color:red;">'+data[0].sobre_el_tutor : '<th style="color:blue;">'+data[0].sobre_el_tutor;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Instituto</th>';
        contenido += data[0].sobre_el_instituto < 4 ? '<th style="color:red;">'+data[0].sobre_el_instituto : '<th style="color:blue;">'+data[0].sobre_el_instituto;
        contenido += '</th></tr>';
        contenido += '<tr style="font-size:150%;"><th><b>Promedio Total Ficha</b></th>';
        contenido += data[0].promedio_total_ficha < 4 ? '<th style="color:red;">'+data[0].promedio_total_ficha : '<th style="color:blue;">'+data[0].promedio_total_ficha;
        contenido += '</th></tr>';
        contenido += '<tr><th><b>Total encuestas ingresadas</b></th>';
        contenido += '<th>'+data[0].total_encuestas_ingresadas;
        contenido += '</th></tr>';
        contenido += '</table>';
        contenido += '<hr>';
    }
    $('#render-html').html(contenido);
}

function cargaRankingAnual(data){
    var contenido = "";
    $('#render-html').html(contenido);
    if(data.length == 0){
        contenido = "No existen registros según su criterio de búsqueda";
    }else{
        contenido += '<img id="pdf-icon" src="../../assets/img/icon-pdf.ico" style="width:40px; cursor:pointer;" class="pull-right" onclick="generatePDF(\'table_ranking_anual\',3);" />';
        contenido += '<table id="table_ranking_anual" class="table table-hover" style="border: 1px solid #e7eaec;text-align:left;">';
        contenido += '<thead>';
        contenido += '<tr>';
        contenido += '<th>Posición</th>';
        contenido += '<th>Tutor</th>';
        contenido += '<th>Total Encuestas</th>';
        contenido += '<th>Promedio Tutor</th>';
        contenido += '</tr>';
        contenido += '</thead>';
        contenido += '<tbody>';
        $.each( data, function( i, item ) {
            if(item.comentarios != ''){
                contenido += '<tr>';
                contenido += '<td>'+(i+1)+'°</td>';
                contenido += '<td>'+item.tutor+'</td>';
                contenido += '<td>' +item.total_encuestas_ingresadas+'</td>';
                contenido += item.sobre_el_tutor < 4 ? '<td style="color:red;">'+item.sobre_el_tutor: '<td>'+item.sobre_el_tutor;
                contenido += '</td></tr>';
            }
        });
        contenido += '</tbody>';
        contenido += '</table>';
    }
    $('#render-html').html(contenido);
}

function cargaRankingMensual(data){
    var contenido = "";
    $('#render-html').html(contenido);
    if(data.length == 0){
        contenido = "No existen registros según su criterio de búsqueda";
    }else{
        contenido += '<img id="pdf-icon" src="../../assets/img/icon-pdf.ico" style="width:40px; cursor:pointer;" class="pull-right" onclick="generatePDF(\'table_ranking_mensual\',4);" />';
        contenido += '<table id="table_ranking_mensual" class="table table-hover" style="border: 1px solid #e7eaec;text-align:left;">';
        contenido += '<thead>';
        contenido += '<tr>';
        contenido += '<th>Posición</th>';
        contenido += '<th>Tutor</th>';
        contenido += '<th>Total Encuestas</th>';
        contenido += '<th>Promedio Tutor</th>';
        contenido += '</tr>';
        contenido += '</thead>';
        contenido += '<tbody>';
        $.each( data, function( i, item ) {
            if(item.comentarios != ''){
                contenido += '<tr>';
                contenido += '<td>'+(i+1)+'°</td>';
                contenido += '<td>'+item.tutor+'</td>';
                contenido += '<td>' +item.total_encuestas_ingresadas+'</td>';
                contenido += item.sobre_el_tutor < 4 ? '<td style="color:red;">'+item.sobre_el_tutor: '<td>'+item.sobre_el_tutor;
                contenido += '</td></tr>';
            }
        });
        contenido += '</tbody>';
        contenido += '</table>';
    }
    $('#render-html').html(contenido);
}

function cargaInformePorEdutecno(data) {
    var contenido = "";
    $('#render-html').html(contenido);
    if(data.length == 0 || data[0].total_encuestas_ingresadas == 0){
        contenido = "No existen registros según su criterio de búsqueda";
    }else{
        contenido += '<img id="pdf-icon" src="../../assets/img/icon-pdf.ico" style="width:40px; cursor:pointer;" class="pull-right" onclick="generatePDF(\'table_empresa_edutecno\',5);" />';
        contenido += '<table id="table_empresa_edutecno" class="table table-hover" style="border: 1px solid #e7eaec;">';
        contenido += '<tr><th>Empresa Edutecno</th><th>'+data[0].edutecno+'</th></tr>';
        contenido += '<tr><th>Impresión General</th>';
        contenido += data[0].impresion_general < 4 ? '<th style="color:red;">'+data[0].impresion_general : '<th style="color:blue;">'+data[0].impresion_general;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Curso</th>';
        contenido += data[0].sobre_el_curso < 4 ? '<th style="color:red;">'+data[0].sobre_el_curso : '<th style="color:blue;">'+data[0].sobre_el_curso;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Manual</th>';
        contenido += data[0].sobre_el_manual < 4 ? '<th style="color:red;">'+data[0].sobre_el_manual : '<th style="color:blue;">'+data[0].sobre_el_manual;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Tutor</th>';
        contenido += data[0].sobre_el_tutor < 4 ? '<th style="color:red;">'+data[0].sobre_el_tutor : '<th style="color:blue;">'+data[0].sobre_el_tutor;
        contenido += '</th></tr>';
        contenido += '<tr><th>Sobre el Instituto</th>';
        contenido += data[0].sobre_el_instituto < 4 ? '<th style="color:red;">'+data[0].sobre_el_instituto : '<th style="color:blue;">'+data[0].sobre_el_instituto;
        contenido += '</th></tr>';
        contenido += '<tr style="font-size:150%;"><th><b>Promedio Total Ficha</b></th>';
        contenido += data[0].promedio_total_ficha < 4 ? '<th style="color:red;">'+data[0].promedio_total_ficha : '<th style="color:blue;">'+data[0].promedio_total_ficha;
        contenido += '</th></tr>';
        contenido += '<tr><th><b>Total encuestas ingresadas</b></th>';
        contenido += '<th>'+data[0].total_encuestas_ingresadas;
        contenido += '</th></tr>';
        contenido += '</table>';
        contenido += '<hr>';
    }
    $('#render-html').html(contenido);
}

function cargaBusquedaPersonalizada(data){
    var contenido = "";
    $('#render-html').html(contenido);
    if(data.length == 0){
        contenido = "No existen registros según su criterio de búsqueda";
    }else{
        contenido += '<img id="pdf-icon" src="../../assets/img/icon-pdf.ico" style="width:40px; cursor:pointer;" class="pull-right" onclick="generatePDF(\'table_busqueda_personalizada\',6);" />';
        contenido += '<table id="table_busqueda_personalizada" class="table table-hover" style="border: 1px solid #e7eaec;">';
        contenido += '<thead>';
        contenido += '<tr>';
        contenido += '<th>N° de Ficha</th>';
        contenido += '<th>Tutor</th>';
        contenido += '<th>Empresa</th>';
        contenido += '<th>Código Curso</th>';
        contenido += '<th>Ejecutivo Comercial</th>';
        contenido += '<th>Cantidad Encuestas</th>';
        contenido += '<th>Impresión General</th>';
        contenido += '<th>Sobre el Curso</th>';
        contenido += '<th>Sobre el Manual</th>';
        contenido += '<th>Sobre el Tutor</th>';
        contenido += '<th>Sobre el Instituto</th>';
        contenido += '<th>Promedio Ficha</th>';        
        contenido += '</tr>';
        contenido += '</thead>';
        contenido += '<tbody>';
        $.each( data, function( i, item ) {
            if(item.comentarios != ''){
                contenido += '<tr>';
                contenido += '<td>';
                contenido += item.num_ficha;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.tutor;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.empresa;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.codigo_curso;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.ejecutivo;
                contenido += '</td>';
                contenido += '<td>';
                contenido += item.total_encuestas_ingresadas;
                contenido += '</td>';
                contenido += item.impresion_general < 4 ? '<td style="color:red;">'+item.impresion_general: '<td>'+item.impresion_general;
                contenido += '</td>';
                contenido += item.sobre_el_curso < 4 ? '<td style="color:red;">'+item.sobre_el_curso: '<td>'+item.sobre_el_curso;
                contenido += '</td>';
                contenido += item.sobre_el_manual < 4 ? '<td style="color:red;">'+item.sobre_el_manual: '<td>'+item.sobre_el_manual;
                contenido += '</td>';
                contenido += item.sobre_el_tutor < 4 ? '<td style="color:red;">'+item.sobre_el_tutor: '<td>'+item.sobre_el_tutor;
                contenido += '</td>';
                contenido += item.sobre_el_instituto < 4 ? '<td style="color:red;">'+item.sobre_el_instituto: '<td>'+item.sobre_el_instituto;
                contenido += '</td>';
                contenido += item.promedio_total_ficha < 4 ? '<td style="color:red;"><b>'+item.promedio_total_ficha: '</b><td><b>'+item.promedio_total_ficha+'</b>';
                contenido += '</td>';
                contenido += '</tr>';
            }
        });        
        contenido += '</tbody>';
        contenido += '</table>';
    }
    $('#render-html').html(contenido);
}

function cargaCombo(midata, item) {
    $("#"+ item).select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
}

function generatePDF(id_table, x){

    var criterio = ['Por número de ficha', 'Rangos entre fechas de cierre del curso', 'Por tutor + mes a consultar', 'Ranking anual de tutores', 'Ranking mensual de tutores', 'Por razón social edutecno', 'Búsqueda personalizada'];
    var doc = new jsPDF();
    doc.setFontSize(8);
    doc.text("EDUTECNO CAPACITACIÓN", 14, 10);
    doc.setFontSize(16);
    
    doc.text("Informe Encuesta Satisfacción", 14, 15);
    doc.setFontSize(10);
    doc.text("Criterio de Búsqueda: "+criterio[x], 14, 20);
    doc.setFontSize(10);
    doc.text("Fecha de emisión del informe: "+d.getDate().toString()+'-'+(d.getMonth()+1).toString()+'-'+d.getFullYear().toString(), 14, 25);
    var elem = document.getElementById(id_table);
    
    var res = doc.autoTableHtmlToJson(elem);

    switch (x) {
        case 1:
        doc.autoTable(res.columns, res.data, {startY: 30,
            headerStyles: {
                overflow: 'linebreak',
                fontSize: 9
            },
            columnStyles: {
                0: {columnWidth: 15,overflow: 'linebreak', halign: 'left', fontSize: 9},
                1: {columnWidth: 35,overflow: 'linebreak', halign: 'left', fontSize: 9},
                2: {columnWidth: 40,overflow: 'linebreak', halign: 'left', fontSize: 9},
                3: {columnWidth: 22,overflow: 'linebreak', halign: 'left', fontSize: 9},
                4: {columnWidth: 22,overflow: 'linebreak', halign: 'left', fontSize: 9},
                5: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                6: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                7: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                8: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                9: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                10: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                11: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9}
            }
        });
            break;
            case 6:
            doc.autoTable(res.columns, res.data, {startY: 30,
                headerStyles: {
                    overflow: 'linebreak',
                    fontSize: 9
                },
                columnStyles: {
                    0: {columnWidth: 15,overflow: 'linebreak', halign: 'left', fontSize: 9},
                    1: {columnWidth: 35,overflow: 'linebreak', halign: 'left', fontSize: 9},
                    2: {columnWidth: 40,overflow: 'linebreak', halign: 'left', fontSize: 9},
                    3: {columnWidth: 22,overflow: 'linebreak', halign: 'left', fontSize: 9},
                    4: {columnWidth: 22,overflow: 'linebreak', halign: 'left', fontSize: 9},
                    5: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                    6: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                    7: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                    8: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                    9: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                    10: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9},
                    11: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 9}
                }
            });
                break;
                case 7:
                doc.autoTable(res.columns, res.data, {startY: 30,
                    headerStyles: {
                        overflow: 'linebreak',
                        fontSize: 8,
                        halign: 'center'
                    },
                    columnStyles: {
                        0: {columnWidth: 15,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        1: {columnWidth: 35,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        2: {columnWidth: 25,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        3: {columnWidth: 18,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        4: {columnWidth: 15,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        5: {columnWidth: 15,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        6: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        7: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        8: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        9: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        10: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        11: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        12: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        13: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        14: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        15: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        16: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        17: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        18: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8},
                        19: {columnWidth: 10,overflow: 'linebreak', halign: 'center', fontSize: 8}
                    }
                });
                    break;
        default:
        doc.autoTable(res.columns, res.data, {startY: 30, theme: 'striped'});
            break;
    }

    var elem2, res2;

    if(x==0){
        elem2 = document.getElementById('table_comentarios');
        res2 = doc.autoTableHtmlToJson(elem2);
        doc.setFontSize(13);
        doc.text("Comentarios de los alumnos", 14, 115);
        doc.autoTable(res2.columns, res2.data, {startY: 120, theme:'grid', 
        headerStyles: {
            overflow: 'visible',
            fontSize: 1,
            fontStyle: 'normal',
            height: 0,
            margin: {top:0,right:0,bottom:0,left:0}
            },
            columnStyles: {
                0: { columnWidth: 8, overflow: 'linebreak', halign: 'left'},
                1: { columnWidth: 180, overflow: 'linebreak', halign: 'left'}
            }
        });
    }
    doc.save('informe_'+criterio[x]+'_'+d.getDate().toString()+'_'+(d.getMonth()+1).toString()+'_'+d.getFullYear().toString()+'_'+d.getTime().toString()+'.pdf');
}

function cargaSalas() {
    
        $('#select_sala').html('').select2({
            placeholder: "Seleccione",
            allowClear: true,
            data: [{id: '', text: ''}]
        });
    
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_sede: $('#select_sede').val()},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function (result) {
                cargaCombo(result, "select_sala");
            },
            url: "Informe_Encuestas/getSalaCBX"
        });
}
