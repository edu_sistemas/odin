$(document).ready(function () {
    cargaCbxAll();
    $("#btn_back").click(function () {
        // retrocede una pagina
        limpiarFormulario();
        // history.back();
    });

    contarCaracteres("comentario", "text-out", 1000);
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $("#btn_enviar_ficha").click(function () {
        var ficha = $("#id_ficha").val();
        validaPreEnvioFicha(ficha);
    });

    inicializaControles();

    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });
});



function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() {


    var id_ficha = "";

    var id_oc = "";


    id_ficha = $("#id_ficha").val();

    id_oc = $("#cbx_orden_compra").val();


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../../cargaDatosOCDocumentoResumen"
    });

    /*Factura */


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_orden_compra: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
           console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatosFactura(result);
        },
        url: "../../getFacturaByOC"
    });

    /*End factura */


}

function cargaTablaDatoDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            {"mDataProp": "fecha_registro"}
        ],
        pageLength: 5,
        responsive: false,
        dom: '<"clear">',
        "order": [[2, "desc"]]

    });

    $('[data-toggle="tooltip"]').tooltip();

}





/*Factura */

function cargaTablaDatosFactura(data) {

    if ($.fn.dataTable.isDataTable('#tbl_facturas_orden_compra')) {
        $('#tbl_facturas_orden_compra').DataTable().destroy();
    }


    /*
     <th>Tipo Factura</th>
     <th>N° FACTURA</th>
     <th>Fecha Emisión</th>
     <th>Fecha de Vencimiento</th>
     <th>Valor Factura</th>
     <th>FACTURA</th>
     <th>Tipo de Entrega</th>
     <th>Estado Factura</th>
     <th>Opciones</th>*/

    $('#tbl_facturas_orden_compra').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_facturacion"},
            {"mDataProp": "n_factura"},
            {"mDataProp": "monto_factura"},
            {"mDataProp": "tipo_entrega"},
            {"mDataProp": "nombre_estado"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = " ";


                    if (o.nombre_estado == 'Pendiente de Pago') {
                        html += '  <div class="btn-group">';


                        html += "  <button type='button' onclick='estadoFacura(" + o.id_factura + ",2)'  class='btn btn-primary dropdown-toggle'>Pagada</button>   <br>";
                        html += "  <button type='button' onclick='estadoFacura(" + o.id_factura + ",3)'  class='btn btn-danger dropdown-toggle'>Anulada</button>  ";

                        html += ' </div>';

                    } else {
                        html += '  <div class="btn-group">';


                        html += "   Sin Opciones   ";

                        html += ' </div>';
                    }
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [1, "desc"]
    });

    $('[data-toggle="tooltip"]').tooltip();

}


function estadoFacura(id_factura, estado_factura) {



    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_factura: id_factura,
            estado_factura: estado_factura
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            console.log(result);
            cargaCbxAll();
        },
        url: "../../setEstadoFactura"
    });


}



/* End factura */