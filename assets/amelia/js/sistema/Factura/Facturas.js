$(document).ready(function () {

    $('#btn_nuevo').click(function () {
        location.href = 'Agregar';
    });

    cargaCBXall();
    listarFacturas();


    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $("#btn_buscar_ficha").click(function () {
        listarFacturas();
    });

    $("#btn_nueva_nota_credito").click(function () {
        nuevaNotaCredito();
    });

    $("#btn_nueva_nota_debito").click(function () {
        nuevaNotaDebito();
    });


    $("#btn_volver_modal_credito").click(function () {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function () {
            $("#sec_tabla").animate({
                height: "toggle"
            }, 500, function () {

            });
            $("#n_factura_cbx").val("");
            $("#num_nota_credito").val("");
            $("#fecha_emision").val("");
            $("#monto_credito").val("");
        });

    });

    $("#btn_volver_modal_debito").click(function () {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function () {
            $("#sec_tabla_debito").animate({
                height: "toggle"
            }, 500, function () {

            });
            $("#n_factura_cbx_debito").val("");
            $("#num_nota_debito").val("");
            $("#fecha_emision_debito").val("");
            $("#monto_debito").val("");
        });


    });

    $('#fecha_emision').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $('#fecha_emision_debito').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $("#btn_agregar_nota_credito").click(function () {
        if ($("#n_factura_cbx").val() == "" || $("#num_nota_credito").val() == "" || $("#fecha_emision").val() == "" || $("#monto_credito").val() == "") {
            swal("Error", "Debe completar todos los campos solicitados", "error");
        } else {
            guardarNotaCredito();
        }
    });

    $("#btn_agregar_nota_debito").click(function () {
        if ($("#n_factura_cbx_debito").val() == "" || $("#num_nota_debito").val() == "" || $("#fecha_emision_debito").val() == "" || $("#monto_debito").val() == "") {
            swal("Error", "Debe completar todos los campos solicitados", "error");
        } else {
            guardarNotaDebito();
        }
    });
});

function cargaCBXall() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "holding");
        },
        url: "Facturas/getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: {id_holding: $('#holding').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "Facturas/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "Facturas/getUsuarioTipoEcutivoComercial"
    });
}
function cargaTablaFacturas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_facturas')) {
        $('#tbl_facturas').DataTable().destroy();
    }


    $('#tbl_facturas').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_facturacion"},
            {"mDataProp": "n_factura"},

            {"mDataProp": "monto_factura"},

            {"mDataProp": "nombre_estado"},
            {"mDataProp": "tipo_entrega"},

            {"mDataProp": "num_orden_compra"},
            {"mDataProp": "razon_social_empresa"},
            {"mDataProp": "nombre_otic"},

            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle ">Accion<span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu pull-right">';
                    var a = "";

                    if (o.id_categoria == 1) {
                        a = "DetalleFicha/index/" + o.id_orden_compra + "/0";
                    } else {
                        a = "DetalleArriendo/index/" + o.id_orden_compra + "/0";
                    }

                    html += '<li><a href=' + a + '>Ver Detalle</a></li>';


                 //   html += '<li><a onclick="openModalNotaDeCredito(' + o.id_orden_compra + ')">Notas de Crédito</a></li>';
                   // html += '<li><a onclick="openModalNotaDeDebito(' + o.id_orden_compra + ')">Notas de Débito</a></li>';
                    html += '<li class="divider"></li>';
                    var fact = "";
                    fact = "EditarFactura/index/" + o.id_factura;
                    html += '<li><a href=' + fact + '>Editar Factura</a></li>';


                    html += ' </ul>';
                    html += ' </div>';
                    $("#oc_modal_credito").text(o.num_orden_compra);
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                }
            }
        ],
        order: [1, 'desc']
    });
}

function listarFacturas() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaFacturas(result);
        },
        url: "Facturas/listarfacturas"
    });
}


function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");


}

//METODOS PARA NOTA DE CREDITO 

function openModalNotaDeCredito(id_orden_compra) {

    $("#id_oc_hidden").val(id_orden_compra);


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_nota_credito").modal("show");
            loadTableNotaDeCredito(result);
        },
        url: "Facturas/getNotasbyOrdenCompra"
    });

//    getProgramasCBX();

}

function nuevaNotaCredito() {
    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function () {

        });
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc';

            $.each(result, function (i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });

}

function loadTableNotaDeCredito(data) {

    $("#sec_tabla").css({
        'display': 'block'
    });

    $("#sec_comentarios").css({
        'display': 'none'
    });

    $("#sec_nuevo").css({
        'display': 'none'
    });

    $("#sec_agregar_programa").css({
        'display': 'none'
    });

    $("#sec_editar").css({
        'display': 'none'
    });


    if (data[0] == undefined || data[0].id_factura == null) {
        $("#btn_nueva_nota_credito").prop('disabled', true);
        $("#btn_nueva_nota_credito").prop('title', 'Para crear una nota de crédito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_credito").prop('disabled', false);
        $("#btn_nueva_nota_credito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';

    $('#tbl_credito tbody').html(html);
    $.each(data, function (i, item) {

        if (item.id_nota_credito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += '<td>' + item.fecha_emision + '</td><td>' + item.n_factura + '</td><td>' + item.num_nota_credito + '</td><td>' + item.monto_credito + '</td>';


            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaCreditoEntrega(' + item.id_nota_credito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';
        }
    });

    $('#tbl_credito tbody').html(html);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_oc_hidden").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("n_factura_cbx", result);
        },
        url: "Facturas/getFacturasbyOcCBX"
    });

}
function notaCreditoEntrega(id, nombre) {

    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_update_tipo_entrega_nc").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    //ajax para llenar select y id hidden
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc_2';
            $('#id_nc').val(id);
            $.each(result, function (i, item) {
                var selected = '';
                if (nombre.toLowerCase() == item.text.toLowerCase()) {
                    selected = "selected"
                }
                html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Facturas/tipo_entrega"
    });
}
function enviarFormNc() {
    //alert('se envia formulario para update de nc');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_nc: $("#id_nc").val(),
            id_tipo_entrega: $("#tipo_entrega_nc_2").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#sec_update_tipo_entrega_nc').attr('style', 'display:none');
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_orden_compra: $("#id_oc_hidden").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    // cargaCombo(result, "modalidad");
                    // cosole.log(result);
                    $("#modal_nota_credito").modal("show");
                    loadTableNotaDeCredito(result);
                },
                url: "Facturas/getNotasbyOrdenCompra"
            });
        },
        url: "Facturas/updateTipoEntrega_nc"
    });
}

function cancelarFormNc() {
    $("#sec_update_tipo_entrega_nc").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_tabla").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
}


function guardarNotaCredito() {
    $.ajax({

        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_credito")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: "Guardado",
                text: "La Nota de Crédito ha sido ingresada exitosamente.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "OK",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                    function (isConfirm) {
                        openModalNotaDeCredito($("#id_oc_hidden").val());
                        $("#n_factura_cbx").val("");
                        $("#num_nota_credito").val("");
                        $("#fecha_emision").val("");
                        $("#monto_credito").val("");
                    });
        },
        url: "Facturas/setNotaCredito"
    });
}

//METODOS PARA NOTA DE DEBITO 

function openModalNotaDeDebito(id_orden_compra) {

    $("#id_oc_hidden_debito").val(id_orden_compra);


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_nota_debito").modal("show");
            loadTableNotaDeDebito(result);
        },
        url: "Facturas/getNotasDebitobyOrdenCompra"
    });



}

function nuevaNotaDebito() {
    $("#sec_tabla_debito").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function () {

        });
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nd';

            $.each(result, function (i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Facturas/tipo_entrega"
    });

}

function loadTableNotaDeDebito(data) {

    $("#sec_tabla_debito").css({
        'display': 'block'
    });

    $("#sec_comentarios_debito").css({
        'display': 'none'
    });

    $("#sec_nuevo_debito").css({
        'display': 'none'
    });

    $("#sec_agregar_programa_debito").css({
        'display': 'none'
    });

    $("#sec_editar_debito").css({
        'display': 'none'
    });


    if (data[0] == undefined || data[0].id_factura == null) {
        $("#btn_nueva_nota_debito").prop('disabled', true);
        $("#btn_nueva_nota_debito").prop('title', 'Para crear una nota de débito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_debito").prop('disabled', false);
        $("#btn_nueva_nota_debito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';

    $('#tbl_debito tbody').html(html);
    $.each(data, function (i, item) {

        if (item.id_nota_debito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += '<td>' + item.fecha_emision + '</td><td>' + item.n_factura + '</td><td>' + item.num_nota_debito + '</td><td>' + item.monto_debito + '</td>';

            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaDebitoEntrega(' + item.id_nota_debito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';

        }

    });

    $('#tbl_debito tbody').html(html);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_oc_hidden_debito").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("n_factura_cbx_debito", result);
        },
        url: "Facturas/getFacturasbyOcCBX"
    });

}

function guardarNotaDebito() {

    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_debito")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: "Guardado",
                text: "La Nota de Débito ha sido ingresada exitosamente.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "OK",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function (isConfirm) {
                openModalNotaDeDebito($("#id_oc_hidden_debito").val());
                $("#n_factura_cbx_debito").val("");
                $("#num_nota_debito").val("");
                $("#fecha_emision_debito").val("");
                $("#monto_debito").val("");
            });
        },
        url: "Facturas/setNotaDebito"
    });
}

//---------------------------MODAL DOCUMENTOS INTERNOS-----------------------

//Modal Documentos Ficha

function getDocumentosFicha(id_ficha, num_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id_ficha
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('fichadoc').text(num_ficha);
            $("#modal_documentos_ficha").modal("show");
            loadTableDocumentosFicha(result);
        },
        url: "Facturas/getDocumentosFicha"
    });
}
function loadTableDocumentosFicha(data) {
    var html = "";
    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';
        if (item.nombre_documento % 2 == 0) {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a>';
        } else {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a>';
        }
        html += '</td></tr>';
    });

    $('#tbl_documentos_factura tbody').html(html);
}

//Modal Documentos Rectificacion

function getDocumentosRectificacion(id_orden_compra, num_orden_compra) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('ocdoc').text(num_orden_compra);
            $("#modal_documentos_rectificacion").modal("show");
            loadTableDocumentosRectificacion(result);
        },
        url: "Facturas/getDocumentosRectificacion"
    });
}
function loadTableDocumentosRectificacion(data) {
    var html = "";
    $('#tbl_documentos_rectificacion tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';
        if (item.nombre_documento % 2 == 0) {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a>';
        } else {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a>';
        }
        html += '</td></tr>';
    });

    $('#tbl_documentos_rectificacion tbody').html(html);
}