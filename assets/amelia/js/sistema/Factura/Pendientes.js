$(document).ready(function () {

    $('#btn_nuevo').click(function () {
        location.href = 'Agregar';
    });

    cargaCBXall();
    listarFichas();

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });


    $('#data_5 .input-daterange').datepicker({
        format: "dd-mm-yyyy",
        language: 'es',
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });


    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $("#btn_buscar_ficha").click(function () {
        listarFichas();
    });

    $("#btn_nueva_nota_credito").click(function () {
        nuevaNotaCredito();
    });

    $("#btn_nueva_nota_debito").click(function () {
        nuevaNotaDebito();
    });

    $('#id_sence').on('click focusin', function () {
        this.value = '';
    });

    $("#btn_volver_modal_credito").click(function () {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function () {
            $("#sec_tabla").animate({
                height: "toggle"
            }, 500, function () {

            });
            $("#n_factura_cbx").val("");
            $("#num_nota_credito").val("");
            $("#fecha_emision").val("");
            $("#monto_credito").val("");
        });

    });

    $("#btn_volver_modal_debito").click(function () {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function () {
            $("#sec_tabla_debito").animate({
                height: "toggle"
            }, 500, function () {

            });
            $("#n_factura_cbx_debito").val("");
            $("#num_nota_debito").val("");
            $("#fecha_emision_debito").val("");
            $("#monto_debito").val("");
        });


    });

    $('#fecha_emision').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $('#fecha_emision_debito').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $("#btn_agregar_nota_credito").click(function () {
        if ($("#n_factura_cbx").val() == "" || $("#num_nota_credito").val() == "" || $("#fecha_emision").val() == "" || $("#monto_credito").val() == "") {
            swal("Error", "Debe completar todos los campos solicitados", "error");
        } else {
            guardarNotaCredito();
        }
    });

    $("#btn_agregar_nota_debito").click(function () {
        if ($("#n_factura_cbx_debito").val() == "" || $("#num_nota_debito").val() == "" || $("#fecha_emision_debito").val() == "" || $("#monto_debito").val() == "") {
            swal("Error", "Debe completar todos los campos solicitados", "error");
        } else {
            guardarNotaDebito();
        }
    });

    $("#btn_enviar_solicitud_de_correccion").click(function () {
        if (validarRequeridoForm($("#form_sdc_correccion"))) {
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#form_sdc_correccion').serialize(),
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(jqXHR);
                },
                success: function (result) {
                    enviaCorreo(result);

                    swal({
                        title: result[0].titulo,
                        text: result[0].mensaje,
                        type: result[0].tipo_alerta,
                        html: true,
                        confirmButtonText: "Aceptar",
                        showCancelButton: false,
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    location.reload();
                                }
                            });
                },
                url: "Pendientes/enviarSolicitudDeCorrecion"
            });
        }
    });

    $("#btn_guardar_mi_observacion").click(function () {

        if (validarRequeridoForm($("#form_sdc_mi_observacion"))) {

            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#form_sdc_mi_observacion').serialize(),
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(jqXHR);
                },
                success: function (result) {


                    swal({
                        title: result[0].titulo,
                        text: result[0].mensaje,
                        type: result[0].tipo_alerta,
                        html: true,
                        confirmButtonText: "Aceptar",
                        showCancelButton: false,
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    },
                            function (isConfirm) {
                                if (isConfirm) {
                                    location.reload();
                                }
                            });
                },
                url: "Pendientes/savemiobservacion"
            });
        }
    });




    $("#btn_guardar_facturacion_observacion").click(function () {
        if (validarRequeridoForm($("#form_facturacion_observacion"))) {
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#form_facturacion_observacion').serialize(),
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(jqXHR);
                },
                success: function (result) {

                    swal({
                        title: result[0].titulo,
                        text: result[0].mensaje,
                        type: result[0].tipo_alerta,
                        html: true,
                        confirmButtonText: "Aceptar",
                        showCancelButton: false,
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });
                },
                url: "Pendientes/enviarFacturaAFacturacion"
            });
        }
    });


    $("#btn_guardar_ID_sence").click(function () {
        updateidsence();
    });

    $("#btn_eliminar_ID_sence").click(function () {
        $("#eliminar_id_sence").val(1); 
        updateidsence();
    });

    $("#btn_guardar_num_oc").click(function () {
        if (validarRequeridoForm($("#form_save_oc"))) {
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#form_save_oc').serialize(),
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(jqXHR);
                },
                success: function (result) {

                    swal({
                        title: 'N° OC',
                        text: 'N° OC Actualizada',
                        type: 'success',
                        html: true,
                        confirmButtonText: "Aceptar",
                        showCancelButton: false,
                        cancelButtonText: "Cancelar",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (isConfirm) {


                            location.reload();

                        }
                    });
                },
                url: "Pendientes/update_num_oc"
            });
        }
    });


});

function enviaCorreo(data) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {datos: data},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "Pendientes/EnviarMailCorreccion"
    });
}

function cargaCBXall() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "holding");
        },
        url: "Pendientes/getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: {id_holding: $('#holding').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "Pendientes/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "Pendientes/getUsuarioTipoEcutivoComercial"
    });
}
function cargaTablaFacturas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_fichas')) {
        $('#tbl_fichas').DataTable().destroy();
    }

    $('#tbl_fichas').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle "><i class="fa fa-angle-double-down"></i> <span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu">';
                    var a = "";
                    var b = "ValidarAlumno/index/" + o.id_ficha;
                    if (o.id_categoria == 1) {
                        a = "DetalleFicha/index/" + o.id_orden_compra + "/0";
                    } else {
                        a = "DetalleArriendo/index/" + o.id_orden_compra + "/0";
                    }
                    html += '<li><a href=' + a + '>Ver Detalle</a></li>';

                    if (o.id_categoria == 1) {
                        html += '<li><a href=' + b + '>Validar Alumnos</a></li>';
                    }
                    html += '<li><a onclick="modalmiobservacion(' + o.num_ficha + ',' + o.id_ficha + ',' + o.id_orden_compra + ')">Observación</a></li>';

                    html += '<li><a onclick="modalSolicitudDeCorrecion(' + o.num_ficha + ',' + o.id_ficha + ',' + o.id_orden_compra + ')">Solicitud de Corrección</a></li>';
                    html += '<li class="divider"></li>';

                    html += '<li><a onclick="modalenviarAFacturacion(' + o.num_ficha + ',' + o.id_ficha + ',' + o.id_orden_compra + ')">Enviar a Facturación</a></li>';

                    html += '<li class="divider"></li>';

                    html += '<li><a onclick="enviarADespacho(' + o.id_ficha + ',' + o.id_orden_compra + ')">Enviar a Despacho</a></li>';

                    html += '<li class="divider"></li>';



                    var x = "'Agregar/index/" + o.id_orden_compra + "'";
                    html += '<li><a onclick="location.href=' + x + '">Agregar Factura</a></li>';
                    html += '<li><a onclick="verFacturas(' + o.id_orden_compra + ')">Ver Factura(s)</a></li>';
                    html += '<li><a onclick="openModalNotaDeCredito(' + o.id_orden_compra + ')">Notas de Crédito</a></li>';
                    html += '<li><a onclick="openModalNotaDeDebito(' + o.id_orden_compra + ')">Notas de Débito</a></li>';
                    html += '<li class="divider"></li>';
                    html += '<li><a onclick="procesoTerminado(' + o.id_ficha + ')">Proceso Terminado</a></li>';
                    html += '<li class="divider"></li>';
                    html += ' </ul>';
                    html += ' </div>';
                    $("#oc_modal_credito").text(o.num_orden_compra);
                    return html;
                }
            },
            {"mDataProp": "num_ficha"},

            {"mDataProp": "id_consolidado"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    var a = "DetalleObservaciones/index/" + o.id_ficha;
                    html += '<a href="' + a + '" target="_blank">' + o.categoria + '</a>';
                    return html;
                }
            },
            {"mDataProp": "ejecutivo"},
            // {"mDataProp": "num_orden_compra"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    // modalmiobservacion 

                    html += '<a onclick="modalnumoc(' + o.num_ficha + ',' + o.id_orden_compra + ')">' + o.num_orden_compra + '</a>';

                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    // modalmiobservacion 
                //   var sence_split = o.id_sence.split(", ");
               // console.log(sence_split);
              //  $.each(sence_split, function (i, item) {
                if (o.id_sence == '-') {

                    html += o.id_sence;
                } else {
                    html += '<a onclick="modalidsence(' + o.num_ficha + ',' + o.id_orden_compra + ')">' + o.id_sence + '</a> ';
                }
             //   })
                    return html;
                }
            },
            //  {"mDataProp": "id_sence"},

            {"mDataProp": "empresa"},
            {"mDataProp": "otic"},
            {"mDataProp": "descripcion_producto"},
            {"mDataProp": "fecha_fin"},
            {"mDataProp": "dias_restantes"},
            {"mDataProp": "dias_transcurridos"},
            {"mDataProp": "gestiones_realizadas"},
            {"mDataProp": "estado_ficha"},
            {"mDataProp": "estado_facturacion"},
            {"mDataProp": "observacion"},
            {"mDataProp": null,  "mRender": function (o) {
                    var html = "";

                    html += '<table style="font-size:90%">';
                    html += '<tr><td style="padding-right:5px;" ><b>';
                    html += '</b></td><td><b>';
                    html += o.valor_total;
                    html += '</b></td></tr>';
                    html += '</table>';

                    return html;
                }
            },
            {"mDataProp": null,  "mRender": function (o) {
                    var html = "";

                    html += '<table style="font-size:90%">';

                    html += '<tr><td style="padding-right:5px;" ><b>';
                    html += '</b></td><td><b>';
                    html += o.saldo_total;
                    html += '</b></td></tr>';

                    html += '</table>';

                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'FichasPendientes'
            }, {
                extend: 'pdf',
                title: 'FichasPendientes'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [9, 'asc']
    });


}

function listarFichas() {
    $.ajax({
        url: "Pendientes/listar",
        type: 'POST',
        dataType: "json",
        data: $('#frm').serialize(),
        beforeSend: function () {
            $("#loading-container").html('<img src="../../assets/img/timer.gif">');
        },
        success: function (result) {
            cargaTablaFacturas(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#loading-container').html("Error en la carga");
        },
        complete: function () {
            $("#loading-container").html("");
        },
        async: true,
        cache: false
    });
}

function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");


}

//METODOS PARA NOTA DE CREDITO 

function openModalNotaDeCredito(id_orden_compra) {

    $("#id_oc_hidden").val(id_orden_compra);


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_nota_credito").modal("show");
            loadTableNotaDeCredito(result);
        },
        url: "Pendientes/getNotasbyOrdenCompra"
    });

//    getProgramasCBX();

}

function nuevaNotaCredito() {
    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function () {

        });
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc';

            $.each(result, function (i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });

}

function loadTableNotaDeCredito(data) {

    $("#sec_tabla").css({
        'display': 'block'
    });

    $("#sec_comentarios").css({
        'display': 'none'
    });

    $("#sec_nuevo").css({
        'display': 'none'
    });

    $("#sec_agregar_programa").css({
        'display': 'none'
    });

    $("#sec_editar").css({
        'display': 'none'
    });


    if (data[0] == undefined || data[0].id_factura == null) {
        $("#btn_nueva_nota_credito").prop('disabled', true);
        $("#btn_nueva_nota_credito").prop('title', 'Para crear una nota de crédito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_credito").prop('disabled', false);
        $("#btn_nueva_nota_credito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';

    $('#tbl_credito tbody').html(html);
    $.each(data, function (i, item) {

        if (item.id_nota_credito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += ' <td>' + item.n_factura + '</td><td>' + item.num_nota_credito + '</td><td>' + item.monto_credito + '</td>';
            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaCreditoEntrega(' + item.id_nota_credito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';
        }
    });

    $('#tbl_credito tbody').html(html);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_oc_hidden").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("n_factura_cbx", result);
        },
        url: "Pendientes/getFacturasbyOcCBX"
    });

}
function notaCreditoEntrega(id, nombre) {

    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_update_tipo_entrega_nc").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    //ajax para llenar select y id hidden
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc_2';
            $('#id_nc').val(id);
            $.each(result, function (i, item) {
                var selected = '';
                if (nombre.toLowerCase() == item.text.toLowerCase()) {
                    selected = "selected"
                }
                html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Pendientes/tipo_entrega"
    });
}

function cancelarForm() {

    $("#formfac").animate({
        left: "+=50",
        height: "toggle"
    }, 500, function () {
        $("#listaFac").animate({
            left: "+=50",
            height: "toggle"
        }, 500, function () {
            // Animation complete.
        });
    });
}

function enviarForm() {

    var id = $('#id_fact').val();
    var id_tipo = $('#tipo_entrega_f').val();

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_fact: id, id_tipo_entrega: id_tipo},
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {

            verFacturas($('#id_oc_hidden').val());
            $('#modal_facturas').modal("toggle");
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#modal_facturas').modal("show");
        },
        url: "Pendientes/updateTipoEntrega"
    });
}

function enviarFormNc() {
    //alert('se envia formulario para update de nc');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_nc: $("#id_nc").val(),
            id_tipo_entrega: $("#tipo_entrega_nc_2").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#sec_update_tipo_entrega_nc').attr('style', 'display:none');
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_orden_compra: $("#id_oc_hidden").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    // cargaCombo(result, "modalidad");
                    // cosole.log(result);
                    $("#modal_nota_credito").modal("show");
                    loadTableNotaDeCredito(result);
                },
                url: "Pendientes/getNotasbyOrdenCompra"
            });
        },
        url: "Pendientes/updateTipoEntrega_nc"
    });
}

function cancelarFormNc() {
    $("#sec_update_tipo_entrega_nc").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_tabla").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
}

function guardarNotaCredito() {
    $.ajax({

        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_credito")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: "Guardado",
                text: "La Nota de Crédito ha sido ingresada exitosamente.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "OK",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                    function (isConfirm) {
                        openModalNotaDeCredito($("#id_oc_hidden").val());
                        $("#n_factura_cbx").val("");
                        $("#num_nota_credito").val("");
                        $("#fecha_emision").val("");
                        $("#monto_credito").val("");
                    });
        },
        url: "Pendientes/setNotaCredito"
    });
}

//METODOS PARA NOTA DE DEBITO 

function openModalNotaDeDebito(id_orden_compra) {

    $("#id_oc_hidden_debito").val(id_orden_compra);


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_nota_debito").modal("show");
            loadTableNotaDeDebito(result);
        },
        url: "Pendientes/getNotasDebitobyOrdenCompra"
    });

//    getProgramasCBX();

}

function nuevaNotaDebito() {
    $("#sec_tabla_debito").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function () {

        });
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nd';

            $.each(result, function (i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Pendientes/tipo_entrega"
    });

}

function loadTableNotaDeDebito(data) {

    $("#sec_tabla_debito").css({
        'display': 'block'
    });

    $("#sec_comentarios_debito").css({
        'display': 'none'
    });

    $("#sec_nuevo_debito").css({
        'display': 'none'
    });

    $("#sec_agregar_programa_debito").css({
        'display': 'none'
    });

    $("#sec_editar_debito").css({
        'display': 'none'
    });


    if (data[0] == undefined || data[0].id_factura == null) {
        $("#btn_nueva_nota_debito").prop('disabled', true);
        $("#btn_nueva_nota_debito").prop('title', 'Para crear una nota de débito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_debito").prop('disabled', false);
        $("#btn_nueva_nota_debito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';

    $('#tbl_debito tbody').html(html);
    $.each(data, function (i, item) {

        if (item.id_nota_debito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += '<td>' + item.fecha_emision + '</td><td>' + item.n_factura + '</td><td>' + item.num_nota_debito + '</td><td>' + item.monto_debito + '</td>';


            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaDebitoEntrega(' + item.id_nota_debito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';

        }

    });

    $('#tbl_debito tbody').html(html);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_oc_hidden_debito").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("n_factura_cbx_debito", result);
        },
        url: "Pendientes/getFacturasbyOcCBX"
    });

}

function notaDebitoEntrega(id, nombre) {
    $("#sec_tabla_debito").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_update_tipo_entrega_nd").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    //ajax para llenar select y id hidden
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nd_2';
            $('#id_nd').val(id);
            $.each(result, function (i, item) {
                var selected = '';
                if (nombre.toLowerCase() == item.text.toLowerCase()) {
                    selected = "selected"
                }
                html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Pendientes/tipo_entrega"
    });
}
function enviarFormNd() {
    //alert('se envia formulario para update de nd');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_nd: $("#id_nd").val(),
            id_tipo_entrega: $("#tipo_entrega_nd_2").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#sec_update_tipo_entrega_nd').attr('style', 'display:none');
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_orden_compra: $("#id_oc_hidden_debito").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    // cargaCombo(result, "modalidad");
                    // cosole.log(result);
                    $("#modal_nota_debito").modal("show");
                    loadTableNotaDeDebito(result);
                },
                url: "Historico/getNotasDebitobyOrdenCompra"
            });
        },
        url: "Historico/updateTipoEntrega_nd"
    });
}
function cancelarFormNd() {

    $("#sec_update_tipo_entrega_nd").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_tabla_debito").animate({
            height: "toggle"
        }, 500, function () {

        });
    });

}
function guardarNotaDebito() {

    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_debito")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: "Guardado",
                text: "La Nota de Débito ha sido ingresada exitosamente.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "OK",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function (isConfirm) {
                openModalNotaDeDebito($("#id_oc_hidden_debito").val());
                $("#n_factura_cbx_debito").val("");
                $("#num_nota_debito").val("");
                $("#fecha_emision_debito").val("");
                $("#monto_debito").val("");
            });
        },
        url: "Pendientes/setNotaDebito"
    });
}
//---------------------------MODAL DOCUMENTOS INTERNOS-----------------------
//Modal Documentos Ficha
function getDocumentosFicha(id_ficha, num_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id_ficha
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('fichadoc').text(num_ficha);
            $("#modal_documentos_ficha").modal("show");
            loadTableDocumentosFicha(result);
        },
        url: "Pendientes/getDocumentosFicha"
    });
}
function loadTableDocumentosFicha(data) {
    var html = "";
    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';


        html += item.nombre_documento;

        html += '</td></tr>';
    });

    $('#tbl_documentos_factura tbody').html(html);
}
//Modal Documentos Rectificacion

function getDocumentosRectificacion(id_orden_compra, num_orden_compra) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('ocdoc').text(num_orden_compra);
            $("#modal_documentos_rectificacion").modal("show");
            loadTableDocumentosRectificacion(result);
        },
        url: "Pendientes/getDocumentosRectificacion"
    });
}
function loadTableDocumentosRectificacion(data) {
    var html = "";
    $('#tbl_documentos_rectificacion tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';

        html += item.nombre_documento;

        html += '</td></tr>';
    });

    $('#tbl_documentos_rectificacion tbody').html(html);
}

function verFacturas(id_orden_compra) {

    $("#formfac").attr('style', 'display:none;');
    $("#listaFac").attr('style', 'left: 50px;');

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('#modal_facturas').modal("show");
            cargaModalFacturas(result);
        },
        url: "Pendientes/getFacturasByOC"
    });

}

function cargaModalFacturas(data) {
    $('#oc_modal').html(data[0].num_orden_compra);
    $('#id_oc_hidden').val(data[0].id_orden_compra);
    var html = "";

    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function (i, item) {

        if (item.n_factura == null) {
            html += '<p style="margin: 35px; padding: auto; text-align:center; vertical-align:center;"><b>No existen facturas para la orden de compra.</b></p>';

        } else {
            var pStyle = "black";

            if (item.id_estado_factura == "1") {
                pStyle = "red";
            }
            if (item.id_estado_factura == "2") {
                pStyle = "green";
            }
            if (item.id_estado_factura == "3") {
                pStyle = "blue";
            }
            html += '<tr>';
            html += '<td>' + item.n_factura + ' </td>';
            var tipoEntrega = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.fecha_registro + '</td>';
            html += '<td>' + item.monto_factura + '</td>';
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td>' + item.fecha_entrega + '</td>';
            html += '<td style="color:' + pStyle + ';">' + item.nombre_estado + ' (' + item.fecha_estado + ')</td>';

            html += '<td>';

            html += '  <div class="btn-group">';
            html += '  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle ">Opciones<span class="caret"></span></button>';
            html += '  <ul class="dropdown-menu pull-right">';
            html += '	<li><a onclick="formulario(' + item.id_factura + ',' + tipoEntrega + ');">Entrega</a></li>';
            html += '	<li><a onclick="cambiarEstadoFactura(' + item.id_factura + ', ' + item.id_estado_factura + ')">Estado Factura</a></li>';
            html += ' </ul>';
            html += ' </div>';

            html += '</td></tr>';
        }

    });

    $('#tbl_documentos_factura tbody').html(html);
}

function formulario(id, nombre) {
    $("#listaFac").animate({
        left: "+=50",
        height: "toggle"
    }, 500, function () {
        $("#formfac").animate({
            left: "+=50",
            height: "toggle"
        }, 500, function () {
            //buscar lista de tipo entrega
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#frm').serialize(),
                error: function (jqXHR, textStatus, errorThrown) {
                },
                success: function (result) {
                    var html = "";
                    var idcbx = 'tipo_entrega_f';
                    $('#id_fact').val(id);
                    $.each(result, function (i, item) {
                        var selected = '';
                        if (nombre.toLowerCase() == item.text.toLowerCase()) {
                            selected = "selected"
                        }
                        html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                        selected = '';
                    });

                    $("#" + idcbx).html(html);
                },
                url: "Pendientes/tipo_entrega"
            });
        });
    });

}

function cambiarEstadoFactura(id_factura, id_estado_factura) {

    var data = cargaEstadoFacturaCBX(id_estado_factura);

    var html5 = "";
    html5 += '<select id="estado_factura_cbx">';
    $.each(data, function (i, item) {


        html5 += '<option value="';
        html5 += item.id;
        if (item.id == id_estado_factura) {
            html5 += '" selected>';
        } else {
            html5 += '">';
        }
        html5 += item.text;
        html5 += '</option>';
    });
    html5 += '</select>';

    swal({
        title: "Seleccione el nuevo estado de la factura",
        text: html5,
        //type: "warning",
        html: true,
        showCancelButton: true,
        //confirmButtonColor: "#DD6B55",
        confirmButtonText: "Actualizar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {

                    var id_estado_nuevo = $('#estado_factura_cbx').val();
                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {id_factura: id_factura, id_estado_nuevo: id_estado_nuevo},
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        success: function (result) {
                            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                        },
                        url: "Pendientes/updateEstadoFactura"
                    });

                    verFacturas($("#id_oc_hidden").val());
                }
            });




}

function cargaEstadoFacturaCBX(id_estado_factura) {

    var resultArray = [];

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: {id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            //cargaCBXSelected("estado_factura_cbx", result, id_estado_factura);
            resultArray = result;

        },
        url: "Pendientes/getEstadoFacturaCBX"
    });

    return resultArray;
}

function modalenviarAFacturacion(num_ficha, id_ficha, id_orden_compra) {

    $("#modal_facturacion_observacion").modal('show');
    $("#facturacion_texto").text("Observaciones: " + num_ficha);
    $("#facturacion_id_ficha").val(id_ficha);
    $("#facturacion_id_orden_compra").val(id_orden_compra);
    $("#facturacion_num_ficha").val(num_ficha);
}


function procesoTerminado(id_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            swal({
                title: "Ficha",
                text: "Error al enviar a Despacho",
                type: "error",
                html: true,
                confirmButtonText: "Aceptar",
                showCancelButton: false,
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
        },
        success: function (result) {

            swal({
                title: "Ficha",
                text: "El Proceso de la Ficha a Sido Terminado",
                type: "success",
                html: true,
                confirmButtonText: "Aceptar",
                showCancelButton: false,
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {

                            location.reload();
                        }
                    });

        },
        url: "Pendientes/procesoFicahTerminado"
    });


}
function enviarADespacho(id_ficha, id_orden_compra) {



    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha, id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            swal({
                title: "Ficha",
                text: "Error al enviar a Despacho",
                type: "error",
                html: true,
                confirmButtonText: "Aceptar",
                showCancelButton: false,
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
        },
        success: function (result) {

            swal({
                title: "Ficha",
                text: "La ficha ha sido enviada a Despacho",
                type: "success",
                html: true,
                confirmButtonText: "Aceptar",
                showCancelButton: false,
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {

                            location.reload();
                        }
                    });

        },
        url: "Pendientes/enviarFacturaADespacho"
    });
}


function modalSolicitudDeCorrecion(num_ficha, id_ficha, id_orden_compra) {
    $("#modal_solicitud_de_correccion").modal('show');

    $("numficha").html(num_ficha);
    $("#sdc_id_ficha").val(id_ficha);
    $("#sdc_id_orden_compra").val(id_orden_compra);
    $("#sdc_num_ficha").val(num_ficha);
}
function modalmiobservacion(num_ficha, id_ficha, id_orden_compra) {
    $("#modal_mi_observacion").modal('show');
    $("#mi_texto").text("Observación: " + num_ficha);
    $("#mi_id_ficha").val(id_ficha);

    $("#mi_id_orden_compra").val(id_orden_compra);

    $("#mi_num_ficha").val(num_ficha);
}

function updateidsence() {
    
    if (validarRequeridoForm($("#form_save_id_sence"))) {
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: $('#form_save_id_sence').serialize(),
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(jqXHR);
            },
            success: function (result) {
    
    
    
                swal({
                    title: 'ID SENCE',
                    text: 'ID SENCE Actualizado',
                    type: 'success',
                    html: true,
                    confirmButtonText: "Aceptar",
                    showCancelButton: false,
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
    
    
                        location.reload();
    
                    }
                });
            },
            url: "Pendientes/updateIDSENCE"
        });
    }
}

function modalidsence(num_ficha, id_orden_compra) {

    var id_sence = "";
    var html = '';

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_orden_compra: id_orden_compra,
            num_ficha: num_ficha
        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR+textStatus+errorThrown);
        },
        success: function (result) {
            console.log(result);
           // id_sence = result[0].id_sence;
           $.each(result, function (i, item) {
               if(item.sence_uso == 0) {
                   html += '<option value="';
                   html += item.id_accion_sence;
                   if (item.sence_activo == 1) {
                       id_sence = item.id_accion_sence;
                        //html += '" selected>';
                   }/* else {
                        html += '">';
                    }*/
                    html += '">';
                    html += item.id_accion_sence;
                    html += '</option>';
               }
            });
        },
        url: "Pendientes/getIDSENCEOC"
    });
    console.log(html);
    if(html == '') {
        id_sence = getsenceviejo(id_orden_compra);
    }
    $("#id_sence").val(id_sence);
    $("#opciones_id_sence").html(html);
    $("#modal_id_sence").modal('show');
    $("#texto_num_ficha").text("FICHA: " + num_ficha);
    $("#sence_id_orden_compra").val(id_orden_compra);
    $("#sence_num_ficha").val(num_ficha);
    // $("#mi_num_ficha").val(num_ficha);
}

function getsenceviejo(id_orden_compra) {
    var id_sence = "";
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR + textStatus + errorThrown);
        },
        success: function (result) {
            id_sence = result[0].id_sence;
        },
        url: "Pendientes/getIDSENCEOCV"
    });
    return id_sence;
}

function modalnumoc(num_ficha, id_orden_compra) {

    var num_orden_compra = "";

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        },
        success: function (result) {
            num_orden_compra = result[0].num_orden_compra;
        },
        url: "Pendientes/get_num_oc"
    });

    $("#num_oc").val(num_orden_compra);
    $("#modal_num_oc").modal('show');
    $("#texto_num_ficha_oc").text("FICHA: " + num_ficha);
    $("#oc_id_orden_compra").val(id_orden_compra);

}


function enviarSolicitudDeCorrecion() {
    $.ajax({
        async: false,
        cache: true,
        dataType: "json",
        type: 'POST',
        data: $('#form_sdc').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaFacturas(result);
        },
        url: "Pendientes/enviarSolicitudDeCorrecion"
    });
}

