$(document).ready(function() {

    $('#btn_nuevo').click(function() {
        location.href = 'Agregar';
    });

    cargaCBXall();
    listarFacturas();

    $('#p_ano').change(function() {
        listarFacturas();
    });

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $("#btn_buscar_ficha").click(function() {
        listarFacturas();
    });

    $("#btn_nueva_nota_credito").click(function() {
        NuevaNotaCredito();
    });

    $("#btn_nueva_nota_debito").click(function() {
        NuevaNotaDebito();
    });


    $("#btn_volver_modal_credito").click(function() {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function() {
            $("#sec_tabla").animate({
                height: "toggle"
            }, 500, function() {

            });
            $("#n_factura").val("");
            $("#num_nota_credito").val("");
            $("#fecha_emision").val("");
            $("#monto_credito").val("");
        });

    });

    $("#btn_volver_modal_debito").click(function() {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function() {
            $("#sec_tabla_debito").animate({
                height: "toggle"
            }, 500, function() {

            });
            $("#n_factura_debito").val("");
            $("#num_nota_debito").val("");
            $("#fecha_emision_debito").val("");
            $("#monto_debito").val("");
        });


    });

    $('#fecha_emision').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $('#fecha_emision_debito').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $("#frm_nota_credito").submit(function() {
        if ($("#n_factura").val() == "" || $("#num_nota_credito").val() == "" || $("#fecha_emision").val() == "" || $("#monto_credito").val() == "") {
            swal("Error", "Debe completar todos los campos solicitados", "error");
        } else {
            guardarNotaCredito();
        }
        event.preventDefault();
    });

    $("#btn_agregar_nota_debito").click(function() {
        if ($("#n_factura_debito").val() == "" || $("#num_nota_debito").val() == "" || $("#fecha_emision_debito").val() == "" || $("#monto_debito").val() == "") {
            swal("Error", "Debe completar todos los campos solicitados", "error");
        } else {
            guardarNotaDebito();
        }
    });
});

function cargaCBXall() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "holding");
        },
        url: "DocumentosTributarios/getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: {id_holding: $('#holding').val()},
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {

            cargaCombo(result, "empresa");
        },
        url: "DocumentosTributarios/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "DocumentosTributarios/getUsuarioTipoEcutivoComercial"
    });
}

function cargaTablaFacturas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_facturas')) {
        $('#tbl_facturas').DataTable().destroy();
    }


    $('#tbl_facturas').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "n_ficha" },
            { "mDataProp": "imputable_a" },
            { "mDataProp": "num_factura" },
            { "mDataProp": "tipo_documento" },
            { "mDataProp": "num_documento" },
            { "mDataProp": "fecha_emision" },
            { "mDataProp": "fecha_vencimiento" },
            { "mDataProp": "monto" },
            { "mDataProp": "estado" },
            { "mDataProp": "tipo_entrega" },
            { "mDataProp": "num_orden_compra" },
            { "mDataProp": "empresa" },
            { "mDataProp": "otic" },
            { "mDataProp": "otec" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";
                    var url_editar_doc = "";
                    switch (o.tipo_documento) {
                        case "Factura":
                            url_editar_doc = "EditarFactura/index/" + o.id_documento;
                            break;
                        case "Nota de Credito":
                            url_editar_doc = "EditarNotaCredito/index/" + o.id_documento;
                            break;
                        case "Nota de Debito":
                            url_editar_doc = "EditarNotaDebito/index/" + o.id_documento;
                            break;
                    }

                    var url_detalle_doc = "";
                    var mensaje = false;
                    switch (o.tipo_documento) {
                        case "Factura":
                            if (o.id_orden_compra == null) {
                                url_detalle_doc = '<a onclick="errorTipoDocumento()">Ver Detalle</a>';
                            } else {
                                url_detalle_doc = '<a href=' + "DetalleFicha/index/" + o.id_orden_compra + "/" + 0 + '>Ver Detalle</a>';
                            }
                            break;
                        case "Nota de Credito":
                            if (o.id_orden_compra == null) {
                                url_detalle_doc = '<a onclick="errorTipoDocumento()">Ver Detalle</a>';
                            } else {
                                url_detalle_doc = '<a href=' + "DetalleFicha/index/" + o.id_orden_compra + "/" + 0 + '>Ver Detalle</a>';
                            }
                            break;
                        case "Nota de Debito":
                            if (o.id_orden_compra == null) {
                                url_detalle_doc = '<a onclick="errorTipoDocumento()">Ver Detalle</a>';
                            } else {
                                url_detalle_doc = '<a href=' + "DetalleFicha/index/" + o.id_orden_compra + "/" + 0 + '>Ver Detalle</a>';
                            }
                            break;
                        default:
                            url_detalle_doc = "";

                    }



                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle ">Accion<span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu pull-right">';
                    html += '<li><a href=' + url_editar_doc + '>Editar</a></li>';
                    if (o.tipo_documento == "Factura") {
                        html += '<li><a onclick="openModalNotaDeCredito(' + o.id_documento + ')">Notas de Crédito</a></li>';
                        html += '<li><a onclick="openModalNotaDeDebito(' + o.id_documento + ')">Notas de Débito</a></li>';
                    }


                    html += '<li>' + url_detalle_doc + '</li>';
                    html += ' </ul>';
                    html += ' </div>';
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Empresa'
        }, {
            extend: 'pdf',
            title: 'Empresa'
        }, {
            extend: 'print',
            customize: function(win) {

                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

            }
        }],
        order: []
    });
}

function errorTipoDocumento() {

    swal("Documento no posee Detalle");
}

function listarFacturas() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {},
        success: function(result) {
            cargaTablaFacturas(result);
        },
        url: "DocumentosTributarios/listarfacturas"
    });
}


function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");


}

//METODOS PARA NOTA DE CREDITO 

function openModalNotaDeCredito(id_factura) {
    $("#id_fac_hidden").val(id_factura);
    console.log(id_factura);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_factura: id_factura
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            // cargaCombo(result, "modalidad");
            var n_factura = result[0].n_factura;
            var id_factura = result[0].id_factura;
            $("#modal_nota_credito").modal("show");
            $('#n_factura_hidden').val(n_factura);
            $('#id_factura_hidden').val(id_factura);
            loadTableNotaDeCredito(result);
        },
        url: "DocumentosTributarios/getNotasbyFactura"
    });

    //    getProgramasCBX();

}

/*function nuevaNotaCredito() {
    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function () {

        });
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            
            var html = "";
            var idcbx = 'tipo_entrega_nc';

            $('#n_factura').val(n_factura);

            $.each(result, function (i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });

}*/

function NuevaNotaCredito() {
    $("#n_factura").val($("#n_factura_hidden").val());
    $("#id_factura").val($("#id_factura_hidden").val());
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {},
        success: function(result) {

            var html = "";
            var idcbx = 'tipo_entrega_nc';

            $.each(result, function(i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });

    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function() {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function() {

        });
    });


}

function loadTableNotaDeCredito(data) {

    $("#sec_tabla").css({
        'display': 'block'
    });

    $("#sec_comentarios").css({
        'display': 'none'
    });

    $("#sec_nuevo").css({
        'display': 'none'
    });

    $("#sec_agregar_programa").css({
        'display': 'none'
    });

    $("#sec_editar").css({
        'display': 'none'
    });


    if (data[0] == undefined || data[0].id_estado_factura == 3) {
        $("#btn_nueva_nota_credito").prop('disabled', true);
        $("#btn_nueva_nota_credito").prop('title', 'Para crear una nota de crédito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_credito").prop('disabled', false);
        $("#btn_nueva_nota_credito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';


    $('#tbl_credito tbody').html(html);
    $.each(data, function(i, item) {

        if (item.id_nota_credito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += '<td>' + item.fecha_emision + '</td><td>' + item.n_factura + '</td><td>' + item.num_nota_credito + '</td><td>' + item.monto_credito + '</td>';


            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaCreditoEntrega(' + item.id_nota_credito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';
        }
    });

    $('#tbl_credito tbody').html(html);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_fac_hidden").val()
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {

        },
        url: "DocumentosTributarios/getFacturasbyOcCBX"
    });

}

function notaCreditoEntrega(id, nombre) {

    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function() {
        $("#sec_update_tipo_entrega_nc").animate({
            height: "toggle"
        }, 500, function() {

        });
    });
    //ajax para llenar select y id hidden
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {},
        success: function(result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc_2';
            $('#id_nc').val(id);
            $.each(result, function(i, item) {
                var selected = '';
                if (nombre.toLowerCase() == item.text.toLowerCase()) {
                    selected = "selected"
                }
                html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "DocumentosTributarios/tipo_entrega"
    });
}

function enviarFormNc() {
    //alert('se envia formulario para update de nc');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_nc: $("#id_nc").val(),
            id_tipo_entrega: $("#tipo_entrega_nc_2").val()
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#sec_update_tipo_entrega_nc').attr('style', 'display:none');
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_factura: $("#id_fac_hidden").val()
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function(result) {
                    // cargaCombo(result, "modalidad");
                    // cosole.log(result);
                    $('#n_factura_hidden').val(result[0].n_factura);
                    $("#modal_nota_credito").modal("show");
                    loadTableNotaDeCredito(result);
                },
                url: "DocumentosTributarios/getNotasbyFactura"
            });
        },
        url: "DocumentosTributarios/updateTipoEntrega_nc"
    });
}

function cancelarFormNc() {
    $("#sec_update_tipo_entrega_nc").animate({
        height: "toggle"
    }, 500, function() {
        $("#sec_tabla").animate({
            height: "toggle"
        }, 500, function() {

        });
    });
}


function guardarNotaCredito() {
    $.ajax({

        cache: false,
        async: false,
        dataType: 'json',
        type: 'POST',
        data: $('#frm_nota_credito').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {
            alert("ERROR")
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            swal({
                    title: result[0].titulo,
                    text: result[0].mensaje,
                    type: result[0].tipo_alerta,
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: true,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    console.log($("#id_factura").val());
                    openModalNotaDeCredito($("#id_factura").val());
                    $("#n_factura").val("");
                    $("#num_nota_credito").val("");
                    $("#fecha_emision").val("");
                    $("#monto_credito").val("");

                });

            listarFacturas();
        },

        url: "DocumentosTributarios/setNotaCredito"
    });
}

//METODOS PARA NOTA DE DEBITO 

function openModalNotaDeDebito(id_factura_nd) {


    $("#id_fac_hidden_nd").val(id_factura_nd);
    console.log(id_factura_nd);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_factura_nd: id_factura_nd
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            // cargaCombo(result, "modalidad");
            var n_factura_nd = result[0].n_factura_nd;
            var id_factura_nd = result[0].id_factura_nd;
            $("#modal_nota_debito").modal("show");
            $('#n_factura_hidden_nd').val(n_factura_nd);
            $('#id_factura_hidden_nd').val(id_factura_nd);
            loadTableNotaDeDebito(result);
        },
        url: "DocumentosTributarios/getNotasDebitobyFactura"
    });

    //    getProgramasCBX();

}



function NuevaNotaDebito() {

    $("#n_factura_nd").val($("#n_factura_hidden_nd").val());
    $("#id_factura_nd").val($("#id_factura_hidden_nd").val());

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function(jqXHR, textStatus, errorThrown) {},
        success: function(result) {

            var html = "";
            var idcbx = 'tipo_entrega_nd';

            $.each(result, function(i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });

    $("#sec_tabla_debito").animate({
        height: "toggle"
    }, 500, function() {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function() {

        });
    });


}

function loadTableNotaDeDebito(data) {

    $("#sec_tabla_debito").css({
        'display': 'block'
    });

    $("#sec_comentarios_debito").css({
        'display': 'none'
    });

    $("#sec_nuevo_debito").css({
        'display': 'none'
    });

    $("#sec_agregar_programa_debito").css({
        'display': 'none'
    });

    $("#sec_editar_debito").css({
        'display': 'none'
    });

    if (data[0] == undefined || data[0].id_estado_factura == 3) {
        $("#btn_nueva_nota_debito").prop('disabled', true);
        $("#btn_nueva_nota_debito").prop('title', 'Para crear una nota de crédito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_debito").prop('disabled', false);
        $("#btn_nueva_nota_debito").prop('title', '');
    }


    var html = "";
    var tipo_entre = '';

    $('#tbl_debito tbody').html(html);
    $.each(data, function(i, item) {

        if (item.id_nota_debito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += '<td>' + item.fecha_emision + '</td><td>' + item.n_factura_nd + '</td><td>' + item.num_nota_debito + '</td><td>' + item.monto_debito + '</td>';

            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaDebitoEntrega(' + item.id_nota_debito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';

        }

    });

    $('#tbl_debito tbody').html(html);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_factura: $("#id_fac_hidden_debito").val()
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {

        },
        url: "DocumentosTributarios/getFacturasbyOcCBX"
    });

}


function guardarNotaDebito() {
    $.ajax({

        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_debito")[0]),
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            swal({
                    title: "Guardado",
                    text: "La Nota de Crédito ha sido ingresada exitosamente.",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#8CD4F5",
                    confirmButtonText: "OK",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: true,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    console.log($("#id_factura_nd").val());
                    openModalNotaDeDebito($("#id_factura_nd").val());
                    $("#n_factura_nd").val("");
                    $("#num_nota_debito").val("");
                    $("#fecha_emision_debito").val("");
                    $("#monto_debito").val("");
                    $("#tipo_entrega_nd").val("");

                });

            listarFacturas();
        },
        url: "DocumentosTributarios/setNotaDebito"
    });
}

//---------------------------MODAL DOCUMENTOS INTERNOS-----------------------

//Modal Documentos Ficha

function getDocumentosFicha(id_ficha, num_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id_ficha
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            $('fichadoc').text(num_ficha);
            $("#modal_documentos_ficha").modal("show");
            loadTableDocumentosFicha(result);
        },
        url: "DocumentosTributarios/getDocumentosFicha"
    });
}

function loadTableDocumentosFicha(data) {
    var html = "";
    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function(i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';
        if (item.nombre_documento % 2 == 0) {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a>';
        } else {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a>';
        }
        html += '</td></tr>';
    });

    $('#tbl_documentos_factura tbody').html(html);
}

//Modal Documentos Rectificacion

function getDocumentosRectificacion(id_orden_compra, num_orden_compra) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            $('ocdoc').text(num_orden_compra);
            $("#modal_documentos_rectificacion").modal("show");
            loadTableDocumentosRectificacion(result);
        },
        url: "DocumentosTributarios/getDocumentosRectificacion"
    });
}

function loadTableDocumentosRectificacion(data) {
    var html = "";
    $('#tbl_documentos_rectificacion tbody').html(html);
    $.each(data, function(i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';
        if (item.nombre_documento % 2 == 0) {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a>';
        } else {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a>';
        }
        html += '</td></tr>';
    });

    $('#tbl_documentos_rectificacion tbody').html(html);
}