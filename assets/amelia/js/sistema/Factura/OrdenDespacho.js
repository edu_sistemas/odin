$(document).ready(function () {

    $('#btn_nuevo').click(function () {
        location.href = 'Agregar';
    });
    cargaCBXall();
    listarFichas();
    $("#btn_subir_factura").click(function () {
        $("#listaFac").animate({height: "toggle"}, 200, function () {

            $(this).stop(true, true);

            $("#formfac").animate({height: "toggle"}, 200, function () {
                $(this).stop(true, true);

            });

        });
    });

    $('#formfac').submit(function () {
        $.ajax({
            cache: false,
            async: true,
            contentType: false,
            processData: false,
            dataType: 'json',
            type: 'POST',
            data: new FormData($("#frm_subir_documento")[0]),
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR.responseText);
            },
            success: function (result) {
                console.log(result);
                if (result.status == "ok") {
                    $("#formfac").animate({height: "toggle"}, 200, function () {
                        $(this).stop(true, true);
                        $("#listaFac").animate({height: "toggle"}, 200, function () {
                            verFacturas($('#id_oc_hidden').val());
                            $(this).stop(true, true);
                        });
                    });
                }
            },
            url: "OrdenDespacho/upload_file"
        });
        event.preventDefault();
    });

    $('#frm_rechazo_ficha').submit(function () {
        FacturaRechazada();
        event.preventDefault();
    });
});


function cargaCBXall() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "holding");
        },
        url: "OrdenDespacho/getHoldingCBX"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: {id_holding: $('#holding').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "OrdenDespacho/getEmpresaCBX"
    });
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "OrdenDespacho/getUsuarioTipoEcutivoComercial"
    });
}
function cargaTablaFacturas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_fichas')) {
        $('#tbl_fichas').DataTable().destroy();
    }

    $('#tbl_fichas').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle "><i class="fa fa-angle-double-down"></i> <span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu">';
                    html += '<li><a onclick="FacturaAceptada(' + o.id_ficha + ',' + o.id_orden_compra + ')">Factura Aceptada</a></li>';
                    html += '<li class="divider"></li>';
                    html += '<li><a onclick="ModalRechazoFicha(' + o.id_ficha + ',' + o.num_ficha + ',' + o.id_orden_compra + ')">Factura Rechazada</a></li>';
                    html += ' </ul>';
                    html += ' </div>';
                    $("#oc_modal_credito").text(o.num_orden_compra);
                    return html;
                }
            },

            {"mDataProp": "dias_restantes"},
            {"mDataProp": "num_ficha"},
            {"mDataProp": "tipo_facturacion"},
            {"mDataProp": "razon_social_empresa"},
            {"mDataProp": "direccion_empresa"},
            {"mDataProp": "nombre_comuna"},
            {"mDataProp": "nombre_contacto"},
            {"mDataProp": "telefono_contacto"},
            {"mDataProp": "estado"},
            {"mDataProp": "tipo_entrega"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html = "<a href='#' title='Subir o editar documentos' onclick='verFacturas(" + o.id_orden_compra + ");'>Ver</a>";
                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'OrdenDespacho'
            }, {
                extend: 'pdf',
                title: 'OrdenDespacho'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [1, 'asc']
    });
}

function FacturaAceptada(id_ficha, id_orden_compra) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha, id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
        },
        success: function (result) {
            console.log(result);
            if (result[0].reset == 1) {
                location.reload();
            }
        },
        url: "OrdenDespacho/FacturaAceptada"
    });
}

function ModalRechazoFicha(id_ficha, num_ficha, id_orden_compra) {
    $('#titulo_rechazo_ficha').append(num_ficha);
    $('#orden_compra_rechazo').val(id_orden_compra);
    $('#ficha_rechazo').val(id_ficha);
    $('#estado_despacho').val(475);
    $('#modalRechazoFactura').modal({
        backdrop: 'static',
        keyboard: false
    });
    $("#modalRechazoFactura").modal('show');
}

function FacturaRechazada() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_rechazo_ficha').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            console.log(result);
            if (result[0].reset == 1) {
                location.reload();
            }
        },
        url: "OrdenDespacho/FacturaRechazada"
    });
}

function listarFichas() {

    $.ajax({
        async: false,
        cache: true,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaFacturas(result);
        },
        url: "OrdenDespacho/listar"
    });
}


function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });
    $("#" + item + " option[value='0']").remove();
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");


}

//METODOS PARA NOTA DE CREDITO 

function openModalNotaDeCredito(id_orden_compra) {

    $("#id_oc_hidden").val(id_orden_compra);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_nota_credito").modal("show");
            loadTableNotaDeCredito(result);
        },
        url: "OrdenDespacho/getNotasbyOrdenCompra"
    });
//    getProgramasCBX();

}

function nuevaNotaCredito() {
    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc';
            $.each(result, function (i, item) {
                var selected = '';
                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });
            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });
}

function loadTableNotaDeCredito(data) {

    $("#sec_tabla").css({
        'display': 'block'
    });
    $("#sec_comentarios").css({
        'display': 'none'
    });
    $("#sec_nuevo").css({
        'display': 'none'
    });
    $("#sec_agregar_programa").css({
        'display': 'none'
    });
    $("#sec_editar").css({
        'display': 'none'
    });
    if (data[0] == undefined || data[0].id_factura == null) {
        $("#btn_nueva_nota_credito").prop('disabled', true);
        $("#btn_nueva_nota_credito").prop('title', 'Para crear una nota de crédito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_credito").prop('disabled', false);
        $("#btn_nueva_nota_credito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';
    $('#tbl_credito tbody').html(html);
    $.each(data, function (i, item) {

        if (item.id_nota_credito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += ' <td>' + item.n_factura + '</td><td>' + item.num_nota_credito + '</td><td>' + item.monto_credito + '</td>';
            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaCreditoEntrega(' + item.id_nota_credito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';
        }
    });
    $('#tbl_credito tbody').html(html);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_oc_hidden").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("n_factura_cbx", result);
        },
        url: "OrdenDespacho/getFacturasbyOcCBX"
    });
}
function notaCreditoEntrega(id, nombre) {

    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_update_tipo_entrega_nc").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    //ajax para llenar select y id hidden
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc_2';
            $('#id_nc').val(id);
            $.each(result, function (i, item) {
                var selected = '';
                if (nombre.toLowerCase() == item.text.toLowerCase()) {
                    selected = "selected"
                }
                html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });
            $("#" + idcbx).html(html);
        },
        url: "OrdenDespacho/tipo_entrega"
    });
}

function cancelarForm() {

    $("#formfac").animate({height: "toggle"}, 200, function () {
        $(this).stop(true, true);
        $("#listaFac").animate({height: "toggle"}, 200, function () {
            $(this).stop(true, true);
        });
    });
}

function facturasCBX() {
    var id_orden_compra = $('#id_oc_facturas').val();

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaCBX("facturas_select", result);

        },
        url: "OrdenDespacho/getFacturasCBX"
    });
}


function enviarFormNc() {
//alert('se envia formulario para update de nc');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_nc: $("#id_nc").val(),
            id_tipo_entrega: $("#tipo_entrega_nc_2").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#sec_update_tipo_entrega_nc').attr('style', 'display:none');
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_orden_compra: $("#id_oc_hidden").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    // cargaCombo(result, "modalidad");
                    // cosole.log(result);
                    $("#modal_nota_credito").modal("show");
                    loadTableNotaDeCredito(result);
                },
                url: "OrdenDespacho/getNotasbyOrdenCompra"
            });
        },
        url: "OrdenDespacho/updateTipoEntrega_nc"
    });
}

function cancelarFormNc() {
    $("#sec_update_tipo_entrega_nc").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_tabla").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
}


function guardarNotaCredito() {
    $.ajax({

        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_credito")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: "Guardado",
                text: "La Nota de Crédito ha sido ingresada exitosamente.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "OK",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                    function (isConfirm) {
                        openModalNotaDeCredito($("#id_oc_hidden").val());
                        $("#n_factura_cbx").val("");
                        $("#num_nota_credito").val("");
                        $("#fecha_emision").val("");
                        $("#monto_credito").val("");
                    });
        },
        url: "OrdenDespacho/setNotaCredito"
    });
}

//METODOS PARA NOTA DE DEBITO 

function openModalNotaDeDebito(id_orden_compra) {

    $("#id_oc_hidden_debito").val(id_orden_compra);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_nota_debito").modal("show");
            loadTableNotaDeDebito(result);
        },
        url: "OrdenDespacho/getNotasDebitobyOrdenCompra"
    });
//    getProgramasCBX();

}

function nuevaNotaDebito() {
    $("#sec_tabla_debito").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nd';
            $.each(result, function (i, item) {
                var selected = '';
                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });
            $("#" + idcbx).html(html);
        },
        url: "OrdenDespacho/tipo_entrega"
    });
}

function loadTableNotaDeDebito(data) {

    $("#sec_tabla_debito").css({
        'display': 'block'
    });
    $("#sec_comentarios_debito").css({
        'display': 'none'
    });
    $("#sec_nuevo_debito").css({
        'display': 'none'
    });
    $("#sec_agregar_programa_debito").css({
        'display': 'none'
    });
    $("#sec_editar_debito").css({
        'display': 'none'
    });
    if (data[0] == undefined || data[0].id_factura == null) {
        $("#btn_nueva_nota_debito").prop('disabled', true);
        $("#btn_nueva_nota_debito").prop('title', 'Para crear una nota de débito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_debito").prop('disabled', false);
        $("#btn_nueva_nota_debito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';
    $('#tbl_debito tbody').html(html);
    $.each(data, function (i, item) {

        if (item.id_nota_debito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += '<td>' + item.fecha_emision + '</td><td>' + item.n_factura + '</td><td>' + item.num_nota_debito + '</td><td>' + item.monto_debito + '</td>';
            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaDebitoEntrega(' + item.id_nota_debito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';
        }

    });
    $('#tbl_debito tbody').html(html);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_oc_hidden_debito").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("n_factura_cbx_debito", result);
        },
        url: "OrdenDespacho/getFacturasbyOcCBX"
    });
}

function notaDebitoEntrega(id, nombre) {
    $("#sec_tabla_debito").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_update_tipo_entrega_nd").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    //ajax para llenar select y id hidden
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nd_2';
            $('#id_nd').val(id);
            $.each(result, function (i, item) {
                var selected = '';
                if (nombre.toLowerCase() == item.text.toLowerCase()) {
                    selected = "selected"
                }
                html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });
            $("#" + idcbx).html(html);
        },
        url: "OrdenDespacho/tipo_entrega"
    });
}
function enviarFormNd() {
//alert('se envia formulario para update de nd');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_nd: $("#id_nd").val(),
            id_tipo_entrega: $("#tipo_entrega_nd_2").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#sec_update_tipo_entrega_nd').attr('style', 'display:none');
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_orden_compra: $("#id_oc_hidden_debito").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    // cargaCombo(result, "modalidad");
                    // cosole.log(result);
                    $("#modal_nota_debito").modal("show");
                    loadTableNotaDeDebito(result);
                },
                url: "Historico/getNotasDebitobyOrdenCompra"
            });
        },
        url: "Historico/updateTipoEntrega_nd"
    });
}
function cancelarFormNd() {

    $("#sec_update_tipo_entrega_nd").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_tabla_debito").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
}
function guardarNotaDebito() {

    $.ajax({
        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_debito")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: "Guardado",
                text: "La Nota de Débito ha sido ingresada exitosamente.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "OK",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function (isConfirm) {
                openModalNotaDeDebito($("#id_oc_hidden_debito").val());
                $("#n_factura_cbx_debito").val("");
                $("#num_nota_debito").val("");
                $("#fecha_emision_debito").val("");
                $("#monto_debito").val("");
            });
        },
        url: "OrdenDespacho/setNotaDebito"
    });
}

//---------------------------MODAL DOCUMENTOS INTERNOS-----------------------

//Modal Documentos Ficha

function getDocumentosFicha(id_ficha, num_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id_ficha
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('fichadoc').text(num_ficha);
            $("#modal_documentos_ficha").modal("show");
            loadTableDocumentosFicha(result);
        },
        url: "OrdenDespacho/getDocumentosFicha"
    });
}
function loadTableDocumentosFicha(data) {
    var html = "";
    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';
        html += item.nombre_documento;
        html += '</td></tr>';
    });
    $('#tbl_documentos_factura tbody').html(html);
}

//Modal Documentos Rectificacion

function getDocumentosRectificacion(id_orden_compra, num_orden_compra) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('ocdoc').text(num_orden_compra);
            $("#modal_documentos_rectificacion").modal("show");
            loadTableDocumentosRectificacion(result);
        },
        url: "OrdenDespacho/getDocumentosRectificacion"
    });
}
function loadTableDocumentosRectificacion(data) {
    var html = "";
    $('#tbl_documentos_rectificacion tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';
        html += item.nombre_documento;
        html += '</td></tr>';
    });
    $('#tbl_documentos_rectificacion tbody').html(html);
}


function verFacturas(id_orden_compra) {

    $("#formfac").attr('style', 'display:none;');
    $("#listaFac").attr('style', 'left: 50px;');
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('#modal_facturas').modal("show");
            $('#modal_facturas').append('<input type="hidden" id="id_oc_facturas" value="' + id_orden_compra + '">');
            facturasCBX();
            cargaModalFacturas(result);
        },
        url: "OrdenDespacho/getFacturasByOC"
    });
}

function cargaModalFacturas(data) {
    $('#oc_modal').html(data[0].num_orden_compra);
    $('#id_oc_hidden').val(data[0].id_orden_compra);
    var html = "";
    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function (i, item) {

        if (item.n_factura == null) {
            html += '<p style="margin: 35px; padding: auto; text-align:center; vertical-align:center;"><b>No existen facturas para la orden de compra.</b></p>';
        } else {
            var pStyle = "black";
            if (item.id_estado_factura == "1") {
                pStyle = "red";
            }
            if (item.id_estado_factura == "2") {
                pStyle = "green";
            }
            if (item.id_estado_factura == "3") {
                pStyle = "blue";
            }
            html += '<tr>';
            html += '<td>' + item.n_factura + ' </td>';
            var tipoEntrega = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.fecha_registro + '</td>';
            html += '<td>' + item.monto_factura + '</td>';
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td>' + item.fecha_entrega + '</td>';
            html += '<td style="color:' + pStyle + ';">' + item.nombre_estado + ' (' + item.fecha_estado + ')</td>';
            html += '<td>';
            html += '  <div class="btn-group">';
            html += '  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle ">Opciones<span class="caret"></span></button>';
            html += '  <ul class="dropdown-menu pull-right">';
            html += '	<li><a onclick="formulario(' + item.id_factura + ',' + tipoEntrega + ');">Entrega</a></li>';
            html += '	<li><a onclick="cambiarEstadoFactura(' + item.id_factura + ', ' + item.id_estado_factura + ')">Estado Factura</a></li>';
            html += ' </ul>';
            html += ' </div>';
            html += '</td></tr>';
        }

    });
    $('#tbl_documentos_factura tbody').html(html);
}

function formulario(id, nombre) {
    $("#listaFac").animate({
        left: "+=50",
        height: "toggle"
    }, 500, function () {
        $("#formfac").animate({
            left: "+=50",
            height: "toggle"
        }, 500, function () {
//buscar lista de tipo entrega
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#frm').serialize(),
                error: function (jqXHR, textStatus, errorThrown) {
                },
                success: function (result) {
                    var html = "";
                    var idcbx = 'tipo_entrega_f';
                    $('#id_fact').val(id);
                    $.each(result, function (i, item) {
                        var selected = '';
                        if (nombre.toLowerCase() == item.text.toLowerCase()) {
                            selected = "selected"
                        }
                        html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                        selected = '';
                    });
                    $("#" + idcbx).html(html);
                },
                url: "OrdenDespacho/tipo_entrega"
            });
        });
    });
}

function cambiarEstadoFactura(id_factura, id_estado_factura) {

    var data = cargaEstadoFacturaCBX(id_estado_factura);
    var html5 = "";
    html5 += '<select id="estado_factura_cbx">';
    $.each(data, function (i, item) {


        html5 += '<option value="';
        html5 += item.id;
        if (item.id == id_estado_factura) {
            html5 += '" selected>';
        } else {
            html5 += '">';
        }
        html5 += item.text;
        html5 += '</option>';
    });
    html5 += '</select>';
    swal({
        title: "Seleccione el nuevo estado de la factura",
        text: html5,
        //type: "warning",
        html: true,
        showCancelButton: true,
        //confirmButtonColor: "#DD6B55",
        confirmButtonText: "Actualizar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {

                    var id_estado_nuevo = $('#estado_factura_cbx').val();
                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {id_factura: id_factura, id_estado_nuevo: id_estado_nuevo},
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        success: function (result) {
                            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                        },
                        url: "OrdenDespacho/updateEstadoFactura"
                    });
                    verFacturas($("#id_oc_hidden").val());
                }
            });
}

function cargaEstadoFacturaCBX(id_estado_factura) {

    var resultArray = [];
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: {id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            //cargaCBXSelected("estado_factura_cbx", result, id_estado_factura);
            resultArray = result;
        },
        url: "OrdenDespacho/getEstadoFacturaCBX"
    });
    return resultArray;
}

function enviarAFacturacion(id_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            swal({
                title: "Ficha",
                text: "Error al enviar a facturación",
                type: "error",
                html: true,
                confirmButtonText: "Aceptar",
                showCancelButton: false,
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
        },
        success: function (result) {

            swal({
                title: "Ficha",
                text: "La ficha ha sido enviada a facturación",
                type: "success",
                html: true,
                confirmButtonText: "Aceptar",
                showCancelButton: false,
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {

                            location.reload();
                        }
                    });
        },
        url: "OrdenDespacho/enviarFacturaAFacturacion"
    });
}



function enviarADespacho(id_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
            swal({
                title: "Ficha",
                text: "Error al enviar a Despacho",
                type: "error",
                html: true,
                confirmButtonText: "Aceptar",
                showCancelButton: false,
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
        },
        success: function (result) {

            swal({
                title: "Ficha",
                text: "La ficha ha sido enviada a Despacho",
                type: "success",
                html: true,
                confirmButtonText: "Aceptar",
                showCancelButton: false,
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {

                            location.reload();
                        }
                    });
        },
        url: "OrdenDespacho/enviarFacturaADespacho"
    });
}


function modalSolicitudDeCorrecion(num_ficha, id_ficha) {
    $("#modal_solicitud_de_correccion").modal('show');
    $("numficha").html(num_ficha);
    $("#sdc_id_ficha").val(id_ficha);
    $("#sdc_num_ficha").val(num_ficha);
}

function enviarSolicitudDeCorrecion() {
    $.ajax({
        async: false,
        cache: true,
        dataType: "json",
        type: 'POST',
        data: $('#form_sdc').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaFacturas(result);
        },
        url: "OrdenDespacho/enviarSolicitudDeCorrecion"
    });
}

// function verFacturas(id_orden_compra) {

//     $("#formfac").attr('style', 'display:none;');
//     $("#listaFac").attr('style', 'left: 50px;');

//     $.ajax({
//         async: true,
//         cache: false,
//         dataType: "json",
//         type: 'POST',
//         data: {
//             id_orden_compra: id_orden_compra
//         },
//         error: function (jqXHR, textStatus, errorThrown) {
//             // code en caso de error de la ejecucion del ajax
//             //  console.log(textStatus + errorThrown);
//         },
//         success: function (result) {
//             $('#modal_facturas').modal("show");
//             cargaModalFacturas(result);
//         },
//         url: "OrdenDespacho/getFacturasByOC"
//     });
// }

function cargaModalFacturas(data) {
    $('#oc_modal').html(data[0].num_orden_compra);
    $('#id_oc_hidden').val(data[0].id_orden_compra);
    var html = "";

    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function (i, item) {

        if (item.n_factura == null || item.nombre_documento == null) {
            html += '<p style="margin: 35px; padding: auto; text-align:center; vertical-align:center;"><b>No hay ningún archivo subido</b></p>';

        } else {
            var pStyle = "black";

            if (item.id_estado_factura == "1") {
                pStyle = "red";
            }
            if (item.id_estado_factura == "2") {
                pStyle = "green";
            }
            if (item.id_estado_factura == "3") {
                pStyle = "blue";
            }
            html += '<tr>';
            html += '<td><a target="_blank" href="../../' + item.ruta_documento + '">' + item.n_factura + '</a></td>';
            var tipoEntrega = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.fecha_registro + '</td>';
            html += '<td>' + item.fecha_archivo + '</td>';
            html += '<td>' + item.tipo_documento + '</td>';
            html += '<td>' + item.monto_factura + '</td>';
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td>' + item.fecha_entrega + '</td>';
            html += '<td style="color:' + pStyle + ';">' + item.nombre_estado + ' (' + item.fecha_estado + ')</td>';
            html += '</tr>';
        }

    });

    $('#tbl_documentos_factura tbody').html(html);
}