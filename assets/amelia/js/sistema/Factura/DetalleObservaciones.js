$(document).ready(function () {
    listarObservaciones();
});


function cargaTablaObservaciones(data) {

    if ($.fn.dataTable.isDataTable('#tbl_observaciones')) {
        $('#tbl_observaciones').DataTable().destroy();
    }


    $('#tbl_observaciones').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "fecha_registro"},
            {"mDataProp": "usuario"},
            {"mDataProp": "tipo_obs"},
            {"mDataProp": "observacion"}
        ],
        pageLength: 50,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Observaciones'
            }, {
                extend: 'pdf',
                title: 'Observaciones'
            }, {
                extend: 'print',
                customize: function (win) {

                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');

                }
            }
        ],
        order: [0, 'desc']
    });
}

function listarObservaciones() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: $("#id_ficha").val()},
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaObservaciones(result);
        },
        url: "../../DetalleObservaciones/listarObservaciones"
    });
}

 