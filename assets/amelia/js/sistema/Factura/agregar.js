$(document).ready(function () {


    $('#fecha_emision').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#fecha_vencimiento').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });



    $("#btn_registrar").click(function () {
        if (validarRequeridoForm($("#frm_factura_registro"))) {
            send();
        }
    });
    llamaDatosControles();
});

function send() {

    $.ajax({
        cache: false,
        async: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_factura_registro")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // console.log(textStatus);
            // console.log(errorThrown);

            console.log(jqXHR.responseText);
        },
        success: function (result) {
            // console.log(result);
            //
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);

            if (result[0].reset == 1) {

                setTimeout(function () {

                    location.reload();
                }, 2000);


            }

        },
        url: '../AgregarFactura'
    });

}


function llamaDatosControles() {

    var id_orden_compra = $('#id_orden_compra').val();

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result);
        },
        url: "../getTipoFacturaCBX"
    });

}

function cargaCombo(midata) {
    $("#tipo_factura").select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

}

 