var url_init = "ValidarAlumno/";
$(document).ready(function () {
    recargaLaPagina();
    contarCaracteres("comentarios_email", "text-out", 2000);



    var id_ficha_interna = $("#cbx_fichas").val();

    if (id_ficha_interna == "") {
        id_ficha_interna = "";
    }

    if ($("#id_ficha_consulta").val().trim() != "") {
        url_init = "../";
    }

    $("#cbx_fichas").select2({
        placeholder: "Todos",
        allowClear: true
    });

    $("#btn_refresh").click(function () {
        var btn = $(this);
        simpleLoad(btn, true);
        simpleLoad(btn, false);
        iniciarcontroles();
    });

    $("#cbx_fichas").change(function () {
        var btn = $("#btn_refresh");
        simpleLoad(btn, true);
        simpleLoad(btn, false);
        iniciarcontroles();
    });


    $("#btn_buscar").click(function () {
        var btn = $("#btn_refresh");
        simpleLoad(btn, true);
        simpleLoad(btn, false);
        iniciarcontroles();
    });

    $("#solicitar_boton").click(function () {
        enviaInformacionCorreo();

    });

    $("#todo_ok_boton").click(function () {
        todoOK();
    });

    iniciarcontroles();
});

function enviaInformacionCorreo() {


    var alumnos = recorreCheckBoxChecked();

    var alumnosobjeto = getAlumnosListados();


    var comentario = $("#comentarios_email").val();

    if (alumnos.length > 0 && comentario.length > 0) {

        var id_ficha_interna = $("#cbx_fichas").val();

        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {
                id_ficha: id_ficha_interna,
                ids_participantes: alumnos,
                observaciones: comentario
            },
            error: function (jqXHR, textStatus, errorThrown) {

                alerta("ERROR", jqXHR.responseText, "error");
            },
            success: function (result) {
                sendMail(result, alumnosobjeto);

            },
            url: url_init + "getDatosAlumnosyDatosFicha"

        });


    } else {

        alerta('Validación', 'No se han seleccionado participantes o no se han ingresado observaciones', 'warning');

    }

}


function todoOK() {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: $("#cbx_fichas").val(),
            comentarios: $("#comentarios_email").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("ERROR", jqXHR.responseText, "error");
        },
        success: function (result) {

            alerta('Validación', 'Nos Alegra saber que esta todo ok en esta venta :)', 'success');

            setTimeout(function () {
                location.reload();
            }, 2000);

            //  cargaDatosFicha(result);

        },
        url: "ValidarAlumno/todoOk"

    });
}
function  sendMail(result, alumnosobjeto) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "text",
        type: 'POST',
        data: {
            data_ficha: result,
            data_alumnos: alumnosobjeto,
            comentarios: $("#comentarios_email").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("ERROR", jqXHR.responseText, "error");
        },
        success: function (result) {
            alerta('Validación', 'Se ha enviado una notifiación con su solicitud', 'success');


            setTimeout(function () {
                location.reload();
            }, 2000);


            //  cargaDatosFicha(result);

        },
        url: url_init + "enviaEmail"

    });


}


function recorreCheckBoxChecked() {

    var listaAlumnos = '';

    $("input[name=participante_check]").each(function () {
        if ($(this).is(':checked')) {
            listaAlumnos = $(this).val() + ',';
        }
    });

    if (listaAlumnos.length > 1) {
        listaAlumnos = listaAlumnos.substr(0, listaAlumnos.length - 1);
    }

    return listaAlumnos;
}
function getAlumnosListados() {

    var alumnos = [];

    $("input[name=participante_check]").each(function () {
        if ($(this).is(':checked')) {
            var id_alumno = $(this).val();
            //  alumnos.push([$("#rut_" + id_alumno).text(), $("#nombre_" + id_alumno).text()]);

            alumnos.push({
                rut: $("#rut_" + id_alumno).text(),
                nombre: $("#nombre_" + id_alumno).text(),
                sence: $("#sence_" + id_alumno).text(),
                interna: $("#interna_" + id_alumno).text(),
                dj: $("#dj_" + id_alumno).text()
            });
        }
    });

    return alumnos;
}



function iniciarcontroles() {



    var id_ficha_interna = $("#cbx_fichas").val();

    getPreviewFicha(id_ficha_interna, url_init);


}

function getPreviewFicha(id_ficha_interna, pre_url) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha_interna},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", jqXHR.responseText, "error");
        },
        success: function (result) {

            cargaDatosFicha(result);

        },
        url: pre_url + "getPreviewFicha"

    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha_interna},
        error: function (jqXHR, textStatus, errorThrown) {

            console.log(jqXHR.responseText);
            // alerta("Ficha", jqXHR.responseText, "error");
        },
        success: function (result) {
            cargarTablaAlumnos(result);
        },
        url: pre_url + "getDataValidarFicha"

    });


}

/* carga tablas */
function cargaDatosFicha(data) {


    var html = "";
    $('#tb_resumen_ficha').html(html);
    $.each(data, function (i, item) {
        html += '<tr><th>Holding:</th>';
        html += '<td>';
        html += item.holding;
        html += '<td>';

        html += '<th>Empresa:</th>';
        html += '<td>';
        html += item.empresa;
        html += '<td></tr>';

        html += '<tr><th>Rut:</th>';
        html += '<td>';
        html += item.rut_empresa;
        html += '<td>';

        html += '<th>Curso:</th>';
        html += '<td>';
        html += item.descripcion_producto;
        html += '<td></tr>';

        html += '<tr><th>Versión:</th>';
        html += '<td>';
        html += item.descripcion_version;
        html += '<td>';

        html += '<th>Modalidad:</th>';
        html += '<td>';
        html += item.modalidad;
        html += '<td></tr>';

        html += '<tr><th>Código Sence:</th>';
        html += '<td>';
        html += item.codigo_sence;
        html += '<td>';

        html += '<th>Fecha inicio:</th>';
        html += '<td>';
        html += item.fecha_inicio;
        html += '<td></tr>';

        html += '<tr><th>Fecha Término:</th>';
        html += '<td>';
        html += item.fecha_fin;
        html += '<td>';

        html += '<th>Horario:</th>';
        html += '<td>';
        html += item.hora_inicio + ' a ' + item.hora_termino;
        html += '<td></tr>';

        html += '<tr><th>Sede:</th>';
        html += '<td>';
        html += item.nombre_sede;
        html += '<td>';

        html += '<th>Sala:</th>';
        html += '<td>';
        html += item.nombre_sala;
        html += '<td></tr>';

        html += '<tr><th>Dirección:</th>';
        html += '<td>';
        html += item.direccion_sede;
        html += '<td>';

        html += '<th>Días:</th>';
        html += '<td>';
        html += item.dias;
        html += '<td></tr>';

        html += '<tr><th>Ejecutivo Comercial:</th>';
        html += '<td>';
        html += item.ejecutivo;
        html += '<td>';

        html += '<tr><th>Ingresado POR:</th>';
        html += '<td>';
        html += item.usuario_ingreso;
        html += '<td>';

        html += '<th>Observaciones:</th>';
        html += '<td>';
        html += item.comentario_orden_compra;
        html += '<td></tr>';

        $('#estado_ficha').val(item.id_estado);



    });

    $('#tb_resumen_ficha').html(html);

    if ($('#estado_ficha').val() >= 60) {

        $('#fila_botones').removeClass("hidden");

    } else {
        $('#fila_botones').addClass("hidden");

    }


}


function cargarTablaAlumnos(data) {

    var utl_extra = "";

    var html = "";
    $('#tbl_praticipantes_validacion tbody').html(html);

    var id_ficha_interna = $("#cbx_fichas").val();

    if (id_ficha_interna == "") {
        id_ficha_interna = "";
    }

    if ($("#id_ficha_consulta").val().trim() != "") {
        utl_extra = "../../";
    }

    $.each(data, function (i, item) {

        html += '            <tr>';
        html += '              <td id="rut_' + item.id_detalle_alumno + '">' + item.rut + '</td>';
        html += '              <td id="nombre_' + item.id_detalle_alumno + '">' + item.nombre + '</td>';
        html += '              <td>' + item.procen_franquiciable + '</td>';
        html += '              <td>' + item.centro_costo + '</td>';
        html += '              <td>' + item.promedio + '</td>';
        html += '              <td id="sence_' + item.id_detalle_alumno + '">' + item.asist_sence + '</td>';
        html += '              <td id="interna_' + item.id_detalle_alumno + '">' + item.asistencia_interna + '</td>';
        html += '              <td id="dj_' + item.id_detalle_alumno + '">' + item.estado_dj + '</td>';
        html += '              <td>' + item.estado_alumno + '</td>';

        //  html += '              <td>' + item.estado_final + '</td>';
        // html += '              <td><img src="../../assets/img/ok.png" alt="Facturable"  ></td>';
        switch (item.estado_final) {
            case "0":
                html += '              <td><img src="' + utl_extra + '../../assets/img/no.png" alt="No Facturable"  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                html += '   <input type="checkbox" name="participante_check" id="participante_check" value="' + item.id_detalle_alumno + '"> </td>';
                break;
            case "1":
                html += '              <td><img src="' + utl_extra + '../../assets/img/ok.png" alt="Facturable"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                html += '   <input type="checkbox" name="participante_check" id="participante_check" value="' + item.id_detalle_alumno + '"> </td>';
                break;
            case "2":
                html += '              <td><img src="' + utl_extra + '../../assets/img/stop.png" alt="Error al Comprobar"></td>';
                break;
            default :
                html += '              <td><img src="' + utl_extra + '../../assets/img/stop.png" alt="Error no corroborado">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                html += '   <input type="checkbox" name="participante_check" id="participante_check" value="' + item.id_detalle_alumno + '"></td>';
                break;



        }

        //   html += '              <td>' + item.estado_final + '</td>';


        html += '            </tr>';


    });

    $('#tbl_praticipantes_validacion tbody').html(html);



}