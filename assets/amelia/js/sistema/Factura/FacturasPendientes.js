$(document).ready(function () {
    
    
        listar();
    
    $('#p_ano').change(function(){
        listar();
    });
    
 });


function cargaTablaFacturasPendientes(data) {

    if ($.fn.dataTable.isDataTable('#tbl_facturas_pendientes')) {
        $('#tbl_facturas_pendientes').DataTable().destroy();
    }

    $('#tbl_facturas_pendientes').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            { "mDataProp": "otec"},
            { "mDataProp": "num_orden_compra"},
            { "mDataProp": "empresa"},
            { "mDataProp": "total_otic"},
            { "mDataProp": "total_facturado_otic"},
            { "mDataProp": "total_pendiente_otic"},
            { "mDataProp": "total_empresa"},
            { "mDataProp": "total_facturado_empresa"},
            { "mDataProp": "total_pendiente_empresa"},
            { "mDataProp": "total_ficha"},
            { "mDataProp": "monto_facturado"},
            { "mDataProp": "monto_pendiente"},
            { "mDataProp": "fecha_inicio"},
            { "mDataProp": "fecha_termino"},
            { "mDataProp": "ejecutivo"},
            { "mDataProp": "dias_transcurridos"},
            { "mDataProp": "estado"}            

        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Facturas Pendientes'
            }, {
                extend: 'pdf',
                title: 'Facturas Pendientes'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [9, 'asc']
    });
}

function listar() {
    $.ajax({
        url: "FacturasPendientes/listar",
        type: 'POST',
        dataType: "json",
        data: $('#frm').serialize(),
        beforeSend: function () {
            $("#loading-container").html('<img src="../../assets/img/loading.gif">');
        },
        success: function (result) {
           
            cargaTablaFacturasPendientes(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            $('#loading-container').html("Error en la carga");
        },
        complete: function () {
            $("#loading-container").html("");
        },
        async: true,
        cache: false
    });
}
