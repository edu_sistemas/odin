$(document).ready(function() {
    cargaCbxAll();
    $("#btn_back").click(function() {
        // retrocede una pagina
        limpiarFormulario();
        // history.back();
    });

    contarCaracteres("comentario", "text-out", 1000);
    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $("#cbx_orden_compra").change(function() {
        var id_ficha = "";

        var id_oc = "";


        id_ficha = $("#id_ficha").val();

        id_oc = $("#cbx_orden_compra").val();


        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: { ficha: id_ficha, oc: id_oc },
            error: function(jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                //  console.log(textStatus + errorThrown);
            },
            success: function(result) {
                cargaTablaDatosAlumnos(result);
            },
            url: "../cargaDatosOC"
        });

    });

    inicializaControles();

    $("#btn_enviar_ficha").click(function() {
        var ficha = $("#id_ficha").val();
        validaPreEnvioFicha(ficha);
    });

    $(':checkbox[readonly=readonly]').click(function() {
        return false;
    });

    selectIdEdutecno();

});

function selectIdEdutecno() {

    var id_ficha = $('#id_ficha').val();
    var id_edutecno = $('#id_edutecno').val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_ficha: id_ficha },
        error: function(jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function(result) {
            //        	$("#modal_edutecno").modal("show");
            //        	$("#modal_id_ficha2").html(id_ficha);
            //        	$("#id_ficha_edutecno").val(id_ficha);

            cargaCBXSelected('edutecno_cbx', result, $("#id_edutecno_original").val());

        },
        url: "../../getidEdutecnoCBX"
    });
}

function inicializaControles() {
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }
}

function cargaCbxAll() {


    var id_ficha = "";
    var id_oc = "";
    id_ficha = $("#id_ficha").val();
    id_oc = $("#cbx_orden_compra").val();

    /* Informacion actual */
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ficha: id_ficha, oc: id_oc },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaDatosAlumnos(result);
        },
        url: "../../cargaDatosOC"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ficha: id_ficha, oc: id_oc },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../../cargaDatosOCDocumentoResumen"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { ficha: id_ficha, oc: id_oc },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaLastDocumentos(result);
        },
        url: "../../cargaLastDocumentoResumen"
    });

    /* End  Informacion actual */


    /* Rectificaciones */

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_orden_compra: id_oc },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaRectficacionesAlumnos(result);
        },
        url: "../../getRectificacionesAlumnosByOC"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_orden_compra: id_oc },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaDatoDocumentosRectificacione(result);
        },
        url: "../../getRectificacionesAlumnosDocbyOrdenCompra"
    });

    /* End Rectificaciones */

    /* Extensiones */

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_orden_compra: id_oc },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaRectficacionesExtensiones(result);
        },
        url: "../../getRectificacionesExtensionByOC"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_orden_compra: id_oc },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaDatoDocumentosExtensiones(result);
        },
        url: "../../getRectificacionesExtensionDocByOC"
    });
    /* END  Extensiones */

    /*Factura */


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: { id_orden_compra: id_oc },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function(result) {
            cargaTablaDatosFactura(result);
        },
        url: "../../getFacturaByOC"
    });

    /*End factura */



}

/* Datos Actuales */
function cargaTablaDatosAlumnos(data) {

    if (data.length > 0) {
        $("#total_alumnos").html(data[0].total_alumnos);
        $("#valor_final").html(data[0].valor_final);
        $("#total_sence").html(data[0].total_sence);
        $("#total_empresa").html(data[0].total_empresa);
        $("#total_sence_empresa").html(data[0].total_sence_empresa);

        // $("#total_agregado").html(data[0].total_agregado);
        $("#total_becados").html(data[0].total_becados);
        $("#total_oc").html(data[0].total_oc);
    }

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno')) {
        $('#tbl_orden_compra_carga_alumno').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "rut" },
            { "mDataProp": "nombre" },
            { "mDataProp": "centro_costo" },
            { "mDataProp": "fq" },
            //            {"mDataProp": "valor_otic"},
            //            {"mDataProp": "valor_empresa"},
            //            {"mDataProp": "valor_agregado"},
            { "mDataProp": "valor_total" },
            { "mDataProp": "num_orden_compra" },
            { "mDataProp": "id_sence" }
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Edutecno'
        }, {
            extend: 'pdf',
            title: 'Edutecno'
        }, {
            extend: 'print',
            customize: function(win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }
        }]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "tipo_documento" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";

                    html = '<a href="../../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            { "mDataProp": "fecha_registro" }
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"clear">',
        order: [2, "desc"]

    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaLastDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_last_documetnos')) {
        $('#tbl_orden_compra_last_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_last_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            // {"mDataProp": "tipo_documento"},
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";

                    html = '<a href="../../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            { "mDataProp": "fecha_registro" }

        ],
        pageLength: 10,
        responsive: false,
        dom: '<"clear">',
        order: [1, "desc"]

    });

    $('[data-toggle="tooltip"]').tooltip();

}
/* end Datos Actuales */


/* Rectificaciones */
function cargaTablaRectficacionesAlumnos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_carga_alumno_rectificacion')) {
        $('#tbl_orden_compra_carga_alumno_rectificacion').DataTable().destroy();
    }

    $('#tbl_orden_compra_carga_alumno_rectificacion').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "tipo_rectificacion" },
            { "mDataProp": "rut" },
            { "mDataProp": "nombre_alumno" },
            { "mDataProp": "apellido_paterno" },
            { "mDataProp": "centro_costo" },
            { "mDataProp": "porcentaje_franquicia" },
            //            {"mDataProp": "costo_otic"},
            //            {"mDataProp": "costo_empresa"},
            //            {"mDataProp": "valor_agregado"},
            { "mDataProp": "valor_total" },
            { "mDataProp": "causa" },
            { "mDataProp": "fecha_rectificacion" },
            { "mDataProp": "estado" }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Edutecno'
        }, {
            extend: 'pdf',
            title: 'Edutecno'
        }, {
            extend: 'print',
            customize: function(win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }
        }]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentosRectificacione(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documentos_rectificaciones')) {
        $('#tbl_orden_compra_documentos_rectificaciones').DataTable().destroy();
    }

    $('#tbl_orden_compra_documentos_rectificaciones').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "tipo_documento" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";

                    html = '<a href="../../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            { "mDataProp": "fecha_registro" }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"clear">',
        order: [2, "desc"]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

/* End Rectificaciones */


/* extensiones */

function cargaTablaRectficacionesExtensiones(data) {

    if ($.fn.dataTable.isDataTable('#tbl_rectificacion_extension_plazo')) {
        $('#tbl_rectificacion_extension_plazo').DataTable().destroy();
    }

    $('#tbl_rectificacion_extension_plazo').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "tipo_rectificacion" },
            { "mDataProp": "fecha_inicio" },
            { "mDataProp": "fecha_cierre" },
            { "mDataProp": "fecha_extension" },
            { "mDataProp": "num_orden_compra" },
            { "mDataProp": "fecha_rectificacion" },
            { "mDataProp": "estado" }

        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Edutecno'
        }, {
            extend: 'pdf',
            title: 'Edutecno'
        }, {
            extend: 'print',
            customize: function(win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }
        }],
        order: [5, "desc"]
    });

    $('[data-toggle="tooltip"]').tooltip();

}

function cargaTablaDatoDocumentosExtensiones(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documentos_extensiones')) {
        $('#tbl_orden_compra_documentos_extensiones').DataTable().destroy();
    }

    $('#tbl_orden_compra_documentos_extensiones').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "tipo_documento" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = "";

                    html = '<a href="../../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            { "mDataProp": "fecha_registro" }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"clear">',
        order: [2, "desc"]

    });

    $('[data-toggle="tooltip"]').tooltip();

}

/* End extensiones */


/*Factura */

function cargaTablaDatosFactura(data) {

    if ($.fn.dataTable.isDataTable('#tbl_facturas_orden_compra')) {
        $('#tbl_facturas_orden_compra').DataTable().destroy();
    }


    /*
     <th>Tipo</th>        
     <th>N° FACTURA</th>
     <th>Valor Factura</th>
     <th>Tipo de Entrega</th>
     <th>Estado Factura</th>
     <th>Opciones</th> 
     */

    $('#tbl_facturas_orden_compra').DataTable({
        "aaData": data,
        "aoColumns": [
            { "mDataProp": "tipo_facturacion" },
            { "mDataProp": "n_factura" },
            { "mDataProp": "monto_factura" },
            { "mDataProp": "tipo_entrega" },
            { "mDataProp": "nombre_estado" },
            {
                "mDataProp": null,
                "bSortable": false,
                "mRender": function(o) {
                    var html = " ";
                    if (o.nombre_estado == 'Pendiente de Pago') {
                        html += '  <div class="btn-group">';
                        html += "  <button type='button' onclick='estadoFacura(" + o.id_factura + ",2)'  class='btn btn-primary dropdown-toggle'>Pagada</button>   <br>";
                        html += "  <button type='button' onclick='estadoFacura(" + o.id_factura + ",3)'  class='btn btn-danger dropdown-toggle'>Anulada</button>  ";
                        html += ' </div>';
                    } else {
                        html += '  <div class="btn-group">';
                        html += "   Sin Opciones   ";
                        html += ' </div>';
                    }
                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [{
            extend: 'copy'
        }, {
            extend: 'csv'
        }, {
            extend: 'excel',
            title: 'Edutecno'
        }, {
            extend: 'pdf',
            title: 'Edutecno'
        }, {
            extend: 'print',
            customize: function(win) {
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
            }
        }],
        order: [1, "desc"]
    });

    $('[data-toggle="tooltip"]').tooltip();

}


function estadoFacura(id_factura, estado_factura) {



    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_factura: id_factura,
            estado_factura: estado_factura
        },
        error: function(jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function(result) {
            console.log(result);
            cargaCbxAll();
        },
        url: "../../setEstadoFactura"
    });


}



/* End factura */