$(document).ready(function () {

    $('#btn_nuevo').click(function () {
        location.href = 'Agregar';
    });

    cargaCBXall();
    listarFichas();

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $("#btn_buscar_ficha").click(function () {
        listarFichas();
    });




    $('#fecha_emision').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $('#fecha_emision_debito').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });



});

function enviaCorreo(data) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {datos: data},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(jqXHR);
        },
        success: function (result) {
            // cargaTablaFicha(result);
        },
        url: "SeguimientoFacturaFicha/EnviarMailCorreccion"
    });
}

function cargaCBXall() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "holding");
        },
        url: "SeguimientoFacturaFicha/getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: {id_holding: $('#holding').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "SeguimientoFacturaFicha/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "SeguimientoFacturaFicha/getUsuarioTipoEcutivoComercial"
    });
}
function cargaTablaFacturas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_fichas')) {
        $('#tbl_fichas').DataTable().destroy();
    }

    $('#tbl_fichas').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "otec"},
            {"mDataProp": "num_orden_compra"},
            {"mDataProp": "empresa"},
            {"mDataProp": "total_otic"},
            {"mDataProp": "total_facturado_otic"},
            {"mDataProp": "total_pendiente_otic"},
            {"mDataProp": "total_empresa"},
            {"mDataProp": "total_facturado_empresa"},
            {"mDataProp": "total_pendiente_empresa"},
            {"mDataProp": "total_ficha"},
            {"mDataProp": "monto_facturado"},
            {"mDataProp": "emision_empresa" },
            {"mDataProp": "emision_otic" },
            {"mDataProp": "monto_pendiente"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "fecha_termino"},
            {"mDataProp": "ejecutivo"},
            {"mDataProp": "dias_transcurridos"},
            {"mDataProp": "estado"}
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'InformeFacturacionFicha'
            }, {
                extend: 'pdf',
                title: 'InformeFacturacionFicha'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [15, 'asc']
    });


}

function listarFichas() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaFacturas(result);
        },
        url: "SeguimientoFacturaFicha/listar"
    });
}


function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");


}
 