$(document).ready(function () {
    recargaLaPagina();

    $(':checkbox[readonly=readonly]').click(function () {
        return false;
    });

    selectIdEdutecno();
    inicializaControles();

    $("#cbx_orden_compra").change(function () {
        var id_oc = "";
        id_oc = $("#cbx_orden_compra").val();

        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {cbx_orden_compra: id_oc},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(jqXHR);
            },
            success: function (result) {
                cargadatosFacturacion(result);
            },
            url: "../../getDatosFacturacionOC"
        });
    });


    $("#agregaFactura").click(function () {

        var id_oc = "";
        var url = "../../../Agregar/index";
        id_oc = $("#cbx_orden_compra").val();
        var url_final = "";
        url_final = url + "/" + id_oc;
        //console.log(url_final);
        window.open(url_final, '_blank');
    });


    $("#todookfactura").click(function () {

        var id_ficha = "";

        id_ficha = $("#id_ficha").val();

        var id_oc = "";
        id_oc = $("#cbx_orden_compra").val();
        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {ficha: id_ficha, oc: id_oc},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(jqXHR);
            },
            success: function (result) {


                swal({
                    title: result[0].titulo,
                    text: result[0].mensaje,
                    type: result[0].tipo_alerta,
                    html: true,
                    confirmButtonText: "Aceptar",
                    showCancelButton: false,
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                        function (isConfirm) {
                            if (isConfirm) {

                                history.back();
                            }
                        });


            },
            url: "../../setTodoOKFicha"
        });
    });

    $("#btn_guardar_observacion_problemas_facturacion").click(function () {

        var id_ficha = "";
        id_ficha = $("#id_ficha").val();
        var id_oc = "";
        id_oc = $("#cbx_orden_compra").val();


        var obs = "";
        obs = $("#facturacion_observacion_problemas").val();

        $.ajax({
            async: true,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {ficha: id_ficha, oc: id_oc, observacion: obs},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                console.log(jqXHR);
            },
            success: function (result) {

                swal({
                    title: result[0].titulo,
                    text: result[0].mensaje,
                    type: result[0].tipo_alerta,
                    html: true,
                    confirmButtonText: "Aceptar",
                    showCancelButton: false,
                    cancelButtonText: "Cancelar",
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {

                        history.back();
                    }
                });

            },
            url: "../../setTodoProblemasFicha"
        });

    });


    $("#problemasfactura").click(function () {
        $("#modal_observacion_problemas_con_ficha").modal('show');
    });


    $("#cbx_orden_compra").trigger("change");
});




function inicializaControles() {
    var id_ficha = "";
    var id_oc = "";
    id_ficha = $("#id_ficha").val();
    id_oc = $("#cbx_orden_compra").val();
    var id_dias = $("#id_dias").val();
    var res = id_dias.split(",");
    for (var i = 0; i < res.length; i++) {
        switch (res[i]) {
            case "1":
                $("#lunes").prop("checked", true);
                break;
            case "2":
                $("#martes").prop("checked", true);
                break;
            case "3":
                $("#miercoles").prop("checked", true);
                break;
            case "4":
                $("#jueves").prop("checked", true);
                break;
            case "5":
                $("#viernes").prop("checked", true);
                break;
            case "6":
                $("#sabado").prop("checked", true);
                break;
            case "7":
                $("#domingo").prop("checked", true);
                break;
        }
    }

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoDocumentos(result);
        },
        url: "../../cargaDatosOCDocumentoResumen"
    });


    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {ficha: id_ficha, oc: id_oc},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaDatoObservacionesFicha(result);
        },
        url: "../../cargaDatosObservacionesFacturar"
    });

}
function selectIdEdutecno() {

    var id_ficha = $('#id_ficha').val();
    var id_edutecno = $('#id_edutecno').val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
//        	$("#modal_edutecno").modal("show");
//        	$("#modal_id_ficha2").html(id_ficha);
//        	$("#id_ficha_edutecno").val(id_ficha);
            cargaCBXSelected('edutecno_cbx', result, $("#id_edutecno_original").val());


        },
        url: "../../getidEdutecnoCBX"
    });
}

function cargadatosFacturacion(data) {

    $("#razaon_social_factura").val(data[0].empresa);
    $("#rut_empresa_factura").val(data[0].rut_empresa);
    $("#giro_empresa_factura").val(data[0].giro);
    $("#comuna_empresa_factura").val(data[0].comuna);
    $("#direccion_factura_factura").val("");
    $("#sence_net_factura").val(data[0].id_sence);
    $("#oc_factura").val(data[0].num_orden_compra);
    $("#oc_otic_empresa_factura").val(data[0].num_orden_compra);
    $("#oc_interna_empresa_factura").val(data[0].num_orden_compra);
    $("#ota_empresa_factura").val(data[0].requiere_ota);
    $("#hes_empresa_factura").val(data[0].requiere_hess);
    $("#cui_empresa_factura").val(data[0].requiere_numero_contrato);

    $("#razon_social_otic_factura").val(data[0].nombre_otic);
    $("#rut_otic_factura").val(data[0].rut_otic);





    $("#link_empresa1").attr("href", '../../../../Empresa/Detalle/index/' + data[0].id_empresa);
    $("#link_empresa2").attr("href", '../../../../Empresa/Detalle/index/' + data[0].id_empresa);
    $("#link_empresa3").attr("href", '../../../../Empresa/Detalle/index/' + data[0].id_empresa);

    $("#monto_otic_factura").val(data[0].valor_total_otic);
    $("#monto_empresa_factura").val(data[0].valor_total_empresa);
    $("#monto_total_oc").val(data[0].valor_total_oc);
    $("#tercera_factura_factura").val(data[0].valor_total_empresa_tercera_factura);
    $("#monto_otic_factura_facturado").val(data[0].valor_facturado_otic);
    $("#monto_empresa_factura_facturado").val(data[0].valor_facturado_empresa);
    $("#monto_total_factura_facturado").val(data[0].valor_facturado_oc);
    $("#monto_total_factura_pendiente_facturar").val(data[0].pendiente_facturar);

}

function cargaTablaDatoDocumentos(data) {

    if ($.fn.dataTable.isDataTable('#tbl_orden_compra_documetnos')) {
        $('#tbl_orden_compra_documetnos').DataTable().destroy();
    }

    $('#tbl_orden_compra_documetnos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "tipo_documento"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";

                    html = '<a href="../../../../../' + o.ruta_documento + '" target="_blank">' + o.nombre_documento.substring(0, o.nombre_documento.length / 2) + '</a>';

                    return html;
                }
            },
            {"mDataProp": "fecha_registro"}
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"clear">',
        order: [2, "desc"]

    });

    $('[data-toggle="tooltip"]').tooltip();

}


function cargaTablaDatoObservacionesFicha(data) {

    if ($.fn.dataTable.isDataTable('#tbl_observaciones_fichas')) {
        $('#tbl_observaciones_fichas').DataTable().destroy();
    }

    $('#tbl_observaciones_fichas').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "usuario"},
            {"mDataProp": "observacion"},
            {"mDataProp": "fecha_registro"}
        ],
        pageLength: 10,
        responsive: false,
        dom: '<"clear">',
        order: [2, "desc"]

    });

    $('[data-toggle="tooltip"]').tooltip();

}