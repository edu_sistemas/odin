$(document).ready(function () {

    $('#fecha_emision').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });

  
    $("#btn_registrar").click(function () {
        if (validarRequeridoForm($("#frm_nota_debito_registro"))) {
            send();
        }
    });
   
});

function send() {

    $.ajax({
        cache: false,
        async: false,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_debito_registro")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // console.log(textStatus);
            // console.log(errorThrown);

            console.log(jqXHR.responseText);
        },
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true
            },
                function (isConfirm) {
                   history.back();
                    
                });
            

         

        },
        url: '../updateNotaDebito'
    });

}





 