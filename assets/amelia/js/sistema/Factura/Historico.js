$(document).ready(function () {

    $('#btn_nuevo').click(function () {
        location.href = 'Agregar';
    });
    listarFichas();
    cargaCBXall();

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true
    });


    $('#fecha_cierre').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $("#btn_buscar_ficha").click(function () {
        listarFichas();
    });

    $("#btn_nueva_nota_credito").click(function () {
        nuevaNotaCredito();
    });

    $("#btn_nueva_nota_debito").click(function () {
        nuevaNotaDebito();
    });


    $("#btn_volver_modal_credito").click(function () {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function () {
            $("#sec_tabla").animate({
                height: "toggle"
            }, 500, function () {

            });
            $("#n_factura_cbx").val("");
            $("#num_nota_credito").val("");
            $("#fecha_emision").val("");
            $("#monto_credito").val("");
        });


    });

    $("#btn_volver_modal_debito").click(function () {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function () {
            $("#sec_tabla_debito").animate({
                height: "toggle"
            }, 500, function () {

            });
            $("#n_factura_cbx_debito").val("");
            $("#num_nota_debito").val("");
            $("#fecha_emision_debito").val("");
            $("#monto_debito").val("");
        });


    });

    $('#fecha_emision').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });

    $('#fecha_emision_debito').datepicker({
        format: "dd-mm-yyyy",
        weekStart: 1,
        maxViewMode: 2,
        todayBtn: "linked",
        language: "es"
    });



    $("#btn_agregar_nota_credito").click(function () {
        if ($("#n_factura_cbx").val() == "" || $("#num_nota_credito").val() == "" || $("#fecha_emision").val() == "" || $("#monto_credito").val() == "") {
            swal("Error", "Debe completar todos los campos solicitados", "error");
        } else {
            guardarNotaCredito();
        }
    });

    $("#btn_agregar_nota_debito").click(function () {
        if ($("#n_factura_cbx_debito").val() == "" || $("#num_nota_debito").val() == "" || $("#fecha_emision_debito").val() == "" || $("#monto_debito").val() == "") {
            swal("Error", "Debe completar todos los campos solicitados", "error");
        } else {
            guardarNotaDebito();
        }
    });

});

function cargaCBXall() {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "holding");
        },
        url: "Listar/getHoldingCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //data: {id_holding: $('#holding').val()},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {

            cargaCombo(result, "empresa");
        },
        url: "Listar/getEmpresaCBX"
    });

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'GET',
        //  data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCombo(result, "ejecutivo");
        },
        url: "Listar/getUsuarioTipoEcutivoComercial"
    });
}

function cargaTablaFacturas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_fichas')) {
        $('#tbl_fichas').DataTable().destroy();
    }

    $('#tbl_fichas').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "categoria"},
            {"mDataProp": "ejecutivo"},
            {"mDataProp": "id_orden_compra"},
            {"mDataProp": "empresa"},
            {"mDataProp": "otic"},
            {"mDataProp": "descripcion_producto"},
            {"mDataProp": "fecha_fin"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    if (o.id_categoria == 1) {
                        html += '<table style="font-size:90%">';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Otic:</b></td><td>';
                        html += o.valor_sence;
                        html += '</td></tr>';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Emp:</b></td><td>';
                        html += o.valor_empresa;
                        html += '</td></tr>';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Total:</b></td><td><b>';
                        html += o.valor_total;
                        html += '</b></td></tr>';
                        html += '</table>';
                    } else {
                        html += '<table style="font-size:90%">';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Otic:</b></td><td>';
                        html += '-';
                        html += '</td></tr>';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Emp:</b></td><td>';
                        html += o.valor_empresa;
                        html += '</td></tr>';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Total:</b></td><td><b>';
                        html += o.valor_total;
                        html += '</b></td></tr>';
                        html += '</table>';
                    }
                    return html;
                }
            },
//            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
//                    var html = "";
//
//                    html += '<table style="font-size:90%">';
//                    html += '<tr><td style="padding-right:5px;" ><b>';
//                    html += 'Facturas:</b></td><td>';
//                    html += o.cantidad_facturas;
//                    html += '</td></tr>';
//                    html += '<tr><td style="padding-right:5px;" ><b>';
//                    html += 'N. de C:</b></td><td>';
//                    html += o.nota_credito;
//                    html += '</td></tr>';
//                    html += '<tr><td style="padding-right:5px;" ><b>';
//                    html += 'N. de D:</b></td><td>';
//                    html += o.nota_debito;
//                    html += '</td></tr>';
//                    html += '</table>';
//                    return html;
//                }
//            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    if (o.id_categoria == 1) {
                        html += '<table style="font-size:90%">';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Otic:</b></td><td>';
                        html += o.saldo_otic;
                        html += '</td></tr>';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Emp:</b></td><td>';
                        html += o.saldo_empresa;
                        html += '</td></tr>';

                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Total:</b></td><td><b>';
                        html += o.saldo_total;
                        html += '</b></td></tr>';

                        html += '</table>';
                    } else {
                        html += '<table style="font-size:90%">';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Otic:</b></td><td>';
                        html += '-';
                        html += '</td></tr>';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Emp:</b></td><td>';
                        html += o.saldo_empresa;
                        html += '</td></tr>';
                        html += '<tr><td style="padding-right:5px;" ><b>';
                        html += 'Total:</b></td><td><b>';
                        html += o.saldo_total;
                        html += '</b></td></tr>';
                        html += '</table>';
                    }
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle ">Accion<span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu pull-right">';
                    var a = "";

                    if (o.id_categoria == 1) {
                        a = "DetalleFicha/index/" + o.id_orden_compra + "/0";
                    } else {
                        a = "DetalleArriendo/index/" + o.id_orden_compra + "/0";
                    }

                    html += '<li><a href=' + a + '>Ver Detalle</a></li>';

                    /* if (o.id_categoria == 1) {
                     var x = "'Agregar/index/" + o.id_orden_compra + "'";
                     html += '<li><a onclick="location.href=' + x + '">Agregar Factura</a></li>';
                     }*/



                    html += '<li><a onclick="verFacturas(' + o.id_orden_compra + ')">Ver Factura(s)</a></li>';

                    html += '<li><a onclick="openModalNotaDeCredito(' + o.id_orden_compra + ')">Notas de Crédito</a></li>';
                    html += '<li><a onclick="openModalNotaDeDebito(' + o.id_orden_compra + ')">Notas de Débito</a></li>';
                    html += ' </ul>';
                    html += ' </div>';
                    $("#oc_modal_credito").text(o.num_orden_compra);
                    return html;
                }
            }
        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [0, 'desc']
    });


}

function listarFichas() {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            $("#loader").html("");
        },
        beforeSend: function () {
            $("#loader").html("<h2>Cargando...</h2>");
        },
        success: function (result) {
            $("#loader").html("");
            cargaTablaFacturas(result);
        },
        url: "Historico/listar"
    });
}

function verFacturas(id_orden_compra) {

    $("#formfac").attr('style', 'display:none;');
    $("#listaFac").attr('style', 'left: 50px;');

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('#modal_facturas').modal("show");
            cargaModalFacturas(result);
        },
        url: "Historico/getFacturasByOC"
    });

}

function cargaModalFacturas(data) {
    $('#oc_modal').html(data[0].num_orden_compra);
    $('#id_oc_hidden').val(data[0].id_orden_compra);
    var html = "";

    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function (i, item) {

        if (item.n_factura == null) {
            html += '<p style="margin: 35px; padding: auto; text-align:center; vertical-align:center;"><b>No existen facturas para la orden de compra.</b></p>';

        } else {
            var pStyle = "black";

            if (item.id_estado_factura == "1") {
                pStyle = "red";
            }
            if (item.id_estado_factura == "2") {
                pStyle = "green";
            }
            if (item.id_estado_factura == "3") {
                pStyle = "blue";
            }
            html += '<tr>';
            html += '<td> ' + item.n_factura + ' </td>';
            var tipoEntrega = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.fecha_registro + '</td>';
            html += '<td>' + item.monto_factura + '</td>';
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td>' + item.fecha_entrega + '</td>';
            html += '<td style="color:' + pStyle + ';">' + item.nombre_estado + ' (' + item.fecha_estado + ')</td>';

            html += '<td>';

            html += '  <div class="btn-group">';
            html += '  <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle ">Opciones<span class="caret"></span></button>';
            html += '  <ul class="dropdown-menu pull-right">';
            html += '	<li><a onclick="formulario(' + item.id_factura + ',' + tipoEntrega + ');">Entrega</a></li>';
            html += '	<li><a onclick="cambiarEstadoFactura(' + item.id_factura + ', ' + item.id_estado_factura + ')">Estado Factura</a></li>';
            html += ' </ul>';
            html += ' </div>';

            html += '</td></tr>';
        }

    });

    $('#tbl_documentos_factura tbody').html(html);
}

function formulario(id, nombre) {
    $("#listaFac").animate({
        left: "+=50",
        height: "toggle"
    }, 500, function () {
        $("#formfac").animate({
            left: "+=50",
            height: "toggle"
        }, 500, function () {
            //buscar lista de tipo entrega
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: $('#frm').serialize(),
                error: function (jqXHR, textStatus, errorThrown) {
                },
                success: function (result) {
                    var html = "";
                    var idcbx = 'tipo_entrega_f';
                    $('#id_fact').val(id);
                    $.each(result, function (i, item) {
                        var selected = '';
                        if (nombre.toLowerCase() == item.text.toLowerCase()) {
                            selected = "selected"
                        }
                        html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                        selected = '';
                    });

                    $("#" + idcbx).html(html);
                },
                url: "Historico/tipo_entrega"
            });
        });
    });

}

function cambiarEstadoFactura(id_factura, id_estado_factura) {
    var data = cargaEstadoFacturaCBX(id_estado_factura);
    var html5 = "";
    html5 += '<select id="estado_factura_cbx">';
    $.each(data, function (i, item) {


        html5 += '<option value="';
        html5 += item.id;
        if (item.id == id_estado_factura) {
            html5 += '" selected>';
        } else {
            html5 += '">';
        }
        html5 += item.text;
        html5 += '</option>';
    });
    html5 += '</select>';

    swal({
        title: "Seleccione el nuevo estado de la factura",
        text: html5,
        //type: "warning",
        html: true,
        showCancelButton: true,
        //confirmButtonColor: "#DD6B55",
        confirmButtonText: "Actualizar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {

                    var id_estado_nuevo = $('#estado_factura_cbx').val();
                    $.ajax({
                        async: false,
                        cache: false,
                        dataType: "json",
                        type: 'POST',
                        data: {id_factura: id_factura, id_estado_nuevo: id_estado_nuevo},
                        error: function (jqXHR, textStatus, errorThrown) {
                        },
                        success: function (result) {
                            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                        },
                        url: "Historico/updateEstadoFactura"
                    });

                    verFacturas($("#id_oc_hidden").val());
                }
            });




}

function cancelarForm() {
    $("#formfac").animate({
        left: "+=50",
        height: "toggle"
    }, 500, function () {
        $("#listaFac").animate({
            left: "+=50",
            height: "toggle"
        }, 500, function () {
            // Animation complete.
        });
    });
}

function enviarForm() {

    var id = $('#id_fact').val();
    var id_tipo = $('#tipo_entrega_f').val();
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_fact: id, id_tipo_entrega: id_tipo},
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
        },
        url: "Historico/updateTipoEntrega"
    });


}

function cargaEstadoFacturaCBX(id_estado_factura) {

    var resultArray = [];

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: {id_orden_compra: id_orden_compra},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //console.log(textStatus + errorThrown);
        },
        success: function (result) {

            //cargaCBXSelected("estado_factura_cbx", result, id_estado_factura);
            resultArray = result;

        },
        url: "Historico/getEstadoFacturaCBX"
    });

    return resultArray;
}


function cargaCombo(midata, item) {

    $("#" + item).select2({
        placeholder: "Seleccione",
        allowClear: true,
        data: midata
    });

    $("#" + item + " option[value='0']").remove();
    // $("#"+item).val($("#id_holding_seleccionado").val()).trigger("change");


}


//METODOS PARA NOTA DE CREDITO 

function openModalNotaDeCredito(id_orden_compra) {

    $("#id_oc_hidden").val(id_orden_compra);
    $('#sec_update_tipo_entrega_nc').attr('style', 'display:none');

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_nota_credito").modal("show");
            loadTableNotaDeCredito(result);
        },
        url: "Historico/getNotasbyOrdenCompra"
    });

//    getProgramasCBX();

}

function nuevaNotaCredito() {
    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo").animate({
            height: "toggle"
        }, 500, function () {

        });
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc';

            $.each(result, function (i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });



}

function loadTableNotaDeCredito(data) {

    $("#sec_tabla").css({
        'display': 'block'
    });

    $("#sec_comentarios").css({
        'display': 'none'
    });

    $("#sec_nuevo").css({
        'display': 'none'
    });

    $("#sec_agregar_programa").css({
        'display': 'none'
    });

    $("#sec_editar").css({
        'display': 'none'
    });


    if (data[0] == undefined || data[0].id_factura == null) {
        $("#btn_nueva_nota_credito").prop('disabled', true);
        $("#btn_nueva_nota_credito").prop('title', 'Para crear una nota de crédito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_credito").prop('disabled', false);
        $("#btn_nueva_nota_credito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';
    $('#tbl_credito tbody').html(html);
    $.each(data, function (i, item) {

        if (item.id_nota_credito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += '<td>' + item.fecha_emision + '</td><td>' + item.n_factura + '</td><td>' + item.num_nota_credito + '</td><td>' + item.monto_credito + '</td>';


            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaCreditoEntrega(' + item.id_nota_credito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';
        }
    });

    $('#tbl_credito tbody').html(html);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_oc_hidden").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("n_factura_cbx", result);
        },
        url: "Historico/getFacturasbyOcCBX"
    });

}
function notaCreditoEntrega(id, nombre) {

    $("#sec_tabla").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_update_tipo_entrega_nc").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    //ajax para llenar select y id hidden
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nc_2';
            $('#id_nc').val(id);
            $.each(result, function (i, item) {
                var selected = '';
                if (nombre.toLowerCase() == item.text.toLowerCase()) {
                    selected = "selected"
                }
                html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });
}

function cancelarFormNc() {
    $("#sec_update_tipo_entrega_nc").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_tabla").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
}
function enviarFormNc() {
    //alert('se envia formulario para update de nc');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_nc: $("#id_nc").val(),
            id_tipo_entrega: $("#tipo_entrega_nc_2").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#sec_update_tipo_entrega_nc').attr('style', 'display:none');
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_orden_compra: $("#id_oc_hidden").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    // cargaCombo(result, "modalidad");
                    // cosole.log(result);
                    $("#modal_nota_credito").modal("show");
                    loadTableNotaDeCredito(result);
                },
                url: "Historico/getNotasbyOrdenCompra"
            });
        },
        url: "Historico/updateTipoEntrega_nc"
    });
}


function guardarNotaCredito() {
    $.ajax({

        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_credito")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: "Guardado",
                text: "La Nota de Crédito ha sido ingresada exitosamente.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "OK",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                    function (isConfirm) {
                        openModalNotaDeCredito($("#id_oc_hidden").val());
                        $("#n_factura_cbx").val("");
                        $("#num_nota_credito").val("");
                        $("#fecha_emision").val("");
                        $("#monto_credito").val("");
                        listarFichas();
                    });
        },
        url: "Historico/setNotaCredito"
    });
}

//METODOS PARA NOTA DE DEBITO 

function openModalNotaDeDebito(id_orden_compra) {

    $("#id_oc_hidden_debito").val(id_orden_compra);
    $('#sec_update_tipo_entrega_nd').attr('style', 'display:none');

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            // cargaCombo(result, "modalidad");
            // cosole.log(result);
            $("#modal_nota_debito").modal("show");
            loadTableNotaDeDebito(result);
        },
        url: "Historico/getNotasDebitobyOrdenCompra"
    });

//    getProgramasCBX();

}

function nuevaNotaDebito() {
    $("#sec_tabla_debito").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_nuevo_debito").animate({
            height: "toggle"
        }, 500, function () {

        });
    });

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nd';

            $.each(result, function (i, item) {
                var selected = '';

                html += '<option value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });

}

function loadTableNotaDeDebito(data) {

    $("#sec_tabla_debito").css({
        'display': 'block'
    });

    $("#sec_comentarios_debito").css({
        'display': 'none'
    });

    $("#sec_nuevo_debito").css({
        'display': 'none'
    });

    $("#sec_agregar_programa_debito").css({
        'display': 'none'
    });

    $("#sec_editar_debito").css({
        'display': 'none'
    });


    if (data[0] == undefined || data[0].id_factura == null) {
        $("#btn_nueva_nota_debito").prop('disabled', true);
        $("#btn_nueva_nota_debito").prop('title', 'Para crear una nota de débito, debe haber al menos 1 factura emitida');
    } else {
        $("#btn_nueva_nota_debito").prop('disabled', false);
        $("#btn_nueva_nota_debito").prop('title', '');
    }

    var html = "";
    var tipo_entre = '';
    $('#tbl_debito tbody').html(html);
    $.each(data, function (i, item) {
        if (item.id_nota_debito != null) {
            html += '<tr>';
            html += '<td style="width: 1px; white-space: nowrap;">' + (i + 1) + '</td>';
            html += '<td>' + item.fecha_emision + '</td><td>' + item.n_factura + '</td><td>' + item.num_nota_debito + '</td><td>' + item.monto_debito + '</td>';


            if (item.id_tipo_facturacion == "1") {
                html += '<td><b>OTIC</b></td>';
            }
            if (item.id_tipo_facturacion == "2") {
                html += '<td><b>EMPRESA</b></td>';
            }
            tipo_entre = "'" + item.tipo_entrega + "'";
            html += '<td>' + item.tipo_entrega + '</td>';
            html += '<td><button class="btn btn-success" onclick="notaDebitoEntrega(' + item.id_nota_debito + ',' + tipo_entre + ');">Entrega</button></td>';
            html += '</tr>';

        }

    });

    $('#tbl_debito tbody').html(html);

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: $("#id_oc_hidden_debito").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaCBX("n_factura_cbx_debito", result);
        },
        url: "Historico/getFacturasbyOcCBX"
    });

}
function enviarFormNd() {
    //alert('se envia formulario para update de nd');
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_nd: $("#id_nd").val(),
            id_tipo_entrega: $("#tipo_entrega_nd_2").val()
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
            $('#sec_update_tipo_entrega_nd').attr('style', 'display:none');
            $.ajax({
                async: false,
                cache: false,
                dataType: "json",
                type: 'POST',
                data: {
                    id_orden_compra: $("#id_oc_hidden_debito").val()
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // code en caso de error de la ejecucion del ajax
                    console.log(textStatus + errorThrown);
                },
                success: function (result) {
                    // cargaCombo(result, "modalidad");
                    // cosole.log(result);
                    $("#modal_nota_debito").modal("show");
                    loadTableNotaDeDebito(result);
                },
                url: "Historico/getNotasDebitobyOrdenCompra"
            });
        },
        url: "Historico/updateTipoEntrega_nd"
    });
}
function cancelarFormNd() {
    $("#sec_update_tipo_entrega_nd").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_tabla_debito").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
}


function notaDebitoEntrega(id, nombre) {
    $("#sec_tabla_debito").animate({
        height: "toggle"
    }, 500, function () {
        $("#sec_update_tipo_entrega_nd").animate({
            height: "toggle"
        }, 500, function () {

        });
    });
    //ajax para llenar select y id hidden
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#frm').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            var html = "";
            var idcbx = 'tipo_entrega_nd_2';
            $('#id_nd').val(id);
            $.each(result, function (i, item) {
                var selected = '';
                if (nombre.toLowerCase() == item.text.toLowerCase()) {
                    selected = "selected"
                }
                html += '<option ' + selected + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                selected = '';
            });

            $("#" + idcbx).html(html);
        },
        url: "Historico/tipo_entrega"
    });
}

function guardarNotaDebito() {
    $.ajax({

        cache: false,
        async: true,
        contentType: false,
        processData: false,
        dataType: 'json',
        type: 'POST',
        data: new FormData($("#frm_nota_debito")[0]),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            swal({
                title: "Guardado",
                text: "La Nota de Débito ha sido ingresada exitosamente.",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#8CD4F5",
                confirmButtonText: "OK",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                    function (isConfirm) {
                        openModalNotaDeDebito($("#id_oc_hidden_debito").val());
                        $("#n_factura_cbx_debito").val("");
                        $("#num_nota_debito").val("");
                        $("#fecha_emision_debito").val("");
                        $("#monto_debito").val("");
                        listarFichas();
                    });
        },
        url: "Historico/setNotaDebito"
    });
}

//---------------------------MODAL DOCUMENTOS INTERNOS-----------------------

//Modal Documentos Ficha

function getDocumentosFicha(id_ficha, num_ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_ficha: id_ficha
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('fichadoc').text(num_ficha);
            $("#modal_documentos_ficha").modal("show");
            loadTableDocumentosFicha(result);
        },
        url: "Historico/getDocumentosFicha"
    });
}
function loadTableDocumentosFicha(data) {
    var html = "";
    $('#tbl_documentos_factura tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';
        if (item.nombre_documento % 2 == 0) {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a>';
        } else {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a>';
        }
        html += '</td></tr>';
    });

    $('#tbl_documentos_factura tbody').html(html);
}

//Modal Documentos Rectificacion

function getDocumentosRectificacion(id_orden_compra, num_orden_compra) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {
            id_orden_compra: id_orden_compra
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            console.log(textStatus + errorThrown);
        },
        success: function (result) {
            $('ocdoc').text(num_orden_compra);
            $("#modal_documentos_rectificacion").modal("show");
            loadTableDocumentosRectificacion(result);
        },
        url: "Historico/getDocumentosRectificacion"
    });
}

function loadTableDocumentosRectificacion(data) {
    var html = "";
    $('#tbl_documentos_rectificacion tbody').html(html);
    $.each(data, function (i, item) {
        html += '<tr><td>';
        html += item.nombre_documento;
        html += '</td><td>';
        html += item.fecha_registro;
        html += '</td><td>';
        if (item.nombre_documento % 2 == 0) {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, item.nombre_documento.length / 2) + '</a>';
        } else {
            html += '<a href="../../' + item.ruta_documento + '" target="_blank">' + item.nombre_documento.substring(0, (item.nombre_documento.length / 2) + 0.5) + '...' + item.nombre_documento.slice(-6) + '</a>';
        }
        html += '</td></tr>';
    });

    $('#tbl_documentos_rectificacion tbody').html(html);
}