// JavaScript Document form_perso
$(document).ready(function () {
    recargaLaPagina();
    llamaFichas();

    $("#btn_buscar_ficha").click(function () {
        llamaFichas();
    });
    llamaEmpresas();
    $("#empresa").select2({
        placeholder: "Todos",
        allowClear: true
    });
    $("#tipo_entrega_btn").click(function () {
        var id_ficha = $('#id_ficha').val();
        var id_entrega = $('#tipo_entrega').val();
        $.ajax({
            async: false,
            cache: false,
            dataType: "json",
            type: 'POST',
            data: {id_ficha: id_ficha, id_tipo: id_entrega},
            error: function (jqXHR, textStatus, errorThrown) {
                // code en caso de error de la ejecucion del ajax
                // console.log(textStatus + errorThrown);
                console.log(jqXHR, textStatus, errorThrown);
            },
            success: function (result) {
                alerta(result[0].titulo, result[0].mensaje, result[0].tipo_alerta);
                llamaFichas();
            },
            url: "Listar/updateTipoentrega"
        });

    });

    $('#Perso').prop('checked', false);
    $('#n_encargado').prop('disabled', true);
    $('#c_encargado').prop('disabled', true);
    $('#empresa_p').prop('disabled', true);

    $("input[name='optionsRadios']").on('click', function () {
        if ($('#optionsRadios3').is(':checked')) {
            $('#saludos').prop('disabled', false);
        } else {
            $('#saludos').prop('disabled', true);
        }
    });


    $('#Perso').on('click', function () {
        if ($('#Perso').is(':checked')) {

            $('#n_encargado').prop('disabled', false);
            $('#c_encargado').prop('disabled', false);
            $('#empresa_p').prop('disabled', false);
        } else {
            $('#n_encargado').prop('disabled', true);
            $('#c_encargado').prop('disabled', true);
            $('#empresa_p').prop('disabled', true);
        }
    });


});


function llamaFichas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_busqueda').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            TablaFichas(result);
        },
        url: "./Listar/fichas"
    });
}

function llamaEmpresas() {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            cargaCombo(result, 'empresa');
        },
        url: "./Listar/Empresas"
    });
}

function TablaFichas(data) {

    if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
        $('#tbl_ficha').DataTable().destroy();
    }

    $('#tbl_ficha').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "modalidad"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "fecha_fin"},
            {"mDataProp": "empresa"},
            {"mDataProp": "tipo_entrega"},

            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    //console.log(o.diploma);
                    if (o.diploma == 0) {
                        html = '<i style="color:red" class="fa fa-times"></i>';
                    } else {
                        html = '<i style="color:green" class="fa fa-check"></i>';
                    }
                    return html;
                }
            },
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var nombre = "'" + o.tipo_entrega + "'";
                    var html = "";
                    html += '  <div class="btn-group">';
                    html += '  <button data-toggle="dropdown" class="btn btn-default dropdown-toggle ">Accion<span class="caret"></span></button>';
                    html += '  <ul class="dropdown-menu pull-right">';


                    html += '  <li><a target="_blank" onclick="personalCertificado(' + o.id_ficha + ');" target="_blank" >Generar Diplomas</li>';

                    html += '	<li><a onclick="entregarCert(' + nombre + ',' + o.id_ficha + ');">Entregar centificado</a></li> ';
                    html += ' </ul>';
                    html += ' </div>';


                    return html;
                }
            }
        ],
        pageLength: 10,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Sence'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        "order": [[0, "desc"]]
    });
}
function entregarCert(nombre, id) {
    $('#id_ficha').val(id);
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        //data: $('#tbl_modulo').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            var idcbx = 'tipo_entrega';
            var html = "";
            var sele = '';
            $.each(result, function (i, item) {
                if (item.text.toLowerCase() == nombre.toLowerCase()) {
                    sele = 'selected';
                }
                html += '<option ' + sele + ' value="' + item.id + '" id="' + item.text.toLowerCase() + '" title="' + item.text.toLowerCase() + '">' + item.text + '</option>';
                sele = '';

            });

            $("#" + idcbx).html(html);

        },
        url: "Listar/tipo_entrega"
    });
    $('#modalCertificados').modal();

}
function cargaCombo(midata, id) {
    $("#empresa").select2({
        placeholder: "Todos",
        allowClear: true,
        data: midata
    });
}
function personalCertificado(id) {

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha : id},
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            // console.log(textStatus + errorThrown);
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            $('#OCbyidFicha').empty();
            $('#OCbyidFicha').append(new Option('Todas O.C.', '0', true, true));
            $.each(result, function(key, value) {   
             $('#OCbyidFicha')
                 .append($("<option></option>")
                            .attr("value",value.id_orden_compra)
                            .text(value.num_orden_compra)); 
            });
        },
        url: "Listar/OC_byIdFicha"
    });


    $('#id_perso').val(id);
    $('#modalCertificadoPersonalizado').modal();
    $('#n_encargado').val('');
    $('#c_encargado').val('');
    $('#empresa_p').val('');
    $('#saludos').val('');

    $('#Perso').prop('checked', false);
    $('#n_encargado').prop('disabled', true);
    $('#c_encargado').prop('disabled', true);
    $('#empresa_p').prop('disabled', true);

    $("#optionsRadios1").prop("checked", true);


    //   $('#form_perso')[0].reset();
}

function validarPerso() {

    if ($('#Perso').is(':checked')) {
        if ($('#n_encargado').val() == '') {
            swal({
                title: "Datos faltantes",
                text: "Favor ingrese el nombre del encargado",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, entiendo!",
                closeOnConfirm: true
            },
                    function () {
                        return false;
                    });
            return false;
        }
        if ($('#c_encargado').val() == '') {
            swal({
                title: "Datos faltantes",
                text: "Favor ingrese el cargo del encargado",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, entiendo!",
                closeOnConfirm: true
            },
                    function () {
                        return false;
                    });
            return false;
        }
        if ($('#empresa_p').val() == '') {
            swal({
                title: "Datos faltantes",
                text: "Favor ingrese el nombre de la empresa",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, entiendo!",
                closeOnConfirm: true
            },
                    function () {
                        return false;
                    });
            return false;
        }
    }
    $('#form_perso').submit();
}