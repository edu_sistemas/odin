 /* Ultima edicion: 2018 - 01 - 03[Jessica Roa]<jroa@edutecno.com>
 * Fecha creacion: 2018 - 01 - 03[Jessica Roa]<jroa@edutecno.com>
 */
$(document).ready(function () {
    $("#calendario_mes").datepicker("setDate", '-2m');
    listarRectificaciones();
    $('#btn_filtrar').click(function () {
        listarRectificaciones();
    });
    
});
//DateIcker para fechas con solo mes y año
$('#calendario_mes').datepicker({
    format: "mm-yyyy",
    endDate: '-2m',
    defaultViewDate: '-2m',
    minViewMode: 1,
    maxViewMode: 1,
    todayBtn: false,
    language: "es"
});

// evento keypress bloqueado para que no ingresen valores por teclado
$('#calendario_mes, #calendario_mes_x, #calendario_mes_e, #calendario_mes_f').keypress(function (e) {
    e.preventDefault();
});

function cargaTablaDescuentos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_descuentos_productos')) {
        $('#tbl_descuentos_productos').DataTable().destroy();
    }
    $('#tbl_descuentos_productos')
        .DataTable(
        {
            "aaData": data,
            "aoColumns": [
                {"mDataProp": "nom_ejecutivo"},
                {"mDataProp": "num_ficha"},
                {"mDataProp": "fecha_inicio"},
                {"mDataProp": "monto_rectificacion"},
                {"mDataProp": "val_final"}

            ],
            pageLength: 25,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {
                    extend: 'copy'
                },
                {
                    extend: 'csv'
                },
                {
                    extend: 'excel',
                    title: 'Empresa'
                },
                {
                    extend: 'pdf',
                    title: 'Empresa'
                },
                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass(
                            'white-bg');
                        $(win.document.body).css('font-size',
                            '10px');
                        $(win.document.body).find('table')
                            .addClass('compact').css(
                            'font-size', 'inherit');
                    }
                }]
        });
}


function listarRectificaciones() {
    console.log($('#frm_filtrarxxxx').serialize());
    var mesrg = $('#calendario_mes').val().split('-');
    console.log(mesrg);
    var rgfinal = [];
    if (mesrg[0] < 11){
        rgfinal[0] = (parseInt(mesrg[0]) + 2);
        mesrg[0] = (parseInt(mesrg[0]) + 1);
        rgfinal[1] = mesrg[1];
    } else {
        if (mesrg[0] == 12) {
            mesrg[0] = '01';
            mesrg[1] = (parseInt(mesrg[1]) + 1);
            rgfinal[0] = '02';
            rgfinal[1] = (mesrg[1]);
        } else {
            if (mesrg[0] == 11){
                mesrg[0] = (parseInt(mesrg[0]) + 1);
                rgfinal[0] = '01';
                rgfinal[1] = (parseInt(mesrg[1]) + 1);
            }
        }
    }
    var res = [];
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: $('#frm_filtrarxxxx').serialize(),
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR, textStatus, errorThrown);
        },
        success: function (result) {
            console.log(result);
            for (let i = 0; i < result.length; i++) {
                if (result[i].monto_rectificacion != '-'){
                    res.push(result[i]);
                }
            }
            $('.rango').html('Del: 21-' + mesrg[0] + '-' + mesrg[1] + ' Al: 20-' + rgfinal[0] + '-' + rgfinal[1]);
            console.log(res);
            cargaTablaDescuentos(res);
        },
        url: "Listar/listarRectificaciones"
    });
}




