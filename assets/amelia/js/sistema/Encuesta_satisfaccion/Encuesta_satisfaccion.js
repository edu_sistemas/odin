$( document ).ready(function() {
    listarFichas();
});

function listarFichas(){
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
        },
        success: function (result) {
            cargaTablaFicha(result);
        },
        url: "Encuesta_satisfaccion/listarFichas"
    });
}

function cargaTablaFicha(data) {
    
        if ($.fn.dataTable.isDataTable('#tbl_ficha')) {
            $('#tbl_ficha').DataTable().destroy();
        }
    
        $('#tbl_ficha').DataTable({
            "aaData": data,
            "aoColumns": [
                {"mDataProp": "num_ficha"},
                {"mDataProp": "razon_social_empresa"},
                {"mDataProp": "fecha_inicio"},
                {"mDataProp": "fecha_termino"},
                {"mDataProp": "cantidad_alumnos"},
                {"mDataProp": "relator"},
                {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                        var html = "";
                        html = '<a href="./Encuesta_ficha_listar/index/'+o.id_ficha+'"><button class="btn btn-primary">Ingresar / Ver Encuestas</button></a>';
                        return html;
                    }
                }    
            ],
            pageLength: 25,
            responsive: false,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {
                    extend: 'copy'
                }, {
                    extend: 'csv'
                }, {
                    extend: 'excel',
                    title: 'Edutecno'
                }, {
                    extend: 'pdf',
                    title: 'Edutecno'
                }, {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');
                        $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                    }
                }
            ],
            order: [[4, "desc"], [2, "desc"]]
        });
    
        $('[data-toggle="tooltip"]').tooltip();
    }