// JavaScript Document
$(document).ready(function () {
    recargaLaPagina();
    $('#fecha_termino').datepicker();
    $('#fecha_entrega').datepicker();

    listarCursos();
});

function listarCursos() {


    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            cargaTablaCurso(result);
        },
        url: "Listar/listar"
    });
}
function cargaTablaCurso(data) {

    if ($.fn.dataTable.isDataTable('#tbl_cursos')) {
        $('#tbl_cursos').DataTable().destroy();
    }


    $('#tbl_cursos').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "num_ficha"},
            {"mDataProp": "nombre_curso"},
            {"mDataProp": "fecha_inicio"},
            {"mDataProp": "fecha_fin"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {
                    var html = "";
                    html += '  <button class="btn btn-default" onclick="modal(' + o.num_ficha + ');">Detalle</button>';

                    return html;
                }
            }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        order: [0, 'desc']
    });
}
function modal(ficha) {
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {num_ficha: ficha},
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
            listaDocente(result);
            $('#myModal').modal();
        },
        url: "Listar/listarDocentes"
    });

}
function listaDocente(data) {
    if ($.fn.dataTable.isDataTable('#tbl_docentes')) {
        $('#tbl_docentes').DataTable().destroy();
    }


    $('#tbl_docentes').DataTable({
        "aaData": data,
        "ordering": true,
        "aoColumns": [
            {"mDataProp": "nombre_usuario"},
            {"mDataProp": "apellidos_usuario"},
            {"mDataProp": "rut"},
            {"mDataProp": null, "bSortable": false, "mRender": function (o) {


                    var html = "";

                    html += '  <button class="btn btn-default" onclick="modal2(' + o.num_ficha + ',' + o.id_usuario + ');">Crear Contrato</button>';



                    return html;
                }
            }

        ],
        pageLength: 10,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Empresa'
            }, {
                extend: 'pdf',
                title: 'Empresa'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ]
    });
    $('#tbl_docentes').attr("style", "-1px");
}

function modal2(num_ficha, id_user) {
    //buscar datos del contrato
    var banderin = false;
    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {num_ficha: num_ficha, id_usuario: id_user},
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {
             console.log(result);
            if (result[0].rut == null) {
                banderin = true;
            }

            $('#relator_ficha').val(result[0].id_dtl_relator_ficha);
            $('#rut').val(result[0].rut);
            $('#nombre').val(result[0].nombre);
            $('#ficha').val(result[0].num_ficha);
            $('#nombre_curso').val(result[0].nombre_curso);
            $('#horas').val(result[0].duracion_curso);
            $('#fecha_termino').val(result[0].fecha_fin);
            $('#alumnos').val(result[0].cant_alumno);
            $('#horas_pagadas').val(result[0].horas_pagadas);
        },
        url: "Listar/listarDatosDocentes"
    });

    if (banderin) {
      /*  swal('Atención', 'No tiene horas registradas este relator para generar un contrato', 'warning');
        return false;*/
    }

    $.ajax({
        async: false,
        cache: false,
        dataType: "json",
        error: function (jqXHR, textStatus, errorThrown) {
        },
        success: function (result) {

            $('#comuna').select2({
                placeholder: 'Seleccione',
                allowClear: true,
                data: result
            });
        },
        url: "Listar/listarComunas"
    });


    //buscar datos del contrato
    $('#myModal').modal('hide');
    setTimeout(function () {
        $('#myModal2').modal();
    }, 200);

}
function setFechas(fecha) {
    var fechaArr = fecha.split('-');
    var dd = fechaArr[2];
    var mm = fechaArr[1]; //January is 0!

    var yyyy = fechaArr[0]
    if (dd < 10) {
        dd = '' + dd;
    }
    if (mm < 10) {
        mm = '' + mm;
    }
    var today = dd + '-' + mm + '-' + yyyy;


    return today;
}