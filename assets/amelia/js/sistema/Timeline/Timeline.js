$(document).ready(function () {
    recargaLaPagina();


    $("#cbx_fichas").select2({
        placeholder: "Todos",
        allowClear: true
    });

    $("#btn_refresh").click(function () {
        var btn = $(this);
        simpleLoad(btn, true);
        simpleLoad(btn, false);
        abrirTimeline();
    });

    $("#cbx_fichas").change(function () {
        var btn = $("#btn_refresh");
        simpleLoad(btn, true);
        simpleLoad(btn, false);
        abrirTimeline();
    });


    $("#btn_buscar").click(function () {
        abrirTimeline();
    });

    abrirTimeline();

    $('#tabla_historial_eventos_ficha').DataTable({
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},

            {extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]

    });
});
function abrirTimeline() {

    var url_init = "Timeline/";
    var id_ficha_interna = $("#cbx_fichas").val();

    if (id_ficha_interna == "") {
        id_ficha_interna = "";
    }

    if ($("#id_ficha_consulta").val().trim() != "") {
        url_init = "../";
    }

    getPreviewFicha(id_ficha_interna, url_init);
    getHitosFicha(id_ficha_interna, url_init);
    getLogFicha(id_ficha_interna, url_init);
}

function getPreviewFicha(id_ficha_interna, url_init) {
    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha_interna},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", jqXHR.textResponse, "error");
        },
        success: function (result) {
            cargaDatosTimeline(result);

        },
        url: url_init + "getEventosFicha"
    });
}

function getHitosFicha(id_ficha_interna, url_init) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha_interna},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", jqXHR.textResponse, "error");
        },
        success: function (result) {
            cargaDatosHitosTimeline(result);

        },
        url: url_init + "getHitosFicha"
    });

}

function getLogFicha(id_ficha, url_init) {

    $.ajax({
        async: true,
        cache: false,
        dataType: "json",
        type: 'POST',
        data: {id_ficha: id_ficha},
        error: function (jqXHR, textStatus, errorThrown) {

            alerta("Ficha", errorThrown, "error");
        },
        success: function (result) {
            cargaTablaLog(result);
        },
        url: url_init + "getLogFicha"
    });
}

/* carga tablas */
function cargaDatosTimeline(data) {


    var html = "";
    $('#tb_resumen_ficha').html(html);
    $.each(data, function (i, item) {
        html += '<tr><th>Holding:</th>';
        html += '<td>';
        html += item.holding;
        html += '<td>';

        html += '<th>Empresa:</th>';
        html += '<td>';
        html += item.empresa;
        html += '<td></tr>';

        html += '<tr><th>Rut:</th>';
        html += '<td>';
        html += item.rut_empresa;
        html += '<td>';

        html += '<th>Curso:</th>';
        html += '<td>';
        html += item.descripcion_producto;
        html += '<td></tr>';

        html += '<tr><th>Versión:</th>';
        html += '<td>';
        html += item.descripcion_version;
        html += '<td>';

        html += '<th>Modalidad:</th>';
        html += '<td>';
        html += item.modalidad;
        html += '<td></tr>';

        html += '<tr><th>Código Sence:</th>';
        html += '<td>';
        html += item.codigo_sence;
        html += '<td>';

        html += '<th>Fecha inicio:</th>';
        html += '<td>';
        html += item.fecha_inicio;
        html += '<td></tr>';

        html += '<tr><th>Fecha Término:</th>';
        html += '<td>';
        html += item.fecha_fin;
        html += '<td>';

        html += '<th>Horario:</th>';
        html += '<td>';
        html += item.hora_inicio + ' a ' + item.hora_termino;
        html += '<td></tr>';

        html += '<tr><th>Sede:</th>';
        html += '<td>';
        html += item.nombre_sede;
        html += '<td>';

        html += '<th>Sala:</th>';
        html += '<td>';
        html += item.nombre_sala;
        html += '<td></tr>';

        html += '<tr><th>Dirección:</th>';
        html += '<td>';
        html += item.direccion_sede;
        html += '<td>';

        html += '<th>Días:</th>';
        html += '<td>';
        html += item.dias;
        html += '<td></tr>';

        html += '<tr><th>Ejecutivo Comercial:</th>';
        html += '<td>';
        html += item.ejecutivo;
        html += '<td>';

        html += '<th>Observaciones:</th>';
        html += '<td>';
        html += item.comentario_orden_compra;
        html += '<td></tr>';


    });
    $('#tb_resumen_ficha').html(html);


}

function cargaDatosHitosTimeline(data) {

    var html = "";
    var estado_titulo;
    var colores = ["success", "secondary"];

    if (data[0].fecha_ingreso_estado == "-") {

        estado_titulo = "Ingresada";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[1] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_ingreso + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    } else {

        estado_titulo = "Ingresada";

        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[0] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_ingreso + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    }

    if (data[0].fecha_apb_comercial_estado == "-") {

        estado_titulo = "Aprobada";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[1] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_apb_comercial + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';

    } else {
        estado_titulo = "Aprobada";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[0] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_apb_comercial + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    }

    if (data[0].fecha_inicio_estado == "-") {
        estado_titulo = "Fecha inicio";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[1] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_inicio + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    } else {
        estado_titulo = "Fecha inicio";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[0] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_inicio + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    }

    if (data[0].fecha_cierre_estado == "-") {
        estado_titulo = "Fecha Cierre";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[1] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_cierre + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    } else {
        estado_titulo = "Fecha Cierre";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[0] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_cierre + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    }
    if (data[0].fecha_facturado_estado == "-") {
        estado_titulo = "Facturado";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[1] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_facturado + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    } else {

        estado_titulo = "Facturado";
        html += '<li class="timeline-item"><div class="timeline-badge '
                + colores[0] +
                '"><i class="glyphicon glyphicon-check"></i></div><div class="timeline-panel"><div class="timeline-heading"><h4 class="timeline-title">'
                + estado_titulo + '</h4><p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> '
                + data[0].fecha_facturado + '</small></p></div><div class="timeline-body"><p></p></div></div></li>';
    }
    $('#eventos-timeline').html(html);
}

function cargaTablaLog(data) {

    if ($.fn.dataTable.isDataTable('#tabla_historial_eventos_ficha')) {
        $('#tabla_historial_eventos_ficha').DataTable().destroy();
    }

    $('#tabla_historial_eventos_ficha').DataTable({
        "aaData": data,
        "aoColumns": [
            {"mDataProp": "id_log_ficha"},
            {"mDataProp": "hito"},
            {"mDataProp": "responsable"},
            {"mDataProp": "ingresado_por"},
            {"mDataProp": "fecha"},
            {"mDataProp": "comentario"}

        ],
        pageLength: 25,
        responsive: false,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Edutecno'
            }, {
                extend: 'pdf',
                title: 'Edutecno'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                }
            }
        ],
        "order": [[0, "desc"]]
    });

    $('[data-toggle="tooltip"]').tooltip();


}
/* end  carga tablas */