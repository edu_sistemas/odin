$(document).ready(function () {
    //ListarFichas();
    $("#frm_agregar_usuario").submit(function () {
        setUsuario();
        event.preventDefault();
    });
    $("#id_dtl_alumno").prop('disabled', 'disabled');
    getCBX_select2('id_trivia', 'sp_trivia_select_cbx');
    getCBX_select2('id_ficha', 'sp_ficha_trivia_select_cbx');
    getCBX_select2('id_trivia_2', 'sp_trivia_select_cbx');
    getCBX_select2('id_ficha_2', 'sp_ficha_trivia_select_cbx');

    $("#id_ficha").change(function () {
        if ($("#id_ficha").val() != "") {
            $("#id_dtl_alumno").html("");
            $("#id_dtl_alumno").append("<option></option>");
            $("#id_dtl_alumno").prop('disabled', false);
            getCBXByID_select2('id_dtl_alumno', 'sp_alumnos_select_by_ficha_id_cbx', $("#id_ficha").val());
        } else {
            $("#id_dtl_alumno").html("");
            $("#id_dtl_alumno").prop('disabled', 'disabled');
        }
    });

    $("#id_dtl_alumno").change(function () {
        if ($("#id_dtl_alumno").val() != "" || $("#id_dtl_alumno").val() == null) {
            $("#rut").val("");
            $("#dv").val("");
            var txt = $("#id_dtl_alumno option:selected").text().split(" ");
            var rut = txt[0].split("-");
            $("#rut").val(rut[0]);
            $("#dv").val(rut[1]);
        }
    });

    $("input[name='tipo_usuario']").click(function () {
        if ($(this).val() == "edutecno") {
            $("#id_ficha").val("").trigger("change");
            $("#id_ficha").prop('disabled', 'disabled');
            $("#id_dtl_alumno").val("").trigger("change");
            $("#id_dtl_alumno").prop('disabled', 'disabled');
        } else {
            $("#id_ficha").prop('disabled', false);
        }
    });

    $("#btn_seleccionar_ficha").click(function () {
        getAlumnosByFicha($("#id_ficha_2").val());
    });
});

function getAlumnosByFicha(id_ficha) {
    $.ajax({
        url: 'Administrar_Usuarios/getAlumnosByFicha',
        type: 'POST',
        dataType: "json",
        data: { id_ficha: id_ficha },
        success: function (result) {
            cargaTablaAlumnos(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "Problema al enviar el formulario: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
}

function setUsuario() {
    $.ajax({
        url: 'Administrar_Usuarios/setUsuario',
        type: 'POST',
        dataType: "json",
        data: $("#frm_agregar_usuario").serialize(),
        success: function (result) {
            swal({
                title: result[0].titulo,
                text: result[0].mensaje,
                type: result[0].tipo_alerta,
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "Problema al enviar el formulario: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Aceptar",
                closeOnConfirm: true,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        async: false,
        cache: false
    });
}

function ListarFichas() {
    $.ajax({
        url: "Listar/getFichas",
        type: 'POST',
        dataType: "json",
        data: $('#frm_busqueda_personalizada').serialize(),
        beforeSend: function () {
            $("#cargando").html('<img src="../../assets/img/loading.gif">');
        },
        success: function (result) {
            cargaTablaFichas(result);
            $("#contenido").css("display", "block");
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // code en caso de error de la ejecucion del ajax
            //  console.log(textStatus + errorThrown);
            swal({
                title: "Error",
                text: "No se han podido cargar las fichas: " + errorThrown,
                type: "error",
                showCancelButton: false,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Recargar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
                function (isConfirm) {
                    location.reload();
                });
        },
        complete: function () {
            $("#cargando").html("");
        },
        async: true,
        cache: false
    });
}
function cargaTablaAlumnos(data) {
    if ($.fn.dataTable.isDataTable('#tbl_alumnos')) {
        $('#tbl_alumnos').DataTable().destroy();
    }

    var table = $('#tbl_alumnos').DataTable({
        "aaData": data,
        "aoColumns": [
            {
                "mDataProp": null, "bSortable": false, 'mRender': function (o, type, full, meta) {
                    return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(o.rut).html() + '" checked>';
                }
            },
            { "mDataProp": "rut" },
            { "mDataProp": "nombre" },
            { "mDataProp": "apellido_paterno" },
            { "mDataProp": "apellido_materno" }

        ],
        pageLength: 25,
        responsive: true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            {
                extend: 'copy'
            }, {
                extend: 'csv'
            }, {
                extend: 'excel',
                title: 'Categoria'
            }, {
                extend: 'pdf',
                title: 'Categoria'
            }, {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');
                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ],
        order: [[0, "desc"]]
    });

    $('#tbl_alumnos-select-all').prop('checked', 'checked');

    // Handle click on "Select all" control
    $('#tbl_alumnos-select-all').click(function () {
        // Get all rows with search applied
        var rows = table.rows({ 'search': 'applied' }).nodes();
        // Check/uncheck checkboxes for all rows in the table
        $('input[type="checkbox"]', rows).prop('checked', this.checked);
    });

    // Handle click on checkbox to set state of "Select all" control
    $('#tbl_alumnos tbody').on('change', 'input[type="checkbox"]', function () {
        // If checkbox is not checked
        if (!this.checked) {
            var el = $('#tbl_alumnos-select-all').get(0);
            // If "Select all" control is checked and has 'indeterminate' property
            if (el && el.checked && ('indeterminate' in el)) {
                // Set visual state of "Select all" control
                // as 'indeterminate'
                el.indeterminate = true;
            }
        }
    });

    // Handle form submission event
    $('#frm_agregar_ficha').submit(function (e) {
        var form = this;
        $("#tbl_output").html("");
        var html = "";
        // Iterate over all checkboxes in the table
        table.$('input[type="checkbox"]').each(function (i, elem) {
            // If checkbox is checked
            if ($(elem).prop('checked')) {
                var id_trivia = $("#id_trivia_2").val();
                var id_ficha = $("#id_ficha_2").val();
                var rut_alumno = $(elem).val().split("-");
                var rut = rut_alumno[0];
                var dv = rut_alumno[1];
                $.ajax({
                    url: 'Administrar_Usuarios/setUsuario',
                    type: 'POST',
                    dataType: "json",
                    data: {
                        id_trivia: id_trivia,
                        id_ficha: id_ficha,
                        rut: rut,
                        dv: dv,
                        tipo_usuario: "alumno"
                    },
                    success: function (result) {
                        html += '<tr><td>'+$(elem).val() + ': </td><td>' + result[0].mensaje+'</td></tr>';
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        // code en caso de error de la ejecucion del ajax
                        //  console.log(textStatus + errorThrown);
                        html += '<tr><td>'+$(elem).val() + ': </td><td>ERROR</td></tr>';
                    },
                    async: false,
                    cache: false
                });
            }
        });
        $("#tbl_output").html(html);
        e.preventDefault();
    });

}